# -*- coding: utf-8 -*-
from datetime import date
from django.conf import settings

import sqlreports
import sqlreports.custom

class IstinaSQLReportsManager(sqlreports.custom.ReportsManager):
    def check_permissions(self, user, report, operation):
        result = False         # decline access by default
        if operation.startswith("edit"):
            result = user.is_superuser
        elif operation.startswith("run") or operation.startswith("checkbox"):
            result = True

        if settings.DEBUG:
            print "Checking sqlreports permissions for ", operation, "... ", ("Granted" if result else "Rejected")
        return result

    def get_default_params(self, request, report=None):
        """
        Возвращает словарь значений по умолчанию для следующих параметров.
        year (текущий год),
        report_year, (до сентября - предыдущий год)
        user_id, worker_id, (текущий пользователь; worker_idx == f_man_id)
        f_organization_id (МГУ),
        f_department_id - основное место работы текущего пользователя
            (если нет, то первое совместительство)
        """
        result = {}
        result['year'] = date.today().year
        if date.today().month >= 9:
            # Пусть отчетный год начинается в сентябре
            result['report_year'] = date.today().year
        else:
            result['report_year'] = date.today().year - 1
        result['f_organization_id'] = 214524 #--FIXME: это МГУ
        result['user_id'] = request.user.id
        result['percent'] = '%%'
        try:
            worker = request.user.get_profile().worker
            result['worker_id'] = worker.id
            emp = worker.current_main_employments or worker.current_employments
            if emp:
                result['f_department_id'] = emp[0].department.id
        except:
            pass
        return result


def init():
    sqlreports.custom.REPORTS_MANAGER = IstinaSQLReportsManager()
