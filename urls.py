# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.views.generic.simple import direct_to_template
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from userprofile.views import public
from organizations.models import Organization
from workers.views import worker_reports_index
import authority
import initscripts.init_sqlreports

admin.autodiscover()
authority.autodiscover()
initscripts.init_sqlreports.init()

urlpatterns = patterns('',
    url(r'^$', direct_to_template, {'template': 'startpage.html',
                                    'extra_context': {'organizations': [Organization.objects.get_msu()]}
                                    }, name='startpage'),
    url(r'^add/$', direct_to_template, {'template': 'add_work.html'}, name='add_work'),
    url(r'^profile/(?P<username>.+)/su/$', "workers.views.su_as_user", name="su_as_user"),
    url(r'^profile/(?P<username>.+)/invalidate_cache/$', "workers.views.invalidate_cache", name="invalidate_cache"),
    url(r'^profile/(?P<username>.+)/reset_password/$', "workers.views.admin_set_password", name="admin_set_password"),
    url(r'^profile/(?P<username>.+)/send_message/$', "workers.views.send_private_email", name="send_private_email"),
    url(r'^profile/(?P<username>.+)/history/$', "common.views.history_view", name="user_history"),
    url(r'^profile/(?P<username>.+)/$', public, name='user_profile_public'),
    url(r'^users/(?P<username>.+)/$', public, name='user_profile_public_users'), # User.get_absolute_url
    url(r'^home/', include('workers.urls.home')),
    url(r'^search/$', direct_to_template, {'template': 'search.html'}, name='search'),
    url(r'^history/$', "common.views.history_view", name="history"),
    url(r'^robots\.txt$', direct_to_template, {'template': 'robots.txt', 'mimetype': 'text/plain'}),
    (r'^accounts/', include('userprofile.urls')),
    (r'^activity/', include('actstream.urls')),
    (r'^admin/', include(admin.site.urls)),
    (r'^awards/', include('awards.urls')),
    (r'^benchmarks/', include('benchmarks.urls')),
    (r'^certificates/', include('certificates.urls')),
    (r'^collections/', include('icollections.urls')),
    (r'^common/', include('common.urls')),
    (r'^conferences/', include('conferences.urls')),
    (r'^courses/', include('courses.urls')),
    (r'^diplomas/', include('diplomas.urls')),
    (r'^dissertation_councils/', include('dissertation_councils.urls')),
    (r'^dissertations/', include('dissertations.urls')),
    (r'^feedback/', include('feedback.urls')),
    (r'^investments/', include('investments.urls')),
    (r'^intrelations/', include('intrelations.urls')),
    (r'^journals/', include('journals.urls')),
    (r'^organizations/', include('organizations.urls')),
    (r'^patents/', include('patents.urls')),
    (r'^permissions/', include('object_permissions.urls')),
    (r'^projects/', include('projects.urls')),
    (r'^publications/', include('publications.urls')),
    (r'^publishers/', include('publishers.urls')),
    (r'^reports/', include('reports.urls')),
    (r'^sentry/', include('sentry.urls')),
    (r'^statistics/', include('statistics.urls')),
    (r'^sqlreports/', include('sqlreports.urls')),
    (r'^workers/', include('workers.urls')),
    (r'^schools/', include('schools.urls')),
    (r'^smi/', include('smi.urls')),
    (r'^semantic/', include('semantic.urls')),
    (r'^achievements/', include('achievements.urls')),
    (r'^scireport/', include('scireport.urls')),
    (r'^equipment/', include('equipment.urls')),
    (r'^depadmin/', include('depadmin.urls')),
    (r'^export/', include('export_activities.urls')),
    (r'^events/', include('events.urls')),
    (r'^unified_permissions/', include('unified_permissions.urls')),
    (r'^pmodel/', include('pmodel.urls')),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^help/registration/problems/$', direct_to_template, {'template': 'help/registration_problems.html'}, name='help_registration_problems'),
    url(r'^help/feedback/$', login_required(direct_to_template), {'template': 'help/feedback.html'}, name='help_feedback'),
    # The following link does not show full list due to lack of organizations support; Full list should not be acceessed by non-MSU users
    url(r'^help/feedback/full/$', login_required(direct_to_template), {'template': 'help/feedback.html', 'extra_context': {'full': False}}, name='help_feedback_full'),
    (r'^authority/', include('authority.urls')),
    (r'^eduwork/', include('eduwork.urls')),
)

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += patterns('',
        # static files for development
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )

