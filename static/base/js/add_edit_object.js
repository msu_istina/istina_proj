$(function() {

    // remove submit on enter for worker autocomplete field
    $(".autocomplete_workers").keypress(function(e){
        if(e.which == 13) {
            e.preventDefault();
            return false
        }
        return true
    })
    // add autocomplete for workers and other fields
    $(".autocomplete_workers").autocomplete(generate_workers_autocomplete_options("/workers/search/"))
    $(".autocomplete_journal").autocomplete({source: "/journals/search/autocomplete/"})
    $(".autocomplete_rubric").autocomplete({source: "/journals/search/autocomplete_rubrics/"})
    $(".autocomplete_collection").autocomplete({source: "/collections/search/simple/"})
    $(".autocomplete_series").autocomplete({source: "/collections/series/search/"})
    $(".autocomplete_publisher").autocomplete({source: "/publishers/search/simple/"})
    $(".autocomplete_conference").autocomplete({source: "/conferences/search/"})
    $(".autocomplete_organization").autocomplete({source: "/organizations/search/"})
    $(".autocomplete_dissertation_council").autocomplete({source: "/dissertation_councils/search/"})
    $(".autocomplete_project").autocomplete({source: "/projects/search/"})
    $(".autocomplete_society").autocomplete({source: "/intrelations/societies/search/"})
    $(".autocomplete_honorary_organization").autocomplete({source: "/intrelations/honorary/organizations/search/"})
    $(".autocomplete_traineeship_organization").autocomplete({source: "/intrelations/traineeships/organizations/search/"})
    $(".autocomplete_award_organization").autocomplete({source: "/awards/organizations/search/"})

    // add datepicker for all date fields
    $(".datepicker").datepicker({ changeMonth: true, changeYear: true, yearRange: '1930:2020' });

    // hide hidden fields' labels in form
    // NB: it should be done before the next operation (transforming inputs into text)
    // otherwise the whole form will be hidden
    $(window).load( function () {
        $('#previous_forms :input').filter(":hidden").not("[class^=select2]").closest("div.field").hide()

        // copy all inputs in the previous forms into text and hide inputs
        $("#previous_forms").find("input, textarea").not('[type=checkbox]').not('[type=radio]').filter(":visible").each(function() {
            $(this).parent().append("<span>" + $(this).val() + "</span>")
            $(this).hide()
        });
        $("#previous_forms").find("input[type=checkbox]").filter(":visible").each(function() {
            if ($(this).is(':checked')) {
                value = 'Да'
            } else {
                value = 'Нет'
            }
            $(this).parent().append("<span>" + value + "</span>")
            $(this).hide()
        });
        $("#previous_forms").find("input[type=radio]").filter(":visible").filter(":checked").each(function() {
            var container = $(this).closest("div.field");
            container.append("<span>" + $(this).closest('label').text() + "</span>");
            container.find("ul").hide();
            container.removeClass("radio"); // so that special radio styles are not applied to this div anymore, it's now a simple text field, otherwise labels are displayed incorrectly
        });
        $("#previous_forms").find("select option:selected").each(function() {
            if (!$(this).find(".select2_result").length){
                $(this).parent().parent().append("<span>" + $(this).text() + "</span>")
            }
            $(this).parent().hide()
        });

        $("#previous_forms").find(".select2_result").each(function() {
            container = $(this).closest('.select2-container')
            container.parent().append("<span>" + $(this).text() + "</span>")
            container.hide()
        });

        //this is added to avoid duplication of label with result data for select2 drop-down lists
        //FIXME: avoid this behavior, move this logic to more appropriate function
        $("#previous_forms").find(".select2-chosen").each(function() {
            container = $(this).closest('.select2-container')
            container.hide()
        });
    })


    // if 'go to previous step' input button has been clicked, click the related button
    $("#go_prev_step").click(function() { // FIXME add event.preventDefault()?
        $("#go_prev_step_button").click()
    });

});

