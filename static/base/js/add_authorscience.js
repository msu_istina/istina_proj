function confirm_authorscience(element,csrf,authorscience_id, confirmflag){
    $.ajax({
      type: "POST",
      data: {"authorscience_id" : authorscience_id, "confirmflag" : confirmflag},
      url: "/common/confirm_authorscience/",
      headers: { 'X-CSRFToken': csrf },
      success: function(response) {
          if (response == "CONFIRMED"){
              $(element).parent().prev('a').removeClass("unvalidated");
              $(element).parent().hide();
          }
          else if (response == "DECLINED"){
              $(element).parent().parent().hide();
          }

      }
    });
};

function recall_authorscience(element,csrf,authorscience_id){
    $.ajax({
      type: "POST",
      data: {"authorscience_id" : authorscience_id},
      url: "/common/recall_authorscience/",
      headers: { 'X-CSRFToken': csrf },
      success: function(response) {
          if (response == "RECALLED"){
              $(element).parent().parent().hide();
          };
      }
    });
};
