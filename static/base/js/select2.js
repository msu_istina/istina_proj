function escape(text) {
    return text
}
function createSearchChoice(term) {
    return $('<option id="">Добавить новый@title_end@</option>')[0]
}
function decode_label(label) {
    return label.split('@title_end@')
}

function get_title(option_text, result) {
    label = option_text[0]
    span = '<span class="select2_option_title'
    if (result) {
        span += ' select2_result'
    }
    span += '">' + label + '</span>'
    return span
}

function get_description(option_text) {
    description = option_text[1]
    if (!description) {
        description = ''
    }
    div = '<div class="select2_option_description">' + description + '</div>'
    return div
}

function format_result(object, container) {
    option_text = decode_label(object.text)
    description = get_description(option_text)
    return get_title(option_text, false) + get_description(option_text)
}

function format_selected(object, container) {
    option_text = decode_label(object.text)
    return get_title(option_text, true)
}
