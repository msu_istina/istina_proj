/* Scripts for showing dialogs (replacement for alert and confirm).
 *
 * Author: Dmitry Shachnev <mitya57@gmail.com>, 2014.
 */

/* USAGE:
 * showDialog(text, callback) -> shows the dialog with buttons OK and Cancel,
 *                               pressing OK triggers callback.
 * showDialog(text) -> shows the dialog with single OK button (like alert()).
 */

function initializeDialogs() {
    var dialog = document.createElement('div');
    dialog.className = 'dialog';

    var label = document.createElement('p');
    label.className = 'label';
    dialog.appendChild(label);

    var customWidgetContainer = document.createElement('div');
    customWidgetContainer.className = 'custom_widget_container';
    dialog.appendChild(customWidgetContainer);

    var buttonBox = document.createElement('div');
    buttonBox.className = 'button_box';

    var cancelButton = document.createElement('button');
    cancelButton.className = 'cancel_button';
    cancelButton.textContent = 'Отмена';
    cancelButton.onclick = closeDialog;
    buttonBox.appendChild(cancelButton);

    var okButton = document.createElement('button');
    okButton.className = 'ok_button';
    okButton.textContent = 'OK';
    buttonBox.appendChild(okButton);

    dialog.appendChild(buttonBox);

    var dialogWrapper = document.createElement('div');
    dialogWrapper.className = 'dialog_wrapper';
    dialogWrapper.id = 'dialog_placeholder';
    dialogWrapper.style.display = 'none';
    dialogWrapper.appendChild(dialog);
    document.body.appendChild(dialogWrapper);
    return dialogWrapper;
}

function closeDialog() {
    var dialog = document.getElementById('dialog_placeholder');
    dialog.style.display = 'none';
}

function showDialog(text, callback, customWidget) {
    var dialog = document.getElementById('dialog_placeholder');
    if (!dialog) {
        dialog = initializeDialogs();
    }

    var label = dialog.getElementsByClassName('label')[0];
    label.textContent = text;

    var customWidgetContainer =
        dialog.getElementsByClassName('custom_widget_container')[0];
    customWidgetContainer.style.display = customWidget ? '' : 'none';
    if (customWidget) {
        clearNode(customWidgetContainer);
        customWidgetContainer.appendChild(customWidget);
    }

    var okButton = dialog.getElementsByClassName('ok_button')[0];
    okButton.onclick = function() {
        closeDialog();
        if (callback) {
            callback(customWidget);
        }
    };

    var cancelButton = dialog.getElementsByClassName('cancel_button')[0];
    cancelButton.style.display = callback ? '' : 'none';

    dialog.style.display = '';
}
