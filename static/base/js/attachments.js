$(function() {

    // basic blocks
    var container = $("#attachment_container")
    var links = $("#attachment_container_edit")
    var form = $("#attachment_form")
    var list = $("#attachments")
    var submit = form.find("input[type='submit']");

    // if form submit is not visible,
    // this means that edit controls are hidden,
    // then hide the formset for adding new files
    // the trick here is that it can be either #attachment_form.submit
    // or #additional_form.submit.
    if (!container.closest("form").find("input[type='submit']").is(":visible")) {
        container.find(".formset, .attachments_header").hide();
    }
    else {
        // show edit controls by default
        // used on add additional info to publication page
        container.find(".delete_all_attachments_initial, .delete_attachment, .formset").show();
        container.find(".show_attachment_edit").hide();
    }

    // show controls for add and delete attachments
    $(".show_attachment_edit").click(function(ev) {
        container.find(".delete_all_attachments_initial, .delete_attachment, .formset, .attachments_header").toggle();
        submit.toggle();
        if (!form.find(".attachment:visible").length) {
            container.find(".delete_all_attachments_initial").hide();
        }
    });

    // add more forms to attachment formset
    container.find('.formset_add_form').click(function() { // FIXME add event.preventDefault()?
        formset_add_form('#attachment_container div.form_in_formset:last', 'form');
    });

    // delete all attachments
    $("a.delete_all_attachments_initial").click(function(event) {
        event.preventDefault()
        $(this).hide()
        $(".delete_all_attachments_question").show()
    });
    $("a.delete_all_attachments_question").click(function(event) {
        event.preventDefault()
        if ($(this).hasClass("confirm")) {
            $(this).addClass("ui-autocomplete-loading-padded");
            $.ajax({
                url: container.data("delete-all-url"),
                dataType: "json",
                context: $(this),
                success: function() {
                    $(".delete_all_attachments_question").hide();
                    list.hide() // hide attachments table
                    if (!form.is(":visible")) {
                        if (links_outside_container) { // if the attachment form is hidden, and there are links for adding attachments outside the attachment container, hide the container
                            container.hide()
                        }
                        else {
                            form.show()
                        }
                    }
                },
                error: function() {
                    $(this).removeClass("ui-autocomplete-loading-padded")
                    links.find(".ajax_error").show()
                }
            });
        }
        else { // cancel
            $(".delete_all_attachments_question").hide()
            $(".delete_all_attachments_initial").show()
            links.find(".ajax_error").hide()
        }
    });

    // delete an attachment
    $(".delete_attachment a").click(function(event) {
        event.preventDefault()
        var attachment = $(this).closest('.attachment');
        var url = attachment.data('delete-url');
        var loading = attachment.find(".loading");
        loading.show().addClass("ui-autocomplete-loading-padded").find(".space").hide();
        $.ajax({
            url: url,
            dataType: "json",
            context: $(this),
            success: function() {
                // update numbers of succeeding attachments
                var current_number = parseInt(attachment.find(".number").html())
                attachment.siblings().filter(function(index) {
                    var number = parseInt($(this).find(".number").html())
                    return number > current_number;
                }).find(".number").each(function(index) {
                    var number = parseInt($(this).html())
                    $(this).html(number - 1 + ".")
                });
                // remove deleted attachment
                attachment.remove()
                if (!container.find(".attachment:visible").length) { // if there are no more attachments
                    container.find(".delete_all_attachments_initial").hide();
                    list.hide() // hide attachments table
                    if (!form.is(":visible")) {
                        if (links_outside_container) { // if the attachment form is hidden, and there are links for adding attachments outside the attachment container, hide the container
                            container.hide()
                        }
                        else {
                            form.show()
                        }
                    }
                }
            },
            error: function() {
                loading.removeClass("ui-autocomplete-loading-padded").hide().find(".space").show();
                attachment.find(".ajax_error").show();
            }
        });
    });

});
