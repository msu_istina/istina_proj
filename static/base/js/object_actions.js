$(function() {
    // show confirmation and link to object
    $("a.link_to_object_initial").click(function(event) {
        event.preventDefault()
        $(this).hide()
        $(".link_to_object_question").show()
    });
    $("a.link_to_object_question").click(function(event) { // FIXME add event.preventDefault() outside 'if'?
        if ($(this).hasClass("cancel")) {
            event.preventDefault()
            $(".link_to_object_question").hide()
            $(".link_to_object_initial").show()
        }
    });
});

$(function() {
    // show confirmation and unauthor object
    $("a.unauthor_object_initial").click(function(event) {
        event.preventDefault()
        $(this).hide()
        $(".unauthor_object_question").show()
    });
    $("a.unauthor_object_question").click(function(event) { // FIXME add event.preventDefault() outside 'if'?
        if ($(this).hasClass("cancel")) {
            event.preventDefault()
            $(".unauthor_object_question").hide()
            $(".unauthor_object_initial").show()
        }
    });
});
