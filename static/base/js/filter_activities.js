$(function() {

    $("#filter_activities").bind('keyup', function() {
        var query = $(this).val().trim();
        var sections = $("li").has(".activity_label");
        var activity_containers = $("ul.activity").parent();
        if (query.length) {
            // sections with matching text in section title. show all activities in them
            var matching_sections = sections.has(".activity_label:icontains('" + query + "')");
            matching_sections.show().find("ul.activity").parent().show();
            var matching_activity_containers = $("ul.activity").has("li:icontains('" + query + "')").parent();
            var sections_with_matching_activities = sections.has(matching_activity_containers);
            sections_with_matching_activities.show();
            sections.not(matching_sections).not(sections_with_matching_activities).hide();
            matching_activity_containers.show();
            sections.not(matching_sections).find("ul.activity").parent().not(matching_activity_containers).hide();
        }
        else {
            sections.show();
            activity_containers.show();
        }
    });

});
