// ####################################3
// jQuery ui autocomplete
function trim(s){return (s || '').replace(/^\s+|\s+$/g, '');}
function split(val) {return val.split( /,\s*/ );}
function extractLast(term) {return trim(split( term ).pop());}
function generate_workers_autocomplete_options(source){
    return {
        minLength: 0,
        source: function(request, response){
            $.getJSON(source, {
                term: extractLast(request.term)
            }, response);
        },
        search: function(){
            // custom minLength
            var term = extractLast(this.value);
            if (term.length < 2) {
                return false;
            }
        },
        focus: function(){
            return false
        },
        select: function(event, ui){
            var terms = split(this.value);
            terms.pop();
            terms.push(trim(ui.item.value));
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    }
}
