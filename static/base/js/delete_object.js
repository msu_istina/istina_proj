$(function() {
    // show confirmation and delete object
    $("a.delete_object_initial").click(function(event) {
        event.preventDefault()
        $(this).hide()
        $(this).parent().find(".delete_object_question").show()
    });
    $("a.delete_object_question").click(function(event) { // FIXME add event.preventDefault() outside 'if'?
        if ($(this).hasClass("cancel")) {
            event.preventDefault()
            $(this).parent().find(".delete_object_question").hide()
            $(this).parent().find(".delete_object_initial").show()
        }
    });
});
