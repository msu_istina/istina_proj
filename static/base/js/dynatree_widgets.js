var dynatree_widgets = dynatree_widgets || {};

dynatree_widgets.collapseAll = function(tree_divname){
    $(tree_divname).dynatree("getRoot").visit(function(node){
        node.expand(false);
    });
    return false;
}

dynatree_widgets.showChecked = function(tree_divname){
    $(tree_divname).dynatree("getRoot").visit(function(node){
        if(node.isSelected()) {
            node.makeVisible();
        }
    });
    return false;
}
