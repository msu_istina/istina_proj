function select_main_on_load( nRow, aData, iDisplayIndex ) {
    var id = $("#id_main_candidate").val()
    if (aData.DT_RowId == id ) {
        $(nRow).addClass('main_selected');
    }
}

function delete_row() {
       var MergeCandidatesTable = $('#tblMergeCandidates').dataTable();
       var row = $(this).closest("tr")
       id = row.attr("id")
       MergeCandidatesTable.fnDeleteRow(row[0])
       $("#id_candidate_" + id).remove()
       ShowOrHideSubmit()
};

function select_main(row) {
    var MergeCandidatesTable = $('#tblMergeCandidates').dataTable();
    MergeCandidatesTable.$('tr.main_selected').toggleClass('main_selected')
    $(row).toggleClass('main_selected')
    var main = $(row).attr('id')
    $("#id_main_candidate").attr('value', main)
    ShowOrHideSubmit()
}
function select_main_event(event){
    if( $(event.target).is(".delete") ) return;
    select_main(this)

}
function ShowOrHideSubmit() {
    var MergeCandidatesTable = $('#tblMergeCandidates').dataTable();
    var selected_num = MergeCandidatesTable._('tr').length
    var main = $(".main_selected").length
    if (selected_num > 1 && main) {
        $('#id_merge_submit, #id_merge_accept').show()

    } else {
        $('#id_merge_submit, #id_merge_accept').hide()
    }
}

function create_delete_button(nTd, sData, oData, iRow, iCol) {
    var input = $("<input type='submit'>")
    input.attr("title", "Убрать из списка объединяемых").attr("value","Убрать").addClass("delete")
    $(nTd).append(input)
}

function search_journals_by_ids( aoData ) {
    var ids = []
    $(".candidate").each(function(){ ids.push($(this).val()) })
    aoData.push( { "name": "ids", "value": ids } );
};

function add_missing_instance_title(nTd, sData, oData, iRow, iCol) {
    $(nTd).find('.instance_missing').attr('title', 'Этот объект был удален из системы');
}

$(function() {
    $('#id_merge_submit').hide()
})