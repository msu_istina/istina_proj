$(function() {
    $(' .formset_add_form').click(function() { // FIXME add event.preventDefault()?
        var div_to_clone = $(this).closest('div.formset').find('div.form_in_formset:last')
        var type = $(this).closest('div.formset').find('input:first').attr('name').split('-')[0]
        formset_add_form(div_to_clone, type);
        $(".datepicker").removeClass('hasDatepicker').unbind().datepicker({
            changeMonth: true, changeYear: true, yearRange: '1930:2020' });
    }    ); // rebind datepicker in case cloned form has date field
})          