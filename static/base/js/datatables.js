function reset_counters ( oSettings ) {
    /* Need to redo the counters if filtered or sorted */
    if ( oSettings.bSorted || oSettings.bFiltered ) {
        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ) {
            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
        }
    }
};           

function get_datatables(selectors, extra_options) {    
    var default_options = {
        "iDisplayLength": 50,
        "oLanguage": {
            "sLengthMenu": "Показывать по _MENU_ строк на странице",
            "sZeroRecords": "Нет записей, удовлетворяющих условиям поиска",
            "sInfo": "Показаны записи с _START_ по _END_. Всего: _TOTAL_",
            "sInfoEmpty": "Нет записей",
            "sInfoFiltered": "(отобрано по условию из _MAX_ записей)",
            "oPaginate": {
               "sNext": "  Следующая страница",
               "sPrevious": "Предыдущая страница  "
            },
            "sSearch": "Поиск (по всем полям):",
        },
        "bJQueryUI": true,
        "fnDrawCallback": reset_counters,
        "aaSorting": [[1,'asc']],
        "aoColumnDefs": [
            {"sType": "numeric-comma", "aTargets": ["number-float"] },
            {"asSorting": [ "desc" ], "aTargets": ["number", "number-float", "percentage"] },
            { "bSortable": false, "sClass": "index", "aTargets": [ 0 ] }
        ]        
    }
    var options = $.extend(true, default_options, extra_options);
    return $(selectors).dataTable(options);
};

function get_datatables_simple(selectors, extra_options) {
    var simple_options = {
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false
    };
    var options = $.extend(true, simple_options, extra_options);
    return get_datatables(selectors, options)
};

