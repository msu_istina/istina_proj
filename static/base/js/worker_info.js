function check_workers(mark_all) {
    // check that all fields are filled and mark bad worker-instance with error class
    // if mark_all=1 then unfilled fields (and only them) will be marked with error class
    // else this function will only remove error classes that were fixed
    //
    // return true if all ok and false otherwise
    var all_checked = true
    $("TABLE.worker-list .multi-way").parents("TR").each(function(){
        if (!$("input[type=radio]:checked", this).length){
            if (mark_all) {
                $(this).addClass("error")
            }
            all_checked = false
        }
        else {
            $(this).removeClass("error")
        }
    })
    if (all_checked) {
        $("TABLE.worker-list tr.workers_not_chosen").hide()
    }
    else if (mark_all) {
        $("TABLE.worker-list tr.workers_not_chosen").show()
    }
    return all_checked
};

function has_unknown_item(container){
    // check if container contains 'unknown-authors' options
    // in the most cases container is a td with .worker-instace's
    return $(".one-way-unknown, .multi-way-unknown", container).length
}

$(function() {
    $(".worker-list .show_works").each(function(){
        $(this).data("dialog",
            $(".works", $(this).parent()).dialog({
                autoOpen: false,
                draggable: true,
                modal: true,
                title: "Информация о сотруднике",
                width: "990px" // 950px (span-24) + 20px left margin + 20px right margin (div.works, add_work.css)
            })
        )
    }).click(function(){ 
       var dialog = $(this).data("dialog")
       var fast = $(".fast", dialog)
       dialog.dialog("open")
       if (fast.length == 1){
           fast.removeClass("fast")
           $.get(fast.metadata()["url"], function(data){
               fast.html(data)
           })
       }
       return false
    })

    $("TABLE.worker-list .one-way .show_works").each(function(){
        if ($(this).hasClass("preserve_worker")) {
            return false;
        }
        var wi_parent_row = $($(this).parents("TR")[0])
        var wi_parent = $($(this).parents("TD")[0])
        var wi_found = $(this).parents(".worker-instance")
        var wi_new = $(".one-way-new", wi_parent)
        var wi_unknown = $(".one-way-unknown", wi_parent)
    
        deselect_all = function(wi_parent, wi_found, wi_new, wi_unknown){
                $("input[type=radio]", wi_parent)
                .add("input[type=radio]", wi_found)
                .attr("checked", false)
                $('.show_works', wi_found).data("dialog").dialog("close")

                $(".selected", wi_parent).removeClass("selected")
                $(".button-select-new, .button-select-existed, .button-select-unknown", $(this).data("dialog")).show()
        }
        select_existed = function(wi_parent, wi_found, wi_new, wi_unknown){
                return function(){
                        deselect_all(wi_parent, wi_found, wi_new)
                        $("input[type=radio]", wi_found).attr("checked", true)
                        $(".worker-found", wi_found).addClass("selected")
                        $(".button-select-existed", $(this).data("dialog")).hide()
                        check_workers()
                        return false
                }
        }(wi_parent, wi_found, wi_new, wi_unknown)
        select_new = function(wi_parent, wi_found, wi_new, wi_unknown){
                return function(){
                        var sure = confirm("Вы уверены, что данного сотрудника нет среди найденных?")
                        if (!sure) return false;
                        deselect_all(wi_parent, wi_found, wi_new)
                        $("input[type=radio]", wi_new).attr("checked", true)
                        $(".one-way-new .worker-new", wi_parent).addClass("selected")
                        $(".button-select-new", $(this).data("dialog")).hide()
                        $(".button-select-unknown", $(this).data("dialog")).hide()
                        check_workers()
                        return false
                }
        }(wi_parent, wi_found, wi_new, wi_unknown)
        select_unknown = function(wi_parent, wi_found, wi_new, wi_unknown){
                return function(){
                        deselect_all(wi_parent, wi_found, wi_new)
                        $("input[type=radio]", wi_unknown).attr("checked", true)
                        $(".one-way-unknown .worker-unknown", wi_parent).addClass("selected")
                        $(".button-select-new", $(this).data("dialog")).hide()
                        $(".button-select-unknown", $(this).data("dialog")).hide()
                        check_workers()
                        return false
                }
        }(wi_parent, wi_found, wi_new, wi_unknown)
        a1 = $("<a>")
            .attr("class", "inner")
            .attr("title", "Не выбирать этого сотрудника, а создать нового")
            .text("создать нового сотрудника")
            .addClass("button-select-new")
            .click(select_new)
        a2 = $("<a>")
            .attr("class", "inner")
            .attr("title", "Не выбирать этого сотрудника и никакого другого")
            .addClass("button-select-unknown")
            .text("не выбирать данного сотрудника")
            .click(select_unknown)
        a3 = $("<a>")
            .attr("class", "inner")
            .attr("title", "Выбрать данного сотрудника")
            .addClass("button-select-existed")
            .text("выбрать данного сотрудника")
            .click(select_existed)
        if (!has_unknown_item(wi_parent)){
            a1.appendTo(  $("h3:first", $(this).data("dialog"))  )
        } else {
            a2.appendTo(  $("h3:first", $(this).data("dialog"))  )
            $(".one-way-unknown A.worker-unknown", wi_parent).click(select_unknown)
        }
        a3.appendTo(  $("h3:first", $(this).data("dialog"))  )
        $(".one-way-new A.worker-new", wi_parent).click(select_new)
        a3.hide()
    })

    $("TABLE.worker-list .multi-way .show_works").each(function(){
        var wi = $(this).parents(".worker-instance")
        var wi_parent = $($(this).parents("TD")[0])
        var wi_parent_row = $($(this).parents("TR")[0])
        $("<a>")
            .attr("class", "inner disambigination-button")
            .attr("title", "Выбрать данного сотрудника")
            .text("выбрать данного сотрудника")
            .click(function(wi, wi_parent){
                return function(){
                    $("input[type=radio]", wi_parent).attr("checked", false)
                    $("input[type=radio]", wi).attr("checked", true)
                    $(".selected", wi_parent).removeClass("selected")
                    $(".worker-found", wi).addClass("selected")
                    $(".show_works", wi_parent).each(function(){
                        $(".disambigination-button", $(this).data("dialog")).show()
                    })
                    $(".disambigination-button", $(".show_works", wi).data("dialog")).hide()
                    $('.show_works', wi).data("dialog").dialog("close")
                    check_workers()
                    return false;
                }
            }(wi, wi_parent))
            .appendTo(  $("h3:first", $(this).data("dialog"))  )
    })
    $("TABLE.worker-list .multi-way .worker-new")
    .add("TABLE.worker-list .multi-way .worker-unknown")
    .click(function(){
        if ($(this).hasClass("worker-new")){
            var sure = confirm("Вы уверены, что данного сотрудника нет среди найденных?")
            if (!sure) return false;
        }
        var wi = $(this).parents(".worker-instance")
        var wi_parent = $($(this).parents("TD")[0])
        var wi_parent_row = $($(this).parents("TR")[0])
        if ($("input[type=radio]", wi).attr("checked")) return false
        $(".selected", wi_parent).removeClass("selected")
        $(this).addClass("selected")
        $("input[type=radio]", wi_parent).attr("checked", false)
        $("input[type=radio]", wi).attr("checked", true)
        $(".show_works", wi_parent).each(function(){
            $(".disambigination-button", $(this).data("dialog")).show()
        })
        check_workers()
        return false
    })

    $("input[type=submit]", $("TABLE.worker-list").parents("FORM")).click(function(){
        if ($(this).attr("name") == "goback") return true
        return check_workers(true)
    })
});
