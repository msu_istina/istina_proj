function isset(val) {
    if (typeof(val) != "undefined" && typeof(val) != "null" )
        return true
    return false
}

$(function() {
    $(".message_hide").click(function(event) {
        event.preventDefault()
        $(this).closest(".message").hide()
        if (!$(".messages .message:visible").length)
            $(".messages").hide()
    });
    $("#message_hide_all").click(function(event) {
        event.preventDefault()
        $(".messages").hide()
    });

    $(".show_feedback_form").click(function(event) {
        event.preventDefault()
        $(".show_feedback_form, #feedback_form").toggle()
    });

    jQuery.expr[':'].icontains = function(a, i, m) {
      return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

});
