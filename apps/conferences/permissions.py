# -*- coding: utf-8; -*-
__author__ = 'goldan'
import authority
from common.permissions import MyModelPermission
from unified_permissions import has_permission
from conferences.models import Conference, ConferenceOrganizer


class ConferencePermission(MyModelPermission):
    label = 'conference_permission'
    operations_over_superuser = tuple(
        MyModelPermission.operations_over_superuser +
        ('delete',)
    )

    def check_delete(self, conference):
        return super(ConferencePermission, self).check_delete(conference) \
            and all(isinstance(obj, ConferenceOrganizer) for obj in conference.related_objects)

authority.register(Conference, ConferencePermission)
