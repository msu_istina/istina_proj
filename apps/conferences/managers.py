# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from actstream import action
from common.managers import SimilarFieldManager
from common.models import merge_two_objects
from common.utils.dict import filter_true
from common.utils.uniqify import uniqify
import datetime
from common.utils.idifflib import get_close_matches

stopwords = [u'конф', u'международн', u'internationa', u'conferenc', u'научн', u'проблем', u'чтени', u'практическ', u'всерос', u'школ', u'семина', u'развити', u'symposiu', u'наук', u'учены', u'congres', u'симпозиу', u'технологи', u'перспектив', u'секци', u'europea', u'worksho', u'студенто', u'конгрес', u'исследовани', u'актуальн', u'meetin', u'аспиранто', u'совещани', u'фору', u'участие', u'посвящ', u'russi', u'новы', u'scienc', u'съез', u'societ', u'всесоюзн', u'практик', u'annua', u'регионал', u'интер']

class ConferenceManager(SimilarFieldManager):
    '''Model manager for Conference model.'''

    def get_similar(self, name, startdate, enddate=None, **kwargs):
        if not enddate: # search by one date
            enddate = startdate
        qs = self.filter(Q(year__isnull=True) | Q(year=startdate.year))
        # filter out kwargs with False value
        kwargs = filter_true(kwargs)
        if kwargs:
            qs = qs.filter(**kwargs)
        def get_dates_kwargs(error_days):
            error = datetime.timedelta(days=error_days)
            return {
                'startdate__gte': startdate - error,
                'startdate__lte': startdate + error,
                'enddate__gte': enddate - error,
                'enddate__lte': enddate + error,
            }
        # first, we take all conferences, regardless names, in span extended by two weeks
        conferences = list(qs.filter(**get_dates_kwargs(14)))

        # second, we take conferences similar by name, in two months extended span
        qs_2months = qs.filter(**get_dates_kwargs(60))
        conferences.extend(self.get_similar_by_field('name', name, cutoff=0.4, n=10, qs=qs_2months, stopwords=stopwords))
        conferences.extend(self.get_similar_by_field('aliases__name', name, cutoff=0.4, n=10, qs=qs_2months, stopwords=stopwords))

        # finally, we take conferences similar by name, regardless of the dates,
        # because the dates are often not set.
        # though we set higher cutoff
        conferences.extend(self.get_similar_by_field('name', name, cutoff=0.7, n=10, qs=qs, stopwords=stopwords))
        conferences.extend(self.get_similar_by_field('aliases__name', name, cutoff=0.7, n=10, qs=qs, stopwords=stopwords))

        res = [c[0] for c in get_close_matches(name, uniqify(conferences, idfun=lambda c: c.id), n=25, cutoff=0.2, key=lambda c:c.name, stopwords=stopwords)]
        return res

    def get_exact(self, name, dates):
        kwargs = {
            'startdate__lte': dates[0],
            'enddate__gte': dates[1] if len(dates) > 1 and dates[1] else dates[0]
        } if dates and dates[0] else {}
        return self.get(Q(name__iexact=name) | Q(aliases__name__iexact=name), **kwargs)

    def merge(self, username, from_ids, to_id):
        '''Merge conference with from_id to conference with to_id.'''

        # user exists
        user = User.objects.get(username=username)
        if not user.is_superuser:
            raise Exception("user must be superuser")

        #conference exists
        c_to = self.get(id=to_id)

        if isinstance(from_ids, int):
            from_ids = (from_ids,)

        for from_id in from_ids:

            if from_id == to_id:
                raise Exception("conferences must differ")
            # conference  exists
            try:
                c_from = self.get(id=from_id)
            except models.ObjectDoesNotExist:
                # skip missing conferences
                continue

            # move related objects
            c_from.committee_memberships.all().update(conference=c_to) # committee memberships
            c_from.presentations.all().update(conference=c_to) # conference presentations
            c_from.aliases.all().update(conference=c_to) # conference aliases

            # delete duplicated organizers of c_from conference
            organizers_deps_qs = c_to.organizers.filter(organization__isnull=True, department__isnull=False).values('department')
            organizers_orgs_qs = c_to.organizers.filter(organization__isnull=False, department__isnull=True).values('organization')
            c_from.organizers.filter(Q(department__in=organizers_deps_qs) | Q(organization__in=organizers_orgs_qs)).delete()
            # copy all the remaining organizers
            c_from.organizers.filter(department__in=organizers_deps_qs).update(conference=c_to)

            # Conference fields
            # if some fields of c_to are empty, move them from c_from
            for field in ["startdate", "enddate", "year", "location", "country", "category", "scope",
                'participants_count', 'participants_foreign_count', 'participants_organization_count', 'speakers_count',
                'url', "description", "xml"]:
                if getattr(c_from, field) and not getattr(c_to, field):
                    setattr(c_to, field, getattr(c_from, field))
            # update counts
            for field in ["participants_count", "participants_foreign_count", "participants_organization_count", "speakers_count"]:
                if getattr(c_from, field) and getattr(c_to, field):
                    try:
                        if int(getattr(c_from, field)) > int(getattr(c_to, field)):
                            setattr(c_to, field, getattr(c_from, field))
                    except ValueError:
                        pass
            # create alias
            from .models import ConfAlias
            alias = ConfAlias.objects.create(conference=c_to, name=c_from.name)
            alias.save()
            # log action
            description = u"Удаленная конференция: %s (id %d). Её данные: \n%s" % (c_from.name, c_from.id, c_from.get_xml().decode('utf8'))
            action.send(user, verb=u'объединил конференции', action_object=c_from, target=c_to, description=description)

            merge_two_objects(object_from=c_from, object_to=c_to)
            # delete c_from
            c_from.delete()

        c_to.save()


    def update_year(self):
        print "Before: %d conferences do not have year" % self.filter(year__isnull=True).count()
        for conference in self.filter(year__isnull=True):
            conference.save() # executes custom save method
        print "After: %d conferences do not have year" % self.filter(year__isnull=True).count()

class FakeQueryset(object):
    def __init__(self, objects, model):
        self.objects = objects
        self.model = model

    def all(self):
        return self.objects

    def count(self):
        return len(self.objects)

    def get(self, **kwargs):
        if 'pk' in kwargs:
            kwargs['id'] = kwargs['pk']
            del kwargs['pk']
        if 'id' in kwargs: kwargs['id'] = int(kwargs['id'])
        return [x for x in self.objects if all(getattr(x, k) == v for k, v in kwargs.iteritems())][0]
