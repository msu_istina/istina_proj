from django.conf.urls.defaults import *
from conferences.models import Conference, ConferenceCommitteeMembership, ConferencePresentation
from conferences.views import ConferenceCommitteeMembershipWizardView, ConferencePresentationWizardView, ConferenceWizardView


# conferences
conference_options_base = {'model': Conference}
conference_options_detail = dict(conference_options_base.items() + [('template_name', 'conferences/conference_detail.html')])

urlpatterns = patterns('common.views',
    url(r'^add/$', ConferenceWizardView.as_view(), name='conferences_conference_add'),
    url(r'^(?P<object_id>\d+)/$', 'detail', conference_options_detail, name='conferences_conference_detail'),
    url(r'^(?P<object_id>\d+)/edit/$', ConferenceWizardView.as_view(), name='conferences_conference_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', conference_options_base, name='conferences_conference_delete'),
    url(r'^search/$', "autocomplete_search", conference_options_base, name="conferences_search"),
)

# committee memberships of conferences
committee_membership_options_base = {'model': ConferenceCommitteeMembership}
committee_membership_options_detail = dict(committee_membership_options_base.items() + [('template_name', 'conferences/committee_membership_detail.html')])

urlpatterns += patterns('common.views',
    url(r'^committee_memberships/add/$', ConferenceCommitteeMembershipWizardView.as_view(), name='conferences_committee_membership_add'),
    url(r'^committee_memberships/(?P<object_id>\d+)/$', 'detail', committee_membership_options_detail, name='conferences_committee_membership_detail'),
    url(r'^committee_memberships/(?P<object_id>\d+)/edit/$', ConferenceCommitteeMembershipWizardView.as_view(), name='conferences_committee_membership_edit'),
    url(r'^committee_memberships/(?P<object_id>\d+)/delete/$', 'delete', committee_membership_options_base, name='conferences_committee_membership_delete'),
)

# presentations at conferences
presentation_options_base = {'model': ConferencePresentation}
presentation_options_detail = dict(presentation_options_base.items() + [('template_name', 'conferences/presentation_detail.html')])

urlpatterns += patterns('common.views',
    url(r'^presentations/add/$', ConferencePresentationWizardView.as_view(), name='conferences_presentation_add'),
    url(r'^presentations/(?P<object_id>\d+)/$', 'detail', presentation_options_detail, name='conferences_presentation_detail'),
    url(r'^presentations/(?P<object_id>\d+)/edit/$', ConferencePresentationWizardView.as_view(), name='conferences_presentation_edit'),
    url(r'^presentations/(?P<object_id>\d+)/delete/$', 'delete', presentation_options_base, name='conferences_presentation_delete'),
)
