# -*- coding: utf-8; -*-
from django import forms
from django.forms.models import inlineformset_factory
from common.forms import WorkershipModelForm, MyForm, MyModelForm, LinkedToWorkersModelForm, MyBaseModelFormSet, CountryChoiceField, ModelChoiceLinkField
from organizations.forms import DepartmentChoiceField
from organizations.models import Organization
from conferences.models import Conference, ConferenceCommitteeMembership, ConferencePresentation, ConferenceOrganizer


class ConferencePresentationForm(LinkedToWorkersModelForm):
    '''A simple form for conference presentations.'''
    conference_str = forms.CharField(label=u"Название конференции", max_length=255)
    fields_order = ["authors_str", "title", "conference_str", "talk_kind", "date", "abstract"]
    css_classes = [('conference_str', 'wide autocomplete_conference')]

    class Meta:
        model = ConferencePresentation
        exclude = ('authors', 'conference', 'xml', 'creator')

    def __init__(self, *args, **kwargs):
        super(ConferencePresentationForm, self).__init__(*args, **kwargs)
        if self.instance_linked(): # initialize conference_str field
            self.fields['conference_str'].initial = self.instance.conference.name

    def save(self, request, workers, conference, commit=True):
        # conference must exist now
        presentation = super(ConferencePresentationForm, self).save(request, commit=False)
        presentation.conference = conference
        if commit:
            self.commit_save(request, presentation, workers)
        return presentation


class ConferenceCommitteeMembershipForm(WorkershipModelForm, ConferencePresentationForm):
    '''A simple form for conference committee membership.'''
    startdate = forms.DateField(label=u"Дата начала конференции")
    enddate = forms.DateField(label=u"Дата окончания конференции", help_text=u"если не помните, укажите примерные даты")
    fields_order = ["member_str", "conference_str", "startdate", "enddate", "type"]

    class Meta:
        model = ConferenceCommitteeMembership
        exclude = ('member', 'conference', 'creator')

    def __init__(self, *args, **kwargs):
        super(ConferenceCommitteeMembershipForm, self).__init__(*args, **kwargs)
        if self.instance_linked():
            self.fields['startdate'].initial = self.instance.conference.startdate
            self.fields['enddate'].initial = self.instance.conference.enddate

    def clean_member_str(self):
        # only one member is allowed
        value = self.cleaned_data['member_str'].strip(", ")
        if "," in value:
            raise forms.ValidationError(u"Пожалуйста, укажите ФИО только одного члена программного комитета. В строке не должно быть запятых.")
        return value

    def clean(self):
        if self.is_valid():
            startdate = self.cleaned_data.get("startdate")
            enddate = self.cleaned_data.get("enddate")
            if startdate > enddate:
                raise forms.ValidationError(u"Дата начала конференции должна предшествовать дате окончания.")
        return self.cleaned_data

    def save(self, request, workers, conference, commit=True):
        # conference must exist now
        committee_membership = super(ConferencePresentationForm, self).save(request, commit=False)
        committee_membership.conference = conference
        if commit:
            self.commit_save(request, committee_membership, workers)
            to_save = False
            for attribute in ["startdate", "enddate"]:
                if not getattr(conference, attribute):
                    setattr(conference, attribute, self.cleaned_data.get(attribute))
                    to_save = True
            if to_save:
                conference.save()
        return committee_membership

def conference_link_text(obj):
    presentations_count = obj.presentations.count()
    if presentations_count == 0:
        return u'(нет докладов)'
    elif presentations_count % 10 == 1:
        return u'(%s доклад)' % presentations_count
    elif presentations_count % 10 >= 5 or presentations_count % 10 == 0:
        return u'(%s докладов)' % presentations_count
    else:
        return u'(%s доклада)' % presentations_count

class SimilarConferencesForm(MyForm):
    conference = ModelChoiceLinkField(label=u"Конференция", queryset=Conference.objects.none(), empty_label="добавить новую конференцию", required=False, widget=forms.RadioSelect, link_text=conference_link_text)
    css_container_classes = [('conference', 'radio')]

    class Media:
        js = ("conferences/js/similar_conferences.js",)

class ConferenceForm(MyModelForm):
    country = CountryChoiceField(label=u"Страна проведения")
    fields_order = ['category', 'scope', 'country', 'location', 'url',
        'participants_count', 'participants_foreign_count', 'participants_organization_count', 'speakers_count',
        'description']
    css_classes = [('location', 'wide'), ('participants_count', 'narrow'), ('participants_foreign_count', 'narrow'),
        ('participants_organization_count', 'narrow'), ('speakers_count', 'narrow')]

    class Meta:
        model = Conference
        exclude = ('name', 'startdate', 'enddate', 'year', 'committee_members', 'xml', 'creator')

    def clean(self):
        if self.is_valid():
            startdate = self.cleaned_data.get("startdate")
            enddate = self.cleaned_data.get("enddate")
            if startdate > enddate:
                raise forms.ValidationError(u"Дата начала конференции должна предшествовать дате окончания.")
            participants_count = self.cleaned_data.get("participants_count")
            if participants_count:
                for field in ["participants_foreign_count", "participants_organization_count", "speakers_count"]:
                    value = self.cleaned_data.get(field)
                    if value and value > participants_count:
                        raise forms.ValidationError(u"%s не может превышать общего числа участников." % self.fields[field].label)
        return self.cleaned_data

    def save(self, request, name, startdate, enddate, commit=True):
        conference = super(ConferenceForm, self).save(request, commit=False)
        conference.name = name
        conference.startdate = startdate
        conference.enddate = enddate
        if commit:
            self.commit_save(request, conference)
        return conference


class ConferenceWithDatesForm(ConferenceForm):
    fields_order = ['startdate', 'enddate', 'category', 'scope', 'country', 'location', 'url',
        'participants_count', 'participants_foreign_count', 'participants_organization_count', 'speakers_count',
        'description']

    class Meta:
        model = Conference
        exclude = ('name', 'year', 'committee_members', 'xml', 'creator')

    def save(self, request, name, commit=True):
        conference = super(ConferenceForm, self).save(request, commit=False)
        conference.name = name
        if commit:
            self.commit_save(request, conference)
        return conference


class ConferenceFullForm(ConferenceForm):
    fields_order = ['name', 'startdate', 'enddate', 'category', 'scope', 'country', 'location', 'url',
        'participants_count', 'participants_foreign_count', 'participants_organization_count', 'speakers_count',
        'description']

    class Meta:
        model = Conference
        exclude = ('year', 'committee_members', 'xml', 'creator')

    def save(self, request, commit=True):
        # skip ConferenceForm.save
        return super(ConferenceForm, self).save(request, commit)


class ConferenceOrganizerForm(MyModelForm):
    department = DepartmentChoiceField(label=u"Подразделение", required=False, help_text=u"необязательно")

    fields_order = ['organization', 'department', 'organization_name']
    css_classes = [('organization_name', 'wide'), ('department', 'wide')]
    disable_organization_preselection = True

    class Meta:
        model = ConferenceOrganizer
        exclude = ('conference', 'ordername')

    class Media:
        js = ("conferences/js/conference_organizers.js",)

    def __init__(self, *args, **kwargs):
        super(ConferenceOrganizerForm, self).__init__(*args, **kwargs)
        self.fields['organization'].empty_label = u"Другая организация"

    def save(self, commit=True):
        return super(ConferenceOrganizerForm, self).save(self.request, commit)


ConferenceOrganizerFormSet = inlineformset_factory(Conference, ConferenceOrganizer, formset=MyBaseModelFormSet, form=ConferenceOrganizerForm, extra=5, can_delete=True)
