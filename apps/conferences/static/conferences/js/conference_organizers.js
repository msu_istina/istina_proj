$(function() {
    var formset = $("#main_form_div .formset");
    if (formset && formset.find("input[name$='organizers-0-id']").length) {

        function toggle_fields() {
            // in visible forms, if organization is None (select has not value), then hide department field
            var visible_forms = formset.find(".form_in_formset").filter(':visible');
            var other_org_forms = visible_forms.find('select[name$="-organization"] option[value=""]').filter(':selected').closest('.form');
            other_org_forms.find('input[name$="-department"]').select2("val", "");
            other_org_forms.find('input[name$="-department"]').closest('.field').hide();
            other_org_forms.find('input[name$="-organization_name"]').closest('.field').show();
            // in visible forms, if organization is from db (has value), then hide organization_name field
            var db_org_forms = visible_forms.find('select[name$="-organization"] option[value!=""]').filter(':selected').closest('.form');
            db_org_forms.find('input[name$="-organization_name"]').val('');
            db_org_forms.find('input[name$="-organization_name"]').closest('.field').hide();
            db_org_forms.find('input[name$="-department"]').closest('.field').show();
        }

        toggle_fields();

        // initially hide empty forms, except the first, if this is an add mode
        formset.find(".form_in_formset").filter(':visible').each(function(index) {
            // this is not the first form
            // and organization is empty_label (other organization)
            // and organization name is not specified
            if (index > 0 &&
                $(this).find('select[name$="-organization"] option[value=""]').filter(':selected').length &&
                !$(this).find('input[name$="-organization_name"]').val().length)
            {
                $(this).hide();
            }
        });

        $('select[name$="-organization"]').change(function(ev) {
            toggle_fields();
        });

        $(".formset_add_form").unbind('click');
        $(".formset_add_form").click(function(ev) {
            ev.preventDefault();
            formset.find(".form_in_formset").filter(':hidden').eq(0).show();
            if (!formset.find(".form_in_formset").filter(':hidden').length) {
                $(this).hide();
            }
        });

    }

});