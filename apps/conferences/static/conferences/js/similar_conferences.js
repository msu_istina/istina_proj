$(function() {

    $("#main_form").validate({
        rules: {
            'similar_conferences-conference': {
                required: true
            }
        },
        messages: {
            'similar_conferences-conference': "Выберите существующую конференцию или добавьте новую."
        },
        ignore: '.ignore',
        errorElement: "div",
        errorClass: "error",
        errorPlacement: function(error, element){
            error.prepend('<span class="ui-icon ui-icon-alert"></span>');
            element.closest('div.field').append(error);
        }
    })

});