# -*- coding: utf-8; -*-
import logging

from common.views import LinkedToWorkersModelWizardView, MyModelWizardView
from workers.forms import WorkerFormSet
from conferences.models import Conference, ConferenceCommitteeMembership, ConferencePresentation
from conferences.forms import ConferenceForm, ConferenceCommitteeMembershipForm, SimilarConferencesForm, ConferencePresentationForm, ConferenceOrganizerFormSet, ConferenceFullForm, ConferenceWithDatesForm
from conferences.managers import FakeQueryset

logger = logging.getLogger("conferences.views")

class ConferencePresentationWizardView(LinkedToWorkersModelWizardView):
    form_list = [
        (LinkedToWorkersModelWizardView.main_step, ConferencePresentationForm),
        ('similar_conferences', SimilarConferencesForm),
        ('new_conference', ConferenceWithDatesForm),
        ('new_conference_organizers', ConferenceOrganizerFormSet),
        (LinkedToWorkersModelWizardView.workers_steps[0], WorkerFormSet)
    ]
    condition_dict = LinkedToWorkersModelWizardView.make_condition_dict(
        'similar_conferences', 'new_conference', 'new_conference_organizers')
    model = ConferencePresentation  # used in templates
    steps_info = {
        'similar_conferences': {'title': u'Выберите существующую конференцию или добавьте новую'},
        'new_conference': {'title': u'Информация о конференции'},
        'new_conference_organizers': {'title': u'Информация об организаторах конференции'},
    }

    def get_form_initial(self, step):
        """
        Returns a dictionary which will be passed to the form for `step`
        as `initial`. If no initial data was provied while initializing the
        form wizard, a empty dictionary will be returned.
        """
        initial = super(ConferencePresentationWizardView, self).get_form_initial(step)
        if step == 'new_conference':
            initial['name'] = self.get_extra_data('conference_str')
        return initial

    def get_stored_dates(self):
        return self.get_extra_data('date'), None

    def set_stored_dates(self, dates):
        self.set_extra_data('date', dates[0])

    def get_form_dates(self, form):
        return form.cleaned_data['date'], None

    def get_form(self, step=None, data=None, files=None):
        form = super(ConferencePresentationWizardView, self).get_form(step, data, files)
        if not step:
            step = self.steps.current
        if step == 'similar_conferences':
            similar_conferences, conference_str = self.get_extra_data('similar_conferences', 'conference_str')
            dates = self.get_stored_dates()
            if not similar_conferences:
                if self.steps.current == step and conference_str: # this is really step 2 and conference_str is stored
                # FIXME if conference_str is None, it seems that there is a problem with smth (it shouldn't be None, because it is set on step 1 and stored in session)
                    similar_conferences = Conference.objects.get_similar(conference_str, startdate=dates[0], enddate=dates[1])
                    # store similar conferences not to calculate them twice (on step 2 and on step 3).
                    self.set_extra_data('similar_conferences', similar_conferences.all())
                else: # this is just a previous form or conference_str is empty
                    similar_conferences = []
            form.fields['conference'].queryset = FakeQueryset(similar_conferences, Conference)
            if similar_conferences:
                form.fields['conference'].initial = -1 # so that all radio buttons are deselected by default
            form.fields['conference'].empty_label = (conference_str or "") + u" (добавить новую конференцию)"
        return form

    def get_context_data(self, form, **kwargs):
        context = super(ConferencePresentationWizardView, self).get_context_data(form, **kwargs)
        forms = context['previous_forms']
        self.hide_form_fields(forms, 'main', ('conference_str',), 'similar_conferences')
        self.hide_form_fields(forms, 'main', ('authors_str', 'title', 'date', 'abstract'),
            ('similar_conferences', 'new_conference', 'new_conference_organizers'))
        self.hide_form_fields(forms, 'new_conference',
            ('startdate', 'enddate', 'category', 'scope', 'participants_count', 'participants_foreign_count', 'participants_organization_count', 'speakers_count', 'url', 'description'),
            ('new_conference_organizers', self.workers_steps[0]))
        self.hide_form_fields(forms, 'new_conference', ('country', 'location'), self.workers_steps[0])
        self.hide_form_fields(forms, 'similar_conferences', 'conference', ('new_conference', 'new_conference_organizers'))
        self.hide_form_fields(forms, 'main', ('abstract',), self.workers_steps[0])
        if 'similar_conferences' in self.steps.all:
            self.hide_form_fields(forms, 'main', ('conference_str',), self.workers_steps[0])
        return context

    def process_step(self, form):
        conference = None
        if self.steps.current == self.main_step:
            conference_str = form.cleaned_data['conference_str']
            dates = self.get_form_dates(form)
            old_dates = self.get_stored_dates()
            old_conference_str = self.get_extra_data('conference_str')
            if conference_str != old_conference_str or dates != old_dates: # some parameters have changed
                self.set_extra_data('conference_str', conference_str) # for later use in creating similar conferences form
                self.set_stored_dates(dates)
                self.delete_extra_data('similar_conferences') # now they should be calculated again
                # try to select conference by name and dates
                try:
                    conference = Conference.objects.get_exact(conference_str, dates)
                except:
                    # calculate similar conferences
                    similar_conferences = Conference.objects.get_similar(conference_str, startdate=dates[0], enddate=dates[1])
                    # store similar conferences not to calculate them twice (on step 2 and on step 3).
                    self.set_extra_data('similar_conferences', similar_conferences)
                    self.skip_or_include_steps('similar_conferences', not similar_conferences)
                    if not similar_conferences:
                        self.include_steps('new_conference', 'new_conference_organizers')
                else: # conference selected by name, go to the last step
                    self.skip_steps('similar_conferences', 'new_conference', 'new_conference_organizers')
        elif self.steps.current == 'similar_conferences':
            conference = form.cleaned_data['conference']
            self.skip_or_include_steps(('new_conference', isinstance(conference, Conference)), ('new_conference_organizers', isinstance(conference, Conference)))
        if isinstance(conference, Conference): # conference found by name or similar conference selected
            self.set_extra_data('conference_instance', conference)

        return super(ConferencePresentationWizardView, self).process_step(form)

    def save_new_conference_form(self, form, name):
        return form.save(self.request, name) # save a new conference

    def save_object(self, form_list):
        '''This function is executed during done() method, and used to save main form object.
        Workers objects has been already saved.
        '''
        if 'new_conference' in self.steps.all: # new conference has been added
            new_conference_form = form_list[self.get_step_index('new_conference')]
            name = self.get_extra_data('conference_str')
            conference = self.save_new_conference_form(new_conference_form, name)
            new_conference_organizers_formset = form_list[self.get_step_index('new_conference_organizers')]
            new_conference_organizers_formset.save(self.request, conference)
        else: # conference could be found by name or selected among similar conferences
            conference = self.get_extra_data('conference_instance', Conference) # this is a conference found by name or a selected similar conference
        return super(ConferencePresentationWizardView, self).save_object(form_list, conference)



class ConferenceCommitteeMembershipWizardView(ConferencePresentationWizardView):
    form_list = [
        (ConferencePresentationWizardView.main_step, ConferenceCommitteeMembershipForm),
        ('similar_conferences', SimilarConferencesForm),
        ('new_conference', ConferenceForm),
        ('new_conference_organizers', ConferenceOrganizerFormSet),
        (ConferencePresentationWizardView.workers_steps[0], WorkerFormSet)
    ]
    condition_dict = ConferencePresentationWizardView.make_condition_dict(
        'similar_conferences', 'new_conference', 'new_conference_organizers')
    model = ConferenceCommitteeMembership  # used in templates

    def get_form_initial(self, step):
        """
        Returns a dictionary which will be passed to the form for `step`
        as `initial`. If no initial data was provied while initializing the
        form wizard, a empty dictionary will be returned.
        """
        initial = super(ConferenceCommitteeMembershipWizardView, self).get_form_initial(step)
        if step == 'new_conference':
            initial['startdate'], initial['enddate'] = self.get_stored_dates()
        return initial

    def get_stored_dates(self):
        return self.get_extra_data('startdate', 'enddate')

    def set_stored_dates(self, dates):
        self.set_extra_data(('startdate', dates[0]), ('enddate', dates[1]))

    def get_form_dates(self, form):
        return form.cleaned_data['startdate'], form.cleaned_data['enddate']

    def get_context_data(self, form, **kwargs):
        context = super(ConferenceCommitteeMembershipWizardView, self).get_context_data(form, **kwargs)
        forms = context['previous_forms']
        self.hide_form_fields(forms, 'main', ('member_str', 'startdate', 'enddate', 'type'),
            ('similar_conferences', 'new_conference', 'new_conference_organizers'))
        self.hide_form_fields(forms, 'main', ('startdate', 'enddate'), self.workers_steps[0])
        return context

    def save_new_conference_form(self, form, name):
        startdate, enddate = self.get_stored_dates()
        return form.save(self.request, name, startdate, enddate) # save a new conference


class ConferenceWizardView(MyModelWizardView):
    form_list = [
        (MyModelWizardView.main_step, ConferenceFullForm),
        ('organizers', ConferenceOrganizerFormSet),
    ]
    model = Conference  # used in templates
    steps_info = {
        'organizers': {'title': u'Информация об организаторах конференции'},
    }

    def get_form_kwargs(self, step=None):
        """
        Returns the keyword arguments for instantiating the form
        (or formset) on given step.
        """
        kwargs = super(ConferenceWizardView, self).get_form_kwargs(step)
        if self.edit and step in ['organizers']:
            kwargs.update({
                'instance': self.instance_dict[self.main_step],
            })
        return kwargs

    def get_context_data(self, form, **kwargs):
        context = super(ConferenceWizardView, self).get_context_data(form, **kwargs)
        forms = context['previous_forms']
        self.hide_form_fields(forms, 'main',
            ('category', 'scope', 'participants_count', 'participants_foreign_count', 'participants_organization_count', 'speakers_count', 'url', 'description'),
            'organizers')
        return context

    def done(self, form_list, **kwargs):
        response = super(ConferenceWizardView, self).done(form_list, **kwargs)
        conference = form_list[self.get_step_index(self.main_step)].instance
        organizers_formset = form_list[self.get_step_index('organizers')]
        organizers_formset.save(self.request, conference)
        return response
