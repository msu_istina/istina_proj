# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
import logging

from common.models import LinkedToWorkersModel, WorkershipModel, \
    AuthorshipModel, AuthoredModel, Activity, MyModel
from common.utils.validators import validate_year
from common.utils.dates import dates_text as _dates_text
from managers import *
from workers.models import Worker
from unified_permissions import has_permission

logger = logging.getLogger("conferences.models")


class Conference(LinkedToWorkersModel):
    id = models.AutoField(primary_key=True, db_column="CONF_ID")
    name = models.CharField(u"Название конференции", max_length=255, db_column="NAME")
    startdate = models.DateField(u"Дата начала", db_column="CONF_START", null=True)
    enddate = models.DateField(u"Дата окончания", db_column="CONF_END", null=True)
    year = models.IntegerField(u"Год проведения", db_column="YEAR", null=True, blank=True, validators=[validate_year])
    location = models.CharField(u"Место проведения", max_length=1000, db_column="CONF_PLACE", blank=True)
    country = models.ForeignKey("common.Country", verbose_name=u"Страна проведения", db_column="F_COUNTRY_ID", null=True)
    category = models.ForeignKey("ConferenceCategory", related_name="conferences", verbose_name=u"тип", db_column="F_CONFKIND_ID", null=True)
    scope = models.ForeignKey("ConferenceScope", related_name="conferences", verbose_name=u"охват", db_column="F_CONFSCOPE_ID", null=True)
    participants_count = models.PositiveSmallIntegerField(u"примерное число участников", help_text=u"необязательно", db_column="CONF_COUNTMAN", blank=True, null=True)
    participants_foreign_count = models.PositiveSmallIntegerField(u"число иностранных участников", help_text=u"для российских конференций", db_column="CONF_COUNTFMAN", blank=True, null=True)
    participants_organization_count = models.PositiveSmallIntegerField(u"число участников из МГУ", db_column="CONF_COUNTSELFMAN", blank=True, null=True)
    speakers_count = models.PositiveSmallIntegerField(u"число докладчиков", db_column="CONF_COUNTDOCL", blank=True, null=True)
    url = models.URLField(u"Ссылка на веб-сайт", max_length=1000, db_column="CONF_URL", blank=True)
    description = models.TextField(u"Описание конференции", db_column="CONF_ABSTRACT", blank=True)
    xml = models.TextField(u"XML конференции", db_column="CONF_XML", blank=True)
    creator = models.ForeignKey(to=User, related_name="conferences_added", db_column="CONF_USER", null=True, blank=True)
    committee_members = models.ManyToManyField("workers.Worker", related_name="conferences_in_committee", through="ConferenceCommitteeMembership")


    objects = ConferenceManager()

    search_attr = "name"
    workers_attr = 'committee_members'
    workerships_attr = 'committee_memberships'
    workers_verbose_name_single = u"Член программного комитета"
    workers_verbose_name_plural = u"Члены программного комитета"
    workers_required_error_msg = u"Укажите членов программного комитета"

    nominative_en = "conference"
    genitive = u"конференции"
    accusative = u"конференцию"
    accusative_short = u"конференцию"
    instrumental = u"конференцией"
    locative = u"конференции"
    gender = "feminine"

    class Meta:
        db_table = "T_CONFS"
        verbose_name = u"конференция"
        verbose_name_plural = u"конференции"
        ordering = ('-startdate', 'name')

    def __unicode__(self):
        return u"%s (%s)" % (self.name, ", ".join(filter(bool, (self.get_location, self.dates_text))))

    @models.permalink
    def get_absolute_url(self):
        return ('conferences_conference_detail', (), {'object_id': self.id})

    def save(self, *args, **kwargs):
        # store the year because it is used in ordering and personal reports
        if self.startdate:
            self.year = self.startdate.year
        elif self.enddate:
            self.year = self.enddate.year
        super(Conference, self).save(*args, **kwargs)

    @property
    def chairs(self):
        worker_ids = self.committee_memberships.filter(type=2).values_list('member', flat=True)
        return Worker.objects.filter(id__in=worker_ids)

    @property
    def chairs_list(self):
        workers = []
        chairs = self.committee_memberships.filter(type=2).values_list('member', 'original_name')
        for member, name in chairs:
            worker = {'worker': Worker.objects.get(pk=member) if member else None}
            if name:
                worker['name'] = name
            elif member:
                worker['name'] = member.fullname_short
            else:
                worker['name'] = ''
            workers.append(worker)
        return workers

    @property
    def ordinary_members(self):
        worker_ids = self.committee_memberships.filter(type=1).values_list('member', flat=True)
        return Worker.objects.filter(id__in=worker_ids)

    @property
    def ordinary_members_list(self):
        workers = []
        members = self.committee_memberships.filter(type=1).values_list('member', 'original_name')
        for member, name in members:
            worker = {'worker': Worker.objects.get(pk=member) if member else None}
            if name:
                worker['name'] = name
            elif member:
                worker['name'] = member.fullname_short
            else:
                worker['name'] = ''
            workers.append(worker)
        return workers

    def datatable_merge_json(self):
        data = dict(enumerate([self.get_absolute_url_html(),
                               self.name,
                               self.year,
                               self.location,
                               self.presentations.count(),
                               self.committee_members.count()]))
        data['DT_RowId'] = self.id
        return data

    @staticmethod
    def get_datatable_merge_column_names():
        return ("ID",
                u"Название",
                u"Год",
                u"Место проведения",
                u"Кол-во докладов",
                u"Кол-во членов комитета")

    @property
    def title(self):
        return self.name

    @property
    def dates_text(self):
        return _dates_text(self.startdate, self.enddate, self.year)

    @property
    def get_year(self):
        return self.startdate.year if self.startdate else self.year

    @property
    def get_location(self):
        return u", ".join(map(unicode, filter(bool, [self.location, self.country])))

    @property
    def get_category(self):
        return self.category or u"конференция"


class ConferenceCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_CONFKIND_ID")
    name = models.CharField(u"название", max_length=255, db_column="F_CONFKIND_NAME")

    class Meta:
        db_table = "CONFKIND"
        verbose_name = u"тип конференции"
        verbose_name_plural = u"типы конференций"

    def __unicode__(self):
        return self.name


class ConferenceScope(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_CONFSCOPE_ID")
    name = models.CharField(u"название", max_length=255, db_column="F_CONFSCOPE_NAME")

    class Meta:
        db_table = "CONFSCOPE"
        verbose_name = u"охват конференции"
        verbose_name_plural = u"охваты конференций"

    def __unicode__(self):
        return self.name


class ConfAlias(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_CONFALIAS_ID")
    conference = models.ForeignKey(to="Conference", related_name="aliases", db_column="CONF_ID")
    name = models.CharField(u"название", max_length=255, db_column="F_CONFALIAS_NAME")

    class Meta:
        db_table = "CONFALIAS"
        verbose_name = u"альтернативное название конференции"
        verbose_name_plural = u"альтернативные названия конференций"

    def __unicode__(self):
        return self.name


class ConferenceOrganizer(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_CONFORG_ID")
    conference = models.ForeignKey(to="Conference", related_name="organizers", db_column="CONF_ID")
    organization = models.ForeignKey("organizations.Organization", related_name="conference_organizers", verbose_name=u"организатор", db_column="F_ORGANIZATION_ID", blank=True, null=True)
    organization_name = models.CharField(u"название организации", max_length=1000, db_column="F_CONFORG_ORGNAME", blank=True)
    department = models.ForeignKey("organizations.Department", related_name="conference_organizers", verbose_name=u"подразделение-организатор", db_column="F_DEPARTMENT_ID", blank=True, null=True)
    ordername = models.CharField(u"приказ о проведении (номер, дата)", max_length=1000, help_text=u"необязательно", db_column="F_CONFORG_ORDERNAME", blank=True)

    class Meta:
        db_table = "CONFORG"
        verbose_name = u"организатор конференции"
        verbose_name_plural = u"организаторы конференций"

    def __unicode__(self):
        return u", ".join(map(unicode, filter(bool, (self.organization, self.organization_name, self.department, self.conference))))


class ConferenceCommitteeMembership(WorkershipModel, Activity):
    id = models.AutoField(primary_key=True, db_column="F_COMMITTEE_ID")
    member = models.ForeignKey(to="workers.Worker", related_name="conference_committee_memberships", db_column="F_MAN_ID", null=True, blank=True)
    conference = models.ForeignKey(to="Conference", related_name="committee_memberships", db_column="CONF_ID")
    type = models.IntegerField(u"Тип участия", max_length=2, choices=(
        (1, u"Член программного комитета"),
        (2, u"Председатель программного комитета"),
        (3, u"Член организационного комитета"),
        (4, u"Председатель организационного комитета"),
    ), default=1, db_column="F_COMMITTEE_TYPE")
    original_name = models.CharField(max_length=255, db_column="F_COMMITTEE_NAME", blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="conference_committee_memberships_added", db_column="F_COMMITTEE_USER", null=True, blank=True)

    worker_attr = "member"
    workers_verbose_name_single = u"ФИО члена программного комитета"
    workers_required_error_msg = u"Укажите члена программного комитета"

    nominative_en = "conference_committee_membership"
    nominative_short = u"членство в программном комитете"
    nominative_plural = u"членство в программных комитетах"
    genitive = u"членства в программном комитете"
    genitive_short = u"членства"
    genitive_plural = u"членства в программных комитетах"
    genitive_plural_full = u"членств в программных комитетах"
    accusative_short = u"членство в программном комитете"
    instrumental = u"членством в программном комитете"
    locative = u"членстве в программном комитете"
    gender = "neuter"

    class Meta:
        db_table = "COMMITTEE"
        verbose_name = u"членство в программном комитете"
        verbose_name_plural = u"членства в программном комитете"
        ordering = ("-conference__year", "-conference__startdate")

    def __unicode__(self):
        return u"Членство в программном комитете конференции {0}, {1}".format(
            unicode(self.conference), self.workers_string)

    @models.permalink
    def get_absolute_url(self):
        return ('conferences_committee_membership_detail', (), {'object_id': self.id})

    @property
    def title(self):
        return u"%s в программном комитете конференции %s" % (self.workers_string, self.conference)


TALK_KIND_CHOICES = (
    ("poster", u"Стендовый"),
    ("oral", u"Устный"),
    ("plenar", u"Пленарный"),
    ("invited", u"Приглашенный"),
    )

class ConferencePresentation(AuthoredModel, Activity):
    id = models.AutoField(primary_key=True, db_column="F_PRESENTATION_ID")
    title = models.CharField(u"Название доклада", max_length=255, db_column="F_PRESENTATION_NAME")
    talk_kind = models.CharField(u"Тип доклада", max_length=50, choices=TALK_KIND_CHOICES, default="oral", db_column="F_PRESENTATION_KIND")
    authors = models.ManyToManyField("workers.Worker", related_name="conference_presentations", through="ConferencePresentationAuthorship")    
    conference = models.ForeignKey(to="Conference", related_name="presentations", db_column="CONF_ID")
    date = models.DateField(u"дата доклада", help_text=u"если не помните, укажите примерную", db_column="F_PRESENTATION_DATE", null=True)
    abstract = models.TextField(u"Аннотация доклада", db_column="F_PRESENTATION_ABSTRACT", blank=True)
    xml = models.TextField(u"XML доклада", db_column="F_PRESENTATION_XML", blank=True)
    creator = models.ForeignKey(to=User, related_name="conference_presentations_added", db_column="F_PRESENTATION_USER", null=True, blank=True)

    nominative_en = "conference_presentation"
    nominative_plural = u"доклад на конференциях"
    genitive = u"доклада на конференции"
    genitive_plural = u"доклада на конференциях"
    genitive_plural_full = u"докладов на конференциях"
    instrumental = u"докладом на конференции"
    locative = u"докладе на конференции"

    class Meta:
        db_table = "PRESENTATION"
        verbose_name = u"доклад на конференции"
        verbose_name_plural = u"доклады на конференциях"
        ordering = ("-conference__year", "-conference__startdate", 'title')

    def __unicode__(self):
        return self.title

    @property
    def year(self):
        return self.conference.get_year

    def talk_kind_name(self):
        for code, name in TALK_KIND_CHOICES:
            if code == self.talk_kind:
                return name
        return u"Не указан"

    @property
    def speaker(self):
        for speaker in self.authorships.filter(role_name='speaker'):
            return speaker.author
        return None

    @models.permalink
    def get_absolute_url(self):
        return ('conferences_presentation_detail', (), {'object_id': self.id})


class ConferencePresentationAuthorship(AuthorshipModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORD_ID")
    author = models.ForeignKey(to="workers.Worker", related_name="conference_presentation_authorships", db_column="F_MAN_ID", null=True, blank=True)
    conference_presentation = models.ForeignKey(to="ConferencePresentation", related_name="authorships", db_column="F_PRESENTATION_ID")
    creator = models.ForeignKey(to="auth.User", related_name="conference_presentation_authorships_added", db_column="F_AUTHORD_USER", null=True, blank=True)
    original_name = models.CharField(max_length=255, db_column="F_AUTHORD_NAME", blank=True)
    role_name = models.CharField(max_length=50, db_column="F_AUTHORD_KIND", blank=True)

    class Meta:
        db_table = "AUTHORD"
        verbose_name = u"авторство доклада на конференции"
        verbose_name_plural = u"авторства докладов на конференциях"

    def __unicode__(self):
        return u"%s, автор доклада '%s'" % (self.workers_string_full, self.conference_presentation.title)

from conferences.admin import *
