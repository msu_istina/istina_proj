# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from views import AddWizard, FORMS, AddWizView, AddSchoolWizView

urlpatterns = patterns('schools.views',

    # public pages
    url(r'^$', 'index', name='schools_index'),
    url(r'^schools/$', 'indexschools', name='schools_index_schools'),
    url(r'^edit/((?P<school_id>\d+)/)?$', 'edit', name="school_edit"),
    url(r'^print/(?P<school_id>\d+)/$', 'print_school', name="school_print"),
    url(r'^sign/(?P<school_id>\d+)/$', 'sign_school', name="school_sign"),
    url(r'^search/member/$', 'autocomplete_search_member', name="school_autocomplete_member"),
    url(r'^search/pub/$', 'autocomplete_search_pub', name="school_autocomplete_pub"),
    url(r'^get_rub/$', 'get_rub', name="school_get_rub"),
    url(r'^all/$', 'list_all', name="school_list_all"),
    url(r'^schools/all/$', 'list_all_schools', name="school_list_all_schools"),
    url(r'^all/grants/$', 'list_all_grants', name="school_list_all_grants"),
    url(r'^addwiz/$', AddWizView),
    url(r'^schools/add/$', AddSchoolWizView)
)
