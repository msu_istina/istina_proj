from common.utils.admin import register_with_versions
from models import School, JournalRub, SchoolRubLink, SchoolMemberRoles, SchoolMembers, SchoolPub, SchoolGrant, SchoolTechno, SchoolPatent, SchoolRegnum

register_with_versions(School)
register_with_versions(JournalRub)
register_with_versions(SchoolRubLink)
register_with_versions(SchoolMemberRoles)
register_with_versions(SchoolMembers)
register_with_versions(SchoolPub)
register_with_versions(SchoolGrant)
register_with_versions(SchoolTechno)
register_with_versions(SchoolPatent)
register_with_versions(SchoolRegnum)

