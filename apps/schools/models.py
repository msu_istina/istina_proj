# -*- coding: utf-8 -*-

from organizations.models import Department
from django.contrib.auth.models import User

from django.db import models, connection
from workers.models import Worker
from django.core.validators import MaxLengthValidator
from common.models import MyModel, Activity

class School(MyModel, Activity):
    id = models.AutoField(primary_key=True, db_column="F_SCHOOL_ID")
    maindep = models.ForeignKey(to=Department, related_name="schools", db_column="F_DEPARTMENT_ID", verbose_name=u"Ведущее подразделение", blank=True, null=True)
    creator = models.ForeignKey(to=User, related_name="schools_added", db_column="F_SCHOOL_USER", null=True, blank=True)
    name = models.CharField(u"Название", max_length=255, db_column="F_SCHOOL_NAME")
    nameeng = models.CharField(u"Название по-английски", max_length=255, db_column="F_SCHOOL_NAMEENG")
    keywords = models.CharField(u"Ключевые слова", max_length=1500, db_column="F_SCHOOL_KEYWORDS")
    keywordseng = models.CharField(u"Ключевые слова по-английски", max_length=1500, db_column="F_SCHOOL_KEYWORDSENG")
    descr = models.TextField(u"Описание", validators=[MaxLengthValidator(4200)], db_column="F_SCHOOL_DESCR")
    descreng = models.TextField(u"Описание по-английски", validators=[MaxLengthValidator(4200)], db_column="F_SCHOOL_DESCRENG")
    state = models.CharField(u"", max_length=255, db_column="F_SCHOOL_STATUS", blank=True, null=True)
    datesigned = models.DateField(u"", db_column="F_SCHOOL_DATESIGNED", blank=True, null=True)
    isschool = models.IntegerField(u"", db_column="F_SCHOOL_ISSCHOOL", blank=True, null=True, default=0)
    achievements = models.TextField(u"Основные достижения", validators=[MaxLengthValidator(4200)], db_column="F_SCHOOL_ACHIEVEMENTS", blank=True, null=True)

    genitive_plural_full = u"научных коллективов"
    instrumental = u"научным коллективом"
    accusative_short = u"научный коллектив"

    nominative_en = "school"
    nominative_short = u"научный коллектив"
    genitive = u"научного коллектива"
    genitive_short = u"коллектива"
    genitive_plural = u"научных коллективов"
    accusative_short = u"научный коллектив"
    instrumental = u"научным коллективом"
    locative = u"научном коллективе"
    gender = 'neuter'

    def state_name(self):
        names={'signed': u'подписана', 'approved': u'утверждена'}
        return names.get(self.state, 'не подписана')

    def leader_name(self):
        role = SchoolMemberRoles.objects.filter(name=u'Руководитель')
        members = self.members.filter(role=role)
        if members:
            leader = members[0]
            return leader.lastname + ' ' + leader.firstname + ' ' + leader.middlename
        return None

    def __unicode__(self):
        return self.name

    @property
    def head_member(self):
        head_memberships = SchoolMembers.objects.filter(school=self, role__name=u"Руководитель")
        if Worker.objects.filter(schoolmembers__in=head_memberships).exists():
            return Worker.objects.get(schoolmembers__in=head_memberships)
        else:
            return None

    @property
    def deputy_members(self):
        deputy_memberships = SchoolMembers.objects.filter(school=self,role__name=u"Заместитель руководителя")
        return Worker.objects.filter(schoolmembers__in=deputy_memberships).distinct()

    @property
    def common_members(self):
        common_memberships = SchoolMembers.objects.filter(school=self,role__name=u"Участник")
        return Worker.objects.filter(schoolmembers__in=common_memberships).distinct()

    class Meta:
        db_table = "SCHOOL"
        verbose_name = u"Научный коллектив"
        verbose_name_plural = u"Научные коллективы"

    @property
    def scopus_rubrics(self):
        return dict((o['rubric'],o['rubric__name']) for o in self.rubrics.filter(rubric__categorization__code='Scopus').values('rubric','rubric__name'))

    @property
    def grnti_rubrics(self):
        return dict((o['rubric'],o['rubric__name']) for o in self.rubrics.filter(rubric__categorization__code='GRNTI').values('rubric','rubric__name'))

    @property
    def specialties(self):
        return self.rubrics.filter(rubric__categorization__code='NOMENKL').values('rubric','rubric__code','rubric__name')


class JournalRub(MyModel):
    id = models.IntegerField(primary_key=True, db_column="F_JOURNALRUB_ID")
    name = models.CharField(max_length=255, db_column="F_JOURNALRUB_NAME")


RUBS=list()

def populate_scopus(type='Scopus', indent=True, node_id=None):
    from django.db import connection, transaction
    if indent:
        sql = """
        select f_journalrub_id, lpad('|---', 4*(level-1), '|---') || decode(level, 1, '', ' ') || substr(f_journalrub_name,1,80) AS f_journalrub_name
        """
    else:
        sql = """
        select f_journalrub_id, f_journalrub_name
        """

    sql += """
    from  journalrub
    where f_journalrubtype_id in (select f_journalrubtype_id from journalrubtype where f_journalrubtype_name=%s)
    start with JOU_F_JOURNALRUB_ID is NULL
    connect by JOU_F_JOURNALRUB_ID = prior F_JOURNALRUB_ID
    order siblings by journalrub.f_journalrub_code, journalrub.f_journalrub_name, journalrub.f_journalrub_id
    """

    cursor = connection.cursor()
    cursor.execute(sql, [type])

    return cursor.fetchall()


def populate_scopus_level(type='Scopus', node_id=None):
    from django.db import connection, transaction

    try:
        node_id = int(node_id)
    except:
        node_id = None

    sql = """
    select f_journalrub_id, f_journalrub_name
    from  journalrub
    where f_journalrubtype_id in (select f_journalrubtype_id from journalrubtype where f_journalrubtype_name=%s)
    """
    if node_id:
        sql += " and JOU_F_JOURNALRUB_ID = %s " % node_id
    else:
        sql += " and JOU_F_JOURNALRUB_ID is NULL "
    sql += """
    order by journalrub.f_journalrub_code, journalrub.f_journalrub_name, journalrub.f_journalrub_id
    """

    cursor = connection.cursor()
    cursor.execute(sql, [type])

    return cursor.fetchall()



GRNTIRUBS=list()
#for rub in JournalRub.objects.raw("""
#    select f_journalrub_id, lpad('--', 2*(level-1)) || decode(level, 1, '', ' ') || f_journalrub_name AS f_journalrub_name
#    from  journalrub
#    where f_journalrubtype_id in (select f_journalrubtype_id from journalrubtype where f_journalrubtype_name=%s)
#    start with JOU_F_JOURNALRUB_ID is NULL
#    connect by JOU_F_JOURNALRUB_ID = prior F_JOURNALRUB_ID
#    """, ['GRNTI']):
#    GRNTIRUBS.append( (rub.id, rub.name) )



def get_rub_choices(rubtype='Scopus'):
    rubs=list()
    for rub in JournalRub.objects.raw("""
        select f_journalrub_id, lpad('--', 2*(level-1)) || decode(level, 1, '', ' ') || f_journalrub_name AS f_journalrub_name
        from  journalrub
        where f_journalrubtype_id in (select f_journalrubtype_id from journalrubtype where f_journalrubtype_name=%s)
        start with JOU_F_JOURNALRUB_ID is NULL
        connect by JOU_F_JOURNALRUB_ID = prior F_JOURNALRUB_ID
        """, [rubtype]):
        rubs.append( (rub.id, rub.name) )
    rubs



class SchoolRubLink(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SCHOOLRUBLINK_ID")
    school = models.ForeignKey(to=School, related_name="rubrics", db_column="F_SCHOOL_ID", verbose_name=u"School")
    rubric = models.ForeignKey(to="journals.JournalRubric", db_column="F_JOURNALRUB_ID", blank=True, null=True)
    # rub2_id = models.IntegerField(u"Рубрика (ГРНТИ)", db_column="F_JOURNALRUB2_ID", blank=True, null=True)
    class Meta:
        db_table = "SCHOOLRUBLINK"


#class SchoolRubLinkGRNTI(MyModel):
#    id = models.AutoField(primary_key=True, db_column="F_SCHOOLRUBLINK_ID")
#    school = models.ForeignKey(to=School, related_name="rubrics", db_column="F_SCHOOL_ID", verbose_name=u"School")
#    rub_id = models.IntegerField(u"rub_id", max_length=255, choices=GRNTIRUBS, db_column="F_JOURNALRUB_ID")
#    class Meta:
#        db_table = "SCHOOLRUBLINK"


class SchoolMemberRoles(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SCHOOLMEMBERROLE_ID")
    name = models.CharField(u"Название", max_length=255, db_column="F_SCHOOLMEMBERROLE_NAME")

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = "SCHOOLMEMBERROLE"

DEGREES=(('BS', u'Без степени'), ('KN', u'Кандидат наук'), ('DN', u'Доктор наук'))
DEGREES_DICT={'BS': u'Без степени', 'KN': u'Кандидат наук', 'DN': u'Доктор наук'}


class SchoolMembers(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SCHOOLMEMBER_ID")
    school = models.ForeignKey(to=School, related_name="members", db_column="F_SCHOOL_ID", verbose_name=u"School")
    worker = models.ForeignKey(to=Worker, related_name="schoolmembers", db_column="F_MAN_ID", verbose_name=u"", blank=True, null=True)
    role =  models.ForeignKey(to=SchoolMemberRoles, db_column="F_SCHOOLMEMBERROLE_ID", verbose_name=u"Роль")
    lastname  =  models.CharField(u"Фамилия", max_length=128, db_column="F_SCHOOLMEMBER_LASTNAME")
    firstname  =  models.CharField(u"Имя", max_length=128, db_column="F_SCHOOLMEMBER_FIRSTNAME")
    middlename  =  models.CharField(u"Отчество", max_length=128, db_column="F_SCHOOLMEMBER_MIDDLENAME", blank=True, null=True)
    email  =  models.CharField(u"Электронная почта", max_length=128, db_column="F_SCHOOLMEMBER_EMAIL", blank=True, null=True)
    tel  =  models.CharField(u"Тел. служебный", max_length=128, db_column="F_SCHOOLMEMBER_TEL", blank=True, null=True)
    degree  =  models.CharField(u"Ученая степень", choices=DEGREES, max_length=128, db_column="F_SCHOOLMEMBER_DEG")
    rank  =  models.CharField(u"Ученое звание", max_length=128, db_column="F_SCHOOLMEMBER_RANK", blank=True, null=True)
    maindep = models.ForeignKey(to=Department, related_name="schoolmembers", db_column="F_DEPARTMENT_ID", verbose_name=u"Основное место работы", blank=True, null=True)
    post  =  models.CharField(u"Должность", max_length=128, db_column="F_SCHOOLMEMBER_POST", blank=True, null=True)

    def degree_name(self):
        if self.degree:
            return DEGREES_DICT.get(self.degree,'')
        return ""

    def __unicode__(self):
        return self.lastname

    class Meta:
        db_table = "SCHOOLMEMBER"
        verbose_name = u"Научный коллектив member"
        verbose_name_plural = u"Научные коллективы members"
        ordering=['role']


class SchoolPub(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SCHOOLPUBLICATION_ID")
    school = models.ForeignKey(to=School, related_name="pubs", db_column="F_SCHOOL_ID", verbose_name=u"School", blank=True, null=True)
    activity_id = models.IntegerField(u"", db_column="F_ACTIVITY_ID", blank=True, null=True)
    activity_type = models.CharField(u"", max_length=64, db_column="F_ACTIVITY_TYPE", blank=True, null=True)
    reference = models.CharField(u"Библиографическая ссылка", max_length=1000, db_column="F_SCHOOLPUBLICATION_REFERENCE")

    def __unicode__(self):
        return self.reference

    class Meta:
        db_table = "SCHOOLPUBLICATION"
        verbose_name = u"Публикация научного коллектива"
        verbose_name_plural = u"Публикация научного коллектива"


class SchoolGrant(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SCHOOLGRANT_ID")
    school = models.ForeignKey(to=School, related_name="grants", db_column="F_SCHOOL_ID", verbose_name=u"School", blank=True, null=True)

    name =  models.CharField(u"Название проекта или темы", max_length=255, db_column="F_SCHOOLGRANT_NAME")
    source =  models.CharField(u"Источник", max_length=255, db_column="F_SCHOOLGRANT_SOURCE")
    russian =  models.CharField(u"РФ?", max_length=1, choices=(('Y', u'Да'), ('N', u'Нет')), db_column="F_SCHOOLGRANT_RUSSIAN")
    number =  models.CharField(u"Номер", max_length=255, db_column="F_SCHOOLGRANT_NUMBER")
    budget =  models.FloatField(u"Объем финансирования (руб)", max_length=255, db_column="F_SCHOOLGRANT_BUDGET")
    leader =  models.CharField(u"Руководитель", max_length=255, db_column="F_SCHOOLGRANT_LEADER")
    start =  models.DateField(u"Начало", max_length=255, db_column="F_SCHOOLGRANT_START")
    end =  models.DateField(u"Окончание", max_length=255, db_column="F_SCHOOLGRANT_END", blank=True, null=True)
    citis =  models.CharField(u"Номер госрегистрации (ЦИТИС)", max_length=255, db_column="F_SCHOOLGRANT_GOSREGNUM", blank=True, null=True)
#    rntd =  models.CharField(u"Номер регистрации РНТД", max_length=255, db_column="F_SCHOOLGRANT_RNTD", blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = "SCHOOLGRANT"
        verbose_name = u"Грант (проект) научного коллектива"
        verbose_name_plural = u"Гранты (проекты) научного коллектива"


class SchoolTechno(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SCHOOLTECHNO_ID")
    school = models.ForeignKey(to=School, related_name="technos", db_column="F_SCHOOL_ID", verbose_name=u"School", blank=True, null=True)

    name =  models.CharField(u"Название", max_length=512, db_column="F_SCHOOLTECHNO_NAME")
    area =  models.CharField(u"Область применения", max_length=512, db_column="F_SCHOOLTECHNO_AREA")
    development =  models.CharField(u"Степень разработки", max_length=255, db_column="F_SCHOOLTECHNO_DEVELOPMENT")
    readyness =  models.CharField(u"Готовность к внедрению", max_length=255, db_column="F_SCHOOLTECHNO_READYNESS")
    authors =  models.CharField(u"Авторы", max_length=750, db_column="F_SCHOOLTECHNO_AUTHORS")
    comment =  models.CharField(u"Примечания", max_length=750, db_column="F_SCHOOLTECHNO_COMMENT", blank=True, null=True)

    class Meta:
        db_table = "SCHOOLTECHNO"
        verbose_name = u"Технология"
        verbose_name_plural = u"Технологии"


PAT_TYPES=((u'Патент', u'Патент'), (u'Международный патент',u'Международный патент'), (u'Регистрация ПО',u'Регистрация ПО'), (u'др.', u'др.'))

class SchoolPatent(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SCHOOLPATENT_ID")
    school = models.ForeignKey(to=School, related_name="patents", db_column="F_SCHOOL_ID", verbose_name=u"School", blank=True, null=True)
    name = models.CharField(u"Название", max_length=512, db_column="F_SCHOOLPATENT_NAME")
    type = models.CharField(u"Тип", max_length=512, choices=PAT_TYPES, db_column="F_SCHOOLPATENT_TYPE")
    date =  models.DateField(u"Дата выдачи", max_length=255, db_column="F_SCHOOLPATENT_DATE")
    issuedby =  models.CharField(u"Выдавшая организация", max_length=750, db_column="F_SCHOOLPATENT_ISSUEDBY")
    number = models.CharField(u"Номер", max_length=512, db_column="F_SCHOOLPATENT_NUMBER")
    authors =  models.CharField(u"Авторы", max_length=1000, db_column="F_SCHOOLPATENT_AUTHORS")
    commerce =  models.CharField(u"Коммерциализация", max_length=1500, db_column="F_SCHOOLPATENT_COMMERCE")

    class Meta:
        db_table = "SCHOOLPATENT"
        verbose_name = u"Патент"
        verbose_name_plural = u"Патенты"

REGNUM_TYPES = [(u'РНТД',u'РНТД'),(u'Научная школа',u'Научная школа')]

class SchoolRegnum(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SCHOOLREGNUM_ID")
    school = models.ForeignKey(to=School, related_name="regnums", db_column="F_SCHOOL_ID", verbose_name=u"School", blank=True, null=True)
    type = models.CharField(u"Тип регистрации", choices=REGNUM_TYPES ,max_length=512, db_column="F_SCHOOLREGNUM_TYPE", blank=True, null=True)
    value = models.CharField(u"Номер регистрации", max_length=512, db_column="F_SCHOOLREGNUM_VALUE")
    date = models.DateField(u"Дата регистрации", max_length=255, db_column="F_SCHOOLREGNUM_DATE")

    class Meta:
        db_table = "SCHOOLREGNUM"
        verbose_name = u"Регистрационный номер"
        verbose_name_plural = u"Регистрационные номера"


class SchoolAction(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SCHOOLACTION_ID")
    school = models.ForeignKey(to=School, related_name="actions", db_column="F_SCHOOL_ID", verbose_name=u"School")
    action = models.CharField(u"Действие", max_length=255, db_column="F_SCHOOLACTION_ACTION", null=True, blank=True)
    user = models.ForeignKey(to=User, related_name="schoolactions", db_column="F_SCHOOLACTION_USER")
    date =  models.DateField(u"Дата выполнения операции", db_column="F_SCHOOLACTION_DATE")

    class Meta:
        db_table = "SCHOOLACTION"
        verbose_name = u"Действие со школой"
        verbose_name_plural = u"Действия со школами"

from schools.admin import *