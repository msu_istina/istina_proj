# -*- coding: utf-8; -*-
from django import forms
from django.db import models
from django.db.models import Q
from django.forms import ModelForm

from workers.models import Worker
from models import School, SchoolRubLink, SchoolMembers, SchoolPub, SchoolGrant, SchoolTechno, JournalRub, SchoolPatent, populate_scopus, SchoolRegnum
from django.forms.formsets import formset_factory, BaseFormSet
from django.forms.models import modelformset_factory
from django.contrib.admin import widgets
from django.forms.models import BaseModelFormSet
from django.forms.widgets import Select, TextInput, Textarea, HiddenInput
from common.widgets import FilteredSelect
from common.forms import Select2ModelMultipleField, Select2WorkerField, MyBaseForm, Select2SingleWorkerField
from journals.models import JournalRubric
from journals.forms import JournalRubricMultipleChoiceField
from organizations.forms import DepartmentChoiceField

class SchoolForm(forms.ModelForm):
    leader = forms.CharField(max_length=100)
    class Meta:
        model = School


class NameForm(forms.ModelForm):
    leader = forms.CharField(max_length=100)
    class Meta:
        model = School
        fields = ('name',)

# hardcoded MSU ID
def populate_deptree(orgid=214524):
    from django.db import connection, transaction
    sql = """
    select  d.f_department_id, lpad('|---', 4*(level-1), '|---') || decode(level, 1, '', ' ') || substr(d.f_department_name,1,80) as f_department_name
    from
    (select f_department_id, dep_f_department_id from departmentlink
    union all
    select f_department_id, NULL from department d2 where not exists (select 1 from departmentlink dl2 where dl2.f_department_id=d2.f_department_id)
    ) dl,
    department d
    where  d.f_department_id = dl.f_department_id
    and d.f_organization_id = 214524
    start with dl.dep_f_department_id is null
    connect by PRIOR dl.f_department_id = dl.dep_f_department_id
    order siblings by d.f_department_name
    """

    cursor = connection.cursor()
    cursor.execute(sql)

    return cursor.fetchall()

# class ScientificTopicManager(models.Manager):
#     def get_query_set(self):
#         qs = super(ScientificTopicManager, self).get_query_set()
#         return qs.filter(Q(categorization__code='NOMENKL'), ~Q(code__icontains='00.00')).order_by('code')
#
# class ScientificTopic(JournalRubric):
#     objects = ScientificTopicManager()
#     def __unicode__(self):
#         return self.code + " " + self.name
#     class Meta:
#         proxy = True
#
class SpecialtySelect2Field(JournalRubricMultipleChoiceField):

    def __init__(self, *args, **kwargs):
        self.search_fields = ['name__icontains', 'code__startswith']
        super(SpecialtySelect2Field, self).__init__(self, *args, add_option=False, queryset=JournalRubric.objects.filter(categorization__code='NOMENKL'), **kwargs)
        self.widget.options['placeholder'] = u'Введите часть названия или кода специальности, например, 01.01'

    def get_title(self, obj):
        return obj.code + " --> " + obj.name


class InfoForm(forms.ModelForm):

    specialty = SpecialtySelect2Field(label=u"Специальность", widget_kwargs={'width': 608}, required=False)
    maindep = DepartmentChoiceField(label=u"Подразделение", widget_kwargs={'width': 608}, required=False)

    class Meta:
        model = School
        exclude = ('creator','state','datesigned')

    def __init__(self, *args, **kwargs):
        super(InfoForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ["name", "nameeng", "descr", "descreng", "specialty", "keywords", "keywordseng", "maindep","achievements"]
        # self.fields["maindep"].widget = Select(choices = populate_deptree())
        self.fields["specialty"].initial = JournalRubric.objects.filter(id__in=[x['rubric'] for x in self.instance.rubrics.filter(rubric__categorization__code='NOMENKL').values('rubric')])
        for f in ["name", "nameeng", "keywords", "keywordseng","achievements"]:
            self.fields[f].widget = Textarea()

    def get_new_select2_ids(self,name):
        prefixed_name = self.add_prefix(name)
        new_ids = self.fields[name].widget.value_from_datadict(self.data, self.files, prefixed_name)
        return new_ids

    def get_old_select2_ids(self,name):
        old_ids=[]
        initial_value = self.fields[name].initial
        for member in initial_value:
            old_ids.append(unicode(member.id))
        return old_ids



class MyBaseModelFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        if kwargs.get("queryset", None):
            self.extra = 0
        super(MyBaseModelFormSet, self).__init__(*args, **kwargs)


class MemberForm(forms.Form):
    head_member = Select2SingleWorkerField(label=u'Руководители коллектива', widget_kwargs={'width': 700}, initial='')
    deputy_members = Select2WorkerField(label=u'Заместители руководителей коллектива', widget_kwargs={'width': 700}, required=False, initial='')
    common_members = Select2WorkerField(label=u'Участники коллектива', required=False, widget_kwargs={'width': 700}, initial='')

    def __init__(self, *args, **kwargs):
        head_member =  kwargs.pop("head_member", None)
        deputy_members =  kwargs.pop("deputy_members", None)
        common_members =  kwargs.pop("common_members", None)
        super(MemberForm, self).__init__(*args, **kwargs)
        self.initial['head_member'] = head_member
        self.initial['deputy_members'] = deputy_members
        self.initial['common_members'] = common_members

    def get_new_member_ids(self,name):
        prefixed_name = self.add_prefix(name)
        new_ids = self.fields[name].widget.value_from_datadict(self.data, self.files, prefixed_name)
        return new_ids

    def get_old_member_ids(self,name):
        old_ids=[]
        initial_value = self.initial[name]
        for member in initial_value:
            old_ids.append(unicode(member.id))
        return old_ids

    def _get_changed_data(self):
        if self._changed_data is None:
            self._changed_data = []
            for name, field in self.fields.items():
                prefixed_name = self.add_prefix(name)
                data_value = field.widget.value_from_datadict(self.data, self.files, prefixed_name)
                initial_value = self.initial.get(name, field.initial)
                if name =='head_members' or name == 'common_members' or name == 'deputy_members' :
                    members_init=[]
                    for member in initial_value:
                        members_init.append(unicode(member.id))
                    members_init.sort()
                    data_value.sort()
                    if members_init != data_value:
                        self._changed_data.append(name)
                else:
                    if field.widget._has_changed(initial_value, data_value):
                        self._changed_data.append(name)
        return self._changed_data
    changed_data = property(_get_changed_data)

class SchoolRubLinkFormSet(MyBaseModelFormSet):
    def __init__(self, *args, **kwargs):
        super(SchoolRubLinkFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.fields["rubric"].widget = FilteredSelect(choices = populate_scopus('Scopus'))
            form.fields["rub2_id"].widget = FilteredSelect(choices = populate_scopus('GRNTI'))


class RequiredFormSet(MyBaseModelFormSet):
    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


def get_rub_choices(rubtype='Scopus'):
    RUBS=list()
    for rub in JournalRub.objects.raw("""
        select f_journalrub_id, lpad('--', 2*(level-1)) || decode(level, 1, '', ' ') || f_journalrub_name AS f_journalrub_name
        from  journalrub
        where f_journalrubtype_id in (select f_journalrubtype_id from journalrubtype where f_journalrubtype_name=%s)
        start with JOU_F_JOURNALRUB_ID is NULL
        connect by JOU_F_JOURNALRUB_ID = prior F_JOURNALRUB_ID
        """, [rubtype]):
        RUBS.append( (rub.id, rub.name) )
    RUBS


class RubLinkForm(forms.Form):
    grnti_rubs = forms.CharField(max_length=4000, required=False, label=u'')
    scopus_rubs = forms.CharField(max_length=4000, required=False, label=u'')
    def __init__(self, *args, **kwargs):
        super(RubLinkForm, self).__init__(*args, **kwargs)
        for fn in ["grnti_rubs", "scopus_rubs"]:
            self.fields[fn].widget.attrs['class'] = 'hide'


RubLinkFormSet = modelformset_factory(SchoolRubLink, exclude = ('school', 'id'), extra=1, can_delete=True, formset=SchoolRubLinkFormSet)#, formset=RequiredFormSet)


class BaseMemberFormSet(MyBaseModelFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseMemberFormSet, self).__init__(*args, **kwargs)
        self.queryset = SchoolMembers.objects.none()
        for form in self.forms:
            form.fields["maindep"].widget = Select(choices = populate_deptree())
            form.fields['lastname'].widget.attrs['class'] = 'autocomplete_workers w_lastname'
            form.fields['firstname'].widget.attrs['class'] = 'w_firstname'
            form.fields['middlename'].widget.attrs['class'] = 'w_middlename'
            form.fields['maindep'].widget.attrs['class'] = 'w_maindep'
            form.fields['degree'].widget.attrs['class'] = 'w_degree'
            form.fields['rank'].widget.attrs['class'] = 'w_rank'
            form.fields['email'].widget.attrs['class'] = 'w_email'

            form.fields["worker"].widget = TextInput()
            form.fields['worker'].widget.attrs['class'] = 'hide w_id'


MemberFormSet = modelformset_factory(SchoolMembers, exclude = ('school', 'id'), extra=1, can_delete=True, formset=BaseMemberFormSet)


class PubForm(forms.ModelForm):
    class Meta:
        model = SchoolPub
        exclude = ('school', 'id', 'activity_id', 'activity_type')
    def __init__(self, *args, **kwargs):
        super(PubForm, self).__init__(*args, **kwargs)
        self.fields['reference'].widget.attrs['class'] = 'school-reference autocomplete_publication'


class BasePubFormSet(MyBaseModelFormSet):
   def __init__(self, *args, **kwargs):
        super(BasePubFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.fields['reference'].widget = Textarea()
            form.fields['reference'].widget.attrs['class'] = 'school-reference autocomplete_publication'
            form.fields['activity_id'].widget.attrs['class'] = 'hide p_activity_id'
            form.fields['activity_type'].widget.attrs['class'] = 'hide p_activity_type'


PubFormSet = modelformset_factory(SchoolPub, exclude = ('school', 'id'), extra=1, can_delete=True,  formset=BasePubFormSet) #)=MyBaseModelFormSet)


class BaseGrantFormSet(MyBaseModelFormSet):
   def __init__(self, *args, **kwargs):
        super(BaseGrantFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.fields['name'].widget = Textarea()
            form.fields['name'].widget.attrs['class'] = 'grant-name'
            form.fields['start'].widget.attrs['class'] = 'narrow datepicker'
            form.fields['end'].widget.attrs['class'] = 'narrow datepicker'


class GrantForm(forms.ModelForm):
    class Meta:
        model = SchoolGrant
        exclude = ('school', 'id')
    def __init__(self, *args, **kwargs):
        super(GrantForm, self).__init__(*args, **kwargs)
        self.fields['start'].widget.attrs['class'] = 'narrow datepicker'
        self.fields['end'].widget.attrs['class'] = 'narrow datepicker'

GrantFormSet = modelformset_factory(SchoolGrant, exclude=('school', 'id'), extra=1, can_delete=True, formset=BaseGrantFormSet)#, formset=RequiredFormSet)


class TechnoForm(forms.ModelForm):
    class Meta:
        model = SchoolTechno
        exclude = ('school', 'id')
    def __init__(self, *args, **kwargs):
        super(TechnoForm, self).__init__(*args, **kwargs)

TechnoFormSet = modelformset_factory(SchoolTechno, exclude = ('school', 'id'), extra=1, can_delete=True, formset=MyBaseModelFormSet)



class BasePatentFormSet(MyBaseModelFormSet):
    def __init__(self, *args, **kwargs):
        super(BasePatentFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.fields['date'].widget.attrs['class'] = 'narrow datepicker'


class PatentForm(forms.ModelForm):
    class Meta:
        model = SchoolPatent
        exclude = ('school', 'id')
    def __init__(self, *args, **kwargs):
        super(PatentForm, self).__init__(*args, **kwargs)

PatentFormSet = modelformset_factory(SchoolPatent, exclude = ('school', 'id'), extra=1, can_delete=True, formset=BasePatentFormSet)


class BaseSchoolRegnumFormSet(MyBaseModelFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseSchoolRegnumFormSet, self).__init__(*args,**kwargs)
        for form in self.forms:
            form.fields['date'].widget.attrs['class'] = 'narrow datepicker'

SchoolRegnumFormSet = modelformset_factory(SchoolRegnum, exclude = ('school', 'id'), extra=1, can_delete=True, formset=BaseSchoolRegnumFormSet)


class BaseRegnumFormSet(MyBaseModelFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseRegnumFormSet, self).__init__(*args,**kwargs)
        for form in self.forms:
            form.fields['date'].widget.attrs['class'] = 'narrow datepicker'


class RegnumForm(forms.ModelForm):
    class Meta:
        model = SchoolRegnum
        exclude = ('school', 'id', 'type')


RegnumFormSet = modelformset_factory(SchoolRegnum, exclude = ('school', 'id', 'type'), extra=1, can_delete=True, formset=BaseRegnumFormSet)
