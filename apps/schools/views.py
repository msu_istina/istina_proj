# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import simplejson
from paging.helpers import paginate
import datetime
from django.contrib import messages
from django.utils.decorators import classonlymethod

from common.utils.activities import get_activities_last_added, fetch_activities_compact
from common.utils.user import get_worker
from publications.models import Article, Book
from workers.models import Worker, Profile
from organizations.models import Department
from organizations.utils import is_representative_for
from django.template.loader import render_to_string
from django.shortcuts import render_to_response
from django.contrib.formtools.wizard.views import SessionWizardView

from django.forms.formsets import formset_factory, BaseFormSet

from django.db import connection, transaction
from collections import namedtuple

from common.utils.autocomplete import autocomplete_helper
from common.utils.uniqify import uniqify
from common.views import LinkedToWorkersModelWizardView, MyModelWizardView
from models import School, populate_scopus, populate_scopus_level, SchoolMemberRoles
from forms import SchoolForm, NameForm, InfoForm, RubLinkForm, RubLinkFormSet, MemberFormSet, PubFormSet, GrantFormSet, TechnoFormSet, SchoolMembers, SchoolPub, SchoolGrant, SchoolTechno, SchoolRubLink, PatentFormSet, SchoolPatent, RegnumFormSet, SchoolRegnum, SchoolRegnumFormSet, MemberForm
from itertools import chain
from journals.models import JournalRubric

from patents.models import Patent

# Create your views here.

from django.utils.decorators import wraps
def must_revalidate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        response["Cache-Control"] = "no-cache, must-revalidate, no-store"
        response["Pragma"] = "no-cache"
        return response
    return wrapper


SCHOOL_ADMINS=['safonin', 'S.Y.Egorov','Reznikova_Alexandra', 'shibaev','smamakina', 'andrey.zenzinov']


def have_access(user, theSchool, action='print'):
    if (theSchool.creator and theSchool.creator == user) and action in ['print', 'edit', 'sign']:
        return True

    if action in ['print', 'edit', 'approve', 'unsign']:
        if (user.is_superuser
            or user.username in SCHOOL_ADMINS
            or is_representative_for(user, theSchool.maindep)):
            return True

    return False



@login_required
@must_revalidate
def index(request):
    if request.user.representatives.exists():
        return redirect("school_list_all")
    schools = School.objects.filter(creator = request.user)
    return render(request, "schools/index.html", {'schools': schools})

@login_required
@must_revalidate
def indexschools(request):
    if request.user.representatives.exists():
        return redirect("school_list_all_schools")
    schools = School.objects.filter(creator = request.user, isschool=1)
    return render(request, "schools/index.html", {'schools': schools, 'isschool':True})


#
# Add wizard
#

FORMS = [("info", InfoForm),
         #("rubs_scopus", RubLinkFormSet),
         ("rubs_scopus", RubLinkForm),
         ("members", MemberForm),
         ("pubs", PubFormSet),
         ("grants", GrantFormSet),
         ("patents", PatentFormSet),
         ("technos", TechnoFormSet),
         ("regnums", RegnumFormSet),
         ]

SCHOOLFORMS = [("info", InfoForm),
         ("rubs_scopus", RubLinkForm),
         ("members", MemberForm),
         ("pubs", PubFormSet),
         ("grants", GrantFormSet),
         ("patents", PatentFormSet),
         ("regnums", SchoolRegnumFormSet),
         ]


@login_required
def AddWizView (request):
    cw = AddWizard.as_view(FORMS)
    return cw(request)

@login_required
def AddSchoolWizView (request):
    cw = AddSchoolWizard.as_view(SCHOOLFORMS, isSchool=True)
    return cw(request)

#class AddWizard(SessionWizardView):
#class AddWizard(MyModelWizardView):


class AddWizard(MyModelWizardView):
    model = School
    isSchool = False

    @classonlymethod
    def as_view(self, form_list=None, **kwargs):
        self.isSchool = kwargs.pop("isSchool",False)
        return super(MyModelWizardView, self).as_view(form_list, **kwargs)

    def get_template_names(self):
        return "schools/add_wizard.html"

    def get_form_instance(self, step):
        # if step == "members" and not self.instance_dict.get(step, None):
        #     return SchoolMembers.objects.none()
        #if step == "rubs_scopus" and not self.instance_dict.get(step, None):
        #    return SchoolRubLink.objects.none()
        if step == "pubs" and not self.instance_dict.get(step, None):
            return SchoolPub.objects.none()
        if step == "grants" and not self.instance_dict.get(step, None):
            return SchoolGrant.objects.none()
        if step == "patents" and not self.instance_dict.get(step, None):
            return SchoolPatent.objects.none()
        if step == "technos" and not self.instance_dict.get(step, None):
            return SchoolTechno.objects.none()
        if step == "regnums" and not self.instance_dict.get(step, None):
            return SchoolRegnum.objects.none()
        return super(AddWizard, self).get_form_instance(step)

    def get_form_kwargs(self, step=None):
        """
        Returns the keyword arguments for instantiating the form
        (or formset) on given step.
        """
        kwargs = super(AddWizard, self).get_form_kwargs(step)
        if step == "members" and self.instance_dict.has_key('info'):
            kwargs['head_member'] = self.instance_dict['info'].head_member
            kwargs['deputy_members'] = self.instance_dict['info'].deputy_members
            kwargs['common_members'] = self.instance_dict['info'].common_members
        return kwargs

    def done(self, form_list, **kwargs):
        step = -1

        for form in form_list:
            step += 1
            if step == 0:
                theSchool = form.save(commit=False)
                theSchool.creator = self.request.user
                theSchool.isschool = self.isSchool
                theSchool.save()
                # save specialties
                if not theSchool.specialties:
                    for item in form.cleaned_data['specialty']:
                        SchoolRubLink.objects.create(school=theSchool,rubric=item)
                        # spec.save()
                    theSchool.save()
                else:
                    old_objects = form.get_old_select2_ids('specialty')
                    new_objects = form.get_new_select2_ids('specialty')
                    del_objects = [x for x in old_objects if x not in new_objects]
                    add_objects = [x for x in new_objects if x not in old_objects]
                    for x in add_objects:
                        obj = JournalRubric.objects.get(pk=x)
                        SchoolRubLink.objects.create(school=theSchool,rubric=obj)
                    if len(del_objects) > 0:
                        SchoolRubLink.objects.filter(school=theSchool,rubric__id__in=del_objects).delete()
                    theSchool.save()

            elif step == 1:
                try:
                    SchoolRubLink.objects.filter(school=theSchool,rubric__categorization__code='Scopus').delete()
                    SchoolRubLink.objects.filter(school=theSchool,rubric__categorization__code='GRNTI').delete()
                    for field_name in ['grnti_rubs', 'scopus_rubs']:
                        for id_str in form.cleaned_data.get(field_name, '').split(','):
                            try:
                                rub_id = None # scopus
                                rub2_id = None # grnti
                                if field_name == 'grnti_rubs':
                                    rub2_id = int(id_str)
                                    rubric = JournalRubric.objects.get(id=rub2_id)
                                else:
                                    rub_id = int(id_str)
                                    rubric = JournalRubric.objects.get(id=rub_id)
                                rublink = SchoolRubLink(school=theSchool, rubric=rubric)
                                rublink.save()
                            except:
                                pass
                except:
                    pass
            elif step == 2:
                try:
                    if 'head_member' in form.changed_data or 'deputy_members' in form.changed_data or 'common_members' in form.changed_data:
                        head_member = form.cleaned_data['head_member']
                        deputy_members = form.get_new_member_ids('deputy_members')
                        common_members_before = form.get_new_member_ids('common_members')
                        common_members = [x for x in common_members_before if x != head_member.id]
                        if len(common_members_before) != len(common_members):
                            messages.info(self.request, u'При добавлении работы необязательно указывать одного человека и как руководителя, '
                                                   u'и как участника. На странице коллектива он будет указан '
                                                   u'просто как руководитель.')
                    name = 'head_member'
                    role = SchoolMemberRoles.objects.get(name=u"Руководитель")
                    if name in form._changed_data:
                        try:
                            form._changed_data.remove(name)
                        except:
                            pass
                        old_head = form.initial[name]
                        if old_head:
                            if head_member != old_head:
                                SchoolMembers.objects.filter(school=theSchool,role=role).filter(worker=old_head).delete()
                                SchoolMembers.objects.create(worker=head_member, role=role, school=theSchool,lastname=head_member.lastname, firstname=head_member.firstname, middlename=head_member.middlename)
                        else:
                            SchoolMembers.objects.create(worker=head_member, role=role, school=theSchool,lastname=head_member.lastname, firstname=head_member.firstname, middlename=head_member.middlename)

                    name = 'deputy_members'
                    role = SchoolMemberRoles.objects.get(name=u"Заместитель руководителя")
                    if name in form._changed_data:
                        try:
                            form._changed_data.remove(name)
                        except:
                            pass
                        old_member_ids = form.get_old_member_ids(name)
                        add_member = [x for x in deputy_members if x not in old_member_ids]
                        del_member = [x for x in old_member_ids if x not in deputy_members]
                        for x in add_member:
                            m = Worker.objects.get(pk=x)
                            SchoolMembers.objects.create(worker=m,role=role,school=theSchool,lastname=m.lastname, firstname=m.firstname, middlename=m.middlename)
                        if len(del_member) > 0:
                            SchoolMembers.objects.filter(school=theSchool,role=role).filter(worker__in=del_member).delete()

                    name = 'common_members'
                    role = SchoolMemberRoles.objects.get(name=u"Участник")
                    if name in form._changed_data:
                        try:
                            form._changed_data.remove(name)
                        except:
                            pass
                        old_member_ids = form.get_old_member_ids(name)
                        add_member = [x for x in common_members if x not in old_member_ids]
                        del_member = [x for x in old_member_ids if x not in common_members]
                        for x in add_member:
                            m = Worker.objects.get(pk=x)
                            SchoolMembers.objects.create(worker=m,role=role,school=theSchool,lastname=m.lastname, firstname=m.firstname, middlename=m.middlename)
                        if len(del_member) > 0:
                            SchoolMembers.objects.filter(school=theSchool,role=role).filter(worker__in=del_member).delete()
                except:
                    pass

            else:
                for one_form in form.forms:
                    try:
                        if hasattr(one_form, 'cleaned_data') and one_form.cleaned_data:
                            if one_form.cleaned_data.get('DELETE', False):
                                if one_form.cleaned_data['id']:
                                    one_form.cleaned_data['id'].delete()
                            else:
                                instance = one_form.save(commit=False)
                                instance.school = theSchool
                                instance.save()
                    except:
                        pass

        if theSchool.isschool:
            messages.success(self.request, u'Данные о научной школе сохранены.')
            return redirect("schools_index_schools")
        messages.success(self.request, u'Данные о научном коллективе сохранены.')
        return redirect("schools_index")



schoolrublink_sql="""
        select *
        from schoolrublink
        where f_journalrub_id in (select f_journalrub_id from journalrub where f_journalrubtype_id in (select f_journalrubtype_id from journalrubtype where f_journalrubtype_name=%s))
        and f_school_id = %s
        order by f_shoolrublink_id
"""


class AddSchoolWizard(AddWizard):
    steps_info = {
        'info' : {'title' : u'Общие сведения о школе'},
        'rubs_scopus' : {'title' : u'Рубрики школы',
                         'help_text': [u'''Необходимо выбрать области наук по классификаторам Scopus и ГРНТИ
(выбирается из выпадающего списка; рекомендуется не более трех областей по
каждому из классификаторов).''']},
        'members' : {'title' :u'Участники школы'},
        'pubs' : {'title' :u'Основные публикации',
#                      'help_text': [u'''Вводится любое количество (рекомендуется не менее 5 и не более 20) основных наиболее значимых и
# цитируемых публикаций научного коллектива.''', u'''<b>Для поиска публикации</b> в базе введите начальные буквы названия работы и выберите соответствующую строчку в появившемся окне.
# В поле может быть введена полная библиографическая ссылка с авторами, названием и т.п. (для работ, которые не были ранее
# введены в ИСТИНу).''']
 },
        'grants' : {'title' :u'Гранты школы',
                    'help_text': [u'''Вносится информация о текущих грантах (всех), выполненных грантах (не более 10), договорах о НИР с организациями (хоздоговора): текущие – все, а законченные – не более 10. Номер проекта – в соответствии с номером, присвоенным организацией-источником гранта. «Номер ЦИТИС» - номер темы, присвоенной при регистрации тематики данного проекта (гранта) в ЦИТИС (http://www.rntd.citis.ru/). В окошке РФ (Российская Федерация) указывается, является ли грант российским.''']},
        'patents' : {'title' :u'Патенты школы',
                     'help_text': [u'''Вносятся данные о полученных патентах, зарегистрированных программах и т.п. (необязательный блок).''']},
        'technos' : {'title' :u'Технологии',
                     'help_text': [u'''Вносятся данные о созданных технологиях (необязательный блок).''']},
        'regnums' : {'title' :u'Регистрационные номера',
                     'help_text': [u'''Номер РНТД заполняется только в том случае, если по грантам (договорам, контрактам) был зарегистрирован соответствующий РНТД.''']}
    }
    def get_template_names(self):
        return "schools/add_school_wizard.html"

    def get_context_data(self, form, **kwargs):
        context = super(AddSchoolWizard,self).get_context_data(form=form, **kwargs)
        context['current_step_info'] = self.steps_info.get(self.steps.current, None)
        return context

@login_required
def edit(request, school_id):
    theSchool = get_object_or_404(School, pk=school_id)
    if theSchool.creator and theSchool.creator != request.user and not request.user.is_superuser:
        messages.warning(request, u'У Вас нет прав для редактирования данной записи.')
        return redirect("schools_index")
    if not (not theSchool.state or theSchool.state == 'edit'):
        messages.warning(request, u'Форма была подписана. У Вас нет прав для редактирования данной записи.')
        return redirect("school_print", school_id=theSchool.id)
    inst_dict = {
        'info': theSchool,
        'members': SchoolMembers.objects.filter(school=theSchool),
        'pubs': SchoolPub.objects.filter(school=theSchool),
        'grants': SchoolGrant.objects.filter(school=theSchool),
        'patents': SchoolPatent.objects.filter(school=theSchool),
        'technos': SchoolTechno.objects.filter(school=theSchool),
        'regnums': SchoolRegnum.objects.filter(school=theSchool),
        }
    scopus_rubs = ""
    grnti_rubs = ""

    for r in theSchool.scopus_rubrics.keys():
        scopus_rubs += str(r) + ","
    for r in theSchool.grnti_rubrics.keys():
        grnti_rubs += str(r) + ","
    initial_dict = {
        'rubs_scopus': {"scopus_rubs": scopus_rubs, "grnti_rubs": grnti_rubs}
        }
    if theSchool.isschool:
        return AddSchoolWizard.as_view(SCHOOLFORMS, instance_dict=inst_dict, initial_dict=initial_dict,isSchool=True)(request)
    return AddWizard.as_view(FORMS, instance_dict=inst_dict, initial_dict=initial_dict)(request)


@login_required
def print_school(request, school_id):
    theSchool = get_object_or_404(School, pk=school_id)
    #    if not ((theSchool.creator and theSchool.creator == request.user)
    #            or request.user.is_superuser or request.user.username in SCHOOL_ADMINS):
    if not have_access(request.user, theSchool, 'print'):
        messages.warning(request, u'У Вас нет прав для просмотра данной записи.')
        return redirect("schools_index")
    # scopus = {}
    # grnti = {}
    # for key, val in populate_scopus('Scopus', indent=False):
    #     scopus[int(key)] = val
    # for key, val in populate_scopus('GRNTI', indent=False):
    #     grnti[int(key)] = val
    # scopus_rubs = [scopus[sr.rub_id] for sr in theSchool.rubrics.all() if sr.rub_id ]
    # grnti_rubs = [grnti[sr.rub2_id] for sr in theSchool.rubrics.all() if sr.rub2_id ]
    can_sign = (theSchool.creator == request.user)
    can_approve = have_access(request.user, theSchool, 'approve')
    return render(request, "schools/print.html", {'school': theSchool,
                                                  # 'scopus_rubs': scopus_rubs, 'grnti_rubs': grnti_rubs,
                                                  'can_sign': can_sign,
                                                  'can_approve': can_approve})

@login_required
def sign_school(request, school_id):
    from django.db import connection, transaction

    theSchool = get_object_or_404(School, pk=school_id)
    action = request.POST.get('action', 'sign')

    if (action not in ['sign','approve','unsign'] or not have_access(request.user, theSchool, action)):
        messages.warning(request, u'У Вас нет прав для выполнения требуемой операции с данной формой.')
        return redirect("schools_index")

    action_state = {'sign': 'signed',
                    'approve': 'approved',
                    'unsign': 'edit'}
    try:
        new_state = action_state[action]
        cursor = connection.cursor()
        cursor.execute("""
          UPDATE SCHOOL SET f_school_status=%s, f_school_datesigned=sysdate
          WHERE f_school_id=%s
          """, [new_state, theSchool.id])
        cursor.execute("""
          INSERT INTO SCHOOLACTION (f_schoolaction_id, f_school_id, f_schoolaction_action, f_schoolaction_user)
          VALUES (seq_new_id.nextval, %s, %s, %s)
          """, [theSchool.id, action, request.user.id])
        transaction.commit()
        if theSchool.isschool:
            messages.success(request, u'Форма подписана. Теперь Вы можете распечатать информацию о научной школе.')
        else:
            messages.success(request, u'Форма подписана. Теперь Вы можете распечатать информацию о научном коллективе.')
    except:
        transaction.rollback()
        messages.error(request, u'Не удалось подписать форму.')
    if theSchool.isschool:
        return redirect("schools_index_schools")
    return redirect("schools_index")


def autocomplete_search_member(request):
    '''Server side for jquery autocomplete
    Returns objects of specified type matching query term.
    '''
    term = request.GET.get('term', '')
    fields = []
    limit = 20
    if len(term) >= 3:
        profiles = autocomplete_helper(Profile, "lastname", term, limit)
        workers = [p.worker for p in profiles if p.worker]
        workers =filter(bool, workers)
        message_data = [{'label': w.fullname,
                         'value': {'id': w.id,
                                   'firstname': w.firstname, 'middlename': w.middlename, 'lastname': w.lastname,
                                   'degree': w.degrees.all()[0].degree.name if w.degrees.all() else '',
                                   'maindep': w.current_employments[0].department.id if w.current_employments else 0,
                                   }
                         } for w in workers]
        #fields = [{'label': item.fullname, 'value': item.get_fullname(initials=True)} for item in workers]
        #fields = uniqify(fields, lambda i: i['value'])
        message_data = uniqify(message_data, lambda i: i['value']['id'])
    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')


def autocomplete_search_pub(request):
    '''Server side for jquery autocomplete
    Returns objects of specified type matching query term.
    '''
    term = request.GET.get('term', '')
    fields = []
    limit = 20
    if len(term) >= 3:
        pubs = autocomplete_helper(Article, "title", term, limit)
        message_data = [{'label': p.title,
                         'value': {'id': p.id,
                                   'type': 'article',
                                   'citation': p.get_citation(short=True)
                                   }
                         } for p in pubs]
        message_data = uniqify(message_data, lambda i: i['value']['id'])

        pubs = autocomplete_helper(Book, "title", term, limit/2)
        message_data2 = [{'label': p.title,
                         'value': {'id': p.id,
                                   'type': 'book',
                                   'citation': p.get_citation(short=True)
                                   }
                         } for p in pubs]

        pubs = autocomplete_helper(Patent, "title", term, limit)
        message_data3 = [{'label': p.title,
                         'value': {'id': p.id,
                                   'type': 'patent',
                                   'citation': u"Патент # %s: %s" % (p.number, p.title)
                                   }
                         } for p in pubs]

        message_data = message_data + message_data2 + message_data3
        message_data = uniqify(message_data, lambda i: i['value']['id'])
    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')


def get_rub(request):
    '''Return GRNTI rubricator as json
    '''
    from django.db import connection, transaction

    type = request.GET.get('rub', 'GRNTI')
    node_id = request.GET.get('node_id', None)
    try:
        node_id = int(node_id)
    except:
        node_id = None

    if node_id: # support for dynatree lazy mode: output all children of node_id
        message_data = [{"title": val, "key": key, "isFolder": True, "isLazy": True} for key, val in populate_scopus_level('GRNTI', node_id=node_id)]
        return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')

    # build complete tree

    message_data = list()
    try:
        cursor = connection.cursor()
        sql = """
        select id, title, tree_level from
        (
        select f_journalrub_id AS id, f_journalrub_name AS title, level AS tree_level
        from  journalrub
        where f_journalrubtype_id in (select f_journalrubtype_id from journalrubtype where f_journalrubtype_name=%s)
        start with JOU_F_JOURNALRUB_ID is NULL
        connect by JOU_F_JOURNALRUB_ID = prior F_JOURNALRUB_ID
        order siblings by journalrub.f_journalrub_code, journalrub.f_journalrub_name, journalrub.f_journalrub_id
        )
        UNION ALL
        select 0, 'DUMMY', 1 from dual
        """
        cursor = connection.cursor()
        cursor.execute(sql, [type])

        # convert into json representation accepted by jquery.dynatree
        prev = None
        stack = [] # stack contains pairs (m, i), where m is a list of completed descriptions
                   # of all nodes at the same level and i is the last item that should be
                   # appended to m when its children field will be constructed.
        count = 0
        for key, title, level in cursor:
            if prev:
                if level == prev["level"]:
                    message_data.append({"title": prev['title'], "key": prev['key'], "isFolder": False, "isLazy": False})
                elif level > prev["level"]:
                    item = {"title": prev['title'], "key": prev['key'], "isFolder": True, "unselectable": True, "isLazy": False}
                    stack.append((message_data, item))
                    message_data = []
                elif level < prev["level"]:
                    message_data.append({"title": prev['title'], "key": prev['key'], "isFolder": False, "isLazy": False})
                    while level < prev["level"]:
                        last_list, last_item = stack.pop()
                        last_item["children"] = message_data
                        message_data = last_list
                        message_data.append(last_item)
                        prev["level"] = prev["level"]-1
            prev = {"key": key, "title": title, "level": level}
    except:
        transaction.rollback()

    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')


@login_required
def list_all(request):
    if not (request.user.is_superuser
            or request.user.username in SCHOOL_ADMINS
            or request.user.representatives.exists()
            ):
        messages.warning(request, u'У Вас нет прав для просмотра данной записи.')
        return redirect("schools_index")
    if request.user.representatives.exists():
        depsids = request.user.representatives.values_list('department', flat=True)
        deps = Department.objects.filter(id__in=depsids)
        subtree = chain.from_iterable(dep.descendants(show_level=False) for dep in deps)
        schools = School.objects.filter(maindep__in=subtree)
    else:
        schools = School.objects.all()
    return render(request, "schools/index.html", {'schools': schools, 'show_all': True})

@login_required
def list_all_schools(request):
    if not (request.user.is_superuser
            or request.user.username in SCHOOL_ADMINS
            or request.user.representatives.exists()
            ):
        messages.warning(request, u'У Вас нет прав для просмотра данной записи.')
        return redirect("schools_index")
    if request.user.representatives.exists():
        depsids = request.user.representatives.values_list('department', flat=True)
        deps = Department.objects.filter(id__in=depsids)
        subtree = chain.from_iterable(dep.descendants(show_level=False) for dep in deps)
        schools = School.objects.filter(maindep__in=subtree, isschool=1)
    else:
        schools = School.objects.filter(isschool=1)
    return render(request, "schools/index.html", {'schools': schools, 'isschool':True, 'show_all': True})



@login_required
def list_all_grants(request):
    if not (request.user.is_superuser
            or request.user.username in SCHOOL_ADMINS
            ):
        messages.warning(request, u'У Вас нет прав для просмотра данной страницы.')
        return redirect("schools_index")
    sql = u"""
    select sg.f_schoolgrant_name as name, sg.f_schoolgrant_leader as leader,
           f_schoolgrant_budget as budget,
           to_char(f_schoolgrant_start,'yyyy') || '-' || to_char(f_schoolgrant_end,'yyyy') as dates,
           s.f_school_id, s.f_school_name as schoolname,
           d.f_department_name depname,
           td.f_department_name topdepname,
           s.f_school_status
    from schoolgrant sg, school s, department td, root_department rdp, department d
    where s.f_school_id = sg.f_school_id
      and (s.f_school_status = 'signed' or s.f_school_status = 'approved')
      and s.f_department_id = rdp.f_department_id
      and rdp.root_f_department_id = td.f_department_id
      and d.f_department_id = s.f_department_id
    """
    cursor = connection.cursor()
    cursor.execute(sql)

    SchoolGrantTuple = namedtuple('SchoolGrantTuple', 'name, leader_name, budget, dates, school_id, school_name, maindep_name, topdep_name school_state')
    sgs = [SchoolGrantTuple(*item) for item in cursor]
    return render(request, "schools/grants_all.html", {'schoolgrants': sgs})
