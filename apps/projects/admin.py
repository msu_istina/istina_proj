from common.utils.admin import register_with_versions, MyModelAdmin
from models import Project, ProjectMembership, ProjectStep, ProjectArticle, ProjectBook, Accomplice, AccompliceCategory, \
    AccompliceRole, ProjectKeyword, ProjectRubric, SponsorCategory, Sponsor, Estimate, EstimateCategory, Customer,\
    PNR, PNRRubricMembership, PStepStatus, ProjectAnnotationalReport,PStepPlan,PlanType


register_with_versions(Project)
register_with_versions(ProjectMembership)
register_with_versions(ProjectStep)
register_with_versions(ProjectArticle)
register_with_versions(ProjectBook)
register_with_versions(Accomplice)
register_with_versions(AccompliceCategory)
register_with_versions(AccompliceRole)
register_with_versions(ProjectKeyword)
register_with_versions(ProjectRubric)
register_with_versions(SponsorCategory)
register_with_versions(Sponsor)
register_with_versions(Estimate)
register_with_versions(EstimateCategory)
register_with_versions(Customer)
register_with_versions(PNRRubricMembership)
register_with_versions(PStepStatus)
register_with_versions(ProjectAnnotationalReport)
register_with_versions(PStepPlan)
register_with_versions(PlanType)

class PNRAdmin(MyModelAdmin):
    list_display = ('name', 'department', 'parent', 'created', 'actual_date')
    list_filter = ('department',)

register_with_versions(PNR, PNRAdmin)