# -*- coding: utf-8; -*-
from django import forms
from django.db.models import Q
from django.forms.models import inlineformset_factory, formset_factory, BaseFormSet
from django.contrib import messages
from django.db import connection
from common.forms import MyForm, MyModelForm, WorkershipModelForm, Select2ModelField, Select2LinkedToWorkersModelForm, LightSelect2ModelField, LinkedToWorkersModelForm, Select2WorkerField, \
    MyBaseModelFormSet
from common.models import Country
from common.widgets import LightSelect2ChoiceWidget
from projects.models import Project, ProjectMembership, Accomplice, AccompliceRole, AccompliceCategory, Estimate,\
    EstimateCategory, Sponsor, ProjectRubric, PNR, SponsorCategory, ProjectStep, ProjectKeyword, ProjectAnnotationalReport, PStepStatus
from publications.models import Article, Book
from organizations.forms import DepartmentChoiceField
from journals.models import JournalRubric
from journals.forms import JournalRubricMultipleChoiceField
from actstream import action
from dissertations.models import Specialty, Dissertation
import re
import datetime
from workers.models import Worker
from django.core import serializers

class ProjectChoiceField(Select2ModelField):

    def __init__(self, *args, **kwargs):
        super(ProjectChoiceField, self).__init__(model=Project,  *args, **kwargs)

    def get_title(self, project):
        return project.name

    def get_description(self, project):
        try:
            department = project.department.name
        except AttributeError:
            return u''
        start_date = project.startdate
        end_date = project.enddate
        description = department + u', с ' + str(start_date)
        if end_date:
            description += u' по ' + str(end_date)
        return description

class ProjectMembershipForm(WorkershipModelForm):

    project = ProjectChoiceField(label=u'НИР',  add_option=True)
    fields_order = ["member_str", "project", "is_head", "startdate", "enddate"]

    class Meta:
        model = ProjectMembership
        exclude = ['member', 'creator']

    def clean_member_str(self):
        # only one member is allowed
        value = self.cleaned_data['member_str'].strip(", ")
        if "," in value:
            raise forms.ValidationError(u"Пожалуйста, укажите ФИО только одного участника НИР. В строке не должно быть запятых.")
        return value


    def save(self, request, workers, project, commit=True):
        # project must exist now
        membership = super(ProjectMembershipForm, self).save(request, commit=False)
        membership.project = project
        if commit:
            self.commit_save(request, membership, workers)
        return membership

class ProjectMembershipSimilarProjectsForm(MyForm):
    project = forms.ModelChoiceField(label=u"НИР", queryset=Project.objects.all(), empty_label=u"добавить новый НИР", required=False)


class ProjectForm(MyModelForm):
    fields_order = ['name', 'organization', 'startdate', 'enddate', 'description', 'main_results']
    class Meta:
        model = Project
        exclude = ('members', 'xml', 'creator', 'articles', 'books', 'outorganization')

class ProjectWizardForm(LinkedToWorkersModelForm):
    keywords = forms.CharField(label=u'Ключевые слова', max_length=1020, required=False, widget=forms.Textarea(attrs={
        'placeholder': u'Набор ключевых слов или фраз, разделенных запятой или точкой с запятой'}
    ))
    department = DepartmentChoiceField(label=u'Подразделение финансирования', required=False)
    outorganization = forms.CharField(label=u'или сторонняя организация', required=False)
    contacts = forms.CharField(label=u'Контакты',  widget=forms.Textarea(attrs={
        'placeholder': u'Укажите, пожалуйста, телефон или электронную почту, чтобы с Вами могли связаться при необходимости.'
                       u' Контактная информация доступна только исполнителям проекта,административным сотрудникам подразделения и ректората'})
    )
    number = forms.CharField(label=u"Номер договора (контракта, соглашения)")
    fields_order = ['name', 'department', 'outorganization', 'number', 'registration_number', 'startdate',
                    'enddate', 'keywords', 'contacts' ]
    css_classes = [('contacts', 'wide low'), ('keywords', 'wide low')]

    class Meta:
        model = Project
        exclude = ('xml', 'creator', 'articles', 'books', 'members')

    def __init__(self, *args, **kwargs):
        from unified_permissions import has_permission
        self.user = kwargs.pop('user', None)
        super(ProjectWizardForm, self).__init__(*args, **kwargs)
        self.remove_css_class('number', 'narrow')

        for field in self.fields:
            self.fields[field].widget.attrs['onchange'] = "edit_field=1"
        self.fields['outorganization'].widget.attrs['placeholder'] = u'Заполните, если проект ВЫПОЛНЯЕТСЯ в другой огранизации.'
        self.fields['outorganization'].widget.attrs['class'] = 'wide'
        self.fields['department'].widget.attrs['class'] = 'wide'
        if self.instance.show_edit and not has_permission(self.user, 'is_representative_for'):
            self.fields['name'].widget.attrs['readonly'] = True
            self.fields['department'].widget.attrs['readonly'] = True
            self.fields['outorganization'].widget.attrs['readonly'] = True

        if self.instance_linked():
            self.fields['keywords'].initial = self.instance.keywords_string()

    def commit_save(self, request, object, workers):
        from unified_permissions import has_permission
        if len(self.changed_data)>0:
            if len(object.outorganization) > 0:
                object.department = None
            if self.edit and object.show_edit:
                if not has_permission(request.user, 'is_representative_for'):
                    Project.objects.filter(id=object.id).update(number=object.number, registration_number=object.registration_number, startdate=object.startdate, enddate=object.enddate, contacts=object.contacts)
                else:
                    Project.objects.filter(id=object.id).update(number=object.number, name=object.name, department=object.department, outorganization=object.outorganization, registration_number=object.registration_number, startdate=object.startdate, enddate=object.enddate, contacts=object.contacts)
            else:
                object.save()
                if self.add:
                    step=ProjectStep.objects.create(project=object, number=1, name=object.name, startdate=object.startdate, enddate=object.enddate)

            if self.changed_data.count('keywords')>0:
                keywords_string = request.POST.get('main-keywords','')
                if keywords_string:
                    keywords = [x.strip().lower() for x in re.split('[,;]', keywords_string) if x.strip()]
                else:
                    keywords = []
                current_keywords = ProjectKeyword.objects.filter(project=object).values_list('word', flat=True)
                keywords_to_delete = set(current_keywords) - set(keywords)
                ProjectKeyword.objects.filter(word__in=keywords_to_delete).delete()
                keywords_to_create = set(keywords) - set(current_keywords)
                for word in keywords_to_create:
                    if len(word) > 255:
                        messages.error(request, u"Набор ключевых слов или фраз разделяются запятой или точкой с запятой.<br>Слишком длинная фраза не сохранена: %s" % word)
                    else:
                        ProjectKeyword.objects.get_or_create(word=word, project=object)
            messages.success(request, u"Данные сохранены")
            self._changed_data=None
            description = ''
            action_str = (u"добавил", u"добавлен" + object.gender_suffix) if self.add else (u"отредактировал", u"отредактирован" + object.gender_suffix)
            action.send(request.user, verb=u'%s %s' % (action_str[0], object.accusative), target=object, description=description)
        else:
            messages.success(request, u"Данные не были изменены.")

class ProjectWizardFormShort(MyModelForm):
    department_or_outorganization = forms.CharField(label=u'Подразделение...', required=False)
    fields_order = ['name', 'department_or_outorganization']

    class Meta:
        model = Project
        fields = ['name', ]

    def __init__(self, *args, **kwargs):
        super(ProjectWizardFormShort, self).__init__(*args, **kwargs)
        self.fields['department_or_outorganization'].initial = self.instance.department_string()

class ArticleSelect2Field(Select2ModelField):
    #search_fields = ('authorships__original_name__startswith', 'title__icontains',)
    # search_fields duplicates records due to wrong SQL code
    def __init__(self, *args, **kwargs):
        super(ArticleSelect2Field, self).__init__(self, *args, add_option=False, model=Article, **kwargs)
        self.widget.options['placeholder'] = u'Введите часть названия статьи'

    def get_title(self, obj):
        try:
            return obj.formatted_references.all()[0].reference
        except Exception as e:
            return obj.title

class ProjectArticleForm(MyForm):
    article = ArticleSelect2Field(label=u'Добавить статью по НИР',
                                  widget_kwargs={'width': 640})

class ProjectBookForm(MyForm):
    book = Select2ModelField(label=u'Добавить книгу по НИР', model=Book, add_option=False,
                             widget_kwargs={'width': 640})

    def __init__(self, *args, **kwargs):
        super(ProjectBookForm, self).__init__(*args, **kwargs)
        self.fields['book'].widget.options['placeholder'] = u'Введите часть названия многорафии'


class ProjectDissertationForm(MyForm):
    dissertation = Select2ModelField(label=u'Добавить диссертацию по НИР', model=Dissertation, add_option=False,
                             widget_kwargs={'width': 640})

    def __init__(self, *args, **kwargs):
        super(ProjectDissertationForm, self).__init__(*args, **kwargs)
        self.fields['dissertation'].widget.options['placeholder'] = u'Введите часть названия диссертации'


class AccompliceForm(MyModelForm):
    category = forms.ModelChoiceField(label=u"Тип организации", queryset=AccompliceCategory.objects.all(),)
    role = forms.ModelChoiceField(label=u'Роль соисполнителя', queryset=AccompliceRole.objects.all())
    country = forms.ModelChoiceField(label=u'Страна соисполнителя', queryset=Country.objects.order_by('name'))

    css_classes = [('full_name', 'wide')]
    class Meta:
        model = Accomplice
        exclude = ('project', 'project_step')

    def __init__(self, *args, **kwargs):
        super(AccompliceForm, self).__init__(*args, **kwargs)
        country, created = Country.objects.get_or_create(iso2='RU', iso3='RUS', name='Russia', name_ru=u'Россия')
        self.fields['country'].initial = country.pk
        self.remove_css_class('name', 'wide')
        for field in self.fields:
            self.fields[field].widget.attrs['onchange'] = "edit_field=1"

    def save(self, commit=True):
        accomplice = super(AccompliceForm, self).save(self.request, commit=False)
        category = self.cleaned_data['category']
        accomplice.category = category
        role = self.cleaned_data['role']
        accomplice.role = role
        accomplice.country = self.cleaned_data['country']
        return super(AccompliceForm, self).save(self.request, commit)


AccompliceFormset = inlineformset_factory(Project, Accomplice, formset=MyBaseModelFormSet, form=AccompliceForm, extra=1)


class ReadOnlyText(forms.Widget):
    input_type = 'text'

    def render(self, name, value, attrs=None):
         if value is None:
             value = ''
         return value


class EstimateForm(MyForm):
    project_step_name = forms.CharField(label=u'Название этапа', widget=ReadOnlyText, required=False)
    class Media:
        js = ("projects/js/estimatesform.js",)

    def __init__(self,  *args, **kwargs):
        from unified_permissions import has_permission
        super(EstimateForm,self).__init__(*args, **kwargs)
        self.project_step = kwargs['initial']['project_step']
        self.fields['project_step_name'].initial = self.project_step.name
        estimate_categories = EstimateCategory.objects.all().order_by('order')
        for estimate_category in estimate_categories:
            field_name = 'category_%s' % estimate_category.id
            self.fields[field_name] = forms.DecimalField(label=estimate_category.name, required=False)
            self.fields[field_name].widget.attrs['onchange'] = "edit_field=1"
            try:
                estimate = Estimate.objects.get(project_step=self.project_step, category=estimate_category)
            except Estimate.DoesNotExist:
                pass
            else:
                self.fields[field_name].initial = estimate.value
            if self.project_step.show_edit and not has_permission(self.project_step._currentuser, 'is_representative_for'):
                self.fields[field_name].widget.attrs['readonly'] = True
        self._changed_data=None

    def _get_changed_data(self):
        if self._changed_data is None:
            self._changed_data = []
            for name, field in self.fields.items():
                if name == 'project_step_name':
                    continue
                prefixed_name = self.add_prefix(name)
                data_value = field.widget.value_from_datadict(self.data, self.files, prefixed_name)
                initial_value = self.initial.get(name, field.initial)
                if initial_value:
                    int_init = int(1000*float(initial_value))
                else:
                    int_init = None
                if data_value:
                    int_value = int(1000*float(data_value))
                else:
                    int_value = None
                if int_init <> int_value:
                    self._changed_data.append(name)
        return self._changed_data
    changed_data = property(_get_changed_data)

    def save(self,request):
        for category in EstimateCategory.objects.all().order_by('order'):
            field_name = 'category_%s' % category.id
            value = self.cleaned_data[field_name]
            if value is not None:
                estimate, created = Estimate.objects.get_or_create(project_step=self.project_step, project=self.project_step.project,
                                                                   category=category, defaults={'value': value})
                if not created:
                    estimate.value = value
                    estimate.save()
                self.fields[field_name].initial = value
            else:
                try:
                    estimate = Estimate.objects.get(project_step=self.project_step, project=self.project_step.project,
                                                    category=category)
                except Estimate.DoesNotExist:
                    pass
                else:
                    estimate.delete()
        self._changed_data=None
        messages.success(request, u"Смета этапа %s сохранена." % self.project_step.number )

class EstimateBaseFormset(BaseFormSet):
    no_extra_button = True
    def __init__(self, *args, **kwargs):
        self.project = kwargs.pop('instance')
        #kwargs['initial'] = [{'project_step': project_step} for project_step in self.project.steps.all()]
        steps_list = []
        for project_step in self.project.steps.all():
            project_step._currentuser = self.project._currentuser
            steps_list.append({'project_step': project_step})
        kwargs['initial'] = steps_list
        self.extra = 0
        return super(EstimateBaseFormset, self).__init__(*args, **kwargs)

    def save(self, request, obj):
        found = False
        for form in self.forms:
            if len(form.changed_data)>0:
                form.save(request)
                found = True
        if not found:
            messages.success(request, u"Данные не были изменены.")

    def total_form_count(self):
        return self.project.steps.count()

EstimateFormset = formset_factory(EstimateForm, formset=EstimateBaseFormset, extra=0)

class SponsorCategorySelectWidget(forms.Select):

    def render_option(self, selected_choices, option_value, option_label):
        option = super(SponsorCategorySelectWidget, self).render_option(selected_choices, option_value, option_label)

        def add_class_to_option(option, css_class):
            option = option[:7] + ' class="%s"' % css_class + option[7:]
            return option

        if option_value:
            sponsor_category = SponsorCategory.objects.get(pk=option_value)
            if sponsor_category.sponsor_has_own_name:
                option = add_class_to_option(option, 'has_own_name')
        return option


class SponsorForm(MyModelForm):
    category = forms.ModelChoiceField(SponsorCategory.objects.all(), label=u'Источник финансирования',
                                      widget=SponsorCategorySelectWidget, required=False)

    class Meta:
        model = Sponsor
        exclude = ('project', 'project_step')

    class Media:
        js = ("projects/js/sponsorform.js",)

    def __init__(self, *args, **kwargs):
        super(SponsorForm, self).__init__(*args, **kwargs)
        self.fields['category'].empty_label = u'другой (указать)'
        for field in self.fields:
            self.fields[field].widget.attrs['onchange'] = "edit_field=1"

    def clean(self):
        cleaned_data = super(SponsorForm, self).clean()
        category = cleaned_data['category']
        sponsor_category_name = cleaned_data['sponsor_category_name']
        name = cleaned_data['name']
        if not category and not sponsor_category_name:
            raise forms.ValidationError(u"Выберете тип источника финансирования из списка или введите свой вариант")
        if category and category.sponsor_has_own_name and not name:
            raise forms.ValidationError(u"Введите название программы для источника финансирования %s" % category)
        if category:
            cleaned_data['sponsor_category_name'] = None
            if not category.sponsor_has_own_name:
                cleaned_data['name'] = None
        return cleaned_data

    def save(self, commit=True):
        return super(SponsorForm, self).save(self.request, commit)


class SponsorFormsetBase(MyBaseModelFormSet):
    def __init__(self, *args, **kwargs):
        instance = kwargs['instance']
        if instance.sponsors.count():
            self.extra = 0
        super(SponsorFormsetBase, self).__init__(*args, **kwargs)

SponsorFormset = inlineformset_factory(Project, Sponsor, formset=SponsorFormsetBase, form=SponsorForm, extra=1)


class ProjectRubForm(MyForm):
    grnti_rubs = forms.CharField(max_length=4000, required=False, label=u'')
    scopus_rubs = forms.CharField(max_length=4000, required=False, label=u'')

    class Meta:
        exclude = ('achievement_id', 'rub_id')

    def __init__(self, instance, *args, **kwargs):
        super(ProjectRubForm, self).__init__(*args, **kwargs)
        self.fields['grnti_rubs'].widget.attrs['id'] = 'id_rubs_scopus-grnti_rubs'
        self.fields['scopus_rubs'].widget.attrs['id'] = 'id_rubs_scopus-scopus_rubs'
        self.fields['grnti_rubs'].widget.attrs['class'] = 'hide'
        self.fields['scopus_rubs'].widget.attrs['class'] = 'hide'
        scopus_rubs = ""
        grnti_rubs = ""
        if instance:
            for rubric in instance.scopus_rubrics:
                if rubric.journal_rubric:
                    scopus_rubs += str(rubric.journal_rubric.id) + ","
            for rubric in instance.grnti_rubrics:
                if rubric.journal_rubric:
                    grnti_rubs += str(rubric.journal_rubric.id) + ","
        self.fields['grnti_rubs'].initial = grnti_rubs
        self.fields['scopus_rubs'].initial = scopus_rubs
        for field in self.fields:
            self.fields[field].widget.attrs['onchange'] = "edit_field=1"

    def save(self, project):
        project.scopus_rubrics.delete()
        project.grnti_rubrics.delete()
        for field_name in ['grnti_rubs', 'scopus_rubs']:
            for id_str in self.cleaned_data.get(field_name, '').split(','):
                if id_str:
                    rub_id = int(id_str)
                    journal_rubric = JournalRubric.objects.get(id=rub_id)
                    ProjectRubric(project=project,  journal_rubric=journal_rubric, journal_category=journal_rubric.categorization).save()


class UDCMultipleChoiceField(JournalRubricMultipleChoiceField, forms.Field):
    default_validators = []
    def __init__(self, *args, **kwargs):
        super(UDCMultipleChoiceField, self).__init__(self, *args, input_string_option=True, **kwargs)

    def clean(self, value):
        qs = self.get_queryset()
        cleaned_value = []
        for rub_id in value:
            try:
                rub = JournalRubric.objects.get(id=int(rub_id))
                if qs.filter(pk=rub.pk).count() == 1:
                    cleaned_value.insert(0, rub)
            except (JournalRubric.DoesNotExist, ValueError, TypeError):
                cleaned_value.insert(0, rub_id) # Это не первичный ключ
        return cleaned_value


class ProjectAdditionalForm(MyModelForm):
    science_work_type = forms.ModelChoiceField(JournalRubric.objects.filter(categorization__code='PROJ-TYPE'), label=u'Тип НИР')
    social_economics_type = forms.ModelChoiceField(JournalRubric.objects.filter(categorization__code='SOC-ECON-GOALS'), label=u'Социально-экономические цели')
    knowledge_domain = forms.ModelChoiceField(JournalRubric.objects.filter(categorization__code='GEN-SCI'), label=u'Область знания')
    pnr_russia = forms.ModelChoiceField(JournalRubric.objects.filter(categorization__code='PNR-RF'), label=u'Приоритетное направление (ПН) развития России', required=False)
    technological_leap = forms.ModelChoiceField(JournalRubric.objects.filter(categorization__code='TP-RF'), label=u'Приоритетное направление технологического прорыва (ТП)', required=False)
    pnr_msu = forms.ModelChoiceField(JournalRubric.objects.filter(categorization__code='PNR-MSU'), label=u'Приоритетные направления развитияx МГУ до 2020 года', required=False)
    udc = UDCMultipleChoiceField(include_code=True, queryset=JournalRubric.objects.filter(categorization__code='UDC'), label=u'УДК', widget_kwargs={'width': 'wide'}, required=False)
    udc_old = forms.CharField(label=u'УДК (устаревшие)', required=False)
    fields_order = ['science_work_type', 'social_economics_type','knowledge_domain', 'pnr', 'pnr_russia', 'technological_leap', 'pnr_msu', 'udc', 'udc_old', 'description', 'main_results',]
    css_classes = [('pnr', 'wide'), ('description', 'wide'), ('main_results', 'wide'), ]

    class Meta:
        model = Project
        fields = ['pnr', 'description', 'main_results']

    def __init__(self, department, *args, **kwargs):
        super(ProjectAdditionalForm, self).__init__(*args, **kwargs)
        for field in ('science_work_type', 'social_economics_type','knowledge_domain', 'pnr_russia', 'technological_leap', 'pnr_msu'):
            category = self.fields[field].queryset[0].categorization
            try:
                rubric = ProjectRubric.objects.get(project=self.instance, journal_category=category)
            except ProjectRubric.DoesNotExist:
                pass
            else:
                self.fields[field].initial = rubric.journal_rubric

        category = self.fields['udc'].queryset[0].categorization
        self.fields['udc'].initial = ProjectRubric.objects.filter(project=self.instance, journal_category=category, journal_rubric__isnull=False).values_list('journal_rubric', flat=True)
        self.fields['udc_old'].widget.attrs['class'] = 'wide'
        self.fields['udc_old'].widget.attrs['placeholder'] = u"Введите коды через точку с запятой"
        self.fields['udc_old'].initial = "; ".join(ProjectRubric.objects.filter(project=self.instance, journal_category=category, journal_rubric__isnull=True).values_list('alternate_name', flat=True))

        if department:
            ancestors = department.get_ancestors_flat()
            descendants = department.descendants_queryset
            pnrs = PNR.objects.filter(Q(actual_date__isnull=True) | Q(actual_date__gte=datetime.datetime.now()), Q(department=department) | Q(department__in=ancestors) | Q(department__in=descendants))
        else:
            #self.fields['pnr'].widget.attrs['disabled'] = True
            pnrs = PNR.objects.filter(Q(department__id=0))
        self.fields['pnr'] = LightSelect2ModelField(queryset=pnrs, label=u'Приоритетное направление научных исследований', required=False, empty_label=u'другое', widget_kwargs={'width': 'wide'})
        for field in self.fields:
            self.fields[field].widget.attrs['onchange'] = "edit_field=1"

    def save(self, request, instance, commit=True):
        instance.description = self.cleaned_data['description']
        instance.main_results = self.cleaned_data['main_results']
        instance.pnr = self.cleaned_data['pnr']
        self.instance = instance
        project = super(ProjectAdditionalForm, self).save(request, commit)
        for field in ('science_work_type', 'social_economics_type','knowledge_domain', 'pnr_russia', 'technological_leap', 'pnr_msu'):
            category = self.fields[field].queryset[0].categorization
            rubric = self.cleaned_data[field]
            ProjectRubric.objects.filter(project=project, journal_category=category).exclude(journal_rubric=rubric).delete()
            if rubric:
                ProjectRubric.objects.get_or_create(project=project, journal_category=category, journal_rubric=rubric)
        # UDC может быть указан пользователем и не ссылаться на запись на journalrub
        category = self.fields['udc'].queryset[0].categorization
        rubrics = self.cleaned_data['udc']
        real_rubrics = [r for r in rubrics if isinstance(r, JournalRubric)]
        ProjectRubric.objects.filter(project=project, journal_category=category).exclude(journal_rubric__in=real_rubrics).delete()
        ProjectRubric.objects.filter(project=project, journal_category=category, journal_rubric__isnull=True).delete()
        for rubric in rubrics:
            if isinstance(rubric, JournalRubric):
                ProjectRubric.objects.get_or_create(project=project, journal_category=category, journal_rubric=rubric)
            else:
                ProjectRubric.objects.create(project=project, journal_category=category, alternate_name=rubric)
        for rubric in self.cleaned_data['udc_old'].split(";"):
            ProjectRubric.objects.create(project=project, journal_category=category, alternate_name=rubric)
        return project

class ProjectStepForm(MyModelForm):
    css_classes = [('name', 'wide'),('head_members', 'wide'),('common_members', 'wide'), ]
    head_members = Select2WorkerField(label=u'Руководители этапа НИР', initial='')
    common_members = Select2WorkerField(label=u'Участники этапа НИР', required=False, initial='')
    fields_order = ['number','name','head_members', 'common_members','startdate','enddate']

    class Meta:
        model = ProjectStep
        fields = ['number','name','head_members', 'common_members','startdate','enddate']
        #exclude = ('project','members','stepresult',)

    def __init__(self, *args, **kwargs):
        super(ProjectStepForm, self).__init__(*args, **kwargs)
        projectstep = self.instance
        if projectstep.id:
            self.initial['head_members'] = projectstep.head_members
            self.initial['common_members'] = projectstep.common_members
        for field in self.fields:
            self.fields[field].widget.attrs['onchange'] = "edit_field=1"
        if projectstep.show_edit:
            self.fields['head_members'].widget.attrs['readonly'] = True

    def clean(self):
        try:
            if self.cleaned_data['startdate'] > self.cleaned_data['enddate']:
                raise forms.ValidationError(u"Дата начала этапа НИР должна предшествовать дате окончания.")
        except KeyError:
            pass
        return super(ProjectStepForm, self).clean()

    def has_changed(self):
        _has_changed = super(ProjectStepForm, self).has_changed()
        raw_data = {}
        for name, field in self.fields.items():
                prefixed_name = self.add_prefix(name)
                data_value = field.widget.value_from_datadict(self.data, self.files, prefixed_name)
                raw_data[prefixed_name] = data_value

        if not _has_changed and any(raw_data.itervalues()):
            return True
        return _has_changed

    def save(self, request, object, commit=True):
        if "DELETE" in self.changed_data:
            step = self.instance
            if step.id is None:
                return None
            messages.success(request, u"Этап %s удален." % step.number )
            step.delete()
            self._changed_data=None
            return step

        if len(self.changed_data)>0:
            mansql="""insert into projstepman (f_man_id,f_project_id,f_projstepman_user,f_projstepman_head,f_projstep_id,f_projstepman_name) values (%s,%s,%s,%s,%s,'%s')"""
            edit = True
            step = self.instance
            change_common=False
            if not step.id or 'head_members' in self._changed_data or 'common_members' in self._changed_data:
                head_members = self.get_new_member_ids('head_members')
                common_members_before = self.get_new_member_ids('common_members')
                common_members = [x for x in common_members_before if x not in head_members]
                if len(common_members_before) != len(common_members):
                    change_common=True
                    messages.info(request, u'При добавлении работы необязательно указывать одного человека и как руководителя, '
                                           u'и как участника. На странице НИР он будет указан '
                                           u'просто как руководитель.')
            if not step.id:
                values = self.cleaned_data
                step=ProjectStep.objects.create(project=object, number=values['number'], name=values['name'], startdate=values['startdate'], enddate=values['enddate'])
                cursor = connection.cursor()
                for x in head_members:
                    try:
                        currentmember = Worker.objects.get(pk=x)
                        #ProjectMembership.objects.create(member=currentmember,is_head=1,project=object,project_step=step,original_name=currentmember.fullname_short,creator=request.user)
                        sql=mansql % (x,object.id,request.user.id,1,step.id,currentmember.fullname_short)
                        cursor.execute(sql)
                    except:
                        pass
                for x in common_members:
                    try:
                        currentmember = Worker.objects.get(pk=x)
                        #ProjectMembership.objects.create(member=currentmember,is_head=0,project=object,project_step=step,original_name=currentmember.fullname_short,creator=request.user)
                        sql=mansql % (x,object.id,request.user.id,0,step.id,currentmember.fullname_short)
                        cursor.execute(sql)
                    except:
                        pass
                cursor.close()
                edit = False
            else:
                cursor = connection.cursor()
                name = 'head_members'
                if name in self._changed_data:
                    try:
                        self._changed_data.remove(name)
                    except:
                        pass
                    old_member_ids = self.get_old_member_ids(name)
                    add_member = [x for x in head_members if x not in old_member_ids]
                    del_member = [x for x in old_member_ids if x not in head_members]
                    if len(del_member) > 0:
                        ProjectMembership.objects.filter(project_step=step,is_head=1).filter(member__in=del_member).delete()
                    for x in add_member:
                        try:
                            currentmember = Worker.objects.get(pk=x)
                            #ProjectMembership.objects.create(member=currentmember,is_head=1,project=object,project_step=step,original_name=currentmember.fullname_short,creator=request.user)
                            sql=mansql % (x,object.id,request.user.id,1,step.id,currentmember.fullname_short)
                            cursor.execute(sql)
                        except:
                            pass

                name = 'common_members'
                if change_common or name in self._changed_data:
                    try:
                        self._changed_data.remove(name)
                    except:
                        pass
                    old_member_ids = self.get_old_member_ids(name)
                    add_member = [x for x in common_members if x not in old_member_ids]
                    del_member = [x for x in old_member_ids if x not in common_members]
                    if len(del_member) > 0:
                        ProjectMembership.objects.filter(project_step=step,is_head=0).filter(member__in=del_member).delete()
                    for x in add_member:
                        try:
                            currentmember = Worker.objects.get(pk=x)
                            #ProjectMembership.objects.create(member=currentmember,is_head=0,project=object,project_step=step,original_name=currentmember.fullname_short,creator=request.user)
                            sql=mansql % (x,object.id,request.user.id,0,step.id,currentmember.fullname_short)
                            cursor.execute(sql)
                        except:
                            pass
                cursor.close()

            if edit and len(self.changed_data)>0:
                ProjectStep.objects.filter(id=step.id).update(project=object, number=step.number, name=step.name, startdate=step.startdate, enddate=step.enddate)

            self._changed_data=None
            if edit:
                messages.success(request, u"Этап %s отредактирован." % self.instance.number )
            else:
                messages.success(request, u"Этап %s добавлен." % self.instance.number )

    def get_new_member_ids(self,name):
        prefixed_name = self.add_prefix(name)
        new_ids = self.fields[name].widget.value_from_datadict(self.data, self.files, prefixed_name)
        return new_ids

    def get_old_member_ids(self,name):
        old_ids=[]
        initial_value = self.initial[name]
        for member in initial_value:
            old_ids.append(unicode(member.id))
        return old_ids

    def _get_changed_data(self):
        if self._changed_data is None:
            self._changed_data = []
            for name, field in self.fields.items():
                prefixed_name = self.add_prefix(name)
                data_value = field.widget.value_from_datadict(self.data, self.files, prefixed_name)
                initial_value = self.initial.get(name, field.initial)
                if not initial_value:
                    if data_value:
                        self._changed_data.append(name)
                    continue
                if name =='head_members' or name == 'common_members' :
                    members_init=[]
                    for member in initial_value:
                        members_init.append(unicode(member.id))
                    members_init.sort()
                    data_value.sort()
                    if members_init<>data_value:
                        self._changed_data.append(name)
                else:
                    if field.widget._has_changed(initial_value, data_value):
                        self._changed_data.append(name)
        return self._changed_data
    changed_data = property(_get_changed_data)

class ProjectStepBaseFormset(MyBaseModelFormSet):
    def __init__(self, *args, **kwargs):
        project = kwargs['instance']
        add_step = None
        try:
            add_step = project.add_new_step
        except:
            pass
        self.no_extra_button=True
        self.extra = 1
        if add_step <> '1':
            self.extra = 0
        super(ProjectStepBaseFormset, self).__init__(*args, **kwargs)
        old_number = None
        old_head = None
        old_common = None
        for form in self.forms:
            if form.instance.id:
                old_number = form.instance.number
                old_head = form.instance.head_members
                old_common = form.instance.common_members
            else:
                if old_number:
                    form.initial['number'] = old_number + 1
                else:
                    form.initial['number'] = 1
                form.initial['head_members'] = old_head
                form.initial['common_members'] = old_common

    def clean(self):
        try:
            forms_cleaned_data = [form.cleaned_data for form in self.forms]
        except AttributeError:
            pass
        return super(ProjectStepBaseFormset, self).clean()

    def save(self, request, object, *args, **kwargs):
        found = False
        err = False
        delete_flag = []
        object.add_new_step = request.POST.get('add_new_step', 0)
        for form in self.forms:
            if not form.is_valid():
                if "DELETE" not in form.changed_data:
                    err=True
            if "DELETE" in form.changed_data:
                delete_flag.append(True)
            else:
                delete_flag.append(False)
        if all(delete_flag):
            err=True
        if not err:
            for form in self.forms:
                form.request = request
                if len(form.changed_data)>0:
                    form.save(request, object)
                    found = True
        if not found and object.add_new_step<>'1':
            messages.success(request, u"Данные не были изменены.")
        if object.add_new_step == '1':
            messages.success(request, u"Добавлен новый этап. Пожалуйста, откорректируйте и сохраните информацию.")
        if all(delete_flag):
            messages.error(request, u"Невозможно удалить все этапы.")

ProjectStepFormset = inlineformset_factory(Project, ProjectStep, formset=ProjectStepBaseFormset, form=ProjectStepForm,extra=1) #, can_delete=False)

class ProjectItogForm(MyModelForm):
    css_classes = [('name', 'wide')]

    class Meta:
        model = ProjectStep
        exclude = ('project','members',)

    def __init__(self, *args, **kwargs):
        super(ProjectItogForm, self).__init__(*args, **kwargs)
        if self.fields['number']:
            self.fields['number'].widget.attrs['disabled'] = True
        if self.fields['name']:
            self.fields['name'].widget.attrs['disabled'] = True
        if self.fields['startdate']:
            self.fields['startdate'].widget.attrs['disabled'] = True
        if self.fields['enddate']:
            self.fields['enddate'].widget.attrs['disabled'] = True

    def has_changed(self):
        _has_changed = super(ProjectItogForm, self).has_changed()
        raw_data = {}
        for name, field in self.fields.items():
                prefixed_name = self.add_prefix(name)
                data_value = field.widget.value_from_datadict(self.data, self.files, prefixed_name)
                raw_data[prefixed_name] = data_value

        if not _has_changed and any(raw_data.itervalues()):
            return True
        return _has_changed

    def save(self, commit=True):
        return super(ProjectItogForm, self).save(self.request, commit)


class ProjectItogBaseFormset(MyBaseModelFormSet):
    def __init__(self, *args, **kwargs):
        project = kwargs['instance']
        super(ProjectItogBaseFormset, self).__init__(*args, **kwargs)

    def clean(self):
        try:
            forms_cleaned_data = [form.cleaned_data for form in self.forms]
        except AttributeError:
            pass
        else:
            if all(cleaned_data.get("DELETE", True) for cleaned_data in forms_cleaned_data):
                raise forms.ValidationError(u"Укажите хотя бы один этап проекта.")
        return super(ProjectItogBaseFormset, self).clean()

ProjectItogFormset = inlineformset_factory(Project, ProjectStep, formset=ProjectItogBaseFormset, form=ProjectItogForm, extra=0, can_delete=False)


class ProjectAnnotationalReportForm(MyModelForm):

    class Meta:
        model = ProjectAnnotationalReport
        exclude = ('year', 'project', 'status', 'date_created', 'creator')

    def __init__(self, *args, **kwargs):
        super(ProjectAnnotationalReportForm, self).__init__(*args, **kwargs)
        self.fields['results'].widget.attrs['class'] = 'results'
        self.fields['results'].label = ""
