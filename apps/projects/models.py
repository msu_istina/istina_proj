# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models,connection
from django.db.models import Q
from django.core.validators import MaxLengthValidator
import datetime

from common.models import LinkedToWorkersModel, WorkershipModel, MyModel, Country, Activity, STATUS_CHOICES
from common.utils.dates import get_nearest_year
from workers.models import Worker
from projects.managers import ProjectManager
from journals.models import JournalRubric
from unified_permissions import has_permission

PSTATUS_ADMINS=['safonin', 'S.Y.Egorov','Reznikova_Alexandra','SvetaZyk','I.Sukhareva', 'smamakina', 'suminaistina', 'Astapov']

class Project(LinkedToWorkersModel, Activity):
    '''A Project.'''
    id = models.AutoField(primary_key=True, db_column="F_PROJECT_ID")
    name = models.CharField(u"Название НИР", max_length=1000, db_column="F_PROJECT_NAME")
    members = models.ManyToManyField("workers.Worker", verbose_name=u'Участники', related_name="projects", through="ProjectMembership")
    articles = models.ManyToManyField("publications.Article", verbose_name=u'Статьи', related_name="projects", through="ProjectArticle")
    books = models.ManyToManyField("publications.Book", verbose_name=u'Книги', related_name="projects", through="ProjectBook")
    dissertations = models.ManyToManyField("dissertations.Dissertation", verbose_name=u'Диссертации', related_name="projects", through="ProjectDissertation")
    organization = models.ForeignKey(to="organizations.Organization", related_name="projects", db_column="F_ORGANIZATION_ID", verbose_name=u"Организация", blank=True, null=True)
    department = models.ForeignKey(to="organizations.Department", related_name='projects', db_column="F_DEPARTMENT_ID", verbose_name=u'Подразделение', blank=True, null=True)
    startdate = models.DateField(u"Дата начала НИР", db_column="F_PROJECT_BEGIN", blank=False, null=True)
    enddate = models.DateField(u"Дата окончания НИР", db_column="F_PROJECT_END", blank=False, null=True)
    description = models.TextField(u"Описание НИР", db_column="F_PROJECT_ABSTRACT", blank=True)
    main_results = models.TextField(u"Итоговые результаты", db_column="F_PROJECT_MAINRESULT", blank=True)
    number = models.CharField(u"Номер договора (контракта, соглашения)", max_length=255, db_column="F_PROJECT_NUMBER")
    registration_number = models.CharField(u"Номер ЦИТИС", max_length=255, db_column="F_PROJECT_STNUMB", blank=True, null=True)
    contacts = models.CharField(u'Контакты', max_length=1000, db_column='F_PROJECT_CONTACT')
    outorganization = models.CharField(u"Сторонняя организация", max_length=255, db_column="f_project_outorganization", blank=True, null=True)
    pnr = models.ForeignKey(to='PNR', verbose_name=u'Приоритетное направление научных исследований', related_name='projects', null=True, blank=True, db_column='F_PNR_ID', )
    xml = models.TextField(u"XML НИР", db_column="F_PROJECT_XML", blank=True)
    creator = models.ForeignKey(to=User, related_name="projects_added", db_column="F_PROJECT_USER", null=True, blank=True)

    objects = ProjectManager()

    search_attr = 'name'
    workers_attr = 'members'
    workerships_attr = 'memberships'
    workers_verbose_name_single = u"Участник НИР"
    workers_verbose_name_plural = u"Участники НИР"
    workers_required_error_msg = u"Укажите хотя бы одного участника НИР"
    _status = None
    _currentuser = None

    nominative_en = "project"
    genitive = u"НИР"
    genitive_plural_full = u"НИР"
    instrumental = u"НИР"
    locative = u"НИР"

    class Meta:
        db_table = "PROJECT"
        verbose_name = u"НИР"
        verbose_name_plural = u"НИРы"
        get_latest_by = "startdate"
        ordering = ('-startdate', '-enddate', 'name')

    def __unicode__(self):
        return self.name + ((' (' + self.organization.name + ')') if self.organization else '')

    def delete(self, using=None):
        cursor = connection.cursor()
        cursor.execute('delete from projstepman where f_project_id=%s' % (self.id))
        cursor.close()
        #ProjectMembership.objects.filter(project=self).delete()
        Estimate.objects.filter(project=self).delete()
        super(Project, self).delete(using)

    @property
    def title(self):
        return unicode(self)

    def get_xml(self):
        '''Disable xml serializaion for projects because for some unknown reasons usual xml generation can not be later
        decoded to unicode. fixme-greg
        '''

        return ''

    @models.permalink
    def get_absolute_url(self):
        return ('projects_detail', (), {'object_id': self.id})

    @property
    def allmembers(self):
        all_memberships = ProjectMembership.objects.filter(project=self)
        return Worker.objects.filter(project_memberships__in=all_memberships).distinct()

    @property
    def head_members(self):
        head_memberships = ProjectMembership.objects.filter(project=self,is_head=True)
        return Worker.objects.filter(project_memberships__in=head_memberships).distinct()
#        head_memberships = self.memberships.filter(is_head=True)
#        return Worker.objects.filter(project_memberships__in=head_memberships).distinct()

    @property
    def common_members(self):
        common_memberships = ProjectMembership.objects.filter(project=self,is_head=False)
        return Worker.objects.filter(project_memberships__in=common_memberships).distinct()
#        common_memberships = self.memberships.filter(is_head=False)
#        return Worker.objects.filter(project_memberships__in=common_memberships).distinct()

    @property
    def workers_list(self, workerships=None, initials=True):
        workers = []
        if workerships is None:
            workerships = self.workerships.all()
        workers_cache = {obj.id: obj for obj in self.workers.all().distinct()}
        if workerships:
            position_exists = hasattr(workerships[0], "position")
            for workership in workerships:
                try:
                    worker = workers_cache[workership.get_worker_id()]
                except KeyError:
                    # In most cases this means that the author isn't linked to any worker.
                    worker = workership.worker
                worker = {'worker': worker}
                if hasattr(workership, "original_name") and workership.original_name:
                    worker['name'] = workership.original_name
                elif worker['worker']:
                    worker['name'] = worker['worker'].get_fullname(initials=initials)
                else:
                    worker['name'] = ""
                if position_exists:
                    worker['position'] = workership.position
                if workers.count(worker)<1:
                    workers.append(worker)
        workers.sort()
        return workers

    @property
    def customer(self):
        if self.customers.exists():
            return self.customers.all()[0]
        else:
            return None

    def members_string(self, head_members=False):
        workerships = self.memberships.filter(is_head=head_members)
        return ", ".join(worker['name'] for worker in self._workers_list(workerships=workerships))

    def add_workers(self, workers, user=None):
        head_members = workers[0]
        common_members = workers[1]
        kwargs = {'startdate': self.startdate,
                  'enddate': self.enddate}
        super(Project, self).add_workers(common_members, user, **kwargs)
        super(Project, self).add_workers(head_members, user, is_head=True, **kwargs)

    def keywords_string(self):
        return ','.join(x.word for x in self.keywords.all())

    @property
    def scopus_rubrics(self):
        return self.rubrics.filter(journal_category__name='Scopus')

    @property
    def grnti_rubrics(self):
        return self.rubrics.filter(journal_category__name='GRNTI')

    def department_string(self):
        if self.department:
            return self.department
        return self.outorganization

    def get_status(self):
        return PStepStatus.objects.filter(Q(project = self)).values_list('statustype', flat=True)

    @property
    def show_edit(self):
        if not self._status:
            self._status = self.get_status()
        if 'SIGN' in self._status or 'CONFIRM' in self._status:
            return True
        return False

    @property
    def can_edit_estimates(self):
        if  not self._currentuser:
            return False
        user = self._currentuser
        if user.id:
            worker = user.get_profile().worker
            is_leader = self.head_members.filter(id=worker.id)
            if is_leader:
                return True
        if any(map(lambda p: has_permission(user, p, self.department), ["confirm_projects", "edit_all_estimates", "is_representative_for"])):
            return True
        if user == self.creator:
            return True
        return False

    def is_admin(self, user):
        if user.username in PSTATUS_ADMINS:
            return True
        return False

    def is_related_to(self, user):
        '''
            This is a custom access control function that is used only in projects views.
            It has nothing to do with is_linked_to, which is used in unauthor operation.
        '''
        if not user or not user.is_authenticated():
            return False
        if user.username in PSTATUS_ADMINS:
            return True
        return user in self.linked_users or self.is_created_by(user)

    def get_rubric_names(self, code):
        return u", ".join(self.rubrics.filter(journal_category__code=code).values_list('journal_rubric__name', flat=True))

    @property
    def pnr_russia(self):
        return self.get_rubric_names("PNR-RF")

    @property
    def character(self):
        return self.get_rubric_names("PROJ-TYPE")

    @property
    def grnti(self):
        return self.rubrics.filter(journal_category__code="GRNTI").values_list('journal_rubric__name', flat=True)

    @property
    def specialty(self):
        return self.get_rubric_names("GEN-SCI")

    @property
    def technological_leap(self):
        return self.get_rubric_names("TP-RF")

    @property
    def udc(self):
        codes = [u"%s %s" % (code, name) for (code, name) in self.rubrics.filter(journal_category__code="UDC", journal_rubric__isnull=False).values_list('journal_rubric__code', 'journal_rubric__name')]
        codes.extend( [u"%s (устаревший)" % code for code in self.rubrics.filter(journal_category__code="UDC", journal_rubric__isnull=True, alternate_name__isnull=False).values_list('alternate_name') ] )
        return codes

    @property
    def current_annotational_report(self):
        return self.annotational_reports.getdefault(year=get_nearest_year())

    def results_for_year(self, year):
        results = u""
        results += u"\n\n".join(self.steps.filter(enddate__year=year).values_list("stepresult", flat=True))
        if self.enddate and self.enddate.year == year:
            results += u"\n\n" + self.main_results
        return results

class ProjectMembership(WorkershipModel):
    id = models.AutoField(primary_key=True, db_column="f_projstepman_id")
    member = models.ForeignKey(to="workers.Worker", verbose_name=u'Участник', related_name="project_memberships", db_column="f_man_id", null=True, blank=True)
    is_head = models.BooleanField(u"Руководитель НИР", db_column="f_projstepman_head", blank=True)

    #project = models.ForeignKey(to="Project", verbose_name=u'НИР', db_column="f_project_id")
    project = models.ForeignKey(to="Project", verbose_name=u'НИР', related_name="memberships", db_column="f_project_id")
    project_step = models.ForeignKey(to='ProjectStep', verbose_name='Этап НИР', related_name="memberships", null=True, blank=True, db_column='f_projstep_id')
    original_name = models.CharField(max_length=255, db_column="f_projstepman_name", null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="project_memberships_added", db_column="f_projstepman_user", null=True, blank=True)
#    ktu = models.FloatField(u'Коэффициент трудового участия в НИР', null=True, blank=True, db_column="f_projstepman_ktu")
    ktu = models.DecimalField(u'Коэффициент трудового участия в НИР', max_digits=6, decimal_places=4, null=True, blank=True, db_column="f_projstepman_ktu")

    worker_attr = 'member'
    workers_verbose_name_single = u"Участник НИР"
    workers_required_error_msg = u"Укажите участника НИР"

    nominative_en = "project_membership"
    nominative_plural = u"участие в НИР"
    genitive = u"участия в НИР"
    genitive_plural = u"участия в НИР"
    genitive_plural_full = u"участий в НИР"
    instrumental = u"участием в НИР"
    locative = u"участии в НИР"
    gender = "neuter"

    class Meta:
        db_table = "PROJSTEPMAN"
        verbose_name = u"участие в НИР"
        verbose_name_plural = u"участие в НИР"
#        get_latest_by = "-startdate"
#        ordering = ('-startdate', '-enddate')

    def __unicode__(self):
#        return u"Участие в НИР %s, %s" % (unicode(self.project), self.workers_string)
        return u"%s участие в этапе НИР %s" % (self.original_name,self.project_step.number)

    @models.permalink
    def get_absolute_url(self):
        return ('projects_membership_detail', (), {'object_id': self.id})

    @property
    def title(self):
#        return u"%s в НИР %s" % (self.workers_string, self.project.name)
        return u"в НИР %s" % (self.project.name)


class ProjectStep(LinkedToWorkersModel):
    id = models.AutoField(primary_key=True, db_column="F_PROJSTEP_ID")
    project = models.ForeignKey(to="Project", verbose_name=u'НИР', related_name="steps", db_column="F_PROJECT_ID")
    number = models.BigIntegerField(u'Номер этапа НИР', db_column="F_PROJSTEP_NUM")
    name = models.CharField(u'Название этапа НИР', max_length=1000, db_column='F_PROJSTEP_NAME')
    startdate = models.DateField(u'Дата начала этапа НИР', db_column='F_PROJSTEP_BEGIN')
    enddate = models.DateField(u'Дата окончания этапа НИР', db_column='F_PROJSTEP_END')
    stepresult = models.TextField(u"Основные итоги этапа", db_column="F_PROJSTEP_MAINRESULT", blank=True, null=True)
    planresult = models.TextField(u"Ожидаемые результаты этапа", db_column="F_PROJSTEP_PLANRESULT", blank=True, null=True)
    members = models.ManyToManyField("workers.Worker", verbose_name=u'Участники', related_name="ProjectStep", through="ProjectMembership")

    nominative_en = "project_step"
    genitive = u"этапа НИР"
    genitive_plural_full = u" этапов НИР"
    instrumental = u"этапом НИР"
    locative = u"этапе НИР"
    workers_attr = 'members'
    workerships_attr = 'memberships'
    workers_verbose_name_single = u"Участник НИР"
    workers_verbose_name_plural = u"Участники НИР"
    workers_required_error_msg = u"Укажите хотя бы одного участника НИР"
    _status = None
    _status_date = None
    _status_comment = None
    _status_creator = None
    _currentuser = None

    class Meta:
        db_table = "PROJSTEP"
        verbose_name = u"этап НИР"
        verbose_name_plural = u"этапы НИР"
        get_latest_by = "startdate"
        ordering = ('project', 'number')

    def __unicode__(self):
        return u"Этап %s НИР" % self.number

    @property
    def estimates_ordered(self):
        return self.estimates.order_by('category__order')

    def delete(self, using=None):
        cursor = connection.cursor()
        cursor.execute('delete from projstepman where f_projstep_id=%s' % (self.id))
        cursor.close()
        #ProjectMembership.objects.filter(project_step=self).delete()
        Estimate.objects.filter(project_step=self).delete()
        super(ProjectStep, self).delete(using)

    @property
    def head_members(self):
        head_memberships = self.memberships.filter(is_head=True)
        return Worker.objects.filter(project_memberships__in=head_memberships)

    @property
    def common_members(self):
        common_memberships = self.memberships.filter(is_head=False)
        return Worker.objects.filter(project_memberships__in=common_memberships)

    @property
    def ktu_members(self):
        return u'%s из %s' % (self.memberships.filter(ktu__isnull=False).count(),self.memberships.all().count())

    @property
    def plan_counter(self):
        return u'%s из %s' % (PStepPlan.objects.filter(projstep=self.id).count(),PlanType.objects.all().count())

    @property
    def is_stepresult(self):
        if len(self.stepresult) > 0:
            return True
        return False

    @property
    def is_planresult(self):
        if len(self.planresult) > 0:
            return True
        return False

    def get_status(self):
        sts = PStepStatus.objects.filter(projstep = self)
        if sts.exists():
            # check that SIGN was the last event
            ps = sts[0]
            self._status_date = ps.date
            self._status_creator = ps.creator
            self._status_comment = ps.comment
            self._creator_profile = ps.creator.get_profile().worker.fullname
            return ps.statustype
        return 'EDIT'

    @property
    def show_sign(self):
        if not self._status:
            self._status = self.get_status()
        if self._status == 'EDIT' or self._status == 'REEDIT':
            return True
        return False

    @property
    def show_edit(self):
        if not self._status:
            self._status = self.get_status()
        if self._status == 'SIGN' or self._status == 'CONFIRM':
            return True
        return False

    @property
    def show_confirm(self):
        if self._status is None:
            self._status = self.get_status()
        if self._status == 'SIGN':
            return True
        return False

    @property
    def get_sign_info(self):
        if not self._status:
            self._status = self.get_status()
        if self._status == 'EDIT':
            return u"Этап %s не подписан" % self.number
        elif self._status == 'REEDIT':
            return u"Этап %s возвращен на доработку: %s %s: %s" % (self.number,self._status_date.strftime("%d.%m.%Y %H:%M"), self._creator_profile, self._status_comment)
        elif self._status == 'SIGN':
            return u"Этап %s подписан" % self.number
        elif self._status == 'CONFIRM':
            return u"Этап %s утвержден" % self.number
        return u""

    def setstatus(self, statustype, user, comment=''):
        pst = PStepStatus(projstep = self, project=self.project, creator=user, statustype=statustype, comment=comment, date=datetime.datetime.now().replace(microsecond=0))
        pst.save()
        return True

class ProjectArticle(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_PROJARTICLE_ID")
    project = models.ForeignKey(to="Project", verbose_name=u'НИР', related_name="project_articles", db_column="F_PROJECT_ID")
    article = models.ForeignKey(to="publications.Article", verbose_name=u'Статья', related_name="project_articles", db_column="F_ARTICLE_ID")
    creator = models.ForeignKey(to="auth.User", related_name="project_articles_added", db_column="F_PROJARTICLE_USER", null=True, blank=True)

    nominative_en = "project_article"
    genitive = u"статьи по НИР"
    instrumental = u"статьей по НИР"
    locative = u"статье по НИР"

    class Meta:
        db_table = u'PROJARTICLE'
        verbose_name = u"статья по НИР"
        verbose_name_plural = u"статьи по НИР"


class ProjectBook(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_PROJBOOK_ID")
    project = models.ForeignKey(to="Project", verbose_name=u'НИР', related_name="project_books", db_column="F_PROJECT_ID")
    book = models.ForeignKey(to="publications.Book", verbose_name=u'Книга', related_name="project_books", db_column="F_BOOK_ID")
    creator = models.ForeignKey(to="auth.User", related_name="project_books_added", db_column="F_PROJBOOK_USER", null=True, blank=True)

    nominative_en = "project_book"
    genitive = u"книги по НИР"
    instrumental = u"книгой по НИР"
    locative = u"книге по НИР"

    class Meta:
        db_table = u'PROJBOOK'
        verbose_name = u"книга по НИР"
        verbose_name_plural = u"книги по НИР"


class ProjectDissertation(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_PROJDISSER_ID")
    project = models.ForeignKey(to="Project", verbose_name=u'НИР', related_name="project_dissertations", db_column="F_PROJECT_ID")
    dissertation = models.ForeignKey(to="dissertations.Dissertation", verbose_name=u'Диссертация', related_name="project_dissertations", db_column="F_DISSER_ID")
    creator = models.ForeignKey(to="auth.User", related_name="project_dissertations_added", db_column="F_PROJDISSER_USER", null=True, blank=True)

    nominative_en = "project_dissertation"
    genitive = u"диссертации по НИР"
    instrumental = u"диссертацией по НИР"
    locative = u"диссертации по НИР"

    class Meta:
        db_table = u'PROJDISSER'
        verbose_name = u"диссертация по НИР"
        verbose_name_plural = u"диссертации по НИР"


class ProjectKeyword(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_PROJWORD_ID")
    project = models.ForeignKey(to="Project", verbose_name=u'НИР', related_name="keywords", db_column="F_PROJECT_ID")
    word = models.CharField(u'Ключевое слово', max_length=1020, db_column="F_PROJWORD_WORD")

    class Meta:
        db_table = u'PROJWORD'
        verbose_name = u"Ключевое слово НИР"
        verbose_name_plural = u"Ключевые слова НИР"

    def __unicode__(self):
        return self.word


class SponsorCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SPONSORTYPE_ID")
    name = models.CharField(u'Название', max_length=1000, db_column="F_SPONSORTYPE_NAME")
    sponsor_has_own_name = models.BooleanField(u'Спонсоры имеют свое название', db_column='F_SPONSORTYPE_NAMEABLE')

    class Meta:
        db_table = u'SPONSORTYPE'
        verbose_name = u'Тип источника финансирования НИР'
        verbose_name_plural = u'Типы источников финансирования НИР'

    def __unicode__(self):
        return self.name


class Sponsor(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SPONSOR_ID")
    category = models.ForeignKey(to='SponsorCategory', verbose_name=u'Источник финансирования', related_name="sponsors", null=True, blank=True, db_column="F_SPONSORTYPE_ID")
    project = models.ForeignKey(to="Project", verbose_name=u'НИР', related_name="sponsors", db_column="F_PROJECT_ID")
    sponsor_category_name = models.CharField(u'Название  источника', max_length=1000, blank=True, db_column='F_SPONSOR_NAMETYPE')
    name = models.CharField(u'Название программы', max_length=1000, blank=True, db_column='F_SPONSOR_NAME')

    class Meta:
        db_table = u'SPONSOR'
        verbose_name = u'Источник финансирования'
        verbose_name_plural = u'Источники финансирования'

    def __unicode__(self):
        if self.category:
            title = self.category.name
        else:
            title = self.sponsor_category_name
        if self.name:
            title += u', %s' % self.name
        return title


class EstimateCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_ESTIMATETYPE_ID")
    name = models.CharField(u'Название', max_length=255, db_column='F_ESTIMATETYPE_NAME')
    comments = models.CharField(u'Пояснение', max_length=1000, blank=True, db_column='F_ESTIMATETYPE_COMMENTS')
    order = models.BigIntegerField(u'Порядковый номер', null=True, blank=True, db_column='F_ESTIMATETYPE_ORD')

    class Meta:
        db_table = u'ESTIMATETYPE'
        verbose_name = u'Статья сметы'
        verbose_name_plural = u'Статьи сметы'

    def __unicode__(self):
        return self.name


class Estimate(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_ESTIMATE_ID')
    category = models.ForeignKey(EstimateCategory, verbose_name=u'Статья сметы', related_name='estimates', db_column='F_ESTIMATETYPE_ID')
    project = models.ForeignKey(Project, verbose_name=u'НИР', related_name='estimates', db_column="F_PROJECT_ID")
    value = models.DecimalField(u'Значение', max_digits=30, decimal_places=3, db_column='F_ESTIMATE_VALUE')
    project_step = models.ForeignKey(ProjectStep, verbose_name='Этап НИР', related_name='estimates', null=True, blank=True, db_column='F_PROJSTEP_ID')

    class Meta:
        db_table = u'ESTIMATE'
        verbose_name = u'Смета НИР'
        verbose_name_plural = u'Сметы НИР'

    def __unicode__(self):
        return u"%s : %s, НИР %s" % (self.category, self.value, self.project)


class ProjectRubric(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_PROJECTRUB_ID')
    journal_rubric = models.ForeignKey(to='journals.JournalRubric', verbose_name=u'Рубрика журнала',
                                       related_name=u'project_rubrics', db_column='F_JOURNALRUB_ID', null=True, blank=True)
    journal_category = models.ForeignKey(to='journals.JournalCategorization', verbose_name=u'Классификатор журнала',
                                         db_column='F_JOURNALRUBTYPE_ID')
    project = models.ForeignKey(Project, verbose_name=u'НИР', related_name='rubrics', db_column="F_PROJECT_ID")
    alternate_name = models.CharField(u'Альтернативное название', max_length=255, blank=True, db_column=u'F_PROJECTRUB_NAME')

    class Meta:
        db_table = u'PROJECTRUB'
        verbose_name = u'Рубрика НИР'
        verbose_name_plural = u'Рубрики НИР'

    def __unicode__(self):
        if self.alternate_name:
            name = self.alternate_name
        else:
            name = unicode(self.journal_rubric)
        return u'%s, рубрика %s ' % (self.project.name, name)


class AccompliceRole(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_ACCOMPLICEROLE_ID')
    name = models.CharField(u'Название', max_length=255, db_column='F_ACCOMPLICEROLE_NAME')

    class Meta:
        db_table = u'ACCOMPLICEROLE'
        verbose_name = u'Роль соисполнителя'
        verbose_name_plural = u'Роли соисполнителя'

    def __unicode__(self):
        return self.name


class AccompliceCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_ACCOMPLICETYPE_ID')
    name = models.CharField(u'Название', max_length=255, db_column='F_ACCOMPLICETYPE_NAME')

    class Meta:
        db_table = u'ACCOMPLICETYPE'
        verbose_name = u'Тип соисполнителя'
        verbose_name_plural = u'Типы соисполнителя'

    def __unicode__(self):
        return self.name


class Accomplice(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_ACCOMPLICE_ID')
    country = models.ForeignKey(Country, verbose_name=u'Страна', related_name='accomplices', db_column='F_COUNTRY_ID')
    role = models.ForeignKey(AccompliceRole, verbose_name=u'Роль', related_name='accomplices', db_column='F_ACCOMPLICEROLE_ID')
    category = models.ForeignKey(AccompliceCategory, verbose_name=u'Тип', related_name='accomplices', db_column='F_ACCOMPLICETYPE_ID')
    project = models.ForeignKey(Project, verbose_name=u'НИР', related_name='accomplices', db_column="F_PROJECT_ID")
    project_step = models.ForeignKey(to='ProjectStep', verbose_name='Этап НИР', null=True, blank=True, db_column='F_PROJSTEP_ID')
    full_name = models.CharField(u'Полное название', max_length=1000, db_column="F_ACCOMPLICE_FULLNAME")
    name = models.CharField(u'Название', max_length=255, db_column='F_ACCOMPLICE_NAME')

    def __unicode__(self):
        return u'%s НИР %s, %s' % (self.role, self.project, self.country)

    class Meta:
        db_table = u'ACCOMPLICE'
        verbose_name = u'Соисполнитель'
        verbose_name_plural = u'Соисполнители'


class Customer(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_CUSTOMER_ID')
    project = models.ForeignKey(Project, verbose_name=u'НИР', related_name='customers', db_column="F_PROJECT_ID")
    name = models.CharField(u'Название', max_length=1000, db_column='F_CUSTOMER_NAME')

    class Meta:
        db_table = u'CUSTOMER'
        verbose_name = u'Заказчик'
        verbose_name_plural = u'Заказчики'

    def __unicode__(self):
        return self.name


class PNR(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_PNR_ID')
    name = models.CharField(max_length=4000, verbose_name=u'Название', db_column='F_PNR_NAME')
    department = models.ForeignKey(to='organizations.Department', verbose_name=u'Подразделение', related_name='pnrs', null=True, blank=True, db_column='F_DEPARTMENT_ID')
    parent = models.ForeignKey('self', verbose_name=u'Родительский ПННИ', db_column='PNR_F_PNR_ID', related_name='childs', null=True, blank=True)
    actual_date = models.DateField(u'Окончание срока ПННИ', db_column='F_PNR_ACTUALDATE', null=True, blank=True)
    created = models.DateField(u'Дата создания', db_column='F_PNR_CREATE', auto_now_add=True)
    creator = models.ForeignKey(to=User, related_name="pnr_added", db_column="F_PNR_USER", null=True, blank=True)
    search_attr = 'name'

    class Meta:
        db_table = u'PNR'
        verbose_name = u'Приоритетное направление научных исследований подразделения'
        verbose_name_plural = u'Приоритетные направления научных исследований подразделения'

    def __unicode__(self):
        return self.name


class PNRRubricMembership(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_PNRRUBLINK_ID')
    pnr = models.ForeignKey(PNR, related_name='rubrics', verbose_name=u'ПННИ', null=True, blank=True, db_column='F_PNR_ID')
    journal_rubric = models.ForeignKey('journals.JournalRubric', verbose_name=u'Рубрика', null=True, blank=True, db_column='F_JOURNALRUB_ID')

    class Meta:
        db_table = u'PNRRUBLINK'
        verbose_name = u"Принадлежность ПННИ рубрике"
        verbose_name_plural = u"Принадлежности ПННИ рубрикам"

    def __unicode__(self):
        return u'%s, рубрика %s' % (unicode(self.pnr), unicode(self.journal_rubric))

class PNRPlan(MyModel):
    id = models.AutoField(primary_key=True, db_column='f_pnr_plan_id')
    pnr = models.ForeignKey(PNR, related_name='pnrplan', verbose_name=u'ПННИ', null=True, blank=True, db_column='f_pnr_id')
    planYear = models.IntegerField(u'Плановые показатели на период (год)', db_column="f_pnr_plan_year")
    totalTopics = models.IntegerField(u'Число тем всего', db_column="f_pnr_plan_total_topics", null=True, blank=True)
    gosTopics = models.IntegerField(u'в т.ч. по госзаданию,ч.2', db_column="f_pnr_plan_gos_topics", null=True, blank=True)
    nsStaffer = models.IntegerField(u'Научные сотрудники штат', db_column="f_pnr_plan_ns_staffer", null=True, blank=True)
    nsSovmestit = models.IntegerField(u'Научные сотрудники совместители', db_column="f_pnr_plan_ns_sovmestit", null=True, blank=True)
    nsNotBudget = models.IntegerField(u'Научные сотрудники внебюджетные', db_column="f_pnr_plan_ns_not_budget", null=True, blank=True)
    supportStaffer = models.IntegerField(u'Научно-вспомогательный персонал штат', db_column="f_pnr_plan_support_staffer", null=True, blank=True)
    supportSovmestit = models.IntegerField(u'Научно-вспомогательный персонал совместители', db_column="f_pnr_plan_support_sovmestit", null=True, blank=True)
    supportNotBudget = models.IntegerField(u'Научно-вспомогательный персонал внебюджетные', db_column="f_pnr_plan_support_not_budget", null=True, blank=True)
    ppsStaffer = models.IntegerField(u'Профессорско-преподавательский состав штат', db_column="f_pnr_plan_pps_staffer", null=True, blank=True)
    ppsSovmestit = models.IntegerField(u'Профессорско-преподавательский состав совместители', db_column="f_pnr_plan_pps_sovmestit", null=True, blank=True)
    ppsNotBudget = models.IntegerField(u'Профессорско-преподавательский состав внебюджетные', db_column="f_pnr_plan_pps_not_budget", null=True, blank=True)

    class Meta:
        db_table = u'pnr_plan'
        verbose_name = u"Планируемый показатель ПННИ"
        verbose_name_plural = u"Планируемые показатели ПННИ"

    def __unicode__(self):
        return u'План ПННИ %s на %s год' % (unicode(self.pnr), unicode(self.planYear))

class PStepStatus(models.Model):
    projstep = models.OneToOneField(ProjectStep, primary_key=True, db_column="f_projstep_id")
    project = models.ForeignKey(to=Project, related_name="pstatuses", db_column="f_project_id") #, verbose_name=u"Project")
    statustype = models.TextField(u"Статус", db_column="f_pstepstatus_status", null=True, blank=True)
    date = models.DateTimeField(u"Дата присвоения", db_column="f_pstepstatus_create", blank=True, null=True, auto_now_add=False)
    creator = models.ForeignKey(to=User, related_name="pstepstatuses", db_column="f_pstepstatus_user", null=False, blank=False)
    comment = models.TextField(u"Комментарий", validators=[MaxLengthValidator(1000)], db_column="f_pstepstatus_comment", blank=True, null=True)

    class Meta:
        db_table = "PSTEPSTATUS"

    def __unicode__(self):
        return u'%s' % unicode(self.statustype)

class ProjectStatus(models.Model):
    pass
    #id = models.AutoField(primary_key=True, db_column="f_pstatus_id")
    #creator = models.ForeignKey(to=User, related_name="pstatuses", db_column="F_PSTATUS_USER", null=False, blank=False)
    #project = models.ForeignKey(to=Project, related_name="statuses", db_column="f_project_id", verbose_name=u"Project")
    #statustype = models.TextField(u"Статус", db_column="f_pstatus_status", null=False, blank=False)
    #comment = models.TextField(u"Комментарий", validators=[MaxLengthValidator(1000)], db_column="F_PSTATUS_COMMENT", blank=True, null=True)
    #date = models.DateTimeField(u"Дата присвоения", db_column="f_pstatus_create", blank=True, null=True, auto_now_add=False)

    #class Meta:
        #db_table = "PSTATUS"

    #def __unicode__(self):
        #return u'%s' % unicode(self.statustype)

class ProjectAnnotationalReport(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_ANNREPORT_ID")
    project = models.ForeignKey(Project, related_name="annotational_reports", verbose_name=u"НИР", db_column="F_PROJECT_ID")
    year = models.PositiveSmallIntegerField(u"отчетный год", db_column="F_ANNREPORT_YEAR")
    results = models.TextField(u"результаты", db_column="F_ANNREPORT_RESULT")
    status = models.CharField(u"Статус", max_length=255, choices=STATUS_CHOICES, default="EDIT", db_column="F_ANNREPORT_STATUS", blank=True)
    date_created = models.DateField(u"дата формирования", auto_now_add=True, db_column="F_ANNREPORT_CREATE")
    creator = models.ForeignKey(to=User, related_name="annotational_reports_added", db_column="F_ANNREPORT_USER")

    nominative_en = "annotational_report"
    instrumental = u"аннотационным отчетом"
    accusative_short = u"отчет"

    class Meta:
        db_table = "ANNREPORT"
        verbose_name = u"аннотационный отчет по НИР"
        verbose_name_plural = u"аннотационные отчеты по НИР"
        unique_together = ('project', 'year')

    def __unicode__(self):
        return u"аннотационный отчет за %d год по проекту %s" % (self.year, self.project)

    @models.permalink
    def get_absolute_url(self):
        return ('projects_annotational_report', (), {'pk': self.id})

    @property
    def title(self):
        return self.project.title

    @property
    def sponsor(self):
        sponsor = self.project.sponsors.all().get_first_or_none()
        if sponsor:
            category = sponsor.category.name if sponsor.category else sponsor.sponsor_category_name
            name = sponsor.name
            return {'category': category, 'name': name}
        else:
            return {'category': "", 'name': ""}

    @property
    def estimates(self):
        if self.project.estimates.filter(project_step__isnull=False).exists():
            # if there are estimates linked to project steps
            estimates = self.project.estimates.filter(project_step__enddate__year=self.year)
        elif self.project.enddate and self.project.enddate.year == self.year:
            estimates = self.project.estimates.all()
        else:
            return {'total': '', 'own': ''}
        total = sum(estimates.filter(category__name=u'всего').values_list('value', flat=True))
        own = sum(estimates.filter(category__name=u'собственными силами').values_list('value', flat=True))
        return {'total': total, 'own': own}

    def sign(self):
        self.status = "SIGN"
        self.save()

    def unsign(self):
        self.status = "EDIT"
        self.save()

    @property
    def signed(self):
        return self.status in ["SIGN", "CONFIRM"]

    @property
    def single_head(self):
        return self.project.head_members[0].fullname_short if self.project.head_members.count() == 1 else None

class PlanType(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_pstepplantype_id")
    name = models.CharField(u'Название', max_length=255, db_column='f_pstepplantype_name')

    class Meta:
        db_table = 'pstepplantype'
        ordering = ('id',)

    def __unicode__(self):
        return self.name

class PStepPlan(models.Model):
    id = models.AutoField(primary_key=True, db_column="f_pstepplan_id")
    projstep = models.ForeignKey(to=ProjectStep, verbose_name='Этап НИР', related_name='stepplan', null=True, blank=True, db_column='f_projstep_id')
    type = models.ForeignKey(to=PlanType, related_name="pstepplantype", db_column="f_pstepplantype_id", null=False, blank=False)
    value = models.BigIntegerField(u"Количество", db_column="f_pstepplan_value", null=True, blank=True)
    date = models.DateTimeField(u"Дата присвоения", db_column="f_pstepplan_create", blank=True, null=True, auto_now_add=False)
    creator = models.ForeignKey(to=User, related_name="pstepplanuser", db_column="f_pstepplan_user", null=True, blank=True)

    class Meta:
        db_table = "pstepplan"

    def __unicode__(self):
        return u'План этапа %s' % unicode(self.projstep)

from projects.admin import *
