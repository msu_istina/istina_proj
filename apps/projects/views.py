# -*- coding: utf-8; -*-
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.forms.formsets import BaseFormSet
from django.contrib.contenttypes.models import ContentType
from django.views.generic.edit import CreateView, UpdateView, ModelFormMixin
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.db.models import Q
from django.db import connection
import datetime
import logging
from actstream import action

from common.forms import make_attachment_formset
from common.models import Attachment
from common.utils.context import render_to_response
from common.utils.user import get_worker
from common.views import LinkedToWorkersModelWizardView, detail
#from common.views import MyModelWizardView
from projects.common_views import MyModelWizardView
from projects.models import Project, ProjectArticle, ProjectBook, ProjectDissertation, ProjectStep, ProjectAnnotationalReport, PStepStatus, ProjectMembership, PStepPlan, PlanType
from projects.forms import ProjectArticleForm, ProjectBookForm, ProjectDissertationForm, \
    ProjectWizardForm, AccompliceFormset, EstimateFormset, SponsorFormset,\
    ProjectRubForm, ProjectAdditionalForm, \
    ProjectWizardFormShort, ProjectStepFormset, ProjectItogFormset, \
    ProjectAnnotationalReportForm
from common.utils.latex import latex_to_pdf
from django.template.loader import render_to_string
from django.http import HttpResponse
from unified_permissions.decorators import permission_required
from unified_permissions import has_permission
import re

from projects import hidden_projects

logger = logging.getLogger("projects.views")


class ProjectWizardView(MyModelWizardView):
    '''
    Wizard, that contains main logic for projects adding. About wizard concept see https://docs.djangoproject.com/en/dev/ref/contrib/formtools/form-wizard/,
    about details of out implementation ask Denis. The main difference from other wizards we use is that we save data on
    every step instead os saving it all at once on the last step.
    '''
    # List of the forms, that will be shown to user.
    # First element in tuple is a name of the wizard, that will be used eveywhere in code.
    form_list = (
        (MyModelWizardView.main_step, ProjectWizardForm),
        ('project_steps', ProjectStepFormset),
        ('accomplices', AccompliceFormset),
        ('sponsors', SponsorFormset),
        ('rubrics', ProjectRubForm),
        ('additional', ProjectAdditionalForm),
        ('estimates', EstimateFormset),
    )

    # We can specify additional info for each step in  this dict.
    # Key is the name of the step, and value is dict with options.
    #
    steps_info = {
        MyModelWizardView.main_step:
        {
            'title': u'Основная информация',
            # lambda used to avoid call of reverse_lazy before urlconf is configured
            'help_text': lambda: [(u'Пожалуйста, перед добавлением НИР <a href="/help/add/project/" '
                                   u'target="_blank">прочтите инструкцию</a>. Список уже добавленных НИР можно'
                                   u' посмотреть <a href=%s> здесь </a>.') % reverse_lazy('projects_list')]
        },
#        'members': {'title': u'Участники НИР'},
        'project_steps': {'title': u'Этапы НИР'},
        'accomplices': {'title': u'Соисполнители и координаторы'},
        'sponsors': {'title': u'Источники финансирования'},
        'rubrics': {'title': u'Рубрики НИР'},
        'additional': {'title': u'Дополнительная информация'},
        'estimates':
        {
            'title': u'Смета НИР',
            'help_text': [u'Все суммы указываются в тысячах рублей.',
                          u'Смета проекта будет показываться только руководителям проекта и ответственным.']
        },
    }

    condition_dict = MyModelWizardView.make_condition_dict('estimates')
    workers_steps = ['heads_disambiguation', 'members_disambiguation']
    model = Project
    no_previous_forms = True # disable showing of previous forms. This option must be ported to all wizards, at the moment it works only in ProjectWizard.
    navigation_menu = True # enable navigation bar. This option must be ported to all wizards, at the moment it works only in ProjectWizard.

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return_dispatch = super(ProjectWizardView, self).dispatch(*args, **kwargs)
        if self.instance_dict.has_key('main'):
            project = self.instance_dict['main']
            user = args[0].__getattribute__('user')
            if project:
                project._currentuser = user
            if not project.can_edit_estimates:
                self.skip_steps('estimates')
        return return_dispatch

    def process_step(self, form):
        if self.steps.current == self.main_step:
            project = form.save(self.request)
            self.set_extra_data('department', form.cleaned_data['department'])
            self.set_extra_data('project_instance', project)
        else:
            project = self.get_extra_data('project_instance')
            # notice, that we store project only from first step,
            # because we don't need updated during wizard versions
            if self.steps.current in ('rubrics'):
                form.save(project)
            else:
                form.save(self.request, project)
        return super(ProjectWizardView, self).process_step(form)

    def get_form(self, step=None, data=None, files=None):
        # This code is added because of some complex bugs, that appeared when we started to save data on every step.
        # It's not right, but i don't know how to fix this.
        wizard_goto_step = self.request.POST.get('wizard_goto_step', None)
        if wizard_goto_step in ['accomplices', 'sponsors', 'project_steps']:
            data = None
        return super(ProjectWizardView, self).get_form(step, data, files)

    def get_form_kwargs(self, step=None):
        kwargs = super(ProjectWizardView, self).get_form_kwargs(step)
        project = self.get_extra_data('project_instance')
        user = self.request.user
        if project:
            project._currentuser = user
        if step in ('accomplices', 'estimates', 'sponsors', 'rubrics', 'additional', 'project_steps'):
            kwargs['instance'] = project
        if step == 'additional':
            #kwargs['department'] = self.get_extra_data('department')
            kwargs['department'] = project.department
        if step == 'main' and project:
            kwargs['instance'] = project
        if step == 'main':
            kwargs['user'] = user
        return kwargs

    def get_context_data(self, form, **kwargs):
        context = super(ProjectWizardView, self).get_context_data(form, **kwargs)
        # if current step form is formset, we don't want to show previous steps forms at all
        project = None
        if self.steps.current == 'main':
            context.pop('previous_forms')
        else:
            # unlike usual wizards, we don't show all previous forms on every step, only  one short form
            project=self.get_extra_data('project_instance')
            previous_forms = [ProjectWizardFormShort(instance=project)]
            context['previous_forms'] = previous_forms
        if self.steps.current == 'main' and self.edit:
            project = self.instance_dict['main']
            self.set_extra_data('project_instance', project)
        if project:
            context['project'] = project
        context['steps_info'] = self.steps_info
        context['navigation_menu'] = self.navigation_menu
        context['current_tab_step'] = self.get_step_index(self.steps.current)+1
        return context

    def get_template_names(self):
        if self.steps.current == 'rubrics':
            return 'projects/add_edit_rubs.html'
        else:
            return 'projects/project_edit.html'

    def render_done(self, form, **kwargs):
        done_response = self.done(None, **kwargs)
        self.storage.reset()
        return done_response

    def done(self, main_form, *args, **kwargs):
        project = self.get_extra_data('project_instance')
        return redirect(project)


def projects_detail(request, object_id):
    if str(object_id) in map(str, hidden_projects): # Projects hidden by sponsor's request
        object_id = -1
    project = get_object_or_404(Project, pk=object_id)
    if request.method == "POST":
        project_article = ProjectArticleForm(request.POST)
        project_book = ProjectBookForm(request.POST)
        project_dissertation = ProjectDissertationForm(request.POST)
        if 'add_article' in request.POST: # user selected article to add to project
            if project_article.is_valid():
                article = project_article.cleaned_data['article']
                if article:
                    defaults = {'creator': request.user}
                    try:
                        ProjectArticle.objects.get_or_create(article=article, project=project, defaults=defaults)
                        messages.success(request, u"Статья успешно добавлена к НИР.")
                        if request.user:
                            action.send(request.user, verb=u'добавил статью к НИР', action_object=article, target=project)
                    except:
                        pass
                    return redirect('projects_detail', object_id=project.id)
                else: # user selected 'add new article' option
                    messages.info(request, u"Добавить статью к НИР можно будет на 4 этапе добавления работы")
                    return redirect("publications_add")
        if 'add_book' in request.POST: # user selected book to add to project
            if project_book.is_valid():
                book = project_book.cleaned_data['book']
                if book:
                    defaults = {'creator': request.user}
                    try:
                        ProjectBook.objects.get_or_create(book=book, project=project, defaults=defaults)
                        messages.success(request, u"Книга успешно добавлена к НИР.")
                        if request.user:
                            action.send(request.user, verb=u'добавил книгу к НИР', action_object=book, target=project)
                    except:
                        pass
                    return redirect('projects_detail', object_id=project.id)
                else:
                    messages.info(request, u"Добавить книгу к НИР можно будет на 4 этапе добавления работы")
                    return redirect("publications_add")
        if 'add_dissertation' in request.POST: # user selected dissertation to add to project
            if project_dissertation.is_valid():
                dissertation = project_dissertation.cleaned_data['dissertation']
                if dissertation:
                    defaults = {'creator': request.user}
                    try:
                        ProjectDissertation.objects.get_or_create(dissertation=dissertation, project=project, defaults=defaults)
                        messages.success(request, u"Диссертация успешно добавлена к НИР.")
                        if request.user:
                            action.send(request.user, verb=u'добавил диссертацию к НИР', action_object=dissertation, target=project)
                    except:
                        pass
                    return redirect('projects_detail', object_id=project.id)
                else:
                    messages.info(request, u"Добавить диссертацию к НИР можно будет на 4 этапе добавления работы")
                    return redirect("publications_add")
    else:
        project_article = ProjectArticleForm()
        project_book = ProjectBookForm()
        project_dissertation = ProjectDissertationForm()

#    if check_permissions(request, project, 'edit') or \
#           has_permission(request.user, "confirm_projects", project.department):
#        messages.info(request, project.get_sign_info())

    extra_context = {'project_article': project_article, 'project_book': project_book, 'project_dissertation': project_dissertation}
    return detail(request, model=Project, extra_context=extra_context,
                  template_name='projects/detail.html', object_id=object_id)


def collect_estimate(project):
    "Представляем сметы всех этапов в табличной форме. Строка - статья сметы. Столбцы - значения по этапам."
    from projects.models import EstimateCategory, Estimate

    categories = [10, 20, 30, 40, 50, 60]
    result = []
    categories_qs = EstimateCategory.objects.filter(order__in=categories).order_by('order')
    steps_qs = project.steps.all().order_by('number')

    # Загружаем все сметы проекта в двухуровневый словарь с индексами
    # "номер этапа", "номер типа статьи". Номер этапа может бьть None,
    # что соответствует смете проекта
    all_estimates = {}
    for e in Estimate.objects.filter(Q(project=project) | Q(project_step__project=project),
                                     category__in=categories_qs):
        step_id = e.project_step.id if e.project_step else None
        if not all_estimates.get(step_id, None):
            all_estimates[ step_id ] = {}
        all_estimates[ step_id ][ e.category.id ] = e.value

    # Формируем таблицу
    columns = [step for step in steps_qs]
    steps_ids = [s.id for s in steps_qs]
    if all_estimates.get(None, None) and False: # Сметы проектов устарели
        ## есть смета проекта
        steps_ids.insert(0, None)
        columns.insert(0, None)

    for cat in categories_qs:
        cat_values = []
        for step_id in steps_ids:
            if all_estimates.get(step_id, None):
                cat_values.append( all_estimates[step_id].get(cat.id, None) )
            else:
                cat_values.append( None )
        result.append({'category': cat, 'values': cat_values})

    return {
        'columns': columns,
        'data': result,
        }


def detail(request, object_id, model, template_name, extra_context=None, operation=None):
    if str(object_id) in map(str, hidden_projects): # Projects hidden by sponsor's request
        object_id = -1
    can_sign_object = False
    object = get_object_or_404(model, pk=object_id)
    user = request.user
    object._currentuser=user
    is_leader_or_creator = None
    if user.id:
        worker = user.get_profile().worker
        is_leader_or_creator = (user == object.creator or object.head_members.filter(id=worker.id))
    show_operation_confirmation = None
    if operation:
        can_admin_object = False
        can_edit_object = False
        can_delete_object = False
        can_view_object_history = True
        show_operation_confirmation = True
    else:
        can_admin_object = has_permission(request.user, "confirm_projects", object.department)
        can_edit_object = has_permission(request.user, "edit", object) or \
                          has_permission(request.user, 'is_representative_for', object.department)
        can_delete_object = has_permission(request.user, "delete", object)
        can_view_object_history = has_permission(request.user, "view_object_history", object)
        can_sign_object = is_leader_or_creator or can_admin_object
    can_view_estimate = is_leader_or_creator or \
                        any(map(lambda p: has_permission(request.user, p, object.department),
                                ["confirm_projects", "edit_all_estimates", "is_representative_for"]))
    estimate = collect_estimate(object) if can_view_estimate else None
    content_type = ContentType.objects.get_for_model(model)
    attachment_formset = make_attachment_formset(content_type, queryset=Attachment.objects.none())
    context = {
        '%s' % object.nominative_en: object,
        'can_edit_object': can_edit_object,
        'can_delete_object': can_delete_object,
        'can_admin_object': can_admin_object,
        'can_view_object_history': can_view_object_history,
        'can_sign_object': can_sign_object,
        'can_view_estimate': can_view_estimate,
        'estimate': estimate,
        'operation': operation,
        'show_operation_confirmation': show_operation_confirmation,
        'show_object_menu': can_admin_object, # and object.get_status() in ["SIGN", "CONFIRM"],   #!!!!!!!!!!!!!!!!!
        'attachment_formset': attachment_formset
    }
    if extra_context is not None:
        context.update(extra_context)
    return render_to_response(request, template_name, context)


def projects_list(request):
    worker = get_worker(request.user)
    if worker:
        departments = worker.departments.all()
    else:
        departments = []
    projects_ids = Project.objects.filter(members__departments__in=departments).values_list('id', flat=True)
    projects = Project.objects.filter(pk__in=projects_ids).prefetch_related('department', 'members').order_by('name').distinct()
    return render_to_response(
        request, "projects/projects_list.html",
        {
            'projects': projects,
            'departments': departments
        })


@login_required
def project_card_pdf(request, object_id):
    if str(object_id) in map(str, hidden_projects): # Projects hidden by sponsor's request
        object_id = -1
    project = get_object_or_404(Project, pk=object_id)
    #estimate = dict( [(e.category.name, e.value)for e in project.estimates] )
    estimate = project.estimates.filter(project_step=None)
    latex = render_to_string(
        "projects/print-card.tex",
        {
            "project": project,
            'estimate': estimate,
        })
    pdf = latex_to_pdf(latex)
    response = HttpResponse(pdf, mimetype='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=project_' + str(object_id) + '.pdf'
    return response

@login_required
def projects_sign(request, object_id, *args, **kwargs):
    project = get_object_or_404(Project, pk=object_id)
    if not check_permissions(request, project):
        return redirect('projects_detail', object_id=project.id)
    if request.method == "POST":
#        if 'sign_project' in request.POST: # user подтверждает подпись, возврат на доработку или утверджение НИР
            action_sign = request.POST['status']
            step_id = request.POST['step_id']
            projectstep = ProjectStep.objects.get(id=step_id)
            if check_permissions(request, project, projectstep, action_sign):
                if action_sign == 'reedit':
                    pstatus_comment = request.POST['pstatus_comment']
                    projectstep.setstatus('REEDIT', request.user, pstatus_comment)
                elif action_sign == 'sign':
                    projectstep.setstatus('SIGN', request.user)
                elif action_sign == 'confirm':
                    projectstep.setstatus('CONFIRM', request.user)
                projectstep._status = None
                project._status = None
    extra_context = {
        'can_sign': check_permissions(request, project, None, 'sign'),
        'can_reedit': check_permissions(request, project, None, 'reedit'),
        'can_confirm': check_permissions(request, project, None, 'confirm'),
    }
    messages.warning(request, u'Внимание! После подписания этапа НИР становится невозможным изменение части информации:<br> названия, подразделения, руководителя и сметы.')
    return detail(request, model=Project, extra_context=extra_context,
                  template_name='projects/psteps_list.html', object_id=object_id)
#    return sign_action(request, object_id, project, 'sign')

def get_message(additional_str=None):
    if not additional_str:
        return u'Извините, у Вас нет прав для выполнения данной операции.'
    return u'Невозможно выполнить операцию: документ ' + additional_str

def check_permissions(request, project, projectstep=None, operation='edit'):
    is_repr = None
    if request.user.id:
        is_repr = request.user.representatives.exists()

    if operation == 'sign':
        if not project.is_related_to(request.user) and not is_repr:
            messages.warning(request, get_message())
            return False
        if projectstep:
            if not projectstep.show_sign:
                if projectstep._status == 'SIGN':
                    messages.warning(request, get_message(u'подписан'))
                if projectstep._status == 'CONFIRM':
                    messages.warning(request, get_message(u'утвержден'))
                return False
        return True

    if operation == 'reedit':
        if not request.user.is_authenticated():
            if projectstep:
                messages.warning(request, get_message())
            return False
        if not (project.is_admin(request.user) \
               or (project.department and has_permission(request.user, "confirm_projects", project.department))):
            if projectstep:
                messages.warning(request, get_message())
            return False
        if projectstep:
            if projectstep.show_sign:
                if projectstep._status == 'EDIT':
                    messages.warning(request, get_message(u'не подписан'))
                if projectstep._status == 'REEDIT':
                    messages.warning(request, get_message(u'уже возвращен на редактирование'))
                return False
        return True

    if operation == 'confirm':
        if not request.user.is_authenticated():
            if projectstep:
                messages.warning(request, get_message())
            return False
        if not (project.is_admin(request.user) \
               or (project.department and has_permission(request.user, "confirm_projects", project.department))):
            if projectstep:
                messages.warning(request, get_message())
            return False
        if projectstep:
            if projectstep.show_sign:
                if projectstep._status == 'EDIT':
                    messages.warning(request, get_message(u'не подписан'))
                if projectstep._status == 'REEDIT':
                    messages.warning(request, get_message(u'возвращен на редактирование'))
                return False
            if projectstep._status == 'CONFIRM':
                messages.warning(request, get_message(u'уже утвержден'))
                return False
        return True

    if operation == 'edit':
        return project.is_related_to(request.user) or is_repr

    return False

class ProjectAnnotationalReportAddView(CreateView):
    model = ProjectAnnotationalReport
    form_class = ProjectAnnotationalReportForm
    context_object_name = "annotational_report"
    template_name = "projects/annotational_report_add.html"

    @permission_required("add_annotational_report", Project, "project_id")
    def dispatch(self, *args, **kwargs):
        self.project = get_object_or_404(Project, pk=kwargs['project_id'])
        self.year = int(kwargs['year'])
        reports_qs = self.project.annotational_reports.filter(year=self.year)
        if reports_qs.exists():
            return redirect('projects_annotational_report_edit', reports_qs.all()[0].id)
        return super(ProjectAnnotationalReportAddView, self).dispatch(*args, **kwargs)

    def get_initial(self):
        initial = super(ProjectAnnotationalReportAddView, self).get_initial()
        if self.__class__ == ProjectAnnotationalReportAddView:
            initial['results'] = self.project.results_for_year(self.year)
        return initial

    def get_context_data(self, **kwargs):
        kwargs = super(ProjectAnnotationalReportAddView, self).get_context_data(**kwargs)
        kwargs['project'] = self.project
        kwargs['year'] = self.year
        return kwargs

    def form_valid(self, form):
        self.object = form.save(self.request, commit=False)
        if self.__class__ == ProjectAnnotationalReportAddView and self.project.annotational_reports.filter(year=self.year).exists():
            results = self.object.results
            self.object = self.project.annotational_reports.filter(year=self.year).all()[0]
            self.object.results = results
            self.object.save()
        else:
            self.object.project = self.project
            self.object.year = self.year
            self.object = form.save(self.request)
        messages.info(self.request, u"После окончания редактирования необходимо подписать отчет.")
        return super(ModelFormMixin, self).form_valid(form)


class ProjectAnnotationalReportEditView(ProjectAnnotationalReportAddView, UpdateView):

    @permission_required("edit", ProjectAnnotationalReport, "pk")
    def dispatch(self, *args, **kwargs):
        return UpdateView.dispatch(self, *args, **kwargs)

    def get_object(self):
        obj = super(ProjectAnnotationalReportEditView, self).get_object()
        self.project = obj.project
        self.year = obj.year
        if obj.signed and self.request.method == "GET":
            messages.info(self.request, u"Вы редактируете уже подписанный отчет.")
        return obj

    def form_valid(self, form):
        response = super(ProjectAnnotationalReportEditView, self).form_valid(form)
        self.object.unsign()
        return response

class ProjectAnnotationalReportView(DetailView):
    model = ProjectAnnotationalReport
    context_object_name = "annotational_report"
    template_name = 'projects/annotational_report.html'

    @permission_required("view", ProjectAnnotationalReport, "pk")
    def dispatch(self, *args, **kwargs):
        return super(ProjectAnnotationalReportView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs = super(ProjectAnnotationalReportView, self).get_context_data(**kwargs)
        content_type = ContentType.objects.get_for_model(self.model)
        kwargs['attachment_formset'] = make_attachment_formset(content_type, queryset=Attachment.objects.none())
        return kwargs

class ProjectAnnotationalReportSignView(ProjectAnnotationalReportView):

    @permission_required("sign", ProjectAnnotationalReport, "pk")
    def dispatch(self, *args, **kwargs):
        return super(ProjectAnnotationalReportSignView, self).dispatch(*args, **kwargs)

    def get_object(self):
        obj = super(ProjectAnnotationalReportSignView, self).get_object()
        if not obj.signed:
            obj.sign()
            action.send(self.request.user, verb=u'подписал аннотационный отчет', target=obj)
            messages.success(self.request, u"Отчет подписан.")
        return obj


class ProjectAnnotationalReportPDFView(ProjectAnnotationalReportView):
    template_name = 'projects/annotational_report.tex'

    def get_response_filename(self):
        return "project_%d_annotational_report_%d.pdf" % (self.object.project.id, self.object.year)

    def render_to_response(self, context, **response_kwargs):
        """
        Returns a response with a template rendered with the given context.
        """
        if not has_permission(self.request.user, "view_pdf", self.object):
            messages.error(self.request, u"Для просмотра PDF-версии сначала необходимо подписать отчет.")
            return redirect(self.object)
        latex = render_to_string(self.get_template_names(), context)
        pdf = latex_to_pdf(latex)
        response = HttpResponse(pdf, mimetype='application/pdf', **response_kwargs)
        response['Content-Disposition'] = 'attachment; filename=%s' % self.get_response_filename()
        return response

@permission_required('edit', Project)
def publication_delete(request, object_id, publication_type, publication_id):
    '''Delete publication from project, keeping it in database.'''
    project = get_object_or_404(Project, pk=object_id)
    project_publication = getattr(project, "project_%ss" % publication_type).getdefault(**{publication_type: publication_id})
    if project_publication:
        action.send(request.user, verb=u'открепил публикацию от проекта', action_object=project_publication, target=project)
        title = getattr(project_publication, publication_type).title
        project_publication.delete()
        messages.success(request, u"Публикация '%s' успешно откреплена от проекта." % title)
    else:
        messages.error(request, u"Публикация не найдена.")
    return redirect(project)

@permission_required('edit', Project)
def dissertation_delete(request, object_id, dissertation_id):
    '''Delete dissertation from project, keeping it in database.'''
    project = get_object_or_404(Project, pk=object_id)
    project_dissertation = project.project_dissertations.getdefault(dissertation=dissertation_id)
    if project_dissertation:
        action.send(request.user, verb=u'открепил диссертацию от проекта', action_object=project_dissertation.dissertation, target=project)
        title = project_dissertation.dissertation.title
        project_dissertation.delete()
        messages.success(request, u"Диссертация '%s' успешно откреплена от проекта." % title)
    else:
        messages.error(request, u"Диссертация не найдена.")
    return redirect(project)

@login_required
def plan(request, object_id, *args, **kwargs):
    project = get_object_or_404(Project, pk=object_id)
    if not check_permissions(request, project):
        messages.warning(request, get_message())
        return redirect('projects_detail', object_id=project.id)
    if request.method == "POST":
        action_sign = request.POST['status']
        step_id = request.POST['step_id']
        projectstep = ProjectStep.objects.get(id=step_id)
        if action_sign == 'plan':
            query = 'select t.f_pstepplantype_id typeid,t.f_pstepplantype_name name,p.f_pstepplan_id id,p.f_pstepplan_value value from pstepplantype t left join pstepplan p on p.f_pstepplantype_id=t.f_pstepplantype_id and p.f_projstep_id=%s order by typeid' % (projectstep.id)
            plan_list0 = getquerylist(query)
            plan_list = []
            for plan in plan_list0:
                plan_list.append({"typeid":plan[0],"name":plan[1],"id":plan[2],"value":'%s' % (plan[3] if plan[3] else '')})
            if 'plan_save' in request.POST:
                found = 0
                found_errors = 0
                for plan in plan_list:
                    old_value = plan["value"]
                    new_value = request.POST.get('plan_%s' % (plan["typeid"]), None)
                    if new_value == old_value:
                        continue
                    try:
                        test = int(new_value)
                    except Exception as e:
                        if new_value == '':
                            if plan["id"]:
                                PStepPlan.objects.filter(pk=str(plan["id"])).delete()
                                found = found + 1
                        else:
                            found_errors = found_errors + 1
                        continue
                    if plan["id"]:
                        PStepPlan.objects.filter(pk=str(plan["id"])).update(value=new_value)
                        found = found + 1
                    else:
                        pt = PlanType.objects.get(pk=plan['typeid'])
                        PStepPlan.objects.create(projstep=projectstep, type=pt, value=new_value, date=datetime.datetime.now().replace(microsecond=0), creator=request.user)
                        found = found + 1
                if found_errors > 0:
                    messages.error(request, u"Плановые показатели этапа %s введены с ошибками (всего ошибок %s)" % (projectstep.number,found_errors))
                else:
                    if found > 0:
                        messages.success(request, u"Плановые показатели этапа %s успешно сохранены (измененных значений %s)" % (projectstep.number,found))
                    else:
                        messages.success(request, u"Плановые показатели этапа %s не были изменены" % projectstep.number)
            else:
                return render_to_response(request, 'projects/psteps_plan_forma.html',
                              {"step": projectstep,
                               "project": project, "model": project, "plan_list": plan_list})
        elif action_sign == 'planresult':
            if 'planresult' in request.POST:
                new_planresult = request.POST.get('planresult', None)
                if projectstep.planresult != new_planresult:
                    ProjectStep.objects.filter(id=projectstep.id).update(planresult=new_planresult)
                    messages.success(request, u"Ожидаемые результаты этапа %s успешно сохранены" % projectstep.number)
                else:
                    messages.success(request, u"Ожидаемые результаты этапа %s не были изменены" % projectstep.number)
            else:
                return render_to_response(request, 'projects/pstep_planresult.html',
                              {"step": projectstep,
                               "project": project, "model": project})
        elif action_sign == 'itog':
            if 'stepresult' in request.POST:
                new_stepresult = request.POST.get('stepresult', None)
                if projectstep.stepresult != new_stepresult:
                    ProjectStep.objects.filter(id=projectstep.id).update(stepresult=new_stepresult)
                    messages.success(request, u"Результаты этапа %s успешно сохранены" % projectstep.number)
                else:
                    messages.success(request, u"Результаты этапа %s не были изменены" % projectstep.number)
            else:
                return render_to_response(request, 'projects/project_step_results.html',
                              {"step": projectstep,
                               "project": project, "model": project})
        elif action_sign == 'ktu':
            query = 'select f_projstepman_id id,f_projstepman_name name,f_projstepman_ktu ktu,f_projstepman_head head,f_man_id man_id from projstepman where f_projstep_id=%s order by head desc,name' % (projectstep.id)
            ktu_list0 = getquerylist(query)
            ktu_list = []
            for ktu in ktu_list0:
                if ktu[2] == None:
                    v_ktu = 1
                else:
                    v_ktu = ktu[2]
                ktu_list.append({"id":ktu[0],"name":ktu[1],"ktu":'%6.4f' % v_ktu})
            if 'ktu_save' in request.POST:
                found = 0
                found_errors = 0
                for ktu in ktu_list:
                    old_ktu = ktu["ktu"]
                    try:
                        new_ktu = float(request.POST.get('ktu_%s' % (ktu["id"]), None))
                    except:
                        found_errors = found_errors + 1
                        continue
                    if new_ktu < 0 or new_ktu > 1:
                        found_errors = found_errors + 1
                        continue
                    new_ktu = '%6.4f' % new_ktu
                    if new_ktu <> old_ktu:
                        ProjectMembership.objects.filter(id=ktu["id"]).update(ktu=new_ktu)
                        found = found + 1
                if found_errors > 0:
                    messages.error(request, u"КТУ этапа %s введены с ошибками (всего ошибок %s)" % (projectstep.number,found_errors))
                else:
                    if found > 0:
                        messages.success(request, u"КТУ этапа %s успешно сохранены (измененных значений %s)" % (projectstep.number,found))
                    else:
                        messages.success(request, u"КТУ этапа %s не были изменены" % projectstep.number)
            else:
                messages.info(request, u'Коэффициентов трудового участия исполнителей нужны, если они используются для расчета рейтингов сотрудников.<br>КТУ вводятся в пределах от 0.0000 до 1.0000, до 4 знаков в после точки.<br>Отсутствующие КТУ обрабатываются как 1.')
                return render_to_response(request, 'projects/psteps_ktu.html',
                              {"step": projectstep,
                               "project": project, "model": project, "ktu_list": ktu_list})
    extra_context = {}
    messages.info(request, u'Ввод и редактирование планов выполнения этапов, итогов выполнения этапов, коэффициентов трудового участия исполнителей.')
    return detail(request, model=Project, extra_context=extra_context,
                  template_name='projects/psteps_plan.html', object_id=object_id)

def getquerylist(sql):
    data = list()
    try:
        cursor = connection.cursor()
        cursor.execute(sql)
        data = list(cursor)
    finally:
        cursor.close()
    return data
