from django.contrib.formtools.wizard.views import SessionWizardView
from django.db import models
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils.decorators import classonlymethod
from django.forms.formsets import BaseFormSet
from django import forms
from common.utils.context import render_to_response
from common.utils.list import is_tuple_list, in_or_equal
from unified_permissions import has_permission
from django.contrib.formtools.wizard.forms import ManagementForm
from django.forms import ValidationError


class MyModelWizardView(SessionWizardView):
    '''Class containing some extra common methods and attributes for wizards.'''
    add = True  # add mode
    edit = False  # edit mode
    model = None  # should be overriden
    main_step = 'main'  # name of the main step
    steps_info = {}

    @classonlymethod
    def make_condition_dict(cls, *steps):
        condition_dict = {}
        for step in steps:
            # see http://stackoverflow.com/a/2295372
            condition_dict[step] = lambda wizard, step = step: wizard.step_included(step)

        return condition_dict

    @classonlymethod
    def create_wizard_from_form(cls, form_class):
        class MyModelWizardViewWrapper(cls):
            form_list = [(MyModelWizardView.main_step, form_class)]
            model = form_class.Meta.model

        return MyModelWizardViewWrapper

    @classonlymethod
    def as_view(cls, *args, **kwargs):
        '''Main function that is executed to get the dispatch function,
        which then processes get and post requests.'''
        return super(MyModelWizardView, cls).as_view(
            form_list = cls.form_list,
            initial_dict = cls.initial_dict,
            instance_dict = cls.instance_dict,
            condition_dict = cls.condition_dict,
            *args, **kwargs)

    def has_edit_permission(self, user, instance):
        return has_permission(user, "edit", instance) or \
            has_permission(user, 'is_representative_for', instance.department)

    def dispatch(self, request, *args, **kwargs):
        object_id = kwargs.get('object_id', None)
        if object_id and self.model:
            try:
                instance = self.model.objects.get(id=object_id)
            except:
                return redirect("home")
            else:
                if not self.has_edit_permission(request.user, instance):
                    return redirect(instance)
                self.instance_dict[self.main_step] = instance
                self.edit = True
                self.add = False
        kwargs['session_object_num'] = self.get_current_wizard_num(request)
        return super(MyModelWizardView, self).dispatch(request, *args, **kwargs)

    def get_next_step(self, step=None):
        if self.edit:
            return self.steps.current
        return super(MyModelWizardView, self).get_next_step(step)

    def get_template_names(self):
        return 'common/add_edit_object_wizard.html'

    def get_context_data(self, form, **kwargs):
        context = super(MyModelWizardView, self).get_context_data(form, **kwargs)
        # show all previous forms (read-only)
        previous_forms = []
        try:
            self.no_previous_forms
        except AttributeError:
            for form_key in self.get_form_list():
                if form_key == self.steps.current:  # show only previous forms
                    break
                form_obj = self.get_form(
                    step=form_key,
                    data=self.storage.get_step_data(form_key),
                    files=self.storage.get_step_files(form_key))
                previous_forms.append(form_obj)
        context.update({
            'model': self.model,
            'add': self.add,
            'edit': self.edit,
            'previous_forms': previous_forms,
            'formset': isinstance(form, BaseFormSet),
            'current_step_info': self.steps_info.get(self.steps.current, None)
        })
        return context

    def set_extra_data(self, *args):
        '''Arguments can be two-tuples (key/value) or two args (key and value).'''
        if not is_tuple_list(args[0]):  # the args are two: key and value.
            data = {args[0]: args[1]}
        else:  # args are two-tuples
            data = {}
            for key, value in args:
                data.update({key: value})
        extra_data = self.storage._get_extra_data()
        extra_data.update(data)
        self.storage._set_extra_data(extra_data)

    def get_extra_data(self, *args):
        '''Get the stored extra data by keys.
        If cls is specified, check if the value is an instance of the class.
        If not, return None.
        @args can be just keys, or two-tuples (key, cls), or two args: key and cls.
        '''
        if len(args) == 2 and type(args[1]) == models.base.ModelBase:  # two args: key and cls
            args = (args,)
        else:
            args = [(arg, None) if not is_tuple_list(arg) else arg for arg in args]

        def check_class(value, cls):
            return value if ((not cls) or isinstance(value, cls)) else None
        values = [check_class(self.storage._get_extra_data().get(key, None), cls) for key, cls in args]
        return tuple(values) if len(values) > 1 else values[0]

    def delete_extra_data(self, key):
        '''Deletes the given key.'''
        extra_data = self.storage._get_extra_data()
        if key in extra_data:
            extra_data.pop(key)
        self.storage._set_extra_data(extra_data)

    def done(self, form_list, **kwargs):
        # all forms are submitted and validated, now process the data
        # save main object form
#        return
#        object = form_list[0].save(self.request)
        return redirect(object)

    def hide_form_fields(self, forms, form_key, fields, current_step_in=None):
        '''This function hides @fields from the form with @form_key in @forms
        if the current step is (optionally) @current_step_in.
        @current_step_in can be a tuple or list or a single string.
        @fields can be a tuple/list or a single field (a string).
        '''
        if not in_or_equal(self.steps.current, current_step_in) or form_key not in self.steps.all:
            return
        forms[self.get_step_index(form_key)].hide_fields(fields)

    def step_included(self, step):
        # if step is not skipped, it is included
        return not self.get_extra_data('skip_%s_step' % step)

    def include_steps(self, *steps):
        for step in steps:
            self.set_extra_data('skip_%s_step' % step, False)

    def skip_steps(self, *steps):
        for step in steps:
            self.set_extra_data('skip_%s_step' % step, True)

    def skip_or_include_steps(self, *args):
        '''args are two: step and condition,
        or two-tuples (step, condition).'''
        if not is_tuple_list(args[0]):  # two args
            if args[1]:  # condition is true
                self.skip_steps(args[0])
            else:
                self.include_steps(args[0])
        else:  # args are two-tuples (step, condition)
            for step, condition in args:
                if condition:
                    self.skip_steps(step)
                else:
                    self.include_steps(step)

    def get_current_wizard_num(self, request):
        '''
        Determine number of wizard. If this is new step of existing wizard, than we get it's number from management
        form. Otherwise, generate new number based on request session.
        '''
        POST = request.POST
        prefix = self.get_prefix_base()
        management_form_field_name = None
        for field in POST:
            if field.startswith(prefix):
                management_form_field_name = field
        if not management_form_field_name:
            return str(self.get_new_wizard_num(request))
        object_num = ''
        for c in management_form_field_name[len(prefix) + 1:]:
#        for c in management_form_field_name[len(prefix):]:
            try:
                int(c)
            except ValueError:
                break
            else:
                object_num += c
        return object_num

    def get_new_wizard_num(self, request):
        '''
        Each wizard has it's own record in request session. We count records of this wizard class in session and
        return number to be assigned to new wizard. "wizard_" is a prefix, automatically added by django to every
        record of wizards in request session.
        '''
        session = request.session
        prefix = 'wizard_' + self.get_prefix_base()
        count_num = len([x for x in session.keys() if x.startswith(prefix)])
        return count_num + 1
#        return count_num

    def get_prefix_base(self,  *args, **kwargs):
        return super(MyModelWizardView, self).get_prefix(*args, **kwargs)

    def get_prefix(self,  *args, **kwargs):
        base_prefix = self.get_prefix_base(*args, **kwargs)
        session_objects_num = kwargs.get('session_object_num', None)
        if not session_objects_num:
            return super(MyModelWizardView, self).get_prefix(*args, **kwargs)
        return base_prefix + '_' + session_objects_num

    def post(self, *args, **kwargs):
        """
        This method handles POST requests.

        The wizard will render either the current step (if form validation
        wasn't successful), the next step (if the current step was stored
        successful) or the done view (if no more steps are available)
        """
        # Look for a wizard_goto_step element in the posted data which
        # contains a valid step name. If one was found, render the requested
        # form. (This makes stepping back a lot easier).
        wizard_goto_step = self.request.POST.get('wizard_goto_step', None)

        if wizard_goto_step and wizard_goto_step in self.get_form_list():
            self.storage.current_step = wizard_goto_step
            form = self.get_form(
                data=self.storage.get_step_data(self.steps.current),
                files=self.storage.get_step_files(self.steps.current))
            return self.render(form)

        # Check if form was refreshed
        management_form = ManagementForm(self.request.POST, prefix=self.prefix)
        if not management_form.is_valid():
            raise ValidationError(
                'ManagementForm data is missing or has been tampered.')

        form_current_step = management_form.cleaned_data['current_step']
        if (form_current_step != self.steps.current and
                self.storage.current_step is not None):
            # form refreshed, change current step
            self.storage.current_step = form_current_step

        # get the form for the current step
        form = self.get_form(data=self.request.POST, files=self.request.FILES)

        # and try to validate
        if form.is_valid():
            # if the form is valid, store the cleaned data and files.
            self.storage.set_step_data(self.steps.current, self.process_step(form))
            form._changed_data = None
            self.storage.set_step_files(self.steps.current, self.process_step_files(form))
            if self.add and self.steps.current == self.steps.last:
                # no more steps, render done view
                return self.render_done(form, **kwargs)
            else:
                return self.render_next_step(form)
        return self.render(form)

    def render_next_step(self, form, **kwargs):
        """
        This method gets called when the next step/form should be rendered.
        `form` contains the last/current form.
        """
        # get the form instance based on the data from the storage backend
        # (if available).
        if form.data.get('add_new_step','0') == '1':
            next_step = self.steps.current
        else:
            next_step = self.steps.next
        if next_step == self.storage.current_step:
            new_form = self.get_form()
        else:
            new_form = self.get_form(next_step,
                data=self.storage.get_step_data(next_step),
                files=self.storage.get_step_files(next_step))

            # change the stored current step
            self.storage.current_step = next_step
        return self.render(new_form, **kwargs)
        
    def render(self, form=None, **kwargs):
        """
        Returns a ``HttpResponse`` containing all needed context data.
        """
        form = form or self.get_form()
#        form = self.get_form()
        context = self.get_context_data(form=form, **kwargs)
        return self.render_to_response(context)
