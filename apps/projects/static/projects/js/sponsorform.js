$(function() {
    $("select[id^='id_sponsors-'][id$='-category']").change(function() {
          value_selected =  $(this).find('option:selected').val()
          source_category_name_field =  $(this).closest('div.form').find('input[id$="_category_name"]').closest('div.field')
          program_name_field =  $(this).closest('div.form').find('input[id$="-name"]').closest('div.field')
          if (value_selected) {
              source_category_name_field.hide()
              if ($(this).find('option.has_own_name:selected').length) {
                  program_name_field.show()
              } else {
                  program_name_field.hide()
              }
          } else {
              source_category_name_field.show()
              program_name_field.show()
          }
    })
    $("select[id^='id_sponsors-'][id$='-category']").change()
    $(' .formset_add_form').click(function() { // FIXME add event.preventDefault()?
        $("select[id^='id_sponsors-'][id$='-category']").change()
     });
})
