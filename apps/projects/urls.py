# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *

from projects.models import Project, ProjectMembership, ProjectAnnotationalReport
from projects.forms import ProjectWizardForm
from projects.views import projects_detail, ProjectWizardView, projects_list, project_card_pdf, projects_sign, plan, \
    ProjectAnnotationalReportAddView, ProjectAnnotationalReportEditView, ProjectAnnotationalReportView, ProjectAnnotationalReportSignView, ProjectAnnotationalReportPDFView, \
    publication_delete, dissertation_delete
from django.views.generic.list_detail import object_detail
from django.contrib.auth.models import User

# projects
project_options_base = {'model': Project}

urlpatterns = patterns('common.views',
    url(r'^$', projects_list, name='projects_list'),
    url(r'^add/$', ProjectWizardView.as_view(), name='projects_add'),
    url(r'^(?P<object_id>\d+)/$', projects_detail,  name='projects_detail'),
    url(r'^(?P<object_id>\d+)/card/pdf/$', project_card_pdf,  name='project_card_pdf'),
    url(r'^(?P<object_id>\d+)/edit/$', ProjectWizardView.as_view(), name='projects_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', project_options_base, name='projects_delete'),
    url(r'^(?P<object_id>\d+)/(?P<publication_type>article|book)/(?P<publication_id>\d+)/delete/$', publication_delete, name='projects_publication_delete'),
    url(r'^(?P<object_id>\d+)/dissertation/(?P<dissertation_id>\d+)/delete/$', dissertation_delete, name='projects_dissertation_delete'),
    url(r'^search/$', 'autocomplete_search', {'model': Project}, name="projects_search"),
    url(r'^(?P<object_id>\d+)/sign/$', projects_sign, name='projects_sign'),
    url(r'^(?P<object_id>\d+)/plan/$', plan, name='plan'),
    url(r'^(?P<project_id>\d+)/annotational_report/(?P<year>\d{4})/add/$', ProjectAnnotationalReportAddView.as_view(), name='projects_annotational_report_add'),
    url(r'^annotational_reports/(?P<pk>\d+)/$', ProjectAnnotationalReportView.as_view(), name='projects_annotational_report'),
    url(r'^annotational_reports/(?P<pk>\d+)/edit/$', ProjectAnnotationalReportEditView.as_view(), name='projects_annotational_report_edit'),
    url(r'^annotational_reports/(?P<object_id>\d+)/delete/$', 'delete', {'model': ProjectAnnotationalReport}, name='projects_annotational_report_delete'),
    url(r'^annotational_reports/(?P<pk>\d+)/sign/$', ProjectAnnotationalReportSignView.as_view(), name='projects_annotational_report_sign'),
    url(r'^annotational_reports/(?P<pk>\d+)/pdf/$', ProjectAnnotationalReportPDFView.as_view(), name='projects_annotational_report_pdf'),
)

# memberships
membership_options_base = {'model': ProjectMembership}
membership_options_detail = dict(membership_options_base.items() + [('template_name', 'projects/membership_detail.html')])

urlpatterns += patterns('common.views',
    #url(r'^memberships/add/$', Wizard.as_view(), name='projects_membership_add'),
    url(r'^memberships/(?P<object_id>\d+)/$', 'detail', membership_options_detail, name='projects_membership_detail'),
    #url(r'^memberships/(?P<object_id>\d+)/edit/ $', Wizard.as_view(), name='projects_membership_edit'),
    url(r'^memberships/(?P<object_id>\d+)/delete/$', 'delete', membership_options_base, name='projects_membership_delete'),
)
