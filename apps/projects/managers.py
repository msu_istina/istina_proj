from common.managers import SimilarFieldManager
from common.utils.dict import filter_true
from publications.models import Article, Book
from django.db.models import Q
from projects import hidden_projects

class ProjectManager(SimilarFieldManager):
    '''Model manager for Project model.'''

    def get_similar(self, name, **kwargs):
        qs = self
        # filter out kwargs with False value
        kwargs = filter_true(kwargs)
        if kwargs:
            qs = qs.filter(**kwargs)
        return self.get_similar_by_field('name', name, cutoff=0.5, n=10, qs=qs)


    def publication_projects(self, publication):
        publication_authors = publication.authors.all()
        ids = self.filter(members__in=publication_authors).values_list('pk', flat=True)
        return self.filter(pk__in=ids)

    def public(self):
        ids = self.exclude(pk__in=hidden_projects).values_list('id').distinct()
        project_ids=[x[0] for x in ids]
        return self.model.objects.filter(id__in=project_ids)

    def recent_public(self, limit=3, recent_type="date"):
        ids=self.exclude(pk__in=hidden_projects).values_list('pk').distinct().order_by('-startdate')[:limit]
        project_ids=[x[0] for x in ids]
        return self.model.objects.filter(pk__in=project_ids).order_by('-startdate')
        
