# -*- coding: utf-8; -*-
__author__ = 'goldan'
import authority
from common.permissions import MyModelPermission, LinkedToWorkersModelPermission
from projects.models import ProjectAnnotationalReport, Project
from unified_permissions import has_permission


class ProjectAnnotationalReportPermission(MyModelPermission):
    label = 'project_annotational_report_permission'
    checks = ('check_view',)

    def check_view(self, report):
        """A user can add annotational report to a project if he (OR):
            - can add annotational reports in department (e.g. representative)
            - head of the project
            - member of the project
            - creator of the project
        """
        return has_permission(self.user, "add_annotational_report", report.project.department) \
            or report.project.memberships.filter(member__profile__user=self.user).exists() \
            or report.project.creator == self.user

    def check_edit(self, report):
        return has_permission(self.user, "add_annotational_report", report.project)

    def check_delete(self, report):
        return has_permission(self.user, "add_annotational_report", report.project)


class ProjectPermission(LinkedToWorkersModelPermission):
    label = 'project_permission'
    checks = ('check_add_annotational_report',)
    operations = (
        ("add_annotational_report", u"может добавлять аннотационные отчеты", False),
    )

    def check_unauthor(self, project):
        # Reason why projects cannot be unauthored:
        # currently, Select2 works only with workers ids,
        # and unauthored memberships have no workers,
        # therefore editing will not work properly as we are unable to fill fields
        # with initial values properly.
        return False

    def check_add_annotational_report(self, project):
        """A user can add annotational report to a project if he (OR):
            - can add annotational reports in department (e.g. representative)
            - head of the project
            - creator of the project
        """
        return has_permission(self.user, "add_annotational_report", project.department) \
            or project.memberships.filter(is_head=True, member__profile__user=self.user).exists() \
            or project.creator == self.user


authority.register(ProjectAnnotationalReport, ProjectAnnotationalReportPermission)
authority.register(Project, ProjectPermission)
