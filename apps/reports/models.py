# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models

from common.models import AuthoredModel, AuthorshipPositionedModel, Activity


class Report(AuthoredModel, Activity):
    '''A report within an organization.'''
    id = models.AutoField(primary_key=True, db_column="F_REPORT_ID")
    title = models.CharField(u"Название отчета", max_length=1000, db_column="F_REPORT_NAME")
    authors = models.ManyToManyField("workers.Worker", related_name="reports", through="ReportAuthorship")
    number = models.CharField(u"Номер", max_length=255, db_column="F_REPORT_NUMBER")
    date = models.DateField(u"Дата выпуска отчета", db_column="F_REPORT_DATE")
    organization = models.ForeignKey(to="organizations.Organization", related_name="reports", db_column="F_ORGANIZATION_ID", verbose_name=u"Организация")
    department = models.ForeignKey(to="organizations.Department", related_name="reports", db_column="F_DEPARTMENT_ID", blank=True, null=True, verbose_name=u"Подразделение")
    pages = models.IntegerField(u"Количество страниц", db_column="F_REPORT_PAGE", null=True, blank=True)
    abstract = models.TextField(u"Аннотация", db_column="F_REPORT_ABSTRACT", blank=True)
    xml = models.TextField(u"XML отчета", db_column="F_REPORT_XML", blank=True)
    creator = models.ForeignKey(to=User, related_name="reports_added", db_column="F_REPORT_USER", null=True, blank=True)

    nominative_en = "report"
    genitive = u"отчета"
    genitive_plural_full = u"отчетов"
    instrumental = u"отчетом"
    locative = u"отчете"

    class Meta:
        db_table = "REPORT"
        verbose_name = u"отчет"
        verbose_name_plural = u"отчеты"
        get_latest_by = "date"
        ordering = ('-date', '-number')

    def __unicode__(self):
        return "%s (# %s)" % (self.title, self.number)

    @models.permalink
    def get_absolute_url(self):
        return ('reports_detail', (), {'object_id': self.id})


class ReportAuthorship(AuthorshipPositionedModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORREP_ID")
    author = models.ForeignKey(to="workers.Worker", related_name="report_authorships", db_column="F_MAN_ID", null=True, blank=True)
    report = models.ForeignKey(to="Report", related_name="authorships", db_column="F_REPORT_ID")
    original_name = models.CharField(max_length=255, db_column="F_AUTHORREP_NAME", blank=True)
    position = models.IntegerField(db_column="F_AUTHORREP_ORD", default=0, null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="report_authorships_added", db_column="F_AUTHORREP_USER", null=True, blank=True)

    class Meta(AuthorshipPositionedModel.Meta):
        db_table = "AUTHORREP"
        verbose_name = u"авторство отчета"
        verbose_name_plural = u"авторства отчетов"

    def __unicode__(self):
        return u"%s, автор отчета '%s'" % (self.workers_string_full, self.report.title)

from reports.admin import *
