# -*- coding: utf-8; -*-
import datetime

from common.forms import LinkedToWorkersModelForm, YearField
from organizations.forms import DepartmentChoiceField
from reports.models import Report


class ReportForm(LinkedToWorkersModelForm):
    '''A simple form for reports.'''
    year = YearField(label=u"Год выпуска")
    department = DepartmentChoiceField(label=u"Подразделение", required=False)
    fields_order = ["organization", "department", "authors_str", "title", "number", "year", "pages", "abstract"]
    css_classes = [('pages', 'narrow'), ('department', 'wide')]
    organization_index = 1

    class Meta:
        model = Report
        exclude = ('authors', 'date', 'creator', 'xml')

    def __init__(self, *args, **kwargs):
        super(ReportForm, self).__init__(*args, **kwargs)
        # remove datepicker class, since the user is expected to enter the year
        self.remove_css_class('date', 'datepicker')
        if self.instance_linked(): # initialize year field
            self.fields['year'].initial = self.instance.date.year

    def commit_save(self, request, object, workers):
        '''Method that is executed just before final commiting the object into a database.'''
        # transform year into date field
        year = self.cleaned_data.get('year', None)
        if year:
            object.date = datetime.date(year, 1, 1)
        super(ReportForm, self).commit_save(request, object, workers)
