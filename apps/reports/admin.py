from common.utils.admin import register_with_versions
from models import Report, ReportAuthorship

register_with_versions(Report)
register_with_versions(ReportAuthorship)
