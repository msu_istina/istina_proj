# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *

from common.models import AuthorRole
from common.views import LinkedToWorkersModelWizardView as Wizard
from reports.forms import ReportForm
from reports.models import Report

ReportFormWizard = Wizard.create_wizard_from_form(ReportForm)

options_base = {'model': Report}
options_detail = dict(options_base.items() + [('template_name', 'reports/detail.html'),
    ('extra_context', {'roles': AuthorRole.objects.filter(authorroleobj__code="report")})])

urlpatterns = patterns('common.views',
    url(r'^add/$', ReportFormWizard.as_view(), name='reports_add'),
    url(r'^(?P<object_id>\d+)/$', 'detail', options_detail, name='reports_detail'),
    url(r'^(?P<object_id>\d+)/edit/$', ReportFormWizard.as_view(), name='reports_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', options_base, name='reports_delete'),
)
