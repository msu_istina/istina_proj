# -*- coding: utf-8; -*-
from django.http import HttpResponse
from django.utils import simplejson
from workers.models import Worker

def xhr_get_worker_name(request):
    worker_id = request.GET.get('term', '')
    try:
        worker = Worker.objects.get(pk=worker_id)
        message_data = worker.fullname
    except (ValueError, Worker.DoesNotExist):
        message_data = worker_id
    #return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')
    return HttpResponse(message_data, content_type='application/javascript')
