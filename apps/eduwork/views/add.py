# -*- coding: utf-8; -*-
import logging
from django.contrib import messages
from django.shortcuts import redirect, render, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Q



import sys
import logging
from django.core.context_processors import request

from common.views import *
from eduwork.forms import EduWorkFilterForm, EduWorkDetailForm, EducGroupListForm
from eduwork.models import EducWork, Discipline, EducGroup, StudyGroup, EducWorkMan
from organizations.models import Department
from gdata.contentforshopping.data import Year
from celery.apps.worker import Worker


logger = logging.getLogger('sentry_debug')



def eduwork_add (request):
    if request.method == 'POST':
        form = EduWorkFilterForm(request.POST)
        if form.is_valid():
            selfEduWork = form.save(request, commit=False)
            return eduwork_list(request, selfEduWork.department.id, selfEduWork.year)
            
              
    else:
        form = EduWorkFilterForm()
    return render(request, "eduwork/eduworkstep1.html", {'form' : form})

def eduwork_list (request, department, year):
    
    from userprofile.views import Profile
    profile =  Profile.objects.get(user=request.user)
    
    context = {
        'eduworks': EducWork.objects.filter(year= year).filter(department= department).filter(workers =profile.worker),
        'department': department,
        'year': year,
        }
    
    return render(request, "eduwork/eduworkstep2.html",
          context  )
     
    


def eduwork_add_row (request, department, year):
    
    from userprofile.views import Profile
    from workers.models import Worker
    profile =  Profile.objects.get(user=request.user)
     
    
    if request.method == 'POST':
        form = EduWorkDetailForm(request.POST, dep = department)
        
        if request.POST['fh_action'] == '1':
            form.LoadParam (request.POST['eductype'])
        elif form.is_valid(): 
                selfEduWork = form.save(request, commit=False)
                selfEduWork.department = Department.objects.get(id=department)
                selfEduWork.year = year
                selfEduWork.save()
                EducWorkMan.objects.create(educwork_id= selfEduWork, worker_id = profile.worker, educworkman_part=100)
                return eduwork_list(request, selfEduWork.department.id, selfEduWork.year)
              
    else:
        form = EduWorkDetailForm(dep = department)
        
    return render(request, "eduwork/eduworkdetail.html", {'form' : form})



def eduwork_copy_row (request, id):
    
    educwork = EducWork.objects.get(id=id)
    if request.method == 'POST':
        form = EduWorkDetailForm(request.POST)
        
        if request.POST['fh_action'] == '1':
            form.LoadParam (request.POST['eductype'])
        elif form.is_valid():
            selfEduWork = form.save(request, commit=False)
            educwork = EducWork.objects.get(id=id)
            selfEduWork.department =  Department.objects.get()
            selfEduWork.year = educwork.year
            selfEduWork.save()
            return eduwork_list(request, selfEduWork.department.id, selfEduWork.year)
              
    else:
        
        form = EduWorkDetailForm(instance=educwork)
        form.LoadParam (educwork.eductype)
        
    return render(request, "eduwork/eduworkdetail.html", {'form' : form})    

def eduwork_edit_row (request, id):
    educwork = EducWork.objects.get(id=id)
    if request.method == 'POST':
        form = EduWorkDetailForm(request.POST,instance=educwork)
        
        if request.POST['fh_action'] == '1':
            form.LoadParam (request.POST['eductype'])
        elif form.is_valid():
            selfEduWork = form.save(request, commit=False)
            educwork = EducWork.objects.get(id=id)
            selfEduWork.department =  Department.objects.get(id=educwork.department.id)
            selfEduWork.year = educwork.year
            selfEduWork.save()
            return eduwork_list(request, selfEduWork.department.id, selfEduWork.year)
              
    else:
        form = EduWorkDetailForm(instance=educwork)
        form.LoadParam (educwork.eductype)
        
    return render(request, "eduwork/eduworkdetail.html", {'form' : form})

def eduwork_delete_row (request, id):
    
    educwork = EducWork.objects.get(id=id)
    department = educwork.department.id 
    year = educwork.year
    educwork.delete()
    return eduwork_list(request, department, year)


def eduwork_educgroup_list (request, id):
    
    educwork = EducWork.objects.get(id=id)
    
    if request.method == 'POST':
        form = EducGroupListForm(request.POST, faculty_id = educwork.faculty.id, educwork_id = id )
        form.SaveEducGroup (request.POST.getlist('chk_in_educwork'))
        return eduwork_list(request, educwork.department.id, educwork.year)
              
    else:
        form = EducGroupListForm(faculty_id = educwork.faculty.id,educwork_id = id)
    return render(request, "eduwork/educgroup_list.html", {'form' : form})
    
    

        
    
    



