# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User

from common.models import MyModel
from common.models import LinkedToWorkersModel, WorkershipModel, Activity, AuthorScience
from common.utils.validators import validate_year


from pmodel.models import EducType, EducForm, EducStage
from organizations.models import Department
from workers.models import Worker
from scipy.signal._max_len_seq import max_len_seq
    


class EducTypePar(MyModel, Activity):
    class Meta:
        db_table = "EDUCTYPEPAR"
        verbose_name = u"возможные параметры вида работы"
    
    id = models.AutoField(primary_key=True, db_column="F_EDUCTYPEPAR_ID")
    eductype = models.ForeignKey(EducType, verbose_name="Вид работы", db_column="F_EDUCTYPE_ID")
    name = models.CharField (verbose_name="Вид работы", db_column="F_EDUCTYPEPAR_CODE", max_length=255)
    
    #возвращает доступность параметра в зависимости от вида работы
    def GetParamReadonly (self, eductype, parname):
        return (EducTypePar.objects.filter(eductype = eductype).filter(name= parname).count() ==0)
    
    
class Discipline(MyModel, Activity):
    class Meta:
        db_table = "DISCIPLINE"
        verbose_name = u"учебная дисциплина"
    
    id = models.AutoField(primary_key=True, db_column="F_DISCIPLINE_ID")
    name = models.CharField (verbose_name="Название дисциплины", db_column="F_DISCIPLINE_NAME", max_length=255)
    departments = models.ManyToManyField(Department, related_name='departments', through= "DiscDep", null=True, blank=True)
    
    def __unicode__(self):
        return self.name
    
class DiscDep(MyModel, Activity):
    class Meta:
        db_table = "DISCDEP"
        verbose_name = u"дисциплины преподаваемые на кафедре"
    
    id = models.AutoField(primary_key=True, db_column="F_DISCDEP_ID")
    discipline = models.ForeignKey(Discipline, verbose_name="Дисциплина", db_column="F_DISCIPLINE_ID")
    department = models.ForeignKey(Department, verbose_name="Кафедра", db_column="F_DEPARTMENT_ID")
    
class TypeAcademicYear (MyModel, Activity):
    class Meta:
        db_table = "TYPEYEARS"
        verbose_name = u"типы учебного года"
    
    id = models.AutoField(primary_key=True, db_column="F_TYPEYEARS_ID")
    name = models.CharField (verbose_name="название типа", db_column="F_TYPEYEARS_NAME", max_length=255)
    

class AcademicYear (MyModel, Activity):
    class Meta:
        db_table = "YEARS"
        verbose_name = u"учебные года"
    
    id = models.AutoField(primary_key=True, db_column="F_YEARS_ID")
    typeyear_id = models.ForeignKey(TypeAcademicYear, verbose_name="Целевой факультет", db_column="F_TYPEYEARS_ID")
    name = models.CharField (verbose_name="название группы", db_column="F_YEARS_NAME", max_length=255)    
    
class StudyGroup (MyModel, Activity):
    class Meta:
        db_table = "STUDYGROUPS"
        verbose_name = u"учебные группы целевого факультета"
    
    id = models.AutoField(primary_key=True, db_column="F_STUDYGROUPS_ID")
    department_id = models.ForeignKey(Department, verbose_name="Целевой факультет", db_column="F_DEPARTMENT_ID")
    year_id = models.ForeignKey(AcademicYear, verbose_name="Учебный год", db_column="F_YEARS_ID")
    studygroup_name = models.CharField (verbose_name="название группы", db_column="F_STUDYGROUPS_NAME", max_length=255)
    studbudjet = models.IntegerField (verbose_name="Кол-во бюджетных студентов", db_column="F_STUDYGROUPS_STUDBUDJET", null=True, blank=True)
    studpaid = models.IntegerField (verbose_name="Кол-во платных студентов", db_column="F_STUDBUDJET_STUDPAID", null=True, blank=True)
    
    in_educwork = False 
        


    
                 
    
class EducWork(MyModel, Activity):
    
    class Meta:
        db_table = "EDUCWORK"
        verbose_name = u"педагогическая нагрузка"
    
    ACADEMIC_YEAR = (
    (2009, '2009/2010 учебный год'),
    (2010, '2010/2011 учебный год'),
    (2011, '2011/2012 учебный год'),
    (2012, '2012/2013 учебный год'),
    (2013, '2013/2014 учебный год'),
    (2014, '2014/2015 учебный год'),
    (2015, '2015/2016 учебный год'),
    (2016, '2016/2017 учебный год'),
)
    
    SEMESTER = (
                (1, '1'),
                (2, '2'),
                (3, '3'),
                (4, '4'),
                (5, '5'),
                (6, '6'),
                (7, '7'),
                (8, '8'),
                (9, '9'),
                (10, '10'),
                (11, '11'),
                (12, '12'),
)
    
    id = models.AutoField(primary_key=True, db_column="F_EDUCWORK_ID")
    eductype = models.ForeignKey(EducType, verbose_name="Вид работы", db_column="F_EDUCTYPE_ID")
    
    year = models.IntegerField (verbose_name="Учебный год", choices=ACADEMIC_YEAR , db_column="F_EDUCWORK_YEAR")
    department = models.ForeignKey(Department, verbose_name="Кафедра", related_name="fk_subfaculty", db_column="DEP_F_DEPARTMENT_ID")
    creator = models.ForeignKey(to="auth.User", db_column="F_EDUCWORK_USER", null=True, blank=True)
    
    faculty = models.ForeignKey(Department, verbose_name="Целевой факультет", related_name="fk_faculty", db_column="F_DEPARTMENT_ID")
    eduform = models.ForeignKey(EducForm, verbose_name="Форма обучения", db_column="F_EDUCFORM_ID", null=True, blank=True)
    edustage = models.ForeignKey(EducStage, verbose_name="Вид подготовки", db_column="F_EDUCSTAGE_ID", null=True, blank=True)
    semester = models.IntegerField (verbose_name="Семестр", choices=SEMESTER , db_column="F_EDUCWORK_SEMESTER")
    
    discipline = models.ForeignKey(Discipline, verbose_name="Дисциплина", db_column="F_DISCIPLINE_ID", null=True, blank=True)
    name = models.CharField (verbose_name="Название работы", db_column="F_EDUCWORK_NAME", max_length=1000, null=True, blank=True)
    
    
    week_count = models.IntegerField (verbose_name="Кол-во недель",  db_column="F_EDUCWORK_WEEK",null=True, blank=True)
    hour_count = models.IntegerField (verbose_name="Кол-во часов в неделю",  db_column="F_EDUCWORK_HOUR",null=True, blank=True)
    
    
    studbudjet = models.IntegerField (verbose_name="Кол-во бюджетных студентов", db_column="F_EDUCWORK_STUDBUDJET", null=True, blank=True)
    studpaid = models.IntegerField (verbose_name="Кол-во платных студентов", db_column="F_EDUCWORK_STUDPAID", null=True, blank=True)
    work_count = models.IntegerField (verbose_name="Кол-во работ", db_column="F_EDUCWORK_WORKCOUNT", null=True, blank=True)
    page_count = models.IntegerField (verbose_name="Кол-во страниц", db_column="F_EDUCWORK_PAGECOUNT", null=True, blank=True)
    keyparent = models.IntegerField (verbose_name="Ключ объекта в ИСТИНА", db_column="F_EDUCWORK_KEYPARENT", null=True, blank=True)
    
    workers = models.ManyToManyField(Worker, related_name='workers', through= "EducWorkMan")
    
class EducWorkMan(MyModel, Activity):
    class Meta:
        db_table = "EDUCWORKMAN"
        verbose_name = u"доля участия струдника в педагогической нагрузке"
    
    id = models.AutoField(primary_key=True, db_column="F_EDUCWORKMAN_ID")
    worker_id = models.ForeignKey(Worker, verbose_name="Сотрудник", db_column="F_MAN_ID")
    educwork_id = models.ForeignKey(EducWork, verbose_name="Педагогическая нагрузка", db_column="F_EDUCWORK_ID")
    educworkman_part = models.IntegerField (verbose_name="доля участия",  db_column="F_EDUCWORKMAN_PART",null=True, blank=True)
        
class EducGroup (MyModel, Activity):
    class Meta:
        db_table = "EDUCGROUP"
        verbose_name = u"учебные группы, указанные в нагрузке"
    
    id = models.AutoField(primary_key=True, db_column="F_EDUCGROUP_ID")
    educwork_id = models.ForeignKey(EducWork, verbose_name="Нагрузка", db_column="F_EDUCWORK_ID")
    educgroup_name = models.CharField (verbose_name="Учебная группа", db_column="F_EDUCGROUP_NAME", max_length=255)
    studygroup_id = models.ForeignKey(StudyGroup , verbose_name="Учебная группа", db_column="F_STUDYGROUPS_ID")   

    
    

    