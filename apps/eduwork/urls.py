# -*- coding: utf-8; -*-
from django.views.generic.simple import direct_to_template
from django.conf.urls.defaults import *


urlpatterns = patterns('eduwork.views',
                       # add
                       url(r'^add/$', 'eduwork_add', name = 'eduwork_add'),
                       url(r'^list/$', 'eduwork_list',  name='eduwork_list'),
                       url(r'^add_row/(?P<department>\d+)/(?P<year>\d+)/$', 'eduwork_add_row',  name='eduwork_add_row'),
                       url(r'^copy_row/(?P<id>\d+)/$', 'eduwork_copy_row',  name='eduwork_copy_row'),
                       url(r'^edit_row/(?P<id>\d+)/$', 'eduwork_edit_row',  name='eduwork_edit_row'),
                       url(r'^delete_row/(?P<id>\d+)/$', 'eduwork_delete_row',  name='eduwork_delete_row'),
                       url(r'^add_row/(?P<department>\d+)/(?P<year>\d+)/$', 'eduwork_add_row',  name='eduwork_add_row'),
                       url(r'^educgroup_list/(?P<id>\d+)/$', 'eduwork_educgroup_list',  name='eduwork_educgroup_list'),
                       
                       
)

