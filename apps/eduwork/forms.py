# -*- coding: utf-8; -*-
from random import choice

from cProfile import label

from actstream import action
from django import forms
from django.core import serializers
from django_extensions.jobs import yearly
from django.db.models import Q
from django.forms.widgets import HiddenInput
from django.forms import fields
from django import forms

from common.forms import MyForm, MyModelForm, WorkershipModelForm, CountryChoiceField
from common.widgets import LightSelect2ChoiceWidget

from organizations.forms import SubFacultyDepartmentChoiceField

from models import *
from pmodel.models import *






class EduWorkFilterForm(MyModelForm):
    
    department = forms.ModelChoiceField(queryset=Department.objects.filter(name__istartswith="Кафедра").order_by("name"), widget=LightSelect2ChoiceWidget(width=500))
    
    
    class Meta:
        model = EducWork
        exclude = ('faculty', 'eductype', 'week_count', 'semester', 'workers')
        
    def __init__(self, *args, **kwargs):
        super(EduWorkFilterForm, self).__init__(*args, **kwargs)
        self.fields['year'].initial = 2015
        
    
        

           
class EduWorkDetailForm(MyModelForm):
    
    faculty = forms.ModelChoiceField(queryset=Department.objects.filter(name__contains="факультет").order_by("name"), widget=LightSelect2ChoiceWidget(width=300))
    eductype = forms.ModelChoiceField(queryset= EducType.objects.all().order_by("name"), widget=LightSelect2ChoiceWidget(width=300, attrs={'onChange': 'f_action(1);'}) )
    eduform = forms.ModelChoiceField(queryset= EducForm.objects.all().order_by("name"))
    edustage = forms.ModelChoiceField(queryset= EducStage.objects.all().order_by("name"))
    discipline = forms.ModelChoiceField(queryset=Discipline.objects.all().order_by("name"), widget=LightSelect2ChoiceWidget(width=300))
    
    class Meta:
        model = EducWork
        exclude = ('department', 'year', 'workers')     

    def __init__(self, *args, **kwargs):
        
        department = kwargs.pop('dep', None)
        super(EduWorkDetailForm, self).__init__(*args, **kwargs)
        
        obj_department = Department.objects.get(id=department)
        
        self.fields['discipline'].queryset = Discipline.objects.all().filter(departments = obj_department).order_by("name")
         
        for field in self.fields:
            self.fields[field].widget.attrs['readonly'] = 'readonly'
            
            
        
    def LoadParam(self, eductype):
        educTypePar =EducTypePar() 
        for field in self.fields:
            if not educTypePar.GetParamReadonly(eductype, field):
                self.fields[field].widget.attrs.pop("readonly", None)

        
class EducGroupListForm(forms.Form):
    
    educgroups = StudyGroup.objects.all()
    educwork_id = 0
             

    def __init__(self, *args, **kwargs):
        self.educwork_id = kwargs.pop('educwork_id', None)
        faculty_id = kwargs.pop('faculty_id', None)
        super(EducGroupListForm, self).__init__(*args, **kwargs)
        self.educgroups = StudyGroup.objects.filter(department_id = faculty_id )
        self.ReLoad()
                    
            

    def SaveEducGroup(self, list_group):    
        #удаляем кто не отмечен    
        for educgroup in EducGroup.objects.filter(educwork_id = self.educwork_id):
            if educgroup.studygroup_id not in list_group:
                educgroup.delete()
        
        #добавляем кто отмечен 
        for studgropup_id in list_group:           
            if EducGroup.objects.filter(educwork_id = self.educwork_id).filter(studygroup_id=studgropup_id ).count() ==0:
                obj_educwork = EducWork.objects.get(id = self.educwork_id)
                obj_studygroup = StudyGroup.objects.get(id = studgropup_id)
                edugroup = EducGroup(educwork_id = obj_educwork ,  educgroup_name=obj_studygroup.studygroup_name, studygroup_id = obj_studygroup)
                edugroup.save()
        self.ReLoad()        
                
    def ReLoad(self):        
        for studgroup in self.educgroups:
            studgroup.in_educwork = EducGroup.objects.filter(educwork_id = self.educwork_id).filter(studygroup_id=studgroup.id ).count() >0 
