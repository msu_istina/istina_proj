# -*- coding: utf-8; -*-
__author__ = 'goldan'
import authority
from common.permissions import MyModelPermission
from achievements.models import Achievement


class AchievementPermission(MyModelPermission):
    label = 'achievement_permission'
    checks = ('check_sign',)

    def check_sign(self, achievement):
        return achievement.creator == self.user or achievement.is_leader(self.user)

authority.register(Achievement, AchievementPermission)
