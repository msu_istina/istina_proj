# -*- coding: utf-8 -*-
import datetime
from django.shortcuts import get_object_or_404

from sqlreports.models import CheckboxesProcessor
from unified_permissions import has_permission_to_department
from organizations.models import Department

from .models import Achievement, AchievementMark, AchievementStatusType


class XHRAchmarkValProcessor(CheckboxesProcessor):
    codename="achievements_marks_processor"

    def update_field(cls, sqlreport, record_id, column_code, new_value, request):
        try:
            if column_code.upper() == 'F_ACHMARK_VAL':
                new_value = int(new_value)
                achievement_id, department_id = map(int, record_id.split(','))
                department = get_object_or_404(Department, pk=department_id)
                if not has_permission_to_department(request.user, 'confirm_achievements', department) and not has_permission_to_department(request.user, 'is_representative_for', department):
                    return u'Недостаточно прав для редактирования оценки'
                achievement = get_object_or_404(Achievement, pk=achievement_id)
                mark = AchievementMark.objects.create(achievement=achievement, value=new_value, department=department, creator=request.user, date=datetime.datetime.now().replace(microsecond=0))
                mark.save()
                if achievement.is_signed and not achievement.is_confirmed and new_value > 0:
                    sttype = AchievementStatusType.objects.get(code='CONFIRM')
                    achievement.setstatus(sttype, request.user)
                return 'ok'
        except Exception as e:
            return u'Не удалось сохранить данные: %s' % e
