$(function() {

    $('.datepicker').datepicker({ changeMonth: true, changeYear: true, yearRange: '1930:2020' });

    // add more forms to aliases formset
    $('#aliases_form .formset_add_form').click(function() { // FIXME add event.preventDefault()?
        formset_add_form('#aliases_form div.form_in_formset:last', current_step);
        $('#aliases_form .datepicker').removeClass('hasDatepicker').datepicker({ changeMonth: true, changeYear: true, yearRange: '1930:2020' });
        $(".autocomplete_workers, .autocomplete_publication").autocomplete("destroy");
        $(".autocomplete_workers, .autocomplete_publication").removeData('autocomplete');
        $(".autocomplete_workers").autocomplete(generate_schoolmembers_autocomplete_options("/schools/search/member/"));
        $(".autocomplete_publication").autocomplete(generate_schoolpubs_autocomplete_options("/schools/search/pub/"));
    });

   // remove submit on enter for worker autocomplete field
    $("#input_authors, .autocomplete_workers, .autocomplete_publication").live("keypress", function(e){
        if(e.which == 13) {
            e.preventDefault();
            return false
        }
        return true
    });

    function generate_schoolmembers_autocomplete_options(source){
        return {
            minLength: 0,
            autoFill: true,
            source: function(request, response){
                $.getJSON(source,
                          {
                            term: extractLast(request.term),
                            dep: $('#department_filter').val()
                          },
                          response);
            },
            search: function(){
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 3) {
                    return false;
                }
            },
            focus: function(){
                return false
            },
            select: function(event, ui){
                //var data = $.parseJSON(this.value);
                $.find('#w_id')[0].value = ui.item.value.id;
                $.find('#w_autocomplete')[0].value = ui.item.value.fullname;
                return false;

                $(this).closest('div .form').find('.w_firstname')[0].value = ui.item.value.firstname;
                $(this).closest('div .form').find('.w_middlename')[0].value = ui.item.value.middlename;
                $(this).closest('div .form').find('.w_lastname')[0].value = ui.item.value.lastname;
                //$(this).closest('div .form').find('.w_email')[0].value = ui.item.value.email;
                $(this).closest('div .form').find('.w_maindep option')
                    .each(function() { this.selected = (this.value == ui.item.value.maindep); });
                return false;
            }
        }
    }



    function generate_schoolpubs_autocomplete_options(source){
        return {
            minLength: 0,
            source: function(request, response){
                $.getJSON(source, 
                          {
                            term: extractLast(request.term)
                          },
                          response);
            },
            search: function(){
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 5) {
                    return false;
                }
            },
            focus: function(){
                return false
            },
            select: function(event, ui){
                $.find('#p_id')[0].value = ui.item.value.id;
                $.find('#p_type')[0].value = ui.item.value.type;
                this.value = ui.item.value.citation;

                // $(this).closest('div .form').find('.p_activity_id')[0].value = ui.item.value.id;
                // $(this).closest('div .form').find('.p_activity_type')[0].value = ui.item.value.type;
                return false;
            }
        }
    }


    //my add
    $("#input_authors").autocomplete(generate_workers_autocomplete_options("/workers/search/"))
    //

    // add autocomplete for workers and other fields
    $(".autocomplete_workers").autocomplete(generate_schoolmembers_autocomplete_options("/achievements/search/members/"));


    // add autocomplete for workers and other fields
    $(".autocomplete_publication").autocomplete(generate_schoolpubs_autocomplete_options("/schools/search/pub/"));



    // =================================  dynatree  ================================


		$("#tree_grnti").dynatree({
			title: "Classificator tree",
			fx: { height: "toggle", duration: 200 },
			autoFocus: false, // Set focus to first child, when expanding or lazy-loading.
			checkbox: true,
			selectMode: 3,
      initAjax: {
              url: "/schools/get_rub/",
              data: { rub: "GRNTI" } // data passed to the query
      },

			onActivate: function(node) {
				$("#echoActive").text("" + node + " (" + node.getKeyPath()+ ")");
			},

			onLazyRead: function(node){
        // Load next level
        node.appendAjax({
           	    url: "/schools/get_rub/",
		            data: {node_id: node.data.key}
        });
			},

			onSelect: function(select, node) {
				// Display list of selected nodes
				var selNodes = node.tree.getSelectedNodes();
				
				var selKeys = $.map(selNodes, function(node){
             return node.data.key
             // convert to title/key array
					   //return "[" + node.data.key + "]: '" + node.data.title + "'";
				});
        var keys=selKeys.join(",");
        $("#id_rubs_scopus-grnti_rubs").val(keys);
			},

			onClick: function(node, event) {
				// We should not toggle, if target was "checkbox", because this
				// would result in double-toggle (i.e. no toggle)
				//if( node.getEventTargetType(event) == "checkbox" )
				//	node.toggleSelect();

				//if( node.getEventTargetType(event) == "title" )
				//	node.toggleSelect();
			},
			onKeydown: function(node, event) {
				if( event.which == 32 ) {
					node.toggleSelect();
					return false;
				}
			},
			// The following options are only required, if we have more than one tree on one page:
			cookieId: "dynatree-grnti",
			idPrefix: "dynatree-grnti-"

		});


    // =========================== Scopus tree =====================
		$("#tree_scopus").dynatree({
			title: "Classificator tree",
			fx: { height: "toggle", duration: 200 },
			autoFocus: false, // Set focus to first child, when expanding or lazy-loading.
			checkbox: true,
			selectMode: 3,

      initAjax: {
              url: "/schools/get_rub/",
              data: { rub: "Scopus" } // data passed to the query
      },

			onActivate: function(node) {
				//$("#echoActive").text("" + node + " (" + node.getKeyPath()+ ")");
			},

			onLazyRead: function(node){
        // Load next level
        node.appendAjax({
           	    url: "/schools/get_rub/",
		            data: {node_id: node.data.key}
        });
			},
			onSelect: function(select, node) {
				// Display list of selected nodes
/*Anton*/				var selNodes = node.tree.getSelectedNodes();
				
				var selKeys = $.map(selNodes, function(node){
             return node.data.key
				});
        var keys=selKeys.join(",");
        $("#id_rubs_scopus-scopus_rubs").val(keys);/**/
			},

			onClick: function(node, event) {
			},

			onKeydown: function(node, event) {
				if( event.which == 32 ) {
					node.toggleSelect();
					return false;
				}
			},

			// The following options are only required, if we have more than one tree on one page:
			cookieId: "dynatree-scopus",
			idPrefix: "dynatree-scopus-"


    });




       function collapseAll(tree_divname){
            $(tree_divname).dynatree("getRoot").visit(function(node){
                node.expand(false);
            });
            return false;
        }

       function showChecked(tree_divname){
            $(tree_divname).dynatree("getRoot").visit(function(node){
                if(node.isSelected()) {
                  node.makeVisible();
                }
            });
            return false;
        }


       // bind buttons scripts
       $("#btnShowChecked").click(function(){
           return showChecked("#tree_grnti");
       });

       $("#btnShowChecked_Scopus").click(function(){
           return showChecked("#tree_scopus");
       });

       $("#btnCollapseAll").click(function(){
            return collapseAll("#tree_grnti");
       });

       $("#btnCollapseAll_Scopus").click(function(){
            return collapseAll("#tree_scopus");
       });


    var retry=0;
  
    function init_selection(divid, fieldid) {
      if($(divid).length == 0) {
		return;
	  }
      var t = $(divid).dynatree("getTree");
      var ms = 500; // 0.5 sec delay
      var max_retries = 50;
      retry++;
      if(t==null || t.isInitializing()) {
        setTimeout(function(){ init_selection(divid, fieldid);}, ms);
        return;
      }
      var ids = $(fieldid).val().split(",");
      var found = false;
      for(var i=0; i < ids.length; i++) {
         if(ids[i].length > 0) {
            var node = t.getNodeByKey(ids[i]);
            if(node != null) {
               node.select();
               node.makeVisible();
               found = true;
            }
         }
      }
      if(!found && retry<max_retries) {
        setTimeout(function(){ init_selection(divid, fieldid);}, ms); // the tree is not ready
        return;
      }
    }

/*Anton*/ 	init_selection("#tree_grnti", "#id_rubs_scopus-grnti_rubs");
   	init_selection("#tree_scopus", "#id_rubs_scopus-scopus_rubs");
});

