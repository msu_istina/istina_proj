# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required

from django.contrib import messages
from django.contrib.auth.models import User

from common.utils.autocomplete import autocomplete_helper
from common.utils.uniqify import uniqify
from common.utils.latex import latex_to_pdf
from django.template.loader import render_to_string
from django.utils import simplejson
import datetime

from django.shortcuts import render_to_response
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse


from django.utils.decorators import method_decorator

from django.db import connection, transaction
from django.db.models import Q

from itertools import chain

#from django.template.loader import render_to_string
#from django.core.context_processors import csrf

from models import Achievement, AchievementDescr, AuthorAch, AchievementRub, MyLanguage, AchievementPub, AchievementFile, AchievementStatusType
from models import AchievementMark
from journals.models import JournalRubric
from workers.models import Worker, Profile
from organizations.models import Department
from organizations.utils import is_representative_for

from forms import AchievementForm, AchievementDescrForm, AchievementPubsForm, AchievementAuthorshipForm, AchievementRubsForm, AchievementFilesForm
from forms import AchievementMarkForm

from unified_permissions.models import AllManagedWorkers, AllGrantedPermissions
from unified_permissions import has_permission

# Create your views here.

ACHIEVEMENTS_ADMINS=['safonin', 'S.Y.Egorov','Reznikova_Alexandra','SvetaZyk','I.Sukhareva', 'smamakina', 'suminaistina']

# Используются следующие статусы:
#  SIGN - подписано автором
#  REEDIT - администратор вернул на доработку
#  CONFIRM - утверждено зам. декана
#



lang_ru = 1

def is_representative_for_ach(user, theAch, exclude_authors=False):
    if user.username in ACHIEVEMENTS_ADMINS:
        return True

    ach_departments_ids = AuthorAch.objects.filter(achievement_id=theAch, author__departments__isnull=False).values('author__departments')

    # Достижение без авторов оценивается по создателю
    if not ach_departments_ids:
        ach_departments_ids = Achievement.objects.filter(pk=theAch.id).values('creator__profile__worker__departments')

    if not exclude_authors:
        if user == theAch.creator or user in [aa.author for aa in theAch.authorship.all()]:
            return True

    return AllGrantedPermissions.objects.current \
            .filter(Q(user=user),
                    Q(role_name__in=('confirm_achievements', 'is_representative_for')),
                    Q(department__in=ach_departments_ids)) \
            .count() > 0



def check_permissions(request, theAch, operation='edit'):
    is_repr = is_representative_for_ach(request.user, theAch)

    if theAch.is_signed and theAch.creator == request.user and not is_repr:
        messages.warning(request, u'Форма была подписана. Изменение данных невозможно. Обратитесь к <a href="/help/feedback/">ответственному</a>.')
        return False

    if operation == 'SIGN' and not (has_permission(request.user, "sign", theAch) or is_repr):
        messages.warning(request, u'Извините, у Вас нет прав для выполнения данной операции.')
        return False
    if operation in ['CONFIRM', 'REEDIT']:
        if not is_repr:
            messages.warning(request, u'Извините, у Вас нет прав для выполнения данной операции.')
            return False
        else:
            return True

    if operation == 'delete':
        if theAch.creator == request.user or is_repr:
            return True
        else:
            messages.warning(request, u'Извините, у Вас нет прав для выполнения данной операции.')
            return False

    if operation == 'edit':
        if is_repr:
            return True
        if theAch.is_signed:
            messages.warning(request, u'Форма была подписана. Изменение данных невозможно.')
            return False
        if theAch.creator != request.user:
            messages.warning(request, u'Извините, у Вас нет прав для редактирования данной записи.')
            return False
    if theAch.creator != request.user and not is_repr:
        messages.warning(request, u'Извините, у Вас нет прав для выполнения данной операции.')
        return False
    return True

@login_required
def edit_descr(request, aid):
    template = "add_edit.html"
    theAch = get_object_or_404(Achievement, pk=aid)
    if not check_permissions(request, theAch, 'edit'):
        return redirect("print", aid=theAch.id)

    theAchDescr = None
    all_descrs = theAch.descriptions
    if all_descrs.count() > 0:
        theAchDescr = all_descrs.all()[0]
    form = AchievementDescrForm(request.POST or None, instance=theAchDescr)

    if request.method == 'POST' and request.POST.get('next', None) and form.is_valid():
        achform = AchievementForm(request.POST)
        ach = achform.save(commit=False)
        theAch.pnr = ach.pnr
        theAch.save()

        achievement_descr = form.save(commit=False)
        achievement_descr.achievement_id = theAch
        achievement_descr.language = MyLanguage.objects.get(pk=lang_ru)

        temp_description = achievement_descr.description
        achievement_descr.description = ''

        achievement_descr.save()
        achievement_descr.description = temp_description
        achievement_descr.save()

        return redirect("edit_publications", aid=theAch.id)

    achform = AchievementForm(instance=theAch)
    return render(request, template,
                  {"form": form, "achform":  achform, "ach": theAch})


@login_required
def edit_publications(request, aid=None):
    template = "add_edit_publications.html"
    theAch = get_object_or_404(Achievement, pk=aid)
    if not check_permissions(request, theAch, 'edit'):
        return redirect("print", aid=theAch.id)

    form = AchievementPubsForm(request.POST or None)
    if request.method == 'POST' and not request.POST.get('next', None) and form.is_valid():
        pub = form.save(commit=False)
        pub.achievement_id = theAch
        pub.save()
        cursor = connection.cursor()
        tables = {"article": "authora", "book": "authorb", "patent": "authorp"}
        if tables.get(pub.activity_type, None):
            sql = """
            INSERT INTO authorach (f_authorach_id, f_achievement_id, f_man_id)
            (SELECT seq_new_id.nextval, %(achid)s, f_man_id
            FROM %(table)s aa
            WHERE aa.f_%(objtype)s_id = %(pub_id)s
              AND aa.f_man_id IS NOT NULL
              AND aa.f_man_id NOT IN (SELECT f_man_id FROM authorach where f_achievement_id = %(achid)s)
             )
             """ % {"achid": theAch.id,
                    "table": tables[pub.activity_type],
                    "objtype": pub.activity_type,
                    "pub_id": pub.activity_id}
            cursor.execute(sql)

    form = AchievementPubsForm() # reset to empty form =>  clear autocomplete field
    return render(request, template,
                  {"ach": theAch, "form": form})


def autocomplete_search_members(request):
    '''Server side for jquery autocomplete
    Returns objects of specified type matching query term.
    '''
    term = request.GET.get('term', '').strip()
    dep = request.GET.get('dep', '')
    fields = []
    limit = 40
    term_words = term.replace(".", " ").split()
    if len(term) >= 3:
        workers = Worker.objects
        for term, name in zip(term_words, ("lastname", "firstname", "middlename")):
            key = name + "__istartswith"
            workers = workers.filter(**{key: term})
        workers = workers.order_by( "employments__department__name", "lastname", "firstname")[:limit]

        message_data = [{'label': w.fullname + (u'; (сотрудник)' if w.current_employments else ''),
                         'value': {'id': w.id,
                                   'fullname': w.fullname,
                                   'firstname': w.firstname, 'middlename': w.middlename, 'lastname': w.lastname,
                                   'degree': w.degrees.all()[0].degree.name if w.degrees.all() else '',
                                   'maindep': w.current_employments[0].department.id if w.current_employments else 0,
                                   }
                         } for w in workers]
        #fields = [{'label': item.fullname, 'value': item.get_fullname(initials=True)} for item in workers]
        #fields = uniqify(fields, lambda i: i['value'])
        message_data = uniqify(message_data, lambda i: i['value']['id'])
    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')


@login_required
def toggle_leader(request, aid, auid):
    theAch = get_object_or_404(Achievement, pk=aid)
    if not check_permissions(request, theAch, 'edit'):
        return redirect("edit_auth", aid=theAch.id)
    try:
        au = get_object_or_404(AuthorAch, achievement_id=theAch.id, id=auid)
        au.leader = 0 if au.leader == 1 else 1
        au.save()
    except AuthorAch.DoesNotExist:
        warning(request, u'Автор не найден')
    return redirect("edit_auth", aid=theAch.id)


@login_required
def edit_auth(request, aid=None):
    template = "add_edit_auth.html"
    theAch = get_object_or_404(Achievement, pk=aid)
    if not check_permissions(request, theAch, 'edit'):
        return redirect("print", aid=theAch.id)

    form = AchievementAuthorshipForm(request.POST or None)
    if request.method == 'POST' and not request.POST.get('next', None) and form.is_valid():
        try:
            man_id = int(request.POST[u'w_id'])
            worker = get_object_or_404(Worker, pk=man_id)
            author = form.save(commit=False)
            author.author = worker;
            author.achievement_id = theAch
            author.save()
        except ValueError:
            pass

    authors = theAch.authorship.all()
    return render(request, template,
                  {"ach": theAch})


@login_required
def edit_rubs(request, aid=None):
    template = "add_edit_rubs.html"
    theAch = get_object_or_404(Achievement, pk=aid)
    if not check_permissions(request, theAch, 'edit'):
        return redirect("print", aid=theAch.id)

    form = AchievementRubsForm(request.POST or None)

    if request.method == 'POST' and request.POST.get('savenext', None) and form.is_valid():
        #AchievementRub.objects.filter(achievement_id_id=theAch.id).delete()
        sql = "delete from RUBACHIEVEMENT where F_ACHIEVEMENT_ID = %(achid)s" % {"achid": theAch.id }
        cursor = connection.cursor()
        cursor.execute(sql)
        connection.commit()

        for field_name in ['grnti_rubs', 'scopus_rubs']:
            for rub in form.cleaned_data.get(field_name):
                rublink = AchievementRub(achievement_id=theAch, rub_id=rub)
                rublink.save()
        return redirect("edit_files", aid=theAch.id)
    else:
        rubs = list(AchievementRub.objects.filter(achievement_id=theAch).select_related('rub_id__categorization__code'))
        form = AchievementRubsForm({
            "grnti_rubs": [','.join(set(unicode(rub.rub_id.id) for rub in rubs if rub.rub_id.categorization.code == 'GRNTI'))],
            "scopus_rubs": [','.join(set(unicode(rub.rub_id.id) for rub in rubs if rub.rub_id.categorization.code == 'Scopus'))]
        })

    return render(request, template,
                  {"ach": theAch, "form": form})



@login_required
def edit_files(request, aid=None):
    template = "add_edit_files.html"
    theAch = get_object_or_404(Achievement, pk=aid)
    if not check_permissions(request, theAch, 'edit'):
        return redirect("print", aid=theAch.id)

    form = AchievementFilesForm()
    if request.method == 'POST' and not request.POST.get('next', None):
        form = AchievementFilesForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = form.save(commit=False)
            newdoc.achievement_id = theAch
            newdoc.save()
            form = AchievementFilesForm()

    return render(request, template,
                  {"ach": theAch, "form": form})


@login_required
def add(request):
    if request.method == 'POST':
        theAch = Achievement.objects.create(creator = request.user,
                                            date=datetime.datetime.now().replace(microsecond=0))
        theAch.save()
        return redirect("edit_descr", aid=theAch.id)
    return render(request, "add.html")




@login_required
def index(request):
    myAchs = Achievement.objects.filter(
        Q(creator = request.user)
        | Q(authorship__author = request.user.get_profile().worker)).distinct().order_by('-id')
    is_representative = None
    if request.user.representatives.exists():
        is_representative = True
    return render(request, "index.html",
                  {"myAchs": myAchs,
                   "me": request.user,
                   "repr": is_representative})

@login_required
def all(request, year=None, department_id=None):
    # FIXME: refactor sqlreports to avoid duplicating code
    from sqlreports.views import run_report, getdropdownlist, get_report_params, generate_adata
    from sqlreports.models import SQLReport, SqlRepPar
    from django.core import signing

    signer = signing.TimestampSigner(salt=generate_adata(request))
    report = SQLReport.objects.get(code='achievements_by_year')

    # mute get_report_params warnings
    old_level = messages.get_level(request)
    messages.set_level(request, 10000)
    default_values, missed_params = get_report_params(request, report, signer)
    messages.set_level(request, old_level)

    if request.user.is_superuser and 'user_id' in request.GET:
        default_values['user_id'] = request.GET.get('user_id')

    datepar = SqlRepPar.objects.filter(repid=report.id).order_by("ord")
    dparam = list()
    for dp in datepar:
        param_info = {'object': dp, 'type' : dp.type, 'name' : dp.name,
                        'id' : "id_%s" % dp.ord, "maxlen" : dp.maxlen,
                         "defvalue" : dp.defvalue }
        if dp.is_hidden():
            # if user_supplied value for hidden parameter apears here it
            # passed signature testin in get_report_params function. So
            # we can simply sign the value.
            param_info["defvalue"] = signer.sign(default_values[dp.code] or dp.defvalue)
            param_info['type'] = 'hidden'
        elif dp.type == 'text':
            param_info["maxlen"] = dp.maxlen
        elif dp.type == 'select':
            if dp.listsql:
                sel_list = getdropdownlist(dp.listsql, dpt=default_values)
            else:
                sel_list = getdropdownlist("select f_sqlreplist_value id,f_sqlreplist_name name from SQLREPLIST where f_sqlreppar_id='%s'" % dp.id)
            dlist = list()
            for sl in sel_list:
                dlist.append({'id' : signer.sign(sl[0]), 'name' : sl[1]})
            param_info["list"] = dlist
            param_info["defvalue"] = signer.sign(default_values.get(dp.code, dp.defvalue))
            if dp.code not in default_values or not default_values[dp.code]:
                default_values[dp.code] = unicode(dp.defvalue or sel_list and sel_list[0] and sel_list[0][0])
        dparam.append( param_info )

    results = run_report(report, default_values, signer)

    if request.POST and "PDF" in request.POST:
        try:
            spacing = float(request.POST.get("spacing", "2.0"))
        except Exception:
            spacing = 2.0
        if spacing <= 0.1 or spacing > 4.0:
            spacing = 2.0

        dep_id = int(default_values['department_id'])
        dep = Department.objects.get(id=dep_id) if dep_id > 0 else { 'name': 'Московский государственный университет им.~М.~В.~Ломоносова' }

        ach_ids = map(lambda i: i['checkbox_id'].split(',')[0], results.data)
        order = dict([(id, i) for i, id in enumerate(ach_ids)])

        latex = render_to_string("achievements/list_selected.tex",
                                 {"achs": sorted(Achievement.objects.filter(id__in=ach_ids), key=lambda ach: order[unicode(ach.id)]),
                                  "dep": dep,
                                  "year": default_values['year'],
                                  "spacing": spacing,
                                  })
        pdf = latex_to_pdf(latex)
        response = HttpResponse(pdf, mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=achievements' + '.pdf'
        return response

    report.header += render_to_string('achievements/sqlreport-dparams.html', { 'dparam': dparam, 'adata': generate_adata(request), 'url': reverse('achievements_list_all') }, context_instance=RequestContext(request))
    return render(request, "sqlreports/generic_table.html",
        {'base_template': 'base.html',
            'report': report,
            'data': results.data,
            'headers': results.headers,
            'title': report.name,
            'table_header': report.name,
            'template': { 'link': None },
            'checkboxes': { 'show': False, } })


@login_required
def print_achievement(request, aid, mode='html'):
    theAch = get_object_or_404(Achievement, pk=aid)
    buttons = []
    is_repr = is_representative_for_ach(request.user, theAch, exclude_authors=True)
    visible_marks = None
    assignable_mark_departments = None
    mark_form = AchievementMarkForm()
    if has_permission(request.user, "sign", theAch) and not theAch.is_signed:
        buttons.append({'label': u'Подписать', 'tag': 'SIGN',
                        'alert': u'Дальнейшие изменения будут невозможны. Продолжить?'}
                       )
    elif request.user != theAch.creator and not theAch.is_signed and is_repr:
        buttons.append({'label': u'Подписать за автора', 'tag': 'SIGN',
                        'alert': u'Вы уверены, что хотите подписать достижение за автора?'}
                       )
    if theAch.is_signed and not theAch.is_confirmed and is_repr:
        buttons.append({'label': u'Вернуть на доработку', 'tag': 'REEDIT',
                        'alert': u'Вы действительно хотите вернуть данную запись на доработку?'}
                       )
    if theAch.is_signed and is_repr:
        ach_departments_ids = AuthorAch.objects.filter(achievement_id=theAch).values('author__departments__nested_parent_relations__parent')
        if not ach_departments_ids:
            ach_departments_ids = Achievement.objects.filter(pk=theAch.id).values('creator__profile__worker__departments__nested_parent_relations__parent')

        ach_marked_departments_qs = AchievementMark.objects.filter(achievement=theAch).values('department')
        visible_marks_deps_qs = Department.objects \
                                .filter(granted_department_roles__role_name__in=('confirm_achievements', 'is_representative_for'),\
                                        granted_department_roles__user=request.user, \
                                        id__in=ach_departments_ids) \
                                .distinct()
        assignable_mark_departments = visible_marks_deps_qs.exclude(id__in=ach_marked_departments_qs).distinct()
        visible_marks = AchievementMark.objects.filter(achievement=theAch,
                                                       department__in=visible_marks_deps_qs)

    if mode == 'pdf':
        # Определим подписанта
        signer = None
        try:
            signer = theAch.signer or request.user.get_profile().worker
        except:
            pass
        latex = render_to_string("achievements/print.tex",
                                 {"ach": theAch,
                                  "worker": signer})
        pdf = latex_to_pdf(latex)
        response = HttpResponse(pdf, mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=ach_' + str(aid) + '.pdf'
        return response


    return render(request, "print.html",
                  {"ach": theAch,
                   "is_repr": is_repr,
                   "buttons": buttons,
                   "mark_form": mark_form,
                   "visible_marks": visible_marks,
                   "assignable_mark_departments": assignable_mark_departments,
                   })


@login_required
def remove(request, aid, objtype, id):
    theAch = get_object_or_404(Achievement, pk=aid)
    if not check_permissions(request, theAch, 'edit'):
        return redirect("print", aid=theAch.id)

    redir = ""
    if objtype == 'auth':
        redir = "edit_auth"
        obj = get_object_or_404(AuthorAch, pk=id)
        obj.delete()
    if objtype == 'pub':
        redir = "edit_publications"
        obj = get_object_or_404(AchievementPub, pk=id)
        obj.delete()
    if objtype == 'file':
        redir = "edit_files"
        obj = get_object_or_404(AchievementFile, pk=id)
        obj.delete()
    return redirect(redir, aid=theAch.id)


@login_required
def delete_achievement(request, aid):
    theAch = get_object_or_404(Achievement, pk=aid)
    if not check_permissions(request, theAch, 'delete'):
        return redirect("print", aid=theAch.id)
    theAch.delete()
    return redirect("index")


@login_required
def setstatus(request, aid):
    theAch = get_object_or_404(Achievement, pk=aid)

    if request.method == 'POST':
        sttypename = request.POST.get('status', None)
        if not sttypename or not check_permissions(request, theAch, sttypename):
            messages.error(request, u'У Вас нет прав для выполнения данной операции')
            return redirect("print", aid=theAch.id)
        sttype = AchievementStatusType.objects.get(code=sttypename)
        theAch.setstatus(sttype, request.user)
    return redirect("print", aid=theAch.id)


@login_required
def setmark(request, aid, depid):
    theAch = get_object_or_404(Achievement, pk=aid)
    dep = Department.objects.get(pk=depid)

    if not is_representative_for_ach(request.user, theAch, exclude_authors=True):
        messages.error(request, u'У Вас нет прав для выполнения данной операции')
        return redirect("print", aid=theAch.id)

    form = AchievementMarkForm(request.POST or None)

    if request.method == 'POST':
        try:
            value = int(request.POST.get('value', None))
            mark = AchievementMark.objects.create(achievement=theAch,
                                                  value=value,
                                                  department=dep,
                                                  creator=request.user,
                                                  date=datetime.datetime.now().replace(microsecond=0))
            mark.save()
            if theAch.is_signed and not theAch.is_confirmed:
                sttype = AchievementStatusType.objects.get(code='CONFIRM')
                theAch.setstatus(sttype, request.user)
        except ValueError:
            messages.error(request, u"Пожалуйста, выберите значение.")
    return redirect("print", aid=theAch.id)
