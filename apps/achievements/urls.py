# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from updates_processor import XHRAchmarkValProcessor

urlpatterns = patterns(
'achievements.views',

    # public pages
    url(r'^$', 'index', name='index'),
    url(r'^add/$', 'add', name="add_achievement"),
    url(r'^delete/(?P<aid>\d+)/$', 'delete_achievement', name="delete_achievement"),
    url(r'^edit/(?P<aid>\d+)/descr/?$', 'edit_descr', name="edit_descr"),
    url(r'^edit/(?P<aid>\d+)/publications/$', 'edit_publications', name="edit_publications"),
    url(r'^edit/(?P<aid>\d+)/auth/$', 'edit_auth', name="edit_auth"),
    url(r'^edit/(?P<aid>\d+)/rubs/$', 'edit_rubs', name="edit_rubs"),
    url(r'^edit/(?P<aid>\d+)/files/$', 'edit_files', name="edit_files"),

    url(r'^(?P<aid>\d+)/toggle_leader/(?P<auid>\d+)/$', 'toggle_leader', name="toggle_leader"),

    url(r'^remove/(?P<aid>\d+)/(?P<objtype>\w+)/(?P<id>\d+)/$', 'remove', name="remove"),
    url(r'^print/(?P<aid>\d+)/((?P<mode>\w+)/)?$', 'print_achievement', name="print"),
    url(r'^all/$', 'all', name="achievements_list_all"),
    url(r'^setstatus/(?P<aid>\d+)/$', 'setstatus', name="setstatus"),
    url(r'^setmark/(?P<aid>\d+)/(?P<depid>\d+)/$', 'setmark', name="achievement_setmark"),
    url(r'^search/members/$', 'autocomplete_search_members', name='search_members'),
    #url(r'^edit/((?P<achievement_id>\d+)/)?$', 'edit', name="add_achievement"),
)
