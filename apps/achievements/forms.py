# -*- coding: utf-8; -*-
from django.forms import ModelForm, CharField
from django import forms
from django.forms.widgets import TextInput, Textarea
from django.core.urlresolvers import reverse

from achievements.models import AchievementDescr, Achievement, AchievementPub, AuthorAch, AchievementRub, AchievementFile, MyLanguage
from achievements.models import AchievementMark
from journals.models import JournalRubric
from common.dynatree_widgets import DynatreeWidget


class AchievementForm(ModelForm):
    class Meta:
        model = Achievement
        exclude = ('id', 'creator')
    def __init__(self, *args, **kwargs):
        super(AchievementForm, self).__init__(*args, **kwargs)
        self.fields['pnr'].empty_label = u"Другое"

class AchievementDescrForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(AchievementDescrForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['class'] = 'wide'
        self.fields['exttitle'].widget.attrs['id'] = 'id_info-exttitle'
        self.fields['keywords'].widget.attrs['id'] = 'id_info-keywords'
        self.fields['description'].widget.attrs['id'] = 'id_info-descr'
        self.fields['contacts'].widget.attrs['id'] = 'id_info-contacts'
        self.fields['title'].widget.attrs['placeholder'] = u'Не более 10 слов.'
        self.fields['keywords'].widget.attrs['placeholder'] = u'Не более 10 ключевых слов.'
        self.fields['exttitle'].widget.attrs['placeholder'] = u'Не более 30 слов. (для публикации на веб-сервере МГУ)'
        self.fields['description'].widget.attrs['placeholder'] = u'Просим придерживаться ориентировочного объема информации, соответствующего не более 0,5 страницы А4 шрифта 12 Times New Roman через 1 интервал.'
        self.fields['contacts'].widget.attrs['placeholder'] = u'Пожалуйста, укажите служебный телефон и/или e-mail, по желанию, мобильный телефон. Контактные данные не отображаются в Интернет и доступны только сотрудникам ректората и Управления информации и медиакоммуникации МГУ для связи с авторам по уточнению текста и возможности интервью.'
        #self.fields['id'].widget.attrs['class'] = 'hide'

    class Meta:
        model = AchievementDescr
        exclude = ('achievement_id', 'language')


class AchievementPubsForm(ModelForm):
    reference = CharField(max_length=4000, required=True, label=u'')
    activity_id = CharField(max_length=4000, required=True, label=u'')
    activity_type = CharField(max_length=4000, required=True, label=u'')

    def __init__(self, *args, **kwargs):
        super(AchievementPubsForm, self).__init__(*args, **kwargs)
        self.fields["reference"].widget = TextInput()
        self.fields['reference'].widget.attrs['class'] = 'autocomplete_publication wide'
        self.fields['reference'].widget.attrs['id'] = 'ach-reference'
        self.fields['activity_type'].widget.attrs['id'] = 'p_type'
        self.fields['activity_id'].widget.attrs['id'] = 'p_id'

        self.fields['activity_id'].widget.attrs['class'] = 'hide'
        self.fields['activity_type'].widget.attrs['class'] = 'hide'
    class Meta:
        model = AchievementPub
        exclude = ('achievement_id')



class AchievementAuthorshipForm(ModelForm):
    lastname = CharField(max_length=100, required=False, label=u'Введите начальные буквы фамилии')

    def __init__(self, *args, **kwargs):
        super(AchievementAuthorshipForm, self).__init__(*args, **kwargs)
        self.fields['lastname'].widget.attrs['class'] = 'autocomplete_workers w_lastname'

    class Meta:
        model = AuthorAch
        exclude = ('author', 'achievement_id')



class AchievementRubsForm(forms.Form):
    grnti_rubs = forms.ModelMultipleChoiceField(required=False, label=u'Рубрики ГРНТИ', queryset=JournalRubric.objects.filter(categorization__code='GRNTI'))
    scopus_rubs = forms.ModelMultipleChoiceField(required=False, label=u'Рубрики Scopus', queryset=JournalRubric.objects.filter(categorization__code='Scopus'))

    def __init__(self, *args, **kwargs):
        super(AchievementRubsForm, self).__init__(*args, **kwargs)
        self.fields['grnti_rubs'].widget = DynatreeWidget(select_mode=2,
                ajax_url=reverse('xdr_equipment_rubs'),
                ajax_extra_data={"rubcode": "GRNTI"})
        self.fields['scopus_rubs'].widget = DynatreeWidget(select_mode=2,
                ajax_url=reverse('xdr_equipment_rubs'),
                ajax_extra_data={"rubcode": "Scopus"})




class AchievementFilesForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AchievementFilesForm, self).__init__(*args, **kwargs)
        self.fields["name"].widget = TextInput()

    class Meta:
        model = AchievementFile
        exclude = ('achievement_id', 'annotation')


class AchievementMarkForm(forms.Form):
    POSSIBLE_MARKS=(
        (None, u"--- Нет значения ---"),
        (0, u"не имет отношения"),
        (1, u"1"),
        (2, u"2"),
        (3, u"3"),
        (4, u"4"),
        (5, u"5"))
    value = forms.ChoiceField(label=u"", choices=POSSIBLE_MARKS, required=True)
