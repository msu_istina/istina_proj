# -*- coding: utf-8 -*-

import datetime

from organizations.models import Department
from django.contrib.auth.models import User

from django.db import models, connection
from django.db.models import Q

from workers.models import Worker

from django.core.validators import MaxLengthValidator

from common.utils.files import validate_uploaded_file
#from publications.models import Language
#from common.models import AuthoredModel;
from common.models import MyModel, Activity
from journals.models import JournalRubric, JournalCategorization


class JournalRubricPNR(JournalRubric):
    def __unicode__(self):
        return self.name
    class Meta:
        proxy = True


class Achievement(models.Model, Activity):
    id = models.AutoField(primary_key=True, db_column="F_ACHIEVEMENT_ID")
    creator = models.ForeignKey(to=User, related_name="achievements",
                                db_column="F_ACHIEVEMENT_USER", null=True, blank=True)
    pnr = models.ForeignKey(to=JournalRubricPNR,
                            verbose_name = u'Направление программы развития',
                            db_column="F_ACHIEVEMENT_PNR", null=True, blank=True,
                            limit_choices_to = {'categorization__code': 'PNR-MSU'})
    date = models.DateTimeField(u"Дата создания", db_column="F_ACHIEVEMENT_CREATE", blank=True, null=True, auto_now_add=False)

    genitive_plural_full = u"достижений"

    @property
    def grnti(self):
        return AchievementRub.objects.filter(achievement_id=self.id, rub_id__categorization__name="GRNTI").select_related('rub_id')

    @property
    def scopus(self):
        return AchievementRub.objects.filter(achievement_id=self.id, rub_id__categorization__name="Scopus").select_related('rub_id')

    @property
    def title(self):
        title = u'-- Без названия --'
        try:
            theAchDescr = self.descriptions.all()[0]
            if len(theAchDescr.title)>0:
                title = theAchDescr.title
        except:
            pass
        return title

    def setstatus(self, statustype, user, value=None, comment=None):
        achst = AchievementStatus.objects.create(creator=user,
                                                 achievement = self,
                                                 statustype = statustype,
                                                 value=value,
                                                 comment=comment,
                                                 date=datetime.datetime.now().replace(microsecond=0))
        achst.save()
        return True

    def has_status(self, statusname, reverse = None):
        if not reverse:
            reverse = statusname
        sts = AchievementStatus.objects.filter(
            Q(achievement = self),
            Q(statustype__code=statusname) | Q(statustype__code=reverse)).order_by('-date')
        if sts.exists():
            # check that SIGN was the last event
            if sts[0].statustype.code == statusname:
                return True
        return False

    @property
    def is_signed(self):
        return self.has_status('SIGN', 'REEDIT')

    @property
    def is_confirmed(self):
        return self.has_status('CONFIRM')

    def is_leader(self, user):
        worker = user.get_profile().worker
        if AuthorAch.objects.filter(achievement_id=self, leader=1, author=worker).count() > 0:
            return True
        return False

    @property
    def signer(self):
        'Returns leader, or main author, for the achievement'
        leaders = AuthorAch.objects.filter(achievement_id=self,leader=1).order_by('-id')
        if leaders.exists():
            return leaders[0].author
        return None

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = "ACHIEVEMENT"
        verbose_name = u"Научное достижение"
        verbose_name_plural = u"Научные достижения"

class AuthorAch(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORACH_ID")
    achievement_id = models.ForeignKey(to=Achievement, related_name="authorship", db_column="F_ACHIEVEMENT_ID")
    author = models.ForeignKey(to=Worker,
                               related_name="achievement_authorships", db_column="F_MAN_ID",
                               null=True, blank=True)
    leader = models.IntegerField(u"Руководитель",
                                 db_column="F_AUTHORACH_LEADER",
                                 null=True, blank=True)
    name = models.TextField(u"Имя", validators=[MaxLengthValidator(512)],
                            db_column="F_AUTHORACH_NAME",
                            null=True, blank=True)

    def __unicode__(self):
      return self.author

    class Meta:
        db_table = "AUTHORACH"
        verbose_name = u"Авторство научного достижения"
        verbose_name_plural = u"Авторство научных достижений"


class MyLanguage(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_LANG_ID")
    name = models.TextField(u"Название", validators=[MaxLengthValidator(255)], db_column="F_LANG_NAME")
    code = models.TextField(u"Kod", validators=[MaxLengthValidator(50)], db_column="F_LANG_CODE")

    def __unicode__(self):
        return self.name


    class Meta:
        db_table = "LANG"
        verbose_name = u"Описание ispolz yazika"
        verbose_name_plural = u"Описания ispolzuem yazikov"


class AchievementRub(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_RUBACHIEVEMENT_ID")
    achievement_id = models.ForeignKey(to=Achievement, related_name="rubrics", db_column="F_ACHIEVEMENT_ID", verbose_name=u"Achievement")
    rub_id = models.ForeignKey(to=JournalRubric, db_column="F_JOURNALRUB_ID", verbose_name=u"Рубрика")
    class Meta:
        db_table = "RUBACHIEVEMENT"



class AchievementDescr(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_ACHIEVEMENTDESC_ID")
    achievement_id = models.ForeignKey(to=Achievement, related_name="descriptions", db_column="F_ACHIEVEMENT_ID")
    title = models.CharField(u"Заголовок", max_length = 512, validators=[MaxLengthValidator(512)],
                             null = False, blank = False,
                             db_column="F_ACHIEVEMENTDESC_NAME")
    exttitle = models.TextField(u"Развернутый заголовок",
                                max_length = 1000, validators=[MaxLengthValidator(1000)],
                                null = True, blank = True,
                                db_column="F_ACHIEVEMENTDESC_EXTNAME")
    language = models.ForeignKey(to=MyLanguage, related_name="Yazik", db_column="F_LANG_ID")
    keywords = models.TextField(u"Ключевые слова",
                                null = True, blank = True,
                                validators=[MaxLengthValidator(500)],
                                db_column="F_ACHIEVEMENTDESC_KEYWORDS")
    contacts = models.TextField(u"Контактные данные",
                                null = True, blank = True,
                                validators=[MaxLengthValidator(900)],
                                db_column="F_ACHIEVEMENTDESC_CONTACTS")
    description = models.TextField(u"Описание",
                                   null = True, blank = True,
                                   validators=[MaxLengthValidator(4200)],
                                   db_column="F_ACHIEVEMENTDESC_TEXT")


    def __unicode__(self):
        return self.title

    @property
    def authors(self):
        return AuthorAch.objects.filter(achievement_id=self.achievement_id);

    @property
    def grnti(self):
        return AchievementRub.objects.filter(achievement_id=self.achievement_id, rub_id__categorization__name="GRNTI")

    @property
    def scopus(self):
        return AchievementRub.objects.filter(achievement_id=self.achievement_id, rub_id__categorization__name="Scopus")

    class Meta:
        db_table = "ACHIEVEMENTDESC"
        verbose_name = u"Описание научного достижения"
        verbose_name_plural = u"Описания научных достижений"


class AchievementFile(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_ACHIEVEMENTFILE_ID")
    achievement_id = models.ForeignKey(to=Achievement, related_name="files",
                                       db_column="F_ACHIEVEMENT_ID",
                                       verbose_name=u"Achievement's file")
    docfile = models.FileField(u"Файл", upload_to="achievements/files/%Y/%m/%d",
                               max_length=255, validators=[validate_uploaded_file],
                               db_column="F_ACHIEVEMENTFILE_FILE")
    name = models.TextField(u"Описание", max_length = 250, validators=[MaxLengthValidator(1000)],
                            help_text = u'Например, подпись к фотографии',
                            db_column="F_ACHIEVEMENTFILE_NAME")
    annotation = models.TextField(u"Аннотация", validators=[MaxLengthValidator(1000)],
                                  db_column="F_ACHIEVEMENTFILE_ANNOTATION")

    def get_absolute_url(self):
        return self.docfile.url

    class Meta:
        db_table = "ACHIEVEMENTFILE"


class AchievementPub(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_ACHIEVEMENTPUB_ID")
    achievement_id = models.ForeignKey(to=Achievement, related_name="publications", db_column="F_ACHIEVEMENT_ID", verbose_name=u"Achievement's file")
    reference = models.TextField(u"Библиографическая ссылка", validators=[MaxLengthValidator(650)], db_column="F_ACHIEVEMENTPUB_REFERENCE")
    activity_type = models.TextField(u"Тип работы", validators=[MaxLengthValidator(1200)], db_column="F_ACTIVITY_TYPE")
    activity_id = models.TextField(u"Ключ статьи", validators=[MaxLengthValidator(1200)], db_column="F_ACTIVITY_ID")

    class Meta:
        db_table = "ACHIEVEMENTPUB"


class AchievementStatusType(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_ACHSTATUSTYPE_ID")
    name = models.TextField(u"Название", db_column="F_ACHSTATUSTYPE_NAME")
    code = models.CharField(u"Код статуса", max_length=1000, db_column="F_ACHSTATUSTYPE_CODE")
    class Meta:
        db_table = "ACHSTATUSTYPE"


class AchievementStatus(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_ACHSTATUS_ID")
    creator = models.ForeignKey(to=User, related_name="achstatuses",
                                db_column="F_ACHSTATUS_USER",
                                null=False, blank=False)
    achievement = models.ForeignKey(to=Achievement, related_name="statuses",
                                    db_column="F_ACHIEVEMENT_ID",
                                    verbose_name=u"Achievement")
    statustype = models.ForeignKey(to=AchievementStatusType, related_name="statuses",
                                   db_column="F_ACHSTATUSTYPE_ID")
    value = models.IntegerField(db_column="F_ACHSTATUS_VAL",
                                verbose_name=u"Значение",
                                blank=True, null=True)
    comment = models.TextField(u"Комментарий", validators=[MaxLengthValidator(1000)],
                               db_column="F_ACHSTATUS_COMMENT",
                               blank=True, null=True)
    date = models.DateTimeField(u"Дата присвоения",
                            db_column="F_ACHSTATUS_CREATE",
                            blank=True, null=True, auto_now_add=False)
    class Meta:
        db_table = "ACHSTATUS"


class AchievementMark(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_ACHMARK_ID")
    achievement = models.ForeignKey(to=Achievement, related_name="marks",
                                    db_column="F_ACHIEVEMENT_ID",
                                    verbose_name=u"Achievement")
    value = models.IntegerField(db_column="F_ACHMARK_VAL",
                                verbose_name=u"Значение",
                                blank=True, null=True)
    department = models.ForeignKey(to="organizations.Department", related_name="achievement_marks", verbose_name=u"Подразделение", db_column="F_DEPARTMENT_ID")
    creator = models.ForeignKey(to=User, related_name="achievement_marks",
                                db_column="F_ACHMARK_USER",
                                null=False, blank=False)
    date = models.DateTimeField(u"Дата создания", db_column="F_ACHMARK_CREATE", blank=True, null=True, auto_now_add=False)

    class Meta:
        db_table = "ACHMARK"
