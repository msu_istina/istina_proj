from common.utils.admin import register_with_versions
from achievements.models import Achievement, AuthorAch, MyLanguage, \
	AchievementRub, AchievementDescr, AchievementFile, \
	AchievementPub, AchievementStatusType, AchievementStatus

register_with_versions(Achievement)
register_with_versions(AuthorAch)
register_with_versions(MyLanguage)
register_with_versions(AchievementRub)
register_with_versions(AchievementDescr)
register_with_versions(AchievementFile)
register_with_versions(AchievementPub)
register_with_versions(AchievementStatusType)
register_with_versions(AchievementStatus)
