# -*- coding: utf-8 -*-
from django.db import models
from common.models import MyModel, AuthoredModel, AuthorshipPositionedModel, Country, Activity


class AwardCategory(MyModel):
    """Category of award: prize, medal, premium."""
    id = models.AutoField(primary_key=True, db_column="f_awardtype_id")
    name = models.CharField(u"Тип награды", max_length=255, db_column="f_awardtype_name")
    code = models.CharField(u"Кодовое имя типа награды", max_length=255,
                            db_column="f_awardtype_code", blank=True)

    class Meta:
        db_table = u'awardtype'
        verbose_name = u"тип награды"
        verbose_name_plural = u"типы наград"

    def __unicode__(self):
        return self.name


class AwardName(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_awardchoice_id")
    name = models.CharField(u"Название награды", max_length=1000,
                            db_column="f_awardchoice_name", blank=True)
    category = models.ForeignKey(AwardCategory, verbose_name=u"Тип награды",
                                 related_name="award_names", db_column="f_awardtype_id")
    code = models.CharField(u"Кодовое имя названия награды", max_length=128,
                            db_column="f_awardchoice_code", blank=True)

    class Meta:
        db_table = u'awardchoice'
        verbose_name = u"название награды"
        verbose_name_plural = u"названия наград"

    def __unicode__(self):
        return self.name


class Award(AuthoredModel, Activity):
    id = models.AutoField(primary_key=True, db_column="f_award_id")
    authors = models.ManyToManyField("workers.Worker", related_name="awards", through="AwardAuthorship")
    name_from_list = models.ForeignKey(AwardName, verbose_name=u"Название награды",
                                       related_name="awards", db_column="f_awardchoice_id", null=True, blank=True)
    name_custom_text = models.CharField(u"Название награды (если нет в списке)", max_length=1000,
                                        db_column="f_award_name", blank=True)
    category = models.ForeignKey(AwardCategory, verbose_name=u"Тип награды",
                                 related_name="awards", db_column="f_awardtype_id")
    organization = models.CharField(u"Организация", max_length=255, db_column="f_award_organ", blank=True)
    country = models.ForeignKey(Country, verbose_name=u"Страна",
                                related_name="awards", db_column="f_country_id")
    startdate = models.DateField(u"Дата присуждения", db_column="f_award_date")
    comment = models.TextField(u"Описание заслуги", db_column="f_award_comment", blank=True)

    nominative_en = "award"
    nominative_short = u"награда"
    genitive = u"награды"
    genitive_short = u"награды"
    genitive_plural = u"награды"
    genitive_plural_full = u"наград"
    accusative_short = u"награду"
    instrumental = u"наградой"
    locative = u"награде"
    gender = 'feminine'

    workers_verbose_name_single = u"Лауреат"
    workers_verbose_name_plural = u"Лауреаты"
    workers_required_error_msg = u"Укажите хотя бы одного лауреата"

    class Meta:
        db_table = "award"
        verbose_name = u"награда"
        verbose_name_plural = u"награды"
        get_latest_by = "startdate"
        ordering = ('-startdate', 'category')

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('awards_detail', (), {'object_id': self.id})

    @property
    def name(self):
        if self.name_from_list:
            return unicode(self.name_from_list)
        else:
            return self.name_custom_text

    @property
    def title(self):
        return self.name


class AwardAuthorship(AuthorshipPositionedModel):
    id = models.AutoField(primary_key=True, db_column="f_authoraward_id")
    author = models.ForeignKey(to="workers.Worker", related_name="award_authorships", db_column="f_man_id", null=True, blank=True)
    award = models.ForeignKey(Award, related_name="authorships", db_column="f_award_id")
    original_name = models.CharField(max_length=1000, db_column="f_authoraward_name")
    position = models.BigIntegerField(db_column="f_authoraward_seq", default=0, null=True, blank=True)

    class Meta(AuthorshipPositionedModel.Meta):
        db_table = u'authoraward'
        verbose_name = u"лауреат награды"
        verbose_name_plural = u"лауреаты наград"

    def __unicode__(self):
        return u"Лауреат награды '%s', %s" % (self.award, self.workers_string_full)
