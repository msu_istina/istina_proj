# -*- coding: utf-8; -*-
from common.forms import LinkedToWorkersModelForm, CountryChoiceField, ValidationError
from awards.models import Award

class AwardForm(LinkedToWorkersModelForm):
    country = CountryChoiceField(label=u"Страна")
    fields_order = ["authors_str", "category", "name_from_list", "name_custom_text",
    	"organization", "country", "startdate", "comment"]

    class Meta:
        model = Award
        exclude = ('authors',)

    def __init__(self, *args, **kwargs):
    	super(AwardForm, self).__init__(*args, **kwargs)
    	# it is already set in common/forms
    	self.fields['organization'].initial = ""

    def clean(self):
        cleaned_data = super(AwardForm, self).clean()
        if not cleaned_data['name_from_list'] and not cleaned_data['name_custom_text']:
            raise ValidationError(u"Введите название награды.")
        return cleaned_data