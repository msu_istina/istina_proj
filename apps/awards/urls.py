# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from common.views import LinkedToWorkersModelWizardView as Wizard, autocomplete_search
from awards.models import Award
from awards.forms import AwardForm

AwardFormWizard = Wizard.create_wizard_from_form(AwardForm)

options_base = {'model': Award}
options_detail = dict(options_base.items() + [('template_name', 'awards/detail.html')])

urlpatterns = patterns('common.views',
    url(r'^add/$', AwardFormWizard.as_view(), name='awards_add'),
    url(r'^(?P<object_id>\d+)/$', 'detail', options_detail, name='awards_detail'),
    url(r'^(?P<object_id>\d+)/edit/$', AwardFormWizard.as_view(), name='awards_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', options_base, name='awards_delete'),
    url(r'^organizations/search/$', autocomplete_search, options_base, name="awards_organizations_search"),
)
