from common.utils.admin import register_with_versions
from models import AwardCategory, AwardName, Award, AwardAuthorship

register_with_versions(AwardCategory)
register_with_versions(AwardName)
register_with_versions(Award)
register_with_versions(AwardAuthorship)
