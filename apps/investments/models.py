# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models

from common.models import LinkedToWorkersModel, WorkershipModel, Activity
from common.utils.validators import validate_year, validate_non_negative_number


class Investment(LinkedToWorkersModel, Activity):
    '''An attracted investment to an organization.'''
    id = models.AutoField(primary_key=True, db_column="F_INVEST_ID")
    title = models.CharField(u"Название инвестиции", max_length=255, db_column="F_INVEST_NAME")
    raisers = models.ManyToManyField("workers.Worker", related_name="investments", through="InvestmentParticipation")
    amount = models.FloatField(u"Сумма (руб.)", db_column="F_INVEST_SUM", validators=[validate_non_negative_number])
    year = models.IntegerField(u"Год привлечения", db_column="F_INVEST_YEAR", validators=[validate_year])
    organization = models.ForeignKey(to="organizations.Organization", related_name="investments", db_column="F_ORGANIZATION_ID", verbose_name=u"Организация", blank=True, null=True)
    project = models.ForeignKey(to="projects.Project", related_name="investments", db_column="F_PROJECT_ID", verbose_name=u"Проект", blank=True, null=True)
    source = models.CharField(u"Источник инвестиции", max_length=255, db_column="F_INVEST_SOURCE", blank=True)
    description = models.TextField(u"Описание", db_column="F_INVEST_ABSTRACT", blank=True)
    xml = models.TextField(u"XML инвестиции", db_column="F_INVEST_CLOB", blank=True)
    creator = models.ForeignKey(to=User, related_name="investments_added", db_column="F_INVEST_USER", null=True, blank=True)

    workers_attr = 'raisers'
    workerships_attr = 'participations'
    workers_verbose_name_single = u"Участник привлечения"
    workers_verbose_name_plural = u"Участники привлечения"
    workers_required_error_msg = u"Укажите хотя бы одного участника привлечения инвестиций"

    nominative_en = "investment"
    genitive = u"инвестиции"
    genitive_plural_full = u"инвестиций"
    accusative = u"инвестицию"
    accusative_short = u"инвестицию"
    instrumental = u"инвестицией"
    locative = u"инвестиции"
    gender = "feminine"

    class Meta:
        db_table = "INVEST"
        verbose_name = u"инвестиция"
        verbose_name_plural = u"инвестиции"
        get_latest_by = "year"
        ordering = ('-year', '-amount')

    def __unicode__(self):
        return u"%s (%f руб.)" % (self.title, self.amount)

    @models.permalink
    def get_absolute_url(self):
        return ('investments_detail', (), {'object_id': self.id})


class InvestmentParticipation(WorkershipModel):
    id = models.AutoField(primary_key=True, db_column="F_INVESTMAN_ID")
    raiser = models.ForeignKey(to="workers.Worker", related_name="investment_participations", db_column="F_MAN_ID")
    investment = models.ForeignKey(to="Investment", related_name="participations", db_column="F_INVEST_ID")
    creator = models.ForeignKey(to="auth.User", related_name="investment_participations_added", db_column="F_INVESTMAN_USER", null=True, blank=True)

    worker_attr = 'raiser'

    class Meta:
        db_table = "INVESTMAN"
        verbose_name = u"участие в привлечении инвестиции"
        verbose_name_plural = u"участия в привлечении инвестиций"

    def __unicode__(self):
        return u"%s, участник привлечения инвестиции '%s'" % (self.raiser, self.investment.title)

from investments.admin import *