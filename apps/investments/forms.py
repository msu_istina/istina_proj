# -*- coding: utf-8; -*-
from django import forms
from common.forms import LinkedToWorkersModelForm
from investments.models import Investment


class InvestmentForm(LinkedToWorkersModelForm):
    '''A simple form for investments.'''
    project_str = forms.CharField(label=u"Название проекта", max_length=255, required=False)
    fields_order = ["organization", "raisers_str", "title", "source", "project_str", "amount", "year", "description"]
    css_classes = [('source', 'wide'), ('amount', 'wide'), ('project_str', 'wide autocomplete_project')]

    class Meta:
        model = Investment
        exclude = ('raisers', 'project', 'creator', 'xml')

    def __init__(self, *args, **kwargs):
        super(InvestmentForm, self).__init__(*args, **kwargs)
        if self.instance_linked() and self.instance.project: # initialize project_str field
            self.fields['project_str'].initial = self.instance.project.name

    def save(self, request, workers, project, commit=True):
        # project must exist now
        investment = super(InvestmentForm, self).save(request, commit=False)
        investment.project = project
        if commit:
            self.commit_save(request, investment, workers)
        return investment
