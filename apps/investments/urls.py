# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *

from investments.models import Investment
from investments.views import InvestmentWizardView as Wizard

options_base = {'model': Investment}
options_detail = dict(options_base.items() + [('template_name', 'investments/detail.html')])

urlpatterns = patterns('common.views',
    #url(r'^add/$', Wizard.as_view(), name='investments_add'), # disabled for now, see #740
    url(r'^(?P<object_id>\d+)/$', 'detail', options_detail, name='investments_detail'),
    url(r'^(?P<object_id>\d+)/edit/$', Wizard.as_view(), name='investments_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', options_base, name='investments_delete'),
)
