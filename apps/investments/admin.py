from django.contrib import admin

from common.utils.admin import register_with_versions
from models import Investment, InvestmentParticipation

register_with_versions(Investment)
register_with_versions(InvestmentParticipation)
