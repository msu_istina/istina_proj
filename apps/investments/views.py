# -*- coding: utf-8; -*-
import logging

from common.views import LinkedToWorkersModelWizardView
from investments.forms import InvestmentForm
from investments.models import Investment
from projects.forms import ProjectMembershipSimilarProjectsForm, ProjectForm
from projects.models import Project
from workers.forms import WorkerFormSet

logger = logging.getLogger("projects.views")


class InvestmentWizardView(LinkedToWorkersModelWizardView):
    form_list = [
        (LinkedToWorkersModelWizardView.main_step, InvestmentForm),
        ('similar_projects', ProjectMembershipSimilarProjectsForm),
        ('new_project', ProjectForm),
        (LinkedToWorkersModelWizardView.workers_steps[0], WorkerFormSet)
    ]
    condition_dict = LinkedToWorkersModelWizardView.make_condition_dict(
        'similar_projects', 'new_project')
    model = Investment

    def get_form_initial(self, step):
        """
        Returns a dictionary which will be passed to the form for `step`
        as `initial`. If no initial data was provied while initializing the
        form wizard, a empty dictionary will be returned.
        """
        initial = super(InvestmentWizardView, self).get_form_initial(step)
        if step == 'new_project':
            initial['name'], initial['organization'] = self.get_extra_data('project_str', 'organization')
        return initial

    def get_form(self, step=None, data=None, files=None):
        form = super(InvestmentWizardView, self).get_form(step, data, files)
        if not step:
            step = self.steps.current
        if step == 'similar_projects':
            # the real step can be 4, but this function can be called to render previous forms
            # get the similar projects basing on project_str entered at 'main' step.
            # if this is really step 2, and it's not skipped, similar projects should be calculated (again)
            similar_projects, project_str, organization = self.get_extra_data('similar_projects', 'project_str', 'organization')
            if not similar_projects:
                if self.steps.current == step: # this is really step 2
                    similar_projects = Project.objects.get_similar(project_str, organization)
                    # store similar projects not to calculate them twice (on step 2 and on step 3).
                    self.set_extra_data('similar_projects', similar_projects)
                else: # this is just a previous form for step 5
                    similar_projects = Project.objects.none()
            form.fields['project'].queryset = similar_projects
            form.fields['project'].empty_label = project_str + u" (добавить новый проект)"
        return form

    def get_context_data(self, form, **kwargs):
        context = super(InvestmentWizardView, self).get_context_data(form, **kwargs)
        forms = context['previous_forms']
        self.hide_form_fields(forms, 'main', 'project_str', ('similar_projects', 'new_project'))
        self.hide_form_fields(forms, 'main', 'organization', 'new_project')
        self.hide_form_fields(forms, 'similar_projects', 'project', 'new_project')
        if 'similar_projects' in self.steps.all or 'new_project' in self.steps.all:
            self.hide_form_fields(forms, 'main', ('project_str', 'organization'), self.workers_steps[0])
        if 'new_project' in self.steps.all:
            self.hide_form_fields(forms, 'main', 'organization', self.workers_steps[0])
            self.hide_form_fields(forms, 'similar_projects', 'project', self.workers_steps[0])
        return context

    def process_step(self, form):
        project = None
        if self.steps.current == self.main_step:
            project_str = form.cleaned_data['project_str']
            organization = form.cleaned_data['organization']
            old_project_str, old_organization = self.get_extra_data('project_str', 'organization')
            if (project_str, organization) != (old_project_str, old_organization): # recalculate similar projects
                self.set_extra_data(('project_str', project_str), ('organization', organization)) # for later use in creating similar projects form
                self.delete_extra_data('similar_projects') # now they should be calculated again
                if project_str: # if project_str is empty, don't calculate similar projects, skip this step
                    try:
                        # try to select project by name
                        project = Project.objects.get(name=project_str, organization=organization)
                    except:
                        # calculate similar projects
                        similar_projects = Project.objects.get_similar(project_str, organization=organization)
                        # store similar projects not to calculate them twice (on step 2 and on step 3).
                        self.set_extra_data('similar_projects', similar_projects)
                        self.skip_or_include_steps('similar_projects', not similar_projects)
                        if not similar_projects:
                            self.include_steps('new_project')
                    else: # project selected by name, go to the last step
                        self.skip_steps('similar_projects', 'new_project')
                else:
                    self.skip_steps('similar_projects', 'new_project')
        elif self.steps.current == 'similar_projects':
            project = form.cleaned_data['project']
            self.skip_or_include_steps('new_project', isinstance(project, Project))
        if isinstance(project, Project): # project found by name or similar project selected
            self.set_extra_data('project_instance', project)
        return super(InvestmentWizardView, self).process_step(form)

    def save_object(self, form_list):
        '''This function is executed during done() method, and used to save main form object.
        Workers objects has been already saved.
        '''
        if 'new_project' in self.steps.all: # new project has been added
            new_project_form = form_list[self.get_step_index('new_project')]
            project = new_project_form.save(self.request) # save a new project
        else: # project could be found by title or selected among similar projects
            project = self.get_extra_data('project_instance', Project) # this is a project found by title or a selected similar project
        return super(InvestmentWizardView, self).save_object(form_list, project)

