from django.conf.urls.defaults import patterns, url


urlpatterns = patterns('benchmarks.views',
    url(r'^$', "main"),
    url(r'^worker_short/(?P<worker_id>\d+)/$', "worker_detail_short", name="benchmark_worker_short"),
    url(r'^worker_short/$', "worker_detail_short_form", name="benchmark_worker_short_form"),
    url(r'^last_added_activities/$', "last_added_activities", name="benchmark_last_added_activities"),
    url(r'^similar_workers/(?P<repeats>\d+)/$', "get_similar_workers_time", name="benchmark_get_similar_workers_time"),
)
