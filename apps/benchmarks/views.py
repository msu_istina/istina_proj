# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import user_passes_test
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render, redirect, HttpResponse
from django.template import RequestContext
from django.template.loader import render_to_string
from workers.forms import WorkerFormSet
from publications.utils import split_authors
import logging
import re
import timeit
import workers.models
import forms


def uncamel(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


@user_passes_test(lambda u: u.is_staff)
def main(request):
    form_names = ("BenchmarkWorkerShortForm", "BenchmarkLastAddedActivitiesForm")
    context = {}
    for form_name in form_names:
        context_var = uncamel(form_name[len("Benchmark"):])
        context[context_var] = getattr(forms, form_name)
    return render(request, "benchmarks/main.html", context)


@user_passes_test(lambda u: u.is_staff)
def worker_detail_short_form(request):
    form = forms.BenchmarkWorkerShortForm(request.GET)
    if not form.is_valid():
        return render(request, 'benchmarks/worker_short.html', locals())
    worker_id = form.worker_id
    url = reverse(worker_detail_short, args=(worker_id,))
    if form.cleaned_data["with_links"]:
        url += "?with_links=1"
    return redirect(url)


@user_passes_test(lambda u: u.is_staff)
def worker_detail_short(request, worker_id):
    """Benchmark worker short info.

    Accepts GET with_links argument
    """
    from workers.templatetags.workers_show import calculate_worker_short_context
    with_links = request.GET.get("with_links")
    worker = get_object_or_404(workers.models.Worker, pk=worker_id)
    context = calculate_worker_short_context({"CACHE_TIME": 10000, "cache_disabled": True}, worker, int(not with_links))
    html = render_to_string('workers/worker_short.html', RequestContext(request, context))
    return render(request, "dummy.html", {"content": html, "title": "Benchmark: worker_short"})


@user_passes_test(lambda u: u.is_staff)
def last_added_activities(request):
    from common.utils.activities import get_activities_last_added
    form = forms.BenchmarkLastAddedActivitiesForm(data=request.GET)
    if form.is_valid():
        activities = get_activities_last_added(limit=form.cleaned_data["limit"])
        compact = not form.cleaned_data["extended"]
    return render(request, 'benchmarks/last_added_activities.html', locals())


def get_similar_workers_time(request, repeats=1):
    authors_str = u'Васенин В.А., Козицын А.С., Афонин С.А., Голомазов Д.Д., Бахтин А.В., Ганкин Г.М.'
    authors = split_authors(authors_str)
    tuple2dict = lambda worker_tuple: dict(zip(('lastname', 'firstname', 'middlename',), worker_tuple))
    initial = map(tuple2dict, authors)

    def test():
        formset = WorkerFormSet(initial=initial, required_error_msg='')
        for form in formset.forms:
            list(form.get_similar())

    timer = timeit.Timer(test)
    repeats = int(repeats)
    repeats_time = timer.repeat(repeats, 1)
    total_time = sum(repeats_time)
    average = total_time / len(repeats_time)
    logging.getLogger("benchmark.get_similar_workers_time").info("Timer: repeats %s, total %s, average %s" %
                                                                 (repeats, total_time, average))
    return HttpResponse()


