# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.db import models

import workers.models


class BenchmarkWorkerShortForm(forms.Form):
    meta_worker_id = forms.CharField(label="Имя пользователя или worker_id")
    with_links = forms.BooleanField(label="Показывать ссылки", required=False)

    def clean_meta_worker_id(self):
        meta_worker_id = self.cleaned_data["meta_worker_id"]
        query_fields = [("pk",  int), ("profile__user__username", str)]
        for query_field, validator in query_fields:
            try:
                validator(meta_worker_id)
            except:
                continue
            try:
                worker = workers.models.Worker.objects.get(**{query_field: meta_worker_id})
            except models.ObjectDoesNotExist:
                continue
            else:
                break
        else:
            raise forms.ValidationError("Неправильный worker_id или имя пользователя")
        self.worker_id = worker.id
        return meta_worker_id


class BenchmarkLastAddedActivitiesForm(forms.Form):
    limit = forms.IntegerField(label="Количество отображаемых записей", initial=10)
    extended = forms.BooleanField(label="Расширенный формат", required=False)

    def clean_limit(self):
        if self.cleaned_data["limit"] < 0:
            raise forms.ValidationError("Введите неотрицательное число")
        if self.cleaned_data["limit"] == 0:
            return None
        return self.cleaned_data["limit"]
