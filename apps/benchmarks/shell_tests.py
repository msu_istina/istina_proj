import cProfile, pstats
from journals.models import Journal
from journals.admin import *

def profile_function(func):
    profile = cProfile.Profile()
    profile.enable()
    func()
    profile.disable()
    ps = pstats.Stats(profile)
    ps.print_stats(.01)

def revisions_time_generation(journals_num=10):
    '''Run from shell because RevisionMiddleware cathes all requests. You must comment
     'my_create_revision' lines in common models.py, otherwise tests will show same time.'''
    from common.utils.reversion_utils import my_create_revision
    journal_name = '!TestRevisionJournal_%s'

    def create_journals():
        for i in range(journals_num):
            Journal.objects.create(name=journal_name % i)

    def change_journals():
         for i in range(journals_num):
            journal = Journal.objects.get(name=journal_name % i)
            journal.url = 'http://url.ru'
            journal.save()

    def delete_journals():
        for i in range(journals_num):
            journal = Journal.objects.filter(name=journal_name % i)
            journal.delete()

    def test_with_revisions():
            my_create_revision(create_journals)()
            my_create_revision(change_journals)()
            my_create_revision(delete_journals)()

    def test_without_revisions():
        create_journals()
        change_journals()
        delete_journals()

    profile_function(test_without_revisions)
    profile_function(test_with_revisions)