# -*- coding: utf-8 -*-
from common.managers import SimilarFieldManager
from common.utils.dict import filter_true

class CourseManager(SimilarFieldManager):
    '''Model manager for Course model.'''

    def get_similar(self, title, **kwargs):
        qs = self
        # filter out kwargs with False value
        kwargs = filter_true(kwargs)
        if kwargs:
            qs = qs.filter(**kwargs)
        return self.get_similar_by_field('title', title, cutoff=0.5, n=20, qs=qs)
