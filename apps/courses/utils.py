from datetime import date
from django.db.models.loading import get_model

def academic_years(startdate, enddate=date.today()):
    """ (date, date) -> int
        Return number of academic years spanning the specified time period.
        If enddate is null, then the end of the current semester is taken (31.12 or 31.05)
        Tests with enddate=null are given for 01.04.2013 as the current date. If you run the tests after 01.09.2013, they should be adjusted.
        >>> academic_years(date(2012, 9, 1), date(2012, 12, 31))
        1
        >>> academic_years(date(2012, 9, 1), date(2013, 5, 31))
        1
        >>> academic_years(date(2012, 9, 1), date(2015, 5, 15))
        3
        >>> academic_years(date(2013, 1, 1), date(2013, 8, 31))
        1
        >>> academic_years(date(2013, 1, 1), date(2013, 12, 31)) # ???
        2
        >>> academic_years(date(2013, 1, 1), date(2014, 12, 31))
        3
        >>> academic_years(date(2013, 1, 1), date(2013, 2, 1))
        1
        >>> academic_years(date(2013, 6, 1), date(2013, 8, 31))
        1
        >>> academic_years(date(2013, 6, 1), date(2013, 9, 15)) # ???
        2
        >>> academic_years(date(2013, 6, 1), date(2018, 8, 31))
        6
        >>> academic_years(date(2013, 9, 1), date(2018, 5, 31))
        5
        >>> academic_years(date(2010, 9, 1), date(2012, 9, 1))
        2
        >>> academic_years(date(2010, 9, 1), date(2012, 9, 30))
        3
        >>> academic_years(date(2010, 5, 30), date(2012, 7, 15))
        3
        >>> academic_years(date(2010, 6, 3), date(2012, 7, 14))
        3
        >>> academic_years(date(2010, 9, 2), date(2012, 6, 30))
        2
        >>> academic_years(date(2010, 3, 2), date(2012, 4, 15))
        3
        >>> academic_years(date(2010, 2, 7), date(2012, 6, 30))
        3
        >>> academic_years(date(2010, 9, 1), date(2012, 9, 30))
        3
        >>> academic_years(date(2010, 9, 1), date(2010, 9, 30))
        1
        >>> academic_years(date(2010, 9, 1), date(2012, 1, 26))
        2
        >>> academic_years(date(2010, 2, 7), date(2012, 2, 1))
        3
        >>> academic_years(date(2010, 6, 1), date(2012, 6, 30))
        3
        >>> academic_years(date(2010, 12, 1), date(2012, 6, 7))
        2
        >>> academic_years(date(2010, 12, 1), date(2009, 6, 7))
        0
        >>> academic_years(date(2012, 1, 1))
        2
        >>> academic_years(date(2012, 2, 1))
        2
        >>> academic_years(date(2012, 3, 1))
        2
        >>> academic_years(date(2012, 6, 1))
        2
        >>> academic_years(date(2012, 8, 1))
        2
        >>> academic_years(date(2012, 9, 1))
        1
        >>> academic_years(date(2012, 11, 1))
        1
        >>> academic_years(date(2013, 1, 1))
        1
        >>> academic_years(date(2013, 5, 1))
        0
        >>> academic_years(date(2011, 8, 1), date(2012, 9, 30))
        3
        >>> academic_years(date(2011, 1, 1), date(2012, 12, 31))
        3
    """
    if startdate > enddate:
        return 0
    years = enddate.year - startdate.year
    if startdate.month < 9:
        # previous academic year is included
        years += 1
    if enddate.month > 9 or (enddate.month == 9 and enddate.day > 10):
        # next academic year is included
        years += 1
    return years 
    

def show_strange_dates():
    """
        Show dates of teachings that are somewhat strange and non-conforming to standards.
        Use in shell only to provide some more tests for the academic_years() function.
    """
    CourseTeaching = get_model("courses", "CourseTeaching")
    for teaching in CourseTeaching.objects.all().values_list('startdate', 'enddate'):
        startdate = teaching[0]
        enddate = teaching[1]
        if startdate.month not in [9,10,11,12,1,2] or (enddate and enddate.month not in [4, 5,11,12]):
            print "%d.%d.%d - %s" % (startdate.day, startdate.month, startdate.year, "%d.%d.%d" % (enddate.day, enddate.month, enddate.year) if enddate else "")

    
if __name__ == "__main__":
    import doctest
    doctest.testmod()
