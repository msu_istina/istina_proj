# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from common.models import AuthorRole
from common.views import LinkedToWorkersModelWizardView
from courses.models import Course, CourseTeaching
from courses.forms import CourseForm, CourseTeachingForm
from courses.views import CourseTeachingWizardView

CourseFormWizard = LinkedToWorkersModelWizardView.create_wizard_from_form(CourseForm)

# courses
options_base = {'model': Course}
options_detail = dict(options_base.items() + [('template_name', 'courses/detail.html'),
  ('extra_context', {'roles': AuthorRole.objects.filter(authorroleobj__code="course").exists()})])

urlpatterns = patterns('common.views',
    url(r'^add/$', CourseFormWizard.as_view(), name='courses_add'),
    url(r'^(?P<object_id>\d+)/$', 'detail', options_detail, name='courses_detail'),
    url(r'^(?P<object_id>\d+)/edit/$', CourseFormWizard.as_view(), name='courses_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', options_base, name='courses_delete'),
)


# teachings
teaching_options_base = {'model': CourseTeaching}
teaching_options_detail = dict(teaching_options_base.items() + [('template_name', 'courses/teaching_detail.html')])

urlpatterns += patterns('common.views',
    url(r'^teachings/add_2/$', CourseTeachingWizardView.as_view(), name='teaching_add_event'),
    url(r'^teachings/add/$', direct_to_template, {'template': 'courses/add_teaching.html'}, name='courses_teaching_add'),
    url(r'^teachings/(?P<object_id>\d+)/$', 'detail', teaching_options_detail, name='courses_teaching_detail'),
    url(r'^teachings/(?P<object_id>\d+)/edit/$', CourseTeachingWizardView.as_view(), name='courses_teaching_edit'),
    url(r'^teachings/(?P<object_id>\d+)/delete/$', 'delete', teaching_options_base, name='courses_teaching_delete'),
)
