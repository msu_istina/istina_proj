# -*- coding: utf-8; -*-
from django import forms

from common.forms import LinkedToWorkersModelForm, WorkershipModelForm, MyForm, MyModelForm, CountryChoiceField
from organizations.models import Organization
from organizations.forms import DepartmentChoiceField
from courses.models import Course, CourseTeaching, CourseTeachingPartnership
from journals.models import JournalRubric


class CourseForm(LinkedToWorkersModelForm):
    '''A simple form for courses.'''
    fields_order = ["authors_str", "title", "year", "organization", "distant", "in_foreign_language", "description"]
    organization_index = 1

    class Meta:
        model = Course
        exclude = ('authors', 'creator', 'xml')


class CourseTeachingForm(WorkershipModelForm):
    '''A simple form for courses teaching.'''
    course_str = forms.CharField(label=u"Название курса", max_length=255)
    department = DepartmentChoiceField(label=u'Подразделение')
    partnership_direction = forms.ChoiceField(label=u"Тип преподавания", choices=CourseTeachingPartnership.direction_choices())
    fields_order = ["teacher_str", "course_str", "department", "partnership_direction", "discipline_type", "teaching_kind", "startdate", "enddate", "hours"]
    css_classes = [('course_str', 'wide autocomplete_course'), ('hours', 'narrow'), ('department', 'wide')]

    class Meta:
        model = CourseTeaching
        exclude = ('teacher', 'course')


    def __init__(self, *args, **kwargs):
        super(CourseTeachingForm, self).__init__(*args, **kwargs)
        if self.instance_linked(): # initialize collection_str field
            self.fields['course_str'].initial = self.instance.course.title
        self.fields['teacher_str'].label = u"ФИО преподавателя"

    def clean_teacher_str(self):
        # only one worker is allowed
        value = self.cleaned_data['teacher_str'].strip(", ")
        if "," in value:
            raise forms.ValidationError(u"Пожалуйста, укажите ФИО только одного преподавателя курса. В строке не должно быть запятых.")
        return value

    def save(self, request, workers, course, commit=True):
        # course must exist now
        teaching = super(CourseTeachingForm, self).save(request, commit=False)
        teaching.course = course
        if commit:
            self.commit_save(request, teaching, workers[0]) # workers[0] is a list: [teacher]
        return teaching


class SimilarCourseChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, course):
        multiple_authorships = course.authorships.count() > 1
        author_str = u", автор%s: %s" % (
            u"ы" if multiple_authorships else "",
            course.authorships.all()[0].workers_string
        ) if course.authorships.exists() else ""
        if multiple_authorships:
            author_str += u" и др."
        value = "%s (%s%s)" % (course.title, course.year, author_str)
        return value


class CourseTeachingSimilarCoursesForm(MyForm):
    course = SimilarCourseChoiceField(label=u"Курс", queryset=Course.objects.all(), empty_label="добавить новый курс", required=False)


class CourseTeachingNewCourseForm(LinkedToWorkersModelForm):
    fields_order = ['authors_str', 'title', 'year', 'organization', "distant", "in_foreign_language", 'description']

    class Meta:
        model = Course
        exclude = ('authors', 'xml', 'creator')

    def __init__(self, *args, **kwargs):
        super(CourseTeachingNewCourseForm, self).__init__(*args, **kwargs)
        self.fields['authors_str'].label = u"Авторы курса"


class CourseTeachingPartnershipForm(MyModelForm):
    organization_subcategory = forms.ModelChoiceField(
        JournalRubric.objects.filter(categorization__code='ORGKIND'),
        label=u'Подтип внешней организации')
    organization_country = CountryChoiceField(label=u"Страна внешней организации")
    fields_order = ["organization_name", "organization_category", "organization_subcategory", "organization_country",
        "sponsor_category", "sponsor_name", "amount"]
    css_classes = [
        ('organization_name', 'wide autocomplete_traineeship_organization'),
        ('sponsor_name', 'wide'),
        ('amount', 'narrow')
    ]

    class Meta:
        model = CourseTeachingPartnership
        exclude = ('course_teaching', 'direction', 'creator')

    def save(self, request, teaching, direction):
        partnership = super(CourseTeachingPartnershipForm, self).save(request, commit=False)
        partnership.course_teaching = teaching
        partnership.direction_code = direction
        partnership.save()
