# -*- coding: utf-8; -*-
__author__ = 'goldan'
import authority
from common.permissions import MyModelPermission, LinkedToWorkersModelPermission
from common.utils.user import get_worker
from courses.models import Course, CourseTeaching

class CoursePermission(LinkedToWorkersModelPermission):
    label = 'course_permission'
    operations_over_superuser = tuple(
        LinkedToWorkersModelPermission.operations_over_superuser + ('delete', )
    )

    def check_delete(self, course):
        '''Disallow all users (including superusers) from deleting a course that has teachings linked. See #1053.'''
        return super(CoursePermission, self).check_delete(course) and not course.teachings.exists()

authority.register(Course, CoursePermission)


class CourseTeachingPermission(MyModelPermission):
    label = 'courseteaching_permission'

    def check_delete(self, teaching):
        '''A course teaching can be deleted by a superuser, site editor or the teacher worker. FIXME: add creator field and allow creator to delete a teaching and disallow a teacher to delete it (but allow to unlink himself from the teaching).'''
        return super(CourseTeachingPermission, self).check_delete(teaching) or get_worker(self.user) == teaching.teacher

authority.register(CourseTeaching, CourseTeachingPermission)
