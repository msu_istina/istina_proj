# -*- coding: utf-8; -*-
from django.http import HttpResponse
from django.utils import simplejson
import logging

from common.utils.autocomplete import autocomplete_helper
from common.utils.uniqify import uniqify
from common.views import LinkedToWorkersModelWizardView
from courses.forms import CourseTeachingForm, CourseTeachingSimilarCoursesForm, CourseTeachingNewCourseForm, CourseTeachingPartnershipForm
from courses.models import Course, CourseTeaching
from workers.forms import WorkerFormSet

logger = logging.getLogger("courses.views")
#

class CourseTeachingWizardView(LinkedToWorkersModelWizardView):
    workers_steps = ['teacher_disambiguation', 'author_disambiguation']  # names of the workers disambiguation steps
    form_list = [
        (LinkedToWorkersModelWizardView.main_step, CourseTeachingForm),
        ('similar_courses', CourseTeachingSimilarCoursesForm),
        ('new_course', CourseTeachingNewCourseForm),
        ('partnership', CourseTeachingPartnershipForm),
    ]
    for step in workers_steps:
        form_list.append((step, WorkerFormSet))
    condition_dict = LinkedToWorkersModelWizardView.make_condition_dict(
        'similar_courses', 'new_course', 'partnership', 'author_disambiguation')
    model = CourseTeaching  # used in templates
    steps_info = {
        'similar_courses': {'title': u'Выберите существующий курс или добавьте новый'},
        'new_course': {'title': u'Информация об учебном курсе'},
        'partnership': {'title': u'Информация о внешней организации'},
    }


    def get(self, request, *args, **kwargs):
        course = self.course
        return super(CourseTeachingWizardView, self).get(request, *args, **kwargs)

    @property
    def course(self):
        course = getattr(self, "course_instance", None) or self.get_extra_data('course_instance', Course)
        if not course:
            course_id = self.request.GET.get('course_id', None)
            if course_id:
                try:
                    course_id = int(course_id)
                except ValueError:
                    pass
                else:
                    course = Course.objects.getdefault(id=course_id)
                    if course:
                        self.course_instance = course
                        self.set_extra_data('course_instance', course)
                        self.skip_steps('similar_courses', 'new_course', 'author_disambiguation')
        return course

    def get_form_kwargs(self, step=None):
        """
        Returns the keyword arguments for instantiating the form
        (or formset) on given step.
        """
        kwargs = super(CourseTeachingWizardView, self).get_form_kwargs(step)
        if step == 'author_disambiguation':
            kwargs.update({'required_error_msg': u"Укажите авторов курса"})
        return kwargs

    def get_form_initial(self, step):
        """
        Returns a dictionary which will be passed to the form for `step`
        as `initial`. If no initial data was provied while initializing the
        form wizard, a empty dictionary will be returned.
        """
        initial = super(CourseTeachingWizardView, self).get_form_initial(step)
        if step == 'new_course':
            initial['title'] = self.get_extra_data('course_str')
        elif step == self.main_step:
            if self.course:
                initial['course_str'] = self.course.title
            if self.edit:
                initial['partnership_direction'] = self.instance_dict[self.main_step].partnership_direction_code
        elif step == 'similar_courses':
            if self.edit:
                initial['course'] = self.instance_dict[self.main_step].course

        return initial

    def get_form_instance(self, step):
        instance = super(CourseTeachingWizardView, self).get_form_instance(step)
        if not instance and step == 'partnership' and self.edit:
            instance = self.instance_dict[self.main_step].partnerships.all().get_first_or_none()
        return instance


    def get_form(self, step=None, data=None, files=None):
        form = super(CourseTeachingWizardView, self).get_form(step, data, files)
        if not step:
            step = self.steps.current
        if step == 'similar_courses':
            # get the similar courses basing on course_str entered at 'main' step.
            # if this is really step 2, and it's not skipped, similar courses should be calculated (again)
            similar_courses, course_str = self.get_extra_data('similar_courses', 'course_str')
            if not similar_courses:
                if self.steps.current == step and course_str: # this is really step 2 and course_str is stored
                    # FIXME if course_str is None, it seems that there is a problem with smth (it shouldn't be None, because it is set on step 1 and stored in session)
                    similar_courses = Course.objects.get_similar(course_str)
                    # store similar courses not to calculate them twice (on step 2 and on step 3).
                    self.set_extra_data('similar_courses', similar_courses)
                else: # this is just a previous form for step 5 or course_str is empty
                    similar_courses = Course.objects.none()
            form.fields['course'].queryset = similar_courses
            form.fields['course'].empty_label = (course_str or "") + u" (добавить новый курс)"
        elif step == self.main_step:
            if self.course:
                form.fields['course_str'].widget.attrs['readonly'] = 'readonly'
        return form

    def get_context_data(self, form, **kwargs):
        context = super(CourseTeachingWizardView, self).get_context_data(form, **kwargs)
        forms = context['previous_forms']
        self.hide_form_fields(forms, 'main', 'course_str', ('new_course', 'similar_courses'))
        self.hide_form_fields(forms, 'main',
            ('department', 'partnership_direction', 'discipline_type', 'teaching_kind', 'startdate', 'enddate', 'hours'),
            ('partnership', 'similar_courses', 'new_course'))
        self.hide_form_fields(forms, 'new_course',
            ("title", "year", "organization", "distant", "in_foreign_language", "description"),
            'partnership')
        self.hide_form_fields(forms, 'similar_courses', 'course', ('new_course', 'partnership'))
        if 'similar_courses' in self.steps.all or 'new_course' in self.steps.all:
            self.hide_form_fields(forms, 'main', 'course_str', self.workers_steps[0])
        if 'new_course' in self.steps.all:
            self.hide_form_fields(forms, 'similar_courses', 'course', self.workers_steps[0])
        if self.steps.current == 'author_disambiguation':
            context.update({
                'workers_verbose_name_single': u"Автор",
                'workers_verbose_name_plural': u"Авторы",
            })
        return context

    def process_step(self, form):
        course = None
        if self.steps.current == self.main_step:
            if not self.course:
                course_str = form.cleaned_data['course_str']
                old_course_str = self.get_extra_data('course_str')
                if course_str != old_course_str : # the parameter has changed
                    self.set_extra_data('course_str', course_str) # for later use in creating similar courses form
                    self.delete_extra_data('similar_courses') # now they should be calculated again
                    try:
                        # try to select course by name
                        course = Course.objects.get(title=course_str)
                    except:
                        # calculate similar courses
                        similar_courses = Course.objects.get_similar(course_str)
                        # store similar courses not to calculate them twice (on step 2 and on step 3).
                        self.set_extra_data('similar_courses', similar_courses)
                        self.skip_or_include_steps('similar_courses', not similar_courses)
                        if not similar_courses:
                            self.include_steps('new_course')
                    else: # course selected by name, go to the last step
                        self.skip_steps('similar_courses', 'new_course', 'author_disambiguation')
            self.skip_or_include_steps('partnership', form.cleaned_data['partnership_direction'] not in ['importing', 'exporting'])

        elif self.steps.current == 'similar_courses':
            course = form.cleaned_data['course']
            self.skip_or_include_steps('new_course', isinstance(course, Course))
            self.skip_or_include_steps('author_disambiguation', isinstance(course, Course))
        elif self.steps.current == 'new_course':
            self.set_extra_data(self.workers_steps[1] + '-workers_str', form.cleaned_data[form.workers_fields[0]])
        if isinstance(course, Course): # course found by name or similar course selected
            self.set_extra_data('course_instance', course)

        return super(CourseTeachingWizardView, self).process_step(form)

    def save_object(self, form_list):
        '''This function is executed during done() method, and used to save main form object.
        Workers objects has been already saved.
        '''
        if 'new_course' in self.steps.all: # new course has been added
            new_course_form = form_list[self.get_step_index('new_course')]
            course = new_course_form.save(self.request, self.workers[1]) # save a new course with workers from step 4 (authors disambiguation)
        else: # course could be found by title or selected among similar courses
            course = self.get_extra_data('course_instance', Course) # this is a course found by title or a selected similar course
        teaching = super(CourseTeachingWizardView, self).save_object(form_list, course)
        if 'partnership' in self.steps.all:
            partnership_form = form_list[self.get_step_index('partnership')]
            direction = form_list[self.get_step_index(self.main_step)].cleaned_data['partnership_direction']
            partnership = partnership_form.save(self.request, teaching, direction)
        else:
            teaching.partnerships.all().delete()

        return teaching


def autocomplete_search_courses(request):
    '''Server side for jquery autocomplete
    '''
    term = request.GET.get('term', '')
    fields = []
    limit = 10
    if len(term) >= 2:
        items = autocomplete_helper(Course, "title", term, limit)
        def extract_info(obj):
            base_fields = [i.attname for i in obj._meta.fields]
            info = dict((i, getattr(obj, i)) for i in base_fields)
            info["label"] = unicode(obj)
            return info
        fields = map(extract_info, items)
        fields = uniqify(fields, lambda i: i['label'])
    return HttpResponse(simplejson.dumps(fields), content_type='application/javascript')
