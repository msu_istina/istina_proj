# -*- coding: utf-8 -*-
import datetime
from django.db import models
from django.contrib.auth.models import User
from common.models import MyModel, AuthoredModel, AuthorshipModel, WorkershipModel, Activity, Country
from common.utils.validators import validate_year
from courses.managers import CourseManager
from courses.utils import academic_years


class Course(AuthoredModel, Activity):
    '''A course that is created by a worker.'''
    id = models.AutoField(primary_key=True, db_column="F_COURSE_ID")
    title = models.CharField(u"Название курса", max_length=255, db_column="F_COURSE_NAME")
    authors = models.ManyToManyField("workers.Worker", related_name="courses", through="CourseAuthorship")
    year = models.IntegerField(u"Год создания", db_column="F_COURSE_YEAR", validators=[validate_year])
    organization = models.ForeignKey(to="organizations.Organization", related_name="courses", db_column="F_ORGANIZATION_ID", verbose_name=u"Организация", blank=True, null=True)
    description = models.TextField(u"Описание", db_column="F_COURSE_ABSTRACT", blank=True)
    distant = models.BooleanField(u"Дистанционный курс", db_column="F_COURSE_DISTANT", default=False, blank=True)
    in_foreign_language = models.BooleanField(u"Курс на иностранном языке", db_column="F_COURSE_FOREIGNLANG", default=False, blank=True)
    xml = models.TextField(u"XML курса", db_column="F_COURSE_XML", blank=True)
    creator = models.ForeignKey(to=User, related_name="courses_added", db_column="F_COURSE_USER", null=True, blank=True)

    objects = CourseManager()

    nominative_short = u"курс"
    nominative_en = "course"
    genitive = u"учебного курса"
    genitive_short = u"курса"
    genitive_plural = u"учебных курса"
    genitive_plural_full = u"учебных курсов"
    accusative_short = u"курс"
    instrumental = u"курсом"
    locative = u"учебном курсе"

    class Meta:
        db_table = "COURSE"
        verbose_name = u"учебный курс"
        verbose_name_plural = u"учебные курсы"
        get_latest_by = "year"
        ordering = ('-year', '-title')

    def __unicode__(self):
        return u"%s" % self.title

    @models.permalink
    def get_absolute_url(self):
        return ('courses_detail', (), {'object_id': self.id})


class DisciplineType(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_disciplinetype_id")
    name = models.CharField(u"Название типа дисциплины", max_length=255, db_column="f_disciplinetype_name")

    class Meta:
        db_table = u'DISCIPLINETYPE'
        verbose_name = u"тип учебной дисциплины"
        verbose_name_plural = u"типы учебных дисциплин"

    def __unicode__(self):
        return self.name


class TeachingKind(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_coursetype_id")
    name = models.CharField(u"Название вида учебных занятий", max_length=1020, db_column="f_coursetype_name")

    class Meta:
        db_table = u'COURSETYPE'
        verbose_name = u"вид учебных занятий"
        verbose_name_plural = u"виды учебных занятий"

    def __unicode__(self):
        return self.name


class CourseAuthorship(AuthorshipModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORC_ID")
    author = models.ForeignKey(to="workers.Worker", related_name="course_authorships", db_column="F_MAN_ID", null=True, blank=True)
    course = models.ForeignKey(to="Course", related_name="authorships", db_column="F_COURSE_ID")
    original_name = models.CharField(max_length=255, db_column="F_AUTHORC_NAME", blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="course_authorships_added", db_column="F_AUTHORC_USER", null=True, blank=True)

    class Meta:
        db_table = "AUTHORC"
        verbose_name = u"авторство учебного курса"
        verbose_name_plural = u"авторства учебных курсов"

    def __unicode__(self):
        return u"%s, автор курса '%s'" % (self.workers_string_full, self.course.title)


class CourseTeaching(WorkershipModel, Activity):
    id = models.AutoField(primary_key=True, db_column="F_READCOURSE_ID")
    teacher = models.ForeignKey(to="workers.Worker", related_name="courses_teachings", db_column="F_MAN_ID", null=True, blank=True)
    course = models.ForeignKey(to="Course", related_name="teachings", db_column="F_COURSE_ID")
    department = models.ForeignKey(to="organizations.Department", related_name="courses_teachings", db_column="F_DEPARTMENT_ID", verbose_name=u"Подразделение")
    discipline_type = models.ForeignKey(to="DisciplineType", verbose_name=u"Тип дисциплины", db_column="F_DISCIPLINETYPE_ID")
    teaching_kind = models.ForeignKey(to="TeachingKind", verbose_name=u"Вид учебных занятий", db_column="F_COURSETYPE_ID")
    startdate = models.DateField(u"Дата начала преподавания", db_column="F_READCOURSE_START")
    enddate = models.DateField(u"Дата окончания преподавания", db_column="F_READCOURSE_END", null=True, blank=True)
    hours = models.PositiveIntegerField(u"Количество часов в учебный год", db_column="F_READCOURSE_WORKHOUR", null=True, blank=True)
    original_name = models.CharField(max_length=255, db_column="F_READCOURSE_NAME", blank=True)
    creator = models.ForeignKey(to=User, related_name="courses_teached", db_column="F_READCOURSE_USER", null=True, blank=True)

    worker_attr = 'teacher'
    workers_verbose_name_single = u"Преподаватель курса"
    workers_required_error_msg = u"Укажите преподавателя курса"
    nominative_short = u"преподавание курса"
    nominative_en = "course_teaching"
    genitive = u"преподавания курса"
    genitive_plural_full = u"преподаваний курсов"
    instrumental = u"преподаванием курса"
    locative = u"преподавании курса"
    gender = "neuter"

    class Meta:
        db_table = "READCOURSE"
        verbose_name = u"преподавание учебного курса"
        verbose_name_plural = u"преподавание учебных курсов"
        get_latest_by = "-startdate"
        ordering = ('-startdate', '-enddate')

    def __unicode__(self):
        return u"%s, преподаватель курса '%s' в %d" % (self.workers_string_full, self.course.title, self.startdate.year)

    @models.permalink
    def get_absolute_url(self):
        return ('courses_teaching_detail', (), {'object_id': self.id})

    @property
    def title(self):
        return self.course.title

    @property
    def academic_years(self):
        """ -> int
            Return number of academic years that the course teaching spans.
        """
        return academic_years(self.startdate, self.enddate)

    @property
    def total_hours(self):
        """ -> int
            Return total number of hours of the teaching.
        """
        return (self.hours or 0) * self.academic_years

    def academic_years_in_period(self, period):
        """ [int] -> int
            Return number of academic years of the course that are included the given period.
            The period is a sorted list of calendar years.
            In the first calendar year, the fall semester is taken. In the last year, the spring one.
            So if there are 6 calendar years, 5 academic years are taken.
            Then we count number of those academic years, in which the course was taught.
        """
        def robust_datetime_date(year, month, day):
            try:
                return datetime.date(year, month, day)
            except ValueError:
                # 29-th of February
                return datetime.date(year, month, day-1)
            
        period_startdate = datetime.date(period[0], 9, 1)
        period_enddate = datetime.date(period[-1], 9, 10)
        if self.startdate < period_startdate:
            if self.startdate.month < 9:
                # the course starts in spring semester, and we do not include spring semester of the first year in period.
                startdate = robust_datetime_date(period[0] + 1, self.startdate.month, self.startdate.day)
            else:
                startdate = robust_datetime_date(period[0], self.startdate.month, self.startdate.day)
        else:
            startdate = self.startdate

        if not self.enddate:
            enddate = period_enddate
        elif self.enddate > period_enddate:
            if self.enddate.month < 9 or (self.enddate.month == 9 and self.enddate.day <= 10):
                enddate = robust_datetime_date(period[-1], self.enddate.month, self.enddate.day)
            else:
                # the course ends in fall semester, and we do not include fall semester of the last year in period.
                enddate = robust_datetime_date(period[-1] - 1, self.enddate.month, self.enddate.day)
        else:
            enddate = self.enddate

        years = academic_years(startdate, enddate)
        if not years < len(period):
            raise Exception("if there are 6 calendar years, max 5 academic years should be taken")

        return years

    def total_hours_in_period(self, period):
        """ [int] -> int
            Return total number of hours of the course in the given period.
            The period is a sorted list of calendar years.
            In the first calendar year, the fall semester is taken. In the last year, the spring one.
            So if there are 6 calendar years, 5 academic years are taken.
            Then we take those academic years, in which the course was taught, and count hours.
        """
        return (self.academic_years_in_period(period) * self.hours) if self.hours else 0

    @property
    def partnership_direction_code(self):
        directions = self.partnerships.values_list('direction', flat=True)
        direction = directions[0] if directions else None
        return dict(CourseTeachingPartnership.DIRECTIONS)[direction][0]


class CourseSponsorCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_finsource_id")
    name = models.CharField(max_length=255, db_column="f_finsource_name")

    class Meta:
        db_table = u'finsource'
        verbose_name = u"тип источника финансирования"
        verbose_name_plural = u"типы источников финансирования"

    def __unicode__(self):
        return self.name


class CourseTeachingPartnership(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_courseforeign_id")
    course_teaching = models.ForeignKey(CourseTeaching, related_name="partnerships", db_column="f_readcourse_id")
    organization_name = models.CharField(u"название внешней организации", max_length=255, db_column="f_courseforeign_organ")
    organization_category = models.ForeignKey("projects.AccompliceCategory", verbose_name=u"тип внешней организации", related_name="course_teaching_partnerships", db_column="f_accomplicetype_id")
    organization_subcategory = models.ForeignKey("journals.JournalRubric", verbose_name=u"подтип внешней организации", related_name="course_teaching_partnerships", db_column="F_JOURNALRUB_ID")
    organization_country = models.ForeignKey(Country, verbose_name=u"Страна внешней организации", related_name="course_teaching_partnerships", db_column="f_country_id")
    sponsor_category = models.ForeignKey(CourseSponsorCategory, verbose_name=u"тип источника финансирования", related_name="course_teaching_partnerships",
        help_text=u"необязательно; доступно только ответственным; укажите, если сотрудник получил финансирование на преподавание курса", db_column="F_FINSOURCE_ID", null=True, blank=True)
    sponsor_name = models.CharField(u"название источника финансирования", max_length=255, db_column="f_courseforeign_source", blank=True)
    direction = models.NullBooleanField(u"направление", db_column="f_courseforeign_inout", null=True, blank=True)
    amount = models.FloatField(u"привлеченная сумма финансирования (тыс. руб.)", db_column="F_COURSEFOREIGN_SUM", null=True, blank=True)
    creator = models.ForeignKey("auth.User", related_name="course_teaching_partnerships_added", verbose_name=u"создатель", db_column="f_courseforeign_user", null=True, blank=True)

    class Meta:
        db_table = u'courseforeign'
        verbose_name = u"преподавание курса в/из внешних организаций"
        verbose_name_plural = u"преподавание курсов в/из внешних организаций"

    DIRECTIONS = (
        (None, ("internal", u"преподавание в подразделении внутри своей организации")),
        (True, ("importing", u"сотрудник внешней организации, преподавание в подразделении")),
        (False, ("exporting", u"сотрудник подразделения, преподавание во внешней организации")),
    )

    gender = "neuter"

    def __unicode__(self):
        return u"Преподавание курса %s, %s" % (self.course_teaching, self.organization_name)

    @classmethod
    def direction_choices(cls):
        return [values for key, values in cls.DIRECTIONS]

    @property
    def direction_text(self):
        return dict(self.DIRECTIONS)[self.direction][1]

    @property
    def direction_code(self):
        return dict(self.DIRECTIONS)[self.direction][0]

    @direction_code.setter
    def direction_code(self, code):
        for key, values in self.DIRECTIONS:
            if values[0] == code:
                self.direction = key

    @property
    def organization_preposition(self):
        if self.direction_code == 'importing':
            prep = u'из'
        elif self.direction_code == 'exporting':
            prep = u'в'
        else:
            prep = u''
        return prep

from courses.admin import *
