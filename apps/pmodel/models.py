# -*- coding: utf-8; -*-

# Author: Dmitry Shachnev <mitya57@gmail.com>, 2014

from django.db import models
from organizations.models import Department, NestedParentDepartment, Organization
import datetime

class PointsFormula(models.Model):
    id = models.AutoField(primary_key=True, db_column='F_POINTSFORMULA_ID')
    title = models.CharField(max_length=255, db_column='F_POINTSFORMULA_TITLE')
    createdby = models.ForeignKey("auth.User", related_name='created_by', db_column='F_POINTSFORMULA_USER')
    creationdate = models.DateField('Creation Date', auto_now_add=True, db_column='F_POINTSFORMULA_CREATED')
    modificationdate = models.DateField('Last Modification Date', auto_now=True, db_column='F_POINTSFORMULA_MODIFIED')
    jsondata = models.TextField('JSON data', db_column='F_POINTSFORMULA_JSONDATA')
    begin = models.IntegerField('Start year', db_column='F_POINTSFORMULA_BEGIN', null=True, blank=True)
    end = models.IntegerField('End year', db_column='F_POINTSFORMULA_END', null=True, blank=True)

    class Meta(object):
        db_table = 'POINTSFORMULA'

    def __str__(self):
        return self.title

    __unicode__ = __str__

    def get_absolute_url(self):
        return '/pmodel/view/%i/' % self.id

    def publish(self, dep, publisher=None, for_admins=None):
        link = PointsFormulaDepartments.objects.create(
            formula=self,
            department=dep,
            creationdate = datetime.datetime.now().replace(microsecond=0),
            createdby=publisher or self.createdby,
            for_admins=1 if for_admins else 0)

    def copy(self, user=None):
        new_formula = PointsFormula.objects.create(
            title = u'(Копия): ' + self.title,
            createdby = user or self.createdby,
            creationdate = datetime.datetime.now().replace(microsecond=0),
            begin = self.begin,
            end = self.end,
            jsondata = self.jsondata)
        return new_formula

    def is_published(self):
        return self.published_for.exists()

    def matches(self, user):
        "Checks that this formula was published for one of the user's department."
        worker = user.get_profile().worker
        if worker:
            deps = worker.current_employments.values_list('department')
            alldeps = NestedParentDepartment.objects.filter(department__in=deps).values_list('parent')
            return self.published_for.filter(department__in=alldeps, for_admins=0)
        return False

    @property
    def main_departments(self):
        result = {}
        for pfdep in PointsFormulaDepartments.objects.filter(formula=self):
            if pfdep.department.is_root:
                result[pfdep.department] = True
            else:
                for root in pfdep.department.roots:
                    if root not in result:
                        result[root] = False
        return result.items()


class PointsFormulaDepartments(models.Model):
    id = models.AutoField(primary_key=True, db_column='F_PFDEPARTMENTLINK_ID')
    formula = models.ForeignKey(PointsFormula, related_name='published_for', db_column='F_POINTSFORMULA_ID')
    department = models.ForeignKey(Department, related_name='formulas', db_column='F_DEPARTMENT_ID')
    creationdate = models.DateField('Creation Date', auto_now_add=True, db_column='F_PFDEPARTMENTLINK_CREATED')
    createdby = models.ForeignKey("auth.User", related_name='published_formulas', db_column='F_PFDEPARTMENTLINK_USER')
    for_admins = models.IntegerField('For admins', db_column='F_PFDEPARTMENTLINK_FORADMINS')

    class Meta(object):
        db_table = 'PFDEPARTMENTLINK'

    def __str__(self):
        return u"'%s' published for '%s'" % (self.formula, self.department)

    __unicode__ = __str__


class CustomCategory(models.Model):
    "User defined catogory, i.e. base category with modifiers."
    id = models.AutoField(primary_key=True, db_column='F_PFCATEGORY_ID')
    name = models.CharField(max_length=1000, db_column='F_PFCATEGORY_NAME') # Short name
    title = models.CharField(max_length=1000, db_column='F_PFCATEGORY_TITLE') # A bit longer description
    createdby = models.ForeignKey("auth.User", related_name='formula_categories', db_column='F_PFCATEGORY_USER')
    basecategory = models.CharField(max_length=255, db_column='F_PFCATEGORY_BASECATEGORY')
    creationdate = models.DateField('Creation Date', auto_now_add=True, db_column='F_PFCATEGORY_CREATED')
    modificationdate = models.DateField('Modification Date', auto_now_add=True, db_column='F_PFCATEGORY_MODIFIED')
    jsondata = models.TextField('JSON data', db_column='F_PFCATEGORY_JSONDATA')

    class Meta(object):
        db_table = 'PFCATEGORY'


class RatingCache(models.Model):
    id = models.AutoField(primary_key=True, db_column='F_RATINGCACHE_ID')
    formula = models.ForeignKey(PointsFormula, db_column='F_POINTSFORMULA_ID')
    worker = models.ForeignKey('workers.Worker', db_column='F_MAN_ID')
    value = models.FloatField('Value', db_column='F_RATINGCACHE_VALUE', null=True)
    date = models.DateField('Date', db_column='F_RATINGCACHE_DATE', auto_now_add=True)
    from_year = models.IntegerField('From year', db_column='F_RATINGCACHE_FROMYEAR', null=True, blank=True)
    to_year = models.IntegerField('To year', db_column='F_RATINGCACHE_TOYEAR', null=True, blank=True)

    class Meta(object):
        db_table = 'RATINGCACHE'


class JournalList(models.Model):
    "User defined list of journals"
    id = models.AutoField(primary_key=True, db_column='F_JOURNALLIST_ID')
    department = models.ForeignKey(Department, related_name='journal_lists', db_column='F_DEPARTMENT_ID')
    organization = models.ForeignKey(Organization, related_name='journal_lists', db_column='F_ORGANIZATION_ID')
    name = models.CharField(max_length=1000, db_column='F_JOURNALLIST_NAME')
    creationdate = models.DateField('Creation Date', auto_now_add=True, db_column='F_JOURNALLIST_CREATE')
    createdby = models.ForeignKey("auth.User", related_name='created_journal_lists', db_column='F_JOURNALLIST_USER')

    class Meta(object):
        db_table = 'JOURNALLIST'

    def get_absolute_url(self):
        return '/pmodel/journallists/%i/' % self.id


class JournalListItem(models.Model):
    "An item of JournalList"
    id = models.AutoField(primary_key=True, db_column='F_JOURNALLISTITEM_ID')
    list = models.ForeignKey(JournalList, related_name='items', db_column='F_JOURNALLIST_ID')
    journal = models.ForeignKey("journals.Journal", related_name='present_in_lists', db_column='F_JOURNAL_ID')
    begin = models.DateField('Start date', db_column='F_JOURNALLISTITEM_BEGIN', null=True, blank=True)
    end = models.DateField('End date', db_column='F_JOURNALLISTITEM_END', null=True, blank=True)
    creationdate = models.DateField('Creation Date', auto_now_add=True, db_column='F_JOURNALLISTITEM_CREATE')
    createdby = models.ForeignKey("auth.User", related_name='created_journal_list_items', db_column='F_JOURNALLISTITEM_USER')

    class Meta(object):
        db_table = 'JOURNALLISTITEM'


class EducForm(models.Model):
    id = models.AutoField(primary_key=True, db_column='F_EDUCFORM_ID')
    name = models.CharField(max_length=255, db_column='F_EDUCFORM_NAME')

    class Meta(object):
        db_table = 'EDUCFORM'
        
    def __unicode__(self):
        return "%s" % self.name    


class EducType(models.Model):
    id = models.AutoField(primary_key=True, db_column='F_EDUCTYPE_ID')
    name = models.CharField(max_length=255, db_column='F_EDUCTYPE_NAME')

    class Meta(object):
        db_table = 'EDUCTYPE'
        
    def __unicode__(self):
        return "%s" % self.name    


class EducStage(models.Model):
    id = models.AutoField(primary_key=True, db_column='F_EDUCSTAGE_ID')
    name = models.CharField(max_length=255, db_column='F_EDUCSTAGE_NAME')

    class Meta(object):
        db_table = 'EDUCSTAGE'

    def __unicode__(self):
        return "%s" % self.name    