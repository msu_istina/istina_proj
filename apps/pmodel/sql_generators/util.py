# -*- coding: utf-8; -*-

import re
from ..models import CustomCategory

format_re = re.compile(r"(.*)[.]custom([0-9]+)$")

def get_restrictions(rule, make_unique=False):
    """Returns a list representing restrictions."""
    restrictions = rule.get('restrictions', [])
    match = format_re.match(rule['category'])
    if match:
        from json import loads
        custom_category_id = match.group(2)
        try:
            custom_cat = CustomCategory.objects.get(pk=custom_category_id)
            restrictions = loads(custom_cat.jsondata) + restrictions
        except CustomCategory.DoesNotExist:
            pass #--- TODO: log error

    # Normalize
    for restriction in restrictions:
        if 'lower_bound' in restriction and isinstance(restriction['lower_bound'], str):
            try:
                restriction['lower_bound'] = float(restriction['lower_bound'])
            except ValueError: # Cannot convert
                restriction['lower_bound'] = None
        if 'upper_bound' in restriction and isinstance(restriction['upper_bound'], str):
            try:
                restriction['upper_bound'] = float(restriction['upper_bound'])
            except ValueError: # Cannot convert
                restriction['upper_bound'] = None

    if not make_unique:
        return restrictions

    # Make unique
    restrictions_dict = {}
    for restriction in restrictions:
        parameter = restriction['parameter']
        lower_bound = restriction.get('lower_bound', None)
        upper_bound = restriction.get('upper_bound', None)
        if 'value' in restriction: # A boolean restriction
            restrictions_dict[parameter] = restriction['value']
            continue
        if parameter in restrictions_dict:
            old_lower_bound, old_upper_bound = restrictions_dict[parameter]
            if (old_lower_bound is None) or (lower_bound is None):
                lower_bound = old_lower_bound or lower_bound
            else:
                lower_bound = max(old_lower_bound, lower_bound)
            if (old_upper_bound is None) or (upper_bound is None):
                upper_bound = old_upper_bound or upper_bound
            else:
                upper_bound = min(old_upper_bound, upper_bound)
        restrictions_dict[parameter] = lower_bound, upper_bound
    restrictions = []
    for parameter, everything in restrictions_dict.iteritems():
        if isinstance(everything, bool):
            restrictions.append({
                'parameter': parameter,
                'value': everything
            })
        elif isinstance(everything, tuple):
            lower_bound, upper_bound = everything
            restrictions.append({
                'parameter': parameter,
                'lower_bound': lower_bound,
                'upper_bound': upper_bound
            })
    return restrictions

def get_bound_value(str):
    try:
        return float(str)
    except (ValueError, TypeError):
        return None
