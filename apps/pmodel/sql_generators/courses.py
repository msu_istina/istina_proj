# -*- coding: utf-8; -*-

def generate_courses_sql(rule, additional_constraints, duration_in_years=None, course_type='lectures'):
    FULL_YEAR_MIN_DURATION = 60
    FULL_YEAR_MAX_DURATION = 90

    COURSE_TYEPS = {
        'lectures': 1248893,
        'seminars': 1248894,
        'labs': 1248895,
        'practics': 1248896,
        }

    DISCIPLINE_TYPES = {
        'main': 1248897,
        }

    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'what_to_sum': '1'
        }

    if rule['parameter'] == 'academic_hours':
        query_inline_parameters['what_to_sum'] = 'nvl(readcourse.f_readcourse_workhour, 1)'

    # Filter by course types
    query_inline_parameters['type_selector_cond'] = ''
    if course_type == 'lectures_main':
        query_inline_parameters['type_selector_cond'] = """ and (
            readcourse.f_coursetype_id = %(course_type)s and readcourse.f_disciplinetype_id = %(discipline_type)s
        ) """ % {'course_type': COURSE_TYEPS['lectures'],
                 'discipline_type': DISCIPLINE_TYPES['main'],
                 }
    elif course_type == 'lectures_misc':
        query_inline_parameters['type_selector_cond'] = """ and (
            readcourse.f_coursetype_id = %(course_type)s and nvl(readcourse.f_disciplinetype_id,-1) <> %(discipline_type)s
        ) """ % {'course_type': COURSE_TYEPS['lectures'],
                 'discipline_type': DISCIPLINE_TYPES['main'],
                 }
    elif course_type in ['seminars', 'labs', 'practics']:
        query_inline_parameters['type_selector_cond'] = """ and (
            readcourse.f_coursetype_id = %(course_type)s
        ) """ % {'course_type': COURSE_TYEPS[course_type]}

    # Filter by course duration
    query_inline_parameters['duration_cond'] = ''
    if duration_in_years:
        query_inline_parameters['duration_cond'] = """       and readcourse.f_readcourse_workhour >= %(min_duration)s
        and readcourse.f_readcourse_workhour <= %(max_duration)s """ % {
            'min_duration': FULL_YEAR_MIN_DURATION * duration_in_years,
            'max_duration': FULL_YEAR_MAX_DURATION * duration_in_years,
            }
    
    return u"""select
         readcourse.f_man_id,
         readcourse.f_readcourse_id AS f_object_id,
         (
         (%(what_to_sum)s)
         *
         ( /* course duration in years */
              (least(extract(year from nvl(readcourse.f_readcourse_end, sysdate)), %(end_year)s)
               - greatest(%(start_year)s, extract(year from readcourse.f_readcourse_start))
              )
              +
              case
                when readcourse.f_readcourse_end is null then 1
                when extract(year from readcourse.f_readcourse_end) = extract(year from readcourse.f_readcourse_start) then 1
                when extract(year from readcourse.f_readcourse_end) > %(end_year)s then 1
                when extract(year from readcourse.f_readcourse_end) = %(start_year)s then 1
                else 0
              end
          )
          ) points
         , course.f_course_name
             || ' (' || greatest(%(start_year)s, extract(year from readcourse.f_readcourse_start))
             || '-'
             || least(extract(year from nvl(readcourse.f_readcourse_end, sysdate)), %(end_year)s)
             || ')'  as act_name
         , 'Объём' as act_paramname
         , to_char(readcourse.f_readcourse_workhour) as act_paramvalue
         , NULL act_nauthors
         , greatest(%(start_year)s, extract(year from readcourse.f_readcourse_start)) as year
    from readcourse
         join course on (course.f_course_id = readcourse.f_course_id)
         join department on (department.f_department_id = readcourse.f_department_id)
    where (readcourse.f_readcourse_end is null or readcourse.f_readcourse_end >= to_date('01.01.%(start_year)s', 'dd.mm.yyyy'))
      and readcourse.f_readcourse_start <= to_date('31.12.%(end_year)s', 'dd.mm.yyyy')
      /* duration */ %(duration_cond)s
      /* type */ %(type_selector_cond)s
      and department.f_organization_id = 214524 /* MSU */
    """ % query_inline_parameters
