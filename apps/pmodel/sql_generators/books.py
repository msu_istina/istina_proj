# -*- coding: utf-8; -*-

from .util import get_bound_value, get_restrictions

def generate_books_sql(rule, additional_constraints,
                       include_bookgroups=None, exclude_bookgroups=None,
                       authors_count_min=None, authors_count_max=None,
                       restrict_to_russian=None):
    personal_report_for = additional_constraints.get('report_worker_id', None)
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'what_to_sum': '1',
        'divider': '1',
        'bookgroup_where': '',
        'authors_count_join': '',
        'report_worker_id': personal_report_for,
        }


    if include_bookgroups or exclude_bookgroups:
        if include_bookgroups:
            ids = ",".join([str(x) for x in include_bookgroups])
            in_or_not_in = "IN"
        else:
            ids = ",".join([str(x) for x in exclude_bookgroups])
            in_or_not_in = "NOT IN"
        query_inline_parameters['bookgroup_where'] = """
            (book.f_book_id """ + in_or_not_in + """ (
                select f_book_id
                from bookgroup
                where bookgroup.f_bookgrouptype_id in (""" + ids + ")))"

    filtering_constraints = []
    for restriction in get_restrictions(rule):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        if param in ('coauthors_count', 'coauthors_count.hidden'):
            authors_count_min = lower_bound
            authors_count_max = upper_bound
        elif param in ('redpages_count', 'pages_count'):
            if param == 'redpages_count':
                expr = u"nvl(book.f_book_redpage, nvl(book.f_book_pages, 50)/16)"
            else:
                expr = u"nvl(book.f_book_pages, 50)"
            if lower_bound != None:
                filtering_constraints.append( (u"%s <= " % lower_bound) + expr )
            if upper_bound != None:
                filtering_constraints.append( expr + (u" <= %s" % upper_bound) )

    if authors_count_min or authors_count_max:
        having = ' '
        if authors_count_min:
            having += 'count(*)>=%s' % int(authors_count_min)
            if authors_count_max:
                having += ' and '
        if authors_count_max:
            having += 'count(*)<=%s' % int(authors_count_max)
            
        query_inline_parameters['authors_count_join'] = """ join
        (select f_book_id
        from authorb
        group by f_book_id
        having %s
        ) au_count on (au_count.f_book_id = ab.f_book_id)""" % having

    if rule['parameter'] == 'pages_count':
        query_inline_parameters['what_to_sum'] = 'nvl(book.f_book_pages, 50)'
    elif rule['parameter'] == 'redpages_count':
        query_inline_parameters['what_to_sum'] = 'nvl(book.f_book_redpage, nvl(book.f_book_pages, 50)/16)'
    else:
        query_inline_parameters['what_to_sum'] = '1'

    if ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'nvl(sqrt(sum(1) over (partition by ab.f_book_id)), 1)'
    elif ['divide_coauthors'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'nvl(sum(1) over (partition by ab.f_book_id), 1)'

    if restrict_to_russian:
        ## Title in Russian
        filtering_constraints.append( u"""nvl(length(translate(nls_upper(book.f_book_name),'~ABCDEFGHIJKLMNOPQRSTUVWXYZ .-;:/"(),[]=0123456789','~')),0)>3""" )

    return (u"""select ab.f_man_id,
       ab.f_book_id AS f_object_id,
       (%(what_to_sum)s / (%(divider)s)) points,
       book.f_book_name || ', ' || decode(book.f_book_pages, null, '', to_char(f_book_pages) || 'с., ') || book.f_book_year AS act_name,
       'Тип' as act_paramname,
       nvl(vids.names_str, '??') as act_paramvalue,
       sum(1) over (partition by ab.f_book_id) act_nauthors,
       book.f_book_year as year
    from authorb ab
         join book on (book.f_book_id = ab.f_book_id)
         left join (select f_book_id,
                        listagg( decode(f_bookgrouptype_id,
                                                      100, 'Учебник',
                                                      217739, 'Учебное пособие',
                                                      6054932, 'Учебное пособие',
                                                      120, 'Учебно-методическая литература',
                                                      140, 'Словарь',
                                                      190, 'Моногр.',
                                                      130, 'Научно-поп. издание',
                                                      217741, 'Стер.',
                                                      ''),
                                              '; ')
                                within group (order by f_bookgrouptype_id) names_str
                  from bookgroup
                  group by f_book_id
         ) vids on (vids.f_book_id = book.f_book_id)
         /* au */ %(authors_count_join)s
    where (book.f_book_year >= %(start_year)s and book.f_book_year <= %(end_year)s)
      and (%(bookgroup_where)s)
    """ + (u" AND (" + u" AND ".join(filtering_constraints) + u")" if filtering_constraints else "") \
            ) % query_inline_parameters
