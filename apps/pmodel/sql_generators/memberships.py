# -*- coding: utf-8; -*-

from .util import get_bound_value, get_restrictions

def generate_collection_editorial_sql(rule, additional_constraints):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'what_to_count': '1',
        'divider': '1',
        'filter_by_pages': '',
        'filter_by_marks': '',
        }

    def role_to_decode(field_name, default=None):
        "Generates SQL decode clause that maps all known editorship roles into user-friendly descriptions."
        from icollections.models import COLLECTION_EDITORSHIP_ROLES
        decode = u"decode(" + field_name
        for (code, label) in COLLECTION_EDITORSHIP_ROLES:
            decode += u", '%(code)s', '%(label)s'" % { 'code': code, 'label': label }
        if default:
            decode += u", '%s'" % default
        decode += ")"
        return decode

    if rule['parameter'] == 'pages_count':
        query_inline_parameters['what_to_count'] = 'nvl(collection.f_collection_pages, 50)'

    if ['divide_coeditors'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'nvl(sum(1) over (partition by corrector.f_collection_id), 1)'

    for seq, restriction in enumerate(get_restrictions(rule)):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        bool_value = restriction.get('value', None)
        if param == 'pages_count':
            if lower_bound != None:
                query_inline_parameters['filter_by_pages'] += " and %s <= collection.f_collection_pages " % (lower_bound, )
            if upper_bound != None:
                query_inline_parameters['filter_by_pages'] += " and collection.f_collection_pages <= %s " % (upper_bound, )
        if param == 'confirmed_only' and (bool_value == True):
            # Сборник был подтвержден
            #--- TODO: учитывать подразделение, для которого этот сборник считается хорошим
            query_inline_parameters['filter_by_marks'] = """and
            corrector.f_collection_id IN (SELECT collectionmark.f_collection_id FROM collectionmark WHERE collectionmark.f_collectionmark_type = 'selected')
            """

    return (u"""SELECT corrector.f_man_id
         , corrector.f_collection_id AS f_object_id
         , ((%(what_to_count)s) / (%(divider)s)) AS points
         , collection.f_collection_name || ' (' || collection.f_collection_year || ')' as act_name
         , 'Тип' as act_paramname
         , """ + role_to_decode('corrector.f_corrector_role', u'член редколлегии') + u""" AS act_paramvalue
         , sum(1) over (partition by corrector.f_collection_id) act_nauthors
         , collection.f_collection_year as year
    FROM corrector
         JOIN collection ON (collection.f_collection_id = corrector.f_collection_id)
    where collection.f_collection_year >= %(start_year)s and collection.f_collection_year <= %(end_year)s
       /* filter by pages */ %(filter_by_pages)s
       /* filter by marks */ %(filter_by_marks)s
    """) % query_inline_parameters



def generate_journal_boards_sql(rule, additional_constraints):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'what_to_count': '1',
        'impact_factor_join': '',
        'impact_factor_filter_join': '',
        'filter_by_impact': '',
        'filter_by_journals_list': '',
        }

    ifactor_subquery = """
           left join (
               select f_journal_id, max(F_JOURNALRANG_VAL) value
               from journalrang
               where F_JOURNALRANG_TYPE in (%(impact_type)s)
               group by f_journal_id
             ) ifactor%(table_suffix)s on (ifactor%(table_suffix)s.f_journal_id = redjournal.f_journal_id)"""
    IMPACTS = {'impact_factor.isi': "'Impact Factor 2013', '5-Year Impact Factor 2013'",
               'impact_factor.rsci': "'RINC'",
               }

    if rule['parameter'].startswith('impact_factor.'):
        query_inline_parameters['what_to_count'] = 'nvl(ifactor.value, 0)'
        query_inline_parameters['impact_factor_join'] = ifactor_subquery % {'impact_type': IMPACTS.get(rule['parameter'], "'UNKNOWN'"),
                                                                            'table_suffix': ''}

    for seq, restriction in enumerate(get_restrictions(rule)):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        bool_value = restriction.get('value', None)
        if param.startswith('impact_factor.'):
            filter_table_suffix = ''
            if param == rule['parameter']:
                # Filter journal by the same type of impact factor; reuse ifactor subquery
                pass
            else:
                filter_table_suffix = '_filter%d' % seq
                query_inline_parameters['impact_factor_filter_join'] += (
                    ifactor_subquery % {'impact_type': IMPACTS.get(param, "'UNKNOWN'"),
                                        'table_suffix': filter_table_suffix})
            if lower_bound != None:
                query_inline_parameters['filter_by_impact'] += " and %s <= ifactor%s.value " % (lower_bound, filter_table_suffix)
            if upper_bound != None:
                query_inline_parameters['filter_by_impact'] += " and ifactor%s.value <= %s " % (filter_table_suffix, upper_bound)
        elif param == 'vak_journal':
            query_inline_parameters['filter_by_journals_list'] += """ and
            redjournal.f_journal_id IN
             ( SELECT jornalvak.f_journal_id
               FROM jornalvak
               WHERE jornalvak.f_journalvaktype_id
                               IN (
                                    SELECT f_journalvaktype_id
                                    FROM journalvaktype
                                    WHERE f_journalvaktype_name IN (%(vak_list_name)s))
               GROUP BY jornalvak.f_journal_id
             )""" % {'vak_list_name': u"'Список ВАК'" }

    return u"""SELECT redjournal.f_man_id
         , redjournal.f_journal_id AS f_object_id
         , %(what_to_count)s points
         , journal.f_journal_name as act_name
         , 'Тип' as act_paramname
         , NULL as act_paramvalue
         , NULL act_nauthors
         , nvl(extract(year from redjournal.f_redjournal_end), %(start_year)s) as year
    FROM redjournal
         JOIN journal ON (journal.f_journal_id = redjournal.f_journal_id)
         /* IF join */ %(impact_factor_join)s
         /* IF filter join */ %(impact_factor_filter_join)s
    WHERE extract(year from redjournal.f_redjournal_begin) <= %(end_year)s
      and NVL(extract(year from redjournal.f_redjournal_end), %(end_year)s) >= %(start_year)s
      and redjournal.f_redjournal_role in ('cheif', 'member')
      /* filter by impact */ %(filter_by_impact)s
      /* filter by list membership */ %(filter_by_journals_list)s
    """ % query_inline_parameters


def generate_discouncils_sql(rule, additional_constraints):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'what_to_count': '1',
        'filter_by_role': '',
        }

    DIS_ROLES = {
        'dis_leader': 5830363,
        'dis_deputy_leader': 5830361,
        'dis_secretary': 5830360
        }

    for seq, restriction in enumerate(get_restrictions(rule)):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        bool_value = restriction.get('value', None)
        if param in ['dis_leader', 'dis_deputy_leader', 'dis_secretary']:
            query_inline_parameters['filter_by_role'] = u""" AND (
            inboard.f_inboardrole_id %s %d
            )
            """ % (("=" if bool_value else "<>"), DIS_ROLES[param])


    return u"""SELECT inboard.f_man_id
         , inboard.f_board_id AS f_object_id
         , 1 points
         , board.f_board_num as act_name
         , 'Тип' as act_paramname
         , NULL as act_paramvalue
         , NULL act_nauthors
         , nvl(extract(year from inboard.f_inboard_end), %(start_year)s) as year
    FROM inboard
         JOIN board ON (board.f_board_id = inboard.f_board_id)
    WHERE extract(year from inboard.f_inboard_begin) <= %(end_year)s and NVL(extract(year from inboard.f_inboard_end), %(end_year)s) >= %(start_year)s
        /* filter by role */ %(filter_by_role)s
    """ % query_inline_parameters
