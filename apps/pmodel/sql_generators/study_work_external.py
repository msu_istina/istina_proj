# -*- coding: utf-8; -*-

import re
from .util import get_bound_value, get_restrictions

type_re = re.compile('study_work_external.type(\d+)')

def generate_study_work_external_sql(rule, additional_constraints):
    eductype_id = type_re.match(rule['category']).group(1)

    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'multiplier': '1',
        'what_to_count': '1',
        'eductype_id': eductype_id,
        'filter_by_semester': '',
        'filter_by_hours': '',
        'filter_by_stage': '',
        'filter_by_form': '',
    }

    if rule['parameter'] == 'budget_students_count':
        query_inline_parameters['what_to_count'] = 'nvl(f_educwork_studbudjet, 1)'
    elif rule['parameter'] == 'paid_students_count':
        query_inline_parameters['what_to_count'] = 'nvl(f_educwork_studpaid, 1)'
    elif rule['parameter'] == 'works_count':
        query_inline_parameters['what_to_count'] = 'nvl(f_educwork_workcount, 1)'
    elif rule['parameter'] == 'weeks_count':
        query_inline_parameters['what_to_count'] = 'nvl(f_educwork_week, 1)'
    elif rule['parameter'] == 'total_hours_count':
        query_inline_parameters['what_to_count'] = 'nvl(f_educwork_week, 1) * nvl(f_educwork_hour, 1)'

    if ['divide_coauthors'] in rule.get('modifiers', []):
        query_inline_parameters['multiplier'] = 'NVL(ewm.f_educworkman_part, 1)'

    for restriction in get_restrictions(rule):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        if param == 'study_semester':
            if lower_bound != None:
                query_inline_parameters['filter_by_semester'] += " and %s <= nvl(ew.f_educwork_semester, 20) " % lower_bound
            if upper_bound != None:
                query_inline_parameters['filter_by_semester'] += " and nvl(ew.f_educwork_semester, 0) <= %s " % upper_bound
        elif param == 'total_hours_count':
            filter_by_hours = ""
            if lower_bound != None:
                filter_by_hours += " and %s <= (nvl(ew.f_educwork_hour, 1) * nvl(ew.f_educwork_week, 1)) " % lower_bound
            if upper_bound != None:
                filter_by_hours += " and (nvl(ew.f_educwork_hour, 0) * nvl(ew.f_educwork_week, 1)) <= %s " % upper_bound
            query_inline_parameters['filter_by_hours'] = filter_by_hours

    if 'educStage' in rule:
        query_inline_parameters['filter_by_stage'] += ' and ew.f_educstage_id = %s' % rule['educStage']
    if 'educForm' in rule:
        query_inline_parameters['filter_by_form'] += ' and ew.f_educform_id = %s' % rule['educForm']

    return u"""select ewm.f_man_id
         , ew.f_educwork_id as f_object_id
         , (%(what_to_count)s) * (%(multiplier)s) points
         , ew.f_educwork_name
             || ', ' || ew.f_educwork_year || '-' || (ew.f_educwork_year + 1) || ' уч. год'
             || decode(department.f_department_name, null, '', ', ' || department.f_department_name)
             || (select ', группы: ' || nvl(listagg (f_educgroup_name, ', ') within group (order by f_educgroup_name), 'н/д')
                 from educgroup where f_educwork_id = ew.f_educwork_id)
             || decode(few.f_feducwork_key,          null, '', ' (ID: ' || few.f_feducwork_key || ')')
           as act_name
         , 'Тип' as act_paramname
         , et.f_eductype_name as act_paramvalue
         , NVL(ewm.f_educworkman_part, 1) as act_nauthors
         , ew.f_educwork_year as year
    from educwork ew
         join eductype et on (et.f_eductype_id = ew.f_eductype_id)
         join educworkman ewm on (ewm.f_educwork_id = ew.f_educwork_id)
         left join feducwork few on (few.f_educwork_id = ew.f_educwork_id)
         left join department on (ew.f_department_id = department.f_department_id)
    where ew.f_educwork_year >= %(start_year)s and ew.f_educwork_year <= %(end_year)s
    and get_educwork_status(ew.f_educwork_id) = 1
    and et.f_eductype_id = %(eductype_id)s
    %(filter_by_semester)s
    %(filter_by_hours)s
    %(filter_by_stage)s %(filter_by_form)s
    """ % query_inline_parameters
