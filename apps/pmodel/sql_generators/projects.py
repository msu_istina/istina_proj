# -*- coding: utf-8; -*-

from common.db_constants import DB_CONSTANTS
from .util import get_bound_value, get_restrictions

def generate_projects_sql(rule, additional_constraints, is_leader=None):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'request_user_id': additional_constraints.get('request_user_id', -1),
        'what_to_count': 'decode(projstepman.f_project_id, null, 0, 1)',
        'budget_sponsor': DB_CONSTANTS["sponsors"]["budget"],
        'filter_by_budget': '',
        }

    if rule['parameter'] == 'money':
        query_inline_parameters['what_to_count'] = "nvl(estimate_self.f_estimate_value, nvl(estimate_total.f_estimate_value, 0))"

    if ['divide_coauthors'] in rule.get('modifiers', []) or ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        if ['divide_coauthors'] in rule.get('modifiers', []):
            query_inline_parameters['what_to_count'] = '(%s)/coworkers_count.ncoworkers' % (query_inline_parameters['what_to_count'],)
        elif ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
            query_inline_parameters['what_to_count'] = '(%s)/sqrt(coworkers_count.ncoworkers)' % (query_inline_parameters['what_to_count'],)

    if additional_constraints.get('report_worker_id', -1) == -1:
        projects_filter_cond = u""" and project.f_department_id in (
                                    select f_department_id
                                    from v_granted_roles
                                    where f_permissionstypes_name IN ('is_representative_for', 'can_view_ratings')
                                      and user_id = %(request_user_id)s
          )"""
    else:
        # It's a personal report
        if additional_constraints.get('report_department_id') == None:
            projects_filter_cond = u"/* no filter by project departments for personal reports */"
        else:
            projects_filter_cond = u""" and project.f_department_id in (
               select f_department_id from all_parent_department where parent_f_department_id = %(depid)s
               union all
               select parent_f_department_id from all_parent_department where f_department_id = %(depid)s
            )
            """ % {
                'depid': additional_constraints.get('report_department_id', -1)
                }

    filtering_constraints = []
    budget_only = None
    budget_included = None
    for restriction in get_restrictions(rule):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        bool_value = restriction.get('value', None)
        if param == 'money':
            expr = u"nvl(estimate_self.f_estimate_value, nvl(estimate_total.f_estimate_value, 0))"
            if lower_bound != None:
                filtering_constraints.append( (u"%s <= " % lower_bound) + expr )
            if upper_bound != None:
                filtering_constraints.append( expr + (u" <= %s" % upper_bound) )
        if param == 'budget_included':
            budget_included = bool_value
        if param == 'budget_only' and (bool_value == True or bool_value == None):
            # bool_value == None для обратной совместимости с небинарным параметром
            budget_only = True
        if param == 'last_year_only' and (bool_value == True):
            query_inline_parameters['start_year'] = query_inline_parameters['end_year']

    query_inline_parameters['filter_by_budget'] = u''
    if budget_included is None and budget_only is None:
        # Обратная совместимость: ничего не указано = госбюджет не считать
        budget_included = False
    if budget_only or budget_included == False:
        query_inline_parameters['filter_by_budget'] += (
            u"AND %(not)s EXISTS (select 1 from sponsor where f_sponsortype_id = %(budget_sponsor)s and f_project_id = project.f_project_id)"
            ) % { 'budget_sponsor': DB_CONSTANTS["sponsors"]["budget"],
                  'not': '' if budget_only else 'NOT'
                  }
        if budget_only and budget_included == False:
            # невозможная комбинация, добавляем ложное условие
            query_inline_parameters['filter_by_budget'] = 'AND 1=0'
    if is_leader is not None:
        leadership_value = 1 if is_leader else 0
        filtering_constraints.append( u"nvl(projstepman.f_projstepman_head, 0) = %(leadership_value)s" % {'leadership_value': leadership_value,} )
        
    projects_filter_cond += ((u"\n    AND " + u" AND ".join(filtering_constraints)) if filtering_constraints else "")

    return (u"""select projstepman.f_man_id
         , projstep.f_projstep_id AS f_object_id
         , nvl(projstepman.f_projstepman_ktu, 1.0) * (%(what_to_count)s) points
         , project.f_project_name || ' (этап ' || projstep.f_projstep_num || to_char(projstep.f_projstep_end, ', yyyy)') as act_name
         , 'Тип' as act_paramname
         , 'НИР' as act_paramvalue
         , coworkers_count.ncoworkers act_nauthors
         , extract(year from projstep.f_projstep_end) as year
    from projstepman
         join projstep on (projstep.f_project_id = projstepman.f_project_id and projstep.f_projstep_id = projstepman.f_projstep_id)
         join project on (project.f_project_id = projstepman.f_project_id)
         join (select f_projstep_id, count(distinct f_man_id) ncoworkers
                 from projstepman
                 group by f_projstep_id) coworkers_count on (coworkers_count.f_projstep_id = projstepman.f_projstep_id)
         left join (select estimate.f_estimate_value, estimate.f_projstep_id
                    from estimate join estimatetype on (estimate.f_estimatetype_id = estimatetype.f_estimatetype_id)
                    where estimatetype.f_estimatetype_ord=10
                   ) estimate_total on (estimate_total.f_projstep_id = projstep.f_projstep_id)
         left join (select estimate.f_estimate_value, estimate.f_projstep_id
                    from estimate join estimatetype on (estimate.f_estimatetype_id = estimatetype.f_estimatetype_id)
                    where estimatetype.f_estimatetype_ord=20
                   ) estimate_self on (estimate_self.f_projstep_id = projstep.f_projstep_id)
    where extract(year from projstep.f_projstep_end) >= %(start_year)s and extract(year from projstep.f_projstep_end) <= %(end_year)s
      """ + projects_filter_cond + u"""
      /* filter by budget */ %(filter_by_budget)s
    """) % query_inline_parameters
