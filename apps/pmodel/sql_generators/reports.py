# -*- coding: utf-8; -*-

def generate_reports_sql(rule, additional_constraints):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'divider': '1',
        }

    if ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = " sqrt(sum(1) over (partition by authorrep.f_report_id))"
    elif ['divide_coauthors'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = "sum(1) over (partition by authorrep.f_report_id)"

    return u"""select authorrep.f_man_id
         , authorrep.f_report_id AS f_object_id
         , 1/(%(divider)s) points
         , report.f_report_name || to_char(report.f_report_date, ' (yyyy)') as act_name
         , 'Тип' as act_paramname
         , 'Отчёт' as act_paramvalue
         , sum(1) over (partition by authorrep.f_report_id) act_nauthors
         , extract(year from report.f_report_date) as year
    from authorrep join report on (report.f_report_id = authorrep.f_report_id)
    where extract(year from report.f_report_date) >= %(start_year)s and extract(year from report.f_report_date) <= %(end_year)s
    """ % query_inline_parameters
