# -*- coding: utf-8; -*-

import random
from .util import get_bound_value, get_restrictions

#
# Для идентификации показателей цитирования используется f_man_id, так как цитирование связано с сотрудником.
# Однако, в случае наличия в формуле более одного параметра цитирования, срабатывает выбор лучшего вхождения
# одного объекта в несколько категорий. В результате пользователь получает только одно ненулевое
# значение. В качестве временной меры используется смещение f_man_id на небольшую величину, которая зависит
# от текущей категории, что гарантирует уникальность ключей. По умолчанию используется случайный сдвиг.
#
FMANID_MODIFIERS = {
    'citations': {'WOS': 0.11, 'Scopus': 0.12},
    'hirsh': {'WOS': 0.21, 'Scopus': 0.22},
    }

def generate_hirsh_sql(rule, additional_constraints, bibsource='WOS'):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'bibsource': bibsource,
        'f_man_id_modifier': random.random(),
        }
    query_inline_parameters['f_man_id_modifier'] = FMANID_MODIFIERS['hirsh'].get(bibsource, random.random())

    return u"""select h.f_man_id
         , h.f_man_id + %(f_man_id_modifier)s AS f_object_id
         , h.hirsh_index points
         , 'Индекс Хирша' as act_name
         , 'Тип' as act_paramname
         , h.f_bibsource_id as act_paramvalue
         , 1 act_nauthors
         , extract(year from sysdate) as year
    from v_citcount_hirsh h
    where h.f_bibsource_id = '%(bibsource)s'
    """ % query_inline_parameters

def generate_citations_sql(rule, additional_constraints, for_period=True):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'bibsource': 'WOS',
        'f_man_id_modifier': random.random(),
        }

    if rule['parameter'] in ('WOS', 'Scopus'):
        query_inline_parameters['bibsource'] = rule["parameter"]
        query_inline_parameters['f_man_id_modifier'] = FMANID_MODIFIERS['citations'].get(rule['parameter'], random.random())

    filtering_constraints = []
    for restriction in get_restrictions(rule):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        cond_expression = None
        if param == 'coauthors_count':
            if lower_bound != None:
                filtering_constraints.append(u"NVL(vac.nauthors, 1) >= %s" % lower_bound)
            if upper_bound != None:
                filtering_constraints.append(u"NVL(vac.nauthors, 1) <= %s" % upper_bound)
    filter_cond = ((u" AND (" + u" AND ".join(filtering_constraints) + u")") if filtering_constraints else "")

    return (u"""SELECT f_man_id
         , f_man_id + %(f_man_id_modifier)s AS f_object_id
         ,  points
         , 'Цитирование' as act_name
         , 'Тип' as act_paramname
         , '%(bibsource)s' as act_paramvalue
         , 1 act_nauthors
         , extract(year from sysdate) as year
    FROM (
        SELECT authora.f_man_id
          , sum(cc.f_citcount_val) points
        FROM v_citcount cc
             JOIN authora ON (authora.f_article_id = cc.f_article_id)
             JOIN article ON (article.f_article_id = authora.f_article_id)
             LEFT JOIN v_article_coefficient vac ON (vac.f_article_id = article.f_article_id)
        WHERE cc.f_citcount_type = '%(bibsource)s'
          """ + (u"AND article.f_article_year >= %(start_year)s and article.f_article_year <= %(end_year)s" if for_period else "") + u"""
          /* */ """ + filter_cond + u""" 
        GROUP BY authora.f_man_id
    )
    """) % query_inline_parameters
