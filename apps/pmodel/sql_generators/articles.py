# -*- coding: utf-8; -*-

from .util import get_bound_value, get_restrictions

MSU_ARTICLE_COAUTHORS_SQL = u"""SELECT f_article_id, count(distinct authora.f_man_id) nauthors
                  FROM authora
                  JOIN manspost ON (manspost.f_man_id = authora.f_man_id)
                  JOIN department ON (department.f_department_id = manspost.f_department_id)
                  WHERE department.f_organization_id = 214524 /* MSU */
                  GROUP BY f_article_id"""

def get_article_typename_by_rule(rule):
    typename = u'Статья'
    if rule['category'] == 'articles.journals.russian.hak':
        typename = u'ВАК'
    if rule['category'] == 'articles.journals.international':
        typename = u'WoS/Scopus'
    if rule['category'] == 'articles.journals.top25':
        typename = u'Топ-25'
    if rule['category'] == 'articles.collections':
        typename = u'в сборнике'
    if rule['category'].startswith('articles.conference_materials'):
        typename = u'Тезисы'
    if rule['category']==('monographs.chapter'):
        typename = u'Глава'
    return typename

def generate_articles_sql(rule, additional_constraints, only_journals_article=True):
    WOS_IMPACTS = "'Impact Factor 2013', '5-Year Impact Factor 2013'"
    SCOPUS_IMPACTS = "'SJR-2012'"
    RINC_IMPACTS = "'RINC 2013', 'RINC'"
    IMPACTS = {
        'impact_factor.isi': WOS_IMPACTS,
        'impact_factor.scopus': SCOPUS_IMPACTS,
        'impact_factor.rsci': RINC_IMPACTS,
        }
    CATEGORY_TO_VAKLIST = {
        'articles.journals.russian.hak': u"'Список ВАК'",
        'articles.journals.international.Scopus': "'Scopus'",
        'articles.journals.international.WoS': """'Science Citation Index', 'Science Citation Index Expanded',
                              'Social Sciences Citation Index', 'JCR', 'Arts & Humanities Citation Index'"""
        }
    query_inline_parameters = {
        'what_to_sum': 'nvl(ifactor.value, 0)',
        'divider': '1',
        'impact_type': WOS_IMPACTS,
        'selected_journals': 'select f_journal_id from journal',
        'selected_journals_join_type': '' if only_journals_article else 'left',
        'org_coauthors_query': u'',
        'standings_type': "f_journalrang_type='NO_SUCH_TYPE'",
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'typename': get_article_typename_by_rule(rule),
        }
    ifactor_required = True

    if ['divide_coauthors'] in rule.get('modifiers', []) \
           or ['divide_org_coauthors'] in rule.get('modifiers', []) \
           or ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        if ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
            query_inline_parameters['divider'] = ' sqrt(nvl(vac.nauthors, 1))'
        elif ['divide_org_coauthors'] in rule.get('modifiers', []):
            query_inline_parameters['org_coauthors_query'] = u"""
            join (""" + MSU_ARTICLE_COAUTHORS_SQL + """) org_coauthors_count ON (org_coauthors_count.f_article_id = aa.f_article_id)
            """
            query_inline_parameters['divider'] = ' nvl(org_coauthors_count.nauthors, 1)'
        else:
            query_inline_parameters['divider'] = ' nvl(vac.nauthors, 1)'

    if rule['parameter'] in ['pages_count', 'redpages_count']:
        expr = u"nvl( nvl(isnumeric(a.f_article_lastpage), isnumeric(a.f_article_firstpage)) - isnumeric(a.f_article_firstpage), 0) + 1"
        if rule['parameter'] == 'redpages_count':
            expr = u"(" + expr + u") / 16"
        query_inline_parameters['what_to_sum'] = expr
    elif rule['parameter'] == 'impact_factor.isi':
        query_inline_parameters['impact_type'] = WOS_IMPACTS
    elif rule['parameter'] == 'impact_factor.scopus':
        query_inline_parameters['impact_type'] = SCOPUS_IMPACTS
    elif rule['parameter'] == 'impact_factor.rsci':
        query_inline_parameters['impact_type'] = "'RINC 2013', 'RINC'"
    elif (not rule['parameter'] or rule['parameter'] == 'count'):
        ifactor_required = False
        query_inline_parameters['what_to_sum'] = '1'
    elif rule['parameter'] == 'standings.isi':
        query_inline_parameters['what_to_sum'] = 'js.R'
        query_inline_parameters['standings_type'] = "(f_journalrang_type in (" + WOS_IMPACTS + """)
        and (f_journalrubtype_id = (select F_JOURNALRUBTYPE_ID from JOURNALRUBTYPE where F_JOURNALRUBTYPE_CODE='WOS'))) """
    elif rule['parameter'] == 'standings.scopus':
        query_inline_parameters['what_to_sum'] = 'js.R'
        query_inline_parameters['standings_type'] = """(f_journalrang_type in (%s)
        and (f_journalrubtype_id = (select F_JOURNALRUBTYPE_ID from JOURNALRUBTYPE where F_JOURNALRUBTYPE_CODE='Scopus'))) """ % SCOPUS_IMPACTS

    if rule['category'] in ['articles.journals.russian.hak', 'articles.journals.international.Scopus', 'articles.journals.international.WoS']:
        vak_list_name = CATEGORY_TO_VAKLIST[ rule['category'] ]
        for restriction in get_restrictions(rule):
            param = restriction['parameter']
            if param == 'russian_intjournal' and restriction.get('value', None) == True:
                if rule['category'] == 'articles.journals.international.Scopus':
                    vak_list_name = u"'Журналы РФ в Scopus'"
                elif rule['category'] == 'articles.journals.international.WoS':
                    vak_list_name = u"'Журналы РФ в WoS'"

        query_inline_parameters['selected_journals'] = """
        SELECT jornalvak.f_journal_id
        FROM jornalvak
        WHERE jornalvak.f_journalvaktype_id IN (SELECT f_journalvaktype_id FROM journalvaktype WHERE f_journalvaktype_name IN (%(vak_list_name)s))
        GROUP BY jornalvak.f_journal_id
        """ % {'vak_list_name': vak_list_name}
    elif rule['category'] == 'articles.journals.international':
        query_inline_parameters['selected_journals'] = """select f_journal_id
        /* from v_journals_info vji */
        /**** where vji.in_wos = 1 OR vji.in_scopus = 1 ***/
        from (
           (select jornalvak.f_journal_id
           from jornalvak
           where jornalvak.f_journalvaktype_id IN
                         (select f_journalvaktype_id from journalvaktype
                          where f_journalvaktype_name
                          IN ('Scopus', 'Science Citation Index', 'Science Citation Index Expanded',
                              'Social Sciences Citation Index', 'JCR', 'PubMed',
                              'Arts & Humanities Citation Index')
                         )
           )
           UNION ALL
           (select f_journal_id
            from journalrang
            where F_JOURNALRANG_TYPE in (%s))
        )
        group by f_journal_id""" % WOS_IMPACTS

    if rule['category'] == 'articles.journals.top25':
        query_inline_parameters['selected_journals'] = \
        """select tj.f_journal_id
        from v_wos_top25_journals tj """

    if rule['category'] == 'articles.journals.russian.misc':
        ## Title in Russian
        query_inline_parameters['selected_journals'] = \
        u"""select f_journal_id
        from journal
        where nvl(length(translate(nls_upper(f_journal_name),'~ABCDEFGHIJKLMNOPQRSTUVWXYZ .-;:/"(),[]=0123456789','~')),0)>=3
        """

    if rule['category'] == 'articles.journals.international.misc':
        ## Title NOT in Russian
        query_inline_parameters['selected_journals'] = \
        u"""select f_journal_id
        from journal
        where nvl(length(translate(nls_upper(f_journal_name),'~ABCDEFGHIJKLMNOPQRSTUVWXYZ .-;:/"(),[]=0123456789','~')),0) <= 3
        """
    if rule['category'] == 'articles.journals.misc':
        query_inline_parameters['selected_journals'] = \
        u"""select f_journal_id from journal """

    if rule['category'] == 'articles':
        pass

    if 'journalList' in rule:
        query_inline_parameters['selected_journals'] = \
        u"""select f_journal_id from journallistitem
        where f_journallist_id = %s
        """ % rule['journalList']

    filtering_constraints = []
    for restriction in get_restrictions(rule):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        cond_expression = None
        if param.startswith('impact_factor.'):
            impact_type = IMPACTS.get(param, "''")
            cond_expression = u"""a.f_journal_id in (
              select f_journal_id
               from journalrang
               where F_JOURNALRANG_TYPE in (%(impact_type)s)""" % {'impact_type': impact_type}
            if lower_bound != None:
                cond_expression += u" AND F_JOURNALRANG_VAL >= %s" % lower_bound
            if upper_bound != None:
                cond_expression += u" AND F_JOURNALRANG_VAL <= %s" % upper_bound
            cond_expression += u"""
            )
            """
        elif param.startswith('standings.'):
            if upper_bound != None or lower_bound != None:
                cond_expression = u"""a.f_journal_id in (
                    select f_journal_id
                    from v_journal_standings_all
                    where (%(standings_type)s)
                    group by f_journal_id
                    having (
                """
                if lower_bound != None:
                    cond_expression += u" (1-min(r) >= %s) " % lower_bound \
                                       + (u" and " if upper_bound else "")
                if upper_bound != None:
                    cond_expression += u" (1-min(r) <= %s) " % upper_bound
                cond_expression += u"""
                     )
                )"""
        elif param in ('coauthors_count', 'coauthors_count.hidden'):
            if lower_bound != None:
                filtering_constraints.append(u"vac.nauthors >= %s" % lower_bound)
            if upper_bound != None:
                filtering_constraints.append(u"vac.nauthors <= %s" % upper_bound)
        elif param == 'author_order':
            cond_expression = u"("
            if lower_bound != None:
                cond_expression += u" NVL(aa.f_authora_ord, 1) >= %s " % lower_bound \
                                   + (u" and " if upper_bound else "")
            if upper_bound != None:
                cond_expression += u" NVL(aa.f_authora_ord, 1) <= %s " % upper_bound
            cond_expression += u")"

        if cond_expression:
            filtering_constraints.append(cond_expression)

    ifactor_subquery = """
           left join (
               select f_journal_id, max(F_JOURNALRANG_VAL) value
               from journalrang
               where F_JOURNALRANG_TYPE in (%(impact_type)s)
               group by f_journal_id
             ) ifactor on (ifactor.f_journal_id = a.f_journal_id)"""

    description_field = u"(select formatted_reference from v_article_reference where v_article_reference.f_article_id=aa.f_article_id)"

    query = u"""SELECT
        aa.f_man_id,
        aa.f_article_id AS f_object_id,
        ((%(what_to_sum)s) / (%(divider)s)) points,
        """ + description_field + u""" act_name,
        'Тип' as act_paramname,
        '%(typename)s' as act_paramvalue,
        vac.nauthors as act_nauthors,
        a.f_article_year as year
    FROM authora aa
         join article a on (a.f_article_id = aa.f_article_id)
         left join v_article_coefficient vac on (vac.f_article_id = a.f_article_id)
         left join (
              select f_journal_id, 1-min(r) as R
              from v_journal_standings_all
              where (%(standings_type)s)
              group by f_journal_id
            ) js on (js.f_journal_id = a.f_journal_id)
         /* org_coauthors */ %(org_coauthors_query)s
         %(selected_journals_join_type)s join (
            %(selected_journals)s
         ) selected_journals on (selected_journals.f_journal_id = a.f_journal_id) """ + \
    (ifactor_subquery if ifactor_required else "") + u"""
    WHERE a.f_article_year >= %(start_year)s and a.f_article_year <= %(end_year)s
      """ + (u" AND ".join( [""] + filtering_constraints ) if filtering_constraints else "")

    return query % query_inline_parameters


def generate_collection_articles_sql(rule, additional_constraints, article_type=None):
    """ARTICLE_TYPE is a subset of ["COLLECTION", "CHAPTER"] or
    None. If None, then all collection articles are counted.
    """
    query_inline_parameters = {
        'what_to_sum': '1',
        'divider': '1',
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'typename': get_article_typename_by_rule(rule),
        'org_coauthors_query': '/* no subquery */',
        'filter_by_indexed': '',
        'filter_by_marks': '/* no filter by marks */',
        'filter_by_collection_types': '/* no filter by collection types */',
        }
    if ['divide_coauthors'] in rule.get('modifiers', []) \
           or ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        if ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
            query_inline_parameters['divider'] = ' sqrt(nvl(vac.nauthors, 1))'
        else:
            query_inline_parameters['divider'] = ' nvl(vac.nauthors, 1)'
    if ['divide_org_coauthors'] in rule.get('modifiers', []):
        query_inline_parameters['org_coauthors_query'] = u"""left join (""" + MSU_ARTICLE_COAUTHORS_SQL + """
        ) org_coauthors_count ON (org_coauthors_count.f_article_id = aa.f_article_id)
        """
        query_inline_parameters['divider'] = ' nvl(org_coauthors_count.nauthors, 1)'

    param = rule['parameter']
    if param in ('redpages_count', 'pages_count'):
        expr = u"nvl( nvl(isnumeric(a.f_article_lastpage), isnumeric(a.f_article_firstpage)) - isnumeric(a.f_article_firstpage), 0) + 1"
        if param == 'redpages_count':
            expr = u"(" + expr + u") / 16"
        query_inline_parameters['what_to_sum'] = expr

    description_field = u"(select formatted_reference from v_article_reference where v_article_reference.f_article_id=aa.f_article_id)"
    nauthors_field = u"(select nauthors from v_article_coefficient vac where vac.f_article_id = aa.f_article_id)"

    if article_type == 'CHAPTER':
        query_inline_parameters['filter_by_collection_types'] = u''' AND (
        c.f_collection_id IN (select f_collection_id
                              from collectiongroup
                                 join collectiongrouptype on (collectiongrouptype.f_collectiongrouptype_id = collectiongroup.f_collectiongrouptype_id)
                              where f_collectiongrouptype_code='collective_monograph'))'''

    for seq, restriction in enumerate(get_restrictions(rule)):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        bool_value = restriction.get('value', None)
        if param in ["indexed_by_wos_or_scopus", "indexed_by_wos", "indexed_by_scopus"]:
            bibsource_id = {
            "indexed_by_wos_or_scopus": "'WOS', 'Scopus'",
            "indexed_by_wos": "'WOS'",
            "indexed_by_scopus": "'Scopus'"
            }
            query_inline_parameters['filter_by_indexed'] = """ AND (
            aa.f_article_id IN (SELECT f_activity_id FROM externalid WHERE f_bibsource_id IN (%s))
            )""" % bibsource_id[param]
        if param == 'confirmed_only' and (bool_value == True):
            # Сборник был подтвержден
            #--- TODO: учитывать подразделение, для которого этот сборник считается хорошим
            query_inline_parameters['filter_by_marks'] = """and
            c.f_collection_id IN (SELECT collectionmark.f_collection_id FROM collectionmark WHERE collectionmark.f_collectionmark_type = 'selected')
            """

    query = u"""SELECT
        aa.f_man_id,
        aa.f_article_id AS f_object_id,        
        ((%(what_to_sum)s) / (%(divider)s)) points,
        """ + description_field + u""" act_name,
        'Тип' as act_paramname,
        '%(typename)s' as act_paramvalue,
        vac.nauthors as act_nauthors,
        a.f_article_year as year
    FROM authora aa
         join article a on (a.f_article_id = aa.f_article_id)
         join collection c on (a.f_collection_id = c.f_collection_id)
         join v_article_coefficient vac on (vac.f_article_id = a.f_article_id)
         %(org_coauthors_query)s
    WHERE a.f_article_year >= %(start_year)s and a.f_article_year <= %(end_year)s
      and nvl(c.f_collection_tesis, 0) = 0
      /* filter by indexed papers */ %(filter_by_indexed)s
      /* filter by marks */ %(filter_by_marks)s
      %(filter_by_collection_types)s
    """

    return query % query_inline_parameters


def generate_collection_thesis_sql(rule, additional_constraints, include_russian=True, include_non_russian=True):
    query_inline_parameters = {
        'what_to_sum': '1',
        'divider': '1',
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'typename': get_article_typename_by_rule(rule),
        }
    if ['divide_coauthors'] in rule.get('modifiers', []) \
           or ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        if ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
            query_inline_parameters['divider'] = ' sqrt(nvl(vac.nauthors, 1))'
        else:
            query_inline_parameters['divider'] = ' nvl(vac.nauthors, 1)'

    description_field = u"(select formatted_reference from v_article_reference where v_article_reference.f_article_id=aa.f_article_id)"
    nauthors_field = u"(select nauthors from v_article_coefficient vac where vac.f_article_id = aa.f_article_id)"
    name_filter = None
    if include_russian and not include_non_russian:
        # Few non-Russian caracters
        name_filter = u"nvl(length(translate(nls_upper(a.f_article_name),'~АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ .-;:/''\"(),[]=0123456789','~')),0)<=3"
    if include_non_russian and not include_russian:
        # Many non-Russain caracters
        name_filter = u"""(
        nvl(length(translate(nls_upper(a.f_article_name),'~АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ .-;:/''\"(),[]=0123456789','~')),0)>=3
        OR
        REGEXP_LIKE(lower(c.f_collection_name), '(межд[.])|(международ)')
        )"""

    query = u"""SELECT
        aa.f_man_id,
        aa.f_article_id AS f_object_id,
        ((%(what_to_sum)s) / (%(divider)s)) points,
        """ + description_field + u""" act_name,
        'Тип' as act_paramname,
        '%(typename)s' as act_paramvalue,
        vac.nauthors as act_nauthors,
        a.f_article_year as year
    FROM authora aa
         join article a on (a.f_article_id = aa.f_article_id)
         join collection c on (a.f_collection_id = c.f_collection_id)
         join v_article_coefficient vac on (vac.f_article_id = a.f_article_id)
    WHERE a.f_article_year >= %(start_year)s and a.f_article_year <= %(end_year)s
      AND nvl(c.f_collection_tesis, 0) = 1
      /* filter by name */""" + ((" AND " + name_filter) if name_filter else '')
        
    return query % query_inline_parameters
