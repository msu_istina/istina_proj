# -*- coding: utf-8; -*-

from .util import get_bound_value, get_restrictions

def generate_diplom_guidance_sql(rule, additional_constraints):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'divider': 1,
        'filter_by_diplom_type': '',
        }

    if ['divide_coauthors'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'nvl(sum(1) over (partition by diplomhead.f_diplom_id), 1)'
    elif ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'sqrt(nvl(sum(1) over (partition by diplomhead.f_diplom_id), 1))'

    DIPLOM_TYPE_CODES = {
        'diploma_specialist': 'SPEC',
        'diploma_magister': 'MAG',
        'diploma_bacalaur': 'BAC'
        }

    for seq, restriction in enumerate(get_restrictions(rule)):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        bool_value = restriction.get('value', None)
        if param in ['diploma_specialist', 'diploma_magister', 'diploma_bacalaur']:
            query_inline_parameters['filter_by_diplom_type'] = u""" AND (
            diplomtype.f_diplomtype_code %s '%s'
            )
            """ % (("=" if bool_value else "<>"), DIPLOM_TYPE_CODES[param])

    return u"""select diplomhead.f_man_id
         , diplomhead.f_diplom_id AS f_object_id
         , 1/(%(divider)s) points
         , diplom.f_diplom_author || ' ' || diplom.f_diplom_name || ' (' || to_char(diplom.f_diplom_year) || ')' as act_name
         , 'Тип' as act_paramname
         , diplomtype.f_diplomtype_name as act_paramvalue
         , NULL act_nauthors
         , diplom.f_diplom_year as year
    from diplomhead
         join diplom on (diplomhead.f_diplom_id = diplom.f_diplom_id)
         join diplomtype on (diplomtype.f_diplomtype_id = diplom.f_diplomtype_id)
    where diplom.f_diplom_year >= %(start_year)s and diplom.f_diplom_year <= %(end_year)s
       /* filter by type */ %(filter_by_diplom_type)s
    """ % query_inline_parameters


def generate_coursework_guidance_sql(rule, additional_constraints):
    return u"""SELECT 0 AS f_man_id
         , 0 AS f_object_id
         , 0 AS points
         , ' ' AS act_name
         , 'Тип' as act_paramname
         , ' ' AS act_paramvalue
         , NULL act_nauthors
         , 2010 as year
    FROM dual
    WHERE 1=0
    """


#--- FIXME(serg): count only dissertations of organization's students???
def generate_dissers_guidance_sql(rule, additional_constraints, doctor=None):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'doctor': 1 if doctor else 0,
        'divider': '1',
        }

    if ['divide_coauthors'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'nvl(sum(1) over (partition by diserhead.f_disser_id), 1)'
    elif ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'sqrt(nvl(sum(1) over (partition by diserhead.f_disser_id), 1))'

    return u"""select diserhead.f_man_id
         , diserhead.f_disser_id AS f_object_id
         , 1/(%(divider)s) points
         , disser.f_disser_authorname || ' ' || disser.f_disser_name as act_name
         , 'Тип' as act_paramname
         , decode(disser.f_diser_prior, 'Y', 'Кандидатская', 'Докторская') as act_paramvalue
         , NULL act_nauthors
         , disser.f_disser_year as year
    from diserhead join disser on (diserhead.f_disser_id = disser.f_disser_id)
    where disser.f_disser_year >= %(start_year)s and disser.f_disser_year <= %(end_year)s
    and decode(disser.f_diser_prior, 'Y', 0, 1) = %(doctor)s
    and disser.f_disser_status = 'defended'
    """ % query_inline_parameters
