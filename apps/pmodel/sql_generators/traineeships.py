# -*- coding: utf-8; -*-

def generate_traineeship_sql(rule, additional_constraints):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'what_to_count': '1',
        }

    if rule['parameter'] == 'duration':
        query_inline_parameters['what_to_count'] = 'trunc(traineeship.f_traineeship_end - traineeship.f_traineeship_begin)'

    return u"""select traineeship.f_man_id
         , traineeship.f_traineeship_id AS f_object_id
         , %(what_to_count)s points
         , nvl(traineeship.f_traineeship_organ, 'ОРГАНИЗАЦИЯ НЕ УКАЗАНА') || ' (' || to_char(traineeship.f_traineeship_begin, 'dd.mm.yyyy') || '-' || to_char(traineeship.f_traineeship_end, 'dd.mm.yyyy') || ')' as act_name
         , 'Срок' as act_paramname
         , to_char(trunc(traineeship.f_traineeship_end - traineeship.f_traineeship_begin)) as act_paramvalue
         , NULL act_nauthors
         , extract(year from traineeship.f_traineeship_end) as year
    from traineeship
    where extract(year from traineeship.f_traineeship_end) >= %(start_year)s and extract(year from traineeship.f_traineeship_end) <= %(end_year)s
    """ % query_inline_parameters
