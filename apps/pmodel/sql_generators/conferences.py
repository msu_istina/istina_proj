# -*- coding: utf-8; -*-

from .util import get_bound_value, get_restrictions

def generate_conferences_sql(rule, additional_constraints, only_classes=None):
    presentation_types = {
        'conf_talk_poster': 'poster',
        'conf_talk_oral': 'oral',
        'conf_talk_plenar': 'plenar',
        'conf_talk_invited': 'invited',
        }
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'filter_by_scope': '',
        'filter_by_talk_kind': '',
        'filter_by_author_kind': '',
        'what_to_sum': '1',
        'divider': '1',
        }
    if only_classes and "international" in only_classes:
        query_inline_parameters['filter_by_scope'] = 'and t_confs.f_confscope_id = 1'

    if only_classes and "russian" in only_classes:
        query_inline_parameters['filter_by_scope'] = 'and t_confs.f_confscope_id <> 1'

    if ['divide_coauthors'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = "sum(1) over (partition by authord.f_presentation_id)"
    elif ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'sqrt(sum(1) over (partition by authord.f_presentation_id))'

    for restriction in get_restrictions(rule):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        bool_value = restriction.get('value', None)
        if param == 'international':
            desired_value = 1 if restriction.get('value', None) == True else 0
            query_inline_parameters['filter_by_scope'] = "and decode(t_confs.f_confscope_id, 1, 1, 0) = %d" % desired_value
        elif param in ['conf_talk_poster', 'conf_talk_oral', 'conf_talk_plenar', 'conf_talk_invited']:
            query_inline_parameters['filter_by_talk_kind'] += u""" AND f_presentation_kind %(op)s '%(type)s'""" % {
                'type': presentation_types[param],
                'op': "=" if restriction.get('value', None) == True else "<>"
                }
        elif param == 'conf_speaker':
            query_inline_parameters['filter_by_author_kind'] = u"AND (authord.f_authord_kind %s 'speaker')" % (
                " = " if bool_value else " <> ",
                )
            
    return u"""
    select authord.f_man_id
         , authord.f_presentation_id AS f_object_id
         , ((%(what_to_sum)s) / decode((%(divider)s), 0, 1, (%(divider)s)) ) points
         , presentation.f_presentation_name || ' (' || t_confs.name || ')' as act_name
         , 'Докладчик' as act_paramname
         , '' as act_paramvalue
         , count(authord.f_man_id) over (partition by authord.f_presentation_id) act_nauthors
         , extract(year from presentation.f_presentation_date) as year
    from authord
         join presentation on (presentation.f_presentation_id = authord.f_presentation_id)
         join t_confs on (t_confs.conf_id = presentation.conf_id)
    where t_confs.year >= %(start_year)s and t_confs.year <= %(end_year)s
      /* filters */ %(filter_by_scope)s %(filter_by_talk_kind)s %(filter_by_author_kind)s
    """ % query_inline_parameters
