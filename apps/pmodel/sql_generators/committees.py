# -*- coding: utf-8; -*-

from .util import get_bound_value, get_restrictions

def generate_committee_sql(rule, additional_constraints):
    committee_types = {
        'conf_pc_member': 1, 'conf_pc_head': 2, 'conf_org_member': 3, 'conf_org_head': 4
        }
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'what_to_count': '1',
        'filter_by_scope': '',
        'filter_by_committee': '',
        }

    if rule['parameter'] == 'international':
        query_inline_parameters['filter_by_scope'] = "and decode(conf.f_confscope_id, 1, 1, 0) = 1"

    for restriction in get_restrictions(rule):
        param = restriction['parameter']
        lower_bound = get_bound_value( restriction.get('lower_bound', None) )
        upper_bound = get_bound_value( restriction.get('upper_bound', None) )
        if param == 'international':
            desired_value = 1 if restriction.get('value', None) == True else 0
            query_inline_parameters['filter_by_scope'] = "and decode(conf.f_confscope_id, 1, 1, 0) = %d" % desired_value
        if param in ['conf_pc_member', 'conf_pc_head', 'conf_org_member', 'conf_org_head']:
            query_inline_parameters['filter_by_committee'] += u""" AND f_committee_type %(op)s %(type)d""" % {
                'type': committee_types[param],
                'op': "=" if restriction.get('value', None) == True else "<>"
                }

    return u"""select committee.f_man_id
         , committee.f_committee_id AS f_object_id
         , 1 points
         , conf.name || ' (' || conf.year || ')' as act_name
         , 'Тип' as act_paramname
         , decode(conf.f_confscope_id, 1, 'Международная', 2, 'Всероссийская', 3, 'Всероссийская', 'Другое') as act_paramvalue
         , NULL act_nauthors
         , conf.year as year
    from committee
         join t_confs conf on (conf.conf_id = committee.conf_id)
    where conf.year >= %(start_year)s and conf.year <= %(end_year)s
    /* filters */ %(filter_by_scope)s %(filter_by_committee)s
    """ % query_inline_parameters
