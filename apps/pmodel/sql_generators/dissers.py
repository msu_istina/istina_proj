# -*- coding: utf-8; -*-

def generate_dissers_sql(rule, additional_constraints, doctor=None):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'doctor': 1 if doctor else 0
        }

    return u"""select disser.f_man_id
         , disser.f_disser_id AS f_object_id
         , 1 points
         , disser.f_disser_name as act_name
         , 'Тип' as act_paramname
         , decode(disser.f_diser_prior, 'Y', 'Кандидатская', 'Докторская') as act_paramvalue
         , count(disser.f_man_id) over (partition by disser.f_disser_id) act_nauthors
         , disser.f_disser_year as year
    from disser
    where disser.f_disser_year >= %(start_year)s and disser.f_disser_year <= %(end_year)s
    and decode(disser.f_diser_prior, 'Y', 0, 1) = %(doctor)s
    and disser.f_disser_status = 'defended'
    """ % query_inline_parameters
