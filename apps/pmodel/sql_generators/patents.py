# -*- coding: utf-8; -*-

def generate_patents_sql(rule, additional_constraints):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'divider': '1',
        }

    if ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'sqrt(nvl(sum(1) over (partition by ap.f_patent_id), 1))'
    elif ['divide_coauthors'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'nvl(sum(1) over (partition by ap.f_patent_id), 1)'

    return u"""select ap.f_man_id
         , ap.f_patent_id AS f_object_id
         , 1/(%(divider)s) points
         , patent.f_patent_name || to_char(patent.f_patent_date, ' (dd.mm.yyyy)') as act_name
         , 'Тип' as act_paramname
         , 'Патент' as act_paramvalue
         , sum(1) over (partition by ap.f_patent_id) act_nauthors
         , extract(year from patent.f_patent_date) as year
    from authorp ap
         join patent on (patent.f_patent_id = ap.f_patent_id)
    where extract(year from patent.f_patent_date) >= %(start_year)s and extract(year from patent.f_patent_date) <= %(end_year)s
      and f_organization_id IS NOT NULL
    """ % query_inline_parameters

def generate_softreg_sql(rule, additional_constraints):
    query_inline_parameters = {
        'start_year': additional_constraints.get('start_year', 0),
        'end_year': additional_constraints.get('end_year', 3000),
        'divider': '1',
        }

    if ['divide_coauthors_sqrt'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'sqrt(nvl(sum(1) over (partition by ap.f_patprogram_id), 1))'
    elif ['divide_coauthors'] in rule.get('modifiers', []):
        query_inline_parameters['divider'] = 'nvl(sum(1) over (partition by ap.f_patprogram_id), 1)'

    return u"""select ap.f_man_id
         , ap.f_patprogram_id AS f_object_id
         , 1/(%(divider)s) points
         , patprogram.f_patprogram_name as act_name
         , 'Тип' as act_paramname
         , 'Регистрация ПО' as act_paramvalue
         , sum(1) over (partition by ap.f_patprogram_id) act_nauthors
         , extract(year from patprogram.f_patprogram_date) as year
    from authorpog ap
         join patprogram on (patprogram.f_patprogram_id = ap.f_patprogram_id)
    where extract(year from patprogram.f_patprogram_date) >= %(start_year)s and extract(year from patprogram.f_patprogram_date) <= %(end_year)s
      and f_organization_id IS NOT NULL
    """ % query_inline_parameters
