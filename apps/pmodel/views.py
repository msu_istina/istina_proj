# -*- coding: utf-8 -*-
# Author: Dmitry Shachnev <mitya57@gmail.com>, 2014.

from json import dumps, loads
from django.http import HttpResponse, HttpResponseForbidden
from django.contrib import messages
#from django.http.response import Http404
from .ruleprocessor import process_rules, compute_averages, process_formula_for_personal_report
from .models import PointsFormula, PointsFormulaDepartments, CustomCategory, JournalList, JournalListItem, \
 EducForm, EducType, EducStage, RatingCache
from .forms import PublishFormulaForm, JournalSelectForm
from organizations.models import Department, NestedParentDepartment, Organization
from workers.models import Position, Worker, Employment
import datetime
import time
from django.db.models import Q

from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.contrib.auth.decorators import user_passes_test, login_required

from common.utils.csv_utils import export_to_csv_response
from common.utils.excel import export_to_excel_response
from common.utils.latex import latex_to_pdf

from unified_permissions import has_permission
from unified_permissions.models import AllGrantedPermissions
from organizations.utils import is_representative_for_user, is_representative_for_worker

FORMULA_VIEWERS = ['obogomol', 'mitya57']
EXTSTUDYWORK_VIEWERS = {
    'alazarev': 276498,           # ФФМ
    'Apolozova': 276235,          # ФГП
    'chiton': 275220,             # биологический
    'kandidate2002': 276287,      # журналистики
    'KaravaevVA': 276546,         # физический
    'Melnikova_YV': 275774,       # ИСАА 2
    'mb': 276167,                 # биоинженерии и биоинформатики
    'mitya57': 277422,            # НИИ механики
    'M_Zhuravel': 275774,         # ИСАА 1
    'neremin': 275572,            # геологический
    'NorinaEV': 276929,           # юридический
    'obogomol': 9403306,          # ректорат
    'Orleanskyn': 276379,         # политологии
    'stepanov7590': 276398,       # почвоведения
    'TatianaCherkasova': 275774,  # ИСАА 3
    'Timofeev': 275459,           # географический
    'Valentina': 276443,          # психологии
    'Veronika_Robustova': 276328, # ФИЯР
    'VMukhanova': 276715,         # филологический
    'STANISLAV_BUSHEV': 276776,   # философский
    'baraboshkina@mail.ru': 275774,  # ИСАА 4
    'ZapletinaEV': 275889,        # мехмат
}

def permissions_check(user, department=None):
    if not user or not user.is_authenticated():
        return False
    return user.is_superuser \
           or has_permission(user, 'is_representative_for', department) \
           or has_permission(user, 'can_view_ratings', department) \
           or (user.username in EXTSTUDYWORK_VIEWERS and (not department or EXTSTUDYWORK_VIEWERS[user.username] == department.id)) \
           or AllGrantedPermissions.objects.current.filter(Q(role_name='is_representative_for') | Q(role_name='can_view_ratings'),
                                                           user=user)


def show_actuality_date(request):
    try:
        from django.db import connections
        cursor = connections['reports_server'].cursor()
        cursor.execute("""SELECT f_sysparam_val, ceil((sysdate-to_date(f_sysparam_val, 'dd.mm.yyyy hh24:mi:ss'))*(60*24))
        FROM sysparam
        WHERE f_sysparam_id = 1""")
        data_info = cursor.fetchone()
        def get_minutes_name(n):
            if 11 <= n <= 19:
                return u'минут'
            if (n % 10) in [1]:
                return u'минута'
            if (n % 10) in [2, 3, 4]:
                return u'минуты'
            return u'минут'
        if data_info[0]:
            messages.info(request, u"Используются данные по состоянию на <b>%s</b> (%s %s назад)." % (data_info[0], data_info[1], get_minutes_name(data_info[1])))
    except Exception as e:
        print e


def is_applicable_post(emp):
    "Проверяет, что для указанной должности нужно выводит сумму баллов и вычислять рейтинг."
    # Only scientists
    try:
        if emp and emp.position.category.id in [1]:
            return True
    except AttributeError:
        pass
    return None

@login_required
def index(request):
    if not permissions_check(request.user):
        return redirect("pmodel.formulas_for_requester")

    formulas = []

    for obj in PointsFormula.objects.filter(createdby=request.user):
        formulas.append(obj)
    if request.user.is_superuser:
        for obj in PointsFormula.objects.filter(~Q(createdby=request.user)):
            formulas.append(obj)
    else:
        granted_departments = AllGrantedPermissions.objects.current.filter(Q(role_name='is_representative_for') | Q(role_name='can_view_ratings'), user=request.user).values('department')
        user_departments = NestedParentDepartment.objects.filter(department__in=granted_departments).values('parent')
        published_formulas = PointsFormulaDepartments.objects.filter(department__in=user_departments).values('formula')
        published_by_others = PointsFormula.objects.filter(pk__in=published_formulas).exclude(createdby=request.user)
        for obj in published_by_others:
            formulas.append(obj)

        subordinate_representatives = AllGrantedPermissions.objects.current \
                                      .filter(Q(role_name='is_representative_for') | Q(role_name='can_view_ratings'),
                                              exact_department_grant=1,
                                              department__in=granted_departments).values('user')
        created_by_dependants = PointsFormula.objects.filter(createdby__in=subordinate_representatives) \
                                .exclude(createdby=request.user).exclude(id__in=published_by_others)
        print created_by_dependants.query
        for obj in created_by_dependants:
            formulas.append(obj)

    return render(request, "pmodel/index.html", {'formulas': formulas})

def getdata(request, formulaid):
    '''Returns JSON value of the requested formula.'''
    formula = get_object_or_404(PointsFormula, pk=formulaid)
    response = dumps({})
    if formula.is_published() \
           or (request.user.username in FORMULA_VIEWERS) \
           or (request.user and request.user == formula.createdby) \
           or (request.user and request.user.is_superuser):
        response = dumps({
            'title': formula.title,
            'startyear': formula.begin,
            'endyear': formula.end,
            'jsondata': formula.jsondata
        })
    return HttpResponse(response, content_type='application/json')

@user_passes_test(lambda u: permissions_check(u))
def addformula(request):
    title = request.POST.get('title', '')
    if not title:
        messages.error(request, u'Вы не ввели название формулы.')
        return redirect('pmodel.index')
    formula = PointsFormula.objects.create(jsondata='[]',
                                           title=title,
                                           createdby=request.user)
    formula.save()
    return redirect("pmodel.editformula", formula.id)

@user_passes_test(lambda u: permissions_check(u))
def deleteformula(request, formulaid):
    formula = get_object_or_404(PointsFormula, pk=formulaid)
    if formula.createdby == request.user:
        title = formula.title
        formula.delete()
        messages.success(request, u'Формула %s удалена.' % title)
    else:
        messages.error(request, u'Удалить формулу может только пользователь, создавший её.')
    return redirect("/pmodel/")

@user_passes_test(lambda u: permissions_check(u))
def savedata(request, formulaid):
    formula = get_object_or_404(PointsFormula, pk=formulaid)
    if formula.createdby != request.user:
        return HttpResponseForbidden("Сохранять формулу может только пользователь, "
            "создавший её. Создайте обращение при помощи формы обратной связи, если "
            "Вы хотите изменить авторство данной формулы.", content_type="text/plain")
    formula.title = request.POST['title']
    formula.jsondata = request.POST['rowsData']
    currentyear = time.localtime().tm_year
    formula.begin = request.POST['startYear'] or currentyear - 4
    formula.end = request.POST['endYear'] or currentyear
    formula.save()
    response = '{"saved": [%s]}' % formulaid
    return HttpResponse(response, content_type='application/json')


@user_passes_test(lambda u: permissions_check(u))
def editformula(request, formulaid):
    formula = get_object_or_404(PointsFormula, pk=formulaid)
    if not (formula.createdby == request.user or request.user.is_superuser):
        return redirect("pmodel.viewformula", formulaid)
    can_view_extstudywork = (request.user.username in EXTSTUDYWORK_VIEWERS)
    return render(request, "pmodel/formulaedit.html",
                  {'formula': formula,
                   'show_extstudywork': can_view_extstudywork})

def viewformula(request, formulaid):
    formula = get_object_or_404(PointsFormula, pk=formulaid)
    if formula.published_for.exists() \
           or (request.user.username in FORMULA_VIEWERS) \
           or (request.user and formula.createdby == request.user) \
           or (request.user and request.user.is_superuser):
        can_view_extstudywork = (request.user and request.user.is_authenticated() and
            request.user.username in EXTSTUDYWORK_VIEWERS)
        return render(request, "pmodel/formulaedit.html",
                      {'formula': formula,
                       'show_extstudywork': can_view_extstudywork})
    return redirect("startpage")


@user_passes_test(lambda u: permissions_check(u))
def submitdata(request, hierarchical=False):
    if request.method == 'GET':
        response = {}
        return HttpResponse(response, content_type='application/json')
    rows_data = loads(request.POST['rowsData'])
    start_year = int(request.POST['startYear'])
    end_year = int(request.POST['endYear'])
    positions = loads(request.POST['positions'])
    histogram, headers, tooltips = process_rules(rows_data,
                                              user=request.user,
                                              distribution_only=True,
                                              start_year=start_year,
                                              end_year=end_year,
                                              positions=positions)
    if not hierarchical:
        response = dumps(histogram)
        return HttpResponse(response, content_type='application/json')
    # Hierarchical histogram was requested
    def chunks(l, n):
        """ Yield successive n-sized chunks from l. """
        for i in xrange(0, len(l), n):
            yield l[i:i+n]

    nbins = 12
    second_level = []
    for block in chunks(histogram, nbins):
        block_min = block[0][0]
        leaves = [{"name": str(k), "size": v} for k, v in block]
        second_level.append(
            {"name": str(block_min),
             "children": leaves
             })
    result = {"name": "histogram",
              "children": second_level}
    response = dumps(result)
    return HttpResponse(response, content_type='application/json')


@login_required
def runformula(request, formulaid, workerid=None):
    formula = get_object_or_404(PointsFormula, pk=formulaid)
    mode = "html"
    if request.method == "POST" and "to_csv" in request.POST:
        mode = "csv"
    elif request.method == "POST" and "to_xls" in request.POST:
        mode = "xls"
    # check permissions
    published_for_departments_exact = formula.published_for.values('department')
    published_for_departments=NestedParentDepartment.objects.filter(parent__in=published_for_departments_exact).values('department')
    is_formula_admin = (request.user.is_superuser \
                        or formula.createdby == request.user \
                        or request.user.granted_department_roles.filter(Q(role_name='can_view_ratings') | Q(role_name='is_representative_for'),
                                                                        department__in=published_for_departments).exists())

    start_year = formula.begin or 2010
    end_year = formula.end or 2014
    if request.method == 'POST':
        start_year = request.POST.get('start_year', start_year)
        end_year = request.POST.get('end_year', end_year)

    if not (permissions_check(request.user) or is_formula_admin):
        worker = request.user.get_profile().worker
        depid = None
        try:
            worker_department = worker.current_employments.values_list('department')[0]
            depid = worker_department.id
        except Exception:
            depid = None
        if depid:
            return redirect("pmodel.personal_report_for_department", formulaid, workerid or worker.id, depid)
        else:
            return redirect("pmodel.personal_report", formulaid, workerid or worker.id, start_year, end_year)

    deps = ['-1']
    if request.method == 'POST':
        deps = request.POST.getlist('dep', deps)
    show_personal_data = not is_formula_admin
    rules = loads(formula.jsondata)
    if deps:
        deps = map(int, deps)
    data, headers, categories, column_headers = None, None, None, None

    if request.method == 'POST' and 'show_average' in request.POST:
        average_url = "/pmodel/run/average/%(formula_id)s/%(department_id)s/?start_year=%(start_year)s&end_year=%(end_year)s" % {
            'formula_id': formula.id,
            'department_id': deps[0],
            'start_year': start_year,
            'end_year': end_year
            }
        return redirect(average_url)

    if deps != [-1]:
        include_parttimers = False
        include_fulltimers = True
        if request.method == 'POST' and 'parttimers' in request.POST:
            if request.POST.get('parttimers', '') == 'include_parttimers':
                include_parttimers = True
            elif request.POST.get('parttimers', '') == 'only_parttimers':
                include_parttimers = True
                include_fulltimers = False

        data, headers, categories = process_rules(rules,
                                                  user=request.user,
                                                  start_year=start_year,
                                                  end_year=end_year,
                                                  department_ids=deps,
                                                  include_parttimers=include_parttimers,
                                                  include_fulltimers=include_fulltimers
                                                  )
        column_headers = [{"name": p[0], "tooltip": p[1]} for p in zip(headers, categories)]

        if mode == "csv":
            return export_to_csv_response(headers, data, "formula_%s_%s_%s.csv" % (formula.id, deps[0], datetime.date.today()), bomb=True)
        elif mode == "xls":
            return export_to_excel_response([headers] + data, "formula results", "formula_%s_%s_%s.xls" % (formula.id, deps[0], datetime.date.today()))

    years = [1900, 2010, 2011, 2012, 2013, 2014, 2015]
    departments = None
    # Find all accessable root departments
    granted_permitions = AllGrantedPermissions.objects.current.filter(Q(role_name='is_representative_for') | Q(role_name='can_view_ratings'),
                                                                      user=request.user)
    all_departments = granted_permitions.values('department')
    all_children = granted_permitions.filter(department__children_relations__child__isnull=False).values('department__children_relations__child')
    root_departments = Department.objects.filter(Q(pk__in=all_departments)).exclude(pk__in=all_children)
    if root_departments.count() > 2:
        departments = Department.objects.filter(Q(pk__in=root_departments))
    else:
        departments = Department.objects.filter(Q(pk__in=root_departments) | Q(parent_relations__parent__in=root_departments))

    show_actuality_date(request)
    return render(request, "pmodel/run_formula.html",
                  {'data': data,
                   'headers': headers,
                   'column_headers': column_headers,
                   'formula': formula,
                   'years': years,
                   'start_year': int(start_year),
                   'end_year': int(end_year),
                   'departments': departments,
                   'default_depid': deps[0] if deps else None,
                   'show_results_table': True if headers else False,
                   })


@login_required
def runformula_average(request, formulaid, depid=None):
    formula = get_object_or_404(PointsFormula, pk=formulaid)
    dep = get_object_or_404(Department, pk=depid)
    # check permissions
    is_formula_admin = permissions_check(request.user, department=dep) or formula.createdby == request.user
    if not (formula.is_published() or is_formula_admin): # do not check that user matches formula: formula.matches(request.user)
        messages.error(request, u"У Вас нет прав для выполнения расчета по данной формуле.")
        return redirect("/pmodel/")

    start_year = formula.begin or 2010
    end_year = formula.end or 2014
    if request.method == 'POST':
        start_year = int(request.POST.get('start_year', start_year))
        end_year = int(request.POST.get('end_year', end_year))
    start_year = int(request.GET.get('start_year', start_year))
    end_year = int(request.GET.get('end_year', end_year))
    #
    show_personal_data = not is_formula_admin
    rules = loads(formula.jsondata)
    averages = compute_averages(rules,
                                user=request.user,
                                start_year=start_year,
                                end_year=end_year,
                                depid=depid,
                                )
    show_actuality_date(request)
    return render(request, "pmodel/averages.html",
                  {'formula': formula,
                   'start_year': start_year,
                   'end_year': end_year,
                   'averages': averages,
                   'department': dep,
                   })


def find_default_formula(user, return_all=False):
    """
    Returns formula published for user's department (non-part time).
    """
    worker = user.get_profile().worker
    emp = worker.current_main_employments
    dep = emp[0].department if emp else None
    # check that requester is a scientist
    #if not is_applicable_post(emp[0]):
    #    return None
    top_department = dep.roots[0] if dep and dep.roots else None

    user_departments = NestedParentDepartment.objects.filter(department=dep).values('parent')
    published_formulas = PointsFormulaDepartments.objects.filter(department__in=user_departments, for_admins=0).order_by('formula')
    if return_all:
        published_formulas.values('formula')
    best_formula, best_formula_level = None, None
    user_departments_ids = [v['parent'] for v in user_departments]
    for pf in published_formulas:
        pf_level = user_departments_ids.index(pf.department.id)
        if (not best_formula
            or ( # more specific formula published by top_department's admin
                pf_level > best_formula_level
                and
                has_permission(pf.createdby, "is_representative_for", top_department)
                )
            # formula published for top_department, no matter why
            or pf.department == top_department):
            best_formula = pf.formula
            best_formula_level = pf_level
    return best_formula

def compute_total_points_for_user(user, formula=None):
    "Evaluate formula for user. If formula is None, then evaluates default formula for the user, if any."
    formula = formula or find_default_formula(user)
    if not formula:
        return None
    worker = user.get_profile().worker

    cache_items = RatingCache.objects.filter(formula=formula, worker=worker,
                                             from_year=formula.begin,
                                             to_year=formula.end)
    if cache_items.exists():
        return cache_items[0].value
    else:
        return False

@login_required
def default_personal_report(request, workerid=None):
    formula = find_default_formula(request.user)
    worker = None
    if workerid:
        worker = get_object_or_404(Worker, pk=workerid)
        formula = find_default_formula(worker.user)
    else:
        worker = request.user.get_profile().worker
    if not formula:
        messages.warning(request, u"Формула расчёта персонального рейтинга не определена.")
        return redirect("home")
    start_year = end_year = None
    try:
        start_year = int(request.GET.get('start', None))
        end_year = int(request.GET.get('end', None))
    except (ValueError, TypeError):
        start_year = end_year = None
    if start_year and end_year:
        return redirect("pmodel.personal_report", formula.id, worker.id, start_year, end_year)
    else:
        return redirect("pmodel.personal_report", formula.id, worker.id)

@login_required
def personal_report(request, formulaid, workerid, depid=None, begin=None, end=None, pdf=False):
    formula = get_object_or_404(PointsFormula, pk=formulaid)
    worker = get_object_or_404(Worker, pk=workerid)
    department = None
    if depid:
        department = get_object_or_404(Department, pk=depid)
    # check permissions
    is_formula_admin = (request.user.is_superuser
                        or request.user.username in FORMULA_VIEWERS
                        or formula.createdby == request.user
                        or is_representative_for_user(request.user, formula.createdby)
                        or (department and permissions_check(request.user, department=department)))
    if not (is_formula_admin or formula.matches(request.user)):
        messages.error(request, u"У Вас нет прав для выполнения расчета по данной формуле.")
        return redirect("/pmodel/")
    if worker.user != request.user \
           and not (is_representative_for_worker(request.user, worker) \
                    or request.user.is_superuser
                    or request.user.username in FORMULA_VIEWERS):
        messages.error(request, u"У Вас нет прав для просмотра персонального рейтинга данного сотрудника.")
        return redirect("/pmodel/")

    emp = worker.current_main_employments
    dep = emp[0].department if emp else None
    top_department = dep.roots[0] if dep else None

    start_year = begin or formula.begin or 2010
    end_year = end or formula.end or 2014
    if request.method == 'GET':
        start_year = request.GET.get('start_year', start_year)
        end_year = request.GET.get('end_year', end_year)
    total_points, changes, data = process_formula_for_personal_report(
        formula,
        worker=worker,
        start_year=start_year,
        end_year=end_year,
        depid=depid
        )
    show_actuality_date(request)
    context = {
        'data': data,
        'formula': formula,
        'start_year': start_year,
        'end_year': end_year,
        'worker': worker,
        'total_points': total_points,
        'changes': changes,
        'worker_department': dep,
        'worker_top_department': top_department,
    }

    if pdf:
        latex = render_to_string("pmodel/personal_report.tex", context)
        pdf = latex_to_pdf(latex)
        response = HttpResponse(pdf, mimetype='application/pdf')
        response['Content-Disposition'] = 'inline; filename=personal_report.pdf'
        return response

    if (emp and not is_applicable_post(emp[0])):
        messages.warning(request, u"""Указанная Вами должность по основному месту работы (%s) не относится
        к категории <b>научных сотрудников</b>. Приведенный ниже персональный рейтинг носит
        информативный характер.""" % (emp[0].position or u"ДОЛЖНОСТЬ НЕ УКАЗАНА"))
    return render(request, "pmodel/personal_report.html", context)


@user_passes_test(lambda u: permissions_check(u))
def publish(request, formulaid):
    formula = get_object_or_404(PointsFormula, id=int(formulaid), createdby=request.user)
    if request.method == 'POST':
        form = PublishFormulaForm(request.POST)
        for_admins = form.data.get("publish_for_admins", None)
        if not form.data.get("department", None):
            messages.error(request, u"Невозможно опубликовать формулу, так как не выбрано ни одно подразделение.")
            return redirect("pmodel.publish", formulaid)
        try:
            depids = map(int, form.data.get("department","").split(','))
            for depid in depids:
                dep = Department.objects.get(pk = depid)
                formula.publish(dep, publisher=request.user, for_admins=for_admins)
        except ValueError:
            messages.error(request, u"Указан недопустимый номер подразделения.")
            return redirect("pmodel.publish", formulaid)
        return redirect("/pmodel/")

    publish_form = PublishFormulaForm(request_user=request.user)
    context = {
        "formula": formula,
        "publish_form": publish_form,
        }
    return render(request, "pmodel/publish.html",
                  context)


@user_passes_test(lambda u: permissions_check(u))
def copy(request, formulaid):
    formula = get_object_or_404(PointsFormula, id=formulaid)
    new_formula = formula.copy(user=request.user)
    return redirect("/pmodel/")


@user_passes_test(lambda u: permissions_check(u))
def manage_departments(request, formulaid):
    formula = get_object_or_404(PointsFormula, id=int(formulaid))
    depdata = formula.published_for.all()
    context = {
        "formula": formula,
        "depdata": depdata,
        "can_unpublish": request.user == formula.createdby,
        }
    return render(request, "pmodel/manage_departments.html",
                  context)

@login_required
def formulas_for_requester(request):
    worker = request.user.get_profile().worker
    if not worker:
        messages.error(request, u"Вы не закончили процедуру регистрации.")
        redirect("/home/")

    deps = worker.current_employments.values_list('department')
    alldeps = NestedParentDepartment.objects.filter(department__in=deps).values_list('parent')
    depdata = PointsFormulaDepartments.objects.filter(department__in=alldeps, for_admins=0)
    other_formulas = PointsFormulaDepartments.objects.filter(for_admins=0).exclude(department__in=alldeps)
    context = {
        "depdata": depdata,
        "other_formulas": other_formulas,
        "worker": worker,
        }
    return render(request, "pmodel/user_formulas.html",
                  context)

@login_required
def formulas_for_department(request, department_id):
    worker = request.user.get_profile().worker

    dep = get_object_or_404(Department, pk=department_id)
    alldeps = NestedParentDepartment.objects.filter(department=dep).values_list('parent')
    depdata = PointsFormulaDepartments.objects.filter(department__in=alldeps, for_admins=0)
    can_view_ratings = has_permission(request.user, 'can_view_ratings', dep)
    context = {
        "department": dep,
        "depdata": depdata,
        "worker": worker,
        "can_view_ratings": can_view_ratings,
        }
    return render(request, "pmodel/department_formulas.html",
                  context)



@user_passes_test(lambda u: permissions_check(u))
def unpublish(request, formulaid, linkid):
    formula = get_object_or_404(PointsFormula, id=int(formulaid), createdby=request.user)
    PointsFormulaDepartments.objects.filter(formula=formula, pk=linkid).delete()
    return redirect("pmodel.manage_departments", formulaid=formula.id)

# User-defined categories

@user_passes_test(lambda u: permissions_check(u))
def xhr_list_categories(request):
    def dump_category(category):
        return {
            'id': category.id,
            'name': category.name,
            'title': category.title,
            'createdby': str(category.createdby),
            'creationdate': str(category.creationdate),
            'modificationdate': str(category.modificationdate),
            'basecategory': category.basecategory,
        }

    if request.user.is_superuser:
        categories = CustomCategory.objects.all()
    else:
        categories = CustomCategory.objects.filter(createdby=request.user)

    response = dumps(map(dump_category, categories))
    return HttpResponse(response, content_type='application/json')

@user_passes_test(lambda u: permissions_check(u))
def xhr_save_category(request, catid=None):
    if catid is None:
        cat = CustomCategory.objects.create(createdby=request.user)
    else:
        cat = CustomCategory.objects.get(pk=catid, createdby=request.user)

    cat.jsondata = dumps({})
    if request.method == "POST":
        cat.jsondata = request.POST['jsonData']
        cat.basecategory = request.POST['basecategory']
        cat.name = request.POST['name']
        cat.title = request.POST['title']
        cat.modificationdate = datetime.datetime.now().replace(microsecond=0)
    cat.save()

    response = dumps({
       'id': cat.id,
       'createdby': str(cat.createdby),
       'creationdate': str(cat.creationdate),
       'modificationdata': str(cat.modificationdate)
    })
    return HttpResponse(response, content_type='application/json')

@user_passes_test(lambda u: permissions_check(u))
def xhr_get_category(request, catid):
    '''Returns JSON value of the requested category.'''
    cat = CustomCategory.objects.get(pk=catid, createdby=request.user)
    response = dumps({
        'name': cat.name,
        'title': cat.title,
        'basecategory': cat.basecategory,
        'jsondata': cat.jsondata
    })
    return HttpResponse(response, content_type='application/json')

@user_passes_test(lambda u: permissions_check(u))
def xhr_delete_category(request, catid):
    cat = CustomCategory.objects.get(pk=catid, createdby=request.user)
    cat.delete()
    return HttpResponse('{"deleted": %s}' % catid, content_type='application/json')

@user_passes_test(lambda u: permissions_check(u))
def manage_categories(request):
    context = {}
    return render(request, "pmodel/categories.html",
                  context)

@login_required
def get_positions(request):
    # TODO: return only positions the user has permission to see
    response = []

    granted_permitions = AllGrantedPermissions.objects.current.filter(role_name='is_representative_for', user=request.user)
    all_departments = granted_permitions.values('department')

    employees_positions = Employment.objects.current().filter(department__in=all_departments).values('position')
    positions = Position.objects.filter(id__in=employees_positions).exclude(category=4).order_by('name')

    for pos in positions:
        response.append({
            'id': pos.id,
            'name': pos.name,
            'shortname': pos.shortname
        })
    return HttpResponse(dumps(response), content_type='application/json')

# Journal lists

@login_required
def xhr_journallists_info(request, formulaid=None):
    granted_departments = AllGrantedPermissions.objects.current.filter(
                              Q(role_name='is_representative_for') | Q(role_name='can_view_ratings'),
                              user=request.user).values('department')
    parent_relations = NestedParentDepartment.objects.filter(department__in=granted_departments)
    parent_departments = parent_relations.values_list('parent', flat=True)

    jls = JournalList.objects.all()
    response = {jl.id: {
        'name': jl.name,
        'available': jl.department.id in parent_departments
    } for jl in jls}
    return HttpResponse(dumps(response), content_type='application/json')

@login_required
def xhr_create_journallist(request):
    organization = get_object_or_404(Organization, pk=int(request.POST['organization']))
    department = get_object_or_404(Department, pk=int(request.POST['department']))
    name = request.POST.get('name', '')
    if not name:
        messages.error(request, u'Вы не ввели название списка журналов.')
    elif permissions_check(request.user, department):
        jl = JournalList.objects.create(organization=organization,
                                        department=department,
                                        name=name,
                                        createdby=request.user)
        jl.save()
        messages.success(request, u'Список журналов %s создан.' % jl.name)
    else:
        messages.error(request, u'У Вас нет прав для выполнения данной операции.')
    return redirect("pmodel.journallists_index")

@login_required
def xhr_delete_journallist(request, jlid):
    jl = get_object_or_404(JournalList, pk=jlid)
    if permissions_check(request.user, jl.department):
        name = jl.name
        jl.delete()
        messages.success(request, u'Список журналов %s удалён.' % name)
    else:
        messages.error(request, u'У Вас нет прав для выполнения данной операции.')
    return redirect("pmodel.journallists_index")

@login_required
def add_journallist_item(request, jlid):
    form = JournalSelectForm(request.POST)
    jl = get_object_or_404(JournalList, pk=jlid)
    if not form.is_valid():
        messages.error(request, u'Неверный ввод.')
    elif permissions_check(request.user, jl.department):
        date_begin = request.POST['date_begin']
        date_end = request.POST['date_end']
        item = JournalListItem.objects.create(list=jl,
                                              journal=form.cleaned_data['journal'],
                                              createdby=request.user)
        if date_begin:
            item.begin = datetime.date(*time.strptime(date_begin, '%Y-%m-%d')[:3])
        if date_end:
            item.end = datetime.date(*time.strptime(date_end, '%Y-%m-%d')[:3])
        item.save()
        messages.success(request, u'Журнал %s добавлен к списку.' % item.journal.name)
    else:
        messages.error(request, u'У Вас нет прав для выполнения данной операции.')
    return redirect("pmodel.edit_journallist", jlid)

@login_required
def delete_journallist_item(request, jlid, itemid):
    item = get_object_or_404(JournalListItem, pk=itemid)
    if permissions_check(request.user, item.list.department):
        journal_name = item.journal.name
        item.delete()
        messages.success(request, u'Журнал %s удалён из списка.' % journal_name)
    else:
        messages.error(request, u'У Вас нет прав для выполнения данной операции.')
    return redirect("pmodel.edit_journallist", jlid)

@login_required
def journallists_index(request):
    permissions = AllGrantedPermissions.objects.current
    available_departments = permissions.filter(Q(role_name='is_representative_for'), user=request.user).values('department')
    available_children_departments = available_departments.filter(
                                         department__children_relations__child__isnull=False
                                     ).values('department__children_relations__child')
    available_nonchild_departments = Department.objects.filter(Q(pk__in=available_departments)
                                         ).exclude(pk__in=available_children_departments)

    parent_relations = NestedParentDepartment.objects.filter(department__in=available_departments)
    parent_departments = parent_relations.values_list('parent', flat=True)

    context = {
        "journal_lists": JournalList.objects.filter(department__id__in=parent_departments),
        "organization_msu": Organization.objects.get_msu(),
        "available_departments": available_nonchild_departments,
    }
    return render(request, "pmodel/journal_lists.html", context)

@login_required
def edit_journallist(request, jlid):
    jl = get_object_or_404(JournalList, pk=jlid)
    items = JournalListItem.objects.filter(list=jl)
    context = {
        "journal_list": jl,
        "items": items,
        "form": JournalSelectForm()
    }
    return render(request, "pmodel/edit_journal_list.html", context)

# External teaching work info ("pednagruzka")

def xhr_ext_classes(request):
    response = dumps({
        "forms": {f.id: f.name.rstrip() for f in EducForm.objects.all()},
        "types": {t.id: t.name.rstrip() for t in EducType.objects.all()},
        "stages": {s.id: s.name.rstrip() for s in EducStage.objects.all()}
    })
    return HttpResponse(response, content_type='application/json')
