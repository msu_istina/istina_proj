# Author: Dmitry Shachnev <mitya57@gmail.com>, 2014.

from common.utils.admin import register_with_versions
from . import models

register_with_versions(models.PointsFormula)
register_with_versions(models.JournalList)
register_with_versions(models.JournalListItem)
