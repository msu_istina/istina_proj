# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from django.conf.urls import patterns, url

from django.views.generic.simple import direct_to_template

# register all update processors
from updates_processor import XHRConfEditorProcessor

urlpatterns = patterns('pmodel.views',
    url(r'^submitdata/$', 'submitdata', name='pmodel.submitdata'),
    url(r'^submitdata/hierarchical/$', 'submitdata', {'hierarchical': True}, name='pmodel.submitdata'),
    url(r'^savedata/(?P<formulaid>\d+)/$', 'savedata', name='pmodel.savedata'),
    url(r'^getdata/(?P<formulaid>\d+)/$', 'getdata', name='pmodel.getdata'),
    url(r'^edit/(?P<formulaid>\d+)/$', 'editformula', name='pmodel.editformula'),
    url(r'^view/(?P<formulaid>\d+)/$', 'viewformula', name='pmodel.viewformula'),
    url(r'^delete/(?P<formulaid>\d+)/$', 'deleteformula', name='pmodel.deleteformula'),
    url(r'^run/(?P<formulaid>\d+)/$', 'runformula', name='pmodel.runformula'),
    url(r'^run/average/(?P<formulaid>\d+)/((?P<depid>\d+)/)?$', 'runformula_average', name='pmodel.runformula_average'),
    url(r'^run/(?P<formulaid>\d+)/(?P<workerid>\d+)/$', 'personal_report', name='pmodel.personal_report'),
    url(r'^run/(?P<formulaid>\d+)/(?P<workerid>\d+)/start/(?P<begin>\d+)/end/(?P<end>\d+)/$', 'personal_report', name='pmodel.personal_report'),
    url(r'^run/(?P<formulaid>\d+)/(?P<workerid>\d+)/start/(?P<begin>\d+)/end/(?P<end>\d+)/pdf/$', 'personal_report', {'pdf': True}, name='pmodel.personal_pdf'),
    url(r'^run/(?P<formulaid>\d+)/(?P<workerid>\d+)/(?P<depid>\d+)/$', 'personal_report', name='pmodel.personal_report_for_department'),
    #
    url(r'^improve/$', direct_to_template, {'template': 'pmodel/personal_report_improvements.html'}, name='pmodel.personal_report_improvements'),
    url(r'^run/personal/$', 'default_personal_report', name='pmodel.run_default_personal_report'),
    url(r'^run/personal/(?P<workerid>\d+)/$', 'default_personal_report', name='pmodel.default_worker_report'),
    url(r'^add/$', 'addformula', name='pmodel.addformula'),
    url(r'^publish/(?P<formulaid>\d+)/$', 'publish', name='pmodel.publish'),
    url(r'^unpublish/(?P<formulaid>\d+)/(?P<linkid>\d+)/$', 'unpublish', name='pmodel.unpublish'),
    url(r'^copy/(?P<formulaid>\d+)/$', 'copy', name='pmodel.copy'),
    url(r'^manage_departments/(?P<formulaid>\d+)/$', 'manage_departments', name='pmodel.manage_departments'),
    url(r'^apply/$', 'formulas_for_requester', name='pmodel.formulas_for_requester'),
    url(r'^department/(?P<department_id>\d+)/$', 'formulas_for_department', name='pmodel.formulas_for_department'),
    url(r'^getpositions/$', 'get_positions', name='pmodel.get_positions'),
    # user-defined categories
    url(r'^listcategories/$', 'xhr_list_categories', name='pmodel.list_categories'),
    url(r'^savecategory/((?P<catid>\d+)/)?$', 'xhr_save_category', name='pmodel.save_category'),
    url(r'^getcategory/(?P<catid>\d+)/$', 'xhr_get_category', name='pmodel.get_category'),
    url(r'^deletecategory/(?P<catid>\d+)/$', 'xhr_delete_category', name='pmodel.delete_category'),
    url(r'^editcategories/$', 'manage_categories', name='pmodel.manage_categories'),
    # journal lists
    url(r'^journallists_info/(?P<formulaid>\d+)/$', 'xhr_journallists_info', name='pmodel.journallists_info'),
    url(r'^createjournallist/$', 'xhr_create_journallist', name='pmodel.create_journallist'),
    url(r'^deletejournallist/(?P<jlid>\d+)/$', 'xhr_delete_journallist', name='pmodel.delete_journallist'),
    url(r'^addjournallistitem/(?P<jlid>\d+)/$', 'add_journallist_item', name='pmodel.add_journallist_item'),
    url(r'^deletejournallistitem/(?P<jlid>\d+)/(?P<itemid>\d+)/$', 'delete_journallist_item', name='pmodel.delete_journallist_item'),
    url(r'^journallists/$', 'journallists_index', name='pmodel.journallists_index'),
    url(r'^journallists/(?P<jlid>\d+)/$', 'edit_journallist', name='pmodel.edit_journallist'),
    # external teaching work info ("pednagruzka")
    url(r'^extclasses/$', 'xhr_ext_classes', name='pmodel.ext_classes'),
    #
    url(r'^$', 'index', name='pmodel.index'),
)
