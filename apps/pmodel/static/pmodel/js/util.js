/* Various utility functions.
 *
 * Author: Dmitry Shachnev <mitya57@gmail.com>, 2015.
 */

Array.prototype.contains = function(val) {
  return this.indexOf(val) != -1;
};

if (String.prototype.startsWith === undefined) {
  Object.defineProperty(String.prototype, 'startsWith', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: function(searchString, position) {
      position = position || 0;
      return this.lastIndexOf(searchString, position) === position;
    }
  });
}

possibleParameterClasses = ['only_property', 'only_restrictions', 'boolean'];

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function getRawParameters(category) {
  var tokens = category.split('.');
  var result = [];
  for (var a = 0; a <= tokens.length; ++a) {
    var subcategory = tokens.slice(0, a).join('.');
    if (database.parameters.hasOwnProperty(subcategory)) {
      result = result.concat(database.parameters[subcategory]);
    }
  }
  return result;
}

function getPossibleParameters(category, mode, boolmode) {
  /* mode is a string, i.e. "property" (the default value) or "restrictions".
   *
   * if boolmode is defined and is true, then only boolean parameters are returned;
   * if boolmode is defined and is false, then only integer parameters are returned;
   * if boolmode is undefined, then all parameters are returned.
   */
  mode = mode || 'property';

  var rawParameters = getRawParameters(category);
  var result = [];
  for (var i = 0; i < rawParameters.length; ++i) {
    var parameterTokens = rawParameters[i].split('.');
    var parameterClasses = [];
    while (possibleParameterClasses.contains(
             parameterTokens[parameterTokens.length - 1])) {
      parameterClasses.push(parameterTokens.pop());
    }
    var parameter = parameterTokens.join('.');
    var hasClass = function(className) {
      return parameterClasses.contains(className);
    };
    if ((mode != 'restrictions' && hasClass('only_restrictions')) ||
        (mode != 'property' && hasClass('only_property'))) {
      continue;
    }
    if ((boolmode == true && !hasClass('boolean')) ||
        (boolmode == false && hasClass('boolean'))) {
      continue;
    }
    result.push(parameter);
  }
  return result;
}

function clearNode(node) {
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }
}

function createOption(value, text, emphasize) {
  var option = document.createElement('option');
  option.value = value;
  if (emphasize) {
    var emph = document.createElement('em');
    emph.textContent = text;
    option.appendChild(emph);
  } else {
    option.textContent = text;
  }
  return option;
}

function findGroupInd(category) {
  var firstToken = category.split('.')[0];
  for (var i = 0; i < database.category_groups.length; ++i) {
    var group = database.category_groups[i];
    if (group.included_categories.contains(firstToken)) {
      return i;
    }
  }
}

function fillCategorySelect(select, groupInd) {
  clearNode(select);

  if (database.category_groups[groupInd].name == 'study_work_external') {
    for (var i in extClasses.types) {
      var option = createOption('study_work_external.type' + i,
                                capitalizeFirstLetter(extClasses.types[i]));
      select.appendChild(option);
    }
    return;
  }

  database.categories.forEach(function(category) {
    var firstToken = category.split('.')[0];
    var group = database.category_groups[groupInd];
    if (groupInd === undefined ||
        group.included_categories.contains(firstToken)) {
      var option = createOption(category, translateQualifier(category, true));
      select.appendChild(option);
    }
  });
}

function selectCategory(value, select, mode, boolmode) {
  /* See getPossibleParameters() documentation for details on what
   * mode and boolmode do.
   */
  clearNode(select);
  var parameters = getPossibleParameters(value, mode, boolmode);
  if (!parameters.length) {
    parameters.push('');
  }
  parameters.forEach(function(parameter) {
    var option = createOption(parameter, translateQualifier(parameter));
    select.appendChild(option);
  });
}
