/* Formula constructor interface implementation.
 *
 * Author: Dmitry Shachnev <mitya57@gmail.com>, 2014-2015.
 */

var rowsData = [];
var groupLimits = [];
var readOnlyMode = false;
var customCategories = [];
var groupStartsInTable = [];
var groupStartsInData = [];
var pageType = null;
var journalLists = null;
var selectedPositions = [405492, 405497, 405521, 405522, 405523, 405546];
var extClasses = {};

Array.prototype.swap = function(i, j) {
  var tmp = this[i];
  this[i] = this[j];
  this[j] = tmp;
};

function initialize() {
  var pathComponents = document.location.pathname.split('/');
  pageType = pathComponents[pathComponents.length - 3];
  readOnlyMode = (pageType == 'view');

  var row = document.createElement('tr');

  var headers = ['Категория', 'Ограничения', 'Свойства', 'Вес'];
  if (readOnlyMode) {
    var table = document.getElementById('categories_table');
    var cols = table.getElementsByTagName('col');
    var newWidths = ['33%', '33%', '28%', '6%'];
    for (var i = 0; i < 4; ++i) {
      cols[i].width = newWidths[i];
    }
    var actionsCol = cols[4];
    actionsCol.parentNode.removeChild(actionsCol);
  } else {
    headers.push('Действия');
  }
  headers.forEach(function(label) {
    var th = document.createElement('th');
    th.textContent = label;
    var help = getHelp(label);
    if (help) {
      var helpSpan = document.createElement('span');
      helpSpan.className = 'help';
      helpSpan.textContent = '[?]';
      helpSpan.title = help;
      th.appendChild(helpSpan);
    }
    row.appendChild(th);
  });
  var tableBody = getTableBody();
  tableBody.appendChild(row);

  for (var i = 0; i < database.category_groups.length; ++i) {
    var group = database.category_groups[i];
    var th = document.createElement('th');
    th.colSpan = 4;

    var div = document.createElement('div');
    div.textContent = translateToken(group.name);
    th.appendChild(div);
    var description = getDescription(group.name);
    if (description) {
      var span = document.createElement('span');
      span.textContent = description;
      span.style['font-weight'] = 'normal';
      th.appendChild(span);
    }
    var placeholder = document.createElement('div');
    placeholder.className = 'limit_placeholder';
    placeholder.style['font-weight'] = 'normal';
    th.appendChild(placeholder);

    var row = document.createElement('tr');
    row.appendChild(th);

    if (!readOnlyMode) {
      var addRowButton = document.createElement('button');
      addRowButton.textContent = '+';
      addRowButton.title = 'Добавить строку';
      addRowButton.onclick = function(groupInd) {
        return function() {
          addRowAndData(groupInd);
        }
      }(i);
      var actionsTh = document.createElement('th');
      actionsTh.appendChild(addRowButton);
      row.appendChild(actionsTh);
    }
    tableBody.appendChild(row);
    groupStartsInTable.push(i + 1);
    groupStartsInData.push(0);
  }
  groupStartsInData.push(0);

  var jlsurl = document.location.pathname.replace(pageType, 'journallists_info');
  $.get(jlsurl, onJournalListsReceived, 'json');

  $.get('/pmodel/extclasses/', onExtClassesReceived, 'json');

  var requesturl = document.location.pathname.replace(pageType, 'getdata');
  $.get(requesturl, onDataReceived, 'json');

  $.get('/pmodel/listcategories/', onCustomCategoriesReceived, 'json');
  if (!readOnlyMode) {
    $.get('/pmodel/getpositions/', onPositionsReceived, 'json');
  }

  if (readOnlyMode) {
    var buttonsRow = document.getElementById('buttons_row');
    buttonsRow.parentNode.removeChild(buttonsRow);
    var histogramArea = document.getElementById('histogram_area');
    histogramArea.parentNode.removeChild(histogramArea);
  }

  var startSelect = document.getElementById('startyear');
  var endSelect = document.getElementById('endyear');
  var currentYear = new Date().getFullYear();
  [startSelect, endSelect].forEach(function(select) {
    for (var year = currentYear - 10; year <= currentYear; ++year) {
      var option = document.createElement('option');
      option.value = String(year);
      option.appendChild(document.createTextNode(String(year)));
      select.appendChild(option);
    }
  });
  startSelect.value = currentYear - 5;
  endSelect.value = currentYear - 1;

  showDiagramWithData([]);
}

function getLimitDescription(groupLimit) {
  if (groupLimit === null) {
    return 'не установлено';
  }
  if (groupLimit.type == 'limit') {
    return groupLimit.value +
      [' балл', ' балла', ' баллов'][getPluralForm(groupLimit.value)];
  }
  if (groupLimit.type == 'percent') {
    return groupLimit.value + ' % от общей суммы баллов';
  }
}

function updateLimit(groupInd) {
  if (groupLimits[groupInd] === undefined) {
    groupLimits[groupInd] = null;
  }
  if (readOnlyMode && groupLimits[groupInd] === null) {
    return;
  }
  var th = getTableBody().children[groupStartsInTable[groupInd]];
  var placeholder = th.getElementsByClassName('limit_placeholder')[0];

  var mpSpan = document.createElement('span');
  mpSpan.textContent = 'Ограничение: ';
  placeholder.appendChild(mpSpan);

  if (readOnlyMode) {
    mpSpan.textContent += getLimitDescription(groupLimits[groupInd]);
    return;
  }

  var valSpan = document.createElement('a');
  valSpan.textContent = getLimitDescription(groupLimits[groupInd]);
  placeholder.appendChild(valSpan);
  var valInput = document.createElement('input');
  valInput.pattern = '[0-9.]+';
  var valTypeSelect = document.createElement('select');
  valTypeSelect.appendChild(createOption('limit', 'баллов'));
  valTypeSelect.appendChild(createOption('percent', '% от общей суммы баллов'));
  var valBox = document.createElement('span');
  valBox.appendChild(valInput);
  valBox.appendChild(valTypeSelect);

  valSpan.onclick = function() {
    var callback = function() {
      var numValue = parseInt(valInput.value);
      if (numValue) {
        groupLimits[groupInd] = {
          category_group: database.category_groups[groupInd].name,
          type: valTypeSelect.value,
          value: numValue
        };
      } else {
        groupLimits[groupInd] = null;
      }
      valSpan.textContent = getLimitDescription(groupLimits[groupInd]);
    };
    showDialog('Введите максимальный балл или процент', callback, valBox);
    if (groupLimits[groupInd] === null) {
      valTypeSelect.value = 'limit';
      valInput.value = '';
    } else {
      valTypeSelect.value = groupLimits[groupInd].type;
      valInput.value = groupLimits[groupInd].value;
    }
  };
}

function importLine(line) {
  if (line.hasOwnProperty('category_group')) {
    var groupName = line.category_group;
    for (var i = 0; i < database.category_groups.length; ++i) {
      if (database.category_groups[i].name == groupName) {
        groupLimits[i] = line;
        return;
      }
    }
    console.warn('Unknown group: ' + groupName);
    return;
  }

  if (database.renamed_categories.hasOwnProperty(line.category)) {
    line.category = database.renamed_categories[line.category];
  }
  var groupInd = findGroupInd(line.category);
  if (groupInd === undefined) {
    console.warn('Unknown category: ' + line.category);
    return;
  }
  rowsData.splice(groupStartsInData[groupInd + 1], 0, line);
  for (var i = groupInd + 1; i < groupStartsInData.length; ++i) {
    ++groupStartsInData[i];
  }
  addRow(groupInd, false);
}

function onJournalListsReceived(data, textStatus, jqXHR) {
  if (jqXHR.status == 200) {
    journalLists = data;
  }
  var placeholders = document.getElementsByClassName('journal_list_placeholder');
  for (var i = 0; i < placeholders.length; ++i) {
    var jlDiv = placeholders[i];
    jlDiv.textContent = 'Журналы только из списка: ' +
                        journalLists[jlDiv.getAttribute('jlId')].name;
  }
}

function onExtClassesReceived(data, textStatus, jqXHR) {
  if (jqXHR.status == 200) {
    extClasses = data;
    var placeholders = document.getElementsByClassName('educ_stage_placeholder');
    for (var i = 0; i < placeholders.length; ++i) {
      var educStageDiv = placeholders[i];
      educStageDiv.textContent = 'Этап обучения: ' +
          extClasses.stages[educStageDiv.getAttribute('stageId')];
    }
    placeholders = document.getElementsByClassName('educ_type_placeholder');
    for (var i = 0; i < placeholders.length; ++i) {
      var educTypeSpan = placeholders[i];
      var id = educTypeSpan.textContent.match(/Type(\d+)/)[1];
      var td = educTypeSpan.parentNode;
      educTypeSpan.textContent = capitalizeFirstLetter(extClasses.types[id]);
      if (educTypeSpan.textContent == td.firstChild.textContent) {
        educTypeSpan.style.display = 'none';
      }
    }
  }
}

function onDataReceived(data, textStatus, jqXHR) {
  if (jqXHR.status == 200) {
    var titleInput = document.getElementById('title_input');
    titleInput.value = data['title'];
    if (readOnlyMode) {
      titleInput.readOnly = true;
    }
    receivedData = JSON.parse(data['jsondata']);
    for (var i = 0; i < receivedData.length; ++i) {
      importLine(receivedData[i]);
    }
    for (var i = 0; i < database.category_groups.length; ++i) {
      updateLimit(i);
    }
    if (data.startyear) {
      document.getElementById('startyear').value = data.startyear;
    }
    if (data.endyear) {
      document.getElementById('endyear').value = data.endyear;
    }
  } else {
    showDialog(textStatus);
  }
}

function onCustomCategoriesReceived(data, textStatus, jqXHR) {
  if (jqXHR.status == 200) {
    data.forEach(function(catData) {
      /* example: articles.collections.custom12345 */
      catData.newId = catData.basecategory + '.custom' + catData.id;
      database.categories.push(catData.newId);
    });
    database.categories.sort();
    customCategories = data;
  } else {
    showDialog(textStatus);
  }
}

function onPositionsReceived(data, textStatus, jqXHR) {
  if (jqXHR.status == 200) {
    var ul = document.createElement('ul');
    ul.className = 'positions';

    var selectAll = document.createElement('input');
    selectAll.type = 'checkbox';
    selectAll.id = 'positions_selectall';
    selectAll.indeterminate = true;
    selectAll.onchange = function() {
      var allCheckBoxes = ul.getElementsByTagName('input');
      for (var i = 0; i < allCheckBoxes.length; ++i) {
        allCheckBoxes[i].checked = this.checked;
      }
    };
    var label = document.createElement('label');
    label.appendChild(selectAll);
    label.appendChild(document.createTextNode('Выбрать все'));
    var li = document.createElement('li');
    li.style['margin-bottom'] = '8px';
    li.appendChild(label);
    ul.appendChild(li);

    data.forEach(function(posData) {
      var input = document.createElement('input');
      input.type = 'checkbox';
      if (selectedPositions.indexOf(posData.id) >= 0) {
        input.checked = true;
      }
      input.onchange = function() {
        if (this.checked) {
          selectedPositions.push(posData.id);
        } else {
          for (var i = 0; i < selectedPositions.length; ++i) {
            if (selectedPositions[i] == posData.id) {
              selectedPositions.splice(i, 1);
              break;
            }
          }
        }

        var allCheckBoxes = ul.getElementsByTagName('input');
        var existsChecked = false;
        var existsUnchecked = false;
        for (var i = 0; i < allCheckBoxes.length; ++i) {
          if (allCheckBoxes[i].id != 'positions_selectall') {
            if (allCheckBoxes[i].checked) {
              existsChecked = true;
            } else {
              existsUnchecked = true;
            }
          }
        }
        selectAll.indeterminate = existsChecked && existsUnchecked;
        selectAll.checked = !existsUnchecked;
      };
      var label = document.createElement('label');
      label.appendChild(input);
      label.appendChild(document.createTextNode(posData.name));
      var li = document.createElement('li');
      li.appendChild(label);
      ul.appendChild(li);
    });
    var placeholder = document.getElementById('positions_placeholder');
    placeholder.appendChild(ul);
  } else {
    showDialog(textStatus);
  }
}

function getTableBody() {
  var table = document.getElementById('categories_table');
  return table.getElementsByTagName('tbody')[0];
}

function selectModifiersForCategory(category, modifierSelect) {
  clearNode(modifierSelect);

  var addOption = createOption('', 'Добавить модификатор', true);
  modifierSelect.appendChild(addOption);
  var clearOption = createOption('DEL', 'Очистить модификаторы', true);
  modifierSelect.appendChild(clearOption);

  for (var modifier in availableModifiers) {
    var modifierObj = availableModifiers[modifier];
    if (isGoodFor(modifierObj, category)) {
      var option = createOption(modifier, modifierObj.title);
      modifierSelect.appendChild(option);
    }
  }
}

function getGroupInd(rowNumber) {
  var groupInd = 0;
  while (groupInd + 1 < groupStartsInTable.length &&
         groupStartsInTable[groupInd + 1] < rowNumber) {
    ++groupInd;
  }
  return groupInd;
}

function saveData(groupInd, localRowNumber) {
  var rowNumber = groupStartsInTable[groupInd] + localRowNumber + 1;
  var row = getTableBody().children[rowNumber];
  var categoriesTd = row.children[0];
  var restrictionsTd = row.children[1];
  var propertyTd = row.children[2];
  var weightTd = row.children[3];

  var indexInData = groupStartsInData[groupInd] + localRowNumber;
  var data = rowsData[indexInData];

  data.category = categoriesTd.getElementsByTagName('select')[0].value;
  data.parameter = propertyTd.firstChild.value;

  data.weight = weightTd.firstChild.value;
  if (!data.weight) {
    data.weight = '1';
  }

  var enteredLabel = categoriesTd.getElementsByTagName('input')[0];
  data.label = enteredLabel.value;
  if (!data.label) {
    customCategories.forEach(function(catData) {
      if (data.category == catData.newId) {
        data.label = catData.title;
      }
    });
  }
  if (!data.label) {
    var tokens = data.category.split('.');
    data.label = translateToken(tokens[tokens.length - 1]);
  }

  data.restrictions = [];
  var lis = restrictionsTd.getElementsByTagName('li');
  for (var i = 0; i < lis.length; ++i) {
    var li = lis[i];
    var param = li.getElementsByClassName('parameter_select')[0].value;
    var leftInput = li.getElementsByClassName('left_input')[0];
    if (param && leftInput.type == 'checkbox') {
      data.restrictions.push({
        parameter: param,
        value: leftInput.checked
      });
      continue;
    }
    var rightInput = li.getElementsByClassName('right_input')[0];
    if (param && (leftInput.value || rightInput.value)) {
      data.restrictions.push({
        parameter: param,
        lower_bound: leftInput.value,
        upper_bound: rightInput.value
      });
    }
  }

  delete data.journalList;
  if (categoryIsJournals(data.category)) {
    var jlSelect = restrictionsTd.getElementsByClassName('journal_list_select')[0];
    if (jlSelect.value) {
      data.journalList = jlSelect.value;
    }
  }

  delete data.educStage;
  if (data.category.startsWith('study_work_external')) {
    var educStageSelect = restrictionsTd.getElementsByClassName('educ_stage_select')[0];
    if (educStageSelect.value) {
      data.educStage = educStageSelect.value;
    }
  }
}

function addRestriction(row, parameter, leftval, rightval) {
  var ul = row.getElementsByClassName('restrictions_ul')[0];
  var li = document.createElement('li');
  var isBoolean = (typeof leftval == 'boolean');
  if (isBoolean) {
    li.className = 'boolean_restriction';
  }

  var leftInput = document.createElement('input');
  if (isBoolean) {
    leftInput.type = 'checkbox';
    leftInput.className = 'left_input';
    leftInput.checked = leftval;
  } else {
    leftInput.className = 'modifier_small_input left_input';
    if (leftval !== undefined) {
      leftInput.value = leftval;
    }

    var rightInput = document.createElement('input');
    rightInput.className = 'modifier_small_input right_input';
    if (rightval !== undefined) {
      rightInput.value = rightval;
    }
  }

  var categorySelect = row.getElementsByClassName('category_select')[0];
  var select = document.createElement('select');
  select.className = 'parameter_select';
  selectCategory(categorySelect.value, select, 'restrictions', isBoolean);
  if (parameter) {
    select.value = parameter;
  }

  var removeButton = document.createElement('button');
  removeButton.textContent = '\u2717';
  removeButton.title = 'Удалить';
  removeButton.onclick = function() {
    ul.removeChild(li);
  };
  removeButton.style['margin-left'] = '2px';

  li.appendChild(leftInput);
  if (!isBoolean) {
    li.appendChild(document.createTextNode('\u2a7d'));
  }
  li.appendChild(select);
  if (!isBoolean) {
    li.appendChild(document.createTextNode('\u2a7d'));
    li.appendChild(rightInput);
  }
  li.appendChild(removeButton);
  ul.appendChild(li);
}

function enableEditMode(groupInd, rowNumber) {
  var rowAbsNumber = groupStartsInTable[groupInd] + rowNumber + 1;
  var row = getTableBody().children[rowAbsNumber];

  for (var i = 0; i < row.children.length; ++i) {
    clearNode(row.children[i]);
  }
  var categoriesTd = row.children[0];
  var restrictionsTd = row.children[1];
  var propertyTd = row.children[2];
  var weightTd = row.children[3];
  var actionsTd = row.children[4];

  var data = rowsData[groupStartsInData[groupInd] + rowNumber];

  /* Label */
  var labelTitle = document.createTextNode('Заголовок:');
  var labelInput = document.createElement('input');
  labelInput.className = 'label_input';

  var tokens = data.category.split('.');
  var defaultLabel = translateToken(tokens[tokens.length - 1]);
  labelInput.placeholder = defaultLabel;

  if (data.label !== undefined && data.label != defaultLabel) {
    labelInput.value = data.label;
  }
  var labelDiv = document.createElement('div');
  labelDiv.appendChild(labelTitle);
  labelDiv.appendChild(labelInput);
  categoriesTd.appendChild(labelDiv);

  /* Categories */
  var categorySelect = document.createElement('select');
  categorySelect.className = 'category_select';
  fillCategorySelect(categorySelect, groupInd);
  categorySelect.value = data.category;
  categoriesTd.appendChild(categorySelect);

  /* Restrictions (common) */
  var newRestrictionSelect = document.createElement('select');
  newRestrictionSelect._update = function(category) {
    clearNode(this);
    this.appendChild(createOption('', 'Добавить ограничение', true));
    this.integerRestrictions = getPossibleParameters(category, 'restrictions', false);
    this.booleanRestrictions = getPossibleParameters(category, 'restrictions', true);
    var allRestrictions = this.integerRestrictions.concat(this.booleanRestrictions);
    for (var i = 0; i < allRestrictions.length; ++i) {
      var restriction = allRestrictions[i];
      this.appendChild(createOption(restriction, translateQualifier(restriction)));
    }
  };
  newRestrictionSelect.onchange = function() {
    if (this.value && this.integerRestrictions.indexOf(this.value) != -1) {
      addRestriction(row, this.value);
    }
    if (this.value && this.booleanRestrictions.indexOf(this.value) != -1) {
      addRestriction(row, this.value, true);
    }
    this.value = '';
  };
  newRestrictionSelect._update(categorySelect.value);
  restrictionsTd.appendChild(newRestrictionSelect);

  var restrictionsUl = document.createElement('ul');
  restrictionsUl.className = 'restrictions_ul';
  restrictionsTd.appendChild(restrictionsUl);

  if (data.restrictions) {
    data.restrictions.forEach(function(restriction) {
      if (restriction.hasOwnProperty('value')) {
        // boolean
        addRestriction(row, restriction.parameter,
                       restriction.value);
      } else {
        addRestriction(row, restriction.parameter,
                       restriction.lower_bound,
                       restriction.upper_bound);
      }
    });
  }

  /* Restrictions (journal lists) */
  var jlSelect = document.createElement('select');
  jlSelect.className = 'journal_list_select';
  jlSelect.appendChild(createOption('', 'Выберите список журналов', true));
  if (journalLists) {
    for (var jlId in journalLists) {
      var jl = journalLists[jlId];
      if (jl.available) {
        jlSelect.appendChild(createOption(jlId, jl.name));
      }
    }
    if (categoryIsJournals(data.category) && data.journalList) {
      jlSelect.value = data.journalList;
    }
  }
  if (!journalLists || !categoryIsJournals(data.category)) {
    jlSelect.style.display = 'none';
  }
  restrictionsTd.appendChild(jlSelect);

  /* Restrictions (external teaching work) */
  if (database.category_groups[groupInd].name == 'study_work_external') {
    var educStageSelect = document.createElement('select');
    educStageSelect.className = 'educ_stage_select';
    educStageSelect.appendChild(createOption('', 'любой этап обучения'));
    for (var i in extClasses.stages) {
      educStageSelect.appendChild(createOption(i, extClasses.stages[i]));
    }
    if (data.educStage) {
      educStageSelect.value = data.educStage;
    }
    restrictionsTd.appendChild(educStageSelect);
  }

  /* Parameters */
  var parameterSelect = document.createElement('select');
  parameterSelect.className = 'parameter_select';
  selectCategory(categorySelect.value, parameterSelect);
  parameterSelect.value = data.parameter;
  propertyTd.appendChild(parameterSelect);

  /* Weight */
  var weightInput = document.createElement('input');
  weightInput.className = 'cell_input';
  weightInput.type = 'text';
  weightInput.pattern = '[0-9.]+';
  weightInput.value = data.weight ? data.weight : '1';
  weightTd.appendChild(weightInput);

  /* Modifiers */
  var modifiersDiv = document.createElement('div');
  modifiersDiv.className = 'modifiers_area';

  var modifierSelect = document.createElement('select');
  modifierSelect.className = 'modifier_select';
  selectModifiersForCategory(categorySelect.value, modifierSelect);

  modifierSelect.onchange = function() {
    if (this.value == 'DEL') {
      data.modifiers = [];
      var cell = modifiersDiv;
      /* Clear cell */
      while (cell.childNodes.length > 1) {
        cell.removeChild(cell.lastChild);
      }
    } else if (this.value) {
      var present = false;
      var newArray = [this.value];
      data.modifiers.forEach(function(modifierArray) {
        if (modifierArray[0] == newArray[0]) {
          present = true;
        }
      });
      if (!present) {
        var modifier = availableModifiers[this.value];
        data.modifiers.push(newArray);
        modifier.create(row, newArray);
      }
    }
    this.value = '';
  };
  modifiersDiv.appendChild(modifierSelect);
  propertyTd.appendChild(modifiersDiv);

  data.modifiers.forEach(function(modifierArray) {
    if (availableModifiers.hasOwnProperty(modifierArray[0])) {
      var modifier = availableModifiers[modifierArray[0]];
      modifier.create(row, modifierArray);
    }
  });

  /* We need to update two selects when categorySelect changes value */
  categorySelect.onchange = function() {
    var tokens = this.value.split('.');
    labelInput.placeholder = translateToken(tokens[tokens.length - 1]);
    selectCategory(this.value, parameterSelect);
    for (var i = 0; i < restrictionsUl.children.length; ++i) {
      var li = restrictionsUl.children[i];
      var isBoolean = (li.className == 'boolean_restriction');
      var restrictSelect = li.getElementsByClassName('parameter_select')[0];
      selectCategory(this.value, restrictSelect, 'restrictions', isBoolean);
    }
    selectModifiersForCategory(this.value, modifierSelect);
    newRestrictionSelect._update(this.value);
    jlSelect.style.removeProperty('display');
    if (!journalLists || !categoryIsJournals(this.value)) {
      jlSelect.style.display = 'none';
    }
  };

  /* Actions */
  var doneButton = document.createElement('button');
  doneButton.textContent = '\u2713';
  doneButton.title = 'Готово';
  doneButton.className = 'done_button';
  doneButton.onclick = function() {
    var tr = this.parentNode.parentNode;
    var groupInd = getGroupInd(tr.rowIndex);
    var localRowNumber = tr.rowIndex - groupStartsInTable[groupInd] - 1;
    saveData(groupInd, localRowNumber);
    disableEditMode(groupInd, localRowNumber);
  };
  actionsTd.appendChild(doneButton);
}

function disableEditMode(groupInd, rowNumber) {
  var rowAbsNumber = groupStartsInTable[groupInd] + rowNumber + 1;
  var row = getTableBody().children[rowAbsNumber];

  var categoriesTd = row.children[0];
  var restrictionsTd = row.children[1];
  var propertyTd = row.children[2];
  var weightTd = row.children[3];

  for (var i = 0; i < row.children.length; ++i) {
    clearNode(row.children[i]);
  }

  var data = rowsData[groupStartsInData[groupInd] + rowNumber];

  var categorySpan = document.createElement('span');
  if (data.category.startsWith('study_work_external') &&
      !extClasses.hasOwnProperty('types')) {
    categorySpan.className = 'educ_type_placeholder';
  }
  categorySpan.textContent = translateQualifier(data.category, true);

  if (data.label && data.label != categorySpan.textContent) {
    categoriesTd.appendChild(document.createTextNode(data.label));
    categoriesTd.appendChild(document.createElement('br'));
    categorySpan.style['font-size'] = 'smaller';
  }
  categoriesTd.appendChild(categorySpan);

  var propertyDiv = document.createElement('div');
  propertyDiv.style['font-weight'] = 'bold';
  propertyDiv.textContent = translateQualifier(data.parameter);
  propertyTd.appendChild(propertyDiv);

  weightTd.appendChild(document.createTextNode(data.weight));

  data.modifiers.forEach(function(modifierArray) {
    if (availableModifiers.hasOwnProperty(modifierArray[0])) {
      var modifier = availableModifiers[modifierArray[0]];
      var displayText = modifier.displayText(modifierArray);
      var myDiv = document.createElement('div');
      myDiv.textContent = displayText;
      propertyTd.appendChild(myDiv);
    }
  });

  if (data.restrictions && data.restrictions.length) {
    var ul = document.createElement('ul');
    data.restrictions.forEach(function(restriction) {
      var li = document.createElement('li');
      var text = translateQualifier(restriction.parameter);
      if (restriction.lower_bound) {
        text = restriction.lower_bound + ' \u2a7d ' + text;
      }
      if (restriction.upper_bound) {
        text += ' \u2a7d ' + restriction.upper_bound;
      }
      if (restriction.value == false) {
        text = 'Не ' + text;
      }
      li.textContent = text;
      ul.appendChild(li);
    });
    restrictionsTd.appendChild(ul);
  }

  if (data.journalList) {
    var jlDiv = document.createElement('div');
    if (journalLists) {
      jlDiv.textContent = 'Журналы только из списка: ' +
                          journalLists[data.journalList].name;
    } else {
      jlDiv.className = 'journal_list_placeholder';
      jlDiv.setAttribute('jlId', data.journalList);
    }
    restrictionsTd.appendChild(jlDiv);
  }

  if (data.educStage) {
    var educStageDiv = document.createElement('div');
    if (extClasses.hasOwnProperty('stages')) {
      educStageDiv.textContent = 'Этап обучения: ' +
                                 extClasses.stages[data.educStage];
    } else {
      educStageDiv.className = 'educ_stage_placeholder';
      educStageDiv.setAttribute('stageId', data.educStage);
    }
    restrictionsTd.appendChild(educStageDiv);
  }

  if (readOnlyMode) {
    return;
  }

  var editButton = document.createElement('button');
  editButton.textContent = '\u270e';
  editButton.title = 'Править';
  editButton.onclick = function() {
    var tr = this.parentNode.parentNode;
    var groupInd = getGroupInd(tr.rowIndex);
    var localRowNumber = tr.rowIndex - groupStartsInTable[groupInd] - 1;
    enableEditMode(groupInd, localRowNumber);
  };

  var deleteButton = document.createElement('button');
  deleteButton.textContent = '\u2717';
  deleteButton.title = 'Удалить';
  deleteButton.onclick = function() {
    showDialog('Удалить это правило?', function() {
      var tr = deleteButton.parentNode.parentNode;
      var groupInd = getGroupInd(tr.rowIndex);
      var localRowNumber = tr.rowIndex - groupStartsInTable[groupInd] - 1;
      var indexInData = groupStartsInData[groupInd] + localRowNumber;

      rowsData.splice(indexInData, 1);
      getTableBody().removeChild(tr);

      for (var i = groupInd + 1; i < groupStartsInData.length; ++i) {
        --groupStartsInData[i];
      }
      for (var i = groupInd + 1; i < groupStartsInTable.length; ++i) {
        --groupStartsInTable[i];
      }
    });
  };

  var moveUpButton = document.createElement('button');
  moveUpButton.textContent = '\u2191';
  moveUpButton.title = 'Поднять';
  moveUpButton.onclick = function() {
    var tr = this.parentNode.parentNode;
    var groupInd = getGroupInd(tr.rowIndex);
    var localRowNumber = tr.rowIndex - groupStartsInTable[groupInd] - 1;

    if (localRowNumber > 0) {
      var indexInData = groupStartsInData[groupInd] + localRowNumber;
      rowsData.swap(indexInData - 1, indexInData);
      var prevTr = tr.parentNode.children[tr.rowIndex - 1];
      tr.parentNode.insertBefore(tr, prevTr);
    }
  };

  var moveDownButton = document.createElement('button');
  moveDownButton.textContent = '\u2193';
  moveDownButton.title = 'Опустить';
  moveDownButton.onclick = function() {
    var tr = this.parentNode.parentNode;
    var groupInd = getGroupInd(tr.rowIndex);
    var localRowNumber = tr.rowIndex - groupStartsInTable[groupInd] - 1;
    var groupEnd = (groupInd + 1 < groupStartsInTable.length) ?
                   groupStartsInTable[groupInd + 1] :
                   tr.parentNode.children.length;

    if (tr.rowIndex < groupEnd - 1) {
      var indexInData = groupStartsInData[groupInd] + localRowNumber;
      rowsData.swap(indexInData, indexInData + 1);
      tr.parentNode.insertBefore(tr.nextSibling, tr);
    }
  };

  var actionsTd = row.children[4];
  actionsTd.appendChild(editButton);
  actionsTd.appendChild(moveUpButton);
  actionsTd.appendChild(moveDownButton);
  actionsTd.appendChild(deleteButton);
}

function addRowAndData(groupInd) {
  var line = {
    'category': '',
    'label': '',
    'restrictions': [],
    'parameter': '',
    'weight': '',
    'modifiers': []
  };
  rowsData.splice(groupStartsInData[groupInd + 1], 0, line);
  for (var i = groupInd + 1; i < groupStartsInData.length; ++i) {
    ++groupStartsInData[i];
  }

  var tr = addRow(groupInd, true);
  var firstSelect = tr.getElementsByClassName('category_select')[0];
  $(firstSelect).focus();
}

function addRow(groupInd, editMode) {
  var tableBody = getTableBody();
  var groupEnd = (groupInd + 1 < groupStartsInTable.length) ?
                 groupStartsInTable[groupInd + 1] :
                 tableBody.children.length;
  var newRowNumber = groupEnd - groupStartsInTable[groupInd] - 1;

  var tr = document.createElement('tr');
  var colsNumber = (readOnlyMode ? 4 : 5);
  for (var i = 0; i < colsNumber; ++i) {
    var td = document.createElement('td');
    tr.appendChild(td);
  }
  if (groupInd + 1 < groupStartsInTable.length) {
    tableBody.insertBefore(tr, tableBody.children[groupStartsInTable[groupInd + 1]]);
    for (var i = groupInd + 1; i < groupStartsInTable.length; ++i) {
      ++groupStartsInTable[i];
    }
  } else {
    tableBody.appendChild(tr);
  }

  if (editMode) {
    enableEditMode(groupInd, newRowNumber);
  } else {
    disableEditMode(groupInd, newRowNumber);
  }
  return tr;
}

function showDiagramWithData(plotData) {
  histogram_plotter.draw(plotData);
}

function saveCallback(data) {
  showDialog('Данные успешно сохранены.');
}

function getDataToSend() {
  var fullRowsData = rowsData.slice();
  for (var i = 0; i < database.category_groups.length; ++i) {
    if (groupLimits[i]) {
      fullRowsData.push(groupLimits[i]);
    }
  }

  return {
    title: document.getElementById('title_input').value,
    rowsData: JSON.stringify(fullRowsData),
    startYear: document.getElementById('startyear').value,
    endYear: document.getElementById('endyear').value
  };
}

function saveAllRows() {
  var doneButtons = getTableBody().getElementsByClassName('done_button');
  while (doneButtons.length) {
    doneButtons[0].click();
  }
}

function submit() {
  $('#histogram_area').show('slow');
  saveAllRows();

  var submitButton = document.getElementById('submit_button');
  submitButton.textContent = 'Обновить гистограмму';
  submitButton.disabled = true;

  var dataToSend = getDataToSend();
  dataToSend.positions = JSON.stringify(selectedPositions);

  $.ajax({
    url: '/pmodel/submitdata/hierarchical/',
    async: true,
    type: 'POST',
    data: dataToSend,
    success: showDiagramWithData,
    complete: function(jqXHR, textStatus) {
      submitButton.disabled = false;
    }
  });
}


function save() {
  saveAllRows();

  var requesturl = document.location.pathname.replace(pageType, 'savedata');
  $.ajax({
    url: requesturl,
    async: true,
    type: 'POST',
    data: getDataToSend(),
    success: saveCallback
  }).fail(function(jqXHR) {
    if (jqXHR.status == 500) {
      showDialog('Произошла внутренняя ошибка сервера.');
    } else {
      showDialog(jqXHR.responseText);
    }
  });
}
