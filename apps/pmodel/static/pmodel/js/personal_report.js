/* Functions specific to the personal report page.
 *
 * Author: Dmitry Shachnev <mitya57@gmail.com>, 2015.
 */

function initializeToolTips() {
  /* Get the formula ID from the path */
  var pathComponents = document.location.pathname.split('/');
  var formulaId = null;
  for (var i = 0; i < pathComponents.length; ++i) {
    if (pathComponents[i] == 'run') {
      formulaId = pathComponents[i + 1];
      break;
    }
  }

  /* Send a request to get formula JSON data */
  var requestUrl = '/pmodel/getdata/' + formulaId + '/';
  $.get(requestUrl, requestCallback, 'json');
}

function requestCallback(data, textStatus, jqXHR) {
  var headers = document.getElementsByClassName('section_header');
  var jsondata = JSON.parse(data.jsondata);

  for (var i = 0; i < headers.length; ++i) {
    var tr = headers[i];
    var lineNumber = parseInt(tr.id.split('_')[2]);
    var lineJson = jsondata[lineNumber];
    var lineString = 'Категория: ' +
                     translateQualifier(lineJson.category) + '\n' +
                     'Свойство: ' +
                     translateQualifier(lineJson.parameter);
    if (lineJson.restrictions) {
      for (var j = 0; j < lineJson.restrictions.length; ++j) {
        var restriction = lineJson.restrictions[j];
        var text = translateQualifier(restriction.parameter);
        if (restriction.lower_bound) {
          text = restriction.lower_bound + ' \u2a7d ' + text;
        }
        if (restriction.upper_bound) {
          text += ' \u2a7d ' + restriction.upper_bound;
        }
        if (restriction.value == false) {
          text = 'Не ' + text;
        }
        lineString += '\nОграничение: ' + text;
      }
    }
    for (var j = 0; j < lineJson.modifiers.length; ++j) {
      var modifierArray = lineJson.modifiers[j];
      var modifier = availableModifiers[modifierArray[0]];
      var text = modifier.displayText(modifierArray);
      lineString += '\nМодификатор: ' + text;
    }
    lineString += '\nВес: ' + lineJson.weight;

    var td = tr.children[0];
    td.title = lineString;
  }
}
