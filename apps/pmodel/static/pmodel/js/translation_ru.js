/* Russian translation.
 *
 * Author: Dmitry Shachnev <mitya57@gmail.com>, 2014.
 */

locales['ru'] = {
    'tokens': {
        '': '(пусто)',
        '4+-authors': 'более 3 авторов',
        'academic_hours': 'Акад. часов',
        'age': 'Возраст',
        'articles': 'Статьи',
        'author_order': 'Порядковый номер автора',
        'budget_included': 'Включая госбюджетные темы',
        'budget_only': 'Только госбюджетные темы',
        'budget_students_count': 'Число бюджетных студентов',
        'candidate': 'Кандидатская',
        'chapter': 'Глава',
        'citations': 'Цитирование',
        'coauthors_count': 'Количество соавторов',
        'collections': 'Сборники',
        'committee': 'Комиссия или совет',
        'conference_materials': 'Тезисы конференции',
        'conferences': 'Участие в конференции',
        'conference': 'Конференция',
        'confirmed_only': 'Только подтвержденные',
        'conf_org_head': 'Председатель орг. комитета',
        'conf_org_member': 'Член орг. комитета',
        'conf_pc_head': 'Председатель прогр. комитета',
        'conf_pc_member': 'Член прогр. комитета',
        'conf_speaker': 'Докладчик',
        'conf_talk_invited': 'Приглашенный доклад',
        'conf_talk_oral': 'Устный доклад',
        'conf_talk_plenar': 'Пленарный доклад',
        'conf_talk_poster': 'Стендовый доклад',
        'count': 'Количество',
        'coursework': 'Курсовая',
        'diploma_bacalaur': 'Бакалавр',
        'diploma_magister': 'Магистр',
        'diploma_specialist': 'Специалист',
        'diploma': 'Дипломная работа',
        'discouncil': 'Диссертационный совет',
        'dis_deputy_leader': 'Заместитель председателя',
        'dis_leader': 'Председатель совета',
        'dis_secretary': 'Ученый секретарь',
        'dissertations': 'Защищённые диссертации',
        'doctor': 'Докторская',
        'duration': 'Срок',
        'editorial_collections': 'Редколлегия сборника',
        'editorial_journals': 'Редколлегия журнала',
        'for_all_years': 'За все время',
        'for_period': 'За период',
        'guidance': 'Руководство',
        'hak': 'ВАК',
        'half_year': '½ года',
        'impact_factor': 'Импакт-фактор',
        'indexed_by_scopus': 'Индексируется в Scopus',
        'indexed_by_wos_or_scopus': 'Индексируется в WoS/Scopus',
        'indexed_by_wos': 'Индексируется в WoS',
        'international': 'Международные',
        'isi': 'ISI',
        'journals': 'Журналы',
        'last_year_only': 'Только за последний год',
        'leader': 'Руководитель',
        'lectures_course': 'Курс лекций',
        'lectures_main': 'Базовый курс',
        'lectures_misc': 'С/к, вариативн,...',
        'membership': 'Членство в',
        'misc': 'Прочее',
        'money': 'Финансирование',
        'monographs': 'Монографии',
        'pages_count': 'Количество страниц',
        'paid_students_count': 'Число платных студентов',
        'patents': 'Патенты',
        'practics': 'Практикум',
        'projects': 'НИР',
        'projmember': 'Участник',
        'redpages_count': 'Печатных листов',
        'reports': 'Отчёты',
        'rsci': 'РИНЦ',
        'russian_intjournal': 'Журнал РФ',
        'russian': 'В России',
        'school_book': 'Пособие',
        'scientific_work': 'Научная работа',
        'seminars': 'Семинары',
        'sem_labs': 'Лабораторная',
        'software': 'На программное обеспечение',
        'standings': 'Позиция',
        'study_work': 'Учебная работа',
        'study_work_external': 'Учебная работа (импортированные данные)',
        'study_semester': 'Номер семестра',
        'textbooks': 'Учебники',
        'textbook': 'Учебник',
        'total_hours_count': 'Число часов в курсе',
        'traineeships': 'Стажировки',
        'uchmetlit': 'Учебно-методич. литература',
        'upto-3-authors': 'до 3 авторов',
        'vak_journal': 'журнал из списка ВАК',
        'weeks_count': 'Число недель',
        'works_count': 'Число работ',
        'WoS': 'Web of Science',
        'year': '1 год'
    },
    'descriptions': {
        'study_work_external': 'Эта часть формулы считается по данным, полученным из АИС ' +
                               '«Педагогическая нагрузка».',
    },
    'help': {
        'Ограничения':  'Ограничения на свойства объектов, которые будут учитываться в данной строке. ' +
                        'Например, можно указать, что «число авторов статьи меньше 5». ' +
                        'Это позволяет присвоить вес в зависимости от свойств объекта. ' +
                        'Отсутствие верхней или нижней границы означает + или − бесконечность.'
    }
};

function getPluralForm(n) {
  if (n % 10 == 1 && n % 100 != 11) {
    return 0;
  }
  if (n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)) {
    return 1;
  }
  return 2;
}
