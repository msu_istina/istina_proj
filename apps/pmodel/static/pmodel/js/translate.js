/* A library for strings translation.
 * Include individual translations files after this one.
 *
 * Author: Dmitry Shachnev <mitya57@gmail.com>, 2014
 */

var locales = {};

function loadDictionary(dictionaryName) {
  var language = window.navigator.language;
  if (!locales.hasOwnProperty(language)) {
    if (locales.hasOwnProperty('ru')) {
      language = 'ru';
    } else {
      return {};
    }
  }
  return locales[language][dictionaryName];
}

function getDescription(string) {
  // Should we fall back to parent categories?
  var dictionary = loadDictionary('descriptions');
  if (dictionary.hasOwnProperty(string)) {
    return dictionary[string];
  }
}

function getHelp(string) {
  var dictionary = loadDictionary('help');
  if (dictionary.hasOwnProperty(string)) {
    return dictionary[string];
  }
}

function translateToken(token) {
  var dictionary = loadDictionary('tokens');
  if (dictionary.hasOwnProperty(token)) {
    return dictionary[token];
  }
  var match = token.match(/custom(\d+)/);
  if (match !== null && customCategories !== undefined) {
    for (var i = 0; i < customCategories.length; ++i) {
      if (customCategories[i].id == match[1] && customCategories[i].name) {
        return customCategories[i].name;
      }
    }
  }
  match = token.match(/type(\d+)/);
  if (match !== null && typeof extClasses != 'undefined' &&
      extClasses.hasOwnProperty('types')) {
    return capitalizeFirstLetter(extClasses.types[parseInt(match[1])]);
  }
  return capitalizeFirstLetter(token);
}

function translateQualifier(qualifier, strip) {
  var separator = ' \u2192 ';
  var tokens = qualifier.split('.');
  if (strip === true && database.strip_first_token.contains(tokens[0])) {
    tokens.splice(0, 1);
  }
  return tokens.map(translateToken).join(separator);
}
