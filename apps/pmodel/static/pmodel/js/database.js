/* Main file containing possible categories and parameters.
 * Please keep sorted and use English names where possible.
 *
 * Originally put together by Dmitry Shachnev <mitya57@gmail.com>.
 */

var database = {
    'categories': [
        'articles',
        'articles.collections',
        'articles.conference_materials',
        'articles.conference_materials.international',
        'articles.conference_materials.russian',
        'articles.journals.international',
        'articles.journals.international.Scopus',
        'articles.journals.international.WoS',
        'articles.journals.misc',
        'articles.journals.russian.hak',
        'articles.journals.russian.misc',
        'articles.journals.top25',
        'citations.for_all_years',
        'citations.for_period',
        'citations.hirsh.Scopus',
        'citations.hirsh.WOS',
        'conferences',
        'conferences.international',
        'conferences.russian',
        'dissertations.candidate',
        'dissertations.doctor',
        'membership.conference',
        'membership.discouncil',
        'membership.editorial_collections',
        'membership.editorial_journals',
        'monographs',
        'monographs.4+-authors',
        'monographs.chapter',
        'monographs.russian',
        'monographs.upto-3-authors',
        'patents',
        'patents.software',
        'projects',
        'projects.leader',
        'projects.projmember',
        'reports',
        'study_work.guidance.candidate',
        'study_work.guidance.coursework',
        'study_work.guidance.diploma',
        'study_work.guidance.doctor',
        'study_work.lectures_course.half_year',
        'study_work.lectures_course.lectures_main',
        'study_work.lectures_course.lectures_misc',
        'study_work.lectures_course.year',
        'study_work.practics',
        'study_work.seminars',
        'study_work.sem_labs',
        'textbooks',
        'textbooks.school_book',
        'textbooks.textbook',
        'textbooks.uchmetlit',
        'traineeships',
    ],
    'renamed_categories': {
        /* Categories that are renamed, with their new names. Old names will
         * be not shown in the interface, and replaced with their new names
         * automatically, if possible.
         *
         * Categories in this list should not be included in the above list.
         */
        'editorials.journals': 'membership.editorial_journals',
        'membership.journal': 'membership.editorial_journals',
        'membership.journals': 'membership.editorial_journals',
        'membership.committee': 'membership.conference',
    },
    'category_groups': [
        {
            'name': 'scientific_work',
            'included_categories': [
                'articles',
                'citations',
                'conferences',
                'dissertations',
                'monographs',
                'patents',
                'reports',
                'textbooks',
            ]
        }, {
            'name': 'study_work',
            'included_categories': [
                'study_work',
            ]
        }, {
            'name': 'study_work_external',
            'included_categories': [
                'study_work_external',
            ]
        }, {
            'name': 'money',
            'included_categories': [
                'projects',
            ]
        }, {
            'name': 'misc',
            'included_categories': [
                'membership',
                'traineeships',
            ]
        }
    ],
    'strip_first_token': [
        'study_work',
        'study_work_external',
    ],
    'parameters': {
        '': [
            'coauthors_count.only_restrictions',
            'count.only_property',
        ],
        'articles.collections': [
            'pages_count',
            'redpages_count',
            'indexed_by_wos.only_restrictions.boolean',
            'indexed_by_scopus.only_restrictions.boolean',
            'indexed_by_wos_or_scopus.only_restrictions.boolean',
            'confirmed_only.only_restrictions.boolean'
        ],
        'articles.journals': [
            'impact_factor.isi',
            'impact_factor.scopus',
            'standings.isi',
            'standings.scopus',
            'author_order.only_restrictions',
            'pages_count',
            'redpages_count'
        ],
        'articles.journals.russian': [
            'impact_factor.rsci'
        ],
        'articles.journals.international': [
            'russian_intjournal.only_restrictions.boolean'
        ],
        'citations.for_period': [
            'WOS',
            'Scopus',
        ],
        'citations.for_all_years': [
            'WOS',
            'Scopus',
        ],
        'conferences': [
            'conf_talk_poster.only_restrictions.boolean',
            'conf_talk_oral.only_restrictions.boolean',
            'conf_talk_plenar.only_restrictions.boolean',
            'conf_talk_invited.only_restrictions.boolean',
            'conf_speaker.only_restrictions.boolean'
        ],
        'membership.conference': [
            'international.only_property',
            'international.only_restrictions.boolean',
            'conf_pc_member.only_restrictions.boolean',
            'conf_pc_head.only_restrictions.boolean',
            'conf_org_member.only_restrictions.boolean',
            'conf_org_head.only_restrictions.boolean'
        ],
        'membership.discouncil': [
            'dis_leader.only_restrictions.boolean',
            'dis_deputy_leader.only_restrictions.boolean',
            'dis_secretary.only_restrictions.boolean'
        ],
        'membership.editorial_journals': [
            'impact_factor.isi',
            'impact_factor.rsci',
            'vak_journal.only_restrictions.boolean'
        ],
        'membership.editorial_collections': [
            'pages_count',
            'confirmed_only.only_restrictions.boolean'
        ],
        'study_work.guidance.diploma': [
            'diploma_specialist.only_restrictions.boolean',
            'diploma_magister.only_restrictions.boolean',
            'diploma_bacalaur.only_restrictions.boolean'
        ],
        'study_work.lectures_course': [
            'academic_hours.only_property'
        ],
        'study_work.seminars': [
            'academic_hours.only_property'
        ],
        'study_work.practics': [
            'academic_hours.only_property'
        ],
        'study_work.sem_labs': [
            'academic_hours.only_property'
        ],
        'study_work_external': [
            'study_semester.only_restrictions',   /* F_EDUCWORK_SEMESTER (varies from 1 to 12) */
            'weeks_count',                        /* F_EDUCWORK_WEEK */
            'total_hours_count',                  /* F_EDUCWORK_HOUR × F_EDUCWORK_WEEK */
            'budget_students_count',              /* F_EDUCWORK_STUDBUDJET */
            'paid_students_count',                /* F_EDUCWORK_STUDPAID */
            'works_count',                        /* F_EDUCWORK_WORKCOUNT */
        ],
        'monographs': [
            'pages_count',
            'redpages_count'
        ],
        'textbooks': [
            'pages_count',
            'redpages_count'
        ],
        'traineeships': [
            'duration.only_property'
        ],
        'projects': [
            'money',
            'budget_only.only_restrictions.boolean',
            'budget_included.only_restrictions.boolean',
            'last_year_only.only_restrictions.boolean'
        ],
    }
};

function categoryIsJournals(category) {
  return category.startsWith('articles.journals') ||
         category.startsWith('membership.editorial_journals');
}
