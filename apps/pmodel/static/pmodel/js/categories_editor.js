/* Categories editor.
 *
 * Author: Dmitry Shachnev <mitya57@gmail.com>, 2015.
 */

var categoriesData = null;

function loadCategoriesList() {
    var row = document.createElement('tr');
    var headers = ['Название', 'Создана', 'Изменена', 'Действия'];
    headers.forEach(function(label) {
        var th = document.createElement('th');
        th.textContent = label;
        row.appendChild(th);
    });
    getTableBody().appendChild(row);

    $.get('/pmodel/listcategories/', onListReceived, 'json');

    var addButton = document.createElement('button');
    addButton.textContent = 'Создать новую категорию';
    addButton.onclick = function() {
        $.get('/pmodel/savecategory/', onNewCategoryCreated, 'json');
    };
    document.getElementById('table_wrapper').appendChild(addButton);
}

function getTableBody() {
    var table = document.getElementById('entries_table');
    return table.getElementsByTagName('tbody')[0];
}

function enableEditMode(index, editEnabled) {
    var tbody = getTableBody();
    var tr = tbody.children[2 * index + 1];
    var trEdit = tbody.children[2 * index + 2];
    var editButton = tr.getElementsByClassName('edit_button')[0];
    var doneButton = tr.getElementsByClassName('done_button')[0];

    var shownInEdit = editEnabled ? '' : 'none';
    var shownInView = editEnabled ? 'none' : '';

    editButton.style.display = shownInView;
    doneButton.style.display = shownInEdit;
    trEdit.style.display = shownInEdit;
}

function fillEditorWithData(index, data) {
    var trEdit = getTableBody().children[2 * index + 2];

    if (data.hasOwnProperty('name')) {
        var nameInput = trEdit.getElementsByClassName('name_input')[0];
        nameInput.value = data.name;
    }
    if (data.hasOwnProperty('title')) {
        var titleInput = trEdit.getElementsByClassName('title_input')[0];
        titleInput.value = data.title;
    }
    if (data.hasOwnProperty('basecategory')) {
        var baseSelect = trEdit.getElementsByClassName('base_select')[0];
        baseSelect.value = data.basecategory;
    }
    if (data.hasOwnProperty('jsondata')) {
        clearNode(trEdit.getElementsByClassName('restrictions_ul')[0]);
        JSON.parse(data.jsondata).forEach(function(restriction) {
            if (restriction.hasOwnProperty('value')) {
                addRestriction(index,
                               restriction.parameter,
                               restriction.value);
            } else {
                addRestriction(index,
                               restriction.parameter,
                               restriction.lower_bound,
                               restriction.upper_bound);
            }
        });
    }
}

function saveData(index) {
    var tr = getTableBody().children[2 * index + 1];
    var trEdit = getTableBody().children[2 * index + 2];

    var restrictions = [];
    var lis = trEdit.getElementsByTagName('li');
    for (var i = 0; i < lis.length; ++i) {
        var li = lis[i];
        var param = li.getElementsByClassName('parameter_select')[0].value;
        var leftInput = li.getElementsByClassName('left_input')[0];
        if (leftInput.type == 'checkbox') {
            restrictions.push({
                parameter: param,
                value: leftInput.checked
            });
        } else {
            var rightInput = li.getElementsByClassName('right_input')[0];
            restrictions.push({
                parameter: param,
                lower_bound: leftInput.value,
                upper_bound: rightInput.value
            });
        }
    }

    var newData = {
        basecategory: trEdit.getElementsByClassName('base_select')[0].value,
        name: trEdit.getElementsByClassName('name_input')[0].value,
        title: trEdit.getElementsByClassName('title_input')[0].value,
        jsonData: JSON.stringify(restrictions)
    };

    updateNameTd(tr.children[0], newData);

    $.ajax({
        url: '/pmodel/savecategory/' + categoriesData[index].id + '/',
        type: 'POST',
        data: newData
    }).done(function(data) {
        updateCreatedTd(tr.children[1], data);
        updateModifiedTd(tr.children[2], data);
    });
}

function updateNameTd(td, tddata) {
    clearNode(td);

    var div1 = document.createElement('div');
    div1.textContent = tddata.title ? tddata.title : 'Нет заголовка';
    td.appendChild(div1);

    if (tddata.hasOwnProperty('basecategory')) {
        var div2 = document.createElement('div');
        div2.style['font-size'] = 'smaller';
        div2.textContent = tddata.basecategory;
        td.appendChild(div2);
    }
}

function updateCreatedTd(td, tddata) {
    td.textContent = (tddata.creationdate || 'В неизвестное время') +
        ' пользователем ' + tddata.createdby;
}

function updateModifiedTd(td, tddata) {
    td.textContent = tddata.modificationdate || 'В неизвестное время';
}

function addRestriction(index, parameter, leftval, rightval) {
    var tbody = getTableBody();
    var trEdit = tbody.children[2 * index + 2];
    var ul = trEdit.getElementsByClassName('restrictions_ul')[0];
    var li = document.createElement('li');
    var isBoolean = (typeof leftval == 'boolean');
    if (isBoolean) {
        li.className = 'boolean_restriction';
    }

    var leftInput = document.createElement('input');
    leftInput.className = 'left_input';
    if (isBoolean) {
        leftInput.type = 'checkbox';
        leftInput.checked = leftval;
    } else {
        leftInput.style.width = '50px';
        if (leftval !== undefined) {
            leftInput.value = leftval;
        }

        var rightInput = document.createElement('input');
        rightInput.className = 'right_input';
        rightInput.style.width = '50px';
        if (rightval !== undefined) {
            rightInput.value = rightval;
        }
    }

    var categorySelect = trEdit.getElementsByClassName('base_select')[0];
    var select = document.createElement('select');
    select.className = 'parameter_select';
    selectCategory(categorySelect.value, select, 'restrictions', isBoolean);
    if (parameter) {
        select.value = parameter;
    }

    var removeButton = document.createElement('button');
    removeButton.textContent = '\u2212';
    removeButton.title = 'Удалить';
    removeButton.onclick = function() {
        ul.removeChild(li);
    };
    removeButton.style['margin-left'] = '10px';

    li.appendChild(leftInput);
    if (!isBoolean) {
        li.appendChild(document.createTextNode('\u2a7d'));
    }
    li.appendChild(select);
    if (!isBoolean) {
        li.appendChild(document.createTextNode('\u2a7d'));
        li.appendChild(rightInput);
    }
    li.appendChild(removeButton);
    ul.appendChild(li);
}

function addRow(index, data) {
    var tbody = getTableBody();
    var tr = document.createElement('tr');
    var trEdit = document.createElement('tr');
    var tdEditor = document.createElement('td');
    tdEditor.className = 'category_editor';

    var td1 = document.createElement('td');
    updateNameTd(td1, data);
    var td2 = document.createElement('td');
    updateCreatedTd(td2, data);
    var td3 = document.createElement('td');
    updateModifiedTd(td3, data);

    var editButton = document.createElement('button');
    var deleteButton = document.createElement('button');
    var doneButton = document.createElement('button');

    editButton.textContent = '\u270e';
    editButton.title = 'Править';
    editButton.className = 'edit_button';
    editButton.onclick = function() {
        $.get('/pmodel/getcategory/' + data.id + '/',
              function(data) {
                  fillEditorWithData(index, data);
              },
              'json');
        enableEditMode(index, true);
    };
    doneButton.textContent = '\u2713';
    doneButton.title = 'Готово';
    doneButton.className = 'done_button';
    doneButton.onclick = function() {
        enableEditMode(index, false);
        saveData(index);
    };
    doneButton.style.display = 'none';
    deleteButton.textContent = '\u2717';
    deleteButton.title = 'Удалить';
    deleteButton.onclick = function() {
        showDialog('Удалить эту категорию?', function() {
            $.get('/pmodel/deletecategory/' + data.id + '/');
            /* Hide rows instead of deleting them: indices should not change */
            tr.style.display = 'none';
            trEdit.style.display = 'none';
        });
    };
    var td4 = document.createElement('td');
    td4.appendChild(editButton);
    td4.appendChild(doneButton);
    td4.appendChild(deleteButton);

    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);

    tdEditor.colSpan = 4;

    var createDiv = function(description, node) {
        var div = document.createElement('div');
        var span = document.createElement('span');
        span.textContent = description;
        span.style['margin-right'] = '4px';
        div.appendChild(span);
        div.appendChild(node);
        return div;
    };

    var nameInput = document.createElement('input');
    nameInput.className = 'name_input';
    tdEditor.appendChild(createDiv('Короткое имя:', nameInput));

    var titleInput = document.createElement('input');
    titleInput.className = 'title_input';
    tdEditor.appendChild(createDiv('Описание:', titleInput));

    var select = document.createElement('select');
    select.className = 'base_select';
    fillCategorySelect(select);
    tdEditor.appendChild(createDiv('Базовая категория:', select));

    var div = document.createElement('div');
    div.style['margin-top'] = '5px';
    div.appendChild(document.createTextNode('Ограничения:'));
    var ul = document.createElement('ul');
    ul.className = 'restrictions_ul';
    div.appendChild(ul);

    var newRestrictionButton = document.createElement('button');
    newRestrictionButton.textContent = 'Новое ограничение';
    newRestrictionButton.onclick = function() {
        addRestriction(index);
    };
    div.appendChild(newRestrictionButton);

    var newBooleanRestrictionButton = document.createElement('button');
    newBooleanRestrictionButton.textContent = 'Новое ограничение типа да/нет';
    newBooleanRestrictionButton.onclick = function() {
        addRestriction(index, null, false);
    };
    newBooleanRestrictionButton.disabled =
        !getPossibleParameters(select.value, 'restrictions', true).length;
    div.appendChild(newBooleanRestrictionButton);

    select.onchange = function() {
        var lis = tdEditor.getElementsByTagName('li');
        for (var i = 0; i < lis.length; ++i) {
            var isBoolean = (lis[i].className == 'boolean_restriction');
            var restrictSelect = lis[i].getElementsByClassName('parameter_select')[0];
            selectCategory(this.value, restrictSelect, 'restrictions', isBoolean);
        }
        newBooleanRestrictionButton.disabled =
            !getPossibleParameters(select.value, 'restrictions', true).length;
    };

    var saveButton = document.createElement('button');
    saveButton.textContent = 'Сохранить';
    saveButton.onclick = function() {
        enableEditMode(index, false);
        saveData(index);
    };
    div.appendChild(saveButton);

    tdEditor.appendChild(div);

    trEdit.style.display = 'none';
    trEdit.appendChild(tdEditor);

    tbody.appendChild(tr);
    tbody.appendChild(trEdit);
}

function onListReceived(data, textStatus, jqXHR) {
    if (jqXHR.status == 200) {
        categoriesData = data;
        for (var i = 0; i < data.length; ++i) {
            addRow(i, data[i]);
        }
    } else {
        showDialog(textStatus);
    }
}

function onNewCategoryCreated(data, textStatus, jqXHR) {
    if (jqXHR.status == 200) {
        categoriesData.push(data);
        addRow(categoriesData.length - 1, data);
    } else {
        showDialog(textStatus);
    }
}
