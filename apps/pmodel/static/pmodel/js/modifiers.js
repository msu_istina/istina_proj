/* Modifiers definitions.
 *
 * Author: Dmitry Shachnev <mitya57@gmail.com>, 2014.
 */

function isGoodFor(modifier, category) {
  if (!modifier.hasOwnProperty('appliesTo')) {
    return true;
  }
  var tokens = category.split('.');
  for (var a = 0; a <= tokens.length; ++a) {
    var subcategory = tokens.slice(0, a).join('.');
    if (modifier.appliesTo.indexOf(subcategory) != -1) {
      return true;
    }
  }
  return false;
}

function createTextModifier(title) {
  return {
    title: title,
    create: function(row, modifierArray) {
      var myDiv = document.createElement('div');
      myDiv.textContent = title;
      var modifiersDiv = row.getElementsByClassName('modifiers_area')[0];
      modifiersDiv.appendChild(myDiv);
    },
    displayText: function(modifierArray) {
      return title;
    }
  };
}

var divideByCoauthorsCount = createTextModifier(
  'Разделить на число соавторов'
);
divideByCoauthorsCount.appliesTo = ['articles', 'projects', 'monographs', 'conferences', 'textbooks', 'patents', 'reports', 'study_work.guidance'];

var divideByOrgCoauthorsCount = createTextModifier(
  'Разд. на число соавторов из МГУ'
);
divideByOrgCoauthorsCount.appliesTo = ['articles'];

var divideBySqrtOfCoauthorsCount = createTextModifier(
  'Разделить на корень из числа соавторов'
);
divideBySqrtOfCoauthorsCount.appliesTo = ['articles', 'projects', 'monographs', 'conferences', 'textbooks', 'patents', 'reports', 'study_work.guidance'];

var divideByCoeditorsCount = createTextModifier(
  'Разделить на число соредакторов'
);
divideByCoeditorsCount.appliesTo = ['membership.editorial_collections'];



var changeParameterModifier = {
  appliesTo: [],
  title: 'Сдвинуть параметр до границы',
  create: function(row, modifierArray) {
    var categorySelect = row.getElementsByClassName('category_select')[0];

    if (modifierArray.length < 2) {
      var parameterSelect = row.getElementsByClassName('parameter_select')[0];
      modifierArray.push(parameterSelect.value);
    }

    var rowNumber = row.rowIndex - 1;
    var newSelect = document.createElement('select');
    newSelect.className = 'modifier_select';
    selectCategory(categorySelect.value, newSelect);
    newSelect.value = modifierArray[1];
    newSelect.onchange = function() {
      modifierArray[1] = this.value;
    };

    var leftInput = document.createElement('input');
    leftInput.className = 'modifier_small_input';
    leftInput.type = 'text';
    leftInput.pattern = '[\\d\.]*';
    if (modifierArray[2]) {
      leftInput.value = modifierArray[2];
    }

    var rightInput = document.createElement('input');
    rightInput.className = 'modifier_small_input';
    rightInput.type = 'text';
    rightInput.pattern = '[\\d\.]*';
    if (modifierArray[3]) {
      rightInput.value = modifierArray[3];
    }

    leftInput.onchange = function() {
      modifierArray[2] = this.value;
    };
    rightInput.onchange = function() {
      modifierArray[3] = this.value;
    };

    var myDiv = document.createElement('div');
    myDiv.appendChild(leftInput);
    myDiv.appendChild(document.createTextNode('\u2a7d'));
    myDiv.appendChild(newSelect);
    myDiv.appendChild(document.createTextNode('\u2a7d'));
    myDiv.appendChild(rightInput);

    var modifiersDiv = row.getElementsByClassName('modifiers_area')[0];
    modifiersDiv.appendChild(myDiv);
  },
  displayText: function(modifierArray) {
    var name = translateQualifier(modifierArray[1]);
    text = this.title;
    if (modifierArray[2]) {
      text = '{NAME} = MIN ({NAME}, {VALUE})'
             .replace(/{NAME}/g, translateQualifier(modifierArray[1]))
             .replace(/{VALUE}/, modifierArray[2]);
    }
    if (modifierArray[3]) {
      text = '{NAME} = MAX ({NAME}, {VALUE})'
             .replace(/{NAME}/g, translateQualifier(modifierArray[1]))
             .replace(/{VALUE}/, modifierArray[3]);
    }
    return text;
  }
};

var limitUnitModifier = {
  title: 'Ограничить балл за 1',
  create: function(row, modifierArray) {
    var categorySelect = row.getElementsByClassName('category_select')[0];

    var leftInput = document.createElement('input');
    leftInput.className = 'modifier_small_input';
    leftInput.type = 'text';
    leftInput.pattern = '[\\d\.]*';
    if (modifierArray[1]) {
      leftInput.value = modifierArray[1];
    }

    var rightInput = document.createElement('input');
    rightInput.className = 'modifier_small_input';
    rightInput.type = 'text';
    rightInput.pattern = '[\\d\.]*';
    if (modifierArray[2]) {
      rightInput.value = modifierArray[2];
    }

    leftInput.onchange = function() {
      modifierArray[1] = this.value;
    };
    rightInput.onchange = function() {
      modifierArray[2] = this.value;
    };

    var myDiv = document.createElement('div');
    myDiv.appendChild(leftInput);
    myDiv.appendChild(document.createTextNode(' \u2a7d балл за 1 \u2a7d '));
    myDiv.appendChild(rightInput);

    var modifiersDiv = row.getElementsByClassName('modifiers_area')[0];
    modifiersDiv.appendChild(myDiv);
  },
  displayText: function(modifierArray) {
    text = 'балл за 1';
    if (modifierArray[1]) {
      text = modifierArray[1] + ' \u2a7d ' + text;
    }
    if (modifierArray[2]) {
      text += ' \u2a7d ' + modifierArray[2];
    }
    return text;
  }
};

var limitSumModifier = {
  title: 'Ограничить суммарный балл',
  create: function(row, modifierArray) {
    var categorySelect = row.getElementsByClassName('category_select')[0];

    var leftInput = document.createElement('input');
    leftInput.className = 'modifier_small_input';
    leftInput.type = 'text';
    leftInput.pattern = '[\\d\.]*';
    if (modifierArray[1]) {
      leftInput.value = modifierArray[1];
    }

    var rightInput = document.createElement('input');
    rightInput.className = 'modifier_small_input';
    rightInput.type = 'text';
    rightInput.pattern = '[\\d\.]*';
    if (modifierArray[2]) {
      rightInput.value = modifierArray[2];
    }

    leftInput.onchange = function() {
      modifierArray[1] = this.value;
    };
    rightInput.onchange = function() {
      modifierArray[2] = this.value;
    };

    var myDiv = document.createElement('div');
    myDiv.appendChild(leftInput);
    myDiv.appendChild(document.createTextNode(' \u2a7d сумма \u2a7d '));
    myDiv.appendChild(rightInput);

    var modifiersDiv = row.getElementsByClassName('modifiers_area')[0];
    modifiersDiv.appendChild(myDiv);
  },
  displayText: function(modifierArray) {
    text = 'сумма';
    if (modifierArray[1]) {
      text = modifierArray[1] + ' \u2a7d ' + text;
    }
    if (modifierArray[2]) {
      text += ' \u2a7d ' + modifierArray[2];
    }
    return text;
  }
};

var upperBoundModifier = {
  appliesTo: [],
  title: 'Ограничить сверху',
  create: function(row, modifierArray) {
    var input = document.createElement('input');
    input.className = 'modifier_small_input';
    input.type = 'text';
    input.pattern = '[\\d\.]*';
    if (modifierArray[1]) {
      input.value = modifierArray[1];
    }
    input.onchange = function() {
      modifierArray[1] = this.value;
    };
    var myDiv = document.createElement('div');
    myDiv.appendChild(document.createTextNode('Ограничить величиной:'));
    myDiv.appendChild(input);
    var modifiersDiv = row.getElementsByClassName('modifiers_area')[0];
    modifiersDiv.appendChild(myDiv);
  },
  displayText: function(modifierArray) {
    return 'Ограничить величиной ' +  modifierArray[1];
  }
};

var logModifier = {
  title: 'Взять логарифм',
  create: function(row, modifierArray) {
    var input = document.createElement('input');
    input.className = 'modifier_small_input';
    input.type = 'text';
    input.pattern = '[\\d\.]*';
    if (modifierArray[1]) {
      input.value = modifierArray[1];
    }
    input.onchange = function() {
      modifierArray[1] = this.value;
    };
    var myDiv = document.createElement('div');
    myDiv.appendChild(document.createTextNode('Основание логарифма:'));
    myDiv.appendChild(input);
    var modifiersDiv = row.getElementsByClassName('modifiers_area')[0];
    modifiersDiv.appendChild(myDiv);
  },
  displayText: function(modifierArray) {
    if (!modifierArray[1]) {
      return 'Взять натуральный логарифм';
    }
    return 'Взять логарифм с основанием ' + modifierArray[1];
  }
};

var availableModifiers = {
  'divide_coauthors': divideByCoauthorsCount,
  'divide_org_coauthors':  divideByOrgCoauthorsCount,
  'divide_coauthors_sqrt': divideBySqrtOfCoauthorsCount,
  'divide_coeditors': divideByCoeditorsCount,
  'upper_bound': upperBoundModifier,
  'limit_unit': limitUnitModifier,
  'limit_sum': limitSumModifier,
  // 'log': logModifier,
  'change_parameter': changeParameterModifier
};


function getPossibleModifiers(category) {
    var result = [];
    for (var modifier in availableModifiers) {
        var modifierObj = availableModifiers[modifier];
        if (isGoodFor(modifierObj, category)) {
            result.push(modifierObj);
        }
    }
    return result;
}
