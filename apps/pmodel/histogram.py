import math

def build_histogram(values, nbins=10, q=None, qbins=None):
    "Build a histogram with NBINS bins over list of VALUES."
    n_samples = len(values)
    if n_samples == 0:
        return []
    histogram = {}
    min_value = min(values)
    max_value = max(values)
    sorted_values = sorted(values)
    if q:
        q_index = int(math.floor(n_samples * q))
        return build_histogram(sorted_values[:q_index], nbins) \
               + build_histogram(sorted_values[q_index:], qbins or int(math.ceil(math.sqrt(nbins))))
    #avg = sum(values)/len(values)
    #sigma = (sum([(v-avg)**2 for v in values])/len(values)) ** 0.5

    binned_histogram = {k: 0 for k in range(nbins)}
    values_range = (max_value-min_value)
    if values_range == 0:
        values_range = 1
    bin_width = values_range / nbins
    if bin_width < 1e-3: # almost zero
        bin_width = max_value or 1
    
    for v in sorted_values:
        try:
            bin_index = min(int(math.floor((v-min_value) / bin_width)), nbins)
            binned_histogram[bin_index] += 1
        except KeyError:
            pass

    h = [ [int(min_value + (k*bin_width)), binned_histogram[k]] for k in sorted(binned_histogram.keys()) ]
    return h
