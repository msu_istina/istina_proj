# -*- coding: utf-8; -*-

# Rules processor.
#
# Author: Dmitry Shachnev <mitya57@gmail.com>, 2014.

import math
import re
from json import loads

from django.db import connections
from django.conf import settings

from .models import CustomCategory, RatingCache
from histogram import build_histogram

from sql_generators.util import get_bound_value
from sql_generators.articles import generate_articles_sql, generate_collection_articles_sql, generate_collection_thesis_sql
from sql_generators.books import generate_books_sql
from sql_generators.citations import generate_hirsh_sql, generate_citations_sql
from sql_generators.committees import generate_committee_sql
from sql_generators.conferences import generate_conferences_sql
from sql_generators.courses import generate_courses_sql
from sql_generators.dissers import generate_dissers_sql
from sql_generators.guidance import generate_diplom_guidance_sql, generate_coursework_guidance_sql, generate_dissers_guidance_sql
from sql_generators.memberships import generate_collection_editorial_sql, generate_journal_boards_sql, generate_discouncils_sql
from sql_generators.patents import generate_patents_sql, generate_softreg_sql
from sql_generators.projects import generate_projects_sql
from sql_generators.reports import generate_reports_sql
from sql_generators.study_work_external import generate_study_work_external_sql
from sql_generators.traineeships import generate_traineeship_sql

import logging
logger = logging.getLogger('sentry_debug')

format_re = re.compile(r"(.*)[.]custom([0-9]+)$")

# Needs to be kept in sync with groups in database.js
CATEGORY_GROUPS = {
    'scientific_work': ['articles', 'citations', 'conferences', 'dissertations', 'monographs', 'patents', 'reports', 'textbooks'],
    'study_work': ['study_work'],
    'study_work_external': ['study_work_external'],
    'money': ['projects'],
    'misc': ['membership', 'traineeships'],
}

class FormulaRule:
    "Wrapper class for json rule description."
    def __init__(self, rule):
        self.rule = rule
    def get_dict(self):
        return self.rule
    def label(self):
        return self.rule.get('label', None)
    def weight(self, default=1.0):
        weight_str = self.rule.get('weight', '1.0')
        if weight_str == "":
            weight_str = "1.0"
        return float( weight_str.replace(',', '.') )
    def get_modifier(self, name):
        modifiers = self.rule.get('modifiers', [])
        for m in modifiers:
            if m[0] == name:
                return m
        return None
    def get_modifier_bounds(self, modifier_name):
        m = self.get_modifier(modifier_name)
        if not m:
            return (None, None)
        return (get_bound_value(m[1]) if len(m)>1 else None, get_bound_value(m[2]) if len(m)>2 else None)
    def get_group_name(self):
        for key in CATEGORY_GROUPS:
            if self.rule['category'].split('.')[0] in CATEGORY_GROUPS[key]:
                return key
        return 'misc'

def apply_modifier(modifier, value):
    # modifier is a raw array as taken from json
    if not modifier or len(modifier) == 0:
        return value
    name = modifier[0]
    if name == 'upper_bound' and len(modifier) >= 2:
        upper_bound = int(modifier[1])
        return min(value, upper_bound)
    if name == 'log':
        if len(modifier) >= 2:
            log_base = float(modifier[1])
            return math.log(value, log_base)
        return math.log(value)
    return value

def apply_rule_modifiers(rule, value):
    new_value = value
    for modifier in rule.get('modifiers', []):
        new_value = apply_modifier(modifier, new_value)
    return new_value


def apply_limits(value, limits):
    "Limits is a list [low, high], where None value indicates no limit."
    lower_bound = limits[0] if len(limits)>0 else None
    upper_bound = limits[1] if len(limits)>1 else None
    category_points = value
    if lower_bound != None and category_points < lower_bound and category_points > 0:
        category_points = lower_bound
    if upper_bound != None and category_points > upper_bound:
        category_points = upper_bound
    return category_points
    

def change_value(modifier, parameter_name, value):
    if modifier[0] != 'change_parameter' and modifier[0] != 'limit_parameter':
        return value
    if modifier[1] != parameter_name:
        return value
    can_change = (modifier[0] == 'change_parameter')
    if len(modifier) >= 2 and modifier[2] is not None and value < modifier[1]:
        value = modifier[2] if can_change else 0
    if len(modifier) >= 3 and modifier[3] is not None and value > modifier[2]:
        value = modifier[3] if can_change else 0
    return value

def change_value_for_all(modifiers, parameter_name, value):
    for modifier in modifiers:
        value = change_value(modifier, parameter_name, value)
    return value



# Список первичных ключей типов книг (f_bookgroup_id)
TEXTBOOKS_ALL = [100, # 'Учебник',
                 217739, # 'Учебное пособие',
                 6054932, # 'Учебное пособие',
                 120, # 'Учебно-методическая литература',
                 ]

TEXTBOOKS = [100, # 'Учебник',
                 ]

TEXTBOOKS_SCHOOLBOOKS = [
    217739, # 'Учебное пособие',
    6054932, # 'Учебное пособие',
    # 120, # 'Учебно-методическая литература',
    ]

TEXTBOOKS_UCHMETLIT = [
    120, # 'Учебно-методическая литература',
    ]


BOOKS_MONOGRAPHS = [
    190, # Монография
    ]

# each SQL generating function accepts rule (r) and additional constraints (c)
CATEGORY_SQL_GENERATOR = {
    'articles': lambda r, c: generate_articles_sql(r, c, only_journals_article=False),
    # journal articles
    'articles.journals.international': generate_articles_sql,
    'articles.journals.international.Scopus': generate_articles_sql,
    'articles.journals.international.WoS': generate_articles_sql,
    'articles.journals.top25': lambda r, c: generate_articles_sql(r, c),
    'articles.journals.russian.hak': generate_articles_sql,
    'articles.journals.russian.misc': lambda r, c: generate_articles_sql(r, c),
    'articles.journals.misc': lambda r, c: generate_articles_sql(r, c, only_journals_article=True),
    # collection articles
    'articles.collections': lambda r, c: generate_collection_articles_sql(r, c, article_type=None),
    'articles.conference_materials': lambda r, c: generate_collection_thesis_sql(r, c),
    'articles.conference_materials.russian': lambda r, c: generate_collection_thesis_sql(r, c, include_non_russian=False),
    'articles.conference_materials.international': lambda r, c: generate_collection_thesis_sql(r, c, include_russian=False),
    #'monographs': lambda r, c: generate_books_sql(r, c, exclude_bookgroups=TEXTBOOKS),
    #'monographs.upto-3-authors': lambda r, c: generate_books_sql(r, c, exclude_bookgroups=TEXTBOOKS, authors_count_max=3),
    #'monographs.4+-authors': lambda r, c: generate_books_sql(r, c, exclude_bookgroups=TEXTBOOKS, authors_count_min=4),
    'membership.conference': lambda r,c: generate_committee_sql(r, c),
    'membership.committee': lambda r,c: generate_committee_sql(r, c), # alias for memberhip.committee
    'membership.discouncil': lambda r, c: generate_discouncils_sql(r, c),
    'membership.journals': lambda r, c: generate_journal_boards_sql(r, c),
    'membership.journal': lambda r, c: generate_journal_boards_sql(r, c), # alias for membership.journals
    'editorials.journals': lambda r, c: generate_journal_boards_sql(r, c), # old name
    'membership.editorial_journals': lambda r, c: generate_journal_boards_sql(r, c), # old name
    'membership.editorial_collections': lambda r, c: generate_collection_editorial_sql(r, c),
    'monographs': lambda r, c: generate_books_sql(r, c, include_bookgroups=BOOKS_MONOGRAPHS),
    'monographs.russian': lambda r, c: generate_books_sql(r, c, include_bookgroups=BOOKS_MONOGRAPHS, restrict_to_russian=True),
    'monographs.upto-3-authors': lambda r, c: generate_books_sql(r, c, include_bookgroups=BOOKS_MONOGRAPHS, authors_count_max=3),
    'monographs.4+-authors': lambda r, c: generate_books_sql(r, c, include_bookgroups=BOOKS_MONOGRAPHS, authors_count_min=4),
    'monographs.chapter': lambda r, c: generate_collection_articles_sql(r, c, article_type='CHAPTER'),
    'textbooks': lambda r, c: generate_books_sql(r, c, include_bookgroups=TEXTBOOKS_ALL),
    'textbooks.textbook': lambda r, c: generate_books_sql(r, c, include_bookgroups=TEXTBOOKS),
    'textbooks.school_book': lambda r, c: generate_books_sql(r, c, include_bookgroups=TEXTBOOKS_SCHOOLBOOKS),
    'textbooks.uchmetlit': lambda r, c: generate_books_sql(r, c, include_bookgroups=TEXTBOOKS_UCHMETLIT),
    'patents': generate_patents_sql,
    'patents.software': generate_softreg_sql,
    'conferences': lambda r, c: generate_conferences_sql(r, c),
    'conferences.international': lambda r, c: generate_conferences_sql(r, c, only_classes=["international"]),
    'conferences.russian': lambda r, c: generate_conferences_sql(r, c, only_classes=["russian"]),
    'dissertations.candidate': lambda r, c: generate_dissers_sql(r, c),
    'dissertations.doctor': lambda r, c: generate_dissers_sql(r, c, doctor=True),
    'traineeships': generate_traineeship_sql,
    'projects': lambda r, c: generate_projects_sql(r, c, is_leader=None),
    'projects.leader': lambda r, c: generate_projects_sql(r, c, is_leader=True),
    'projects.projmember': lambda r, c: generate_projects_sql(r, c, is_leader=False),
    'reports': lambda r, c: generate_reports_sql(r, c),
    'study_work.guidance.candidate': lambda r, c: generate_dissers_guidance_sql(r, c),
    'study_work.guidance.doctor': lambda r, c: generate_dissers_guidance_sql(r, c, doctor=True),
    'study_work.guidance.diploma': lambda r, c: generate_diplom_guidance_sql(r, c),
    'study_work.guidance.coursework': lambda r, c: generate_coursework_guidance_sql(r, c),
    'study_work.lectures_course.half_year': lambda r, c: generate_courses_sql(r, c, duration_in_years=0.5),
    'study_work.lectures_course.year': lambda r, c: generate_courses_sql(r, c, duration_in_years=1.0),
    'study_work.lectures_course.lectures_main': lambda r, c: generate_courses_sql(r, c, course_type='lectures_main'),
    'study_work.lectures_course.lectures_misc': lambda r, c: generate_courses_sql(r, c, course_type='lectures_misc'),
    'study_work.seminars': lambda r, c: generate_courses_sql(r, c, course_type='seminars'),
    'study_work.practics': lambda r, c: generate_courses_sql(r, c, course_type='practics'),
    'study_work.sem_labs': lambda r, c: generate_courses_sql(r, c, course_type='labs'),
    'study_work_external': generate_study_work_external_sql,
    'citations.hirsh.WOS': lambda r, c: generate_hirsh_sql(r, c, bibsource='WOS'),
    'citations.hirsh.Scopus': lambda r, c: generate_hirsh_sql(r, c, bibsource='Scopus'),
    'citations.for_period': lambda r, c: generate_citations_sql(r, c, for_period=True),
    'citations.for_all_years': lambda r, c: generate_citations_sql(r, c, for_period=False),
    }


def populate_selected_men_table(request_user_id, department_ids=None, positions=None, include_parttimers=False, include_fulltimers=True):
    "Заполнение временной таблицы сотрудников, включенных в отчёт. Возвращает текст выполненного запроса."
    requester_params = {
        'request_user_id': request_user_id,
        }
    department_limit_cond = u""
    if department_ids:
        department_limit_cond = u" and " + u"(amd.department_id in (" + u"""
        SELECT f_department_id FROM all_parent_department WHERE parent_f_department_id IN (
        """ + u",".join(map(str, department_ids)) + u")))" + u"""
        AND amd.L = 0
        """
    positions_limit_cond = u""
    if positions:
        positions_limit_cond = u" and post.f_post_id in (" + u",".join(map(str, positions)) + u")"
    parttimers_cond = "nvl(amd.f_manspost_part, 0) = 0"
    if include_parttimers and include_fulltimers:
        parttimers_cond = '/* no filter by parttime */ (1=1)'
    elif include_parttimers and not include_fulltimers:
        parttimers_cond = "nvl(amd.f_manspost_part, 0) = 1"
    elif include_fulltimers and not include_parttimers:
        parttimers_cond = "nvl(amd.f_manspost_part, 0) = 0"
    selected_men_subquery = (u"""
       select amd.f_man_id,
              min(amd.department_id) keep (dense_rank first order by amd.b) f_department_id,
              min(post.f_post_id) keep (dense_rank first order by amd.b) f_post_id
       from all_man_department amd
            left join post on (amd.f_post_id = post.f_post_id)
            left join postkind on (postkind.f_postkind_id = post.f_postkind_id)
            join (select f_department_id
                       from v_granted_roles
                       where f_permissionstypes_name IN ('is_representative_for', 'can_view_ratings')
                         and user_id = %(request_user_id)s
                  ) rl on (amd.department_id=rl.f_department_id)
       where """ + parttimers_cond + """
         and (amd.e is null or amd.e > sysdate)""" + department_limit_cond + positions_limit_cond + u"""
         and nvl(postkind.f_postkind_id, 0) <> 4 /* do not count students */
       group by amd.f_man_id""") % requester_params

    sql = u"""
    BEGIN
        SELECT f_man_id, f_department_id, f_post_id
            BULK COLLECT INTO pack_temp_points.tempTs_men('tables')
            FROM     (
            """ + selected_men_subquery + """
            );
    END;
    """
    if settings.DEBUG:
        print "========= POPULATE MEN =============="
        print sql
    cursor = connections['reports_server'].cursor()
    cursor.execute(sql)
    return selected_men_subquery


def populate_temporary_points_table(rule, start_year=None, end_year=None, workerid=None, user=None, seq=0):
    """
    Строит SQL запрос, который заполняет временную таблицу tmp_points.
    """
    current_rule = FormulaRule( rule )
    params = {
        'weight': current_rule.weight(),
        'category_name': rule['category'],
        'seq': seq,
        }
    if rule['category'].startswith('study_work_external.'):
        generator = generate_study_work_external_sql
    else:
        generator = CATEGORY_SQL_GENERATOR.get(rule['category'], None)
    if not generator:
        #--- TODO: report error message
        message = u'Empty generator for pmodel category %s' % (rule['category'],)
        logger.error(message)
        if settings.DEBUG:
            print message
        return None

    # generate category-specific subquery
    additional_constraints = {
        'start_year': start_year,
        'end_year': end_year,
        'request_user_id': user.id if user else -1,
        'report_worker_id': workerid,
        }
    subquery = generator(rule, additional_constraints)
    unit_min_value, unit_max_value = current_rule.get_modifier_bounds('limit_unit')
    params['min_unit'] = unit_min_value or 0
    params['max_unit'] = unit_max_value or -1

    if workerid:
        filter_by_men = u"WHERE catq.f_man_id = %s" % workerid
    else:
        filter_by_men = u"JOIN (SELECT f_man_id FROM TABLE(pack_temp_points.getData_men('tables'))) /* tmp_points_men */ tpm on (tpm.f_man_id = catq.f_man_id)"

    if workerid:
        params["object_description_fields"] = """substr(catq.act_name, 1, 1900) act_name
        , catq.act_paramname
        , catq.act_paramvalue
        , catq.act_nauthors
        """
    else:
        params["object_description_fields"] = "NULL act_name, NULL act_paramname, NULL act_paramvalue, NULL act_nauthors"
    sql = (u"""
    BEGIN
      SELECT
        f_man_id
        , category_name
        , act_name
        , act_paramname
        , act_paramvalue
        , act_nauthors
        , weight
        , points
        , total_points
        , year
        , ordering_seq
        , f_object_id
       BULK COLLECT INTO pack_temp_points.tempTs('tables_%(seq)s')
       FROM
        (SELECT
            catq.f_man_id
            , '%(category_name)s' category_name
            /* four NULLS or some strings */
            , %(object_description_fields)s
            , %(weight)s weight
            , catq.points
            , case
                when (nvl(catq.points, 0) * %(weight)s) < %(min_unit)s then %(min_unit)s
                when %(max_unit)s > 0 and (nvl(catq.points, 0) * %(weight)s) > %(max_unit)s then %(max_unit)s
                else (nvl(catq.points, 0) * %(weight)s)
              end total_points
            , catq.year
            , %(seq)s ordering_seq
            , catq.f_object_id
         FROM (
        """ % params) + subquery + u"""
        ) catq
          /* JOIN tmp_points_men tpm on (tpm.f_man_id = catq.f_man_id) */
        """ + filter_by_men + \
        u""");
    END;
    """
    if settings.DEBUG:
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        print sql
    cursor = connections['reports_server'].cursor()
    cursor.execute(sql)
    return sql


def remove_duplicated_objects():
    """ОЧЕНЬ МЕДЛЕННАЯ ФУНКЦИЯ. Для дублирующихся записей выбирает лучшую категорию, то есть
    оставляет во временной таблице только те, которые дают наибольшее
    число баллов."""
    query = u"""
    DELETE FROM tmp_points
    WHERE (f_man_id, f_object_id, ordering_seq) NOT IN (
          SELECT f_man_id
             , f_object_id
             , FIRST_VALUE(ordering_seq) OVER (PARTITION BY f_man_id, f_object_id ORDER BY total_points DESC) best_category_seq
          FROM tmp_points)
    """
    cursor = connections['reports_server'].cursor()
    cursor.execute(query)
    return True


# Запрос, который убирает дублирующиеся работы, оставляя только одно вхождение с максимальным количеством баллов.
CLEANED_TMP_POINTS_SQL = u"""
        SELECT  f_man_id, category_name, act_name, act_paramname, act_paramvalue, act_nauthors
                 , weight, points, total_points, year, ordering_seq
                 , f_object_id
        FROM (
          SELECT f_man_id, category_name, act_name, act_paramname, act_paramvalue, act_nauthors
                 , weight, points, total_points, year, ordering_seq
                 , f_object_id
                 , FIRST_VALUE(ordering_seq) OVER (PARTITION BY f_man_id, f_object_id ORDER BY total_points DESC) best_category_seq
          FROM TABLE(pack_temp_points.getAllData())
          )
        WHERE best_category_seq = ordering_seq
"""

def generate_categories_totals_sql(limits_for_subtotals=None):
    """
    Возвращает запрос, который выбирает f_man_id, ordering_seq и
    category_points с учетом ограничений на минимальное и максимальное
    количество баллов в категории, заданных в словаре
    limits_for_subtotals. Этот словарь сопоставляет порядковым номерам
    категорий список из минимального и максимального значения. None
    считается отсутствием ограничения.

    Использует ГЛОБАЛЬНУЮ переменную generate_categories_totals_sql.limits.
    """
    case_expression = u"CASE"
    limits_to_process = limits_for_subtotals or generate_categories_totals_sql.limits or []
    limits_present = False
    for seq in limits_to_process:
        lower_bound, upper_bound = limits_to_process[seq]
        if lower_bound != None or upper_bound != None:
            limits_present = True
        params = {
            'lower_bound': lower_bound,
            'upper_bound': upper_bound,
            'seq': seq,
            }
        if lower_bound != None:
            case_expression += """
            WHEN ordering_seq = %(seq)s AND category_points < %(lower_bound)s THEN %(lower_bound)s""" % params
        if upper_bound != None:
            case_expression += """
            WHEN ordering_seq = %(seq)s AND category_points > %(upper_bound)s THEN %(upper_bound)s""" % params
    case_expression += u"""
            ELSE category_points
          END"""

    if not limits_to_process or not limits_present:
        case_expression = u"category_points"

    return u"""
    SELECT
        f_man_id, ordering_seq
        , trunc(""" + case_expression + """, 3) category_points
    FROM (
        SELECT f_man_id, ordering_seq
               , sum(total_points) category_points
        FROM (
          SELECT f_man_id, total_points, ordering_seq
                 , FIRST_VALUE(ordering_seq) OVER (PARTITION BY f_man_id, f_object_id ORDER BY total_points DESC) best_category_seq
          FROM TABLE(pack_temp_points.getAllData())
          )
        WHERE best_category_seq = ordering_seq
        GROUP BY f_man_id, ordering_seq
        )
        """ 


def compute_categories_totals_sql(limits_for_subtotals=None):
    query = u"""
    SELECT tmp_points_men.f_man_id
           , man.f_man_namel || ' ' || man.f_man_namef || ' ' || man.f_man_namem FIO
           , tmp_points_men.f_department_id AS f_department_id
           , department.f_department_name
           , post.f_post_name
           , NVL(points.ordering_seq, 0) /* men with no activities appear in category 0 */
           , NVL(category_points, 0) category_points
    FROM
    (SELECT * FROM TABLE(pack_temp_points.getData_men('tables'))) tmp_points_men
    LEFT JOIN (""" + generate_categories_totals_sql(limits_for_subtotals) + u"""
    ) points ON (tmp_points_men.f_man_id = points.f_man_id)
    JOIN man ON (man.f_man_id = tmp_points_men.f_man_id)
    LEFT JOIN post ON (post.f_post_id = tmp_points_men.f_post_id)
    JOIN department ON (department.f_department_id = tmp_points_men.f_department_id)
    ORDER BY points.f_man_id, points.ordering_seq
    """
    return query


def compute_averages(rules, user, start_year=0, end_year=3000, depid=None):
    department_ids = [depid] if depid else None
    
    evaluate_rules(rules, user=user, start_year=start_year, end_year=end_year, department_ids=department_ids)
    sql = u"""SELECT f_post_name
       , count(distinct tmp_points.f_man_id)
       , trunc(avg(total_points), 3)
       , trunc(PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY total_points), 3) median
       , trunc(PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY total_points), 3) p25
       , trunc(PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY total_points), 3) p75
    FROM (
      SELECT f_man_id, sum(category_points) total_points
      FROM (""" + generate_categories_totals_sql() + u""")
      GROUP BY f_man_id
      ) tmp_points
    JOIN (SELECT * FROM TABLE(pack_temp_points.getData_men('tables'))) tmp_points_men ON (tmp_points_men.f_man_id = tmp_points.f_man_id)
    JOIN post ON (post.f_post_id = tmp_points_men.f_post_id)
    GROUP BY f_post_name"""

    cursor = connections['reports_server'].cursor()
    cursor.execute(sql)
    result = []
    for item in cursor:
        result.append({'postname': item[0],
                       'countmen': item[1],
                       'avg': item[2],
                       'median': item[3],
                       'p25': item[4],
                       'p75': item[5],
                       })
    return result

def evaluate_rules(rules, user=None, start_year=0, end_year=3000, workerid=None, positions=None, department_ids=None, include_parttimers=False, include_fulltimers=True):
    """
    Вычисляет формулу, заданную набором правил rules. Результат записывается во временную таблицу tmp_points.
    """
    cursor = connections['reports_server'].cursor()
    limits_for_subtotals = {}
    group_limits = {}
    
    populate_selected_men_table(user.id if user else -1, department_ids, positions=positions, include_parttimers=include_parttimers, include_fulltimers=include_fulltimers)
    for (seq, rule) in enumerate(rules):
        if 'category_group' in rule:
            group_limits[rule['category_group']] = (rule['type'], rule['value'])
            continue
        current_rule = FormulaRule(rule)
        total_min_value, total_max_value = current_rule.get_modifier_bounds('limit_sum')
        limits_for_subtotals[seq] = [total_min_value, total_max_value]

        category_sql = populate_temporary_points_table(rule, start_year, end_year, workerid, user=user, seq=seq)

    generate_categories_totals_sql.limits = limits_for_subtotals

    if settings.DEBUG:
        cursor.execute("SELECT COUNT(*) FROM TABLE(pack_temp_points.getAllData())")
        for nitems in cursor:
            print "Temporary table populated by %s records" % nitems
    #remove_duplicated_objects()
    return group_limits


def get_sum(group_points, group_limits):
    changes = []
    for name in group_points:
        if name in group_limits and group_limits[name][0] == 'limit':
            if group_points[name] > group_limits[name][1]:
                changes.append((name, group_points[name], group_limits[name][1]))
                group_points[name] = group_limits[name][1]
    one_percent = sum(group_points.values()) * 0.01
    for name in group_points:
        if name in group_limits and group_limits[name][0] == 'percent':
            if group_points[name] > group_limits[name][1] * one_percent:
                changes.append((name, group_points[name], group_limits[name][1] * one_percent))
                group_points[name] = group_limits[name][1] * one_percent
    return sum(group_points.values()), changes

def process_rules(rules, distribution_only=False, user=None, start_year=0, end_year=3000, workerid=None, positions=None, department_ids=None, include_parttimers=False, include_fulltimers=True):
    """
    Возвращает тройку (results, headers, tooltipos).
    Headers и tooltips - это заголовки и подсказки столбцов таблицы с результатами (списки строк).
    Results - Список списков. Каждая строка соответствует одному f_man_id, столбцы - категориям формулы.
    """
    result = []
    all_headers = [u"ID", u"Сотрудник", u"Подразделение", u"Должность", u"Сумма"]
    all_headers_tooltips = [u"Идентификатор", u"ФИО", u"Основное место работы", u"Должность по основному месту работы", u"Общая сумма баллов"]
    n_extra_headers = len(all_headers)
    total_points_index = n_extra_headers-1 # Позиция в списке result[x], где находится общая сумма (последний фиксированный столбец)

    limits_for_subtotals = {}
    group_limits = {}
    cursor = connections['reports_server'].cursor()
    
    for (seq, rule) in enumerate(rules):
        current_rule = FormulaRule(rule)

        if 'category_group' in rule:
            group_limits[rule['category_group']] = (rule['type'], rule['value'])
            continue

        rule_category_name = rule.get('category', None)
        all_headers.append( rule.get('label', '') )
        all_headers_tooltips.append( rule.get('category', '') )

        total_min_value, total_max_value = current_rule.get_modifier_bounds('limit_sum')
        limits_for_subtotals[seq] = [total_min_value, total_max_value]

    evaluate_rules(rules, user=user, start_year=start_year, end_year=end_year,
                   workerid=workerid, positions=positions, department_ids=department_ids,
                   include_parttimers=include_parttimers,
                   include_fulltimers=include_fulltimers)

    totals_sql = compute_categories_totals_sql(limits_for_subtotals)
    if settings.DEBUG:
        print totals_sql
    
    cursor = connections['reports_server'].cursor()
    cursor.execute(totals_sql)

    prev_f_man_id = None
    record = None
    n_categories = len(rules)
    group_points = None
    for f_man_id, fio, department_id, department_name, postname, seq, category_points in cursor:
        if f_man_id != prev_f_man_id:
            if record:
                record[total_points_index], changes = get_sum(group_points, group_limits)
                result.append(record)
            record = [f_man_id, fio, department_name, postname, 0] + [0] * (n_categories)
            prev_f_man_id = f_man_id
            group_points = {name: 0 for name in CATEGORY_GROUPS}
        record[seq+n_extra_headers] = category_points
        group_points[FormulaRule(rules[seq]).get_group_name()] += float(category_points)
    if record:
        record[total_points_index], changes = get_sum(group_points, group_limits)
        result.append( record )

    if distribution_only:
        return (build_histogram([row[total_points_index] for row in result], nbins=144-12, q=0.9, qbins=12), None, None)
    return (result, all_headers, all_headers_tooltips)


def load_category_description(category_name):
    "Returns two values: base category name and custom category description (optionally)."
    match = format_re.match(category_name)
    if not match:
        return (category_name, None)
    base_category_name = match.group(1)
    custom_category_id = match.group(2)
    try:
        custom_cat = CustomCategory.objects.get(pk=custom_category_id)
        return (base_category_name, custom_cat.title)
    except CustomCategory.DoesNotExist:
        #--- TODO: log error
        return (base_category_name, None)


def process_formula_for_personal_report(formula, worker, start_year=0, end_year=3000, depid=None):
    rules = loads(formula.jsondata)
    group_limits = evaluate_rules(rules, start_year=start_year, end_year=end_year, workerid=worker.id)
    cursor = connections['reports_server'].cursor()
    sql = """
    SELECT
        ordering_seq
        , category_name
        , act_name
        , act_paramname
        , act_paramvalue
        , act_nauthors
        , weight
        , trunc(points, 2) points
        , trunc(total_points, 2) total_points
        , f_object_id
    FROM ( """ + CLEANED_TMP_POINTS_SQL + u"""
    )  tmp_points
    WHERE f_man_id = %(f_man_id)s
    ORDER BY ordering_seq, year
    """ % {'f_man_id': worker.id }
    cursor.execute(sql)

    subtotals = {}
    total_points = 0
    last_seq = None
    header_data = None
    category_points = 0
    result = []
    for seq, category_name, act_name, act_paramname, act_paramvalue, act_nauthors, weight, factor, total_points, f_object_id in cursor:
        header_data = None
        if last_seq != seq:
            header_data = {
                'line_number': seq,
                'category_name': rules[seq].get('label', category_name),
                'total_points': lambda seq=seq: subtotals[seq],
                }
            category_points = 0
        last_seq = seq
        category_points += total_points or 0
        subtotals[seq] = category_points
        result.append(
            {'name': act_name,
             'param_value': act_paramvalue,
             'nauthors': act_nauthors,
             'weight': weight,
             'points': factor,
             'total_points': total_points,
             'header_data': header_data,
             })
    group_points = {name: 0 for name in CATEGORY_GROUPS}
    for seq in subtotals.keys():
        current_rule = FormulaRule(rules[seq])
        total_min_value, total_max_value = current_rule.get_modifier_bounds('limit_sum')
        subtotals[seq] = apply_limits(subtotals[seq], [total_min_value, total_max_value])
        group_points[current_rule.get_group_name()] += float(subtotals[seq])
    total_points, changes = get_sum(group_points, group_limits)

    # Update the cache
    RatingCache.objects.filter(formula=formula, worker=worker).delete()
    new_cache_item = RatingCache.objects.create(
        formula=formula,
        worker=worker,
        value=total_points,
        from_year=start_year,
        to_year=end_year)
    new_cache_item.save()

    return total_points, changes, result
