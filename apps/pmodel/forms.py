# -*- coding: utf-8; -*-

from common.dynatree_widgets import DynatreeWidget
from django import forms
from django.forms import ModelChoiceField, ModelMultipleChoiceField
from organizations.models import Department
from journals.models import Journal
from journals.forms import JournalChoiceField

class PublishFormulaForm(forms.Form):
    department = ModelMultipleChoiceField(label=u"Подразделение, сотрудники которого могут выполнять эту формулу", queryset=Department.objects.all())

    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop('request_user', None)
        super(PublishFormulaForm, self).__init__(*args, **kwargs)
        if request_user:
            departments_sql = request_user.representatives.create_sql_statement_for_departments_dynatree(request_user)
            self.fields['department'].widget = DynatreeWidget(select_mode=2, initial_sql=departments_sql)

class JournalSelectForm(forms.Form):
    journal = JournalChoiceField(label=u"Журнал", queryset=Journal.objects.all(), required=True)
