# -*- coding: utf-8; -*-

import logging
import sys

from django.db import connection, transaction

from django.core.exceptions import ObjectDoesNotExist
from sqlreports.models import CheckboxesProcessor

from conferences.models import Conference, ConferencePresentation, ConferencePresentationAuthorship

logger = logging.getLogger('sentry_debug')

class XHRConfEditorProcessor(CheckboxesProcessor):
    "Обработчик запросов для отчета редактирования классификаторов проектов."
    codename = 'pmodel_confs_processor'

    def update_field(cls, sqlreport, record_id, column_code, new_value, request):
        try:
            if column_code.upper() == 'SPEAKER_NAME':
                ConferencePresentationAuthorship.objects.filter(conference_presentation=record_id,
                                                                role_name='speaker').update(role_name='')
                # record_id используется для дополнительной проверки, что обновляется правильный автор
                if int(new_value) > 0:
                    pauthor = ConferencePresentationAuthorship.objects.get(conference_presentation=record_id,
                                                                           pk=new_value)
                    pauthor.role_name = 'speaker'
                    pauthor.save()
            elif column_code.upper() == 'TALK_KIND':
                ConferencePresentation.objects.filter(pk=record_id).update(talk_kind=new_value)
            elif column_code.upper() in ['F_CONFSCOPE_NAME', 'F_CONFKIND_NAME', 'CONF_COUNTMAN', 'CONF_COUNTDOCL']:
                presentation = ConferencePresentation.objects.get(pk=record_id)
                conf = presentation.conference
                if column_code.upper() == 'F_CONFSCOPE_NAME':
                    Conference.objects.filter(pk=conf.id).update(scope=new_value)
                elif column_code.upper() == 'F_CONFKIND_NAME':
                    Conference.objects.filter(pk=conf.id).update(category=new_value)
                elif column_code.upper() == 'CONF_COUNTMAN':
                    Conference.objects.filter(pk=conf.id).update(participants_count=int(new_value))
                elif column_code.upper() == 'CONF_COUNTDOCL':
                    Conference.objects.filter(pk=conf.id).update(speakers_count=int(new_value))
            return "ok"
        except Exception as e:
            return u"Не удалось сохранить данные: %s" % e
