# -*- coding: utf-8; -*-
from __future__ import unicode_literals

from django.utils import unittest
import doctest

from publications import utils


class SplitAuthorsTest(unittest.TestCase):
    def test_split_authors_doctest(self):
        """Run doctests in publications.utils"""
        doctest.testmod(utils, raise_on_error=True)
