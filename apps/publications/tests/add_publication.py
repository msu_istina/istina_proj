# -*- coding: utf-8; -*-
from __future__ import unicode_literals

from django.utils import unittest
from django.test.client import Client

import lxml.html


class AddPublication(object):
    def setUp(self):
        self.client = Client()
        self.client.login(username='test', password='test')

    @staticmethod
    def prepare_form_data(data):
        for i in data.keys()[:]:
            if data[i] is None:
                del data[i]
        return data

    def do_step2(self, citation, true_form_fields):
        """Submit citation and check that it was parsed correctly

        Stores form as self.form
        """
        response = self.client.post("/publications/add/step2/", data={"citation": citation})
        tree = lxml.html.fromstring(response.content.decode("utf8"))
        self.assertIn("шаг 2", unicode(tree.xpath("//title/text()")[0]).lower())
        form = filter(lambda x: x.attrib.get("id") == "input_fields", tree.forms)[0]
        for key in true_form_fields:
            self.assertEqual(true_form_fields[key], form.fields[key])
        self.form = form

    def do_step3(self):
        """Submit step2-form and check that we on the step3 page now

        Stores form as self.form
        """
        response = self.client.post("/publications/add/step3/", data=dict(self.form.fields))
        tree = lxml.html.fromstring(response.content.decode("utf8"))
        self.assertIn("шаг 3", unicode(tree.xpath("//title/text()")[0]).lower())
        self.form = filter(lambda x: x.attrib.get("id") == "add_publication_form", tree.forms)[0]

    def do_step4(self):
        """Submit step3-form and check that we on the step4 (add more info) page"""
        data = self.prepare_form_data(dict(self.form.fields))
        del data['goback']

        response = self.client.post("/publications/add/step3/save/", data=data)
        tree = lxml.html.fromstring(response.content.decode("utf8"))
        self.assertIn("Добавление дополнительной информации".lower(), unicode(tree.xpath("//title/text()")[0]).lower())


class WorkerFormMixin(unittest.TestCase):
    """Mixin for tests containing worker disambiguation form"""

    def check_similar_worker_search(self, form, worker_name, candidates_count, checked_variant_exists):
        """Check that worker is among author list and has exactly candidates_count variants with checked selected

        form
            part of lxml parse tree that contains author-related fields
        worker_name
            fullname string
        candidates_count
            integer or None
            if None the check will be skipped
        checked_variant_exists
            True or False or None
            if None the check will be skipped
        """
        endswith = lambda s, tail: "[{1} = substring({0}, string-length({0}) - string-length({1}) + 1)]".format(s, tail)
        for subform in form.xpath(".//table[@class='worker-list']/tbody/tr[not(contains(@class, 'hide'))]/td[2]"):
            fullname = subform.xpath(".//input%s" % endswith("@name", "'fullname'"))[0].attrib["value"]
            if fullname.strip() != worker_name:
                continue
            break
        else:
            raise Exception("Cannot find '%s' among workers" % worker_name)
        if candidates_count is not None:
            variants = subform.xpath(".//input[not(@value='0')][not(@value='-1')]%s" % endswith("@name", "'wid'"))
            self.assertEqual(len(variants), candidates_count)
        if checked_variant_exists is not None:
            real_checked_variant_exists = len(subform.xpath(".//input[@checked='checked']{0}|.//input[@type='hidden']{0}".format(endswith("@name", "'wid'"))))
            self.assertEqual(real_checked_variant_exists, int(checked_variant_exists))


class AddJournalArticle(AddPublication, WorkerFormMixin):
    def test_add(self):
        """Add standard journal article: check citation parse, author disambiguation, saving"""
        citation = "В.А. Васенин, С.А. Афонин, А.С. Козицын. Автоматизированная система тематического анализа информации. Информационные технологии, 4, 1-32, 2009."
        true_dict = {
                "work_type": "in_journal",
                "authors": "Васенин В.А., Афонин С.А., Козицын А.С.",
                "title": "Автоматизированная система тематического анализа информации",
                "journal": "Информационные технологии",
                "number": "4",
                "year": "2009",
                "firstpage": "1",
                "lastpage": "32",
                }
        self.do_step2(citation, true_dict)
        self.do_step3()
        for author in true_dict["authors"].split(", "):
            self.check_similar_worker_search(self.form, author, candidates_count=1, checked_variant_exists=True)
        self.do_step4()


class AddCollectionArticle(AddPublication, WorkerFormMixin):
    def test_add(self):
        """Add standard in-collection article: check citation parse, author disambiguation, saving"""
        citation = """
            @inproceedings{astapov2008monitoring211668,
                author = "Астапов, И.С. and Дзыба, М.С. and Коршунов, А.А.",
                title = "Мониторинг телекоммуникационной среды Института механики МГУ как составной части распределенного экспериментального полигона",
                booktitle = "Ломоносовские чтения. Тезисы докладов научной конференции. Секция механики. Апрель 2008",
                year = "2008",
                pages = "26--26",
                url = "http://istina.msu.ru/publications/article/211668/"
            }
        """
        true_dict = {
                "work_type": "in_collection",
                "authors": "Астапов И.С., Дзыба М.С., Коршунов А.А.",
                "title": "Мониторинг телекоммуникационной среды Института механики МГУ как составной части распределенного экспериментального полигона",
                "collection": "Ломоносовские чтения. Тезисы докладов научной конференции. Секция механики. Апрель 2008",
                "year": "2008",
                "firstpage": "26",
                "lastpage": "26",
                }
        self.do_step2(citation, true_dict)
        self.do_step3()
        self.check_similar_worker_search(self.form, "Астапов И.С.", candidates_count=1, checked_variant_exists=True)
        self.check_similar_worker_search(self.form, "Дзыба М.С.", candidates_count=0, checked_variant_exists=True)
        self.check_similar_worker_search(self.form, "Коршунов А.А.", candidates_count=1, checked_variant_exists=True)
        self.do_step4()


class AddBook(AddPublication, WorkerFormMixin):
    def test_add(self):
        """Add standard book: check citation parse, author disambiguation, saving"""
        citation = """
            @book{andronov2006vikhrevye212507,
                author = "Андронов, П.Р. and Гувернюк, С.В. and Дынникова, Г.Я.",
                title = "Вихревые методы расчёта нестационарных гидродинамических нагрузок",
                year = "2006",
                pages = "184",
                publisher = "Изд-во Московского университета Москва",
                url = "http://istina.msu.ru/publications/book/212507/"
            }
        """
        true_dict = {
                "work_type": "in_book",
                "authors": "Андронов П.Р., Гувернюк С.В., Дынникова Г.Я.",
                "title": "Вихревые методы расчёта нестационарных гидродинамических нагрузок",
                "year": "2006",
                "publisher_name": "Изд-во Московского университета Москва",
                }
        self.do_step2(citation, true_dict)
        self.do_step3()
        for author in true_dict["authors"].split(", "):
            self.check_similar_worker_search(self.form, author, candidates_count=0, checked_variant_exists=True)
        self.do_step4()
