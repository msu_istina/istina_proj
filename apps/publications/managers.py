# -*- coding: utf-8 -*-
import os
from django.db.models import Q
from common.managers import MyManager, SimilarFieldManager
from utils import bibtex2html


class PublicationManager(MyManager):
    '''Manager for Publication class descendants, Article and Book.'''
    use_for_related_fields = True

    def in_style(self, style):
        '''Returns a styled html of citations of publications.'''
        return self._in_style(self.get_query_set(), style)

    @staticmethod
    def _in_style(queryset, style):
        bibtex = " ".join(publication.bibtex for publication in queryset)
        return bibtex2html(bibtex, style)

    def project_publications(self, project):
        project_authors = project.members.all()
        ids = self.filter(authors__in=project_authors).values_list('pk', flat=True).distinct()
        return self.filter(pk__in=ids)

class ArticleManager(SimilarFieldManager, PublicationManager):
    '''Model manager for Article model.'''

    def get_similar(self, title):
        return self.get_similar_by_field("title", title, 0.7, 3)

    def pure(self):
        '''Returns 'pure' articles (ie. that are not theses).'''
        return self.exclude(collection__theses=True)

    def pure_journal(self):
        '''Returns 'pure' articles (ie. that are not theses) published in a journal.'''
        return self.filter(journal__isnull=False)

    def pure_collection(self):
        '''Returns 'pure' articles (ie. that are not theses) published in a collection.'''
        return self.filter(journal__isnull=True).exclude(collection__theses=True)

    def theses(self):
        '''Returns articles that are collection theses.'''
        return self.filter(collection__theses=True)

    def recent_pure(self, limit=3, recent_type="date", pure_type=None):
        pure = None
        if pure_type == "articles_journal":
            pure = self.pure_journal()
        elif pure_type == "articles_collection":
            pure = self.pure_collection()
        else:
            pure = self.pure()
        if recent_type == "date":
            return self.recent_qs(pure, '-year', order_by=False, limit=limit)
        else:
            return self.recent_by_id_qs(pure, limit=limit)

    def recent_theses(self, limit=3, recent_type="date"):
        if recent_type == "date":
            return self.recent_qs(self.theses(), '-collection__year', limit=limit)
        else:
            return self.recent_by_id_qs(self.theses(), limit=limit)

    def in_style_pure(self, style):
        return self._in_style(self.pure(), style)

    def in_style_theses(self, style):
        return self._in_style(self.theses(), style)

    def top_ids(self):
        '''Return list of ids of top articles, i.e. satisfying formal criteria (top journal).'''
        from statistics.utils import get_top_publications
        return get_top_publications()

    def top(self):
        '''Return queryset of top articles, i.e. satisfying formal criteria (top journal).'''
        return self.filter(id__in=self.top_ids())

    def rewarded(self):
        '''Return queryset of rewarded articles, i.e. included in official prikaz and some of authors got the money for them.'''
        from statistics.models import RankedArticle
        return self.filter(id__in=RankedArticle.objects.all().values_list('articleid', flat=True))

    def update_year(self):
        print "Before: %d articles do not have year" % self.filter(year__isnull=True).count()
        for article in self.filter(year__isnull=True):
            article.save() # executes custom save method
        print "After: %d articles do not have year" % self.filter(year__isnull=True).count()

    def prepare_data_for_terms_extraction(self, categorization_name, base_dir):
        u"""
        Процесс выделения терминов из сущностей базы данных (например, статей), состоит из следующих этапов:
        1. Подготовка данных для выделения терминов.
        В качестве исходных данных могут использоваться аннотации статей, разделенных на рубрики в ИСТИНЕ.
        Тогда вызывается функция Article.objects.prepare_data_for_terms_extraction(categorization_name, base_dir), где
            categorization_name - имя рубрикации (например, GRNTI)
            base_dir - путь к папке, в которую будут складываться аннотации, разделенные на рубрики.
        Отметим, что по умолчанию используются рубрики, принадлежащие верхнему уровню рубрикации.
        2. Выделение терминов из исходных данных, например, с помощью алгоритма Brainsterm.
        Исходный код алгоритма находится в https://bitbucket.org/goldan/brainsterm. Там же есть инструкция по установке и использованию модуля.
        В качестве папки с документами указывается как раз base_dir.
        В первых раз алгоритм может работать значительное время (несколько часов для 50 тыс. документов), т.к. он подготавливает исходные данные, обрабатывая все документы.
        При последующих запусках файл с подготовленными исходными данными будет уже существовать, и алгоритм будет работать значительно быстрее (несколько минут).
        3. Использование выделенных терминов в различных сервисах ИСТИНЫ.
        См. документацию к этим сервисам (например, разрабатываемый сервис поиска экспертов).
        """
        from journals.models import JournalCategorization
        categorization = JournalCategorization.objects.get(name=categorization_name)
        rubrics = categorization.rubrics.filter(parent__isnull=True)
        for rubric in rubrics:
            articles = [article for article in self.filter(journal__rubric_memberships__rubric=rubric).values_list('id', 'abstract') if article[1]]
            rubric_dir = os.path.join(base_dir, rubric.name)
            if not os.path.exists(rubric_dir):
                os.makedirs(rubric_dir)
            for id, abstract in articles:
                filename = os.path.join(rubric_dir, "%d.txt" % id)
                f = open(filename, "w")
                f.write(abstract.encode('utf8'))
                f.close()


class ThesisManager(ArticleManager):
    '''Model manager for Article model operating on theses (article.collection.theses=True).'''
    def get_query_set(self):
        return super(ThesisManager, self).get_query_set().filter(collection__theses=True)


class PureArticleManager(ArticleManager):
    '''Model manager for Article model operating on 'pure' articles (ie. that are not theses).'''
    def get_query_set(self):
        return super(PureArticleManager, self).get_query_set().exclude(collection__theses=True)


class ArticleAuthorshipManager(MyManager):
    '''Model manager for ArticleAuthorship model.'''

    def get_registered_authors(self, article_ids):
        '''Return a list of worker ids that are linked to users and are authors of articles from article_ids list.'''
        return self.filter(article__in=article_ids, author__profile__isnull=False).values_list('author', flat=True).distinct()


class BookManager(SimilarFieldManager, PublicationManager):
    '''Model manager for Book model.'''

    def get_similar(self, title):
        return self.get_similar_by_field("title", title, 0.7, 3)


class ParsingEventManager(MyManager):
    '''Model manager for ParsingEvent model.'''

    def get_queue_size(self, user):
        '''Returns number of parsing events queued for parsing by the user.'''
        return self.filter(user=user, type="in queue").count()

    def get_next_from_queue(self, user):
        '''Returns the oldest event from user queue.'''
        try:
            return self.filter(user=user, type="in queue").order_by("created")[0]
        except IndexError: # the queue is empty
            return None

    def clear_queue(self, user):
        '''Clears user queue, setting type of remaining events to "deleted from queue".'''
        events = self.filter(user=user, type="in queue")
        for event in events:
            event.type = "deleted from queue"
            event.save()
        return self.filter(journal__isnull=True)

    def theses(self):
        '''Returns articles that are collection theses.'''
        return self.filter(collection__theses=True)

    def recent_pure(self, limit=3, recent_type="date"):
        if recent_type == "date":
            return self.recent_qs(self.pure(), '-year', order_by=False, limit=limit)
        else:
            return self.recent_by_id_qs(self.pure(), limit=limit)

    def recent_theses(self, limit=3, recent_type="date"):
        if recent_type == "date":
            return self.recent_qs(self.theses(), '-collection__year', limit=limit)
        else:
            return self.recent_by_id_qs(self.theses(), limit=limit)

    def in_style_pure(self, style):
        return self._in_style(self.pure(), style)

    def in_style_theses(self, style):
        return self._in_style(self.theses(), style)

    def top_ids(self):
        '''Return list of ids of top articles, i.e. satisfying formal criteria (top journal).'''
        from statistics.utils import get_top_publications
        return get_top_publications()

    def top(self):
        '''Return queryset of top articles, i.e. satisfying formal criteria (top journal).'''
        return self.filter(id__in=self.top_ids())

    def rewarded(self):
        '''Return queryset of rewarded articles, i.e. included in official prikaz and some of authors got the money for them.'''
        from statistics.models import RankedArticle
        return self.filter(id__in=RankedArticle.objects.all().values_list('articleid', flat=True))


class ThesisManager(ArticleManager):
    '''Model manager for Article model operating on theses (article.collection.theses=True).'''
    def get_query_set(self):
        return super(ThesisManager, self).get_query_set().filter(collection__theses=True)


class PureArticleManager(ArticleManager):
    '''Model manager for Article model operating on 'pure' articles (ie. that are not theses).'''
    def get_query_set(self):
        return super(PureArticleManager, self).get_query_set().exclude(collection__theses=True)


class ArticleAuthorshipManager(MyManager):
    '''Model manager for ArticleAuthorship model.'''

    def get_registered_authors(self, article_ids):
        '''Return a list of worker ids that are linked to users and are authors of articles from article_ids list.'''
        return self.filter(article__in=article_ids, author__profile__isnull=False).values_list('author', flat=True).distinct()


class BookManager(SimilarFieldManager, PublicationManager):
    '''Model manager for Book model.'''

    def get_similar(self, title):
        return self.get_similar_by_field("title", title, 0.7, 3)


class ParsingEventManager(MyManager):
    '''Model manager for ParsingEvent model.'''

    def get_queue_size(self, user):
        '''Returns number of parsing events queued for parsing by the user.'''
        return self.filter(user=user, type="in queue").count()

    def get_next_from_queue(self, user):
        '''Returns the oldest event from user queue.'''
        try:
            return self.filter(user=user, type="in queue").order_by("created")[0]
        except IndexError: # the queue is empty
            return None

    def clear_queue(self, user):
        '''Clears user queue, setting type of remaining events to "deleted from queue".'''
        events = self.filter(user=user, type="in queue")
        for event in events:
            event.type = "deleted from queue"
            event.save()

class BookCategoryManager(MyManager):
    def book_types(self):
        #return self.filter(pk__gte=100, pk__lt=200)
        return self.filter(Q(pk__in=[100, 217739, 6054932]) | (Q(pk__gte=100) & Q(pk__lt=200)))

    def book_grifs(self):
        return self.filter(pk__gte=200, pk__lt=300)
