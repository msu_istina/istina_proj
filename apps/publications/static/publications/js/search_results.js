$(function() {
    var search_string =  $("#id_search_string").val()
    $('a[href^="/publications/search"]').each(function(){ $(this).attr('href', $(this).attr('href') + location.search)   })
    
    $('.current_work_type').replaceWith( "<span class='current_work_type'>" + $('.current_work_type').text() + "</span>")
});
