$(function() {

    var target = null;
    $('#add_publication_form :submit').click(function() {
        target = this;
    });

    $('#add_publication_form').submit(function(event) {
        var self = $(this);
        if (target && $(target).attr("id") == "submit_step3") {
            event.preventDefault();
            $(":submit").each(function(index, elem) {
                $(elem).attr('disabled', true);
                setTimeout(function() {
                    $(elem).attr('disabled', false);
                }, 5000);
            });
            target = null;
            self.submit();
        }
    });

});




