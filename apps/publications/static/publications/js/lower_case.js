$(function() {
    
    function toTitleCase(str) {
        return str.toString().toLowerCase().replace(/(^|[ ,.])([^ ,.])/g, function(match) {
            return match.toString().toUpperCase();
        });
    }
    
    $(".lower_case").click(function(event) {
        event.preventDefault();
        var input = $(this).prev("input[type=text]")
        var new_value;
        if ($(this).hasClass("authors")) { // special way of transforming
            new_value = toTitleCase(input.val())
        }
        else {
            new_value = input.val().charAt(0)+ input.val().toString().toLowerCase().substr(1);	
        }
        input.val(new_value);      
    });  
    
    $(".lower_case").prev("input[type=text]").css('width', 660);
    
});
