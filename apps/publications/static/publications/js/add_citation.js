$(function() {
    $("a.help_input").click(function(event) {
        event.preventDefault()
        $(".help_input").toggle()
    });
    $("div.help_input a.inner").click(function(event) {
        event.preventDefault()
        $(this).closest("li").find("a, div").toggle()
    });
});
