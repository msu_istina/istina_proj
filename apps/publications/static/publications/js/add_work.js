$(function() {
    $(".gostep1").click(function(event) {
        event.preventDefault();
        $("#input_fields :input").not(':button, :submit, :reset, :hidden').val('')
        $(".step").hide()
        $("#step1").show()
        var title = document.title
        document.title = title.replace("шаг 2", "шаг 1")
        $(".messages").hide()
    });

    $("input.gostep1_permanent").click(function(event) {
        event.preventDefault();
        var url = $("a.gostep1_permanent").attr("href");
        window.location.href = url;
    });

    $(".gostep2").click(function(event){
        event.preventDefault();
        $(".step").hide()
        $("#step2").show()
        var title = document.title
        document.title = title.replace("шаг 1", "шаг 2")
        document.location.hash = '#step2';
    })

    if (
       (document.location.hash && document.location.hash == "step2") ||
       (document.location.search && document.location.search == "?step=2")
    ){
        $(".gostep2").click()
    }

    $(".show_xml").click(function(event){
        event.preventDefault();
        $(".show_xml, .xml").toggle()
    })

    $("#paste_selection button").click(function(event) {
        event.preventDefault();
        var txt = getSelectedText()
        if (txt == "") {
            return false;
        }
        var classList =$(this).attr('class').split(/\s+/);
        var klass;
        $.each (classList, function(index, item) {
            if (item.substr(0,3) != 'in_') { // classname does not start with "in_". FIXME: this is not reliable and relies on the order of the classes.
               klass = item
            }
        });
        // pages is a special case
        if (klass == "pages") {
            // try to extract two values
            regexp_two_pages = /^(.+?)[- ]+(.+)$/
            match = regexp_two_pages.exec(txt)
            if (match && match.length == 3) { // first element is the matched string, the second and the third are the matched groups
                var firstpage = match[1]
                var lastpage = match[2]
                $(".firstpage").find(":input").val(firstpage)
                $(".lastpage, .pages").find(":input").val(lastpage)
            }
            else {
                $(".firstpage, .lastpage, .pages").find(":input").val(txt)
            }
        }
        else {
            $("."+klass).show().find(":input").val(txt)
        }
    });

    $("input[name=work_type]").change(function(event) {
        var id =$(this).attr('id')
        $(".in_journal, .in_collection, .in_book").hide()
        $(".in_all, ." + id).show()
    });
    // get work type and emulate click on the radio button
    var work_type_radio = $('#input_fields input:radio:checked')
    if (work_type_radio.length) {
        work_type_radio.change().click()
    }
    else { // if work type is not specified, check journal. Reason: otherwise there are too many scary fields
        $('#input_fields #in_journal').change().click()
    }

    $("#raw_string").mouseup(function() {
        var txt = getSelectedText()
        if (txt != "") {
            $("#paste_selection").fadeIn("slow")
        }
    });
    $("body").click(function(event) {
        var txt = getSelectedText()
        if (txt == "")
            $("#paste_selection").fadeOut("slow")
    });

    // remove submit on enter for all autocomplete fields
    $("#input_authors, #input_editors, #input_journal")
    .add("#input_collection, #input_publisher_name").keypress(function(e){
        if(e.which == 13) {
            e.preventDefault();
            return false
        }
        return true
    })
    $("#input_authors").autocomplete(generate_workers_autocomplete_options("/workers/search/"))
    $("#input_editors").autocomplete(generate_workers_autocomplete_options("/collections/editors/search/"))
    $("#input_journal").autocomplete({source: "/journals/search/autocomplete/"})
    $("#input_series").autocomplete({source: "/collections/series/search/"})
    $("#input_collection").autocomplete({
        focus: function(){
            return false
        },
        select: function(event, ui){
            $( "#input_collection" ).val(ui.item.title)
            if (ui.item.publisher_id) {
                $("#input_publisher_name").val(ui.item.publisher_name)
                $("#input_publisher_city").val(ui.item.publisher_city)
            } else if (ui.item.location) {
                $("#input_publisher_name").val(ui.item.location)
            }
            if (ui.item.series_id) {
                $("#input_series").val(ui.item.series_title)
            }
            if (ui.item.volume) {
                $("#input_volume").val(ui.item.volume)
            }
            if (ui.item.year) {
                $("#input_year").val(ui.item.year)
            }
            return false
        },
        source: "/collections/search/"
    })
    $("#input_publisher_name").autocomplete({
        source: "/publishers/search/",
        focus: function(){
            return false
        },
        select: function(event, ui){
            $( "#input_publisher_name" ).val(ui.item.name)
            $( "#input_publisher_city" ).val(ui.item.city)
            return false
        }
    }).data( "autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + item.name + " (" + item.city + ")</a>" )
                .appendTo( ul );
        };


    // ####################################3
    // validation
    $("input[name=work_type]").change(function(event) {
        var id =$(this).attr('id')
        $(".in_journal, .in_collection, .in_book").find(":input").addClass("ignore")
        $(".in_all, ." + id).find(":input").removeClass("ignore")
    });
    $('#input_fields input:radio:checked').change().click()

    $("#input_fields").validate({
        rules: {
            collection: {
                required: true,
                maxlength: 255
            },
            publisher_city: {
                maxlength: 255
            }
        },
        messages: {
            title: "Укажите название",
            authors: "Укажите авторов",
            journal: "Укажите название журнала",
            collection: {
                required: "Укажите название сборника",
                maxlength: "Название сборника не должно быть длиннее 255 символов"
            },
            publisher_city: {
                maxlength: "Место издания не должно быть длиннее 255 символов"
            },
            book: "Укажите название книги",
            number:"Укажите корректный номер",
            volume:"Укажите корректный том",
            pages:"Укажите корректное количество страниц",
            year:"Укажите корректный год",
            abstract: "Укажите корректную аннотацию"
        },
        ignore: '.ignore',
        errorElement: "div",
        errorClass: "error",
        errorPlacement: function(error, element){
            error.prepend('<span class="ui-icon ui-icon-alert"></span>')
            element.parent().prepend(error)
        },
        highlight: function(element, errorClass, validClass){
            field_row = $(element).parents(".field-row")
            field_row.addClass("error-container")
            $("input", field_row).addClass(errorClass).removeClass(validClass)
        },
        unhighlight: function(element, errorClass, validClass){
            field_row = $(element).parents(".field-row")
            field_row.parent().removeClass("error-container")
            $("input", field_row.parent()).removeClass(errorClass).addClass(validClass)
            field_row.removeClass("error-container")
            $("input", field_row).removeClass(errorClass).addClass(validClass)
        },
        submitHandler: function(form) {
            $(form).find(":submit").each(function(index, elem) {
                $(elem).attr('disabled', true);
                setTimeout(function() {
                    $(elem).attr('disabled', false);
                }, 10000);
            });
            form.submit()
            return false
        }
    })
    // dirty hack to make alert icon persistent on error messages.
    // I don't know how to do this properly with jquery.validate plugin.
    $("div.error").live("mouseup mousedown blur focus focusin focusout", function() {
        if (!$(this).find("span.ui-icon").length)
            $(this).prepend('<span class="ui-icon ui-icon-alert"></span>')
    })
});




