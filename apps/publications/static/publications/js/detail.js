$(function() {
    // display publication in various formats
    var container = $("#in_format_container")
    var inner_container = $("#in_format_inner_container")
    $("#show_in_format_links a").click(function(event) {
        event.preventDefault()
        if ($(this).hasClass("active")) {
            container.hide()
            $("#show_in_format_links a").removeClass("active")
            return;
        }
        $("#show_in_format_links a").removeClass("active")
        var format = $(this).attr("class")
        var format_div = inner_container.find("." + format)
        var download_link = container.find("a." + format)
        var format_name = $(this).html()
        $("#show_in_format").removeClass("ui-autocomplete-loading-padded")
        $(this).addClass("active")
        if (!format_div.text().length) {
            $("#show_in_format").addClass("ui-autocomplete-loading-padded")
            $("#in_format_error").hide()
            format_div.load("format/" + format.replace("_", ".") + "/", function(response, status, xhr) {
                if (format == "word_xml") {
                    format_div.text(format_div.html()) // change html to text not to escape xml tags
                }
                $("#show_in_format").removeClass("ui-autocomplete-loading-padded")
                if (status == "success") {
                    inner_container.children().hide()
                    $("#in_format_download_links").children().hide()
                    $("#in_format_name").html(format_name)
                    container.show()
                    format_div.show()
                    download_link.show()
                }
                else if (status == "error") {
                    $("#in_format_error").show()
                }
            });
        }
        else {
            inner_container.children().hide()
            $("#in_format_download_links").children().hide()
            $("#in_format_name").html(format_name)
            container.show()
            format_div.show()
            download_link.show()
        }
    });
    $(".hide_in_format").click(function(event) {
        event.preventDefault()
        container.hide()
        $("#show_in_format_links a").removeClass("active")
    });

    // display citation in styles
    var citation_inner_container = $("#citation_inner_container")
    $("#show_in_style_links a").click(function(event) {
        event.preventDefault()
        $("#show_in_style_links a").removeClass("active")
        var style = $(this).attr("class")
        var style_div = citation_inner_container.find("." + style)
        $("#style_loading").removeClass("ui-autocomplete-loading-padded")
        $(this).addClass("active")
        $("#in_style_error").hide()
        citation_inner_container.children().hide()
        style_div.show()
        if (!style_div.text().length) {
            $("#style_loading").addClass("ui-autocomplete-loading-padded")
            style_div.load("style/" + style + "/", function(response, status, xhr) {
                $("#style_loading").removeClass("ui-autocomplete-loading-padded")
                if (status == "error") {
                    $("#in_style_error").show()
                }
            });
        }
    });
    $("#show_in_style_links a.gost").click()

    // delete publication
    $("a.delete_publication_initial").click(function(event) {
        event.preventDefault()
        $(this).hide()
        $(".delete_publication_question").show()
    });
    $("a.delete_publication_question").click(function(event) { // FIXME add event.preventDefault() outside 'if'?
        if ($(this).hasClass("cancel")) {
            event.preventDefault()
            $(".delete_publication_question").hide()
            $(".delete_publication_initial").show()
        }
    });

    // author publication
    $("a.author_publication_initial").click(function(event) {
        event.preventDefault()
        $(this).hide()
        $(".author_publication_question").show()
    });
    $("a.author_publication_question").click(function(event) { // FIXME add event.preventDefault() outside 'if'?
        if ($(this).hasClass("cancel")) {
            event.preventDefault()
            $(".author_publication_question").hide()
            $(".author_publication_initial").show()
        }
    });

});
