function menu_click(event) {
        $("#additional_form div.field").not('#attachment_container div.field').hide()
        $("#attachment_container").hide();
        $(".hmenu li").removeClass('active')
        $(event.target).closest('li').addClass('active')
}

$(function() {
    $(".hmenu li").click(function(event) {
        event.preventDefault()
    })

    $("#id_menu_abstract").click(function(event) {
        menu_click(event)
        $("#id_abstract").closest('.field').show()
        $("#id_doi").closest('.field').show()
    })

    $("#id_menu_article_tags").click(function(event) {
        menu_click(event)
        $("label[for=id_tags]").closest('.field').show()
    })
    $("#id_menu_add_project").click(function(event) {
        menu_click(event)
        $("#id_project").closest('.field').show()
    })

    $("#id_menu_add_file").click(function(event) {
        menu_click(event)
        $("#attachment_container").show();
        $("#attachment_container .delete_all_attachments_initial, .delete_attachment, .formset, .attachments_header").show();
    })

    $("#id_menu_article_tags_confirmation").click(function(event) {
        menu_click(event)
        $("label[for=id_tags_confirmation]").closest('.field').show()
    })

    $('li.active').trigger('click')
    if ($('.error-container').length) {
         $("#id_menu_add_file").trigger('click')
    }
});

