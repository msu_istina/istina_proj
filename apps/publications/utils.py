# -*- coding: utf-8 -*-
from subprocess import Popen, PIPE
import logging
import re
import pybtex
from django.template.loader import render_to_string
import os
from django.conf import settings

logger = logging.getLogger("publications.utils")


def split_authors(authors_string):
    u"""Split authors_string into list of triples. Dots are removed from initials.
    >>> split_authors(u"I.S.Udvarhelyi")
    [(u'Udvarhelyi', u'I', u'S')]
    >>> split_authors(u"Udvarhelyi    I.   S. ")
    [(u'Udvarhelyi', u'I', u'S')]
    >>> split_authors(u"Gatsonis C.A")
    [(u'Gatsonis', u'C', u'A')]
    >>> split_authors(u"A Smirnov")
    [(u'Smirnov', u'A', '')]
    >>> split_authors(u"A. Ivanov")
    [(u'Ivanov', u'A', '')]
    >>> split_authors(u"L. P. Kane")
    [(u'Kane', u'L', u'P')]
    >>> split_authors(u"Sidorov")
    [(u'Sidorov', '', '')]
    >>> split_authors(u"Kane E.")
    [(u'Kane', u'E', '')]
    >>> split_authors(u"Epstein, K P")
    [(u'Epstein', '', ''), (u'K', u'P', '')]
    >>> split_authors(u"Kane E")
    [(u'Kane', u'E', '')]
    >>> split_authors(u"Golomazov Denis")
    [(u'Golomazov', u'Denis', '')]
    >>> split_authors(u"Golomazov Denis Dmitrievich")
    [(u'Golomazov', u'Denis', u'Dmitrievich')]
    >>> split_authors(u"D D Golomazov")
    [(u'Golomazov', u'D', u'D')]
    >>> split_authors(u"D Golomazov")
    [(u'Golomazov', u'D', '')]
    >>> split_authors(u"Golomazov Denis D")
    [(u'Golomazov', u'Denis', u'D')]
    >>> print repr(split_authors(u"Голомазов Денис")).decode("unicode-escape") # hack for non-ascii strings, see http://stackoverflow.com/questions/1733414/how-do-i-include-unicode-strings-in-python-doctests and http://stackoverflow.com/questions/5648573/python-print-unicode-strings-in-arrays-as-characters-not-code-points
    [(u'Голомазов', u'Денис', '')]
    >>> print repr(split_authors(u"Голомазов Д Д")).decode("unicode-escape")
    [(u'Голомазов', u'Д', u'Д')]
    >>> print repr(split_authors(u"Д.А. Голомазов")).decode("unicode-escape")
    [(u'Голомазов', u'Д', u'А')]
    >>> split_authors(u"D.A. Golomazov")
    [(u'Golomazov', u'D', u'A')]
    >>> split_authors(u"D D Golomazov, D Golomazov")
    [(u'Golomazov', u'D', u'D'), (u'Golomazov', u'D', '')]
    >>> split_authors(u"Dynnikova G.Ya.")
    [(u'Dynnikova', u'G', u'Ya')]
    >>> split_authors(u"Dynnikova G Ya")
    [(u'Dynnikova', u'G', u'Ya')]
    >>> split_authors(u"Dynnikova G")
    [(u'Dynnikova', u'G', '')]
    >>> split_authors(u"Dynnikova Galina Ya")
    [(u'Dynnikova', u'Galina', u'Ya')]
    >>> split_authors(u"Dynnikova Ga Y")
    [(u'Dynnikova', u'Ga', u'Y')]
    >>> split_authors(u"Osipov Alexander P.")
    [(u'Osipov', u'Alexander', u'P')]
    >>> split_authors(u"Osipov Alexander P")
    [(u'Osipov', u'Alexander', u'P')]
    >>> print repr(split_authors(u"и др.")).decode("unicode-escape")
    [(u'и др.', '', '')]
    >>> print repr(split_authors(u"и др")).decode("unicode-escape")
    [(u'и др.', '', '')]
    >>> print repr(split_authors(u"Голомазов Д.Д. и др.")).decode("unicode-escape")
    [(u'Голомазов', u'Д', u'Д'), (u'и др.', '', '')]
    >>> print repr(split_authors(u"Голомазов Д.Д. и др")).decode("unicode-escape")
    [(u'Голомазов', u'Д', u'Д'), (u'и др.', '', '')]
    >>> print repr(split_authors(u"Голомазови др.")).decode("unicode-escape")
    [(u'Голомазови', u'др', '')]
    >>> print repr(split_authors(u"Голомазови др")).decode("unicode-escape")
    [(u'Голомазови', u'др', '')]
    >>> split_authors(u"et al.")
    [(u'et al.', '', '')]
    >>> split_authors(u"et al")
    [(u'et al.', '', '')]
    >>> split_authors(u"D.D. Golomazov et al.")
    [(u'Golomazov', u'D', u'D'), (u'et al.', '', '')]
    >>> split_authors(u"D.D. Golomazov et al")
    [(u'Golomazov', u'D', u'D'), (u'et al.', '', '')]
    >>> split_authors(u"Golomazovet al.")
    [(u'Golomazovet', u'al', '')]
    >>> split_authors(u"Golomazovet al")
    [(u'Golomazovet', u'al', '')]
    >>> split_authors(u"Elena S.Belenkaya")
    [(u'Belenkaya', u'Elena', u'S')]
    >>> split_authors(u"Elena S. Belenkaya")
    [(u'Belenkaya', u'Elena', u'S')]
    >>> split_authors(u"Elena S. Li") # I know that it works incorrectly because it conflicts with "Dynnikova G. Ya"
    [(u'Elena', u'S', u'Li')]
    >>> split_authors(u"A. S. Semisalova, *")
    [(u'Semisalova', u'A', u'S'), (u'*', '', '')]
    >>> split_authors(u"I.Ya. Erukhimovich")
    [(u'Erukhimovich', u'I', u'Ya')]
    >>> split_authors(u"Yu.Ya. Erukhimovich")
    [(u'Erukhimovich', u'Yu', u'Ya')]
    >>> split_authors(u"Yu.I. Erukhimovich")
    [(u'Erukhimovich', u'Yu', u'I')]
    >>> split_authors(u"Monica Olvera de la Cruz")
    [(u'de la Cruz', u'Monica', u'Olvera')]
    >>> split_authors(u"Monica Olvera Valdez Marquez Perez")
    [(u'Valdez Marquez Perez', u'Monica', u'Olvera')]
    >>> split_authors(u"de la Cruz Monica Olvera")
    [(u'de la Cruz', u'Monica', u'Olvera')]
    >>> split_authors(u"M.O. de la Cruz")
    [(u'de la Cruz', u'M', u'O')]
    >>> split_authors(u"M.O. Valdez Marquez Perez")
    [(u'Valdez Marquez Perez', u'M', u'O')]
    >>> split_authors(u"de la Cruz M.O.")
    [(u'de la Cruz', u'M', u'O')]
    >>> split_authors(u"Valdez Marquez Perez M.O.")
    [(u'Valdez Marquez Perez', u'M', u'O')]
    >>> split_authors(u"Monica O. de la Cruz")
    [(u'de la Cruz', u'Monica', u'O')]
    >>> split_authors(u"de la Cruz Monica O.")
    [(u'de la Cruz', u'Monica', u'O')]
    >>> split_authors(u"Valdez Marquez Perez Monica O.")
    [(u'Valdez Marquez Perez', u'Monica', u'O')]
    >>> split_authors(u"Monica O. Valdez Marquez Perez")
    [(u'Valdez Marquez Perez', u'Monica', u'O')]
    >>> split_authors(u"Alessandro de Luca")
    [(u'de Luca', u'Alessandro', '')]
    >>> split_authors(u"de Luca Alessandro")
    [(u'de Luca', u'Alessandro', '')]
    >>> split_authors(u"Essery R.L.H.")
    [(u'Essery', u'R', u'L.H.')]
    >>> split_authors(u"R.L.H. Essery")
    [(u'Essery', u'R', u'L.H.')]
    >>> split_authors(u"Woo M.-k.")
    [(u'Woo', u'M', u'-k')]
    >>> split_authors(u"Dal Cappello C.")
    [(u'Dal Cappello', u'C', '')]
    >>> split_authors(u"Luca Alessandro")
    [(u'Luca', u'Alessandro', '')]
    >>> split_authors(u"Elena Belenkaya, Maxim Khodachenko") # deliberately incorrect, see #1002
    [(u'Elena', u'Belenkaya', ''), (u'Maxim', u'Khodachenko', '')]
    >>> split_authors(u"Belenkaya E., Khodachenko M.")
    [(u'Belenkaya', u'E', ''), (u'Khodachenko', u'M', '')]
    >>> split_authors(u"Belenkaya E.; Khodachenko M.") # split by semicolon
    [(u'Belenkaya', u'E', ''), (u'Khodachenko', u'M', '')]

    """
    # strip whitespaces
    authors_string = authors_string.strip()

    # check for "et al." string
    seps = [' ', ',', '(']
    add_et_al = False
    for suffix in [u"и др.", u"и др", u"et al.", u"et al"]:
        if authors_string.endswith(suffix) and (len(authors_string) == len(suffix) or authors_string[-(len(suffix) + 1)] in seps):
            authors_string = authors_string[:-(len(suffix) + 1)]
            add_et_al = suffix
            if not add_et_al.endswith("."):
                add_et_al += '.'
            break

    def parser(author):
        subparts = ["de", "la", "van", "von", "der", "dal"]
        for part in subparts: # secure subparts by replacing space with comma temporarily
            author = re.sub(r"(\b{0}) ".format(part), r"\1,", author)
            author = re.sub(r"(\b{0}) ".format(part.capitalize()), r"\1,", author)

        sep = re.compile(r'[ .]+') # dot or a whitespace
        names = filter(bool, sep.split(author.strip())) # split the string by dots and whitespaces
        if not names:
            return None

        names = [name.replace(",", " ") for name in names] # restore spaces instead of commas

        # lastname is the first item, or the last one. Determine it.
        # if lastname is going first, order is reverse, otherwise normal.
        reverse = True
        conditions = [
            len(names[0]) <= 2 and len(names[-1]) > len(names[0]),
            len(names) >= 3 and len(names[-1]) > 2 and len(names[1]) <= 2,
            any(part + " " in names[-1] for part in subparts),
            len(names) > 3 and all(len(name) > 2 for name in names)
        ]
        if any(conditions):
            reverse = False

        if len(names) <= 3:
            last = names.pop(0 if reverse else -1) # pop first or last item
            first = names.pop(0) if names else ""
            middle = names.pop() if names else ""
        else:
            last = []
            middle = ""
            if reverse:
                first = ""
                for i, name in enumerate(names):
                    if last and (len(name) == 1 or i >= len(names) - 2): # last is over
                        if not first:
                            first = name
                        else: # first is over, remaining to middle; and add dots for initials
                            middle += name
                            if len(name) == 1 and len(names) - len(last) - 1 > 1: # add dot if this is not the single letter
                                middle += "."
                            elif i < len(names) - 1:
                                middle += " "
                    else:
                        last.append(name)
            else:
                first = names.pop(0)
                flag_middle = True
                middles = []
                for name in names:
                    if flag_middle and middles and len(name) > 1:
                        flag_middle = False
                    if flag_middle:
                        middles.append(name)
                    else:
                        last.append(name)
                for i, name in enumerate(middles):
                    middle += name
                    if len(name) == 1 and len(middles) > 1:
                        middle += "."
                    elif i < len(middles) - 1:
                        middle += " "
            last = " ".join(last)
        return (last, first, middle)

    authors_string_parts = authors_string.split(",")
    if len(authors_string_parts) == 1: # the string contains no commas with non-empty parts
        # try to split by semicolon
        authors_string_parts = authors_string.split(";")

    authors = filter(bool, map(parser, authors_string_parts))
    if add_et_al:
        authors.append((add_et_al, '', ''))
    return authors


def bibtex2html(bibtex, style='gost'):
    '''Converts bibtex to html using style specified.'''
    if not bibtex:
        return ""
    if style in ["plain", "amsplain"]: # encode, see #1075
        encoding = 'utf-8'
    else:
        encoding = 'koi8-r'
    parameters = ["istina_bibtex2html"]
    if style == 'gost':
        # add an environment variable indicating where to search for .bst style files        
        os.environ['BSTINPUTS'] = os.path.join(settings.WC_DIR, 'env', 'bin')
        parameters.append('gost71u') # this is the bst style file
        macros_file = os.path.join(settings.WC_DIR, 'env', 'bin', 'cyrillic.macros.tex')
        parameters.append(macros_file)
    else:
        # reset the environment variable, since it is stored per-server, and is kept
        os.environ.pop("BSTINPUTS", None)
        parameters.append(style)
    bibtex_encoded = bibtex.encode(encoding, 'xmlcharrefreplace')
    process = Popen(parameters, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    logger.debug("Bibtex2html: %s, %d", bibtex, len(bibtex))
    html = process.communicate(input=bibtex_encoded)[0]
    # decode
    html = html.decode(encoding, 'ignore')
    # cut ads links from html
    html = re.sub('\n', ' ', html)
    html = re.sub(r'<hr>.*', '', html)
    if style == 'siam': # fix bug #290 with extra symbols
        html = html.replace("height 2pt depth -1.6pt width 23pt", "&nbsp;" * 10)
    if style == 'gost':
        # replace the symbols that could not be added in 8bit encoding
        html = html.replace("latex__textnumero", u"№")
    logger.debug("Bibtex2html html: %s, %d", html, len(html))
    return html

def bibtex2html_multiple(publications, style='gost'):
    """
    Takes a list of publications, gets bibtexs for each of them, 
    combines it into a single string, applies bibtex2html, then
    splits the result into individual html fragments.
    """
    full_bibtex = "\n".join([publication.bibtex for publication in publications])
    html = bibtex2html(full_bibtex, style)
    # split resulting html and strip extra html
    regexp1 = r'\s*</td> </tr>   <tr valign="top"> <td align="right" class="bibtexnumber"> \[<a name="[^"]+">[^<]+</a>\] </td> <td class="bibtexitem">\s*'
    regexp2 = r'\s*<table>  <tr valign="top"> <td align="right" class="bibtexnumber"> \[<a name="[^"]+">[^<]+</a>\] </td> <td class="bibtexitem">\s*'
    regexp3 = r'\s*</td> </tr> </table>\s*'
    regexp = "|".join((regexp1, regexp2, regexp3))
    html_records = filter(bool, re.split(regexp, html))
    if len(html_records) != len(publications):
        raise Exception("Bibtex2html returned not the same number of records as requested")
    return html_records

def bibtex2any(bibtex, format_extension):
    '''Converts bibtex to format specified by extension.'''
    if not bibtex:
        return None
    extensions = ['ads', 'enw', 'isi', 'ris', 'word.xml']
    formats = ['ads', 'end', 'isi', 'ris', 'wordbib'] # parameters used in external scripts
    try:
        format = formats[extensions.index(format_extension)]
    except:
        return None
    process = Popen(["istina_bibtex2any", format], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    return process.communicate(input=bibtex.encode('utf-8'))[0]


def pybtex_entry2bibtex(entry):
    '''Converts pybtex entry object into a unicode representation.'''
    if not isinstance(entry, pybtex.core.Entry):
        return entry
    # field names are needed for correct order of fields
    field_names = ('author', 'title', 'journal', 'booktitle', 'year', 'volume', 'number', 'editor', 'series', 'pages', 'isbn', 'publisher', 'address', 'annote', 'url')
    fields = []
    for key in field_names:
        try:
            fields.append((key, entry.fields[key]))
        except KeyError:
            pass
    for key in set(entry.fields.keys() + entry.persons.keys()) - set(field_names): # add special keys that are not in standard set
        fields.append((key, entry.fields[key]))
    return render_to_string("publications/pybtex_entry2bibtex.bib", {'entry': entry, 'fields': fields})

def guess_language(text):
    u"""
    Returns language of the text. Currently works only for russian and english, and is very dumb.
    If the text contains any russian letter, then it's russian. Otherwise, english :)
    Install guess_language library (http://stackoverflow.com/a/2026586/304209) to improve quality.
    >>> guess_language(u'привет')
    'russian'
    >>> guess_language(u'ПРИВЕТ')
    'russian'
    >>> guess_language(u'hello')
    'english'
    >>> guess_language(u'HELLO')
    'english'
    """
    regexp = ur'.*[а-яА-Я].*'
    if re.match(regexp, text):
        return "russian"
    else:
        return "english"


if __name__ == "__main__":
    import doctest
    doctest.testmod()
