# -*- coding: utf-8 -*-
from django import template
from django.template.defaultfilters import stringfilter
from publications.models import get_bibtex_styles

register = template.Library()


@register.filter
@stringfilter
def escape_bibtex(text):
    """Escapes special characters for bibtex representation.
        Partially taken from http://bibliographie-trac.ub.rub.de/browser/bibtex_util.py
        Partially taken from http://soar.googlecode.com/svn-history/r12491/trunk/SoarSuite/ManualSource/wikicmd/moin2latex.py
        Special characters should be replaced with HTML Ascii codes: http://www.w3schools.com/tags/ref_ascii.asp
        Though we should replace only characters that cause bibtex2html to fail. 
        Do not replace all characters (e.g. $), since they can be included in formulas and be formatted using bibtex nicely.
        Includes fixes for curly braces.
        #&\$ characters don't cause any problems (as far as I tested), so we may leave them as-is, so that formulas will be rendered nicely.
        The thing is we'd like to allow all symbols, so that bibtex would render latex formulas nicely,
        but if this formulas are written in wrong way (or curly braces are not matched, for example), bibtex2html fails, and we must avoid failing at all costs.
    """  
    text = text.replace('_', r'\_')
    text = text.replace('%', r'\%')

    text = text.replace('{', r'&#123;')
    text = text.replace('}', r'&#125;')
    
    text = text.replace('~', '$\\sim$')

    text = text.replace(u'"', '{"}')
    text = text.replace(u'ü', '{\\"u}')
    text = text.replace(u'Ü', '{\\"U}')
    text = text.replace(u'ä', '{\\"a}')
    text = text.replace(u'Ö', '{\\"O}')
    text = text.replace(u'Ä', '{\\"A}')
    text = text.replace(u'ö', '{\\"o}')
    text = text.replace(u'ß', '{\\"s}')
    text = text.replace(u'é', "{\\'e}")
    text = text.replace(u'è', "{\\`e}")
    text = text.replace(u'É', "{\\'E}")
    text = text.replace(u'á', "{\\'a}")
    text = text.replace(u'à', "{\\`a}")
    text = text.replace(u'À', "{\\`A}")
    text = text.replace(u'ó', "{\\'o}")
    text = text.replace(u'ò', "{\\`o}")
    text = text.replace(u'ú', "{\\'u}")
    text = text.replace(u'ù', "{\\`u}")
    text = text.replace(u'í', "{\\'i}")
    text = text.replace(u'ì', "{\\`i}")
    text = text.replace(u'â', "{\\^a}")
    text = text.replace(u'ô', "{\\^o}")
    text = text.replace(u'û', "{\\^u}")
    text = text.replace(u'ê', "{\\^e}")
    text = text.replace(u'î', "{\\^i}")
    text = text.replace(u'ç', "{\\cc}")
    text = text.replace(u'ñ', "{\\~n}")
    text = text.replace(u'§', "$\\S$")
    text = text.replace(u'°', "$\\circ$")
    text = text.replace(u'²', "$\\^2$")
    text = text.replace(u'³', "$\\^3$")
    text = text.replace(u'Ù18O', "$\\beta$")
    text = text.replace(u'µ', "$\\mu$")
    text = text.replace(u'«', "{\\flqq}")
    text = text.replace(u'»', "{\\frqq}")
    text = text.replace(u'Å', "{\\AA}")

    return text


def show_publication_list_links(context, object_id, object_url, bibtex_url, style_url):
    '''Renders a template with export and display options of publication list.'''

    return {'object_id': object_id, 'object_url': object_url, 'bibtex_url': bibtex_url, 'style_url': style_url, 'bibtex_styles': get_bibtex_styles(), 'current_style': context.get('style', None), 'STATIC_URL': context.get('STATIC_URL', None)}


# takes_context is needed to get access to style variable in the context, identifying the current style
register.inclusion_tag('publications/publication_list_links.html', takes_context=True)(show_publication_list_links)
