# -*- coding: utf-8 -*-
from django import template
from statistics.models import ExternalArticle, TopArticle, ConfirmedArticle

register = template.Library()

def articles_with_full_text(worker):
    '''Returns list of ids of all full-text articles authored by worker.'''
    from django.db import connection
    cursor = connection.cursor()
    try:
        cursor.execute("""
        select f_object_id
        from files
        where (f_files_type is null or f_files_type = 'full_text')
        and f_object_id in (
             select f_article_id from authora where f_man_id = %s
           )
        and f_contenttype_id in (
             select id from django_content_type where model = 'article'
           )
        """, [worker.id])
        res = [item[0] for item in cursor]
        return res
    except Exception:
        pass
    finally:
        cursor.close()
    return []


@register.assignment_tag
def get_article_list(articles, worker, all_in_systems=False, all_top=False, hide_top=False,
    hide_confirmed=False, set_confirmed=False, confirmed_by_user=False):
    article_ids = [article.id for article in articles]
    articles_in_systems = ExternalArticle.objects.select_from(article_ids) if not all_in_systems else []
    top_articles = TopArticle.objects.select_from(article_ids) if not all_top and not hide_top else []
    confirmed_articles_by_user = []
    worker_full_text_articles = articles_with_full_text(worker) if worker else []
    if not hide_confirmed or set_confirmed:
        # if confirmed_by_user is not False (and is a User instance),
        # we show as confirmed only articles confirmed by that user
        if confirmed_by_user:
            confirmed_articles_by_user = ConfirmedArticle.objects.select_from(article_ids, user=confirmed_by_user)
        if not confirmed_by_user or not hide_confirmed:
            confirmed_articles = ConfirmedArticle.objects.select_from(article_ids)
    else:
        confirmed_articles = []
    for article in articles:
        in_systems = article.id in articles_in_systems if not all_in_systems else True
        confirmed = article.id in confirmed_articles if not hide_confirmed else False
        full_text = article.id in worker_full_text_articles if worker else article.has_full_text
        if hide_top:
            top = False
        elif all_top:
            top = True
        else:
            top = article.id in top_articles
        article.icons = {
            'in_systems': in_systems,
            'top': top,
            # here we set global confirmed status, based on any user's confirmation
            'confirmed': confirmed,
            'full_text': full_text,
        }
        if set_confirmed:
            # here we set user confirmed status, based only on current user's confirmation
            if confirmed_by_user:
                article.confirmed = article.id in confirmed_articles_by_user
            else:
                article.confirmed = article.id in confirmed_articles
    return articles
