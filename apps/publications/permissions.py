# -*- coding: utf-8; -*-
import authority
from common.permissions import MyModelPermission, LinkedToWorkersModelPermission
from unified_permissions import has_permission
from publications.models import Publication
from publications.models import Article

class PublicationPermission(LinkedToWorkersModelPermission):
    label = 'publication_permission'
    operations = ('check_edit_additional',)

    def check_edit_additional(self, article):
        return self.check_edit(article)


class ArticlePermission(LinkedToWorkersModelPermission):
    label = 'article_permission'
    operations = ('check_edit_additional',)

    def check_edit_additional(self, obj):
        """Checks additional info required to edit the object.
        
        This should be identical to self.check_edit (defined in
        /common/permissions), but without obj.is_locked check."""
        #---FIXME(serg): find a way to call obj.check_edit with
        #redefined is_locked property
        return obj.id and (
            has_permission(self.user, "is_editor")
            or obj.is_creator_managed_by(self.user)
            or obj.is_linked_managed_by(self.user)
            or ((obj.is_created_by(self.user) or obj.is_linked_to(self.user)) )
        )

authority.register(Publication, PublicationPermission)
authority.register(Article, ArticlePermission)
