# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals
from django.core.management.base import BaseCommand
from django.test.client import Client

import cProfile as profile
import logging
import re
import time


class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            loops = int(args[0])
            if not 0 < loops:
                raise Exception("error")
        except:
            loops = 10
        do_profile = "p" in args
        skip_first = "s" in args

        logger = logging.getLogger("benchmark")
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        logger.addHandler(ch)

        c = Client()
        response = c.post('/accounts/login/', {'username': 'anton', 'password': 'nSrxd60wS9es9Szw5fIN5Wa8I'})
        if not response.status_code < 400:
            raise Exception(response.status_code)
        response = c.post('/publications/add/step2/', POST_DATA_2)
        if not response.status_code < 400:
            raise Exception(response.status_code)
        post = POST_DATA_3.copy()
        post.update(find_hidden_attrs(response.content))
        times = []
        if skip_first:
            response = c.post('/publications/add/step3/', post)
        t = time.time()
        for i in range(loops):
            print()
            print(" step {0} ".format(i).center(80, "#"))
            if do_profile:
                f = "/home/anton/step3.prof"
                profile.runctx("c.post('/publications/add/step3/', post)", globals(), locals(), f)
                print("Saved profile in", f)
                break
            else:
                response = c.post('/publications/add/step3/', post)
                if not response.status_code < 400:
                    raise Exception(response.status_code)
            times.append(time.time() - t)
            t = time.time()
        print("==========================")
        print(__file__)
        print("Loops: %s" % loops)
        print("Total time: %f" % sum(times))
        print("Avg time: %f" % (sum(times) / loops))


def find_hidden_attrs(content):
    attrs = {}
    attrs["parsing_event-id"] = re.search("""<input type="hidden" name="parsing_event-id" value="(\d+)" id="id_parsing_event-id" />""", content).group(1)
    attrs["xml_hash"] = re.search("""<input type="hidden" value="(.*)" name="xml_hash" />""", content).group(1)
    return attrs


POST_DATA_2 = {
"citation": "В.А. Васенин, С.А. Афонин, А.С. Козицын. Автоматизированная система тематического анализа информации. Информационные технологии, 4, 1-32, 2009.",
}
POST_DATA_3 = {
"work_type": "in_journal",
"authors": "Васенин В.А., Афонин С.А., Козицын А.С.",
"title": "Автоматизированная система тематического анализа информации",
"journal": "Информационные технологии",
"collection": "",
"series": "",
"volume": "",
"number": "4",
"publisher_name": "",
"publisher_city": "",
"institution": "",
"year": "2009",
"firstpage": "1",
"lastpage": "32",
"pages": "32",
"isbn": "",
"abstract": "",
}
POST_DATA_3.update(POST_DATA_2)
