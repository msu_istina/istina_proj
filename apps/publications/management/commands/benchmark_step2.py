# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.test.client import Client
import time

from publications.views import parse_data

class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            loops = int(args[0])
            if not 0 < loops:
                raise Exception("error")
        except:
            loops = 10
        c = Client()
        response = c.post('/accounts/login/', {'username': 'anton', 'password': 'nSrxd60wS9es9Szw5fIN5Wa8I'})
        if not response.status_code < 400:
            raise Exception(response.status_code)
        times = []
        t = time.time()
        for i in range(loops):
            c.post('/publications/add/step2/', {'citation':u'В.А. Васенин, С.А. Афонин, А.С. Козицын. Автоматизированная система тематического анализа информации. Информационные технологии, 4, 1-32, 2009.'})
            times.append(time.time() - t)
            t = time.time()
        print("==========================")
        print(__file__)
        print("Loops: %s" % loops)
        print("Total time: %f" % sum(times))
        print("Avg time: %f" % (sum(times) / loops))

        
        


