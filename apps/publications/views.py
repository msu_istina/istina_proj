# -*- coding: utf-8; -*-
from collections import namedtuple
import copy
import hashlib
import datetime
import logging
try:
    import cPickle as pickle
except ImportError:
    import pickle

from actstream import action
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404, redirect
from django.shortcuts import render_to_response as _render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import simplejson
from django.utils.datastructures import SortedDict
from django.contrib.contenttypes.models import ContentType
from django.db.models import Count, Max

from common.forms import make_attachment_formset
from common.models import Attachment, AuthorScience, AuthorRoleObject, AuthorRole
from common.utils.files import process_attachments
from common.utils.user import get_worker
from common.utils.cutoff import CutoffTimer
from common.utils.context import render_to_response
from common.utils.slughifi import slughifi
from common.utils.uniqify import uniqify
from common.utils.doi import DoiServiceError

from common.views import delete_attachments
from publications.forms import FORM_TYPES, ArticleAdditionalForm, \
    BookAdditionalForm, ParsingEventForm
from icollections.models import CollectionCategory
from publications.models import get_publication_by_type, \
    Article, Book, BookCategory, ParsingEvent
from publications.parsers.parse import apply_parsers
from publications.utils import bibtex2html, split_authors
from publishers.forms import PublisherForm
from statistics.models import ExternalArticleSearchLog, ExternalArticleSearchRequest
from workers.forms import *
from workers.models import Worker
from unified_permissions import has_permission


logger = logging.getLogger("publications.views")


def generate_workers_form(request, initial_field, prefix, required=False, required_error_msg=None, save_publication=False, object_instance=None):
    """Generate workers fieldset

        populated by comma-separated string from  request.POST[initial_field]
        prefixed with prefix
        save_publication indicates the mode: for rendering step 3 or for saving data from step 3 in the database.
    """
    kwargs = dict(required=required, required_error_msg=required_error_msg)
    try:
        kwargs["profile_worker"] = request.user.get_profile().worker
    except AttributeError:
        pass
    kwargs["allow_unknown"] = True
    kwargs["user"] = request.user
    kwargs["object_instance"] = object_instance
    initial = split_authors(request.POST[initial_field])
    f = lambda i: dict(zip(('lastname', 'firstname', 'middlename',), i))
    if save_publication:
        form = WorkerFormSet(initial=map(f, initial), data=request.POST, prefix=prefix, **kwargs)
    else:
        form = WorkerFormSet(initial=map(f, initial), prefix=prefix, **kwargs)
        form.calculate_similar()
    return form


def _get_work_or_404(work_type, work_id):
    """Get work instance or raise Http404"""
    if work_type not in ("book", "article"):
        raise Http404
    work_model = Book if work_type == "book" else Article
    work = get_object_or_404(work_model, pk=work_id)
    return work


def _edit_work_access_check(request, work):
    """Check, if request.user can edit work

    return
        (True, None) on success
        (False, response) on fail
    """
    if has_permission(request.user, "edit", work):
        return (True, None)
    context = {
        "action": "редактирования этой публикации",
        "description": "Редактировать публикацию могут только её авторы,"
                       " а также добавивший её сотрудник.",
    }
    return (False, render_to_response(request, '403.html', context))


@login_required
def edit_work(request, publication_type, publication_id):
    work = _get_work_or_404(publication_type, publication_id)
    checked, response = _edit_work_access_check(request, work)
    if not checked:
        return response
    fields = [
        ("authors", "authors_string"),
        "title",
        "theses",
        ("journal", "journal_name"),
        ("collection", "collection_title"),
        ("series", "series_title"),
        ("volume", "get_volume"),
        ("number", "get_number"),
        ("publisher", "publisher_name"),
        ("location", "publisher_or_location"),
        "institution",
        ("year", "get_year"),
        "firstpage",
        "lastpage",
        ("pages", "get_pages_max_number"),
        "redpages",
        ("isbn", "get_isbn"),
        ("ncopies", "get_ncopies"),
        ("note", "abstract")
    ]
    first_item = lambda x: x[0] if isinstance(x, tuple) else x
    second_item = lambda x: x[1] if isinstance(x, tuple) else x
    data = dict((first_item(field), getattr(work, second_item(field), '') or '') for field in fields)
    if publication_type == "book":
        data["work_type"] = "book"
    elif work.journal:  # article in journal
        data["work_type"] = "journal"
    else:  # article in collection
        data["work_type"] = "collection"
    if hasattr(work, 'categories'):
        for category in work.categories.all():
            data['book_category_%d' % category.id] = True
    if getattr(work, 'collection', None):
        for category in work.collection.categories.all():
            data['collection_category_%d' % category.id] = True
    context = {"fields": data, "edit_publication_id": publication_id, 'book_categories': BookCategory.objects.all(), 'collection_categories': CollectionCategory.objects.all()}
    return render_to_response(request, 'publications/save_work.html', context)


@login_required
def add_work(request, save_publication=False):
    """Saves a work into a database.

    also
        - renders "go back to step 2" button
        - work editing step3 (if "edit_work_id" in POST)
    if save_publication is False, this is a request for rendering step 3 with data from step 2.
    if save_publication if True, this is a request *from* step 3 with final data to be saved and step 4 to be shown.
    """
    timer = CutoffTimer(None)
    start_time = timer.cutoff("start")

    parsing_event_form = ParsingEventForm(request.POST, prefix="parsing_event")

    # if "go back to step2" clicked ...
    if "goback" in request.POST:
        fields = copy.copy(request.POST)
        fields['work_type'] = fields.get('work_type', '')[3:]
        fields['publisher'] = fields.get('publisher-publisher_name', '')
        fields['location'] = fields.get('publisher-publisher_city', '')
        fields['theses'] = fields.get('theses', '') == '1'
        fields['note'] = fields.get('abstract', '')
        for category in BookCategory.objects.all():
            if fields.get('book_category_%d' % category.id) == u"False":
                fields['book_category_%d' % category.id] = False
        for category in CollectionCategory.objects.all():
            if fields.get('collection_category_%d' % category.id) == u"False":
                fields['collection_category_%d' % category.id] = False
        context = {
            "edit_publication_id": request.POST.get('edit_publication_id', ''),
            "fields": fields,
            "xml_hash": request.POST.get('xml_hash', ''),
            "parsing_event_form": parsing_event_form,
            "book_categories": BookCategory.objects.all(),
            "collection_categories": CollectionCategory.objects.all(),
        }
        return render_to_response(request, 'publications/save_work.html', context)

    # determine work_type and required form class
    work_type = request.POST.get("work_type", None)
    publication_type = 'article' if work_type in ("in_journal", "in_collection") else 'book'
    work_form_class = dict(FORM_TYPES).get(work_type, None)
    logger.debug("Got POST request: %s", request.POST)
    timer.cutoff("detect_types")

    # check xml_hash correctness if add work
    correct_xml_hash = hashlib.md5(request.session.get('xml', '')).hexdigest()
    if work_form_class is None \
            or request.method == 'GET' \
            or ("edit_publication_id" not in request.POST
                and request.POST.get('xml_hash', '') != correct_xml_hash):
        return redirect("publications_add_step2")

    # check work id correctness if edit work
    if "edit_publication_id" in request.POST:
        edit_mode = True
        if publication_type == 'article':
            edit_work_type = "article"
            other_work_type = "book"
        else:
            edit_work_type = "book"
            other_work_type = "article"
        try:
            edit_work = _get_work_or_404(edit_work_type, request.POST["edit_publication_id"])
        except Http404:  # the work type of the publication could have changed, try another type
            # if found, edit_work_type will be the new work type, while other_work_type = the old work type
            edit_work = _get_work_or_404(other_work_type, request.POST["edit_publication_id"])
            work_type_changed = True
        else:
            work_type_changed = False
        old_work_type = other_work_type if work_type_changed else edit_work_type
        checked, response = _edit_work_access_check(request, edit_work)
        if not checked:
            return response
    else:
        edit_work = None
        edit_mode = False
    timer.cutoff("special_cases")

    # strip spaces and dots in string fields in the POST in save in a post variable
    post = copy.copy(request.POST)
    for field in ("journal", "collection", "title", "series",
                  "publisher_name", "publisher_city"):
        if field in post:
            post[field] = post[field].strip(". ")
    post['project'] = None
    timer.cutoff("clean_post")

    # create and fill forms
    main_form = work_form_class(post, user=request.user)
    timer.cutoff("create_forms_main")
    authors_form = generate_workers_form(
        request, "authors", "authors", True,
        u"Укажите хотя бы одного автора", save_publication, object_instance=edit_work)
    timer.cutoff("create_forms_authors")
    if work_type in ("in_collection", "in_book"):
        publisher_prefix = "publisher"
        if not save_publication:  # i.e. submit from step2
            for field in "publisher_name", "publisher_city":
                field_new = "%s-%s" % (publisher_prefix, field)
                post[field_new] = post[field]
        publisher_form = PublisherForm(prefix=publisher_prefix, data=post)
    else:
        publisher_form = None
    forms = SortedDict(dict(main=main_form, authors=authors_form, publisher=publisher_form))
    timer.cutoff("create_forms_other")

    # check validity and save data
    valid = [f.is_valid() for f in forms.itervalues() if f is not None]
    if all(valid) and save_publication:  # save_publication means that this is the final information from step 3, not from step 2.
        kwargs = {}
        authors_form.save()
        kwargs['authors'] = authors_form.worker_instances
        kwargs['user'] = request.user
        if work_type in ("in_collection", "in_book"):
            kwargs["publisher"], kwargs["location"] = publisher_form.find_publisher()
        old_bibtex = ""
        if edit_work:
            kwargs["edit_work"] = edit_work
            old_bibtex = edit_work.bibtex
        main_form.save(**kwargs)

        # save creator user
        # save pre-edited and post-edited fields to xml
        if main_form.work:
            if not edit_work or work_type_changed:
                main_form.work.creator = request.user

            realdata = {
                "authors": authors_form.cleaned_data,
                "main": main_form.cleaned_data,
                "date": str(datetime.datetime.now()),
            }
            if publisher_form is not None:
                realdata["publisher"] = publisher_form.cleaned_data
            try:
                if edit_work:
                    parsed = simplejson.loads(edit_work.xml)["parsed"]
                else:
                    parsed = pickle.loads(request.session['xml'])
            except:
                logger.warning("Exception during xml (de)serialization occurred", exc_info=True)
                parsed = ""
            data = {"realdata": realdata, "parsed": parsed, }

            # log parsing dissatisfied event
            if parsing_event_form.is_valid():
                # the form is linked to an existing ParsingEvent instance (via 'id' hidden input)
                # we update its user_data and its type,
                # if the user has checked "dissatisfied" checkbox
                parsing_event_id = parsing_event_form.cleaned_data.get("id")
                if parsing_event_id:
                    try:
                        parsing_event = ParsingEvent.objects.get(id=parsing_event_id)
                    except:
                        pass
                    else:
                        parsing_event.user_data = realdata
                        if parsing_event_form.cleaned_data.get("dissatisfied"):
                            parsing_event.type = "dissatisfied"
                            if request.user:
                                action.send(request.user, verb=u'не доволен результатами разбора', action_object=main_form.work, target=parsing_event)
                        parsing_event.save()
            try:
                data = simplejson.dumps(data)
            except:
                logger.warning("Exception during xml json'isation occurred", exc_info=True)
                data = ""
            main_form.work.xml = data
            main_form.work.save()  # FIXME-greg Remove double saving of new books\article
            request.session['xml'] = ""

            if edit_work:
                work = main_form.work
                if not work_type_changed:
                    action.send(request.user, verb=u"отредактировал %s" % work.accusative, target=work, description="%s\n->\n%s" % (old_bibtex, work.bibtex))
                else:
                    action.send(request.user, verb=u"отредактировал %s" % edit_work.accusative, action_object=work, target=edit_work, description="%s\n->\n%s" % (old_bibtex, work.bibtex))
                    action.send(request.user, verb=u"удалил %s" % edit_work.accusative, target=edit_work, description="%s\n->\n%s" % (old_bibtex, work.bibtex))

        # log activities by means of actstream
        if hasattr(main_form, "article_instance"):
            work = main_form.article_instance
            work_type = u"Статья"
            if request.user:
                if main_form.article_created:  # FIXME-greg Remove double savings
                    action.send(request.user, verb=u'добавил %s' % work.accusative, target=work)
                if main_form.journal_alias_created:
                    action.send(request.user, verb=u'добавил псевдоним журнала', action_object=main_form.journal_alias_instance, target=main_form.journal_instance)
                if main_form.journal_created:
                    action.send(request.user, verb=u'добавил журнал', target=main_form.journal_instance)
                if main_form.series_created:
                    action.send(request.user, verb=u'добавил серию', target=main_form.series_instance)
                if main_form.collection_created:
                    action.send(request.user, verb=u'добавил сборник', target=main_form.collection_instance)
        elif hasattr(main_form, "book_instance"):
            work = main_form.book_instance
            work_type = u"Книга"
            if main_form.book_created:
                action.send(request.user, verb=u'добавил книгу', target=work)
        else:
            work = None
            work_type = u"Публикация"
        if request.user.is_authenticated():
            for worker in authors_form.workers_created:  # log new worker creations
                action.send(request.user, verb=u'добавил сотрудника', action_object=worker, target=work)
            for worker_alias in authors_form.worker_aliases_created:  # log new worker aliases creations
                action.send(request.user, verb=u'добавил псевдоним сотрудника', action_object=worker_alias, target=work)
        timer.cutoff("saved")

        # send "ok"-message to the user and redirect
        if not edit_mode:  # add mode
            work_str = u' <a href="%s" title="Перейти на страницу публикации">%s</a>' % (work.get_absolute_url(), work.title) if work else ""
            messages.success(request, u"%s%s успешно добавлена." % (work_type, work_str))
            # redirect to page with adding additional info to publication
            additional_form = ArticleAdditionalForm(post, instance=work) if publication_type == 'article' else BookAdditionalForm(post, instance=work)
            return publication_detail_additional(request, work.nominative_en, work.id, additional_form=additional_form)
        else:
            messages.success(request, u"%s успешно отредактирована%s." % (u"Статья" if old_work_type == 'article' else u"Книга", u" (и превращена в %s)" % work.accusative if work_type_changed else ""))
            # TODO: redirect to step 4 for editing of additional info
            return redirect(work if work else "home")
    timer.cutoff("validated")

    # additional form is created separately. It is needed only for preserving additional info (abstract etc.) from step 2 to step 4.
    additional_form = ArticleAdditionalForm(post) if publication_type == 'article' else BookAdditionalForm(post)
    # pre calculate some variables to simplify benchmarking
    if forms.get("publisher"):
        forms["publisher"].similar = forms["publisher"].get_similar()
        timer.cutoff("publisher_get_similar")

    # pack context data and show form, this is step 3
    context = {}
    context["forms"] = forms
    context["work_type"] = work_type
    context["work_type_verbose"] = dict(WORK_TYPES)[request.POST["work_type"]]
    context["parsing_event_form"] = parsing_event_form
    context["additional_form"] = additional_form

    if request.is_ajax():
        for form_name in forms:
            forms[form_name]["form"] = forms[form_name]["form"].as_table()
        context["status"] = "ok"
        return HttpResponse(simplejson.dumps(context), content_type='application/javascript')
    timer.cutoff("build_context")

    response = render_to_response(request, "publications/add_work.html", context)
    timer.cutoff("build_response")
    timer.cutoff("total", start_time)
    logging.getLogger("benchmark.step3").info("Timer: %s", timer)
    return response


@login_required
def parse_data(request, from_queue=False):
    """Parse input publication data.
    Render the step 2 page with a populated form for editing.
    This method also saves xml in session.
    from_queue means get the next parsing data from user queue.
    """
    timer = CutoffTimer(None)
    start_time = timer.cutoff("start")

    request.session['xml'] = ""
    context = {}  # can be updated in parsers

    def empty_form():
        context["book_categories"] = BookCategory.objects.all()
        context["collection_categories"] = CollectionCategory.objects.all()
        context["xml_hash"] = hashlib.md5(request.session['xml']).hexdigest()
        return render_to_response(request, 'publications/save_work.html', context)

    if not from_queue:
        if request.method != "POST":
            return empty_form()
        # get and prepare data for parsing
        logger.debug("Got POST request: %s", request.POST)
        data = request.POST.get('citation')  # data entered by a user
        logger.debug("Input data length: %d", len(data))
        if not data:
            return empty_form()
    timer.cutoff("not_from_queue")

    if from_queue:
        parsing_event = ParsingEvent.objects.get_next_from_queue(request.user)
        if not parsing_event:  # the queue is empty
            messages.info(request, u"Очередь добавления пуста. Пожалуйста, введите новые данные.")
            return redirect("publications_add")
        data = parsing_event

    try:
        timer.cutoff("from_queue")
        parsing_event = apply_parsers(data, request, context)  # note: it can modify the data variable, don't use it anymore
        timer.cutoff("apply_parsers")
    except DoiServiceError, e:
        messages.warning(request, u"Ошибка DOI: %s" % e)
        return redirect("publications_add")

    if not getattr(parsing_event, "parsed_data", None):
        # if, for some strange reason, all parsers failed, including dummy, render an empty form
        return empty_form()

    prefix = "parsing_event"
    parsing_event_form = ParsingEventForm({prefix + '-id': parsing_event.id, prefix + '-dissatisfied': False}, prefix=prefix)  # prefix is needed not to confuse fields with other forms

    timer.cutoff("generate_forms")
    # find similar articles and books
    similar_articles = Article.objects.get_similar(parsing_event.parsed_data['title'])
    similar_books = Book.objects.get_similar(parsing_event.parsed_data['title'])
    timer.cutoff("find_similar")

    # save fields data in session
    try:
        request.session['xml'] = pickle.dumps(parsing_event.parsed_data)
    except:
        logger.warning("Exception during xml serialization occurred", exc_info=True)
        request.session['xml'] = ""
    context["xml_hash"] = hashlib.md5(request.session['xml']).hexdigest()

    context.update({
        "fields": parsing_event.parsed_data,
        "similar_books": similar_books,
        "similar_articles": similar_articles,
        "has_similar_works": any((similar_books, similar_articles)),
        "parsing_event_form": parsing_event_form,
        "book_categories": BookCategory.objects.all(),
        "collection_categories": CollectionCategory.objects.all(),
    })
    logger.debug("Passing context: %s", context)
    timer.cutoff("build_context")

    response = render_to_response(request, 'publications/save_work.html', context)
    timer.cutoff("build_response")
    timer.cutoff("total", start_time)
    logging.getLogger("benchmark.step2").info("Timer: %s", timer)
    return response


def list_years(request):
    '''Generates a page with links to lists of publications by year for automatic parsing.'''
    years = uniqify(filter(bool, Article.objects.values_list('year', flat=True)))
    return render_to_response(request, "publications/list_years.html", {'years': years})


def list_by_year(request, year):
    '''Generates a page with all publications by year.'''
    articles = Article.objects.filter(year=year)
    books = Book.objects.filter(year=year)
    return render_to_response(request, "publications/list_by_year.html", {'articles': articles, 'books': books, 'year': year})


def list_by_year_in_style(request, year, style):
    '''Generates a page with all publications by year displayed in style specified.'''
    articles = Article.objects.filter(year=year)
    articles_bibtex = " ".join(article.bibtex for article in articles)
    articles_html = bibtex2html(articles_bibtex, style)
    books = Book.objects.filter(year=year)
    books_bibtex = " ".join(book.bibtex for book in books)
    books_html = bibtex2html(books_bibtex, style)
    return render_to_response(request, "publications/list_by_year.html", {'year': year, 'articles_html': articles_html, 'books_html': books_html, 'style': style})


def list_by_year_bibtex(request, year):
    '''Generates a bibtex page with all publications by year.'''
    articles = Article.objects.filter(year=year)
    books = Book.objects.filter(year=year)
    year = {'articles': {'all': articles}, 'books': {'all': books}}
    return _render_to_response("publications/publication_list.bib", {'object': year}, mimetype='text/plain')


def publication_detail_in_style(request, publication_type, publication_id, style):
    publication = get_publication_by_type(publication_type, publication_id)
    html = publication.in_style(style)
    if not html.strip():
        raise Http404
    return HttpResponse(html, content_type='text/html')


def publication_detail_in_format(request, publication_type, publication_id, format_extension, download=False, publication_type2=None):
    '''View page with publication exported to specified format.
    publication_type is either 'article' or 'book'
    publication_type2 is the same as publication_type and is used to make download file names pretty (e.g. article1234.bib instead of 1234.bib)
    if download is True, return not the page, but the file.
    '''
    publication = get_publication_by_type(publication_type, publication_id)
    if download:
        mimetype = 'application/octet-stream'
    elif format_extension == 'word.xml':
        mimetype = 'application/xhtml+xml'
    else:
        mimetype = 'text/plain'
    if format_extension == 'bib':
        return _render_to_response("publications/publication_detail.bib", {'publication': publication}, context_instance=RequestContext(request), mimetype=mimetype)
    else:
        return HttpResponse(publication.in_format(format_extension), content_type=mimetype)


def object_publications_in_style(request, object_id, style, model, template_name, template_object_name):
    '''Shows an object page (Collection, Journal, Publisher etc.) with publications styled in specified style. Worker is a special case, see worker_detail_in_style function.'''
    object_instance = get_object_or_404(model, pk=object_id)
    try:
        articles_html = object_instance.articles.in_style(style)
    except AttributeError:
        articles_html = None
    try:
        books_html = object_instance.books.in_style(style)
    except AttributeError:
        books_html = None
    return render_to_response(
        request, template_name, {
            template_object_name: object_instance,
            'articles_html': articles_html,
            'books_html': books_html,
            'style': style
        }
    )


@login_required
def request_search(request, publication_type, publication_id):
    publication = get_publication_by_type(publication_type, publication_id)

    worker = request.user.get_profile().worker
    if not worker:
        messages.warning(request, u'Вы не можете выбрать данную операцию.')
        return redirect(publication)

    pubauthors = publication.authors.filter(pk=worker.id)
    if not pubauthors:
        messages.warning(request, u'Вы не можете выбрать данную операцию.')
        return redirect(publication)

    ExternalArticleSearchRequest.objects.create(
        article=publication,
        user=request.user
    )
    messages.success(request, u'Запрос успешно создан.')
    return redirect(publication)


def publication_detail(request, publication_type, publication_id):
    '''Shows a publication page.'''
    publication = get_publication_by_type(publication_type, publication_id)
    content_type = ContentType.objects.get_for_model(publication.__class__)
    attachment_formset = make_attachment_formset(content_type, queryset=Attachment.objects.none())

    template_name = 'publications/%s_detail.html' % publication.nominative_en
    context = {
        'publication': publication,
        'attachment_formset': attachment_formset
    }
    if publication.nominative_en == 'article':
        latest_log = ExternalArticleSearchLog.objects.filter(article=publication).aggregate(Max('date'))
        latest_search_date = latest_log['date__max']

        latest_request = ExternalArticleSearchRequest.objects.filter(article=publication).aggregate(Max('date'))
        latest_search_request_date = latest_request['date__max']

        context['latest_search_date'] = latest_search_date
        if latest_search_request_date and latest_search_date and latest_search_date <= latest_search_request_date:
            context['latest_search_request_date'] = latest_search_request_date
        else:
            context['latest_search_request_date'] = None

        if request.user.is_authenticated():
            worker = request.user.get_profile().worker
            if worker:
                pubauthors = publication.authors.filter(pk=worker.id)
                if pubauthors:
                    context['show_request_button'] = True

            roles = AuthorRole.objects.filter(authorroleobj__code="article")
            context['roles'] = roles
    elif publication.nominative_en == 'book':
        roles = AuthorRole.objects.filter(authorroleobj__code="book")
        context['roles'] = roles
    return render_to_response(request, template_name, context)


def publication_detail_additional(request, publication_type, publication_id, additional_form=None, attachment_formset=None, section='abstract'):
    '''Shows page with forms for adding additional info to publication (abstract, files, etc.)
    if additional_form is specified, use it in initial form rendering. This is used when adding a publication, when additional info (abstract, doi, etc) had been automatically parsed and we want to show it.
    additional_form or attachment_formset can also be passed for re-rendering from save_additional_info or save_attachments methods, if forms contain errors.
    '''
    publication = get_publication_by_type(publication_type, publication_id)
    if not has_permission(request.user, "edit_additional", publication):
        return redirect(publication)  # show main publication page
    if not additional_form:  # if additional form is provided in args, use it, otherwise create
        kwargs = {'user': request.user} if publication_type == 'article' else {}
        additional_form = publication.additional_form_class(instance=publication, **kwargs)
    if not attachment_formset:
        content_type = ContentType.objects.get_for_model(publication.__class__)
        attachment_formset = make_attachment_formset(content_type, queryset=Attachment.objects.none())
    # add extra message for user to add doi to publication
    if not publication.doi:
        messages.info(request, u'Для удобного обращения к первичной информации о публикации мы настоятельно рекомендуем ввести <a href="http://ru.wikipedia.org/wiki/%D0%98%D0%B4%D0%B5%D0%BD%D1%82%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%82%D0%BE%D1%80_%D1%86%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%BE%D0%B3%D0%BE_%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D0%B0" title="Что такое DOI?" target="_blank">DOI</a> (ниже на странице).')
    return render_to_response(request, 'publications/publication_detail_additional.html', {
        'publication': publication, 'additional_form': additional_form,
        'attachment_formset': attachment_formset, 'section': section,
    })


def save_additional_info(request, publication_type, publication_id):
    '''Updates publication's additional info (abstract etc.) using information from request. Also emits information logs.'''
    publication = get_publication_by_type(publication_type, publication_id)
    if not has_permission(request.user, "edit_additional", publication) or not request.method == 'POST':
        return redirect(publication)  # show main publication page
    logger.debug("Got POST request: %s", request.POST)
    old_info = publication.additional_info_dict
    old_abstract = publication.abstract

    def process_additional_form(additional_form):
        if additional_form.is_valid():
            logger.debug("Additional form cleaned data: %s", additional_form.cleaned_data)
            new_abstract = additional_form.cleaned_data['abstract']
            if old_abstract:
                if not new_abstract:  # user is deleting the abstract
                    action.send(request.user, verb=u'удалил аннотацию к %s' % publication.dative, target=publication, description=old_abstract)
                elif new_abstract != old_abstract:  # user is editing the abstract
                    action.send(request.user, verb=u'отредактировал аннотацию к %s' % publication.dative, target=publication, description="%s -> %s" % (old_abstract, new_abstract))
            else:  # user is adding an abstract
                action.send(request.user, verb=u'добавил аннотацию к %s' % publication.dative, target=publication, description=new_abstract)
            action.send(request.user, verb=u'изменил дополнительную информацию о %s' % publication.dative, target=publication, description='%s -> %s' % (old_info, dict((key, value) for key, value in additional_form.cleaned_data.items() if key != 'abstract')))
            additional_form.save(request)

    kwargs = {'user': request.user} if publication_type == 'article' else {}
    additional_form = publication.additional_form_class(request.POST, instance=publication, **kwargs)  # A form bound to the POST data
    process_additional_form(additional_form)
    content_type = ContentType.objects.get_for_model(publication.__class__)
    attachment_formset = make_attachment_formset(content_type, request.POST, request.FILES)
    process_attachments(request, attachment_formset, publication, request.user)
    if additional_form.is_valid() and attachment_formset.is_valid():
        return redirect(publication)
    else:
        return publication_detail_additional(request, publication_type, publication_id, additional_form=additional_form,
                                             attachment_formset=attachment_formset)


def publication_delete(request, publication_type, publication_id):
    '''Deletes the publication.'''
    publication = get_publication_by_type(publication_type, publication_id)
    if not has_permission(request.user, "delete", publication):
        return redirect(publication)
    delete_attachments(request, content_type_id=publication.content_type.id, object_id=publication.id, delete_all=True)
    description = 'bibtex: %s, xml: %s' % (publication.bibtex, publication.xml)
    action.send(request.user, verb=u'удалил %s' % publication.accusative, target=publication, description=description)
    message = u'%s "%s" успешно удалена.' % (publication.nominative.capitalize(), publication.title)
    publication.delete()
    messages.success(request, message)
    return redirect("home")


def clear_adding_queue(request):
    '''Clears the adding queue for the user.'''
    queue_size = ParsingEvent.objects.get_queue_size(request.user)
    ParsingEvent.objects.clear_queue(request.user)
    messages.success(request, u"Очередь добавления успешно очищена (удалено %d публикаций)." % queue_size)
    action.send(request.user, verb=u'очистил очередь добавления', description=u"Удалил %d публикаций." % queue_size)
    return redirect("add_work")


def citation2bibtex(request):
    '''Converts a citation into a bibtex entry using Freecite service.'''
    # get and prepare data for parsing
    if request.method == "POST":
        api = False
        data = request.POST.get('citation')  # data entered by a user
    else:  # GET request, presumably API
        api = True
        data = request.GET.get('citation')  # data entered by a user

    if not data:
        return render_to_response(request, 'publications/citation2bibtex.html')

    from publications.parsers.parse import FreeciteParser, parse_data

    # parse data using freecite parser
    Publication = namedtuple('Publication', 'bib_type, bibtex_id, bibtex_fields')
    if not isinstance(request.user, User):
        try:
            request.user = User.objects.get(username=request.GET.get('user'))
        except User.DoesNotExist:
            return render_to_response(request, 'publications/citation2bibtex.html')
    parser = FreeciteParser(request, {})
    parsing_event = parse_data(parser, data)
    parsed_fields = parsing_event.parsed_data

    # detect bibtex entry type
    if parsed_fields['work_type'] == "journal":
        bibtex_type = "article"
    elif parsed_fields['work_type'] == "collection":
        bibtex_type = "inproceedings"
    elif parsed_fields['work_type'] == "book":
        bibtex_type = "book"
    else:
        bibtex_type = "article"

    # map inner field names -> bibtex field names
    field_names = ["title", "journal", ("collection", "booktitle"), ("editors", "editor"), "volume", "publisher", "institution", "number", "year", ("location", "address"), ("note", "annote")]
    fields = {}
    for name in field_names:
        if isinstance(name, tuple):
            inner_name = name[0]
            bibtex_name = name[1]
        else:  # this is a string, inner and outer name coincide
            inner_name = name
            bibtex_name = name
        try:
            if parsed_fields[inner_name]:
                fields[bibtex_name] = parsed_fields[inner_name]
        except (IndexError, AttributeError):
            pass
    # special fields processing
    fields["author"] = parsed_fields["authors"].replace(", ", " and ")
    fields["pages"] = "%s--%s" % (parsed_fields["firstpage"], parsed_fields["lastpage"])
    original_string = parsed_fields["raw_string"]

    # construct bibtex entry id
    title = fields["title"].split()[0]
    if len(title) < 4:
        title = " ".join(fields["title"].split()[:2])
    first_author = slughifi(fields["author"].split(" and ")[0].split(" ")[0])
    bibtex_id = u"%s%s%s" % (first_author, fields["year"], slughifi(title))

    # render bibtex string
    publication = Publication(bibtex_type, bibtex_id, fields.items())
    if api:
        return _render_to_response('publications/publication_detail.bib', {'publication': publication}, mimetype="text/plain")
    else:
        bibtex = render_to_string("publications/publication_detail.bib", {'publication': publication})
        return render_to_response(request, 'publications/citation2bibtex.html', {'bibtex': bibtex, 'original_string': original_string})


from statistics.sqlutils_getdata import book_material_representation_sql, article_material_representation_sql, get_department_pubs_generic, get_aggregate_info_searchresults, get_department_generate_pubs_query,get_department_generate_nonarticles_pubs_query
from statistics.views import format_pubs_generic_conditions
from django.http import HttpResponseRedirect


def publications_search(request, publication_type):
    articles = []
    theses = []
    books = []
    sql = ""
    books_sql = ""
    theses_sql = ""
    aggregate_results_info = {}

    constrained_search = False  # set to True if extra constraints are used (department_id, rub_id, year, etc)
    if request.method == "POST":
        search_string = request.POST.get("publication_name", "")
        if request.POST.get("q", None):
            constrained_search = True
        if dict(request.POST).get("year", None):
            constrained_search = True
    else:
        search_string = request.GET.get("publication_name", "")
        search_string = request.GET.get("s", search_string)
        if request.GET.get("q", None):
            constrained_search = True
        if dict(request.GET).get("year", None):
            constrained_search = True
    if search_string == "":
        search_string = request.GET.get("s", search_string)
    search_string_too_short = False
    search_string = search_string.strip()

    if request.GET.get("exclude", None):
        newpath = request.build_absolute_uri()[0:request.build_absolute_uri().find('?')]
        newpath = newpath + '?publication_name=' + search_string
        return HttpResponseRedirect(newpath)
        #print request.build_absolute_uri()[0:request.build_absolute_uri().find('?')]
    if request.GET.get("search", None):
        newpath = request.build_absolute_uri()[0:request.build_absolute_uri().find('?')]
        newpath = newpath + '?publication_name=' + search_string
        return HttpResponseRedirect(newpath)


    print publication_type
    query_conditions = format_pubs_generic_conditions(request)
    query_conditions['ROWNUM'] = 50000
    sqlparams_article = []
    sqlparams_books = []
    if len(search_string) >= 3:
        sqlparams_article.append(search_string)
        sqlparams_books.append(search_string)
    if query_conditions.get("department"):
        for i in query_conditions.get("department").split(","):
            sqlparams_article.append(i)


    #print sqlparams_article
    #print sqlparams_books

    if len(search_string) >= 3 or constrained_search:
        query_conditions['use_material_represent'] = 1
        query_conditions['title_filter'] = search_string
        matched_articles = get_department_pubs_generic(addon_fields={"ALL": 1},
                                                           conditions=query_conditions, sqlparam_list=sqlparams_article)
        articles_sql = article_material_representation_sql(conditions = query_conditions)
        articles_ids = [a.pk for a in matched_articles]
        articles = Article.objects.filter(id__in=articles_ids)

    if len(search_string) >= 3:
        query_conditions["books"] = "1"
        matched_books = get_department_pubs_generic(addon_fields={"ALL": 1},
                                                               conditions=query_conditions, sqlparam_list=sqlparams_books)
        books_sql = book_material_representation_sql(conditions = query_conditions)

                    #get_department_generate_nonarticles_pubs_query(addon_fields={"ALL": 1},
                    #                                               conditions=query_conditions);
                    #print books_sql
        books_ids = [a.pk for a in matched_books]
        books = Book.objects.filter(id__in=books_ids)
        del query_conditions["books"]

        query_conditions['FREE'] = query_conditions['FREE'] + u" and category='Тезисы'"
        query_conditions['FREE_NEW'] = query_conditions['FREE_NEW'] + u" and category='Тезисы'"
                    #theses_sql = get_department_generate_pubs_query(addon_fields={"ALL": 1},
                    #                                               conditions=query_conditions)
        theses_sql = article_material_representation_sql(conditions = query_conditions)
        matched_theses = get_department_pubs_generic(addon_fields={"ALL": 1},
                                                                   conditions=query_conditions, sqlparam_list=sqlparams_article)
        theses_ids = [a.pk for a in matched_theses]
        theses = Article.objects.filter(id__in=theses_ids)

    if not articles and publication_type == "articles":
        publication_type = "theses"
    if not theses and publication_type == "theses":
        publication_type = "books"

    if len(search_string) < 3 and not constrained_search:
       search_string_too_short = True

    if search_string_too_short == False:
        if publication_type == "articles":
            aggregate_results_info = get_aggregate_info_searchresults(innerSql = articles_sql, type=publication_type, sqlparam_list=sqlparams_article)
        elif publication_type == "theses":
            aggregate_results_info = get_aggregate_info_searchresults(innerSql = theses_sql, type=publication_type, sqlparam_list=sqlparams_article)
        elif publication_type == "books":
            aggregate_results_info = get_aggregate_info_searchresults(innerSql = books_sql, type=publication_type, sqlparam_list=sqlparams_books)
    years = aggregate_results_info.get('years')
    departments = aggregate_results_info.get('departments')


    checked_years = {}
    checked_deps = {}
    for _year in dict(request.GET).get("year", {}):
        checked_years[int(_year)] = True
    for _depid in dict(request.GET).get("selectedDepartment", {}):
        checked_deps[int(_depid)] = True


    params_dict = {}
    if request.method == "GET":
        params_dict = request.GET
    if request.method == "POST":
        params_dict = request.POST


    if request.GET.get("q", None):
        # we should remove all filters in case if this page was generated as redirect from dynamic page (with diagram).
        #This is temporary and should be fixed in the future
        del years[:]
        del departments[:]
        checked_years.clear()
        checked_deps.clear()


    # print params_dict
    #form = YearsListForm(params_dict, {'years' : years})

    #print form

    return render_to_response(request,
                              "publications/publication_search_results.html",
                              {'checked_years' : checked_years, 'checked_deps' : checked_deps, 'years' : years, 'departments' : departments, 'articles': articles, 'theses':theses, "books":books, 'search_string':search_string,
                               'publication_type':publication_type, 'search_string_too_short':search_string_too_short,
                               })




