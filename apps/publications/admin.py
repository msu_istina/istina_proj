from django.contrib import admin

from common.utils.admin import register_with_versions
from models import *


register_with_versions(Article)
register_with_versions(ArticleAuthorship)
register_with_versions(ArticleKeyword)
register_with_versions(Book)
register_with_versions(BookAuthorship)
register_with_versions(BookCategory)
register_with_versions(BookCategoryMembership)
register_with_versions(Language)


class ParsingEventAdmin(admin.ModelAdmin):
    list_display = ['user', '__unicode__', 'type', 'method', 'created']
admin.site.register(ParsingEvent, ParsingEventAdmin)
