# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Q
from django.http import Http404
from django.template.loader import render_to_string
import logging

from common.models import MyModel, AuthoredModel, AuthorshipPositionedModel, Activity, AuthorScience
from common.utils.slughifi import slughifi
from common.utils.number import quantize_int
from publications.managers import *
from publications.utils import split_authors, bibtex2any, bibtex2html, guess_language
from workers.utils import get_fullname_raw
from tags.utils import get_all_list_tag_values


logger = logging.getLogger("publications.models")


class Publication(AuthoredModel, Activity):
    '''A common abstract model for articles and books.'''
    is_publication = True

    class Meta:
        abstract = True

    @models.permalink
    def get_absolute_url(self):
        return ('publications_publication_detail', (), {'publication_type': self.nominative_en, 'publication_id': self.id})

    @models.permalink
    def get_edit_url(self):
        return ('publications_edit', (), {'publication_type': self.nominative_en, 'publication_id': self.id})

    @property
    def get_collection(self):
        return getattr(self, "collection", None)

    @property
    def get_year(self):
        return self.year or getattr(self.get_collection, "year", None)

    def get_bibtex(self, publication=None):
        '''
        Returns bibtex string of a publication.
        publication argument has been added, so that the function can be called
        for fake Publication objects created in Python, see WorkTuple class,
        not duplicating the function code.
        '''
        if publication is None:
            publication = self
        return render_to_string("publications/publication_detail.bib", {'publication': publication})

    bibtex = property(get_bibtex)

    def in_style(self, style='gost'):
        '''Returns a styled citation of a publication.'''
        return bibtex2html(self.bibtex, style)

    @property
    def citation(self):
        '''Returns bibliographic citation of an article in default GOST style.'''
        return self.in_style(style='gost')

    def in_format(self, format_extension='enw'):
        '''Takes an file extension specifying a format and returns a citation of a publication in this format (ads, bib, end, isi, ris, wordbib).'''
        return bibtex2any(self.bibtex, format_extension)

    @property
    def journal_name(self):
        return getattr(getattr(self, "journal", None), "name", None)

    @property
    def collection_title(self):
        return getattr(self.get_collection, "title", None)

    @property
    def get_volume(self):
        return getattr(self, "volume", None) or getattr(self.get_collection, "volume", None)

    @property
    def get_number(self):
        return getattr(self, "number", None)

    @property
    def get_doi(self):
        'Removes all junk around doi-like text in user provided doi string.'
        import re
        if self.doi:
            return re.sub(".*?(10[^ ]*).*", "\\1", self.doi)
        return None

    @property
    def get_isbn(self):
        return getattr(self, "isbn", None)

    @property
    def get_ncopies(self):
        return getattr(self,"ncopies", None)

    @property
    def bib_issn(self):
        issn_str = u""
        if getattr(self, "journal", None):
            issn_str = u"; ".join(self.journal.issns.all().values_list('value', flat=True))
        return issn_str

    @property
    def get_pages_max_number(self):
        return self.pages if isinstance(self, Book) else self.lastpage

    @property
    def series_title(self):
        return getattr(getattr(self.get_collection, "series", None), "title", None)

    @property
    def publisher_or_location(self):
        return getattr(self.publisher, "city", None) or self.location

    @property
    def publisher_name(self):
        return getattr(self.publisher, "name", None)

    @property
    def publisher_city(self):
        return getattr(self.publisher, "city", None)

    @property
    def theses(self):
        return getattr(self.get_collection, "theses", None)

    def get_language_func(self, title=None):
        '''
        Returns a string indicating publication language. Currently only russian and english are supported. Used in bibtex.
        title argument has been added, so that the function can be called
        for fake Publication objects created in Python, see WorkTuple class,
        not duplicating the function code.
        '''
        if title is None:
            title = self.title
        return guess_language(title)

    get_language = property(get_language_func)

    @property
    def bibtex_fields(self):
        '''Returns a list of tuples (key, value) of bibtex fields for the publication.'''
        fields = [
            ('author', self.authors_string_bibtex),
            ('title', self.title),
            ('journal', self.journal_name),
            ('booktitle', self.collection_title),
            ('year', self.get_year),
            ('volume', self.get_volume),
            ('number', self.get_number),
            ('isbn', self.get_isbn),
            ('issn', self.bib_issn),
            ('doi', self.doi),
            ('editor', getattr(self.get_collection, "bib_editors", None)),
            ('series', self.series_title),
            ('pages', self.bib_pages),
            ('publisher', self.publisher_or_location),
            ('address', self.publisher_city),
            ('annote', self.abstract),
            # ('url', self.get_url),
            ('language', self.get_language)
        ]
        return filter(lambda x: bool(x[1]), fields)

    def get_bibtex_id(self, title=None, year=None, first_author_lastname=None):
        '''
        Returns bibtex id of the publication.
        title, year and first_author_lastname arguments have been added, so that the function can be called
        for fake Publication objects created in Python, see WorkTuple class,
        not duplicating the function code.
        '''
        if title is None:
            title = self.title
        if year is None:
            year = self.get_year
        if first_author_lastname is None:
            # note that slughifi("") = ""
            first_author_lastname = self.authors.order_by("lastname")[0].lastname if self.authors.exists() else ""
        short_title = title.split()[0]
        if len(short_title) < 4:
            short_title = " ".join(title.split()[:2])
        return u"%s%s%s%s" % (slughifi(first_author_lastname), year, slughifi(short_title), self.id)

    bibtex_id = property(get_bibtex_id)

    @property
    def users(self):
        '''Returns a list of users-authors of the publication.'''
        users = []
        for worker in self.authors.all():
            try:
                users.append(worker.profile.user)
            except (ObjectDoesNotExist, AttributeError):
                pass
        return set(filter(bool, users))

    def get_authors_string_bibtex(self, authors_string=None):
        '''
        Return a string of authors formatted for bibtex entry.
        First, get original authors string and split it into triples (lastname, firstname, middlename).
        Then transform each triple in a fullname string formatted for bibtex.

        authors_string argument has been added, so that the function can be called
        for fake Publication objects created in Python, see WorkTuple class,
        not duplicating the function code.
        '''
        if not authors_string:
            authors_string = self.authors_string
        return " and ".join(
            get_fullname_raw(lastname=lastname,
                             firstname=firstname,
                             middlename=middlename,
                             comma=True)
            for (lastname, firstname, middlename) in split_authors(authors_string)
        )

    authors_string_bibtex = property(get_authors_string_bibtex)

    @property
    def authors_list_meta(self):
        '''
        Return a list of authors names formatted for publication meta tags.
        First, get original authors string and split it into triples (lastname, firstname, middlename).
        Then transform each triple in a fullname string formatted for meta tags.
        '''
        return [get_fullname_raw(lastname=lastname, firstname=firstname, middlename=middlename, initials=True, comma=True) for (lastname, firstname, middlename) in split_authors(self.authors_string)]

    @property
    def export_formats(self):
        '''
        Returns a dictionary of formats in which the publication can be exported. The output is a dictionary:
        'format_extension': {'short_name': 'format_short_name', 'full_name': 'format_full_name'}).
        '''
        formats = [
            ('bib', 'BibTeX'),
            ('enw', 'EndNote'),
            ('ris', 'RIS'),
            ('word_xml', 'Word', 'Microsoft Word 2007'),
            ('isi', 'ISI', 'ISI Web of Knowledge'),
            ('ads', 'ADS')
        ]
        # return dictionary
        return [{'extension': item[0], 'short_name': item[1], 'full_name': item[1] if len(item) == 2 else item[2]} for item in formats]

    @property
    def bibtex_styles(self):
        return get_bibtex_styles()

    @property
    def short_info_html(self):
        '''Renders and returns HTML with short info about the publication.'''
        template = "publications/%s_short.html" % self.nominative_en
        return render_to_string(template, {self.nominative_en: self})

    @property
    def additional_info_dict(self):
        '''Return a dictionary containing additional info about the publication: doi etc.'''
        return {'doi': self.doi}

    @property
    def get_url(self):
        return self.get_full_url()

    def get_url_scopus(self, scopus_id=None):
        """
        scopus_id argument has been added, so that the function can be called
        for fake Publication objects created in Python, see WorkTuple class,
        not duplicating the function code.
        """
        if scopus_id is None:
            # try to get scopus id from database
            scopus_ids = self.external_articles.filter(category='Scopus').values_list('external_id', flat=True)
            if scopus_ids:
                scopus_id = scopus_ids[0]
        if scopus_id:
            return "http://www.scopus.com/record/display.url?origin=recordpage&eid=2-s2.0-%s" % scopus_id


def get_bibtex_styles():
    '''Returns a list of bibtex styles.'''
    return ['gost', 'plain', 'abbrv', 'acm', 'alpha', 'amsalpha', 'amsplain', 'apalike', 'ieeetr', 'siam']


def get_publication_by_type(publication_type, publication_id, raise_404=True):
    '''Returns the publication with specified type and id.'''
    success = True
    if publication_type == "article":
        model = Article
    elif publication_type == "book":
        model = Book
    else:
        success = False

    try:
        return model.objects.get(id=publication_id)
    except:
        success = False

    if not success:
        if raise_404:
            raise Http404
        else:
            return None


class Article(Publication):
    id = models.AutoField(primary_key=True, db_column="F_ARTICLE_ID")
    title = models.CharField(u"Название статьи", max_length=1000, db_column="F_ARTICLE_NAME")
    authors = models.ManyToManyField("workers.Worker", related_name="articles", through="ArticleAuthorship")
    journal = models.ForeignKey(to="journals.Journal", related_name="articles", db_column="F_JOURNAL_ID", null=True, blank=True)
    volume = models.IntegerField(u"Том журнала", db_column="F_ARTICLE_VOL", null=True, blank=True)
    number = models.CharField(u"Номер журнала", max_length=255, db_column="F_ARTICLE_NUM", null=True, blank=True)
    year = models.IntegerField(u"Год издания журнала", db_column="F_ARTICLE_YEAR", null=True, blank=True)
    collection = models.ForeignKey(to="icollections.Collection", related_name="articles", db_column="F_COLLECTION_ID", null=True, blank=True)
    firstpage = models.CharField(u"Первая страница статьи", max_length=32, db_column="F_ARTICLE_FIRSTPAGE", blank=True)
    lastpage = models.CharField(u"Последняя страница статьи", max_length=32, db_column="F_ARTICLE_LASTPAGE", blank=True)
    abstract = models.TextField(u"Аннотация", db_column="F_ARTICLE_ABSTRACT", null=True, blank=True)
    doi = models.CharField(u"DOI", max_length=255, db_column="F_ARTICLE_DOI", null=True, blank=True)
    language = models.ForeignKey(to="Language", related_name="articles", db_column="LANG_ID", null=True, blank=True)
    xml = models.TextField(u"XML журнала", db_column="F_ARTICLE_XML", null=True, blank=True)
    creator = models.ForeignKey(to=User, related_name="created_articles", db_column="F_ARTICLE_USER", null=True, blank=True)

    objects = ArticleManager()
    objects_theses = ThesisManager()
    objects_pure = PureArticleManager()

    nominative_en = "article"

    class Meta:
        db_table = "ARTICLE"
        verbose_name = u"статья"
        verbose_name_plural = u"статьи"
        # cannot include m2m field into an unique constraint
        #unique_together = (("authors", "title", "journal", "collection", "number", "volume", "year", ))
        ordering = ('-year', 'title')

    def __unicode__(self):
        s = u"'%s'" % self.title
        target = self.journal or self.collection
        if target:
            s += " in %s" % unicode(target)
        return s

    @property
    def pure(self):
        return not self.theses

    @property
    def nominative(self):
        return u"статья" if self.pure else u"тезисы доклада"

    @property
    def nominative_short(self):
        return u"статья" if self.pure else u"тезисы"

    @property
    def nominative_plural(self):
        u'''Used with count=21, e.g. 21 публикация, членство в редколлегиях журналов.'''
        return u"статья" if self.pure else u"тезисы докладов"

    @property
    def nominative_detail(self):
        if self.injournal:
            text = u"статья в научном журнале"
        elif self.incollection:
            if self.pure:
                text = u"статья в сборнике научных трудов"
            else:
                text = u"тезисы доклада научной конференции"
        return text

    @property
    def genitive(self):
        return u"статьи" if self.pure else u"тезисов доклада"

    @property
    def genitive_short(self):
        return u"статьи" if self.pure else u"тезисов"

    @property
    def genitive_plural(self):
        u'''Used with count=2.
        The is essentially a single genitive, e.g. "(2) публикации, патента", but
        if the name is composite ("членство в редколлегии"), then genitive_plural combines
        single genitive of the main word ((2)"членства") and plural of the remainder ("в редколлегиях").'''
        return u"статьи" if self.pure else u"тезисов докладов"

    @property
    def genitive_plural_full(self):
        u'''Used with count=5.
        The is a pure genitive case of the plural form, e.g. "(5) публикаций, патентов".'''
        return u"статей" if self.pure else u"тезисов докладов"

    @property
    def dative(self):
        return u"статье" if self.pure else u"тезисам доклада"

    @property
    def accusative(self):
        return u"статью" if self.pure else u"тезисы доклада"

    @property
    def accusative_short(self):
        return u"статью" if self.pure else u"тезисы"

    @property
    def instrumental(self):
        return u"статьей" if self.pure else u"тезисами доклада"

    @property
    def locative(self):
        return u"статье" if self.pure else u"тезисах доклада"

    @property
    def gender(self):
        return 'feminine' if self.pure else 'plural'

    @property
    def type(self):
        '''Type of the article. Returns 'injournal', 'incollection' or 'unknown'.'''
        if self.journal:
            return 'injournal'
        if self.collection:
            return 'incollection'
        return 'unknown'

    @property
    def bib_type(self):
        '''Bibtex type of the article.'''
        if self.injournal:
            return 'article'
        if self.incollection:
            return 'inproceedings'

    @property
    def injournal(self):
        '''Returns true if the article is in journal.'''
        return bool(self.journal)

    @property
    def incollection(self):
        '''Returns true if the article is in collection.'''
        return bool(not self.journal and self.collection)

    @property
    def publisher(self):
        return getattr(self.collection, "publisher", None) or getattr(self.journal, "publisher", None)

    @property
    def location(self):
        return getattr(self.collection, "location", None)

    @property
    def published_in(self):
        return getattr(self.journal, "name", None) or getattr(self.collection, "title", None)

    def save(self, *args, **kwargs):
        # store the year because it is used in ordering
        if not self.year and self.collection and self.collection.year:
            self.year = self.collection.year
        super(Article, self).save(*args, **kwargs)

    def get_source(self):
        '''
        Returns a single citation string with information about journal/proc, volume, pages, etc.
        No title and authors.
        '''
        cit = u""
        published_in = self.journal or self.collection
        language = self.get_language_func(self.title or u' ' + published_in or u' ')
        if published_in:
            try:
                cit += published_in.name + ". "  # in journal
            except AttributeError:
                cit += published_in.title + ". "  # in collection
        if language == 'russian':
            if self.volume:
                cit += u"том " + unicode(self.volume) + ", "
            if self.number:
                cit += u"н. " + unicode(self.number) + ", "
            if self.firstpage or self.lastpage:
                cit += u"с. " + unicode(self.get_pages()) + ", "
        else:
            if self.volume:
                cit += "vol. " + unicode(self.volume) + ", "
            if self.number:
                cit += "n. " + unicode(self.number) + ", "
            if self.firstpage or self.lastpage:
                cit += "pp. " + unicode(self.get_pages()) + ", "
        if self.year:
            cit += unicode(self.year) + ". "
        return cit

    source = property(get_source)

    def get_citation(self, short=False):
        '''
        Returns a single citation string with information about the article.
            short
                If True, show only first 10 authors.
        '''
        cit = u""
        cit += self.authors_string if not short else self.authors_string_short
        if cit:
            cit += ". "
        cit += self.title + ". "

        cit += self.get_source()
        return cit

    def get_pages(self, double_dash=False, firstpage=None, lastpage=None):
        """
        firstpage and lastpage arguments have been added, so that the function can be called
        for fake Publication objects created in Python, see WorkTuple class,
        not duplicating the function code.
        """
        if firstpage is None:
            firstpage = self.firstpage
        if lastpage is None:
            lastpage = self.lastpage
        dash = "-" if not double_dash else "--"
        return ("%s" % firstpage if firstpage else '') + ("%s%s" % (dash, lastpage) if lastpage else '')

    pages = property(get_pages)

    def get_number_of_pages(self):
        """
        Returns number of pages of the article.
        """
        try:
            fp = int(self.firstpage)
            lp = int(self.lastpage)
            return lp - fp + 1
        except ValueError:
            pass
        return 1

    @property
    def bib_pages(self):
        return self.get_pages(double_dash=True)

    @property
    def short_info(self):
        '''Returns short info about the article, including journal/collection, volume/number, pages and year.'''
        return ", ".join(map(unicode, filter(bool, [self.published_in, self.get_volume, self.number, self.pages, self.get_year])))

    @property
    def additional_form_class(self):
        '''Returns form class for additional info.'''
        import publications
        return publications.forms.ArticleAdditionalForm

    @property
    def has_full_text(self):
        '''Returns True if some file is attached to this article.'''
        from django.db import connection
        cursor = connection.cursor()
        try:
            cursor.execute("""
            select count(*) from files
            where (f_files_type is null or f_files_type = 'full_text')
            and f_object_id = %s
            and f_contenttype_id in (
                 select id from django_content_type where model = 'article'
               )
            """, [self.id])
            n_files = int(cursor.fetchone()[0])
            if n_files > 0:
                return True
        except Exception:
            pass
        finally:
            cursor.close()
        return False

    @property
    def is_top(self):
        '''Returns True if the article may be rewarded, i.e. it satisfies formal criteria (top journal).'''
        try:
            return self.top_article
        except ObjectDoesNotExist:
            return False

    @property
    def top_publications_ids(self):
        return self._default_manager.top_ids()

    @property
    def is_confirmed(self):
        '''Returns True if the article is confirmed, i.e. it's metadata was manually checked for correctness.'''
        return self.confirmed_articles.exists()

    @property
    def is_rewarded(self):
        '''Returns True if the article has been rewarded, i.e. it was included in official prikaz and some of its authors got the money for it.'''
        return self.rewarded_articles.exists()

    @property
    def in_systems(self):
        '''Returns True if the article citation information has been found in external systems.'''
        return self.external_articles.exists()

    @property
    def external_urls_with_citation(self):
        '''Returns a list of dictionaries {external_id: , url of the article in that system}.
           Take only systems that provide citation index.
        '''
        urls = {}
        for dct in self.external_articles.filter(category__has_citation_index=True).values(
                "external_id", "category__full_name", "url").distinct():
            urls[dct['category__full_name']] = {'external_id': dct['external_id'], 'url': dct['url']}
        return urls

    @property
    def icons_detail(self):
        '''Returns a dictionary of icons for article to show on detail page: if it is top, in external systems etc.'''
        return {
            'in_systems': self.in_systems,
            'top': self.is_top,
            'confirmed': self.is_confirmed,
            'full_text': self.has_full_text,
        }

    @property
    def is_locked(self):
        '''We lock from editing only articles that have been manually confirmed or were rewarded (in case the confirmation was deleted after rewarding).'''
        return self.is_confirmed or self.is_rewarded

    @property
    def reward_amount(self):
        '''Amount of money payed to each rewarded author for the article.'''
        return quantize_int(self.rewarded_articles.all()[0].rating * 50000)

    @property
    def reward_amount_total(self):
        '''Total amount of money payed to rewarded authors for the article.'''
        return self.reward_amount * sum(article.rewarded_authorships.count() for article in self.rewarded_articles.all())

    @property
    def rewarded_authors(self):
        '''Returns list of rewarded authors (workers) for the article.'''
        # this line could be enough, but we need to sort workers according to original position in the article authors list
        authors_unsorted = [authorship.user.worker for article in self.rewarded_articles.all() for authorship in article.rewarded_authorships.all()]
        return [author['worker'] for author in self.authors_list if author['worker'] in authors_unsorted]

    @property
    def is_vak(self):
        '''Return True if the article is in VAK journal.'''
        return self.journal and self.journal.is_vak

    @property
    def is_wos_or_scopus(self):
        '''Return True if the article is in WOS/Scopus journal.'''
        return self.journal and self.journal.is_wos_or_scopus

    @property
    def publication_type_tags(self):
        from tags.models import TagCategory

        article_type = TagCategory.objects.article_type()
        return get_all_list_tag_values(self, tag_type=article_type)

    @property
    def confirmation_tags(self):
        from tags.models import TagCategory
        return get_all_list_tag_values(self, tag_type=TagCategory.objects.confirmation)

    @property
    def impact_factor(self):
        return getattr(self.journal, 'impact_factor', None)


class ArticleAuthorship(AuthorshipPositionedModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORA_ID")
    author = models.ForeignKey(to="workers.Worker", related_name="article_authorships", db_column="F_MAN_ID", null=True, blank=True)
    article = models.ForeignKey(to="Article", related_name="authorships", db_column="F_ARTICLE_ID")
    original_name = models.CharField(max_length=255, db_column="F_AUTHORA_NAME", blank=True)
    position = models.IntegerField(db_column="F_AUTHORA_ORD", default=0, null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="article_authorships_added", db_column="F_AUTHORA_USER", null=True, blank=True)

    objects = ArticleAuthorshipManager()

    class Meta(AuthorshipPositionedModel.Meta):
        db_table = "AUTHORA"
        verbose_name = u"Авторство статей"
        verbose_name_plural = u"Авторства статей"

    def __unicode__(self):
        return u"%s - авторство статьи '%s'" % (self.workers_string_full, self.article.title)

    @property
    def is_rewarded(self):
        '''Returns True if the article authorship has been rewarded, i.e. it was included in official prikaz and the author got the money for it.'''
        for user in self.author.rewarded_users.all():  # there is a rewarded user linked to the author
            if user.rewarded_authorships.filter(article__article=self.article).exists():  # there is a rewarded authorship of this article for this user
                return True
        return False


class ArticleKeyword(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_KEYWORDA_ID")
    article = models.ForeignKey(to="Article", related_name="keywords", db_column="F_ARTICLE_ID")
    keyword = models.CharField(u"Ключевое слово", max_length=2000, db_column="F_KEYWORDA_WORD", null=True, blank=True)

    class Meta:
        db_table = "KEYWORDA"
        verbose_name = u"Ключевое слово статьи"
        verbose_name_plural = u"Ключевые слова статей"


class ArticleReference(models.Model):
    # Mark article as primary key to make Django happy
    article = models.ForeignKey(to="Article", related_name="formatted_references", db_column="F_ARTICLE_ID", primary_key=True, on_delete=models.DO_NOTHING)
    reference = models.CharField(u"Ключевое слово", max_length=2000, db_column="FORMATTED_REFERENCE")

    def save(self, **kwargs):
        raise NotImplementedError() # Disable insertion/deletion

    def delete(self, **kwargs):
        raise NotImplementedError() # Disable insertion/deletion

    class Meta:
        db_table = "V_ARTICLE_REFERENCE"
        managed = False



class Book(Publication):
    id = models.AutoField(primary_key=True, db_column="F_BOOK_ID")
    title = models.CharField(u"Название книги", max_length=255, db_column="F_BOOK_NAME")
    authors = models.ManyToManyField("workers.Worker", related_name="books", through="BookAuthorship")
    year = models.IntegerField(u"Год издания", db_column="F_BOOK_YEAR")
    publisher = models.ForeignKey(to="publishers.Publisher", related_name="books", db_column="F_PUBLISHING_ID", null=True, blank=True)
    location = models.CharField(u"Место издания", max_length=255, db_column="F_BOOK_PLACE", null=True, blank=True)
    pages = models.IntegerField(u"Количество страниц", db_column="F_BOOK_PAGES", null=True, blank=True)
    redpages = models.FloatField(u"Объём (п.л.)", db_column="F_BOOK_REDPAGE", null=True, blank=True)
    ncopies = models.IntegerField(u"Тираж", db_column="F_BOOK_NCOPIES", null=True, blank=True)
    isbn = models.CharField(u"ISBN", max_length=255, db_column="F_BOOK_ISBN", null=True, blank=True)
    abstract = models.TextField(u"Аннотация", db_column="F_BOOK_ABSTRACT", null=True, blank=True)
    doi = models.CharField(u"DOI", max_length=255, db_column="F_BOOK_DOI", null=True, blank=True)
    categories = models.ManyToManyField("publications.BookCategory", related_name="books", through="BookCategoryMembership")
    language = models.ForeignKey(to="Language", related_name="books", db_column="LANG_ID", null=True, blank=True)
    xml = models.TextField(u"XML журнала", db_column="F_BOOK_XML", null=True, blank=True)
    creator = models.ForeignKey(to=User, related_name="created_books", db_column="F_BOOK_USER", null=True, blank=True)

    objects = BookManager()

    nominative_en = "book"
    nominative_detail = "книга"
    genitive = u"книги"
    genitive_plural_full = u"книг"
    dative = u"книге"
    accusative = u"книгу"
    accusative_short = u"книгу"
    instrumental = u"книгой"
    gender = 'feminine'

    class Meta:
        db_table = "BOOK"
        verbose_name = u"книга"
        verbose_name_plural = u"книги"
        # cannot include m2m field into an unique constraint
        #unique_together = (("authors", "title", "year", "pages", ))
        ordering = ('-year', 'title')

    def __unicode__(self):
        return "'%s' (%s, %s)" % (self.title, self.location, self.year)

    def get_citation(self, short=False):
        '''
        Returns a single citation string with information about the book.
            short
                If True, show only first 10 authors.
        '''
        cit = u""
        cit += self.authors_string if not short else self.authors_string_short
        if cit:
            cit += ". "
        cit += unicode(self.title) + ". " + getattr(self.publisher, "name", "") + ", " + unicode(self.location) + ", " + unicode(self.year) + ". " + unicode(self.pages) + " p."
        return cit

    @property
    def bib_type(self):
        '''Bibtex type of the book.'''
        return 'book'

    @property
    def source(self):
        '''Returns short info about the book, including publisher/location and year.'''
        published_in = self.publisher.short_info() if self.publisher else self.location
        return u", ".join(map(unicode, filter(bool, [published_in, self.year])))

    @property
    def short_info(self):
        '''Returns short info about the book, including publisher/location, pages and year.'''
        published_in = self.publisher.short_info() if self.publisher else self.location
        pages = (u"%d с." % self.pages) if self.pages else u""
        return u", ".join(map(unicode, filter(bool, [published_in, pages, self.year])))

    @property
    def bib_pages(self):
        return self.pages

    @property
    def type(self):
        return "book"

    @property
    def additional_form_class(self):
        '''Returns form class for additional info.'''
        import publications
        return publications.forms.BookAdditionalForm

    def update_categories(self, categories, user=None):
        '''Adds book in specified categories. Categories is a list of tuples (category, include).
           if include is True, add category, otherwise remove.
           @user is the user adding/removing categories.'''
        for category, include in categories:
            if include:
                membership, created = BookCategoryMembership.objects.get_or_create(book=self, category=category)
                if user and created:
                    membership.creator = user
                    membership.save()
            else:
                BookCategoryMembership.objects.filter(book=self, category=category).delete()


class BookAuthorship(AuthorshipPositionedModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORB_ID")
    author = models.ForeignKey(to="workers.Worker", related_name="book_authorships", db_column="F_MAN_ID", null=True, blank=True)
    book = models.ForeignKey(to="Book", related_name="authorships", db_column="F_BOOK_ID")
    original_name = models.CharField(max_length=255, db_column="F_AUTHORB_NAME", blank=True)
    position = models.IntegerField(db_column="F_AUTHORB_ORD", default=0, null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="book_authorships_added", db_column="F_AUTHORB_USER", null=True, blank=True)

    class Meta(AuthorshipPositionedModel.Meta):
        db_table = "AUTHORB"
        verbose_name = u"Авторство книг"
        verbose_name_plural = u"Авторства книг"

    def __unicode__(self):
        return u"%s - авторство книги '%s'" % (self.workers_string_full, self.book.title)


class BookCategory(MyModel):
    '''A category of books having common properties.'''
    id = models.AutoField(primary_key=True, db_column="F_BOOKGROUPTYPE_ID")
    name = models.CharField(max_length=255, db_column="F_BOOKGROUPTYPE_NAME")

    objects = BookCategoryManager()

    class Meta:
        db_table = "BOOKGROUPTYPE"
        verbose_name = u"Категория книг"
        verbose_name_plural = u"Категории книг"

    def __unicode__(self):
        return self.name


class BookCategoryMembership(MyModel):
    '''A membership of a book in a category.'''
    id = models.AutoField(primary_key=True, db_column="F_BOOKGROUP_ID")
    book = models.ForeignKey(to=Book, related_name="category_memberships", db_column="F_BOOK_ID")
    category = models.ForeignKey(to=BookCategory, related_name="memberships", db_column="F_BOOKGROUPTYPE_ID")
    creator = models.ForeignKey(to=User, related_name="category_memberships_added", db_column="F_BOOKGROUP_USER", null=True, blank=True)

    class Meta:
        db_table = "BOOKGROUP"
        verbose_name = u"Членство в категории книг"
        verbose_name_plural = u"Членства в категориях книг"

    def __unicode__(self):
        return u"книга '%s' в категории '%s'" % (self.book.title, self.category.name)


class Language(MyModel):
    id = models.AutoField(primary_key=True, db_column="LANG_ID")

    class Meta:
        db_table = "T_LANGS"
        verbose_name = u"Язык"
        verbose_name_plural = u"Языки"


class ParsingEvent(MyModel):
    '''A single logged event of automatic parsing.'''
    id = models.AutoField(primary_key=True, db_column="F_LOGPARS_ID")
    user = models.ForeignKey(to=User, related_name="parsing_events", db_column="F_LOGPARS_USER", null=True, blank=True)
    source_data = models.TextField(u"Исходные данные", db_column="F_LOGPARS_SOURCE", null=True, blank=True)
    parsed_data = models.TextField(u"Данные, полученные из парсера", db_column="F_LOGPARS_AUTODATA", null=True, blank=True)
    user_data = models.TextField(u"Данные, сохраненные пользователем", db_column="F_LOGPARS_USERDATA", null=True, blank=True)
    type = models.CharField(u"Тип события", choices=(("preprocessed", u"Прошло предобработку"), ("in queue", u"В очереди"), ("parsed", u"Распарсено успешно"), ("dissatisfied", u"Распарсено неудовлетворительно"), ("deleted from queue", u"Удалено из очереди")), max_length=255, db_column="F_LOGPARS_TYPE", null=True, blank=True)
    method = models.CharField(u"Метод парсинга", choices=(("bibtex_mods", "Bibtex via mods"), ("pybtex", "pybtex for bibtex"), ("freecite", "Freecite service"), ("elibrary", "Elibrary.ru parser"), ("mods", "Mods"), ("simplexml", "SimpleXML"), ("webofscience", "ISI Web of Science"), ("pubmed", "PubMed"), ("dummy", "Dummy default parser")), max_length=255, db_column="F_LOGPARS_METHOD", blank=True, null=True)
    created = models.DateTimeField(u"Дата создания", db_column="F_LOGPARS_CREATE", auto_now_add=True, blank=True, null=True)

    objects = ParsingEventManager()

    class Meta:
        db_table = "LOGPARS"
        verbose_name = u"Событие автоматического разбора"
        verbose_name_plural = u"Журнал автоматического разбора"
        get_latest_by = "created"
        ordering = ('-created',)

    def __unicode__(self):
        if self.type == 'preprocessed':  # multiple entries stored
            text = self.source_data
        else:
            from publications.parsers.parse import create_parser
            parser = create_parser(self.method)
            text = parser.unpickle_entry_to_text(self.source_data)
        return u"%s (от пользователя %s)" % (text, self.user)

from publications.admin import *
