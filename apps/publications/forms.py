# -*- coding: utf-8; -*-
from django import forms
import logging
from django.contrib import messages
from actstream import action

from common.forms import YearField, MyModelForm
from common.widgets import MyFilteredSelectMultiple
from icollections.models import Collection, Series, CollectionCategory
from journals.models import Journal, JournalAlias
from models import Article, Book, BookCategory
from myforms.forms import MyBaseForm
from projects.models import ProjectArticle, ProjectBook, Project
from projects.forms import ProjectChoiceField
from tags.models import ListTagValue, TagCategory, ArticleTag
from tags.utils import get_all_list_tag_values
from actstream import action
from unified_permissions import has_permission

logger = logging.getLogger("publications.forms")

WORK_TYPES = (
    ("in_journal", "статья в журнале"),
    ("in_book", "книга"),
    ("in_collection", "статья в сборнике"),
)


class AddArticleBookBaseForm(forms.BaseForm):

    @property
    def work(self):
        return getattr(self, 'article_instance', None) or getattr(self, 'book_instance', None)

    def instance_linked(self):
        try:
            id = self.instance.id
            return bool(id)
        except:
            return False


class AddArticleBaseForm(MyBaseForm, AddArticleBookBaseForm):
    id = forms.IntegerField(widget=forms.HiddenInput, required=False) # id of journal or collection
    title = forms.CharField(label=u"Название статьи", max_length=255)
    year = YearField(label=u"Год издания", required=False)
    firstpage = forms.CharField(label=u"Первая страница", max_length=32, required=False)
    lastpage = forms.CharField(label=u"Последняя страница", max_length=32, required=False)

    def __init__(self, *args, **kwargs):
        # needed for event logging. Objects that can be created when saving a form.
        self.article_created = False
        self.journal_alias_created = False
        self.journal_created = False
        self.series_created = False
        self.collection_created = False
        self.user = kwargs.pop("user", None)
        super(AddArticleBaseForm, self).__init__(*args, **kwargs)

    def save(self, authors, extra_fields=[], user=None, edit_work=None):
        """Select/create/update article

        args:
            authors - list of authors of the article
            extra_fields - extra form fields which forms model's uniq key
            user - user saving the object
            edit_work - the work being edited, if this is edit mode. If work type changed, it contains actual object of 'old' type

        requirements:
            xor(self.journal_instance, self.collection_instance)

        creates
            self.article_instance
        """
        if not hasattr(self, "article_saved"):
            data = dict(
                    title=self.cleaned_data['title'],
                    journal=self.journal_instance,
                    volume=self.cleaned_data['volume'],
                    year=self.cleaned_data['year'],
                    collection=self.collection_instance,
                    firstpage=self.cleaned_data['firstpage'],
                    lastpage=self.cleaned_data['lastpage'],
                    )
            for field in extra_fields:
                data[field] = self.cleaned_data[field]  # number field, etc.

            if edit_work and not isinstance(edit_work, Article): # it's a Book being transformed into Article
                work_type_changed = True
            else:
                work_type_changed = False

            if edit_work and not work_type_changed:
                self.article_instance = edit_work
                for attr, value in data.items():
                    setattr(self.article_instance, attr, value)
                self.article_instance.save()
                # remove all existing authorships
                self.article_instance.notify_new_workers(authors, user)
                self.article_instance.authorships.all().delete()
            else:
                if work_type_changed:
                    data["doi"] = edit_work.doi
                self.article_instance = Article.objects.create(**data)
                self.article_created = True

            if work_type_changed:
                self.article_instance.abstract = edit_work.abstract # double save for avoiding binding LONG problem, see #1029
                self.article_instance.save()
                edit_work.attachments_old.all().update(book_old=None, article_old=self.article_instance) # update attachments
                # remove edit_work
                edit_work.category_memberships.all().delete()
                edit_work.notify_new_workers(authors, user)
                edit_work.authorships.all().delete()
                edit_work.delete()
            elif not edit_work:
                self.article_instance.notify_new_workers(authors, user)

            self.article_instance.add_authors(authors, user)
        self.article_saved = True


class AddArticleInJournalForm(AddArticleBaseForm):
    journal = forms.CharField(label=u"Журнал", max_length=255)
    number = forms.CharField(label=u"Номер журнала", max_length=255, required=False)
    volume = forms.IntegerField(label=u"Том", required=False)
    source_type = forms.CharField(widget=forms.HiddenInput, required=False) # "alias", "guess_alias" or "similar"

    def __init__(self, *args, **kwargs):
        super(AddArticleInJournalForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ["title", "journal", "id", "volume", "number", "year", "firstpage", "lastpage", "source_type"]

    def _get_similar(self):
        """Get similar journals without taking into account publisher

        Returns list of Journal's
        """
        journal_name = self.get_cleaned_list('journal')[0]
        return Journal.objects.get_similar(journal_name)

    def save(self, authors, user=None, edit_work=None):
        """Select/create journal and article

        creates
            self.journal_instance
            self.article_instance
        """
        if not hasattr(self, "saved"):
            id = self.cleaned_data['id']
            journal_name = self.cleaned_data['journal']
            objs = id and Journal.objects.filter(id=id)
            if objs:
                self.journal_instance = objs[0]
                if (Journal.objects.is_abbr(
                              journal_name,
                              self.journal_instance.name
                      )
                      and # this journal does not have such an alias
                      not JournalAlias.objects.filter(journal=self.journal_instance, alias__iexact=journal_name).exists()
                      and # the alias is not the same as the journal's name
                      journal_name.lower() != self.journal_instance.name.lower()
                   ):
                    self.journal_alias_instance = JournalAlias.objects.create(
                                    alias=journal_name,
                                    journal=self.journal_instance,
                                    creator=self.user)
                    self.journal_alias_created = True
                    logging.debug("Create alias: '%s' -> '%s'" % (journal_name, self.journal_instance))
            else:
                data = dict(name=journal_name)
                objs = Journal.objects.filter(**data)
                if objs:
                    self.journal_instance = objs[0]
                else:
                    data['creator'] = self.user
                    journal_instance = Journal(**data)
                    journal_instance.xml = journal_instance.get_xml()
                    journal_instance.save()
                    self.journal_instance = journal_instance
                    self.journal_created = True

            self.collection_instance = None
            AddArticleBaseForm.save(self, authors, ['number'], user=user, edit_work=edit_work)
        self.saved = True


class AddArticleInCollectionForm(AddArticleBaseForm):
    theses = forms.BooleanField(label=u"Тезисы", required=False)
    collection = forms.CharField(label=u"Сборник", max_length=255)
    volume = forms.IntegerField(label=u"Том", required=False)
    series = forms.CharField(label=u"Серия", max_length=255, required=False)

    def __init__(self, *args, **kwargs):
        super(AddArticleInCollectionForm, self).__init__(*args, **kwargs)
        for category in CollectionCategory.objects.all():
            self.fields['collection_category_%d' % category.id] = forms.BooleanField(label=category.name, required=False)
        self.fields.keyOrder = ["title", "collection", "id", "series", "volume", "theses", "year", "firstpage", "lastpage"] \
            + ["collection_category_%d" % category.id for category in CollectionCategory.objects.all()]
        if self.instance_linked(): # initialize the fields
            for category in self.instance.collection.categories.all():
                self.fields['collection_category_%d' % category.id].initial = True


    def _get_similar(self):
        """Get similar collections without taking into account series and location

        Returns list of Collection's
        """

        data = self.get_cleaned_dict("collection", "year", "volume", "theses", "series")
        data["title"] = data.pop("collection")
        if data["series"]:
            data["series__title"] = data.pop("series")
        data = dict((k, v) for k, v in data.iteritems() if v)
        return Collection.objects.get_similar(**data)

    def save(self, authors, publisher, location, user=None, edit_work=None):
        """Select/create collection and article

        creates
            self.collection_instance
            self.article_instance
        """
        if not hasattr(self, "saved"):
            id = self.cleaned_data['id']
            objs = id and Collection.objects.filter(id=id)
            if objs:
                self.collection_instance = objs[0]
            else:
                series = None
                if self.cleaned_data["series"]:
                    try:
                        series = Series.objects.get(title=self.cleaned_data["series"])
                    except Series.DoesNotExist:
                        series = Series(title=self.cleaned_data["series"], creator=self.user)
                        series.xml = series.get_xml()
                        series.save()
                        self.series_created = False
                    else:
                        self.series_created = True
                    if self.series_created:
                        self.series_instance = series

                data = dict(
                        title=self.cleaned_data["collection"],
                        year=self.cleaned_data["year"],
                        series=series,
                        location=location,
                        volume=self.cleaned_data["volume"],
                        theses=self.cleaned_data["theses"],
                        )
                objs = Collection.objects.filter(**data)
                if objs:
                    self.collection_instance = objs[0]
                else:
                    data['creator'] = self.user
                    collection_instance = Collection(publisher=publisher, **data)
                    collection_instance.xml = collection_instance.get_xml()
                    collection_instance.save()
                    self.collection_instance = collection_instance
                    self.collection_created = True

            self.journal_instance = None
            collection_categories = [(category, self.cleaned_data.get('collection_category_%d' % category.id, False)) for category in CollectionCategory.objects.all()]
            self.collection_instance.update_categories(collection_categories, user)

            AddArticleBaseForm.save(self, authors, [], user=user, edit_work=edit_work)
        self.saved = True


class AddBookForm(MyBaseForm, AddArticleBookBaseForm):
    title = forms.CharField(label=u"Название книги", max_length=255)
    year = YearField(label=u"Год издания")
    pages = forms.IntegerField(label=u"Количество страниц", required=False)
    redpages = forms.FloatField(label=u"Объём (п.л.)", required=False)
    isbn = forms.CharField(label=u"ISBN", max_length=255, required=False)
    ncopies = forms.IntegerField(label=u"Тираж", required=False)

    def __init__(self, *args, **kwargs):
        # needed for event logging.
        self.book_created = False
        self.user = kwargs.pop('user', None)
        super(AddBookForm, self).__init__(*args, **kwargs)
        for category in BookCategory.objects.all():
            self.fields['book_category_%d' % category.id] = forms.BooleanField(label=category.name, required=False)
        self.fields.keyOrder = ["title", "year", "pages", "redpages", "isbn", "ncopies"] + ["book_category_%d" % category.id for category in BookCategory.objects.all()]
        if self.instance_linked(): # initialize the fields
            for category in self.instance.categories.all():
                self.fields['book_category_%d' % category.id].initial = True

    def save(self, authors, publisher, location, user=None, edit_work=None):
        """Select/create book
        args
            user - user saving the object
        creates
            self.book_instance
        """
        if not getattr(self, "saved", False):
            data = dict(
                    title=self.cleaned_data["title"],
                    year=self.cleaned_data["year"],
                    pages=self.cleaned_data["pages"],
                    redpages=self.cleaned_data["redpages"],
                    isbn=self.cleaned_data["isbn"],
                    ncopies=self.cleaned_data["ncopies"],
                    location=location,
                    publisher=publisher,
                    )
            categories = [(category, self.cleaned_data.get('book_category_%d' % category.id, False)) for category in BookCategory.objects.all()]

            if edit_work and not isinstance(edit_work, Book): # it's an Article being transformed into Book
                work_type_changed = True
            else:
                work_type_changed = False


            if edit_work and not work_type_changed:
                self.book_instance = edit_work
                for attr, value in data.items():
                    setattr(self.book_instance, attr, value)
                self.book_instance.save()
                # remove all existing authorships
                self.book_instance.notify_new_workers(authors, user)
                self.book_instance.authorships.all().delete()
            else:
                if work_type_changed:
                    data["doi"] = edit_work.doi
                self.book_instance = Book.objects.create(**data)
                self.book_created = True

            if work_type_changed:
                self.book_instance.abstract = edit_work.abstract # double save for avoiding binding LONG problem, see #1029
                self.book_instance.save()
                edit_work.attachments_old.all().update(article_old=None, book_old=self.book_instance) # update attachments
                # remove edit_work
                edit_work.notify_new_workers(authors, user)
                edit_work.authorships.all().delete()
                edit_work.delete()
            elif not edit_work:
                self.book_instance.notify_new_workers(authors, user)

            self.book_instance.add_authors(authors, user)
            self.book_instance.update_categories(categories, user)

        self.saved = True


FORM_TYPES = (
    ("in_journal", AddArticleInJournalForm),
    ("in_book", AddBookForm),
    ("in_collection", AddArticleInCollectionForm),
)


class ArticleAdditionalForm(MyModelForm):
    '''A simple form for adding additional info to articles.'''

    tags = forms.ModelMultipleChoiceField(label=u'Типы статьи', widget=MyFilteredSelectMultiple(u"типы статьи",
                                                                                                is_stacked=False),
                                          queryset=ListTagValue.objects.none(), required=False)
    project = ProjectChoiceField(label=u'Добавить данную статью к НИР', required=False)

    tags_confirmation = forms.ModelMultipleChoiceField(
        label=u'Статусы проверки', widget=MyFilteredSelectMultiple(
            u"статусы проверки", is_stacked=False),
      queryset=ListTagValue.objects.none(), required=False)

    class Meta:
        model = Article
        fields = ('abstract', 'doi')

    # This method should probably be generalized like this:

    # def __init__(self, article, tagtype, *post_args):
    #     super(ArticleTagForm, self).__init__(*post_args)
    #     self.tagtype = tagtype
    #     tagtype_name_lower = self.tagtype.name[0].lower() + self.tagtype.name[1:]

    #     self.fields['tags'] = forms.ModelMultipleChoiceField(
    #         label=self.tagtype.name, widget=MyFilteredSelectMultiple(
    #             tagtype_name_lower, is_stacked=False),
    #         queryset=queryset, required=False)

    #     article_tags = get_all_list_tag_values(article, tag_type=self.tagtype)
    #     self.fields['tags'].initial = [x for x in queryset if x in article_tags]


    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ArticleAdditionalForm, self).__init__( *args, **kwargs)

        queryset = ListTagValue.objects.get_article_category_tags()
        self.fields['tags'].queryset = queryset
        instance_tags = get_all_list_tag_values(self.instance)
        self.fields['tags'].initial = [x for x in queryset if x in instance_tags]

        if self.user and has_permission(self.user, "edit_status_tags", self.instance):
            confirmation_queryset = ListTagValue.objects.get_tags_by_category(TagCategory.objects.confirmation)
            self.fields['tags_confirmation'].queryset = confirmation_queryset
            self.fields['tags_confirmation'].initial = [x for x in confirmation_queryset if x in instance_tags]


    def commit_save(self, request, object):
        tags = self.cleaned_data.get('tags', None)
        for tag in self.fields['tags'].queryset:
            kwargs = {'article': object,
                      'category': TagCategory.objects.article_type(),
                      'list_value': tag}
            if tag in tags:
                new_tag, created = ArticleTag.objects.get_or_create(defaults={'user': request.user}, **kwargs)
                if created and self.user:
                    action.send(self.user,
                        verb=u"добавил статус '%s'" % tag,
                        action_object=self.instance,
                        target=new_tag)
            else:
                for existing_tag in ArticleTag.objects.filter(**kwargs):
                    action.send(self.user,
                        verb=u"удалил статус '%s'" % tag,
                        action_object=self.instance,
                        target=existing_tag)
                    existing_tag.delete()

        if self.user and has_permission(self.user, "edit_status_tags", self.instance):
            tags_confirmation = self.cleaned_data.get('tags_confirmation', None)
            for tag in self.fields['tags_confirmation'].queryset:
                kwargs = {'article': object,
                          'category': TagCategory.objects.confirmation,
                          'list_value': tag}
                if tag in tags_confirmation:
                    new_tag, created = ArticleTag.objects.get_or_create(defaults={'user': request.user}, **kwargs)
                    if created and self.user:
                        action.send(self.user,
                            verb=u"добавил статус '%s'" % tag,
                            action_object=self.instance,
                            target=new_tag)
                else:
                    for existing_tag in ArticleTag.objects.filter(**kwargs):
                        action.send(self.user,
                            verb=u"удалил статус '%s'" % tag,
                            action_object=self.instance,
                            target=existing_tag)
                        existing_tag.delete()
        project = self.cleaned_data.get('project', None)
        if project:
            defaults = {'creator': request.user}
            ProjectArticle.objects.get_or_create(project=project, article=object, defaults=defaults)
            messages.success(request, u"Статья успешно добавлена к НИР.")
            if request.user:
                    action.send(request.user, verb=u'добавил стаью к НИР', action_object=object, target=project)


        super(ArticleAdditionalForm, self).commit_save(request, object)


class BookAdditionalForm(MyModelForm):
    '''A simple form for adding additional info to books.'''
    project = ProjectChoiceField(label=u'Добавить данную книгу к НИР', required=False)

    class Meta:
        model = Book
        fields = ('abstract', 'doi',)


    def commit_save(self, request, object):
        project = self.cleaned_data.get('project', None)
        if project:
            defaults = {'creator': request.user}
            ProjectBook.objects.get_or_create(project=project, book=object, defaults=defaults)
            messages.success(request, u"Книга успешно добавлена к НИР.")
            if request.user:
                    action.send(request.user, verb=u'добавил книгу к НИР', action_object=object, target=project)
        super(BookAdditionalForm, self).commit_save(request, object)


class ParsingEventForm(forms.Form):
    '''A simple form for storing user negative reaction to automatic parsing.'''
    id = forms.IntegerField(widget=forms.HiddenInput)
    dissatisfied = forms.BooleanField(label=u"Результаты автоматического разбора оставляют желать лучшего", required=False)







class YearsListForm(forms.Form):
    selectedYearClusterCategories = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(), required=False)


    def __init__(self, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        print args[1]["years"]
        self.fields["selectedYearClusterCategories"].queryset = args[1]["years"]


