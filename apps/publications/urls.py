# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

from publications.models import *


urlpatterns = patterns(
    'publications.views',
    url(r'^add/$', direct_to_template, {'template': 'publications/add_citation.html'}, name='publications_add'),
    url(r'^add/next/$', "parse_data", {'from_queue': True}, name="publications_add_next_from_queue"),
    url(r'^add/step2/$', "parse_data", name="publications_add_step2"),
    url(r'^add/step3/$', "add_work", name="publications_add_step3"),
    url(r'^add/step3/save/$', "add_work", {'save_publication': True}, name="publications_add_step3_save"),
    url(r'^add/queue/clear/$', "clear_adding_queue", name="publications_add_queue_clear"),

    # publication detail page
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/$', "publication_detail", name='publications_publication_detail'),
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/additional/$', "publication_detail_additional", name='publications_publication_detail_additional'),
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/additional/abstract/$', "publication_detail_additional", {'section': 'abstract'}, name='publications_publication_detail_additional_abstract'),
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/additional/tags/$', "publication_detail_additional", {'section': 'tags'}, name='publications_publication_detail_additional_tags'),
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/additional/projects', "publication_detail_additional", {'section': 'projects'}, name='publications_publication_detail_additional_projects'),
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/additional/tags/confirmation/$', "publication_detail_additional", {'section': 'tags_confirmation'}, name='publications_publication_detail_additional_tags_confirmation'),
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/additional/attachments/$', "publication_detail_additional", {'section': 'attachments'}, name='publications_publication_detail_additional_attachments'),
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/save/additional/$', "save_additional_info", name='publications_save_additional_info'),

    # publication export to format pages
    # format page
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)\.(?P<format_extension>.+)$', "publication_detail_in_format", name='publications_publication_detail_in_format'),
    # the same page for javascript ajax loading (different address)
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/format/(?P<format_extension>.+)/$', "publication_detail_in_format"),
    # direct download link
    url(r'^(?P<publication_type>article|book)/download/(?P<publication_type2>article|book)(?P<publication_id>\d+)\.(?P<format_extension>.+)$', "publication_detail_in_format", {'download': True}, name='publications_publication_detail_in_format_download'),

    # publication citation in style, used in javascript ajax loading
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/style/(?P<style>.+)/$', "publication_detail_in_style"),

    # edit publication
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/edit/$', "edit_work", name="publications_edit"),

    # request search in external systems
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/requestsearch/$', "request_search", name="publications_request_search"),

    # delete publication
    url(r'^(?P<publication_type>article|book)/(?P<publication_id>\d+)/delete/$', "publication_delete", name="publications_publication_delete"),

    # years pages
    url(r'^years/$', "list_years", name='publications_list_years'),
    url(r'^years/(?P<year>\d+)/$', "list_by_year", name='publications_list_by_year'),
    url(r'^years/(?P<year>\d+)/style/(?P<style>.+)/$', "list_by_year_in_style", name='publications_list_by_year_in_style'),
    url(r'^years/(?P<year>\d+).bib$', "list_by_year_bibtex", name='publications_list_by_year_bibtex'),

    # convert citation to bibtex without saving to database
    url(r'^citation2bibtex/$', "citation2bibtex", name="publications_citation2bibtex"),

    # search
    url(r'^search/(?P<publication_type>articles|books|theses)/$', "publications_search", name="search_publications"),
)
