# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.utils.http import urlencode
import httplib
import logging
import urllib2
import xml.dom.minidom

from publications.parsers.base_parser import BaseParser, ParsingEntryError
from publications.utils import split_authors

logger = logging.getLogger("publications.parsers.freecite")


class FreeciteParser(BaseParser):

    name = "freecite"

    def preprocess_entry(self, entry):
        '''Gets a single entry and prepare it for parsing.
        '''
        if isinstance(entry, str) or isinstance(entry, unicode): # it is essentially a string
            return " ".join(filter(bool, map(unicode.strip, entry.split("\n"))))
        return entry

    def entry2fields(self, entry):
        '''Parse citation entry via Freecite external parser. 'Context' is needed to pass xml to template via updating of context dictionary.'''
        try:
            opener = urllib2.build_opener()
            opener.addheaders = [
                ("Accept", "application/xml, text/xml, */*; q=0.01"),
            ]
            try:
                freecite_url = settings.FREECITE_URL
            except AttributeError:
                logger.error("FREECITE_URL setting is not set")
                messages.debug(self.request, 'Параметр FREECITE_URL не задан в настройках')
                raise ParsingEntryError
            if len(entry) > 2000:
                logger.warning("Entry length > 2000 (%d), cut it not to get down freecite service." % len(entry))
                messages.info(self.request, u"Введенная информация была обрезана с %d до 2000 символов." % len(entry))
                entry = entry[:2000]
            xml_data = opener.open(freecite_url, urlencode({'citation': entry})).read()
            logger.debug("Received Freecite parsing response (type %s): %s", type(xml_data), xml_data)
        except (urllib2.URLError, httplib.BadStatusLine):
            logger.debug("Freecite parsing failed.")
            logger.error("Freecite service does not seem to work")
            messages.debug(self.request, 'Сервис Freecite не работает.')
            # mail freecite service manager
            try:
                freecite_manager = User.objects.get(username=settings.FREECITE_MANAGER[0])
                try:
                    from_email = settings.SERVER_EMAIL
                except:
                    from_email = settings.DEFAULT_FROM_EMAIL
                email_body = render_to_string('publications/email_freecite_not_working.txt', {'manager': freecite_manager, 'freecite_url': freecite_url})
                email_subject = u"Сервис Freecite не работает"
                freecite_manager.email_user(email_subject, email_body, from_email=from_email)
            except:
                pass
            raise ParsingEntryError

        # parse XML data, retrieve similar articles etc.
        try:
         dom = xml.dom.minidom.parseString(xml_data)
        except:
         raise ParsingEntryError
        self.context["xml"] = dom.toprettyxml(indent="    ")

        # create and fill a dictionary

        # if element is a tuple, first element in the tuple is inner name of the field, and the second is outer (external, from script) name of the tag.
        field_names = ["title", "journal", ("collection", "booktitle"), ("editors", "editor"), "volume", "publisher", "institution", "location", "number", "pages", "isbn", "year", "tech", "note", ("work_type", "ctx:format"), ("firstpage", "rft:spage"), ("lastpage", "rft:epage")]
        fields = {}
        for name in field_names:
            if isinstance(name, tuple):
                inner_name = name[0]
                external_tag = name[1]
            else: # this is a string, inner and outer name coincide
                inner_name = name
                external_tag = name
            try:
                fields[inner_name] = dom.getElementsByTagName(external_tag)[0].firstChild.data
            except (IndexError, AttributeError):
                fields[inner_name] = ""

        # parse authors separately
        # get a list of triple-tuples (lastname, firstname, middlename)
        authors = [split_authors(item.firstChild.data)[0] for item in dom.getElementsByTagName("author") if getattr(item, "firstChild", None)]
        delimiter = lambda f: "" if len(f) == 1 or len(f) == 2 and f[-1] == "." else " " # between firstname and middlename
        add_dot = lambda x: x + "." if len(x) == 1 else x
        fields["authors"] = ", ".join(" ".join((lastname + " " + add_dot(firstname) + delimiter(firstname) + add_dot(middlename)).split()) for (lastname, firstname, middlename) in authors) # extra join-split is added to remove extra whitespaces between words

        # copy institution to publisher if publisher is empty
        if not fields['publisher'] and fields['institution']:
            fields['publisher'] = fields['institution']

        # determine the type of the work (book, article in a journal, article in a collection)
        if "journal" in fields['work_type']: # this is a article in a journal
            fields['work_type'] = "journal"
        elif "collection" in fields['work_type']: # this is a article in a collection
            fields['work_type'] = "collection"
        elif "book" in fields['work_type']: # this is a book or an article in a collection
            # if title and collection(tag booktitle) are set, this is an article in a collection
            if fields["title"] and fields["collection"]:
                fields['work_type'] = "collection"
            # if title is not set and collection(tag booktitle) is set, this is a book. FIXME: probably field collection should be named booktitle, as it can be a title of a book, not only of a collection.
            elif fields["collection"]:
                fields['work_type'] = "book"
        else:
            fields['work_type'] = "journal"

        # smart detection of theses
        if fields['work_type'] == 'collection':
            collection_name = fields["collection"].lower()
            if collection_name and (u"тезис" in collection_name or "theses" in collection_name or "thesis" in collection_name):
                fields['theses'] = True

        return fields
