# -*- coding: utf-8 -*-
from actstream import action
from django.contrib import messages
from pytils.templatetags.pytils_numeral import get_plural, choose_plural
import logging
import re

from common.utils.smart_pickle import smart_pickle, smart_unpickle
from publications.models import ParsingEvent
from publications.parsers.utils import parse_pages

logger = logging.getLogger("publications.parsers.base_parser")


class ParsingEntryError(Exception):
    pass


class PreprocessDataError(Exception):
    pass


class URLLoadingError(Exception):
    pass


class BaseParser(object):
    '''Base class for publication parsers. The methods that can be rewritten are: data2entries, preprocess_entry, entry2fields, postprocess_fields, pickle_entries, pickle_entry, unpickle_entry, entry2text, show_data.'''

    name = "base"

    def __init__(self, request=None, context=None):
        self.request = request
        self.context = context
        self.user = request.user if request else None
        self.data = None # raw user data

    def parse(self, data_or_parsingevent):
        '''Main parsing function, gets an input data (which can be raw data or a parsing event from user queue) and returns parsing_event with parsed data.'''
        # convert data to a list of entries ready for parsing, and then parse the first entry
        if isinstance(data_or_parsingevent, ParsingEvent): # this is a parsing event from queue
            parsing_event = data_or_parsingevent
            entry = self.parsing_event2entry(parsing_event) # get entry for parsing from queued parsing event
        else: # new raw data
            self.data = data_or_parsingevent.strip()
            entry = self.data2entry(self.data) # input data -> first entry
            parsing_event = ParsingEvent.objects.create(user=self.user, source_data=self.pickle_entry(entry), type="in queue", method=self.name)
        # parse the entry
        try:
            parsing_event.parsed_data = self.parse_entry(entry)
        except ParsingEntryError:
            raise ParsingEntryError(parsing_event) # pass information about the parsing event to the upper level of execution
        parsing_event.type = "parsed"
        parsing_event.save()
        action.send(self.user, verb=u'разобрал данные с помощью %s' % self.name, target=parsing_event)
        return parsing_event # parsing_event containing parsed data

    def parsing_event2entry(self, parsing_event):
        '''Gets a parsing event from queue and returns an extracted entry from it ready for parsing.'''
        if parsing_event.method != self.name:
            if self.name != "dummy":
                raise PreprocessDataError # find the parser by which the event has been entered in queue, and fallback to the dummy one.
            else:
                logger.warning("Parsing an entry from queue with another method. id: %d, method: %s, parsing with: %s", parsing_event.id, parsing_event.method, self.name)
                parsing_event.method = self.name
                parsing_event.save()
        entry = self.unpickle_entry(parsing_event.source_data)
        #parser.original_data = parser.entry2text(entry)
        return entry

    def data2entry(self, data):
        '''Get initial data entered by user and transform it into a list of entries for future parsing.'''
        # simple splitting: split by double newline (=split by empty lines)
        entries = self.data2entries(data)
        if len(entries) > 1: # split multiple entries and put in queue
            ParsingEvent.objects.create(user=self.user, source_data=data, parsed_data=self.pickle_entries(entries), type="preprocessed", method=self.name) # event storing result of extracting entries. If there is a single entry, this event is not created because it would duplicate the main event
            self.add_to_queue(entries[1:])
        # get the first entry
        entry = entries[0]
        return entry

    def data2entries(self, data):
        '''Get initial data entered by user and transform it into a list of entries for future parsing.'''
        logger.debug("Splitting input data in parser %s: %s", self.name, data)
        sep = re.compile('\n\s*\n')
        entries = sep.split(data)
        logger.debug("Splitting input data in parser %s: got %d entries", self.name, len(entries))
        return entries

    def add_to_queue(self, entries):
        # store all entries in queue
        parsing_events = [ParsingEvent.objects.create(user=self.user, source_data=self.pickle_entry(entry), type="in queue", method=self.name) for entry in entries]
        action.send(self.user, verb=u'поставил данные в очередь на разбор', description="Парсер: %s, %d записей: %s" % (self.name, len(parsing_events), ", ".join(str(event.id) for event in parsing_events)))
        messages.success(self.request, u"Успешно %s %s, из них %s в очередь." % (choose_plural(len(entries) + 1, (u"разобрана", u"разобраны", u"разобрано")), get_plural(len(entries) + 1, (u"запись", u"записи", u"записей")), get_plural(len(entries), (u"поставлена", u"поставлены", u"поставлено"))))
        #self.original_data = parser.entry2text(entries[0]) # original data is the single entry that is parsed now

    def parse_entry(self, entry):
        '''Parse a single entry. Note: this method should be able to operate on an entry retrieved from database (and unpickled),
          so that preprocess_data() is only called once when getting a list of entries, and then this method runs independently for each entry.
        '''
        entry = self.preprocess_entry(entry)
        fields = self.entry2fields(entry)
        fields = self.postprocess_fields(fields, entry)
        return fields

    def preprocess_entry(self, entry):
        '''Gets a single entry and prepare it for parsing.
        '''
        return entry

    def entry2fields(self, entry):
        fields = {}
        return fields

    def postprocess_fields(self, fields, entry):
        '''Post-process parsed fields, e.g. adding some common fields and smart processing pages fields.'''
        # smart pages parsing
        fields["firstpage"], fields["lastpage"], fields["pages"] = parse_pages(fields.get("firstpage"), fields.get("lastpage"), fields.get("pages"))
        fields["raw_string"] = self.show_data(entry) # if original data is accessible (this is not an event from queue), show it. Otherwise convert entry to text representation.
        return fields

    def pickle_entries(self, entries):
        '''Pickles list of entries for storing in a database.'''
        return smart_pickle(entries)

    def pickle_entry(self, entry):
        '''Pickles an entry for storing in a database.'''
        return smart_pickle(entry)

    def unpickle_entry(self, entry):
        '''Unpickles an entry stored in a database for parsing.'''
        return smart_unpickle(entry)

    def unpickle_entry_to_text(self, pickled_entry):
        '''Unpickles an entry stored in a database for display.'''
        entry = self.unpickle_entry(pickled_entry)
        text = self.entry2text(entry)
        return text

    def entry2text(self, entry):
        '''Converts an entry from an internal to text representation for using in representation for user or admin.'''
        return unicode(entry)

    def show_data(self, entry):
        '''Show original data based on self.data and/or entry.'''
        return self.data or self.entry2text(entry)
