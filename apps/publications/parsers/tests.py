# -*- coding: utf-8; -*-
from collections import defaultdict
from django.conf import settings
from django.contrib.auth.models import User
from django.test.client import Client
from django.utils import unittest
from os.path import join
import os

from publications.models import ParsingEvent


class AddPublicationTestCase(unittest.TestCase):

    def setUp(self):
        self.client = Client()
        try:
            user = User.objects.get(username="test")
            user.set_password("test")
            user.save()
        except User.DoesNotExist:
            user = User.objects.create_user('test', 'test@example.com', 'test')
        self.client.login(username='test', password='test')

    def test_add(self):
        examples = self.get_data_examples()
        past_events = []
        results = []
        for example in examples:
            print "Parsing file: %s" % example['file']
            response = self.client.post('/publications/add/step2/', {'citation': example['data']}, follow=True)
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.context['fields']['title']) # context in last response contains not-null fields['title'] variable.
            new_events = [event for event in ParsingEvent.objects.all() if event not in past_events]
            freq = defaultdict(int)
            for event in new_events:
                freq[event.method] += 1
            for method, frequency in freq.items():
                if frequency > 1:
                    frequency -= 1 # don't count preprocessing event
                print "%s parsed with %s (%d items)" % (example['file'], method, frequency)
                results.append({'file': example['file'], 'method': method, 'freq': frequency})
            past_events.extend(new_events)
        print 'Results:'
        print "\n".join("%s: %s (%d)" % (res['file'], res['method'], res['freq']) for res in results)

    def get_data_examples(self):
        '''Get all examples from parsers/examples folder.'''
        examples = []
        base_dir = join(settings.SETTINGS_DIR, 'apps', 'publications', 'parsers', 'examples')
        for dirpath, dirnames, filenames in os.walk(base_dir):
            for filename in filenames:
                filepath = join(dirpath, filename)
                f = open(filepath)
                example = f.read()
                f.close()
                examples.append({'file': lastpath(filepath), 'data': example})
        return examples

def get_parent_dir(filepath):
    return os.path.basename(os.path.dirname(filepath))

def lastpath(filepath):
    return join(get_parent_dir(filepath), os.path.basename(filepath))
