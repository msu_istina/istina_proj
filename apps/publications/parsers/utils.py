# -*- coding: utf-8 -*-
import re


def parse_pages(first, last, total):
    u'''
    Smart parsing and copy of three fields: firstpage, lastpage, and pages (total number of pages).
    >>> parse_pages('5', '10', '25')
    ('5', '10', '25')
    >>> parse_pages('', '', '23--25')
    ('23', '25', '25')
    >>> parse_pages('25', '', '')
    ('25', '', '25')
    >>> parse_pages('25', None, None)
    ('25', '', '25')
    >>> parse_pages('', '25', '')
    ('25', '25', '25')
    >>> parse_pages(None, '25', None)
    ('25', '25', '25')
    >>> parse_pages('', '', '25')
    ('25', '25', '25')
    >>> parse_pages(None, None, '25')
    ('25', '25', '25')
    >>> parse_pages('5', '10', '')
    ('5', '10', '10')
    >>> parse_pages('hjsf', 'sdfjkl', '12')
    ('hjsf', 'sdfjkl', '12')
    >>> parse_pages('023564-1', '023564-10', '')
    ('023564-1', '023564-10', '023564-10')
    >>> parse_pages('id 12345', '', '')
    ('id 12345', '', 'id 12345')
    >>> parse_pages('id 012345-1', '', '')
    ('id 012345-1', '', 'id 012345-1')
    >>> parse_pages('12345--565456', '', '')
    ('12345--565456', '', '12345--565456')
    >>> parse_pages('', '12345--565456', '')
    ('12345--565456', '12345--565456', '12345--565456')
    >>> parse_pages('', '', '')
    ('', '', '')
    >>> parse_pages(None, None, None)
    ('', '', '')
    >>> print repr(parse_pages(u"б", u"в", None)).decode("unicode-escape")
    (u'б', u'в', u'в')
    >>> print repr(parse_pages(u"б", None, None)).decode("unicode-escape")
    (u'б', '', u'б')
    '''
    two_pages = re.compile(r'^([^\-]+)\-+([^\-]+)$') # matches a string containing two values separated by dashes

    # using int() function is not reliable, since int('0123451') = 123451 but this was a correct page number

    def extract_separator(field):
        match = two_pages.match(field)
        if match:
            return match.groups()[0], match.groups()[1] # return first and last page
        return None

    if total and not first and not last:
        pages = extract_separator(total)
        if pages:
            first = pages[0]
            last = pages[1]
            total = last

    if not total:
        total = last or first
    elif not first and not last:
        first = total
        last = total

    if last and not first:
        first = last

    convert_none = lambda x: "" if x is None else x
    return tuple(map(convert_none, (first, last, total))) # return them unchanged, but converted from None to empty strings


def extract_number(field, convert=False):
    '''If a field contains only one number with possibly some characters, extract that number.'''
    if not field:
        return ""
    numbers = re.findall("\d+", field)
    if len(numbers) == 1: # only one number is parsed
        number = numbers[0]
        if convert:
            try:
                number = int(number)
            except:
                pass
        return number
    return field # otherwise return the original value


if __name__ == "__main__":
    import doctest
    doctest.testmod()
