# -*- coding: utf-8 -*-
from publications.parsers.base_parser import BaseParser


class DummyParser(BaseParser):
    '''Dummy parser that 'always' succeeds.'''

    name = "dummy"

    def entry2fields(self, entry):
        '''Parse a single entry.'''
        return {'title': entry, 'work_type': 'journal'}
