# -*- coding: utf-8 -*-
from subprocess import Popen, PIPE
import logging
import re

from publications.parsers.base_parser import PreprocessDataError
from publications.parsers.mods import ModsParser

logger = logging.getLogger("publications.parsers.pubmed")


class PubMedParser(ModsParser):

    name = "pubmed"

    def data2entries(self, data):
        '''Get initial data entered by user and transform it into a list of entries for future parsing.
        '''
        # check if this is PubMed format
        if not re.match(r'\s*\<\?xml version="1.0"\?\>\s*\<\!DOCTYPE PubmedArticleSet.*', data):
            raise PreprocessDataError
        # transform pubmed to intermediate mods xml format
        process = Popen(["med2xml", "-i", "utf8"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        mods_xmlstring, status = process.communicate(input=data.encode('utf-8'))
        if status.endswith("med2xml: Processed 0 references.\n"): # this was not a pubmed format
            raise PreprocessDataError
        # now we have a mods xml, use ModsParser
        return super(PubMedParser, self).data2entries(mods_xmlstring)
