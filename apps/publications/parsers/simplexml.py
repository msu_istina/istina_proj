# -*- coding: utf-8 -*-
from lxml import etree
import logging

from publications.parsers.base_parser import BaseParser, PreprocessDataError

logger = logging.getLogger("publications.parsers.simplexml")


class SimpleXMLParser(BaseParser):
    '''This parser is currently heavily used as auxiliary, but can be used among primary parsers as well.'''

    name = "simplexml"

    def data2entries(self, data=None, tree=None):
        '''Get initial data (xml string) entered by user and transform it into a list of entries for future parsing.
        If @data is specified, it is initial data from user (raw simplexml string).
        If @tree is specified, it is an simplexml tree passed from another parser.
        '''
        if tree is None:
            try:
                tree = etree.fromstring(data.encode('utf8'))
            except etree.XMLSyntaxError: # parsing was unsuccessful, this is probably not an xml or a not well-formed xml
                logger.debug("Simplexml parsing failed.")
                raise PreprocessDataError
        # even if it is xml, it should contain root "citations" tag indicating it's SimpleXML format.
        if tree.tag != 'citations':
            raise PreprocessDataError
        entries = tree.getchildren()
        if not entries:
            raise PreprocessDataError
        return entries

    def entry2fields(self, entry):
        '''Process an entry which is an xml Element.'''

        # create and fill a dictionary

        # if element is a tuple, first element in the tuple is inner name of the field, and the second is outer (external, from script) name of the tag.
        field_names = ["title", "journal", ("collection", "booktitle"), "series", ("editors", "editor"), "volume", "publisher", "institution", "location", "number", "issue", "pages", "isbn", "issn", "year", "doi", "tech", ("note", "abstract"), "work_type", ("firstpage", "spage"), ("lastpage", "epage")]
        fields = {}
        for name in field_names:
            if isinstance(name, tuple):
                inner_name = name[0]
                external_tag = name[1]
            else: # this is a string, inner and outer name coincide
                inner_name = name
                external_tag = name
            try:
                fields[inner_name] = entry.find(external_tag).text
            except (IndexError, AttributeError):
                fields[inner_name] = ""
            if fields[inner_name] == None:
                fields[inner_name] = ""

        # fix year, mods format 'feature'
        fields['year'] = fields['year'][:4]

        # copy issue to number is number is empty
        if not fields['number'] and fields['issue']:
            fields['number'] = fields['issue']

        # parse authors separately
        # get a list of triple-tuples (lastname, firstname, middlename)
        fields["authors"] = ''
        try:
            authors_nodes = entry.find('authors').findall('author')
        except AttributeError:
            pass
        else:
            authors = []
            for author_node in authors_nodes:
                firstname = ''
                middlename = ''
                lastname = ''
                givennames = author_node.findall('givenname')
                if len(givennames) == 1:
                    firstname = givennames[0].text
                elif len(givennames) > 1:
                    middlename = givennames[-1].text # last of given names
                    firstname = " ".join(node.text for node in givennames[:-1])
                lastname = getattr(author_node.find('familyname'), 'text', '')
                if lastname is None:
                    if middlename:
                        lastname, middlename = middlename, ''
                    elif firstname:
                        lastname, firstname = firstname, ''
                    else:
                        continue
                authors.append((lastname, firstname, middlename))
            delimiter = lambda f: "" if len(f) == 1 or len(f) == 2 and f[-1] == "." else " " # between firstname and middlename
            add_dot = lambda x: x + "." if len(x) == 1 else x
            join_names = lambda lastname, firstname, middlename: lastname + " " + add_dot(firstname) + delimiter(firstname) + add_dot(middlename)
            fields["authors"] = ", ".join(" ".join(join_names(*names).split()) for names in authors) # extra join-split is added to remove extra whitespaces between words

        # copy institution to publisher if publisher is empty
        if not fields['publisher'] and fields['institution']:
            fields['publisher'] = fields['institution']

        if fields['work_type'] not in ["journal", "collection", "book"]:
            fields['work_type'] = "journal"

        # smart detection of theses
        if fields['work_type'] == 'collection':
            collection_name = fields["collection"].lower()
            if collection_name and (u"тезис" in collection_name or "theses" in collection_name or "thesis" in collection_name):
                fields['theses'] = True

        return fields

    def pickle_entries(self, entries):
        '''Pickles list of entries for storing in a database.'''
        return "\n".join(self.pickle_entry(entry) for entry in entries)

    def pickle_entry(self, entry):
        '''Pickles an entry for storing in a database.'''
        # just convert entry to text and store in database.
        return self.entry2text(entry)

    def unpickle_entry(self, entry):
        '''Unpickles an entry stored in a database for parsing.'''
        return etree.fromstring(entry)

    def unpickle_entry_to_text(self, pickled_entry):
        '''Unpickles an entry stored in a database for display.'''
        # pickled entry is essentially a plain xml
        text = pickled_entry
        return text

    def entry2text(self, entry):
        '''Converts an entry from an internal to text representation for using in representation for user or admin.'''
        return etree.tostring(entry, encoding=unicode, pretty_print=True)
