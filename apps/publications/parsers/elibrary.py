# -*- coding: utf-8 -*-
from django.contrib import messages
from urlparse import urlparse
import cookielib
import logging
import lxml.etree
import re
import socket
import sys
import urllib2

from common.utils.smart_pickle import smart_pickle, smart_unpickle
from publications.parsers.base_parser import BaseParser, ParsingEntryError, PreprocessDataError, URLLoadingError
from publications.parsers.utils import extract_number

logger = logging.getLogger("publications.parsers.elibrary")


_TEMPLATES = {
    'journal':
        """@article{%(id)s,
            author = "%(author)s",
            title = "%(title)s",
            journal = "%(journal)s",
            volume = "%(vol)s",
            number = "%(issue)s",
            year = "%(year)s",
            pages = "%(pages)s",
            url = "%(url)s"
        }""",
    'collection':
        """@inproceedings{%(id)s,
            author = "%(author)s",
            title = "%(title)s",
            year = "%(year-proc)s",
            pages = "%(pages)s",
            url = "%(url)s"
        }""",
    'inproceedings_author_page':
        """@inproceedings{%(id)s,
            author = "%(author)s",
            title = "%(title)s",
            booktitle = "%(collection)s",
            year = "%(year)s",
            pages = "%(pages)s"
        }"""
}

# config for publication page

# xpath finding a minimal container of a publication
_PUBLICATION_ENTRY_XPATH = '//td[form[@name="results"]]'

_PUBLICATION_FIELD_NAMES = {
    'type':           u'Тип',
    'author':       u'Авторы',
    'title':            u'Название',
    'journal':       u'Журнал',
    'volume':      u'Том',
    'number':     u'Номер',
    'collection':   u'Название',
    'series':         u'Серия',
    'publisher':   u'Издательство',
    'location':     u'Место издания',
    'year':           u'Год выпуска',
    'year-proc':   u'Год издания',
    'pages':         u'Страницы',
    'pages-proc': u'Страницы',
    'note': u'Аннотация'
}

_PUBLICATION_FIELD_XPATH_MAIN = '//tr[td[font[text() = "%s"]]]'

_PUBLICATION_FIELD_XPATH_SUFFIXES = {
    'type':            '/td[4]/font',
    'author':        ('/td[2]/font/a/b', '/td[2]/font/b/font', '/td[2]/center/a/b', '/td[2]/center/b/font'),
    'title':             '/td[2]/font/b',
    'collection':    '/td[2]/font/b',
    'series':          '/td[4]/font',
    'location':       '/td[4]/font',
    'journal':        '/td[2]/a/b',
    'volume':       ('/td[2]', '/td[2]/a'),
    'year':            ('/td[2]', '/td[2]/a'),
    'pages':         '/td[4]/font',
    'pages-proc': '/td[2]/font',
    'default':        '/td[2]/a'
}

_PUBLICATION_FIELD_XPATH_OTHER = { # special patterns
    'note': '//tr[td/table/tr/td/font[text() = "%s"]]/td[2]/table/tr/td/font'
}

# config for author page

# xpath locating number of total records and records on the current author page
_AUTHOR_PAGES_XPATH = ('//form[@name="results"]/center[1]/b/font', '//form[@name="results"]/center[1]/font/b')
# xpath locating the current page number of the author page
_AUTHOR_CURRENT_PAGE_XPATH = '//input[@name="pagenum"]/@value'

# xpath finding a minimal container of a publication on an author page
# element is either a normal item (with an item link) or has links to it (if it is from a references list)
_AUTHOR_ENTRY_XPATH = '//tr[td[a[contains(@href, "cit_items.asp?")]]]/td[2]|//td[a[contains(@href, "item.asp?id=")]]'

_AUTHOR_JOURNAL_XPATH = './/a[contains(@href, "issueid") or contains(@href, "titleid")]'

_AUTHOR_FIELD_XPATH = {
    'author':              './/font[1]/i',
    'title':                 ('./a/b', './b/font'),
    'collection':         './font/a[contains(@href, "item.asp")]',
    'journal':             ('./font/a[contains(@href, "issueid=")][1]', './font/a[contains(@href, "titleid=")][1]'),
    'volume':             './font/a[contains(@href, "volume=")]',
    'number':            './font/a[contains(@href, "issueid=")][2]',
    'year':                 './font/a[contains(@href, "jyear=")]',
    'pages':               './font',
    'publication_url': './a[contains(@href, "item.asp")]', # url of the publication page, if it exists
    'collection_url':   './font/a[contains(@href, "item.asp")]', # url of the collection page, if it exists
    'default':             './font'
}

_AUTHOR_FIELD_REGEXP = {
    'number': u'№[ \t\r\n.]*([1-9][0-9]*)',
    'volume':  u'Т[ \t\r\n.]*([1-9][0-9]*)',
    'year':       u'([1,2][0-9][0-9][0-9])[ \t\r\n.]*[№Т]',
}

_AUTHOR_FIELDS_WITHOUT_SUFFIX = ['publication_url', 'collection_url'] # fields' keys which xpaths should return nodes, not text

_XPATH_SUFFIX = '/descendant-or-self::*/text()'


class ElibraryLoadingError(Exception):
    '''Indicates problems with loading elibrary pages.'''
    pass


class ElibraryParser(BaseParser):

    name = "elibrary"

    def __init__(self, *args, **kwargs):
        super(ElibraryParser, self).__init__(*args, **kwargs)
        self.cookiejar = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cookiejar))

    def data2entries(self, data):
        '''Get initial data entered by user and transform it into a list of entries for future parsing.
        '''
        # check url if this is not an url with elibrary domain, raise exception
        parsed_url = urlparse(data)
        if parsed_url.netloc not in ['elibrary.ru', 'www.elibrary.ru']: # not valid url
            raise PreprocessDataError
        self.url = data

        # parse url and load xml tree
        try:
            self.tree = self.load_url(self.url)
        except URLLoadingError:
            raise ElibraryLoadingError

        # make id of bibtex entry
        if isinstance(data, (str, unicode)) and '?' in data:
            self.id = "elibrary_" + data.rsplit('?', 2)[-1].rsplit('=', 1)[-1]
        else:
            self.id = None

        # detect the type of the url (author page / publication page)
        self.detect_page_type(data)
        if not self.type:
            logger.debug("Elibrary parsing failed, unknown url.")
            raise ElibraryLoadingError

        # extract list of entries from the webpage
        if self.type == "publication":
            # extract minimal div containing information on publication, and store it as an entry
            nodes = self.tree.xpath(_PUBLICATION_ENTRY_XPATH) # it should contain only one node
        else: # author page
            nodes = self.tree.xpath(_AUTHOR_ENTRY_XPATH)
            # if there are several pages, we need to download and parse them all
            # extract information about number of records
            pages = self.tree.xpath("|".join(item + _XPATH_SUFFIX for item in _AUTHOR_PAGES_XPATH))
            if len(pages) == 3:
                total_records = extract_number(pages[0], convert=True)
                first_record_on_page = extract_number(pages[1], convert=True)
                last_record_on_page = extract_number(pages[2], convert=True)
                if last_record_on_page < total_records and all([total_records, first_record_on_page, last_record_on_page]): # there is more than one page
                    records_on_page = last_record_on_page - first_record_on_page + 1
                    records_left = total_records - last_record_on_page # how many records are on subsequent pages
                    pages_left = (records_left - (records_left % records_on_page)) / records_on_page + (1 if records_left % records_on_page else 0)
                    try:
                        current_page_num = int(self.tree.xpath(_AUTHOR_CURRENT_PAGE_XPATH)[0]) # 1-based
                    except (ValueError, IndexError):
                        pass
                    else:
                        for i in range(pages_left): # the first page has already been parsed
                            # parse the page
                            # remove possible pagenum parameter from the url and add it with the current page
                            url = re.sub(r"&?pagenum=\d+", "", self.url) + "&pagenum=%d" % (current_page_num + i + 1)
                            try:
                                self.tree = self.load_url(url)
                            except URLLoadingError:
                                pass
                            else:
                                # add entries from the current page
                                nodes.extend(self.tree.xpath(_AUTHOR_ENTRY_XPATH))

        if not nodes:
            raise ElibraryLoadingError

        return nodes # list of entries

    def entry2fields(self, entry):
        # detect entry type (publication page / author page)
        self.detect_page_type(entry.base) # entry.base stores the initial url
        if not self.type: # type not detected
            raise ParsingEntryError

        self.original_data = self.entry2text(entry)
        self.tree = entry

        fields = {'url': entry.base}
        if self.type == "publication": # the entry has been parsed from a publication page
            self.parse_publication_page_entry(fields)
        else: # the entry has been parsed from an author page
            # parse data extracted from the author page
            self.parse_author_page_entry(fields)
            # download and parse additional data from a publication page
            if fields.get('publication_url'): # publication page url has been extracted
                # add domain
                fields['publication_url'] = 'http://elibrary.ru' + fields['publication_url']
                # download publication page for additional data
                try:
                    publication_tree = self.load_url(fields['publication_url'])
                except URLLoadingError:
                    pass
                else:
                    # parse additional data from the publication page and update not extracted fields
                    self.parse_publication_page_entry(fields, tree=publication_tree)
            # download and parse additional data from a collection page
            if fields['work_type'] == 'collection' and fields.get('collection_url'):
                # add domain
                fields['collection_url'] = 'http://elibrary.ru' + fields['collection_url']
                # download collection page for additional data
                try:
                    collection_tree = self.load_url(fields['collection_url'])
                except URLLoadingError:
                    pass
                else:
                    # parse additional data from the collection page and update not extracted fields
                    self.parse_publication_page_entry(fields, tree=collection_tree)

        if 'author' in fields.keys():
            fields['authors'] = ', '.join(fields['author']) if isinstance(fields['author'], list) else fields['author']
        else:
            fields['authors'] = ''
        if not fields.get('title'):
            fields['title'] = ''
        if fields.get('volume'):
            fields['volume'] = extract_number(fields['volume'])
        if fields.get('number'):
            fields['number'] = fields['number'].strip(u"№ ")
        if not fields.get('year'):
            fields['year'] = fields.get('year-proc')
        if not fields.get('pages'):
            fields['pages'] = fields.get('pages-proc')
        if fields['pages']:
            fields['pages'] = fields['pages'].replace("-", "--")
        return fields

    def parse_publication_page_entry(self, fields, tree=None):
        '''Parses publication page (a tree) and fills in the fields dictionary.'''
        if not tree:
            tree = self.tree
        work_type = self.extract_field('type', tree, page_type="publication")
        if work_type == u'публикация в сборнике трудов конференции':
            if not fields.get('work_type'):
                fields['work_type'] = 'collection'
            field_names = ['author', 'title', 'publisher', 'location', 'year-proc', 'pages-proc', 'note']
        elif work_type == u'сборник трудов конференции':
            if not fields.get('work_type'):
                fields['work_type'] = 'collection'
            field_names = ['collection', 'publisher', 'location', 'year-proc', 'series']
        else: # otherwise we consider this a journal article
            if not fields.get('work_type'):
                fields['work_type'] = 'journal'
            field_names = ['author', 'title', 'journal', 'volume', 'number', 'publisher', 'year', 'pages', 'note']
        for field in field_names:
            if not fields.get(field): # the field is not already extracted
                fields[field] = self.extract_field(field, tree, page_type="publication")

    def parse_author_page_entry(self, fields):
        '''Parses a single entry from author page and fills in fields dictionary.'''
        if self.tree.xpath(_AUTHOR_JOURNAL_XPATH): # this is a journal article
            fields['work_type'] = 'journal'
            field_names = ['author', 'title', 'collection', 'journal', 'volume', 'number', 'year', 'pages', 'publication_url']
        else: # this is an article in a collection
            fields['work_type'] = 'collection'
            field_names = ['author', 'title', 'publication_url', 'collection_url']
        for field in field_names:
            fields[field] = self.extract_field(field, page_type="author").replace("\r\n", "")

    def fields2bibtex(self, fields):
        template = _TEMPLATES[fields['work_type']]
        return template % fields

    def load_url(self, url):
        '''Loads url and transforms it into an ElementTree, returning the tree.'''
        try:
            webpage = self.opener.open(url, timeout=10)
            tree = lxml.etree.parse(webpage, lxml.etree.HTMLParser(encoding='utf-8'))
        except (urllib2.URLError, urllib2.HTTPError, socket.timeout):
            logger.warning("Error when loading url %s: %s", url, sys.exc_info())
            messages.warning(self.request, u'Ошибка при загрузке страницы <a href="%s">%s</a>. Данные могут быть загружены не полностью. Попробуйте еще раз или введите информацию вручную.' % (url, url))
            raise URLLoadingError
        except:
            logger.warning("Unexpected error when loading url %s: %s", url, sys.exc_info())
            messages.warning(self.request, u'Ошибка при загрузке страницы <a href="%s">%s</a>. Данные могут быть загружены не полностью. Попробуйте еще раз или введите информацию вручную.' % (url, url))
            raise URLLoadingError
        return tree

    def detect_page_type(self, url):
        '''Detects page type by the url (publication page / author page).'''
        if getattr(self, "type", None): # page type already detected
            return
        if "author_items" in url or "authorid" in url: # author page
            self.type = "author"
        elif "item.asp" in url: # publication page
            self.type = "publication"
        else:
            self.type = None

    def extract_field(self, field, tree=None, page_type=None):
        '''Extracts field value from the tree.'''
        if tree is None:
            tree = self.tree
        if not page_type:
            page_type = self.page_type
        value = u""
        extract_all_text = True # extract all text from the matched nodes, this is the default behaviour
        matches = tree.xpath(self.build_xpath(field, page_type))
        if not matches:
            if page_type == "author" and field in _AUTHOR_FIELD_REGEXP.keys():
                # if this is a cited entry (#499), the number is usually raw, not in a tag. We can parse it via regular expression.
                extract_all_text = False # don't extract all text from the matched nodes, because xpath is too unrestricted
                matches = tree.xpath(self.build_xpath("default", page_type))
                for text in reversed(matches):
                    number = re.search(_AUTHOR_FIELD_REGEXP[field], text)
                    if number:
                        value = number.group(1)
                        if value:
                            break
        if not value:
            if page_type == "publication" and field == 'author':
                value = [text for text in matches if text]
            elif page_type == "author":
                if field == "pages":
                    extract_all_text = False # don't extract all text from the matched nodes, because xpath is too unrestricted
                    for text in reversed(matches):
                        pages = re.search(u'С[ \t\r\n.]*([1-9][0-9]*(-[1-9][0-9]*)?)', text)
                        if pages:
                            value = pages.group(1)
                            if value:
                                break
                elif field in ["publication_url", "collection_url"] and isinstance(matches, list) and len(matches):
                    # url of the page with additional data has been extracted
                    url_node = matches[0]
                    value = dict(url_node.items())['href'] # extracted url
        if not value and extract_all_text:
            value = "".join(text for text in matches if text)

        # convert non-breaking space to a plain one
        if value:
            conv = lambda x: x.replace(u"\xa0", u" ")
            if isinstance(value, basestring):
                value = conv(value)
            elif isinstance(value, list) and all(isinstance(x, basestring) for x in value):
                value = map(conv, value)

        return value

    def build_xpath(self, field, page_type=None):
        '''Returns xpath expression for a given field.'''
        if not page_type:
            page_type = self.type
        if page_type == "publication":
            if field in _PUBLICATION_FIELD_XPATH_OTHER.keys(): # specific patterns
                xpath = _PUBLICATION_FIELD_XPATH_OTHER[field] % _PUBLICATION_FIELD_NAMES.get(field) + _XPATH_SUFFIX # add /text() common suffix
            else:
                main = _PUBLICATION_FIELD_XPATH_MAIN % _PUBLICATION_FIELD_NAMES.get(field)
                suffix = _PUBLICATION_FIELD_XPATH_SUFFIXES.get(field, _PUBLICATION_FIELD_XPATH_SUFFIXES['default'])
                if isinstance(suffix, tuple):
                    xpath = "|".join(main + suffix_item + _XPATH_SUFFIX for suffix_item in suffix)
                else:
                    xpath = main + suffix + _XPATH_SUFFIX
        else: # author page
            main = _AUTHOR_FIELD_XPATH.get(field)
            if isinstance(main, tuple):
                xpath = "|".join(main_item + _XPATH_SUFFIX for main_item in main)
            else:
                xpath = main
                if field not in _AUTHOR_FIELDS_WITHOUT_SUFFIX:
                    xpath += _XPATH_SUFFIX
        return xpath

    def pickle_entries(self, entries):
        '''Pickles list of entries for storing in a database.'''
        return smart_pickle([self.pickle_entry(entry, pickle=False) for entry in entries])

    def pickle_entry(self, entry, pickle=True):
        '''Pickles an entry for storing in a database.'''
        xml = lxml.etree.tostring(entry, encoding='utf-8').rstrip("&#13;\n") # stripping is used because otherwise fromstring could raise an error
        # we need to store additional data, e.g. entry.base
        data = {'xml': xml, 'url': entry.base}
        if pickle:
            data = smart_pickle(data)
        return data

    def unpickle_entry(self, data, unpickle=True):
        '''Unpickles an entry stored in a database for parsing.'''
        if unpickle:
            data = smart_unpickle(data)
        entry = lxml.etree.fromstring(data['xml'])
        entry.base = data['url']
        return entry

    def entry2text(self, entry):
        '''Converts an entry from split to text representation.'''
        alphanum = re.compile(r'.*\w.*', re.DOTALL | re.UNICODE)
        text = "\n".join(text_piece.replace("\n", "").replace("\r", "").lstrip(". ") for text_piece in entry.itertext() if alphanum.match(text_piece))
        text += "\n" + entry.base # original url
        return text

    def show_data(self, entry):
        '''Show entry2text, since original data is merely a url.'''
        return self.entry2text(entry)
