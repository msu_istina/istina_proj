# -*- coding: utf-8 -*-
from django.conf import settings
from lxml import etree
from os.path import join
import logging

from publications.parsers.base_parser import PreprocessDataError
from publications.parsers.simplexml import SimpleXMLParser

logger = logging.getLogger("publications.parsers.mods")


class ModsParser(SimpleXMLParser):

    name = "mods"
    empty_xml = '<?xml version="1.0" encoding="UTF-8"?><modsCollection xmlns="http://www.loc.gov/mods/v3"></modsCollection>"'

    def data2entries(self, data):
        '''Get initial data entered by user and transform it into a list of entries for future parsing.
        '''
        # convert mods xml to simplexml
        # check if the xml is empty, this is probably was not an mods xml
        if data in [self.empty_xml, u'\xef\xbb\xbf' + self.empty_xml] :
            raise PreprocessDataError
        # transform the mods xml string to simple xml tree
        tree = self.mods2simplexml(data)
        # now we have a simple xml, use SimpleXmlParser
        return super(ModsParser, self).data2entries(tree=tree)

    def mods2simplexml(self, mods_xmlstring):
        '''Convert xml string in mods format into simplexml xml object.'''
        try:
            mods_xmlstring = mods_xmlstring.encode('utf8') # if data has been entered by user in mods, it's in unicode. Should be encoded in utf8.
        except UnicodeDecodeError:
            pass
        try:
            tree = etree.fromstring(mods_xmlstring)
        except etree.XMLSyntaxError:
            raise PreprocessDataError
        f_xslt = open(join(settings.SETTINGS_DIR, '..', 'env', 'bin', 'mods2sxml.xsl'))
        xslt_data = etree.parse(f_xslt)
        f_xslt.close()
        transform = etree.XSLT(xslt_data)
        return transform(tree).getroot()
