# -*- coding: utf-8 -*-
from subprocess import Popen, PIPE
import logging
import re

from publications.parsers.base_parser import PreprocessDataError
from publications.parsers.mods import ModsParser

logger = logging.getLogger("publications.parsers.webofscience")


class WebofScienceParser(ModsParser):

    name = "webofscience"

    def data2entries(self, data):
        '''Get initial data entered by user and transform it into a list of entries for future parsing.
        '''
        # check if this is ISI Web of Science format
        conditions = [
            data.startswith("FN Thomson Reuters Web of Knowledge"),
            re.match(ur'^\s*(FN|PT)\s.*\s(EF|ER)\s*$', data, re.DOTALL)
        ]
        if not any(conditions):
            raise PreprocessDataError
        # transform webofscience to intermediate mods xml format
        process = Popen(["isi2xml", "-i", "utf8"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        mods_xmlstring, status = process.communicate(input=data.encode('utf-8'))
        if status.endswith("isi2xml: Processed 0 references.\n"): # this was not a webofscience format
            raise PreprocessDataError
        # now we have a mods xml, use ModsParser
        return super(WebofScienceParser, self).data2entries(mods_xmlstring)
