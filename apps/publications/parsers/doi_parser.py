# -*- coding: utf-8 -*-
from publications.parsers.bibtex_mods import BibtexModsParser
from common.utils.doi import doi_to_bibtex, DoiServiceError, TryDoiError
from publications.parsers.base_parser import PreprocessDataError
from django.contrib import messages


class DoiParser(BibtexModsParser):

    name = "doi"

    def data2entries(self,data):

        list = []
        error_message = ""
        not_doi = False
        for line in data.split('\r\n\r\n'):
            try:
                list.append(doi_to_bibtex(line))
            except (DoiServiceError,TryDoiError), e:
                error_message = e

            except:
                not_doi = True
        if not list:
            if not_doi:
                raise PreprocessDataError
            else:
                raise DoiServiceError(error_message)
        else:
            bibtex = '\r\n\r\n'.join(list)
        return super(DoiParser, self).data2entries(bibtex)
