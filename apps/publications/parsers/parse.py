# -*- coding: utf-8 -*-
import logging

from publications.parsers.base_parser import ParsingEntryError, PreprocessDataError
from publications.parsers.bibtex_mods import BibtexModsParser
from publications.parsers.dummy import DummyParser
from publications.parsers.elibrary import ElibraryParser, ElibraryLoadingError
from publications.parsers.freecite import FreeciteParser
from publications.parsers.mods import ModsParser
from publications.parsers.pubmed import PubMedParser
from publications.parsers.pybtex_parser import PybtexParser
from publications.parsers.simplexml import SimpleXMLParser
from publications.parsers.webofscience import WebofScienceParser
from publications.parsers.doi_parser import DoiParser

logger = logging.getLogger("publications.parsers.parse")


# set the order in which parsers are applied
ParserClasses = [ElibraryParser, BibtexModsParser, WebofScienceParser, PubMedParser, ModsParser, SimpleXMLParser, PybtexParser, DoiParser,FreeciteParser, DummyParser]


def apply_parsers(data_or_parsingevent, request, context):
    '''Applies all available parsers in order.
    As soon as one of the parsers succeeds, returns parsed fields and the name of the parser.
    data_or_parsingevent can be some user entered data, or a parsing event from queue
    '''

    # ensure that the dummy default parser, which 'never' fails, is among the parsers. Reason: some parser should work successfully
    if DummyParser not in ParserClasses:
        ParserClasses.append(DummyParser)

    # try to parse data using available parsers
    for ParserClass in ParserClasses:
        parser = ParserClass(request, context)
        try:
            return parser.parse(data_or_parsingevent)
        except PreprocessDataError:
            pass # try other parsers
        except ElibraryLoadingError:
            # data is a elibrary url, but loading of pages failed. Do not try freecite parser (it often terminates at such input), just use the dummy one.
            parser = DummyParser(request, context)
            return parser.parse(data_or_parsingevent)
        except ParsingEntryError as exception:
            # data has been successfully preprocessed (split into entries, put in queue),
            # and now we try to parse the returned parsing event with another parser
            data_or_parsingevent = exception.args[0]
    return None


def create_parser(name):
    '''Create a parser object by name.'''
    for ParserClass in ParserClasses:
        if ParserClass.name == name:
            return ParserClass()
    return DummyParser()
