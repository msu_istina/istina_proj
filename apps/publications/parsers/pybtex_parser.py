# -*- coding: utf-8 -*-
from pybtex.database.input import bibtex as pybtex
import StringIO
import logging
import re

from publications.parsers.base_parser import BaseParser, PreprocessDataError
from publications.utils import pybtex_entry2bibtex

logger = logging.getLogger("publications.parsers.pybtex")


class PybtexParser(BaseParser):

    name = "pybtex"

    def __init__(self, *args, **kwargs):
        super(PybtexParser, self).__init__(*args, **kwargs)
        self.parser = pybtex.Parser()

    def data2entries(self, data):
        '''Get initial data entered by user and transform it into a list of entries for future parsing.
        '''
        data_fixed = self.fix_bibtex(data) # fix possible errors in bibtex
        data_str = StringIO.StringIO(data_fixed)
        try:
            bibtex_object = self.parser.parse_stream(data_str)
        except: # parsing was unsuccessful, this is probably not a bibtex or a not well-formed bibtex
            logger.debug("Bibtex parsing failed.")
            raise PreprocessDataError
        entries = bibtex_object.entries.values()
        if not entries:
            raise PreprocessDataError
        return entries

    def entry2fields(self, entry):
        '''Parse bibtex entry to fields. Note that the entry is not a string, but one of the objects returned by the preprocess_data function
        '''
        fields = {}
        # mapping from (inner temporary format used to pass information to the step2 form) to standard bibtex format
        field_names = ["title", "journal", ("collection", "booktitle"), ("editors", "editor"), "volume", "publisher", "series", "institution", ("location", "address"), "number", "year", "firstpage", "lastpage", "pages", "isbn", "doi", "note"] # FIXME add 'annote' field to note
        for name in field_names:
            if isinstance(name, tuple):
                inner_name = name[0]
                bibtex_name = name[1]
            else: # this is a string, inner and outer name coincide
                inner_name = name
                bibtex_name = name
            try:
                fields[inner_name] = entry.fields[bibtex_name]
            except (IndexError, AttributeError, KeyError):
                fields[inner_name] = ""

        # work_type smart detection
        if entry.type == 'article':
            fields['work_type'] = 'journal'
        elif entry.type in ['conference', 'inbook', 'incollection', 'inproceedings', 'proceedings']:
            fields['work_type'] = 'collection'
            # smart detection of theses
            if u"тезис" in fields["collection"].lower() or "theses" in fields["collection"].lower() or "thesis" in fields["collection"].lower():
                fields['theses'] = True
        elif entry.type in ['book', 'booklet', 'manual', 'mastersthesis', 'phdthesis', 'techreport']:
            fields['work_type'] = 'book'
        else:
            fields['work_type'] = 'journal'

        # authors smart parsing
        fields['authors'] = ", ".join(entry.fields['author'].replace(",", "").split(" and "))

        # remove and replace special characters
        try:
            fields = dict((key, re.sub(r'[\{\}]', '', unicode(value).strip('"\'').replace('~', ' '))) for key, value in fields.items())
        except:
            pass
        return fields

    def entry2text(self, entry):
        '''Converts an entry from split to text representation.'''
        return pybtex_entry2bibtex(entry)

    def fix_bibtex(self, data):
        '''Fix possible errors in input bibtex, e.g. spaces in field names (see #726).'''
        # replace all spaces with dashes in first parts of strings that contain only one '='. First part means the part before the '=' and surrounding whitespaces.
        data_fixed = '\n'.join((line.split('=')[0].strip().replace(' ', '-') + ' =' + line.split('=')[1]) if line.count('=') == 1 else line for line in data.splitlines())
        return data_fixed.replace('Web of Science-Category = {{', 'Web-of-Science-Category = {{')
