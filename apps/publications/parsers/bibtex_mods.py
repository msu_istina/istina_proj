# -*- coding: utf-8 -*-
from subprocess import Popen, PIPE
import logging

from publications.parsers.mods import ModsParser
from publications.parsers.base_parser import PreprocessDataError

logger = logging.getLogger("publications.parsers.bibtex_mods")


class BibtexModsParser(ModsParser):

    name = "bibtex_mods"

    def data2entries(self, data):
        '''Get initial data entered by user and transform it into a list of entries for future parsing.
        '''
        # first transform bibtex to intermediate mods xml format
        process = Popen(["bib2xml", "-i", "utf8"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        mods_xmlstring, status = process.communicate(input=data.encode('utf-8'))
        if status.endswith("bib2xml: Processed 0 references.\n"): # this was not a bibtex
            raise PreprocessDataError
        # now we have a mods xml, use ModsParser
        return super(BibtexModsParser, self).data2entries(mods_xmlstring)
