


             PT J

             
AU Dudko, LV
et al.
AF Dudko, L. V.
et al.
CA D0 Collaboration
TI Measurement of the W Boson Mass with the D0 Detector
SO PHYSICAL REVIEW LETTERS
AB We present a measurement of the W boson mass using data corresponding
to 4: 3 fb(-1) of integrated luminosity collected with the D0 detector
during Run II at the Fermilab Tevatron p (p) over tilde collider. With a
sample of 1 677 394 W -> e nu candidate events, we measure M-W = 80.367
+/- 0.026 GeV. This result is combined with an earlier D0 result
determined using an independent Run II data sample, corresponding to 1
fb(-1) of integrated luminosity, to yield MW 80.375 +/- 0.023 GeV.
SN 0031-9007
PD APR 12
PY 2012
VL 108
IS 15
AR 151804
DI 10.1103/PhysRevLett.108.151804
UT WOS:000302703600002

ER                                   


