# -*- coding: utf-8; -*-
from common.forms import LinkedToWorkersModelForm
from diplomas.models import Diploma
from django import forms

class DiplomaForm(LinkedToWorkersModelForm):
    '''A simple form for diplomas.'''
    fields_order = ["diptype", "organization", "outorganization", "advisers_str", "author", "title", "year", "abstract"]
    css_classes = [('author', 'wide autocomplete_workers')]
    organization_index = 1

    def __init__(self, *args, **kwargs):
        super(DiplomaForm, self).__init__(*args, **kwargs)
        self.fields['outorganization'].label = "|--"
        self.fields['organization'].empty_label = u'---- Другая организация ----'

    class Meta:
        model = Diploma
        exclude = ('advisers', 'creator', 'xml')
        widgets = {
            'outorganization': forms.TextInput(
                attrs={'placeholder': u'Пожалуйста, укажите название организации, если она отсутствует в приведенном выше списке',
                       'class': 'wide'}),
        }
