from common.utils.admin import register_with_versions
from models import Diploma, DiplomaAdvisement

register_with_versions(Diploma)
register_with_versions(DiplomaAdvisement)
