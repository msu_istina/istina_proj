# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from common.models import MyModel
from common.models import LinkedToWorkersModel, WorkershipModel, Activity, AuthorScience
from common.utils.validators import validate_year


class DiplomaType(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DIPLOMTYPE_ID")
    name = models.CharField(u"Название типа дипломной работы", max_length=255, db_column="F_DIPLOMTYPE_NAME")
    code = models.CharField(u"Кодовое название типа дипломной работы", max_length=255, db_column="F_DIPLOMTYPE_CODE")

    class Meta:
        db_table = "DIPLOMTYPE"
        verbose_name = u"Тип дипломной работы"
        verbose_name_plural = u"Типы дипломных работ"

    def __unicode__(self):
        return self.name


class Diploma(LinkedToWorkersModel, Activity):
    '''A diploma that has been defended under a supervision of a worker.'''
    id = models.AutoField(primary_key=True, db_column="F_DIPLOM_ID")
    title = models.CharField(u"Название дипломной работы", max_length=255, db_column="F_DIPLOM_NAME")
    diptype = models.ForeignKey(to=DiplomaType, related_name="diplomas", db_column="F_DIPLOMTYPE_ID", verbose_name=u"Тип", null=False, blank=False)
    author = models.CharField(u"Автор", max_length=255, db_column="F_DIPLOM_AUTHOR")
    advisers = models.ManyToManyField("workers.Worker", related_name="diplomas", through="DiplomaAdvisement")
    year = models.IntegerField(u"Год защиты", db_column="F_DIPLOM_YEAR", validators=[validate_year], null=True, blank=True)
    organization = models.ForeignKey(to="organizations.Organization", related_name="diplomas", db_column="F_ORGANIZATION_ID", verbose_name=u"Организация, где проходила защита", blank=True, null=True)
    outorganization = models.CharField(u"Название организации", max_length=255, db_column="F_DIPLOM_OUTORGANIZATION", blank=True)
    abstract = models.TextField(u"Аннотация", db_column="F_DIPLOM_ABSTRACT", blank=True)
    xml = models.TextField(u"XML диплома", db_column="F_DIPLOM_XML", blank=True)
    creator = models.ForeignKey(to=User, related_name="diplomas_added", db_column="F_DIPLOM_USER", null=True, blank=True)

    workers_attr = 'advisers'
    workerships_attr = 'advisements'
    workers_verbose_name_single = u"Научный руководитель"
    workers_verbose_name_plural = u"Научные руководители"
    workers_required_error_msg = u"Укажите научного руководителя"

    nominative_short = u"диплом"
    nominative_en = "diploma"
    genitive = u"дипломной работы"
    genitive_short = u"диплома"
    genitive_plural = u"дипломные работы"
    genitive_plural_full = u"дипломных работ"
    accusative = u"дипломную работу"
    accusative_short = u"диплом"
    instrumental = u"дипломом"
    locative = u"дипломной работе"
    gender = 'feminine'

    class Meta:
        db_table = "DIPLOM"
        verbose_name = u"дипломная работа"
        verbose_name_plural = u"дипломные работы"
        get_latest_by = "year"
        ordering = ('-year', '-title')

    def __unicode__(self):
        return u"%s" % self.title

    @models.permalink
    def get_absolute_url(self):
        return ('diplomas_detail', (), {'object_id': self.id})

    @property
    def authorscience(self):
        authors = AuthorScience.objects.filter(Q(sciencework__id=self.id, flag_validate=1)|Q(sciencework__id=self.id,flag_validate__isnull=True))
        authorsdict = {}
        for author in authors:
            if author.authorrole.name not in authorsdict.keys():
                authorsdict[author.authorrole.name] = []
            authorsdict[author.authorrole.name].append(author)
        return authorsdict



class DiplomaAdvisement(WorkershipModel):
    id = models.AutoField(primary_key=True, db_column="F_DIPLOMHEAD_ID")
    adviser = models.ForeignKey(to="workers.Worker", related_name="diploma_advisements", db_column="F_MAN_ID", null=True, blank=True)
    diploma = models.ForeignKey(to="Diploma", related_name="advisements", db_column="F_DIPLOM_ID")
    original_name = models.CharField(max_length=255, db_column="F_DIPLOMHEAD_NAME", blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="diploma_advisements_added", db_column="F_DIPLOMHEAD_USER", null=True, blank=True)

    worker_attr = 'adviser'

    class Meta:
        db_table = "DIPLOMHEAD"
        verbose_name = u"руководство дипломной работой"
        verbose_name_plural = u"руководства дипломными работами"

    def __unicode__(self):
        return u"%s, руководитель диплома '%s'" % (self.workers_string_full, self.diploma.title)

from diplomas.admin import *
