# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from common.models import AuthorRole
from common.views import LinkedToWorkersModelWizardView as Wizard
from diplomas.models import Diploma
from diplomas.forms import DiplomaForm

DiplomaFormWizard = Wizard.create_wizard_from_form(DiplomaForm)

options_base = {'model': Diploma}
options_detail = dict(options_base.items() + [('template_name', 'diplomas/detail.html'),
    ('extra_context',{'roles': AuthorRole.objects.filter(authorroleobj__code="diploma")})])

urlpatterns = patterns('common.views',
    url(r'^add/$', DiplomaFormWizard.as_view(), name='diplomas_add'),
    url(r'^(?P<object_id>\d+)/$', 'detail', options_detail, name='diplomas_detail'),
    url(r'^(?P<object_id>\d+)/edit/$', DiplomaFormWizard.as_view(), name='diplomas_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', options_base, name='diplomas_delete'),
)
