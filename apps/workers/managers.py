# -*- coding: utf-8 -*-
from collections import defaultdict, namedtuple
from pytils.translit import detranslify
import editdist
import editdististina
import json
import logging
import operator
import requests
import settings

from django.contrib.auth.models import User
from django.core.cache import get_cache
from django.db import models
from django.db.models import Q

from common.managers import MyManager
from common.utils.idifflib import get_close_matches
from common.utils.cutoff import CutoffTimer
from common.utils.uniqify import uniqify
from common.utils.user import get_worker
from common.utils.databases import oracle_engine_string, current_database_engine
from workers.utils import get_all_fullnames, merge_users, merge_workers
from django.db.models import Q
import re
import itertools
class WorkerManager(MyManager):
    '''Model manager for Worker model.'''
    ALIASES_MANAGER_NAME = "objects"

    @property
    def aliases_manager(self):
        from models import WorkerAlias
        return getattr(WorkerAlias, self.ALIASES_MANAGER_NAME)

    @staticmethod
    def get_cached_worker_list(request_cache=None):
        """Get worker list using mid-term and short-term caches

        request_cache
            request or any other other object for cache

        returns list of wcache.WorkerSimplified
        """
        if request_cache is not None and hasattr(request_cache, "workers_cache"):
            return request_cache.workers_cache

        from workers.wcache import get_worker_list, WorkerSimplified
        locmemcache = get_cache("bigdata")
        cache_key = "workers_list_full_tmp"
        data = locmemcache.get(cache_key)
        if data is None:
            logging.getLogger("workers.managers").info("Locmem '%s' is empty", cache_key)
            try:
                data = get_worker_list()
                if not data:
                    raise Exception("Worker list cache is empty!")
            except:
                # cache failed or empty - try to read data from db
                logging.getLogger("workers.managers").error("Failed to read worker list from cache", exc_info=True)
                from workers.models import ProxyWorker
                from workers.wcache import simplify_proxy_worker
                data = ProxyWorker.get_all_proxy_workers()
                data = map(simplify_proxy_worker, data)
            locmemcache.set(cache_key, tuple(map(tuple, data)), timeout=12)
        else:
            data = [WorkerSimplified(*i) for i in data]
        if request_cache is not None:
            request_cache.workers_cache = data
        return data

    def get_disambiguation(self, names, current_worker=None, count=20, unrelated=False):
        try:
            service = settings.DISAMBIGUATION
            to_dict = lambda n: dict(zip(['last_name', 'first_name', 'middle_name'], n))
            data = { 'authors': map(to_dict, names) }
            if current_worker: data['man_id'] = current_worker.id
            total_authors = len(names)
            r=requests.post(service['url'], data=json.dumps(data), timeout=service['timeout'])
            if r.status_code == 200:
                from utils import get_fullname_raw
                r.encoding='utf-8'
                result = json.loads(r.text)
                ids = [i['id'] for authors in result['authors'] for i in authors[:count]] + result['result_id']
                workers = self.in_bulk(ids)
                get_name = lambda a: get_fullname_raw(a['last_name'], a['first_name'] if a['first_name'] != '.' else None, a['middle_name'] if a['middle_name'] != '.' else None)
                def step(authors, selected_id):
                    r = [(w, get_name(a), a['name_similarity']*0.95)
                    for a in authors[:count] if int(a['id']) in workers for w in [workers[int(a['id'])]]]
                    if total_authors > 1 and int(selected_id) in workers and not unrelated:
                        r += [(workers[int(selected_id)], workers[int(selected_id)].fullname, 0.95)]
                    return r
                return [ step(authors, selected_id) for authors, selected_id in zip(result['authors'], result['result_id'])]
        except Exception as e:
            logging.getLogger("workers.managers").error("Error while calling disambiguation-authors service with names: %s", names, exc_info=True)
            return []

    def get_similar(self, unknown_man, request_cache=[], simple_mode=False, current_worker=None):
        """Get list of similar workers with score and representative name

        Uses two metrics - get_difflib_similar and self.get_very_similar

        unknown_man
            dict with keys firstname, middlename, lastname
        request_cache
            request or any other other object for cache
        simple_mode
            if True, simple mode of similar workers calculation is used:
            only exact matches of fullnames and alias fullnames are searched for.

        returns
            list of tuples(Worker object, fullname, similarity score)
            note, that fullname here isn't to be equal to Worker.fullname,
            as it maybe acquired from worker's alias
        """
        if not request_cache:
            logging.getLogger("workers.managers").warning("Get_similar for %s without disambiguation-authors service")
        workers = self.find_exact(simple_mode=True, **unknown_man)

        from workers.models import WorkerAlias, ProxyWorker
        proxy_workers = map(lambda a: ProxyWorker(a.worker, "ID", a.lastname, a.firstname, a.middlename),  WorkerAlias.objects.filter(worker__in=workers))
        
        return (request_cache or []) + self._get_similar(workers + proxy_workers, unknown_man, simple_mode=simple_mode)


    def _get_similar(self, proxy_workers, unknown_man, simple_mode=False, lower_proxy=True):
        """This function do the main work of get_similar

        proxy workerks
            list of ProxyWorker or cache.WorkerSimplified
        unknown_man
            dict with keys firstname, middlename, lastname
        simple_mode
            if True, simple mode of similar workers calculation is used:
            only exact matches of fullnames and alias fullnames are searched for.

        returns
            list of tuples(proxy worker, fullname, similarity score)
            note, that fullname here isn't to be equal to Worker.fullname,
            as it maybe acquired from worker's alias
        """
        timer = CutoffTimer(None)
        start_time = timer.cutoff("start")
        kwargs = dict(unknown_man=unknown_man, proxy_workers=proxy_workers, lower_proxy=lower_proxy)
        difflib_similar = self.get_difflib_similar(**kwargs) if not simple_mode else []
        timer.cutoff("difflib_similar")
        very_similar = self.get_very_similar(**kwargs)
        timer.cutoff("very_similar")
        timer.cutoff("total", start_time)
        logging.getLogger("benchmark.worker_manager.inner").info("Timer: %s", timer)
        return difflib_similar + very_similar

    def _get_similar_dumb(self, proxy_workers, unknown_man):
        """Temporary function. Returns pworkers with same lastnames as unknown man, thus there is no any simialrity score."""
        return [
            (i, i.fullname, -1) # similarity score equals -1
            for i in proxy_workers
            if i.lastname.strip() == unknown_man["lastname"].strip()
        ]

    def get_difflib_similar(self, unknown_man, proxy_workers, lower_proxy=True):
        """Return pworkers with similar (in terms of difflib) fullname"""
        if lower_proxy:
            prepare_lastname = lambda x: x.lower().encode("utf8")
        else:
            prepare_lastname = lambda x: x.encode("utf-8")
        unknown_man_lastname_encoded = unknown_man["lastname"].lower().encode("utf8")
        # Distance in 1 cyrillic symbol equals to distance 2 in bytes
        is_similar = lambda x: editdististina.distance_threshold(prepare_lastname(x.lastname), unknown_man_lastname_encoded, 2)

        proxy_workers = [x for x in proxy_workers if is_similar(x)]

        from utils import get_fullname_raw
        prepare_fullname = lambda x: ("".join(i for i in x if i.isalnum() or i.isspace() or i == ".")).lower()
        fullname = prepare_fullname(get_fullname_raw(**unknown_man))
        all_fullnames = []
        for w in proxy_workers:
            all_fullnames.append((w.id, w.fullname))
            all_fullnames.append((w.id, w.fullname_initials))
        cutoff = .6
        similar_fullnames_scored = get_close_matches(fullname, all_fullnames, cutoff=cutoff, n=5, key=lambda x: prepare_fullname(x[1]))
        similar_fullnames_scored = dict((i[0][0], i[1]) for i in similar_fullnames_scored)
        # map score in [.2; .5]
        normalize_score = lambda score: (score - cutoff) / (1. - cutoff) * (.5 - .2) + .2
        return [
            (i, i.fullname, normalize_score(similar_fullnames_scored[i.id]))
            for i in proxy_workers
            if i.id in similar_fullnames_scored
        ]

    def get_very_similar(self, unknown_man, proxy_workers, lower_proxy=True):
        """Return similarity type 2 results

        i.e. pworkers
            - with exactly same names (after stripping dots)
            - with swapped lastname and firstname, i.e Petrov Anry Ivanovich and Anry Petrov Ivanovich
            - with fullname which can be produced from unknown person fullname
                   by word addition only or by substraction only
                   i.e 'Pertov Anry' and 'Petrov Anry Koko' but not 'Pertov Zavie Anry'
            - with fullname initials which can be produced from unknown person fullname initials
                   by word addition only or by substraction only
                   i.e 'Pertov Andrey Zavie' and 'Petrov Anry'

        """
        from utils import get_fullname_raw
        unknown_man = dict((k, v.lower()) for k, v in unknown_man.iteritems())
        fullname = get_fullname_raw(**unknown_man)
        fullname_splitted = fullname.split()
        fullname_initials_splitted = get_fullname_raw(initials=True, **unknown_man).replace(".", " ").split()

        def rate_similarity(man):
            man_lastname = man.lastname
            man_firstname = man.firstname
            man_initials = man.fullname_initials
            man_fullname = man.fullname
            if lower_proxy:
                man_lastname = man_lastname.lower()
                man_firstname = man_firstname.lower()
                man_initials = man_initials.lower()
                man_fullname = man_fullname.lower()
            if man_lastname == unknown_man['firstname'] and man_firstname == unknown_man['lastname']:
                return .75
            if man_lastname == unknown_man['middlename'] and man_firstname == unknown_man['lastname']:
                return .95
            if man_lastname != unknown_man["lastname"]:
                return 0
            if fullname == man_fullname:
                return .9
            if fullname == man_initials:
                return .85
            if self.addsub_check(fullname_splitted, man_fullname.split()):
                return .8
            if self.addsub_check(fullname_initials_splitted, man_initials.replace(".", " ").split()):
                return .7
            return 0
        rated_workers = ((worker, rate_similarity(worker)) for worker in proxy_workers)
        return [(worker, worker.fullname, score)
                for worker, score in rated_workers
                if score > 0]

    @staticmethod
    def addsub_check(list1, list2):
        """Check if list1 can be transformed into list2 by means of addsub

        i.e. by addition only or by substraction only

        >>> WorkerManager.addsub_check(*map(lambda i: i.split(), ("Petrov", "Petrov V. A.")))
        True
        >>> WorkerManager.addsub_check(*map(lambda i: i.split(), ("Petrov V. B.", "Petrov V. A.")))
        False
        """
        if len(list1) < len(list2):
            list1, list2 = list2, list1

        # assert: len(list1) >= len(list2):
        # seeking for list2[i2] in list1[i1:]
        i1 = i2 = 0
        while i1 < len(list1) and i2 < len(list2):
            if list1[i1] == list2[i2]:
                i2 += 1
            i1 += 1

        return i2 == len(list2)

    def get_similar_smart(self, firstname, middlename, lastname, count=10, extra_workers=[], current_worker=None, request_cache=None, simple_mode=False):
        """Find similar workers by means of both internal and external evidences

        Iternanal evidences are the person name and worker database
        External evidences are current logged in user info and and extra (forced) worker list
        See also https://bitbucket.org/goldan/istina/wiki/SimilarNameSuggest

        firstname, middlename, lastname
            properties of target (unknown) person we'd like to match agains over db
        count
            number of items to return
        extra_workers
            list of workers that are forcefully added to the list of similar workers
            (used to display the worker that is currently linked to profile, see #436).
        current_worker
            Worker object for current logged in user or None
            This user has some priority is the worker ambiguity resolution form
        request_cache
            request or any other other object for cache
        simple_mode
            if True, simple mode of similar workers calculation is used:
            only exact matches of fullnames and alias fullnames are searched for.

        Returns
            tuple(similarity_type, list of similar workers)
            similar worker is an instance of namedtuple "worker, representation_name, score"
        """
        logger = logging.getLogger("workers.get_similar_smart ")
        SimilarWorker = namedtuple("SimilarWorker", "worker, representation_name, score")
        unknown_man = dict(lastname=lastname, firstname=firstname, middlename=middlename)
        logger.info("lastname='%s', firstname='%s' middlename='%s'", lastname, firstname, middlename)
        logger.info("unknown_man: %r", unknown_man)

        if not lastname:
            return ("unknown", [])

        extra_workers = [SimilarWorker(worker, worker.fullname, 1.) for worker in extra_workers]
        total = extra_workers + [SimilarWorker(*worker) for worker in self.get_similar(unknown_man, request_cache=request_cache, simple_mode=simple_mode, current_worker=current_worker)]

        if not total:
            return ("new", [])

        if current_worker is not None:
            for i, w in enumerate(total[:]):
                if w[0].id == current_worker.id:
                    total[i] = SimilarWorker(w[0], w[1], w[2] + .1)

        # before uniqify we have to sort items by score in order to supply each candidate
        # with the best possible score
        total.sort(key=operator.attrgetter("score"), reverse=True)
        total = uniqify(total, lambda similar_worker: similar_worker.worker.id)[:count]
        # then find peloton, i.e. some total[0:top_items] with the top_score
        top_score = total[0].score
        top_items = 1
        while top_items < len(total) and total[top_items].score == top_score:
            top_items += 1
        if top_items == 1 and top_score > .5:
            total = total[:1] + sorted(total[1:], key=lambda a: (-a.score, a.representation_name))
            return ("first", total)

        total = sorted(total, key=lambda a: (-a.score, a.representation_name))
        return ("choice", total)

    def get_unemployed(self):
        '''Returns querylist of unemployed workers.'''
        from workers.models import Employment
        employed_workers_ids = Employment.objects.filter(enddate__isnull=True).values_list('worker', flat=True).distinct()
        return self.exclude(id__in=employed_workers_ids)

    def print_merge_candidates(self, linked_only=True):
        '''Prints a list of pairs of workers that are very similar to each other, and probably should be merged.
          If @linked_only is True, print only pairs that contain at least one worker linked to a user.
        '''
        name_index = defaultdict(list) # short fullname => [user.username/worker.id]
        detranslified_name_index = defaultdict(list) # short fullname (ru, detransified from en) => en fullname
        for worker in self.all():
            name_index[worker.fullname_short].append(worker.user if worker.user else worker)
            detranslified_name = detranslify(worker.fullname_short) # en -> ru
            if detranslified_name != worker.fullname_short: # the original name is english
                detranslified_name_index[detranslified_name].append(worker.fullname_short)

        def get_workers_by_indeces(names):
            workers = []
            for name in names:
                workers.extend(name_index[name])
                if name in detranslified_name_index:
                    workers.extend(get_workers_by_indeces(detranslified_name_index[name]))
            workers = set(workers)
            return workers
        for name, workers in name_index.items():
            similar_names = get_close_matches(name, (set(name_index.keys()) - set([name])) | set(detranslified_name_index.keys()), cutoff=0.8, n=5)
            similar_names = [similar_name for similar_name, score in similar_names if (name < similar_name) or similar_name in detranslified_name_index] # filter not to count them twice
            if similar_names or len(workers) > 1:
                # if linked_only is True, check that there is a user in index of these names
                workers = get_workers_by_indeces([name] + similar_names)
                if linked_only:
                    user_enlisted = False
                    for worker in workers:
                        if isinstance(worker, User):
                            user_enlisted = True
                    if not user_enlisted:
                        continue
                print ", ".join("%s(%s)" % (worker.get_profile().worker.fullname_short if isinstance(worker, User) else worker.fullname_short, worker.username if isinstance(worker, User) else worker.id) for worker in workers)

    def find(self, name, print_results=False):
        '''Return a list of workers with one of the names containing the given name.'''
        workers = self.filter(Q(firstname__icontains=name) | Q(middlename__icontains=name) | Q(lastname__icontains=name))
        if print_results:
            print "\n".join("%s, %s" % (worker, worker.id) for worker in workers)
        return workers

    def get_merge_candidates(self, user):
        '''Get Workers whose names or aliases are similar to current user

        Matches against current user's profile, binded worker and its aliases

        Uses the same metrics from WorkerManager.get_similar()
        '''
        from workers.wcache import unsimplify_list


        current_worker = get_worker(user)
        current_worker_fullnames = get_all_fullnames(current_worker)

        all_workers = self.get_cached_worker_list()
        similar_workers = []
        for fullname in current_worker_fullnames:
            similar_workers.extend(self.get_very_similar(fullname, all_workers))
        similar_proxy_workers = unsimplify_list([i[0] for i in similar_workers])

        workers = [proxy_worker.worker for proxy_worker in similar_proxy_workers]
        workers = uniqify(workers, idfun=lambda worker: worker.id)
        workers = [worker for worker in workers if not worker.get_profile()] # Only unbinded workers can be merged.
        if current_worker:
            workers = filter(lambda w: w != current_worker, workers)
        return workers

    def find_in_cached_list(self, string, exact_match=False):
        '''Return a list of workers with lastname containing string, search includes aliases and uses cached  worker
        list, see wcache.py  '''
        from workers.wcache import unsimplify_list

        all_workers = self.get_cached_worker_list()
        string = string.lower()
        if exact_match:
            search_method = lambda x: (string == x.lastname.lower())
        else:
            search_method = lambda x: string in x.lastname.lower()
        found_simplified_workers = filter(search_method, all_workers)
        found_proxy_workers = unsimplify_list(found_simplified_workers)
        workers = [proxy_worker.worker for proxy_worker in found_proxy_workers]
        workers = uniqify(workers, idfun=lambda worker: worker.id)

        return workers

    def delete_unlinked_objects(self, delete_condition=None):
        delete_condition = lambda x: getattr(x, 'has_no_links')
        return super(WorkerManager, self).delete_unlinked_objects(delete_condition=delete_condition)

    def add_names_cleansing_to_sql(self, sql):
        u'''Modifies sql query, so it filters all db columns, that are corresponded to fields in fields_to_clean of
        given models. It also filters all parameters passed by Django(%s in sql text). Removes from string everything except latin
        letters, cyrillic letters and percent symbol. Replaces ё to е. '''
        from workers.models import Worker, WorkerAlias
        models = (Worker, WorkerAlias)
        fields_to_clean = ('lastname', 'middlename', 'firstname')
        models_fields_database_columns = [{field.name: '"%s"."%s"' % (model._meta.db_table, field.db_column) for field in model._meta.fields}
                            for model in models]
        columns_to_clean = [model_fields[field_to_clean] for model_fields in models_fields_database_columns
                            for field_to_clean in fields_to_clean]
        columns_to_clean.append('%s')
        replace_regexps = [
            "REGEXP_REPLACE(%s, 'ё', 'е','1','0','i')",
            "REGEXP_REPLACE(%s, '[^а-я^А-Я^ё^Ё^a-z^A-Z^%%%%]', '','1','0','i')"
        ]
        replace_regexp = "%s"
        for replace in replace_regexps:
            replace_regexp = replace_regexp % replace

        condition_clause_start_index = sql.index('WHERE')
        sql_condition_clause = sql[condition_clause_start_index:]
        for column_to_clean in columns_to_clean:
            sql_condition_clause = sql_condition_clause.replace(column_to_clean, replace_regexp % column_to_clean)
        return sql[:condition_clause_start_index] + sql_condition_clause

    def get_workers_raw(self, filter_condition, match_type, select_related_fields=None, simple_mode=False):
        ''' Returns workers filtrated by dict with conditions filter_condition. Adds attribute match_type to each returned instance, so we can
        distinct exact or alias match later on. Performs raw sql generated from filter conditions. If database engine is not oracle,
        doesn't use filtration.
        '''
        queryset = self.filter(Q(**filter_condition)).distinct()
        if not simple_mode and current_database_engine() == oracle_engine_string:
            queryset_sql, params = queryset.query.sql_with_params()
            queryset_sql = self.add_names_cleansing_to_sql(queryset_sql)
            workers = self.raw(queryset_sql, params=params)
        else:
            workers = queryset
        if select_related_fields:
            workers_ids = [x.pk for x in workers]
            workers = self.filter(pk__in=workers_ids).select_related(*select_related_fields)
        map(lambda x: setattr(x, "match_type", match_type), workers)
        return workers

    def find_exact_fast(self, search_string):
        ''' Returns workers using search string. Searching string format is treated as 'Lastname Firstname Middlename'.
        User can specify only part of fullname. Results are ordered by profile existance.
        '''
        search_string = re.sub(ur'[^а-я^А-Я^ё^Ё^a-z^A-Z ]', '', search_string, re.UNICODE)
        search_term_parts = filter(None, search_string.split())[:3]
        if not search_term_parts:
            return self.none()
        return self.find_exact(*search_term_parts)

    def find_exact(self, lastname, firstname = None, middlename = None, simple_mode=False):
        if len(lastname) <= 1:
            return self.none()
        filter_conditions = { }
        filter_conditions['lastname__iexact' if len(lastname) == 2 else 'lastname__istartswith'] = lastname
        if firstname:
            filter_conditions['firstname__iexact' if middlename and len(firstname) > 2 else 'firstname__istartswith'] = firstname
        if middlename:
            filter_conditions['middlename__istartswith'] = middlename

        select_related_fields = ['profile']
        workers_exact = self.get_workers_raw(filter_conditions, 'exact', select_related_fields, simple_mode=simple_mode)
        alias_filter_condition = {'aliases__' + x: filter_conditions[x] for x in filter_conditions}
        workers_by_alias = self.get_workers_raw(alias_filter_condition, 'alias', select_related_fields, simple_mode=simple_mode)
        unique_workers = uniqify(itertools.chain(workers_exact, workers_by_alias))
        unique_workers = sorted(unique_workers,
                                key=lambda x: (x.profile is not None, x.match_type is 'exact'),
                                reverse=True)
        return unique_workers

    def merge(self, username, from_ids, to_id):
        user = User.objects.get(username=username)
        if not user.is_superuser:
            raise Exception("user must be superuser")
        return merge_workers(from_ids, to_id)

    def filter_doctors(self):
        return self.filter(Q(degrees__degree__is_degree_prior='N') | Q(dissertations_defended__phd='N')).distinct()

    def filter_candidates(self, include_doctors=False):
        """include_doctors: include workers with doctoral degree, which also specified candidate degree.
        Note that not all doctors have specified candidate degree.
        """
        queryset = self.filter(Q(degrees__degree__is_degree_prior='Y') | Q(dissertations_defended__phd='Y'))
        if not include_doctors:
            queryset = queryset.exclude(degrees__degree__is_degree_prior='N').exclude(dissertations_defended__phd='N')
        return queryset.distinct()

    def create_from_fullname(self, fullname, save=True, **create_kwargs):
        "Guess correct first/lastname order and create new worker. If SAVE is True then new object is created in the database."
        names = re.split(r"[ \t,.]+", fullname)
        if not names:
            raise Exception("Name should contain at least one word")
        nitems = len(names)
        # Collect statistics about similar names in the database
        firstname_frequencies = map(lambda n: self.filter(Q(firstname__iexact=n) | Q(aliases__firstname__iexact=n)).count(), names)
        best_name_match = firstname_frequencies.index(max(firstname_frequencies))

        if best_name_match == 0 and nitems <= 3:
            lastname = names[-1]
            firstname = names[0]
            middlename = " ".join(names[1:-1]) if len(names) > 2 else None
        else:
            lastname = names[0]
            firstname = names[1] if len(names) > 1 else None
            middlename = " ".join(names[2:]) if len(names) > 2 else None
        obj = self.model(lastname=lastname, firstname=firstname, middlename=middlename, **create_kwargs)
        if save:
            obj.save()
        return obj

class ProfileManager(MyManager):
    def merge(self, username, from_ids, to_id):
        ''' This function merges users instead of profiles, but each user has one and only one profile,
        so it's ok.
        '''
        user = User.objects.get(username=username)
        if not user.is_superuser:
            raise Exception("user must be superuser")
        if len(from_ids) < 1:
            return
        username_to = self.get(pk=to_id).user.username
        result = None
        for from_id in from_ids:
            try:
                username_from = self.get(pk=from_id).user.username
                result = merge_users(username_from, username_to)
            except self.model.DoesNotExist:
                pass
        return result

def test_delete_unlinked_objects():
    test_lastname = u'Тестовый'

    def assert_exists():
        try:
            Worker.objects.get(lastname=test_lastname)
        except Worker.DoesNotExist:
            assert False
        else:
            pass

    def create_worker():
        return Worker.objects.create(lastname=test_lastname)

    from workers.models import Worker, ManDegree, Degree
    w = create_worker()
    ManDegree.objects.create(worker=w, degree=Degree.objects.all()[0], year=1991)
    Worker.objects.delete_unlinked_objects()
    assert_exists()
    w.delete()

    from publications.models import ArticleAuthorship, Article
    w = create_worker()
    ArticleAuthorship.objects.create(author=w, article=Article.objects.all()[0])
    Worker.objects.delete_unlinked_objects()
    assert_exists()
    w.delete()

    from dissertation_councils.models import DissertationCouncil, DissertationCouncilMembership
    from datetime import datetime
    w = create_worker()
    d = DissertationCouncil.objects.create(number='test')
    DissertationCouncilMembership.objects.create(dissertation_council=d, member=w,startdate=datetime.now())
    assert_exists()
    w.delete()
    d.delete()

    from workers.models import Profile
    w = create_worker()
    p = Profile.objects.all()[0]
    old_w = p.worker
    p.worker = w
    p.save()
    Worker.objects.delete_unlinked_objects()
    assert_exists()
    p.worker = old_w
    p.save()
    w.delete()

    from django.contrib.auth.models import User
    w = create_worker()
    w.creator = User.objects.all()[0]
    w.save()
    Worker.objects.delete_unlinked_objects()
    try:
        Worker.objects.get(lastname=test_lastname)
    except Worker.DoesNotExist:
        pass
    else:
        assert False
    w.delete()


class UnlinkedWorkerManager(WorkerManager):
    '''Model manager for Worker model operating on workers without profiles.'''
    ALIASES_MANAGER_NAME = "objects_unlinked"

    def get_similar(self, unknown_man, **kwargs):
        """Same as WorkerManager.get_similar  but doesn't use cache

        See docstring for WorkerManager.get_similar for details
        """
        workers = list(self.all())
        for i in workers:
            i.worker = i
        worker_aliases = list(self.aliases_manager.all())
        proxy_workers = workers + worker_aliases
        all_similar = self._get_similar(proxy_workers, unknown_man)
        all_similar = [(i[0].worker, i[1], i[2]) for i in all_similar]
        return all_similar

    def get_query_set(self):
        return super(UnlinkedWorkerManager, self).get_query_set().filter(profile__isnull=True)


class WorkerManagerOrderedByArticles(WorkerManager):
    '''Model manager for Worker model applying custom ordering by number of workers' articles.'''
    def get_query_set(self):
        # this is not slow (as may seem):
        # >>> Worker.objects.count()
        # 390
        # >>> Worker.objects.filter(lastname__gt=u'Т').count()
        # 53 # reasonable number
        # first filter, then order
        # >>> t1 = timeit.Timer("Worker.objects.filter(lastname__gt=u'Т').annotate(num_articles=models.Count('articles')).order_by('-num_articles')", 'from workers.models import Worker; from django.db import models')
        # first order all, then filter
        # >>> t2 = timeit.Timer("Worker.objects.annotate(num_articles=models.Count('articles')).order_by('-num_articles').filter(lastname__gt=u'Т')", 'from workers.models import Worker; from django.db import models')
        # >>> t1.repeat(3, 1000)
        # [2.3759419918060303, 2.3729720115661621, 2.3725640773773193]
        # >>> t2.repeat(3, 1000)
        # [2.264693021774292, 2.2592060565948486, 2.2622370719909668]
        # the times are almost the same
        return super(WorkerManagerOrderedByArticles, self).get_query_set().annotate(num_articles=models.Count('articles')).order_by('-num_articles', 'lastname', 'id')


class UnlinkedWorkerAliasManager(MyManager):
    '''Model manager for WorkerAlias model operating on workers without profiles.'''
    def get_query_set(self):
        return super(UnlinkedWorkerAliasManager, self).get_query_set().filter(worker__profile__isnull=True)


class EmploymentManager(MyManager):
    def latest(self):
        '''Returns the latest employment.'''
        try:
            return self.all()[0]
        except IndexError:
            return None

    def recent(self, limit=2):
        '''Returns `limit` recent employments.'''
        return self.all()[:limit]

    def current(self):
        import datetime
        return self.filter(Q(enddate__isnull=True) | Q(enddate__gte=datetime.date.today()))
