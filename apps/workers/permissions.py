# -*- coding: utf-8; -*-
__author__ = 'goldan'
import authority
from common.permissions import MyModelPermission
from organizations.models import Department
from workers.models import Worker
from unified_permissions import has_permission
from common.utils.dates import get_nearest_year
from common.utils.user import get_worker
from organizations.utils import is_representative_for_worker


class WorkerPermission(MyModelPermission):
    label = 'worker_permission'
    checks = ('check_confirm_personal_reports',)
    operations = (
        ("confirm_personal_reports", u"может подтверждать персональные отчеты", False),
        ("merge_worker", u"может присоединить к себе сотрудника", False),
    )
    operations_over_superuser = tuple(
        MyModelPermission.operations_over_superuser +
        ('merge_worker', )
    )

    def check_confirm_personal_reports(self, worker):
        year = get_nearest_year()
        departments_ids = list(set(worker.get_employments_by_year(year).values_list('department', flat=True)))
        departments = Department.objects.filter(id__in=departments_ids)
        return any(has_permission(self.user, "confirm_personal_reports", department) for department in departments)

    def check_merge_worker(self, worker):
        '''Returns True if the worker is allowed to be merged to self.user's worker.'''
        users_worker = get_worker(self.user)
        return bool((users_worker or users_worker.is_representative_for) and not worker.user and worker.is_similar_to(users_worker)) # if worker is None, the is_similar_to method is never called

    def check_edit(self, worker):
        '''Returns True if the worker profile can be edited by the user.'''
        return get_worker(self.user) == worker \
            or is_representative_for_worker(self.user, worker) \
            or worker.is_managed_in_dissertation_council_by(self.user)


authority.register(Worker, WorkerPermission)
