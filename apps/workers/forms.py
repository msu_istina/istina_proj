# -*- coding: utf-8; -*-
from django import forms
from django.db import models
from django.forms.formsets import BaseFormSet, formset_factory
from django.forms.models import inlineformset_factory
from django.forms import ModelForm, Form
import logging
from django.forms.extras.widgets import SelectDateWidget
from models import Employment, EmploymentClaim, Profile, UnknownWorker, Worker, WorkerAlias, Degree, Academy, ManRank
from models import WorkerExternalID, WorkerExternalIDSystem
from organizations.models import Department
from myforms.forms import MyBaseForm
from common.models import ObjectListItem, ObjectList, ObjectListChangeStatusEvent
from django.contrib.contenttypes.models import ContentType
from collections import namedtuple
from workers.utils import make_fullname_from_dict, make_fullname_from_names
from unified_permissions import has_permission

from dissertations.models import Domain

from common.forms import Select2ModelField

import datetime

logger = logging.getLogger("workers.forms")

WORK_TYPES = (
    ("in_journal", "статья в журнале"),
    ("in_book", "книга"),
    ("in_collection", "статья в сборнике"),
)

DAMN_MANY_WORKERS_TO_CHOICE = 10


class WorkerForm(MyBaseForm):
    wid = forms.IntegerField(widget=forms.HiddenInput, required=False)
    lastname = forms.CharField(label=u"Фамилия", max_length=255)
    firstname = forms.CharField(label=u"Имя", max_length=255, required=False)
    middlename = forms.CharField(label=u"Отчество", max_length=255, required=False)

    def __init__(self, data=None, allow_unknown=False, user=None, request_cache=None, preserve_worker=False, initial_worker=None, initial_unknown=False, *args, **kwargs):
        self.similar_no_profiles = kwargs.pop('similar_no_profiles', False)  # filter out similar workers with profiles in worker_form or not
        self.profile_worker = kwargs.pop('profile_worker', None)  # used in profile linking to worker
        self.initial_worker = initial_worker # worker that is found in existing workerships
        self.preserve_worker = preserve_worker if initial_worker else False
        self.initial_unknown = initial_unknown
        self.NAME_FIELDS = ('lastname', 'firstname', 'middlename')
        super(WorkerForm, self).__init__(data, *args, **kwargs)
        self.worker_created = False  # needed for event logging
        self.worker_instance = None
        self.worker_alias_created = False  # needed for event logging
        self.worker_alias_instance = None
        self.allow_unknown = allow_unknown
        self.user = user
        self.request_cache = request_cache

    def get_names(self):
        """Try to read names either from cleaned_data or from initial"""
        try:
            if hasattr(self, 'cleaned_data'):
                return [self.cleaned_data[i] for i in self.NAME_FIELDS]
            else:
                return [self.initial[i] for i in self.NAME_FIELDS]
        except (KeyError, AttributeError):
            raise self.FormIsntReady

    def get_similar(self, request_cache=None):
        """Get similar workers

        request_cache
            request or any other other object for cache

        Reads/Populates self.similar_cache

        Returns tuple(action, list of Workers) or (None, None) if not ready
        For the list of actions consult WorkerManager.get_similar_smart docstring
        """
        if self.preserve_worker:
            worker = self.initial_worker
            SimilarWorker = namedtuple("SimilarWorker", "worker, representation_name, score")
            return ("first", [SimilarWorker(worker, worker.fullname, 1)])

        try:
            l, f, m = self.get_names()
        except self.FormIsntReady:
            return None, None
        if not request_cache:
            request_cache = getattr(self, 'request_cache', None)
        if not hasattr(self, "similar_cache"):
            #FIXME
            # To reduce time of calculation of similar workers,
            # we use advanced methods (via difflib) only for first 5 authors
            # for remaining authors simple method is used:
            # it searches only for exact match of fullname or alias fullname.
            # To determine the number of the author in the form,
            # self.prefix is used. It contains string like 'authors-2' (0-based)
            if self.prefix and "-" in self.prefix:
                author_number = int(self.prefix.split("-")[1]) + 1
            else:
                author_number = 0
            simple_mode = author_number > 5

            # If no_profiles is True, return only workers not linked to profiles
            # (used in link-profile-to-worker page)
            no_profiles = self.similar_no_profiles

            # why this is needed?
            # suppose we add a publication with author Makarov A.A. (worker A)
            # then we add 10 more workers with name Makarov A.A.
            # and then we change the name of A to Makarov B.B.
            # if we try to edit the publication, A will not be found in similar workers
            # because it's not similar any more
            # but it will be passed as self.initial_worker, because the name Makarov A.A.
            # is preserved in the original_name of workership
            # so we force to add that worker to the similar worker list, and it will be selected by default
            # because of wid (see populate_selected_worker templatetag)
            extra_workers = filter(bool, [self.initial_worker])

            if no_profiles:
                if self.profile_worker:
                    extra_workers.append(self.profile_worker)
                self.similar_cache = Worker.objects_unlinked.get_similar_smart(f, m, l, extra_workers=extra_workers, simple_mode=simple_mode)
            else:
                self.similar_cache = Worker.objects.get_similar_smart(f, m, l, current_worker=self.profile_worker, extra_workers=extra_workers, request_cache=request_cache, simple_mode=simple_mode)

            if self.initial_unknown:
                self.similar_cache = list(self.similar_cache)
                self.similar_cache[0] = "unknown"

        if self.similar_cache[0] == "unknown" and not self.allow_unknown:
            self.similar_cache[0] = "new" if not self.similar_cache[1] else "choice"
        return self.similar_cache

    def get_fullname(self):
        """Build fullname from cleaned data. Used also to construct back original name of the worker and save it to Workership."""
        try:
            l, f, m = self.get_names()
        except self.FormIsntReady:
            return None
        return make_fullname_from_names(l, f, m)

    def clean_name(self, name):
        '''Remove extra dots, whitespace and other characters from the name. In the database there should be no dots in names, they are added only in rendering.'''
        try:
            return name.strip(" .,:-")
        except AttributeError:
            return name

    def clean_firstname(self):
        '''Remove extra dots and other characters from the firstname.'''
        logger.debug("Cleaning firstname %s in the form.", self.cleaned_data['firstname'])
        return self.clean_name(self.cleaned_data['firstname'])

    def clean_middlename(self):
        '''Remove extra dots and other characters from the middlename.'''
        return self.clean_name(self.cleaned_data['middlename'])

    def save(self):
        """Select or create coresponding worker

        first tries to select worker with id=wid
        if fails and wid==-1 pass then just skip
        else create new worker

        creates self.worker_instance if required
        returns id for current worker or None if current worker is unknown
        """
        if not hasattr(self, "worker_saved"):
            c = dict((i, self.cleaned_data[i]) for i in self.NAME_FIELDS)
            wid = self.cleaned_data['wid']
            objs = Worker.objects.filter(id=wid)
            if wid and objs:
                logger.debug("selected existing worker: %s (id %s)", objs[0].fullname, objs[0].id)
                c["worker"] = self.worker_instance = objs[0]
                if (c["lastname"], c["firstname"], c["middlename"]) != (self.worker_instance.lastname, self.worker_instance.firstname, self.worker_instance.middlename): # create worker alias only if new data is not equal to worker's current name
                    try:
                        WorkerAlias.objects.get(worker=c["worker"], lastname=c["lastname"], firstname=c["firstname"], middlename=c["middlename"])
                    except WorkerAlias.DoesNotExist:
                        logger.debug("about to create new worker alias with parameters: %s", c)
                        self.worker_alias_instance = WorkerAlias.objects.create(validated=0, creator=self.user, **c )
                        self.worker_alias_created = True
                    except models.exceptions.MultipleObjectsReturned:
                        all_aliases = WorkerAlias.objects.filter(**c)
                        logger.error(u"We somehow have several identical aliases: %s", u",".join(str(i.id) for i in all_aliases))
            elif self.allow_unknown and wid == -1:
                logger.debug("user has no any idea about who is '%s'", c)
                self.worker_instance = UnknownWorker()
            else:
                logger.debug("about to create new worker with parameters: %s", c)
                self.worker_instance = Worker.objects.create(creator=self.user, **c)
                self.worker_created = True
            self.id = self.worker_instance and self.worker_instance.id
            self.worker_saved = bool(self.id)
        return self.id


class BaseWorkerFormSet(BaseFormSet):
    unrelated=False
    def __init__(self, required=False, required_error_msg=None, profile_worker=None, allow_unknown=False, user=None, object_instance=None, workerships=None, *args, **kwargs):
        self.required = required
        self.required_error_msg = required_error_msg
        self.profile_worker = profile_worker
        if self.required and self.required_error_msg is None:
            raise Exception("You must pass required_error_msg argument")
        self.workers_created = []  # needed for event logging. The list is passed to upper level of execution, where we know the user who created the form.
        self.worker_aliases_created = []  # needed for event logging. The list is passed to upper level of execution, where we know the user who created the form.
        self.allow_unknown = allow_unknown
        self.user = user
        self.request_cache = None
        self.object_instance = object_instance
        if workerships: # used to pass non-default workerships to preserve (e.g. opponents for dissertation)
            self._workerships = workerships
            self._workerships_used = []
        super(BaseWorkerFormSet, self).__init__(*args, **kwargs)
        self.request_cache = Worker.objects.get_disambiguation(map(lambda f: f.get_names(), self.forms), current_worker=profile_worker,
          unrelated=self.unrelated)
        for form, cache in zip(self.forms, self.request_cache):
            form.request_cache = cache

    def _construct_form(self, i, **kwargs):
        if self.profile_worker:
            kwargs["profile_worker"] = self.profile_worker
        kwargs["allow_unknown"] = self.allow_unknown
        kwargs["user"] = self.user
        kwargs["request_cache"] = self.request_cache and len(self.request_cache) > i and self.request_cache[i]
        if self.object_instance:
            self.preserve_existing_workerships(i, kwargs) # do not use ** to modify existing dict

        return super(BaseWorkerFormSet, self)._construct_form(i, **kwargs)

    def preserve_existing_workerships(self, i, kwargs):
        instance = self.object_instance
        try:
            workerships = self._workerships
        except AttributeError:
            if getattr(instance, "is_workership", False):
                workerships = [instance]
            else:
                workerships = instance.workerships.all()
            self._workerships = workerships
            self._workerships_used = []
        try:
            initial = self.initial[i]
        except KeyError:
            return
        else:
            name = make_fullname_from_dict(initial)
            for workership in workerships:
                # if the field value (worker name) is equal to some workership's original name, or workership's.worker name,
                # then we set this worker as initial for this field, and then select it by default
                # if, moreover, the workership is linked to other non-editable user, we preserve the worker (lock it)
                worker_name = getattr(workership, "original_name", None)
                if not worker_name and workership.worker:
                    worker_name = workership.worker.fullname_short
                if worker_name == name and workership not in self._workerships_used:
                    self._workerships_used.append(workership) # not to use it twice
                    if workership.worker:
                        self.initial[i]['wid'] = workership.worker.id
                        kwargs['initial_worker'] = workership.worker
                        if workership.worker.user and not has_permission(kwargs["user"], "edit", workership.worker):
                            kwargs['preserve_worker'] = True
                    else:
                        kwargs['initial_unknown'] = True # to select it by default
                    return # use only first found (not used before) workership

    def check_workerships_preserved(self):
        try:
            workerships = self._workerships_used
        except AttributeError:
            pass
        else:
            selected_workers_ids = set([form.cleaned_data['wid'] for form in self.forms])
            for workership in workerships:
                if workership.worker \
                and not workership.worker.id in selected_workers_ids \
                and workership.worker.user \
                and not has_permission(self.user, "edit", workership.worker):
                    name = getattr(workership, "original_name", workership.worker.fullname_short)
                    username = workership.worker.user.username
                    raise forms.ValidationError(u"При редактировании объекта вы открепили от него другого пользователя, что запрещено. Пожалуйста, добавьте сотрудника %s (пользователь %s) в список авторов." % (name, username))

    def calculate_similar(self):
        """Force calculate similar workers lists. Apply damn-many-authors policy"""
        choices_count = 0
        for i, form in enumerate(self.forms):
            try:
                action, workers = form.get_similar(request_cache=self.request_cache and len(self.request_cache) > i and self.request_cache[i])
            except TypeError:
                # catch the strange bug when single None is returned
                logger.error("Error in calculate_similar", exc_info=True)
                continue
            if action == "choice":
                choices_count += 1
        if choices_count >= DAMN_MANY_WORKERS_TO_CHOICE and self.allow_unknown:
            for form in self.forms:
                try:
                    action, workers = form.similar_cache
                except AttributeError:
                    # there wasn't any data in the form so no cache were caclulated
                    continue
                except TypeError:
                    # catch the strange bug when single None is returned
                    logger.error("Error in calculate_similar", exc_info=True)
                    continue
                if action == "choice":
                    form.similar_cache = "unknown", workers

    def clean(self):
        """Checks that no two articles have the same title."""
        if any(self.errors):
            return
        if self.required and self.total_form_count() == 0:
            raise forms.ValidationError(self.required_error_msg)
        # Check that all workerships that are linked to users and cannot be edited by the request.user
        # are preserved
        if self.object_instance:
            self.check_workerships_preserved()

    def save(self):
        """Call save() in all subforms and return ids

        Save each worker postion in worker.position
             -||-        fullname in worker.fullname_orig
        """
        if any(self.errors):
            return
        ids = []
        self.worker_instances = []
        for i in range(self.total_form_count()):
            form = self.forms[i]
            id = form.save()
            form.worker_instance.position = i + 1
            form.worker_instance.fullname_orig = form.get_fullname()
            if form.worker_created and form.worker_instance and form.worker_instance not in self.workers_created:
                self.workers_created.append(form.worker_instance)
            if form.worker_alias_created and form.worker_alias_instance and form.worker_alias_instance not in self.worker_aliases_created:
                self.worker_aliases_created.append(form.worker_alias_instance)
            if id in ids:
                continue
            if id is not None:
                ids.append(id)
            self.worker_instances.append(form.worker_instance)


WorkerFormSet = formset_factory(WorkerForm, formset=BaseWorkerFormSet, extra=0)
UnrelatedWorkerFormSet = type('UnrelatedWorkerFormSet', (WorkerFormSet,), { 'unrelated': True })


WorkerAliasFormSet = inlineformset_factory(Worker, WorkerAlias, fields=('lastname', 'firstname', 'middlename'), extra=1)

def get_EmploymentForm(theModel):
    institutionLabel = u"Я работаю в другой организации:" if theModel is EmploymentClaim else u"В другой организации:"
    class EmploymentForm(forms.ModelForm):
        parttime = forms.BooleanField(label=u"Работа по совместительству", required=False)
        dep = forms.CharField(required=False, widget=forms.HiddenInput())
        institution = forms.CharField(label=institutionLabel, max_length=1500, required=False)

        class Meta:
            model = theModel
            fields = ('startdate', 'enddate', 'parttime', 'position')

        def __init__(self, *args, **kwargs):
            super(EmploymentForm, self).__init__(*args, **kwargs)
            self.fields['startdate'].input_formats = ["%d.%m.%Y"]
            self.fields['enddate'].input_formats = ["%d.%m.%Y"]
            self.fields['parttime'].label = u"По совместительству"
            self.fields['institution'].widget.attrs['placeholder'] = u'Пожалуйста, укажите полное название организации'
            if kwargs.get('instance', None):
                del self.fields['institution']
                self.fields['dep'].initial = kwargs.get('instance').department.id

        def clean(self):
            try:
                if self.cleaned_data['startdate'] > self.cleaned_data['enddate']:
                    raise forms.ValidationError(u"Дата начала работы должна предшествовать дате окончания работы.")
            except (KeyError, TypeError):
                pass
            return self.cleaned_data
    return EmploymentForm

EmploymentForm = get_EmploymentForm(Employment)
EmploymentClaimForm = get_EmploymentForm(EmploymentClaim)


class EmploymentPositionForm(forms.ModelForm):

    class Meta:
        model = Employment
        fields = ('position',)

    def __init__(self, *args, **kwargs):
        super(EmploymentPositionForm, self).__init__(*args, **kwargs)
        self.fields['position'].label = u""
        self.fields['position'].empty_label = u"Выберите должность..."


class WorkerProfileForm(forms.ModelForm):
    '''The form containing fields for the Profile model.'''

    class Meta:
        model = Profile
        fields = ('lastname', 'firstname', 'middlename',)


class DegreeForm(Form):
    degree_prior = "degree_prior"
    doctor = "doctor"
    degree_choices = {
        (degree_prior, u'Кандидат'),
        (doctor, u'Доктор'),
    }
    degree_type = forms.ChoiceField(label=u"Степень", choices=degree_choices)
    branch = forms.ModelChoiceField(label=u"Область науки", queryset=Domain.objects.all())
    year = forms.IntegerField(label=u"Год присуждения", required=False)

    def clean(self):
        try:
            if self.cleaned_data.get('year') and (self.cleaned_data['year'] > datetime.date.today().year or self.cleaned_data['year'] < 1900):
                raise forms.ValidationError(u"Некорректная дата")
        except KeyError:
                pass
        return self.cleaned_data

from common.dynatree_widgets import DynatreeWidget, DynatreeLazyWidget
class RankForm(Form):
    corresponding_member = "corresponding_member"
    academician = "academician"
    department_docent = "department_docent"
    department_professor = "department_professor"
    branch_docent = "branch_docent"
    branch_professor = "branch_professor"

    academics = (
        (academician, u'академик'),
        (corresponding_member, u'член-корреспондент'),
    )

    ordinary = (
        (branch_professor, u'профессор по специальности'),
        (branch_docent, u'доцент/с.н.с. по специальности'),
        (department_docent, u'доцент по кафедре'),
        (department_professor, u'профессор по кафедре'),
    )

    rank_choices = academics + ordinary
    rank_type = forms.ChoiceField(label=u"Звание", choices=rank_choices)
    academy =  forms.ModelChoiceField(label=u"Академия", queryset=Academy.objects.all(), required=False)
    branch_num = forms.CharField(label=u"Название специальности", max_length=255, required=False)
    department = forms.ModelChoiceField(label=u"Кафедра", queryset=Department.objects.filter(name__icontains=u'Кафедра'),
                                        empty_label=u"Из другой организации", required=False)
    date = forms.DateField(label=u"Дата присвоения", required=False)

    def clean(self):
        try:
            chosen_rank_type = self.cleaned_data['rank_type']
            if ((chosen_rank_type == self.corresponding_member or chosen_rank_type == self.academician) and not self.cleaned_data['academy']):
                    raise forms.ValidationError(u"Укажите академию")
            if ((chosen_rank_type == self.branch_docent or chosen_rank_type == self.branch_professor) and not self.cleaned_data['branch_num']):
                    raise forms.ValidationError(u"Укажите номер специальности")

        except KeyError:
                pass
        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        #from equipment.forms import departments_sql #FIXME
        super(RankForm, self).__init__(*args, **kwargs)
        #self.fields['department'].widget = DynatreeWidget(select_mode=1, initial_sql=departments_sql)


class OrdinaryRankForm(RankForm):
    rank_type = forms.ChoiceField(label=u"Звание", choices=RankForm.ordinary)
    def __init__(self, *args, **kwargs):
        super(OrdinaryRankForm, self).__init__(*args, **kwargs)
        for f in ['academy']:
            self.fields.pop(f)

class AcademyRankForm(RankForm):
    rank_type = forms.ChoiceField(label=u"Звание", choices=RankForm.academics)
    def __init__(self, *args, **kwargs):
        super(AcademyRankForm, self).__init__(*args, **kwargs)
        for f in ['branch_num']:
            self.fields.pop(f)

class ExternalIDForm(forms.ModelForm):
    system = forms.ModelChoiceField(label=u"Система", queryset=WorkerExternalIDSystem.objects.all(), required=False,
                                    empty_label=u"Пожалуйста, выберите название из списка")
    system_other = forms.CharField(label=u"Система не из списка", max_length=1500, required=False)
    userid = forms.CharField(label=u"Ваш идентификатор", max_length=1500, required=True)

    class Meta:
        model = WorkerExternalID
        fields = ('system', 'system_other', 'userid')

    def clean(self):
        import re
        cleaned_data = super(ExternalIDForm, self).clean()
        system_other = cleaned_data.get('system_other', None)
        system = cleaned_data.get('system', None)
        userid = cleaned_data.get('userid', None)
        if userid and system and system.userid_regex:
            pattern = re.compile(system.userid_regex)
            if not pattern.match( userid ):
                raise forms.ValidationError(u"Идентификатор не соответствует шаблону для '%s': %s" % (system.name, system.userid_hint or ''))
        if not system and not system_other:
            raise forms.ValidationError(u"Пожалуйста, укажте название системы.")
        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(ExternalIDForm, self).__init__(*args, **kwargs)
        self.fields['system_other'].widget.attrs['placeholder'] = u'Пожалуйста, укажите название системы'


class ReportSignForm(forms.ModelForm):
    selected = forms.BooleanField(label=u"", initial=True, required=False, widget=forms.CheckboxInput(attrs={'style': 'float: left'}))

    class Meta:
        model = ObjectListItem
        fields = ('object_id', 'content_type', 'title')

    def __init__(self, *args, **kwargs):
        activity = kwargs.pop('activity', None)
        self.user = kwargs.pop('user')
        self.object_list = kwargs.pop('object_list', None)
        initially_checked = kwargs.pop('initially_checked', [])
        auto_check = kwargs.pop('check_all_as_first_open', False)
        super(ReportSignForm, self).__init__(*args, **kwargs)
        self.fields['object_id'].widget = forms.HiddenInput()
        self.fields['content_type'].widget = forms.HiddenInput()
        self.fields['title'].widget = forms.HiddenInput()
        if activity:
            object_id = activity.id
            content_type = ContentType.objects.get_for_model(activity).id
            activity_kwargs = {
                'object_id': object_id,
                'content_type': content_type
            }
            if self.object_list and not activity_kwargs in initially_checked:
                self.fields['selected'].initial = False
            if auto_check:
                self.fields['selected'].initial = True
            self.fields['object_id'].initial = object_id
            self.fields['content_type'].initial = content_type
            self.fields['title'].initial = activity.title

    def save(self, **kwargs):
        object_list_item = super(ReportSignForm, self).save(commit=False)
        object_list_item.class_name = self.cleaned_data['content_type'].name
        object_list_item.object_list = self.object_list
        object_list_item.save()





class ReportDisapproveForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ReportDisapproveForm, self).__init__(*args, **kwargs)
        self.fields['comment'].label = u"Укажите комментарий"

    class Meta:
        model = ObjectListChangeStatusEvent
        fields = ('comment',)


class AdminSetPasswordForm(forms.Form):
    password = forms.CharField(label=u"Новый пароль", max_length=64, widget=forms.PasswordInput())


class ProfileChoiceField(Select2ModelField):

    def __init__(self, *args, **kwargs):
        super(ProfileChoiceField, self).__init__(model=Profile, *args, **kwargs)

    def get_title(self, obj):
        return obj.get_fullname()

    def get_description(self, obj):
        if obj.worker:
            all_emps = ""
            for emp in obj.worker.current_employments:
                all_emps += emp.department.name + ", " + emp.department.organization.name + "; "
            return u"""%s, %s""" % (obj.user.username, all_emps[:-2])
        else:
            return obj.user.username
