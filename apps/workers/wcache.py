"""Here located tools for worker list loading speed up

The main ideas:
1. We preserve labeled cache of workers. It consist of two parts
    a. List of workers and aliases itself
    b. Date of the last created worker/alias
2. On request, we check if cache exists and recalculate it if required.
   Then we fetch workers created after date saved in the cache.
   Cache + New db data lists concatenation is the answer

If you just want to use it right now - check out function managers.WorkerManager.get_worker_list
"""
from collections import namedtuple
from django.core.cache import get_cache
import datetime
import cPickle as pickle
import logging
import time

from common.utils.uniqify import uniqify
from models import Worker, WorkerAlias, ProxyWorker, WORKER_TYPES
from settings import CACHE_TIME_WORKERS_LIST

cache = get_cache("bigdata")


WorkerSimplified = namedtuple("WorkerSimplified", "lastname, firstname, middlename, fullname, fullname_initials, id, type, transformation")

WLIST_CACHE = "workers_list_v4"
LASTUSER_CACHE = "workers_last_update"

logger = logging.getLogger("workers.wcache")


def get_worker_list():
    """Get (almost) up-to-date list of worker info generated by ProxyWorker.get_all_proxy_workers

    "Almost" means that cache "aware" of all workers and their aliases updates,
    but if any worker or alias had been deleted after cache were created, then
    the function will still return record for him

    returns list of WorkerSimplified
    """
    last_updated_at, data = _load()
    if data is not None:
        logger.info("Using workers list")
    else:
        logger.info("Recalculating workers list")
        last_updated_at, data = recalculate_worker_list_cache()
    last_updated_at = datetime.datetime.fromtimestamp(float(last_updated_at))
    ndata = ProxyWorker.get_all_proxy_workers(updated_at__gte=last_updated_at)
    ndata = map(simplify_proxy_worker, ndata)
    data = uniqify(ndata + data, idfun=lambda w: (w.id, w.type, w.transformation))
    return data


def recalculate_worker_list_cache():
    """Setup worker lists related caches"""
    workers = ProxyWorker.get_all_proxy_workers()
    last_updated_at = max(filter(bool, (i.updated_at for i in workers))) # somehow a worker can have None in updated_at, if it has been added not through django
    last_updated_at = time.mktime(last_updated_at.timetuple())
    list_simplified_workers = map(simplify_proxy_worker, workers)
    _dump(last_updated_at, list_simplified_workers)
    return last_updated_at, list_simplified_workers


def simplify_proxy_worker(w):
    """Convert ProxyWorker to a WorkerSimplified"""
    return WorkerSimplified(w.lastname.lower(), w.firstname.lower(), w.middlename.lower(),
                            w.fullname.lower(), w.get_fullname(initials=True).lower(),
                            w.id, w.worker_type, w.transformation_type)


def unsimplify_list(ws_list):
    """Convert a list of WorkerSimplified to a list of ProxyWorker's using db"""
    wids = [w.id for w in ws_list if w.type == WORKER_TYPES.WORKER]
    waids = [w.id for w in ws_list if w.type == WORKER_TYPES.WORKER_ALIAS]
    w_cache = dict((i.id, i) for i in Worker.objects.filter(id__in=wids))
    wa_cache = dict((i.id, i) for i in WorkerAlias.objects.filter(id__in=waids).select_related())
    proxy_workers = []
    for w in ws_list:
        try:
            if w.type == WORKER_TYPES.WORKER:
                worker_or_alias = w_cache[w.id]
            else:
                worker_or_alias = wa_cache[w.id]
        except KeyError:
            # i.e. worker (or alias) was deleted
            continue
        proxy_workers.append(worker_or_alias.get_transformation(w.transformation))
    return proxy_workers


def _dump(last_updated_at, list_simplified_workers):
    """Save list of simplified workers and last_update_at in cache

    last_update_at should be a timestamp as string
    """
    data = {LASTUSER_CACHE: last_updated_at, WLIST_CACHE: tuple(map(tuple, list_simplified_workers))}
    try:
        cache.set_many(data, timeout=CACHE_TIME_WORKERS_LIST)
    except:
        logger.error("Cache set failed", exc_info=True)
        cache.delete(LASTUSER_CACHE)


def _load():
    """Reads list of simplified workers and last update date from cache

    if data is not found (or incomplete) returns (None, None)
    else returns (last_updated_at, list of WorkerSimplified)
    """
    c = map(cache.get, (LASTUSER_CACHE, WLIST_CACHE))
    if any(i is None for i in c):
        return (None, None)
    last_updated_at = c[0]
    try:
        data = [WorkerSimplified(*i) for i in c[1]]
    except pickle.UnpickleableError:
        return (None, None)
    return last_updated_at, data
