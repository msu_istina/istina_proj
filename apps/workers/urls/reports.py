from django.conf.urls.defaults import *

urlpatterns = patterns('workers.views',
    url(r'^$', "worker_reports_index", name="workers_worker_reports_index"),
    url(r'^year/(?P<years_str>[\d\-]+)/full/$', "worker_report_year_full", name="workers_worker_report_year_full"),
    url(r'^year/(?P<years_str>[\d\-]+)/department/$', "worker_report_year_choose_department", name="workers_worker_report_year_choose_department"),
    url(r'^year/(?P<years_str>[\d\-]+)/(?P<department_id>\d+)/$', "worker_report_year", name="workers_worker_report_year"),
    url(r'^year/(?P<years_str>[\d\-]+)/(?P<department_id>\d+)/check/$', "worker_report_year_check", name="workers_worker_report_check"),
    url(r'^year/(?P<years_str>[\d\-]+)/(?P<department_id>\d+)/onbehalf/$', "worker_report_year", {'on_behalf': True}, name="workers_worker_report_year_onbehalf"),
    url(r'^year/(?P<years_str>[\d\-]+)/(?P<department_id>\d+)/sign/$', "worker_report_year_sign", name="workers_worker_report_year_sign"),
    url(r'^year/(?P<years_str>[\d\-]+)/(?P<department_id>\d+)/getpdf/$', "worker_report_year_get_pdf", name="workers_worker_report_year_get_pdf"),
    url(r'^year/(?P<years_str>[\d\-]+)/(?P<department_id>\d+)/update_marked_list/$', "worker_report_year_update_marked_list", name="workers_worker_report_year_update_marked_list"),
    url(r'^year/(?P<years_str>[\d\-]+)/(?P<department_id>\d+)/onbehalf/update_marked_list/$', "worker_report_year_update_marked_list", {'on_behalf': True}, name="workers_worker_report_year_update_marked_list"),
    url(r'^year/(?P<years_str>[\d\-]+)/(?P<department_id>\d+)/approve/$', "worker_report_year_set_status", {'status': 'CONFIRM'}, name="workers_worker_report_year_approve"),
    url(r'^year/(?P<years_str>[\d\-]+)/(?P<department_id>\d+)/disapprove/$', "worker_report_year_set_status", {'status': 'REEDIT'}, name="workers_worker_report_year_disapprove"),
    url(r'^articles/vak/$', "worker_report_vak_articles", name="workers_worker_report_vak_articles"),

	url(r'^monitoring/', include('workers.urls.monitoring')),
    )  + patterns('workers.views.reports',
    url(r'^infolist/((?P<mode>.+)/)?$', "worker_report_infolist", name="workers_worker_report_infolist"),
    url(r'^publications_list/(?P<report_depth>[\d]+)/(?P<count_current_year>[\d]+)/$', "worker_report_publications_list", name="workers_worker_report_publications_list"),
    url(r'^publications_list/(?P<report_depth>[\d]+)/$', "worker_report_publications_list", name="workers_worker_report_publications_list"),
    url(r'^ext_articles/$', "worker_report_ext_articles", name="workers_worker_report_ext_articles"),
    )
