from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_detail

excel_dict = {'export_excel': True}

urlpatterns = patterns('workers.views.monitoring',

    url(r'^$', "index", name="worker_reports_monitoring_index"),

    url(r'^profile/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$', 'profile', name="worker_reports_monitoring_profile"),
    url(r'^profile/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$', 'profile', excel_dict, name="worker_reports_monitoring_profile_excel"),

    url(r'^books/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$', 'books',
        name='worker_reports_monitoring_books'),
    url(r'^books/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$', 'books',
        excel_dict, name='worker_reports_monitoring_books_excel'),

    url(r'^publications/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$', 'publications',
        name='worker_reports_monitoring_publications'),
    url(r'^publications/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$', 'publications',
        excel_dict, name='worker_reports_monitoring_publications_excel'),

    url(r'^template/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$', 'publications_template',
        name='worker_reports_monitoring_publications_template_excel'),

    url(r'^patents/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$', 'patents',
        name='worker_reports_monitoring_patents'),
    url(r'^patents/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$', 'patents',
        excel_dict, name='worker_reports_monitoring_patents_excel'),

    url(r'^dissertations/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$', 'dissertations',
        name='worker_reports_monitoring_dissertations'),
    url(r'^dissertations/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$', 'dissertations',
        excel_dict, name='worker_reports_monitoring_dissertations_excel'),

    url(r'^conferences/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$', 'conferences',
        name='worker_reports_monitoring_conferences'),
    url(r'^conferences/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$', 'conferences',
        excel_dict, name='worker_reports_monitoring_conferences_excel'),

    url(r'^theses/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$', 'theses',
        name='worker_reports_monitoring_theses'),
    url(r'^theses/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$', 'theses',
        excel_dict, name='worker_reports_monitoring_theses_excel')

)
