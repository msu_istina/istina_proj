from django.conf.urls.defaults import *
from django.views.generic import RedirectView
from django.views.generic.list_detail import object_detail, object_list
from django.db.models import Count

from workers.models import Worker, Employment

urlpatterns = patterns('workers.views',
    url(r'^$', object_list, {'queryset': Worker.objects.annotate(Count('articles')).order_by('-articles__count', 'lastname'), 'template_object_name': 'worker'}, name='workers_worker_list'),
    url(r'^unemployed/$', object_list, {'queryset': Worker.objects.get_unemployed(), 'template_object_name': 'worker'}, name='workers_worker_list_unemployed'),
    url(r'^last_added/$', "worker_last_published", name="workers_worker_last_published"),
    url(r'^(?P<worker_id>\d+)/$', "worker_detail", name="workers_worker_detail"),
    url(r'^(?P<worker_id>\d+)/ajax/$', "worker_detail_short_ajax",),
    url(r'^(?P<object_id>\d+).bib$', object_detail, {'queryset': Worker.objects.all(), 'template_name': 'publications/publication_list.bib', 'mimetype': 'text/plain'}, name='workers_worker_publications_bibtex'),
    url(r'^(?P<worker_id>\d+)/style/(?P<style>.+)/$', "worker_detail_in_style", name='workers_worker_detail_in_style'),
    url(r'^(?P<worker_id>\d+)/similar/$', "similar_workers_list", name="similar_workers_list"),
    url(r'^(?P<worker_id>\d+)/worker_merge/$', "merge_workers_request", name='merge_workers'),
    url(r'^search/$', "autocomplete_search_workers", name="workers_worker_search"),
    url(r'^raw/$', "worker_list_raw", name='workers_worker_list_raw'),
    url(r'^raw/(?P<page_index>\d+)/$', "worker_list_raw_page", name='workers_worker_list_raw_page'),
    url(r'^profile/edit/$', "profile_edit", name='workers_profile_edit'),
    url(r'^profile/edit/main/$', "profile_edit", {'form_name': 'main'}, name='workers_profile_edit_main'),
    url(r'^profile/edit/aliases/$', "profile_edit", {'form_name': 'aliases'}, name='workers_profile_edit_aliases'),
    url(r'^profile/link/$', "profile_link", name='workers_profile_link'),
    url(r'^employment/(?P<object_id>\d+)/$', object_detail, {'queryset': Employment.objects.all(), 'template_name': 'workers/employment_detail.html', 'template_object_name': 'employment'}, name='workers_employment_detail'),
    url(r'^(?:(?P<worker_id>\d+)/)?employment/edit/(?:(?P<employment_id>\d+)/)?$', "employment_edit", name='workers_employment_edit'),
    url(r'^employment/edit/position/(?P<employment_id>\d+)/$', "employment_edit_position", name='workers_employment_edit_position'),
    url(r'^employment/delete/(?P<employment_id>-?\d+)/$', "employment_delete", name='workers_employment_delete'),
    url(r'^worker_search/$', "search_workers", name="search_workers"),
    url(r'^worker_similar_list/$', "similar_workers_list", name="similar_workers_list"),
    url(r'^(?:(?P<worker_id>\d+)/)?degree/edit/$', "degree_edit", name='workers_degree_edit'),
    url(r'^degree/delete/(?P<degree_id>\d+)/$', "degree_delete", name='workers_delete_delete'),
    url(r'^rank/delete/(?P<rank_id>\d+)/$', "rank_delete", name='workers_rank_delete'),
    (r'^(?P<worker_id>\d+)/reports/', include('workers.urls.reports')),
    (r'^(?P<worker_id>\d+)/contests/', include('contests.urls')),
    (r'^registration/', include('workers.urls.registration')),
)
