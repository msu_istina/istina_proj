from django.conf.urls.defaults import *

urlpatterns = patterns(
    'workers.views.registration',
    url(r'^$', "registration_fullname", name='workers_registration'),
    url(r'^fullname/$', "registration_fullname", name='workers_registration_fullname'),
    url(r'^aliases/$', "registration_aliases", name='workers_registration_aliases'),
    url(r'^search/$', "registration_search", name='workers_registration_search'),
    url(r'^employment/$', "registration_employment", name='workers_registration_employment'),
    url(r'^degree/$', "registration_degree", name='workers_registration_degree'),
    url(r'^keywords/$', "registration_keywords", name='workers_registration_keywords'),
    url(r'^other_systems/$', "registration_other_systems", name='workers_registration_other_systems'),
    url(r'^other_systems/delete/(?P<pk>\d+)/$', "delete_external_id", name='workers_registration_other_systems_delete'),
    url(r'^completed/$', "registration_completed", name='workers_registration_completed'),
)
