from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'^$', 'workers.views.home', name='home'),
    url(r'^profile/$', 'userprofile.views.my_public', name='home_profile'),
    url(r'^articles/lost/$', 'workers.views.lost_articles', name='home_lost_articles'),
    url(r'^reports/', include('workers.urls.reports')),
    url(r'^contests/', include('contests.urls')),
)
