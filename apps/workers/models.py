# -*- coding: utf-8 -*-
import datetime

from functools import partial
from itertools import chain
from collections import namedtuple

from pytils.translit import translify
from userprofile.models import BaseProfile
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.db.models.signals import post_save
from django.contrib.sites.models import Site
from django.db.models.loading import get_model
from common.models import MyModel
from common.utils.activities import get_activities, get_activities_last_added
from common.utils.dates import get_nearest_year
from common.utils.enum import Enum
from common.utils.uniqify import uniqify
from common.utils.models_related import get_related_objects, get_linked_to_instance_objects
from workers.utils import get_fullname_raw, get_all_fullnames, get_all_named_representations
from workers.managers import *
from statistics.sqlutils import get_ext_articles_ids
from unified_permissions import has_permission
from unified_permissions.models import PermissionWithLabel
from common.utils.models_related import has_related_objects as _has_related_objects

logger = logging.getLogger("workers.models")


# transformations indicates what kind of transformation were applied to
# original object to get this guessed alias
TRANSFORMATIONS = Enum("ID", "TRANSLIT")
WORKER_TYPES = Enum("WORKER", "WORKER_ALIAS")

Name = namedtuple("Name", "lastname, firstname, middlename")

class ManAbstract(object):
    """Abstract class for everything that has names

    i.e. firstname, middlename, lastname
    """

    def get_fullname(self, initials=False):
        return get_fullname(self, initials)

    fullname = property(get_fullname)
    fullname_initials = property(partial(get_fullname, initials=True))

    @property
    def fullname_short(self):
        return self.get_fullname(initials=True)

    @property
    def fullname_bib(self):
        return get_fullname(self, comma=True)

    @property
    def title(self):
        return self.fullname


class WorkerAbstract(ManAbstract):
    """Object that is interesting for WorkerManager's get similar routines

    i.e. only models that matches WORKER_TYPES could inherite from WorkerAbstract
    """
    def get_transformation(self, transformation):
        """Apply transformation and return resulting ProxyWorker or None if fails

        None can be returned, for example, during translitiration
        if original string not in russian
        """
        if transformation == TRANSFORMATIONS.ID:
            return ProxyWorker(self, transformation)
        elif transformation == TRANSFORMATIONS.TRANSLIT:
            name_fields = "lastname", "firstname", "middlename"
            try:
                names_dict = dict((f, translify(getattr(self, f))) for f in name_fields)
            except (ValueError, TypeError):
                return None
            return ProxyWorker(self, transformation, **names_dict)
        raise Exception("Unknown transformation: %s" % transformation)

    def get_transformations_list(self):
        """Returns list of possible unique transformations (guessed aliases) for person

        Currently it supports only translitification and id (dummy) transformation
        List of transformation is stored in TRANSFORMATIONS enumeration

        returns
            list of ProxyWorker's
        """
        ts = filter(None, map(self.get_transformation, TRANSFORMATIONS))
        ts = uniqify(ts, idfun=lambda pw: (pw.lastname, pw.firstname, pw.middlename))
        return ts


class Profile(BaseProfile, ManAbstract, MyModel):
    firstname = models.CharField(u"Имя", max_length=255, blank=True)
    middlename = models.CharField(u"Отчество", max_length=255, blank=True)
    lastname = models.CharField(u"Фамилия", max_length=255, blank=True)
    researcher_id = models.CharField(u"ResearcherID", max_length=255, db_column="RESEARCHER_ID", blank=True)
    worker = models.OneToOneField("Worker", related_name="profile", verbose_name=u"Сотрудник", null=True, blank=True)
    keywords = models.CharField(u"Ключевые слова", max_length=2000, db_column="KEYWORDS", blank=True)
    engkeywords = models.CharField(u"Ключевые слова на английском языке", max_length=2000, db_column="ENGKEYWORDS", blank=True)
    registration_status = models.CharField(u"Состояние регистрации", max_length=128, db_column="REGISTRATION_STATUS", blank=True)
    sciarea = models.CharField(u"Описание области научных интересов", max_length=2000, db_column="SCIAREA", blank=True)

    objects = ProfileManager()
    search_attr = "lastname"

    class Meta:
        verbose_name = u"Профиль пользователя"
        verbose_name_plural = u"Профили пользователей"
        ordering = ("lastname", "worker__lastname")

    def __unicode__(self):
        s = u"Profile of "
        if self.worker:
            return s + "worker " + unicode(self.worker)
        try:
            return s + "user " + unicode(self.user)
        except User.DoesNotExist: # There are some broken profiles in base
            return u"Invalid profile with no user"

    @models.permalink
    def get_absolute_url(self):
        return ('user_profile_public', (), {'username': self.user.username})

    @property
    def name(self):
        '''Returns the first existing name of: fullname of the profile, fullname of the linked worker, username.'''
        return self.fullname or (self.worker and self.worker.fullname) or self.user.username

    @property
    def get_researcher_id(self):
        if not hasattr(self, 'cached_researcher_id'):
            if self.worker:
                self.cached_researcher_id = self.worker.get_external_id("researcherid") or self.researcher_id
            else:
                self.cached_researcher_id = None
        return self.cached_researcher_id

    @property
    def summary(self):
        '''Returns a dictionary with info from the model.'''
        return {'fullname': self.fullname, 'researcher_id': self.get_researcher_id}

    def datatable_merge_json(self):
        person = self.worker or self
        user = self.user
        data = dict(enumerate([self.get_absolute_url_html(),
                               user.username,
                               person.lastname,
                               person.firstname,
                               person.middlename,
                               user.email,
                               user.date_joined.strftime("%d.%m.%Y"),
                               user.last_login.strftime("%d.%m.%Y")
        ]))
        data['DT_RowId'] = self.id
        return data

    @staticmethod
    def get_datatable_merge_column_names():
        return ("ID",
                u"Пользователь",
                u"Фамилия",
                u"Имя",
                u"Отчество",
                u'Email',
                u"Дата регистрации",
                u"Последнее посещение",
        )

    def registration_completed(self):
        # old users have empty registration_status, but should have a worker assigned
        if self.registration_status == 'completed' \
           or (self.worker and (self.registration_status is None or self.registration_status == '')):
            return True
        return False

    def get_points_for_default_formula(self):
        from pmodel.views import compute_total_points_for_user
        value = compute_total_points_for_user(self.user)
        if value is None:
            return None
        if value is False:
            return 'нажмите, чтобы посчитать'
        return '%.2f' % value

class UnknownWorker(ManAbstract):
    """UnknownWorker is used in worker disambiguation process as an unrecognized author indicator"""
    id = None

    def __init__(self, lastname=None, firstname=None, middlename=None, **kwargs):
        self.lastname = lastname
        self.firstname = firstname
        self.middlename = middlename

    @property
    def user(self):
        return None


class Worker(MyModel, WorkerAbstract):
    id = models.AutoField(primary_key=True, db_column="F_MAN_ID")
    lastname = models.CharField(u"Фамилия", max_length=255, db_column="F_MAN_NAMEL")
    firstname = models.CharField(u"Имя", max_length=255, db_column="F_MAN_NAMEF", null=True, blank=True)
    middlename = models.CharField(u"Отчество", max_length=255, db_column="F_MAN_NAMEM", null=True, blank=True)
    birthdate = models.DateField(u"Дата рождения", db_column="F_MAN_BIRTH", blank=True, null=True)
    departments = models.ManyToManyField(to="organizations.Department", related_name="workers", through="Employment")
    creator = models.ForeignKey(to="auth.User", related_name="workers_added", db_column="F_MAN_USER", null=True, blank=True)
    created_at = models.DateTimeField(null=True, blank=True, db_column="F_MAN_CREATE", auto_now_add=True)
    updated_at = models.DateTimeField(null=True, blank=True, db_column="F_MAN_UPDATE", auto_now=True)
    institution = models.CharField(u"Место работы", max_length=512, db_column="F_MAN_INSTITUTION", null=True, blank=True)

    objects = WorkerManager()
    objects_unlinked = UnlinkedWorkerManager()
    objects_ordered_by_articles = WorkerManagerOrderedByArticles()

    accusative = u"сотрудника"

    class Meta:
        db_table = "MAN"
        verbose_name = u"Сотрудник"
        verbose_name_plural = u"Сотрудники"
        ordering = ("lastname", "firstname", "middlename", "birthdate")

    def __unicode__(self):
        return self.fullname

    @models.permalink
    def get_absolute_url_profile(self): # needs a lot of sql queries to run
        if self.username:  # if worker has a profile, show link to the profile page
            return ('user_profile_public', (), {'username': self.username})
        else:
            return ('workers_worker_detail', (), {'worker_id': self.id})

    @models.permalink
    def get_absolute_url(self):
        return ('workers_worker_detail', (), {'worker_id': self.id})

    def get_fullname(self, initials=False):
        # if profile.fullname is the full version of self.fullname, display profile fullname
        # Disabled because too slow - too many database hits for nothing
        # if not initials: # full name is requested
            # try:
                # profile = self.profile
            # except:
                # pass
            # else: # profile exists
                # if profile and profile.firstname.startswith(self.firstname) and profile.middlename.startswith(self.middlename) and profile.lastname == self.lastname:
                    # return self.profile.fullname
        return super(Worker, self).get_fullname(initials)

    fullname = property(get_fullname)

    def get_activities(self, limit=False, summary=False, clickable_summary=False, years=None, no_duplicates=False):
        ''' Returns full list of works and activities of the worker.
            if @limit is specified, return this number of latest activities.
            years - filter activities by the given year range.
        '''
        return get_activities(self, limit=limit, summary=summary, clickable_summary=clickable_summary, years=years, no_duplicates=no_duplicates)

    activities = property(get_activities)

    def get_activities_prefetched(self, limit=False, summary=False, years=None):
        return get_activities(self, limit, summary, years, prefetched=True)

    activities_prefetched = property(get_activities_prefetched)

    def get_activities_bool(self, limit=False, summary=False, years=None):
        return get_activities(self, limit, summary, years, bool_only=True)

    activities_existence = property(get_activities_bool)

    @property
    def has_activities(self):
        activities = self.activities_existence
        return True in activities.values()

    def get_activities_last_added(self, limit=None):
        ''' Returns full list of works and activities added by the worker sorted by date.
            if @limit is not None, return this number of latest activities.
        '''
        return get_activities_last_added([self], limit)
    activities_last_added = property(lambda self: self.get_activities_last_added(20))

    @property
    def activities_latest_added(self):
        '''Return dictionary or activities added by worker or someone else to worker's page. Ordered by id descending.'''
        return get_activities(self, limit=5, recent_type="id")

    @property
    def activities_summary(self):
        return self.get_activities(summary=True)

    @property
    def activities_clickable_summary(self):
        return self.get_activities(summary=True, clickable_summary=True)


    def get_external_id(self, system_name):
        '''Returns worker's identifier in specified external system. Search predefined list of systems before user supplied system's name.'''
        try:
            return self.external_ids.get(worker=self, system__code=system_name).userid
        except Exception:
            try:
                return self.external_ids.get(worker=self, system_other=system_name).userid
            except Exception:
                pass
        return None

    @property
    def rinc_id(self):
        return self.get_external_id('elibrary')

    def get_coauthors(self, limit=False):
        ''' Returns coauthors of the worker.
            Limit specifies maximum number of objects.
            Sorted by number of coauthorships (articles and books).
        '''
        articles = self.articles.all().prefetch_related("authors")
        books = self.books.all().prefetch_related("authors")
        coauthors_articles = chain(*[list(i.authors.all()) for i in articles])
        coauthors_books = chain(*[list(i.authors.all()) for i in books])
        coauthors = chain(coauthors_articles, coauthors_books)
        counter = defaultdict(int)
        for author in coauthors:
            counter[author] += 1
        if self in counter:
            del counter[self]
        sorted_coauthors = sorted(counter.iteritems(), key=operator.itemgetter(0), reverse=False) # sort by name
        sorted_coauthors = sorted(sorted_coauthors, key=operator.itemgetter(1), reverse=True)
        if limit:
            sorted_coauthors = sorted_coauthors[:limit]
        sorted_coauthors = [item for item, count in sorted_coauthors]
        Worker.precalculate_usernames(sorted_coauthors)
        return sorted_coauthors

    coauthors = property(get_coauthors)

    @property
    def coauthors_limited(self):
        '''Just for convenient using in templates, where parameters can not be passed.'''
        return self.get_coauthors(limit=10)

    @property
    def employed(self):
        return self.institution or self.departments.exists() or self.employments_claimed.exists()

    @property
    def current_employments(self):
        '''Return current employments of the worker (excluding closed).'''
        employments_startdate = self.employments.filter(startdate__lte=datetime.datetime.now())
        employments = employments_startdate.filter(Q(enddate__isnull=True) | Q(enddate__gte=datetime.datetime.now()))
        return employments

    @property
    def current_main_employments(self):
        '''Return current main employments of the worker (excluding closed).'''
        employments = self.current_employments.filter(Q(parttime=False) | Q(parttime__isnull=True))
        if not employments:
            employments = self.current_employments
        return employments

    @property
    def main_position(self):
        employments = self.current_main_employments
        return employments[0].position if employments else None

    @property
    def current_main_first_department(self):
        employments = self.current_main_employments
        return employments[0].department if employments else None

    @property
    def current_main_first_root_department(self):
        department = self.current_main_first_department
        return department.roots[0] if department and department.roots else None

    @property
    def current_departments_ids(self):
        return self.employments.values_list('department', flat=True)

    @property
    def current_main_departments_ids(self):
        return self.current_main_employments.values_list('department', flat=True)

    @property
    def current_main_root_departments_ids(self):
        Department = get_model("organizations", "Department")
        main_departments_ids = self.current_main_departments_ids
        main_departments = Department.objects.filter(id__in=main_departments_ids)
        roots = []
        for department in main_departments:
            roots.extend(department.roots)
        return list(set([root.id for root in roots]))

    def get_employments_by_year(self, year):
        start = datetime.date(year, 1, 1)
        end = datetime.date(year, 12, 31)
        return self.employments.exclude(startdate__gt=end).exclude(enddate__lt=start)

    def get_employments_by_years(self, years):
        employments_ids = []
        for year in years:
            employments_ids.extend(self.get_employments_by_year(year).values_list('id', flat=True))
        return self.employments.filter(id__in=employments_ids)

    @property
    def organization_str(self):
        return ", ".join(set(self.current_employments.values_list('department__organization__name', flat=True)))

    @property
    def organization_main_str(self):
        return ", ".join(set(self.current_main_employments.values_list('department__organization__name', flat=True))) or self.institution or ""

    @property
    def has_works(self):
        return self.articles.exists() or self.books.exists()

    def get_profile(self):
        try:
            profile = self.profile
        except Profile.DoesNotExist:
            profile = None
        return profile

    @property
    def user(self):
        profile = self.get_profile()
        return profile.user if profile else None

    def is_similar_to(self, worker):
        """Check that worker (right part) is similar to self (left, base part). Order matters."""
        return worker.is_similar_to_fullnames(get_all_fullnames(self))

    def is_similar_to_fullnames(self, fullnames):
        """Check that self (right part) is similar to fullnames (left, base part). Order matters."""
        worker_representations = get_all_named_representations(self)
        for fullname in fullnames:
            if Worker.objects.get_very_similar(fullname, worker_representations):
                return True
        return False

    def get_similar_workership(self, workerships):
        '''Returns a similar workership from the list to the worker. Similarity is understood in 'strict' sense, e.g. lastnames should match, and initials should expand. Also tries to swap lastnames and firstnames.'''
        workers = []
        for workership in workerships:
            # add workership.worker to workers
            worker = workership.worker
            if worker:
                worker.workership = workership # store to extract later
                workers.append(worker)
            # add original name to workers
            # it should be done, because original name might not be similar to linked worker's name or alias, and there can be no linked worker ('free' author)
            # make a ManAbstract instance from workership original_name
            original_name = getattr(workership, "original_name", None)
            if original_name:
                from publications.utils import split_authors
                names = split_authors(original_name)
                if names:
                    name = names[0]
                    worker = ManAbstract()
                    worker.lastname, worker.firstname, worker.middlename = name[0], name[1], name[2]
                    worker.workership = workership # store to extract later
                    workers.append(worker)

        for name in self.names + self.names_swapped:
            similars = self._default_manager.get_very_similar(name._asdict(), workers)
            if similars:
                return similars[0][0].workership # get first similar workership
        return None

    def is_managed_in_dissertation_council_by(self, user):
        '''Returns True if the worker participates in some dissertation council,
            and @user can edit that council.
        '''
        # get ids of councils in which @self has active participation
        council_ids = self.dissertation_councils_memberships.filter(
            startdate__lte=datetime.datetime.now()).filter(
            Q(enddate__isnull=True) | Q(enddate__gte=datetime.datetime.now())).values_list(
            'dissertation_council', flat=True)
        # return True if @user has a permission to edit any of the councils
        return PermissionWithLabel.objects.filter(
            codename='edit',
            approved=True,
            object_id__in=council_ids,
            user__id=user.id
        ).exists()

    def is_managed_by(self, user):
        from unified_permissions.models import AllManagedWorkers
        roles = ['is_representative_for']
        return AllManagedWorkers.objects.current().filter(user=user, worker=self, role_name__in=roles).exists()

    def get_departments_year_report_can_be_approved_in(self, user):
        Department = get_model("organizations", "Department")
        year = get_nearest_year()
        departments_ids = list(set(self.get_employments_by_year(year).values_list('department', flat=True)))
        departments = Department.objects.filter(id__in=departments_ids)
        return filter(lambda department: has_permission(user, "confirm_personal_reports", department), departments)

    def year_report_status(self, department):
        year = get_nearest_year()
        object_list = self.user.object_lists.filter(
            name=u"Годовой отчет", category=u"Отчеты", code=year, department=department).get_first_or_none()
        status = object_list.get_status_display() if object_list else u"Редактируется"
        return status

    @property
    def has_no_links(self):
        if self.get_profile():
            return False
        if self.has_activities:
            return False
        if get_related_objects(self) or get_linked_to_instance_objects(self):
            return False
        return True

    @property
    def names(self):
        '''Return a list of Name objects with all worker's names: his own name and all his aliases.'''
        return [Name(self.lastname, self.firstname, self.middlename)] + [Name(*item) for item in self.aliases.values_list('lastname', 'firstname', 'middlename')]

    @property
    def names_swapped(self):
        '''Return a list of Name objects with all worker's names with firstnames and lastnames swapped: his own name and all his aliases.'''
        return [Name(self.firstname, self.lastname, self.middlename)] + [Name(*item) for item in self.aliases.values_list('firstname', 'lastname', 'middlename')]


    @property
    def username(self):
        """Read username from/write username to self._username and return it

        Also, see precalculate_username
        """
        if not hasattr(self, "_username"):
            self._username = self.user and self.user.username
        return self._username

    @staticmethod
    def precalculate_usernames(list_of_workers):
        """For a list of workers precalculate their username property"""
        list_of_workers = filter(lambda x: not hasattr(x, "_username"), list_of_workers)
        if not list_of_workers:
            return

        ids = [i.id for i in list_of_workers]
        w2u = User.objects.filter(
                profile__worker_id__in=ids
              ).values_list(
                "profile__worker_id", "username"
              )
        w2u = dict(w2u)
        for worker in list_of_workers:
            worker._username = w2u.get(worker.id)

    def add_alias(self, lastname, firstname, middlename):
        if not lastname: # lastname must not be empty
            lastname = self.lastname
        alias, created = self.aliases.get_or_create(lastname=lastname, firstname=firstname, middlename=middlename)
        return alias, created

    def add_alias_from_name(self):
        '''Create an alias for the worker equal to his name.'''
        return self.add_alias(self.lastname, self.firstname, self.middlename)

    def add_alias_from_profile(self):
        '''Create an alias for the worker equal to his profile's name (if profile is linked).'''
        profile = self.get_profile()
        if profile:
            return self.add_alias(profile.lastname, profile.firstname, profile.middlename)
        else:
            return None, None

    def copy_name_from_profile(self):
        '''Copies name from profile, creating an alias with old name.'''
        profile = self.get_profile()
        if profile:
            lastname = profile.lastname or self.lastname # This is to avoid clearing worker's lastname, which is forbidden
            if (self.lastname, self.firstname, self.middlename) != (lastname, profile.firstname, profile.middlename): # if profile's name is already equal to worker's name, do nothing
                alias, created = self.add_alias_from_name()
                self.lastname, self.firstname, self.middlename = lastname, profile.firstname, profile.middlename
                self.save()
                # if there are aliases equal to new worker's name, delete them (to avoid a situation when worker's alias is equal to worker's name)
                self.aliases.filter(lastname=lastname, firstname=profile.firstname, middlename=profile.middlename).delete()
                return alias, created
        return None, None

    def citation_counts_dict(self):
        '''Return a list of dicts [{'system__full_name': 'Web of Science', 'value': 4}, {'system__full_name': 'Scopus', 'value': 10}] of total number of citations of each type, e.g. WOS, or Scopus.'''
        return self.citation_counts.values('system__full_name', 'value')

    @property
    def ext_articles_ids(self):
        '''Return ids of articles, that are found in external systems.'''
        return get_ext_articles_ids(self.id)

    @property
    def vak_articles(self):
        """ -> (list of Article)
            Return list of articles by worker that are published in journals belonging to VAK index.
        """
        from journals.models import JournalIndex
        vak_indexes = JournalIndex.objects.get_vak_indexes()
        return uniqify(self.articles.filter(journal__index_memberships__index__in=vak_indexes) if vak_indexes else [])

    @property
    def is_doctor(self):
        return self.degrees.filter(degree__is_degree_prior='N').exists() \
            or self.dissertations_defended.filter(phd='N').exists()

    @property
    def is_candidate(self):
        return self.degrees.filter(degree__is_degree_prior='Y').exists() \
            or self.dissertations_defended.filter(phd='Y').exists()

    @property
    def candidate_year(self):
        years = self.degrees.filter(degree__is_degree_prior='Y').values_list('year', flat=True) \
            or self.dissertations_defended.filter(phd='Y').values_list('year', flat=True)
        return min(years) if years else None

    @property
    def degree_domain(self):
        domain = None
        doctor_dissertation = self.dissertations_defended.getdefault(phd='N')
        if doctor_dissertation:
            domain = doctor_dissertation.domain
        else:
            doctor_degree = self.degrees.getdefault(degree__is_degree_prior='N')
            if doctor_degree:
                domain = doctor_degree.degree.branch
            else:
                candidate_dissertation = self.dissertations_defended.getdefault(phd='Y')
                if candidate_dissertation:
                    domain = candidate_dissertation.domain
                else:
                    candidate_degree = self.degrees.getdefault(degree__is_degree_prior='Y')
                    if candidate_degree:
                        domain = candidate_degree.degree.branch
        return domain

    @property
    def degree_text(self):
        if self.is_doctor:
            text = u"доктор наук"
        elif self.is_candidate:
            text = u"кандидат наук"
        else:
            text = ""
        return text

    @property
    def main_dissertation(self):
        return self.dissertations_defended.filter(phd='N').get_first_or_none() or self.dissertations_defended.filter(phd='Y').get_first_or_none()

    @property
    def degree_str(self):
        degree = ""
        try:
            degree = self.degrees.filter(degree__is_degree_prior='N')[0].degree.short_name
        except IndexError: # no doctor degree
            try:
                degree = self.degrees.filter(degree__is_degree_prior='Y')[0].degree.short_name
            except IndexError: # no candidate degree
                pass
        return degree

    @property
    def rank_str(self):
        rank = ""
        if self.ranks.filter(rank__name__istartswith=u'профессор').exists():
            rank = u"проф."
        elif self.ranks.filter(rank__name__istartswith=u'доцент').exists():
            rank = u"доц."
        return rank

    @property
    def rank_str_full(self):
        rank = ""
        if self.ranks.filter(rank__name__istartswith=u'профессор').exists():
            rank = u"профессор"
        elif self.ranks.filter(rank__name__istartswith=u'доцент').exists():
            rank = u"доцент"
        return rank

    @property
    def academic_str(self):
        academic_parts = []
        for academic_rank in self.ranks.filter(rank__is_academ='Y'):
            academy = academic_rank.academy
            if academic_rank.rank.name.startswith(u'член-корреспондент') \
                and self.ranks.filter(
                    rank__is_academ='Y',
                    academy=academy,
                    rank__name__istartswith=u'академик'):
                # if I'm member-corr and academic in the same academy, then
                # do not show that I'm member-corr
                continue
            academic_parts.append("%s %s" % (academic_rank.rank.shortname, academy.name))
        return ", ".join(academic_parts)

    @property
    def degree_rank_str(self):
        return ", ".join(filter(bool, (self.degree_str, self.rank_str, self.academic_str)))

    @property
    def degree_rank_str_with_comma(self):
        s = self.degree_rank_str
        return (', ' + s) if s else ''

    @property
    def degree_rank_org_str(self):
        return ", ".join(filter(bool, (self.degree_rank_str, self.organization_str)))

    @property
    def degree_rank_org_str_with_comma(self):
        s = self.degree_rank_org_str
        return (', ' + s) if s else ''

    def datatable_merge_json(self):
        data = dict(enumerate([self.get_absolute_url_html(),
                               self.lastname,
                               self.firstname,
                               self.middlename,
                               self.has_activities,
                               unicode(self.creator),
                               self.created_at.strftime("%d.%m.%Y")
        ]))
        data['DT_RowId'] = self.id
        return data

    @staticmethod
    def get_datatable_merge_column_names():
        return ("ID",
                u"Фамилия",
                u"Имя",
                u"Отчество",
                u'Наличие работ',
                u'Создатель',
                u"Дата создания",
        )

    @property
    def representatives(self):
        return self.user.representatives.filter(is_active=True)

    def publications_for_dissertation_council(self, years):
        publications = list(self.articles.filter(year__in=years)) + list(self.books.filter(year__in=years))
        for publication in publications:
            publication.coauthors = publication.get_workers_string(exclude_workers=[self])
            publication.citation_short = publication.short_info if publication.type == 'book' else publication.get_source()
        return publications

    @property
    def has_related_objects(self):
        return _has_related_objects(self,exceptions=['aliases'])


class WorkerAlias(MyModel, WorkerAbstract):
    id = models.AutoField(primary_key=True, db_column="F_MANALIAS_ID")
    worker = models.ForeignKey(to="Worker", related_name="aliases", verbose_name=u"Сотрудник", db_column="F_MAN_ID")
    lastname = models.CharField(u"Фамилия", max_length=255, db_column="F_MANALIAS_NAMEL")
    firstname = models.CharField(u"Имя", max_length=255, db_column="F_MANALIAS_NAMEF", null=True, blank=True)
    middlename = models.CharField(u"Отчество", max_length=255, db_column="F_MANALIAS_NAMEM", null=True, blank=True)
    validated = models.BooleanField(u"Проверено", db_column="F_MANALIAS_ISCORRECT")
    creator = models.ForeignKey(to="auth.User", related_name="worker_aliases_added", db_column="F_MANALIAS_USER", null=True, blank=True)
    created_at = models.DateTimeField(null=True, blank=True, db_column="F_MANALIAS_CREATE", auto_now_add=True)
    updated_at = models.DateTimeField(null=True, blank=True, db_column="F_MANALIAS_UPDATE", auto_now=True)

    objects = MyManager()
    objects_unlinked = UnlinkedWorkerAliasManager()

    class Meta:
        db_table = "MANALIAS"
        verbose_name = u"Псевдоним сотрудника"
        verbose_name_plural = u"Псевдонимы сотрудников"

    def __unicode__(self):
        return u"'%s', псевдоним сотрудника '%s'" % (self.fullname, self.worker.fullname)

class WorkerExternalIDSystem(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_MANSEXTIDSYSTEM_ID")
    code = models.CharField(u"Код системы", max_length=255, db_column="F_MANSEXTIDSYSTEM_CODE")
    name = models.CharField(u"Название системы", max_length=512, db_column="F_MANSEXTIDSYSTEM_NAME")
    url = models.CharField(u"Шаблон ссылки на страницу пользователя", max_length=512, db_column="F_MANSEXTIDSYSTEM_URL", null=True)
    userid_regex = models.CharField(u"Шаблон допустимого идентификатора пользователя", max_length=512, db_column="F_MANSEXTIDSYSTEM_IDMASK", null=True)
    userid_hint = models.CharField(u"Описание шаблона", max_length=512, db_column="F_MANSEXTIDSYSTEM_HINT", null=True)
    userid_example = models.CharField(u"Пример идентификатора", max_length=512, db_column="F_MANSEXTIDSYSTEM_EXAMPLE", null=True)

    class Meta:
        db_table = "MANSEXTIDSYSTEM"
        verbose_name = u"Идентификатор сотрудника во внешней системе"
        verbose_name_plural = u"Идентификаторы сотрудника во внешних системах"
        unique_together = ("code",)

    def __unicode__(self):
        return self.name

class WorkerExternalID(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_MANSEXTID_ID")
    system_other = models.CharField(u"Система", max_length=255, db_column="F_MANSEXTID_SYSTEM")
    userid = models.CharField(u"Идентификатор пользователя", max_length=1024, db_column="F_MANSEXTID_USERID")
    worker = models.ForeignKey(to="Worker", related_name="external_ids", verbose_name=u"Сотрудник", db_column="F_MAN_ID")
    system = models.ForeignKey(to="WorkerExternalIDSystem", related_name="workers", verbose_name=u"Зарегистрированный сотрудник", db_column="F_MANSEXTIDSYSTEM_ID", null=True, blank=True)

    @property
    def system_name(self):
        if self.system:
            return self.system.name
        return self.system_other

    def get_url(self):
        if self.system and self.system.url:
            return self.system.url.replace('{{userid}}', str(self.userid))
        return None

    class Meta:
        db_table = "MANSEXTID"
        verbose_name = u"Идентификатор сотрудника во внешней системе"
        verbose_name_plural = u"Идентификаторы сотрудника во внешних системах"
        unique_together = ("worker", "system")



class ProxyWorker(ManAbstract):
    """Object derived from Worker and WorkerAlias by means of some transformation

    This class mainly used by WorkerMananager's get similar functions
    """

    def __init__(self, original, transformation_type, lastname=None, firstname=None, middlename=None):
        """Initialize ProxyWorker with original person object and transformation type

        It's also possible to pass names if they differ from ones in the original

        original
            istance of either Worker or WorkerAlias
        transformation_type
            element from TRANSFORMATIONS enumeration
        """
        self.lastname = lastname or original.lastname
        self.firstname = firstname or original.firstname
        self.middlename = middlename or original.middlename
        self.original = original
        if isinstance(original, Worker):
            self.worker_type = WORKER_TYPES.WORKER
            self.worker = self.original
        elif isinstance(original, WorkerAlias):
            self.worker_type = WORKER_TYPES.WORKER_ALIAS
            self.worker = self.original.worker
        else:
            raise Exception("Unexpected worker type: %s" % type(w))
        self.transformation_type = transformation_type

        self.id = self.original.id
        self.updated_at = self.worker.updated_at

    def __unicode__(self):
        return self.fullname

    def __repr__(self):
        return "<ProxyWorker: %r (%r)>" % (self.fullname, self.worker)

    @staticmethod
    def get_all_proxy_workers(**filter_kwargs):
        """Fetch list of all ProxyWorker's (Worker's and WorkerAliases' and their transformations)

        filter_kwargs are passed to the filter() function
        of Worker's and WorkerAlias' object managers

        return list of ProxyWorker's
        """
        get_objects = lambda model: model.objects.filter(**filter_kwargs).select_related()
        get_transformations = lambda model: list(chain(*[w.get_transformations_list() for w in get_objects(model)]))
        l = get_transformations(Worker) + get_transformations(WorkerAlias)
        return l

class PositionCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_POSTKIND_ID")
    name = models.CharField(u"Название", max_length=1020, db_column="F_POSTKIND_NAME")

    class Meta:
        db_table = "POSTKIND"
        verbose_name = u"Группа должностей"
        verbose_name_plural = u"Группы должностей"

    def __unicode__(self):
        return "%s" % (unicode(self.name))


class Position(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_POST_ID")
    name = models.CharField(u"Название", max_length=1020, db_column="F_POST_NAME")
    shortname = models.CharField(u"Сокращенное название", max_length=255, db_column="F_POST_SHORTNAME")
    category = models.ForeignKey(to=PositionCategory, related_name="positions", db_column="F_POSTKIND_ID", null=True, blank=True)

    class Meta:
        db_table = "POST"
        verbose_name = u"Должность"
        verbose_name_plural = u"Должности"

    def __unicode__(self):
        return "%s" % (unicode(self.name))


class Employment(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_MANSPOST_ID")
    worker = models.ForeignKey(to="Worker", related_name="employments", verbose_name=u"сотрудник", db_column="F_MAN_ID")
    department = models.ForeignKey(to="organizations.Department", related_name="employments", verbose_name=u"подразделение", db_column="F_DEPARTMENT_ID")
    startdate = models.DateField(u"Дата начала работы", help_text=u"обязательно введите значение, пример: 21.01.2000", db_column="F_MANSPOST_BEGIN")
    enddate = models.DateField(u"Дата окончания работы", help_text=u"оставьте пустой, если продолжаете работать в этом подразделении", db_column="F_MANSPOST_END", null=True, blank=True)
    parttime = models.NullBooleanField(u"Работа по совместительству", db_column="F_MANSPOST_PART", blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="employments_added", db_column="F_MANSPOST_USER", null=True, blank=True)
    position = models.ForeignKey(to="Position", related_name="employments", verbose_name=u"должность", db_column="F_POST_ID", null=True, blank=True)

    objects = EmploymentManager()

    class Meta:
        db_table = "MANSPOST"
        verbose_name = u"Работа в подразделении"
        verbose_name_plural = u"Работы в подразделениях"
        unique_together = ("worker", "department", "startdate")
        ordering = ('-startdate', '-enddate')

    def __unicode__(self):
        return "%s works for %s" % (unicode(self.worker), unicode(self.department))

    @models.permalink
    def get_absolute_url(self):
        return ('workers_employment_detail', (), {'object_id': self.id})

    @property
    def position_lower(self):
        return unicode(self.position).lower()


class EmploymentClaim(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_MANSPOSTQ_ID")
    worker = models.ForeignKey(to="Worker", related_name="employments_claimed", verbose_name=u"сотрудник", db_column="F_MAN_ID")
    department = models.ForeignKey(to="organizations.Department", related_name="employments_claimed", verbose_name=u"подразделение", db_column="F_DEPARTMENT_ID")
    startdate = models.DateField(u"Дата начала работы", help_text=u"обязательно введите значение, пример: 21.01.2000", db_column="F_MANSPOSTQ_BEGIN")
    enddate = models.DateField(u"Дата окончания работы", help_text=u"оставьте пустой, если продолжаете работать в этом подразделении", db_column="F_MANSPOSTQ_END", null=True, blank=True)
    parttime = models.NullBooleanField(u"Работа по совместительству", db_column="F_MANSPOSTQ_PART", blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="employments_claimed", db_column="F_MANSPOSTQ_USER", null=True, blank=True)
    position = models.ForeignKey(to="Position", related_name="employments_claimed", verbose_name=u"должность", db_column="F_POST_ID", null=True, blank=True)

    class Meta:
        db_table = "MANSPOSTQ"
        verbose_name = u"Работа в подразделении"
        verbose_name_plural = u"Работы в подразделениях"
        unique_together = ("worker", "department", "startdate")
        ordering = ('-startdate', '-enddate')

    def __unicode__(self):
        return "%s claims to be a worker at %s" % (unicode(self.worker), unicode(self.department))

    @property
    def position_lower(self):
        return unicode(self.position).lower()

    def accept(self):
        '''Creates an Employment record, drops this claim and returns record created.'''
        emp = Employment.objects.create(worker=self.worker,
                                        department=self.department,
                                        position=self.position,
                                        startdate=self.startdate,
                                        enddate=self.enddate,
                                        parttime=self.parttime,
                                        creator=self.creator)
        emp.save()
        self.delete()
        return emp


def get_fullname(object, initials=False, comma=False):
    '''Returns last name + first name + middle name with left out blank fields for Worker object.

    If initials is true, only last name and  initials are returned.
    If comma is True, extra comma is added after lastname, e.g. Ivanov, I.A. Needed for meta tags in rector's format.
    '''
    fields = ("lastname", "firstname", "middlename",)
    kwargs = dict((i, getattr(object, i, None)) for i in fields)
    kwargs['initials'] = initials
    kwargs['comma'] = comma
    return get_fullname_raw(**kwargs)


class Academy(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_ACADEMY_ID")
    name = models.CharField(u"Название академии", max_length=255, db_column="F_ACADEMY_NAME")

    class Meta:
        db_table = u'ACADEMY'
        verbose_name = u"Академия"
        verbose_name_plural = u"Академии"

    def __unicode__(self):
        return self.name


class ManDegree(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_MANDEGREE_ID")
    degree = models.ForeignKey(to="Degree", related_name="man_degrees", verbose_name=u"Степень", db_column="F_DEGREE_ID")
    worker = models.ForeignKey(to="Worker", related_name="degrees", verbose_name=u"Сотрудник", db_column="F_MAN_ID")
    year = models.BigIntegerField(db_column="F_MANDEGREE_YEAR", blank=True, null=True)
    class Meta:
        db_table = u'MANDEGREE'
        verbose_name = u"Присуждение степени"
        verbose_name_plural = u"Присуждения степени"

class Degree(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEGREE_ID")
    name = models.CharField(max_length=255, verbose_name=u"Полное название", db_column="F_DEGREE_NAME", null=True, blank=True)
    short_name = models.CharField(max_length=255, verbose_name=u"Сокращенное название", db_column="F_DEGREE_SHORTNAME", null=True, blank=True)
    branch = models.ForeignKey(to="dissertations.Domain", related_name = u"degrees", verbose_name=u"Область науки", db_column="F_BRANCH_ID", null=True, blank=True)
    is_degree_prior = models.CharField(max_length=1, db_column="F_DEGREE_PRIOR", null=True, blank=True)
    class Meta:
        db_table = u'DEGREE'
        verbose_name = u"Степень"
        verbose_name_plural = u"Степени"

    def __unicode__(self):
        return self.name + ((u" в " + self.branch.name) if self.branch else "")


class ManRank(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_MANRANK_ID")
    department = models.ForeignKey(to="organizations.Department", related_name = u"man_ranks", verbose_name=u"Подразделение",
                                   db_column="F_DEPARTMENT_ID", null=True, blank=True)
    worker = models.ForeignKey(to="Worker", related_name="ranks", verbose_name=u"Сотрудник", db_column="F_MAN_ID")
    rank = models.ForeignKey(to="Rank", related_name="man_ranks", verbose_name=u"Звание", db_column="F_RANK_ID")
    academy = models.ForeignKey(to="Academy", related_name="man_ranks", verbose_name="Академия", db_column="F_ACADEMY_ID", null=True, blank=True)
    date = models.DateField(verbose_name="Дата присвоения", db_column="F_MANRANK_DATE", blank=True, null=True)
    branch_num = models.CharField(max_length=255, verbose_name="Номер специальности", db_column="F_MANRANK_BRANCHNUM", blank=True)
    class Meta:
        db_table = u'MANRANK'
        verbose_name = u"Звание сотрудника"
        verbose_name_plural = u"Звания сотрудника"

    def get_fullname(self):
        rank_type = self.rank
        name = rank_type.name + " "
        if rank_type.is_academ == "Y":
            return name + self.academy.name
        if rank_type.is_branch == "Y":
            return name + u"№ " + self.branch_num
        if rank_type.is_department == "Y":
            try:
                department_name = self.department.name
            except AttributeError:
                department_name_trunkated = ''
            else:
                words = department_name.split()
                department_name_trunkated = u" ".join(words[1:])
            return name + department_name_trunkated
        return name

    fullname = property(get_fullname)

class Rank(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_RANK_ID")
    name = models.CharField(max_length=255, db_column="F_RANK_NAME", blank=True)
    shortname = models.CharField(max_length=255, db_column="F_RANK_SHORTNAME", blank=True)
    is_department = models.CharField(max_length=1, db_column="F_RANK_DEP", blank=True)
    is_branch = models.CharField(max_length=1, db_column="F_RANK_BRANCH", blank=True)
    is_academ = models.CharField(max_length=1, db_column="F_RANK_ACADEM", blank=True)
    class Meta:
        db_table = u'RANK'
        verbose_name = u"Звание"
        verbose_name_plural = u"Звания"

    def __unicode__(self):
        return self.name


#
# Модель для представления, выбирающего все персональные отчеты сотрудников
#
class PersonalReport(MyModel):
    markedlist = models.ForeignKey(to="common.ObjectList", related_name = u"man_reports", verbose_name=u"Список работ",
                                   db_column="F_MARKEDLIST_ID", primary_key=True)
    department = models.ForeignKey(to="organizations.Department", related_name = u"man_personal_reports", verbose_name=u"Подразделение",
                                   db_column="F_DEPARTMENT_ID", null=True, blank=True)
    worker = models.ForeignKey(to="Worker", related_name="personal_reports", verbose_name=u"Сотрудник", db_column="F_MAN_ID")
    status = models.CharField(u"Статус", max_length=255, db_column="F_MARKEDLIST_SIGNTYPE", blank=True)
    date = models.DateField(verbose_name="Дата присвоения", db_column="F_MARKEDLIST_DATE", blank=True, null=True)
    creator = models.ForeignKey(to="auth.User", related_name="workers_reports_added", db_column="F_MARKEDLIST_USER", null=True, blank=True)

    class Meta:
        db_table = "V_PERSONAL_REPORTS"
        managed = False
        verbose_name = u"персональный отчёт"
        verbose_name_plural = u"персональные отчёты"


from journals.models import JournalRubric
class UserTopic(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_USERTOPIC_ID")
    user = models.ForeignKey(to="auth.User", related_name="topics", db_column="F_USERTOPIC_USER", null=True, blank=True)
    rubric = models.ForeignKey(to=JournalRubric, related_name='usersmembership', verbose_name=u'Рубрика', db_column='F_JOURNALRUB_ID', on_delete=models.PROTECT)

    class Meta:
        db_table = "USERTOPIC"
        verbose_name = u"область интересов пользователя"
        verbose_name_plural = u"области интересов пользователя"


from workers.admin import *
