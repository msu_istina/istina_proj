# -*- coding: utf-8 -*-
import logging
from django import template
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.core.cache import cache, get_cache
from workers.forms import EmploymentPositionForm

logger = logging.getLogger("workers_show")

register = template.Library()

default_cache = get_cache("default")
bigdata_cache = get_cache("bigdata")

@register.inclusion_tag('workers/worker_short.html', takes_context=True)
def show_worker_short(context, worker, no_links=0, show_activities=1):
    '''Displays a basic information about worker, including his latest activities.

    If no_links is True, don't add hyperlinks to other entities' pages.
    if show_activities is True, show 3 latest activities (otherwise show only summary).
    '''
    if context.get("fast"):
        return {"fast": True, "worker": worker}
    key = "show_worker_short.%s.%s" % (worker.id, no_links)
    tpl_context = default_cache.get(key)
    if tpl_context is None or context.get("cache_disabled") or tpl_context["latest_activities"] is None:
        if tpl_context is None:
            logger.warning("MISS (default): %s" % key)
        tpl_context = bigdata_cache.get(key)
        if tpl_context is None or context.get("cache_disabled") or tpl_context["latest_activities"] is None:
            if tpl_context is None:
                logger.warning("MISS (bigdata): %s" % key)
            tpl_context = calculate_worker_short_context(context, worker, no_links)
            try:
                default_cache.set(key, tpl_context, timeout=context.get('CACHE_TIME'))
            except:
                bigdata_cache.set(key, tpl_context, timeout=context.get('CACHE_TIME'))
        else:
            logger.warning("HIT (bigdata): %s" % key)
    else:
        logger.warning("HIT (default): %s" % key)
    if not show_activities:
        tpl_context["latest_activities"] = None
    return tpl_context


def calculate_worker_short_context(context, worker, no_links=0):
    latest_activities = worker.get_activities(limit=3)
    activities_summary = worker.activities_summary
    coauthors = worker.get_coauthors()  # need to get all coauthors to count them, and then take first 8 to fit one line
    coauthors_limit = 8  # 8 usually fit one line
    coauthors_show_etc = len(coauthors) > coauthors_limit  # show "и др."
    coauthors = coauthors[:coauthors_limit]
    CACHE_TIME = context.get('CACHE_TIME')
    activities_list_type = "latest"
    return {'worker': worker, 'latest_activities': latest_activities, 'activities_summary': activities_summary, 'coauthors': coauthors, 'coauthors_show_etc': coauthors_show_etc, 'no_links': no_links, 'CACHE_TIME': CACHE_TIME, 'activities_list_type': activities_list_type, }


@register.simple_tag
def render_fullname_from_form(worker_form):
    """Build fullname from cleaned data worker_form using nbsp if needed"""
    try:
        l, f, m = worker_form.get_names()
    except worker_form.FormIsntReady:
        return ""
    if len(f) <= 1 and len(m) <= 1:
        fmt = r"%s&nbsp;%s%s"
    else:
        fmt = r"%s %s %s"
    f, m = map(lambda x: len(x) == 1 and x + "." or x, (f, m))
    l, f, m = map(escape, (l, f, m))
    return mark_safe(" ".join((fmt % (l, f, m)).split()))


class SelectedWorkerNode(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        selected_wid = context['form']['wid'].value()
        try:
            selected_wid = int(selected_wid)
        except:
            pass
        if selected_wid is not None and context.get('similar_worker'):
            selected = (selected_wid == context['similar_worker'].worker.id)
            selected_unknown = False
        elif context.get('similar_workers'):
            selected = (context['similar_workers'][0] == 'first' and context.get("forloop") and context["forloop"]["first"])
            selected_unknown = (context['similar_workers'][0] == 'unknown')
        else:
            selected = selected_unknown = False
        context.update({"selected": selected, "selected_unknown": selected_unknown})
        output = self.nodelist.render(context)
        context.pop()
        return output


@register.tag
def populate_selected_worker(parser, token):
    """Populate selected variable within the block

    Syntax:
    {% populate_selected_worker %}
       use {{ selected }} where needed
    {% end_populate_selected_worker %}
    """
    nodelist = parser.parse(('end_populate_selected_worker',))
    parser.delete_first_token()
    return SelectedWorkerNode(nodelist)

@register.tag
def set_is_profile_owner(parser, token):
    """
    On some profile edit pages, e.g. edit avatar, email, password,
    there is no worker variable (though a user can actually have a worker, so users_worker != worker)
    so we should take it into account
    so there can be following options:
    (users_worker = user.get_profile().worker)
        users_worker != None, worker != None, users_worker == worker:
            a linked user edits his own profile
        users_worker != None, worker != None, users_worker != worker:
            a linked user edits another worker's profile
        users_worker != None, worker == None:
            a linked user edits his own profile, but this is an account page
            e.g. editing avatar, email, password
        users_worker == None, worker != None:
            a non-linked user edits another worker's profile
        users_worker == None, worker == None:
            a non-linked user edits his own profile
    """
    bits = token.contents.split()
    return IsProfileOwnerNode(bits[1], bits[2])


class IsProfileOwnerNode(template.Node):

    def __init__(self, user, worker):
        self.user = template.Variable(user)
        self.worker = template.Variable(worker)

    def render(self, context):
        user = self.user.resolve(context)
        try:
            worker = self.worker.resolve(context)
        except template.VariableDoesNotExist:
            worker = None
        # mind the order of checks, it has meaning! (see docstring)
        if worker is None:
            is_owner = True
        else:
            is_owner = worker == user.get_profile().worker
        context['is_profile_owner'] = is_owner
        return ''

@register.simple_tag
def get_employment_position_form(employment):
    return EmploymentPositionForm(instance=employment)
