# -*- coding: utf-8 -*-
import logging
from functools import wraps
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import available_attrs
from django.contrib.auth.decorators import login_required
from django.contrib import messages

logger = logging.getLogger("workers.decorators")


def worker_required(function):
    """
    Decorator for views that checks that a worker exists is logged in, redirecting
    to the link worker help page or login page if necessary.
    """
    def decorator(view_func):

        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            from workers.models import Worker
            from common.utils.user import get_worker
            worker_id = kwargs.pop("worker_id", None)
            if worker_id:
                worker = get_object_or_404(Worker, pk=worker_id)
            else:
                if not request.user.is_authenticated():
                    return login_required(view_func)(request, *args, **kwargs)
                worker = get_worker(request.user)
                if not worker:
                    messages.warning(request, u'Чтобы просмотреть запрашиваемую страницу, вам нужно сначала связать свой профиль с сотрудником. Для этого следуйте инструкции на данной странице, а затем вернитесь на <a href="%s" target="_blank">предыдущую страницу</a> еще раз.' % request.build_absolute_uri())
                    return redirect("/help/link_worker/")
            return view_func(request, worker, *args, **kwargs) # worker_id is removed from kwargs
        return _wrapped_view

    return decorator(function)