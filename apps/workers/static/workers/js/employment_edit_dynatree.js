
// =========================== Department tree =====================
$(function() {
    $("#org_tree").dynatree({
        title: "Organization tree",
        fx: { height: "toggle", duration: 200 },
        autoFocus: false, // Set focus to first child, when expanding or lazy-loading.
        checkbox: true,
        selectMode: 1, // 1:single, 2:multi, 3:multi-hier

        initAjax: {
            url: "/statistics/get_org_tree/",
            data: { "org_id": "0" } // data passed to the query
        },

        onSelect: function(select, node) {
            // Update value of #id_dep field with list of selected nodes
            var selNodes = node.tree.getSelectedNodes();

            var selKeys = $.map(selNodes, function(node){
                return node.data.key
            });
            var keys=selKeys.join(",");

            var path = "";
            var node = selNodes[0];
            while(node != null) {
                if( node.data.title ) {
                    if(path.length > 0) {
                        path = path + ", ";
                    }
                    path = path + node.data.title;
                }
                node = node.getParent();
            }

            $("#id_dep").val(keys);
            $("#id_dep").change();
            $("#chosen_department").text(selNodes[0].data.title);
            $("#chosen_department").text(path);
        },

        onClick: function(node, event) {
        },

        onKeydown: function(node, event) {
            if( event.which == 32 ) {
                node.toggleSelect();
                return false;
            }
        },

        onPostInit: function() {
          node = $("#org_tree").dynatree("getTree").getNodeByKey($("#id_dep").val());
          if (node) node.select();
        },

        // The following options are only required, if we have more than one tree on one page:
        cookieId: "dynatree-depid",
        idPrefix: "dynatree-depid-"
    });
    // end of dynatree
});
