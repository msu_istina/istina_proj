$(function() {
    $(".toggle_similar_workers_form").click(function(event) {
        event.preventDefault();
        $("#similar_workers_form, .toggle_similar_workers_form").toggle()
    });
    
    // add more forms to aliases formset
    $('#aliases_form .formset_add_form').click(function() { // FIXME add event.preventDefault()?
        formset_add_form('#aliases_form div.form_in_formset:last', 'form');
    });
    
});
