$(function() {
    var rank_options = $("#id_ordinary-branch_num, #id_ordinary-department").closest("div.field")
    var academy = $("#id_ordinary-academy").closest("div.field")
    var branch_num = $("#id_ordinary-branch_num").closest("div.field")
    var department = $("#id_ordinary-department").closest("div.field")

    function show_rank_fields() {
        academy.hide()
        $("#id_academy-branch_num").closest("div.field").hide()
        $("#id_academy-department").closest("div.field").hide()

        rank_options.hide()
        var option_selected = $("#id_ordinary-rank_type").val()
        if (option_selected == "branch_professor" || option_selected == "branch_docent") {
            branch_num.show()
        }
        if (option_selected == "department_docent" || option_selected == "department_professor") {
            department.show()
        }
    }

    show_rank_fields()
    $("#id_ordinary-rank_type").change(function(){
        show_rank_fields()
    });

    $(".degree_delete, .rank_delete").click(function(event) {
        event.preventDefault();
        var id = $(this).closest('ul.activity').find('input[type="hidden"]').val()
        if ($(this).attr('class') == "degree_delete inner") {
            delete_url = "/workers/degree/delete/" + id + "/"
            block = $("#degrees")
        } else {
            delete_url = "/workers/rank/delete/" + id + "/"
            block = $("#ranks")
        }       
        $.ajax({
            url: delete_url,
            dataType: "json",
            context: $(this),
            success: function() {
                $(this).closest('ul.activity').hide()
                if (!block.find(".activity:visible").length) 
                    block.hide()
            }
        })
    });
    
    $('#id_ordinary-date, #id_academy-date').datepicker({ changeMonth: true, changeYear: true, yearRange: '1930:2020' });
});
