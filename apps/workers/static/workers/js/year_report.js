$(function() {
    $("#sign").click(function(event) {
        if (!$("#sign_warning").is(":visible")) {
	        event.preventDefault();
	        $("#sign_warning").show();
        }
    });
    $("#disapprove").click(function(event) {
        if (!$("#disapprove_form").is(":visible")) {
	        event.preventDefault();
	        $("#disapprove_form").show();
	        $("#pdf_link").hide();
        }
        else {
	        if (!$("#id_comment").val().trim().length) {
		        event.preventDefault();
		        $("#disapprove_form label").css('color', 'red');
	        }
        }
    });
});
