function hide_department_tree() {
    document.getElementById('divDepSelect').style.display = "none"
}

function show_department_tree() {
	    document.getElementById('divDepSelect').style.display = "block";
}

$(function() {
    var employment_options = $("#id_startdate, #id_enddate, #id_parttime, #id_position").closest("div.field")
    var ext_institution_options = $("#id_institution").closest("div.field")
    var chosen_department = $(".chosen_department")
    var form = $("#add_employment_form")
    var errors = form.find("ul.errorlist")
    var new_organization_name = $("#id_new_organization_name").closest("div.field")
    var org_tree = $("#org_tree")
    var org_tree_div = $("#divDepSelect")

    // disable empty option in organization and departments
    $("#id_organization option:first-child, #id_parent_department option:first-child, #id_child_department option:first-child").attr("disabled", "disabled")

    new_organization_name.hide()

	    $("#id_organization").change(function(){
        // Reload dynatree with selected organization structure
        var option_selected = $("#id_organization option:selected")
        var org_tree = $("#org_tree").dynatree("getTree")

        show_department_tree()
        //ext_institution_options.hide()
        $("#org_tree").dynatree("option", "initAjax", {
            url: "/statistics/get_org_tree/",
            data: { "org_id": $("#id_organization").val() }
        })
        org_tree.reload()
        employment_options.hide()
        chosen_department.hide()
        $("#div_submit").hide()
    });


    $(".employment_delete").click(function(event) {
        event.preventDefault();
        var id = $(this).closest('ul.activity').find('input[type="hidden"]').val()
        $.ajax({
            url: "/workers/employment/delete/" + id + "/",
            dataType: "json",
            context: $(this),
            success: function() {
                $(this).closest('ul.activity').hide()
                if (!$("#employments").find(".activity:visible").length) // if there are no more employments
                    $("#employments").hide()
            }
        })
    });

    $(".employment_add_position").click(function(event) {
        event.preventDefault();
        $(this).hide().parent().find("form.employment_edit_position").show();
    });
    $("#id_startdate, #id_enddate").datepicker({ changeMonth: true, changeYear: true, yearRange: '1930:2020' });

    $("#id_dep").change(function() {
        if($(this).val() != "") {
            employment_options.show()
            chosen_department.show()
            $("#div_submit").show()
            hide_department_tree()
            ext_institution_options.hide()
        }
        else {
            employment_options.hide()
            chosen_department.hide()
        }
    })

    $("#ext_institution").click(function() {
        employment_options.hide()
        chosen_department.hide()
        hide_department_tree()
        ext_institution_options.show()
        $("#id_institution").change()
        return false
    })

    $("#id_institution").change(function() {
        var user_input = $(this).val()
        if(user_input.length >= 1) {
            employment_options.hide()
            chosen_department.hide()
            hide_department_tree()
            $("#div_submit").show()
        }
        else {
            $("#div_submit").hide()
        }
    })
    $("#id_institution").keypress( function() { $("#id_institution").change(); } )

    if (!errors.length) { // if the condition is false, some data has been entered, and there are errors. Show the full form.
        employment_options.hide()
        chosen_department.hide()
        if ($("#id_organization").val()) {
          $("#id_organization").change()
          $("#id_dep").change() // reload dynatree
        }
    }
    else {
        employment_options.show()
        $("#id_organization").change()
        $("#id_dep").change() // reload dynatree
        hide_department_tree()
    }
});
