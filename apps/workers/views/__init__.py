# -*- coding: utf-8; -*-
import logging

from actstream import action
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse, Http404, HttpResponseForbidden, HttpResponseServerError
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.utils import simplejson

from common.views import redirect_with_error
from common.utils.user import get_worker
from common.utils.autocomplete import autocomplete_helper
from common.utils.context import render_to_response
from common.utils.dates import extract_years_list, get_nearest_year
from common.utils.latex import latex_to_pdf
from common.utils.activities import _ACTIVITIES_TYPES

from common.models import ObjectListItem, ObjectList
from unified_permissions import has_permission
from workers.forms import *
from organizations.forms import *
from organizations.utils import is_representative_for_worker
from workers.forms import DegreeForm, OrdinaryRankForm, AcademyRankForm, RankForm, ReportSignForm
from workers.models import Degree, ManDegree, Rank, ManRank
from userprofile.views import DEFAULT_AVATAR_SIZE, GOOGLE_MAPS_API_KEY
from workers.utils import merge_workers, worker_year_report_export_to_pdf
from workers.decorators import worker_required
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect

from django.db import connection, transaction
from django.db.models import Q
from publications.models import Article
from pytils.translit import translify, detranslify

from profile import *

import datetime
logger = logging.getLogger("workers.views")


def worker_detail_short_ajax(request, worker_id):
    """Generate no-links 'inner' worker info for step3"""
    from workers.templatetags.workers_show import calculate_worker_short_context
    worker = get_object_or_404(Worker, pk=worker_id)
    context = calculate_worker_short_context({"CACHE_TIME": settings.CACHE_TIME}, worker, 1)
    return render(request, 'workers/worker_short_inner.html', context)


def worker_detail(request, worker_id):
    worker = get_object_or_404(Worker, pk=worker_id)

    similar_worker = None
    if not worker.user:
        similar_worker_id = request.GET.get('similarto', None)
        user_worker = get_worker(request.user)
        if similar_worker_id:
            try:
                similar_worker = get_object_or_404(Worker, pk=int(similar_worker_id))
            except:
                pass
        elif request.user.is_authenticated():
            names=[['','','']]
            if worker.lastname:
                names[0][0] = worker.lastname.strip()
            if worker.firstname:
                names[0][1] = worker.firstname.strip()[0]
            if worker.middlename:
                names[0][2] = worker.middlename.strip()[0]
            disambiguations = Worker.objects.get_disambiguation(names=names, count=100)
            if disambiguations:
                similar_workers_list = [x[0] for x in disambiguations[0]]
                for managed_worker in request.user.managed_workers.filter(worker__in=similar_workers_list, role_name="is_representative_for"):
                    if managed_worker.user and has_permission(managed_worker.user, "merge_worker", worker):
                        similar_worker = managed_worker.worker
                        break
    # it is a worker, or a special page (e.g. et.al)
    if worker.id in [218750, 498225]:
        status_text = u"служебная страница"
    elif "collaboration" in worker.fullname.lower():
        status_text = u"коллаборация"
    else:
        status_text = u"сотрудник"
    try: # the worker is linked to a registered user, redirect to user profile page
        return redirect(worker.profile)
    except Profile.DoesNotExist: # allow only unlinked workers to be merged to request.user's worker
        return render_to_response(request, "workers/worker_detail.html", {'worker': worker, 'DEFAULT_AVATAR_SIZE': DEFAULT_AVATAR_SIZE, 'status_text': status_text, 'similar_worker': similar_worker})


def worker_detail_in_style(request, worker_id, style):
    worker = get_object_or_404(Worker, pk=worker_id)
    articles_html = worker.articles.in_style_pure(style)
    theses_html = worker.articles.in_style_theses(style)
    books_html = worker.books.in_style(style)
    context = {'articles_html': articles_html, 'theses_html': theses_html, 'books_html': books_html, 'style': style}
    try: # the worker is linked to a registered user, show user profile page
        profile = worker.profile
        template_name = "userprofile/profile/public.html"
        context.update({'profile': profile, 'GOOGLE_MAPS_API_KEY': GOOGLE_MAPS_API_KEY, 'DEFAULT_AVATAR_SIZE': DEFAULT_AVATAR_SIZE})
    except Profile.DoesNotExist:
        template_name = "workers/worker_detail.html"
        context.update({'worker': worker})
    return render_to_response(request, template_name, context)


@worker_required
def worker_reports_index(request, worker):
    page_owner = get_worker(request.user) == worker
    year = get_nearest_year()
    departments_ids = set(worker.get_employments_by_year(year).values_list('department', flat=True))
    if len(departments_ids) == 1 and worker.user:
        department = Department.objects.filter(id__in=departments_ids)[0]
        year_report_status = worker.year_report_status(department)
    else:
        year_report_status = ""
    return render_to_response(request, "workers/worker_reports_index.html",
        {'worker': worker, 'page_owner': page_owner, 'year_report_status': year_report_status})


@worker_required
def worker_report_year_full(request, worker, years_str):
    activities = worker.get_activities(years=extract_years_list(years_str))
    return render_to_response(request, "workers/worker_report_year_full.html", {'worker': worker, 'activities': activities, 'years_str': years_str})


@worker_required
def worker_report_year_choose_department(request, worker, years_str):
    years = extract_years_list(years_str)
    departments_ids = list(set(worker.get_employments_by_years(years).values_list('department', flat=True)))
    departments = Department.objects.filter(id__in=departments_ids)
    is_owner = worker.user == request.user
    if is_owner:
        departments_allowed = departments
    else:
        # this should be a department manager of year reports
        departments_allowed = worker.get_departments_year_report_can_be_approved_in(request.user)
        if not departments_allowed:
            return redirect_with_error(request, u"У вас нет прав просмотра годового отчета данного пользователя.")
        if not worker.user:
            return redirect_with_error(request, u"К сожалению, %s не зарегистрирован в системе как пользователь. В настоящий момент годовой отчет может быть заполнен только для зарегистрированных пользователей." % worker)

    if len(departments_allowed) == 1:
        return redirect('workers_worker_report_year', worker_id=worker.id, years_str=years_str, department_id=departments_allowed[0].id)

    for department in departments_allowed:
        department.year_report_status = worker.year_report_status(department)

    return render_to_response(request, "workers/worker_report_year_choose_department.html", {'years_str': years_str, 'worker': worker, 'departments': departments_allowed})

@login_required
@worker_required
def worker_report_year(request, worker, years_str, department_id, on_behalf=None):
    department = get_object_or_404(Department, pk=department_id)
    activities = worker.get_activities(years=extract_years_list(years_str), no_duplicates=True)
    is_owner = worker.user == request.user
    can_approve = has_permission(request.user, "confirm_personal_reports", department)
    object_list = ObjectList.objects.filter(
        name=u"Годовой отчет", category=u"Отчеты", code=years_str, user=worker.user, department=department).get_first_or_none()
    can_edit = (is_owner and (not object_list or object_list.can_be_edited)) \
               or (can_approve and on_behalf)



    if not worker.current_employments.filter(department=department_id).exists():
        logger.error(u'Year report: Worker have no employment entry   year=%s worker=%s department=%s"' % (years_str, str(worker.id), str(department.id)))
        return HttpResponseRedirect(worker.get_full_url())




    #object_list, _ = ObjectList.objects.get_or_create(
    #    name=u"Годовой отчет", category=u"Отчеты", code=years_str, user=user, department=department)
    # clean the current object list
    is_first_report_opening = False
    if not object_list:
        is_first_report_opening = True
        try:
            object_list, _ = ObjectList.objects.get_or_create(
                name=u"Годовой отчет", category=u"Отчеты", code=years_str, user=worker.user, department=department)
        except Exception as e:
            if "unique constraint" in e.message.message:
                try:
                    object_list, _ = ObjectList.objects.get_or_create(
                        name=u"Годовой отчет", category=u"Отчеты", code=years_str, user=worker.user, department=department)
                except Exception as e:
                    messages.error(request, u"Произошла ошибка при создании страницы отчета.")
                    logger.error(u'Year report: Unable to get_or_create year report 2 times at least year=%s worker=%s department=%s"' % (years_str, str(worker.id), str(department.id)))
                    return HttpResponseRedirect(request.META.get('PATH_INFO'))


    #object_list.set_status(user, "SIGN",
    if not is_owner:
        if not can_approve:
            return redirect_with_error(request, u"У вас нет прав просмотра годового отчета данного пользователя по данному подразделению.")
        # only admins here
        if not worker.user:
            return redirect_with_error(request, u"К сожалению, %s не зарегистрирован в системе как пользователь. В настоящий момент годовой отчет может быть заполнен только для зарегистрированных пользователей." % worker)
        if not on_behalf:
            info_message = None
            if not object_list:
                info_message = u'Пользователь еще не сформировал свой отчёт.'
            elif object_list.can_be_edited:
                info_message = u'Отчёт еще не подписан пользователем.'

            if info_message:
                info_message += u' Вы можете <a href="%s">Подать отчёт за сотрудника</a>.' \
                                % reverse("workers_worker_report_year_onbehalf", args=(worker.id, years_str, department_id))
                messages.info(request, info_message)
        else:
            messages.info(request, u'Вы редактируете отчет пользователя <strong>%s</strong>' % worker)
    if can_edit:
        initially_checked = object_list.items.all().values('object_id', 'content_type') if object_list else []
        for key, subactivities in activities.items():
            for activity in subactivities:
                if is_first_report_opening:
                    objectitem = ObjectListItem.objects.get_or_create(object_list=object_list, object_id=activity.id,
                                                class_name=ContentType.objects.get_for_model(activity).name,
                                                content_type=ContentType.objects.get_for_model(activity),
                                                title=activity.title)
                    activity.check_form = ReportSignForm(
                        activity=activity, user=request.user, prefix="%s%d" % (key, activity.id), object_list=object_list, check_all_as_first_open=True)
                else:
                    try:
                        objectitem = ObjectListItem.objects.get(object_list=object_list, object_id=activity.id,
                                                class_name=ContentType.objects.get_for_model(activity).name,
                                                content_type=ContentType.objects.get_for_model(activity),
                                                title=activity.title)
                        activity.check_form = ReportSignForm(
                            activity=activity, user=request.user, prefix="%s%d" % (key, activity.id), object_list=object_list, initially_checked=initially_checked)
                    except:
                        activity.check_form = ReportSignForm(
                            activity=activity, user=request.user, prefix="%s%d" % (key, activity.id), object_list=object_list)


    elif object_list:
        filter_activities_in_object_list(activities, object_list)
    else:
        pass
    disapprove_form = ReportDisapproveForm() if can_approve else None
    if is_first_report_opening:
        object_list.set_status(request.user, "EDIT")
    return render_to_response(request, "common/activities_edit.html", {'worker': worker, 'is_owner': is_owner, 'can_edit': can_edit,
        'activities': activities, 'years_str': years_str, 'department': department, 'can_approve': can_approve, 'object_list': object_list, 'disapprove_form': disapprove_form})



from django.template.defaulttags import register
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@login_required
def check_project(request, project, worker, year=None, department_id=None):
    from sqlreports.views import run_report, getdropdownlist, get_report_params, generate_adata
    from sqlreports.models import SQLReport, SqlRepPar
    from django.core import signing

    old_level = messages.get_level(request)
    messages.set_level(request, 1000000)

    signer = signing.TimestampSigner(salt=generate_adata(request))
    report = SQLReport.objects.get(code='projects_errors_list')

    default_values, missed_params = get_report_params(request, report, signer)
    if request.user.is_superuser and 'user_id' in request.GET:
        default_values['user_id'] = request.GET.get('user_id')
    default_values['sign_type'] = u'0';
    #report.sql += "and p.f_project_id =" +  str(project.id);
    results = run_report(report, default_values, signer);
    messages.set_level(request, old_level)

    # TODO: PDF





@login_required
@worker_required
def worker_report_year_check(request, worker, years_str, department_id, on_behalf=None):
    department = get_object_or_404(Department, pk=department_id)
    activities = worker.get_activities(years=extract_years_list(years_str), no_duplicates=True)
    is_owner = worker.user == request.user
    can_approve = has_permission(request.user, "confirm_personal_reports", department)
    object_list = ObjectList.objects.get(
        name=u"Годовой отчет", category=u"Отчеты", code=years_str, user=worker.user, department=department)
    can_edit = (is_owner and (not object_list or object_list.can_be_edited)) \
               or (can_approve and on_behalf)
    #object_list, _ = ObjectList.objects.get_or_create(
    #    name=u"Годовой отчет", category=u"Отчеты", code=years_str, user=user, department=department)
    # clean the current object list
    is_first_report_opening = False


    #object_list.set_status(user, "SIGN",
    if not is_owner:
        if not can_approve:
            return redirect_with_error(request, u"У вас нет прав просмотра годового отчета данного пользователя по данному подразделению.")
        # only admins here
        if not worker.user:
            return redirect_with_error(request, u"К сожалению, %s не зарегистрирован в системе как пользователь. В настоящий момент годовой отчет может быть заполнен только для зарегистрированных пользователей." % worker)
        if not on_behalf:
            info_message = None
            if not object_list:
                info_message = u'Пользователь еще не сформировал свой отчёт.'
            elif object_list.can_be_edited:
                info_message = u'Отчёт еще не подписан пользователем.'

            if info_message:
                info_message += u' Вы можете <a href="%s">Подать отчёт за сотрудника</a>.' \
                                % reverse("workers_worker_report_year_onbehalf", args=(worker.id, years_str, department_id))
                messages.info(request, info_message)
        else:
            messages.info(request, u'Вы редактируете отчет пользователя <strong>%s</strong>' % worker)


    filter_activities_in_object_list(activities, object_list)

    check_results_text = {}

    #here we should create dictionary {'object_id' : [text_for_showing, link_for_check_block]}
    for key, subactivities in activities.items():
        for activity in subactivities:
            if key == 'projects':
                check_project(request, project=activity, worker=worker, department_id=department_id, year=str(years_str));
                check_results_text[activity.id] = 'Custom text here for object + &lt;a href=&quot;' + activity.get_full_url() + '&quot;&gt; Link &lt;/a&gt;';
                #check_results_text[activity.id] = 'Custom text here for object + <a href="' + activity.get_full_url() + '"> Link </a>';

    disapprove_form = ReportDisapproveForm() if can_approve else None
    return render_to_response(request, "common/activities_check.html", {'worker': worker, 'is_owner': is_owner, 'can_edit': can_edit,
        'activities': activities, 'years_str': years_str, 'department': department, 'can_approve': can_approve, 'object_list': object_list, 'disapprove_form': disapprove_form,
                                                                        'check_results_text' : check_results_text})







@login_required
@worker_required
def worker_report_year_sign(request, worker, years_str, department_id, on_behalf=None):
    department = get_object_or_404(Department, pk=department_id)
    activities = worker.get_activities(years=extract_years_list(years_str), no_duplicates=True)
    success = True
    user = worker.user
    if user != request.user and not has_permission(request.user, "confirm_personal_reports", department):
        return redirect_with_error(request, u"У Вас недостаточно прав для подписи отчёта данного пользователя.")
    if not user:
        return redirect_with_error(request, u"К сожалению, %s не зарегистрирован в системе как пользователь. В настоящий момент годовой отчет может быть заполнен только для зарегистрированных пользователей." % worker)

    object_list = ObjectList.objects.filter(name=u"Годовой отчет", category=u"Отчеты", code=years_str, user=user, department=department).get_first_or_none()
    if object_list is None:
        messages.error(request, u"Отчет сотрудника %s для подразделения %s не найден." % (worker, department))
        success = False

    if success:
        filter_activities_in_object_list(activities, object_list)
        attachment = worker_year_report_export_to_pdf(user, worker, years_str, activities, object_list)
        object_list.set_status(user, "SIGN", pdf_url=attachment.get_full_url())
        messages.success(request, u"Отчет подписан и сохранен.")
    return redirect("workers_worker_reports_index", worker_id=worker.id)


@login_required
@worker_required
def worker_report_year_get_pdf(request, worker, years_str, department_id, on_behalf=None):
    department = get_object_or_404(Department, pk=department_id)
    activities = worker.get_activities(years=extract_years_list(years_str), no_duplicates=True)
    success = True
    user = worker.user
    if user != request.user and not has_permission(request.user, "confirm_personal_reports", department):
        return redirect_with_error(request, u"У Вас недостаточно прав для подписи отчёта данного пользователя.")
    if not user:
        return redirect_with_error(request, u"К сожалению, %s не зарегистрирован в системе как пользователь. В настоящий момент годовой отчет может быть заполнен только для зарегистрированных пользователей." % worker)
    try:
        object_list = ObjectList.objects.get(
            name=u"Годовой отчет", category=u"Отчеты", code=years_str, user=user, department=department)
    except ObjectList.DoesNotExist:
        messages.error(request, u"Ошибка при генерации печатной формы отчета.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))



    if success:
        filter_activities_in_object_list(activities, object_list)
        pdf = worker_year_report_export_to_pdf(user, worker, years_str, activities, object_list, isTempReport=True)
        response = HttpResponse(pdf, mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=' + "worker_year_report_%d_%d_%s.pdf" % (worker.id, object_list.department.id, years_str)
        return response
    return redirect("workers_worker_reports_index", worker_id=worker.id)



@login_required
@worker_required
def worker_report_year_update_marked_list(request, worker, years_str, department_id, on_behalf=None):
    if request.method != 'POST':
        return HttpResponseServerError("Request method is not applied. ")

    user = worker.user
    department = get_object_or_404(Department, pk=department_id)
    activities = worker.get_activities(years=extract_years_list(years_str), no_duplicates=True)
    object_id = request.POST.get("changed_object_id", "0")
    if user != request.user and not has_permission(request.user, "confirm_personal_reports", department):
        return redirect_with_error(request, u"У Вас недостаточно прав для подписи отчёта данного пользователя.")
    if not user:
        return redirect_with_error(request, u"К сожалению, %s не зарегистрирован в системе как пользователь. В настоящий момент годовой отчет может быть заполнен только для зарегистрированных пользователей." % worker)

    try:
        object_list = ObjectList.objects.get(
            name=u"Годовой отчет", category=u"Отчеты", code=years_str, user=user, department=department)
    except ObjectList.DoesNotExist:
        return HttpResponseServerError(simplejson.dumps({'object_id': object_id}))


    checked = request.POST.get("checked")
    try:
        if checked == "false":
        # checked -- статус чекбокса до нажатия
        # установили галку и надо добавить эту запись в отчет
        # здесь мы работаем с состоянием чекбокса до нажатия
#            res = any(getattr(worker, key).filter(pk=int(0)).exists()
#                      for key in _ACTIVITIES_TYPES
#                      )


            for key, subactivities in activities.items():
                for activity in subactivities:
                    if activity.id == int(object_id):
                        objectitem = ObjectListItem.objects.get_or_create(object_list=object_list, object_id=activity.id,
                                                class_name=ContentType.objects.get_for_model(activity).name,
                                                content_type=ContentType.objects.get_for_model(activity),
                                                title=activity.title)
        else:
        # убрали галку, надо удалить объект из списка
            objectitem = ObjectListItem.objects.get(object_list=object_list, object_id=object_id)
            objectitem.delete()
        return HttpResponse(simplejson.dumps({'object_id': object_id, 'checked' : checked}), content_type='application/javascript')
    except Exception:
        logger.error(u'ActivitiesEdit: Unable to change object state in marked list: marked_list_id=%s, object_id=%s"' % (object_list.id, object_id))
        return HttpResponseServerError(simplejson.dumps({'object_id': object_id}))

    logger.error(u'ActivitiesEdit: Unable to change object state in marked list: marked_list_id=%s, object_id=%s"' % (object_list.id, object_id))
    return HttpResponseServerError(simplejson.dumps({'object_id': object_id}))







@login_required
@worker_required
def worker_report_year_set_status(request, worker, years_str, department_id, status):
    department = get_object_or_404(Department, pk=department_id)
    object_list = ObjectList.objects.filter(
        name=u"Годовой отчет", category=u"Отчеты", code=years_str, user=worker.user, department=department).get_first_or_none()
    if not object_list:
        return redirect_with_error(request, u"Отчет еще не подписан пользователем.")
    if not has_permission(request.user, "confirm_personal_reports", department):
        return redirect_with_error(request, u"У вас нет прав просмотра годового отчета данного пользователя по данному подразделению.")

    disapprove_form = ReportDisapproveForm(request.POST)
    if disapprove_form.is_valid():
        comment = disapprove_form.cleaned_data['comment']
    else:
        comment = ""
    object_list.set_status(request.user, status, comment)
    new_status_message = u"утвержден" if status == "CONFIRM" else u"отправлен на доработку"
    messages.success(request, u"Отчет %s." % new_status_message)
    return redirect("workers_worker_reports_index", worker_id=worker.id)


def filter_activities_in_object_list(activities, object_list):
    checked = object_list.items.all().values_list('object_id', 'content_type')
    for key, subactivities in activities.items():
        activities_to_remove = []
        for activity in subactivities:
            activity_args = (activity.id, ContentType.objects.get_for_model(activity).id)
            if not activity_args in checked:
                activities_to_remove.append(activity)
        for activity in activities_to_remove:
            subactivities.remove(activity)

@login_required
def worker_last_published(request):
    context = {'profile': request.user.get_profile(), }
    return render(request, "workers/worker_last_activities.html", context)


def autocomplete_search_workers(request):
    '''Server side for jquery autocomplete
    Returns objects of specified type matching query term.
    '''
    term = request.GET.get('term', '')
    fields = []
    limit = 10
    if len(term) >= 2:
        items = autocomplete_helper(Worker, "lastname", term, limit)
        fields = [{'label': item.fullname, 'value': item.get_fullname(initials=True)} for item in items]
        fields = uniqify(fields, lambda i: i['value'])
    return HttpResponse(simplejson.dumps(fields), content_type='application/javascript')


def worker_list_raw(request):
    '''Generates a page with links to lists of workers (100 per page) for automatic parsing.'''
    worker_count = Worker.objects.count()
    workers_per_page = 100
    num_pages = (worker_count - (worker_count % workers_per_page)) / workers_per_page + 1
    links = [str(workers_per_page * i + 1) + "-" + str(workers_per_page * (i + 1)) for i in range(num_pages - 1)] # ['1-100', '101-200', '201'-'300']
    links.append(str(workers_per_page * (num_pages - 1) + 1) + '-' + str(worker_count)) # last link, e.g. ['301-355']
    return render_to_response(request, "workers/worker_list_raw.html", {'links': links})


def worker_list_raw_page(request, page_index):
    '''Generates a page (the page number is specified in page_index, 1-based) with a list of 100 workers.'''
    worker_count = Worker.objects.count()
    workers_per_page = 100
    num_pages = (worker_count - (worker_count % workers_per_page)) / workers_per_page + 1
    page_index = int(page_index)
    if page_index > num_pages:
        raise Http404
    workers = Worker.objects.order_by('id')[workers_per_page * (page_index - 1):workers_per_page * page_index]
    return render_to_response(request, "workers/worker_list_raw_page.html", {'workers': workers})

def search_workers(request):
    if request.method == "POST":
        search_string = request.POST.get("worker_name", "")
    else:
        search_string = request.GET.get("s", "")
    if len(search_string.strip()) > 1:
        worker_list = Worker.objects.find_exact_fast(search_string)
        search_string_too_short = False
    else:
        worker_list = []
        search_string_too_short = True
    return render_to_response(request, "workers/worker_search_results.html", {'worker_list': worker_list, 'search_string':search_string,
                                                                              'search_string_too_short':search_string_too_short})

def merge_workers_request(request, worker_id):
    worker = get_object_or_404(Worker, pk=worker_id)
    similar_worker_id = request.GET.get('to',None)

    redirect_to = worker
    if request.method == "POST":
        user = request.user
        user_worker = get_worker(user)
        if has_permission(user, "merge_worker", worker):
            success, log_list = merge_workers(worker, user)
            fullname = worker.fullname
            description = "\n".join(log_list)
            if success:
                action.send(request.user, verb=u'привязал сотрудника к себе', target=user_worker, description=description)
                messages.success(request, u"Сотрудник %s успешно объединен с Вашим пользователем. Результаты операции (перепривязанные публикации и т.д.) Вы можете увидеть на своей странице." % fullname)
                redirect_to = user_worker
            else:
                action.send(request.user, verb=u'привязал сотрудника к себе (неудачно)', target=user_worker, description=description)
                messages.error(request, u"В процессе объединения сотрудников произошла ошибка. Администраторы системы уведомлены и свяжутся с Вами в ближайшее время.")
                logger.error(u"Ошибка в процессе привязки сотрудника %s к пользователю %s", worker_id, user.username)
        elif similar_worker_id:
            similar_worker = get_object_or_404(Worker, pk=similar_worker_id)
            similar_user = similar_worker.user
            if (request.user.is_superuser or is_representative_for_worker(user,similar_worker)) and has_permission(similar_user, "merge_worker", worker):
                success, log_list = merge_workers(worker, similar_user)
                fullname = worker.fullname
                description = "\n".join(log_list)
                if success:
                    action.send(request.user, verb=u'привязал сотрудника к себе', target=similar_worker, description=description)
                    messages.success(request, u"Сотрудник %s успешно объединен с пользователем %s. Результаты операции (перепривязанные публикации и т.д.) он сможет увидеть на своей странице." % (fullname, similar_user.username))
                    redirect_to = similar_worker
                else:
                    action.send(request.user, verb=u'привязал сотрудника к %s (неудачно)' % similar_user.username, target=similar_worker, description=description)
                    messages.error(request, u"В процессе объединения сотрудников произошла ошибка. Администраторы системы уведомлены и свяжутся с Вами в ближайшее время.")
                    logger.error(u"Ошибка в процессе привязки сотрудника %s к пользователю %s", worker_id, similar_user.username)
    return redirect(redirect_to)


@login_required
def similar_workers_list(request, worker_id=None):
    if not worker_id:
        user = request.user
        is_you = True
    else:
        worker = get_object_or_404(Worker,pk=worker_id)
        user = worker.user
        is_you = False
    # user = request.user if not worker_id else get_object_or_404(Worker,pk=worker_id).user
    similar_workers = Worker.objects.get_merge_candidates(user)

    return render_to_response(request, "workers/worker_similar_list.html", {'similar_workers': similar_workers, 'is_you':is_you})


@login_required
def lost_articles(request):
    user = request.user
    worker = get_worker(user)

    if worker:
        sql = """
        select f_article_id from v_lost_articles where f_man_id=%s
        order by f_article_id desc
        """

        cursor = connection.cursor()
        cursor.execute(sql, [worker.id])

        lost_ids = [item[0] for item in cursor]
    else:
        lost_ids = []
    lost_articles = Article.objects.filter(id__in=lost_ids)
    return render_to_response(request, "workers/lost_articles.html", {'worker': worker, 'lost_articles': lost_articles})


@user_passes_test(lambda u: u.is_superuser)
def su_as_user(request, username):
    from django.contrib.auth import SESSION_KEY
    from django.contrib.auth.models import User
    su_user = get_object_or_404(User, username=username, is_active=True)
    request.session[SESSION_KEY] = su_user.id
    return redirect("home")

@user_passes_test(lambda u: u.is_superuser)
def invalidate_cache(request, username):
    from common.utils import cache
    from django.contrib.auth.models import User

    user = get_object_or_404(User, username=username, is_active=True)
    cache.invalidate_user(username)
    return redirect("/profile/" + username + "/")

@login_required
def admin_set_password(request, username):
    from django.contrib.auth.models import User
    from actstream import action
    from workers.forms import AdminSetPasswordForm
    from django.core.mail import EmailMessage
    from organizations.utils import is_representative_for_user

    user = get_object_or_404(User, username=username, is_active=True)

    if not (request.user.is_superuser or is_representative_for_user(request.user, user)):
        messages.error(request, u'У Вас нет прав для изменения пароля данного пользователя.')
        return redirect("/profile/" + username + "/")

    admin = request.user
    try:
        admin_name = "%s (%s)" % (admin.get_profile().worker.fullname, admin.username)
    except Exception:
        admin_name = admin.username

    form = AdminSetPasswordForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        user.set_password(form.cleaned_data['password'])
        user.save()
        try:
            description = ''
            action.send(request.user, verb=u'изменил пароль', target=user, description=description)
            from_email = u"ИСТИНА <istina-noreply@istina.msu.ru>"
            message = EmailMessage(subject=u'Изменение пароля в системе ИСТИНА',
                                   body=u"Пользователь %s изменил Ваш пароль в системе ИСТИНА." % admin_name,
                                   from_email=from_email,
                                   to=[user.email],
                                   headers = {'Reply-To': admin.email})
            message.send()
        except:
            messages.warning(request, u"Не удалось отправить письмо.")
        return redirect("/profile/" + username + "/")

    return render_to_response(request, "workers/admin_set_password.html",
                              {'user_to_alter': user, 'form': form})


@login_required
def send_private_email(request, username):
    from django.contrib.auth.models import User
    from django.core.mail import EmailMessage

    user = get_object_or_404(User, username=username, is_active=True)
    sender = request.user
    def get_fullname(user, default=None):
        try:
            a_worker = get_worker(user)
            return a_worker.fullname
        except Exception:
            return default or u'Полное имя не указано'
    sender_fullname = get_fullname(sender)
    user_fullname = get_fullname(user, username)
    from_email = u"ИСТИНА (сообщения пользователей) <istina-messages@istina.msu.ru>"

    if request.method == 'POST' and request.POST.get('send', None):
        body = u'''
        Уважаемый(-ая) %s!

        Пользователь %s (%s) отправил Вам личное сообщение в системе
        ИСТИНА, текст которого приведен ниже. Мы не сообщали этому
        пользователю адрес Вашей электронной почты. Если Вы хотите
        ответить на это сообщение, просто пошлите ответ на данное
        письмо. В этом случае Вы отправите обычное электронное
        письмо со своего адреса на адрес пользователя %s.

        Ссылка на страницу пользователя %s: http://istina.msu.ru/profile/%s/
        Адрес отправителя сообщения: %s
        ---------------------------------------------------------
        ''' % (user_fullname,
               sender.username, sender_fullname,
               sender.username,
               sender.username, sender.username, sender.email)
        body = body + u'\n' + request.POST.get('body', u'')
        message = EmailMessage(subject=u'Личное сообщение от пользователя ИСТИНЫ',
                               body=body,
                               from_email=from_email,
                               to=[user.email],
                               headers = {'Reply-To': sender.email})
        try:
            message.send()
            message.to = [sender.email]
            messages.success(request, u'Ваше сообщение отправлено.')
            try:
                message.send()
                messages.info(request, u'Копия сообщения отправлена на Ваш адрес.')
            except Exception:
                messages.warning(request, u'При отправке КОПИИ сообщения на Ваш адрес возникла ошибка.')
            action.send(request.user, verb=u'отправил личное сообщение пользователю', target=user)
        except Exception:
            messages.warning(request, u'При отправке сообщения возникла ошибка.')
    if request.method == 'POST':
        return redirect("/profile/" + username + "/")
    # show email form
    return render_to_response(request,
                              "workers/send_private_email.html",
                              {'username': username,
                               'user_fullname': user_fullname,
                               'sender_fullname': sender_fullname,
                               })

@login_required
def home(request):
    profile = request.user.get_profile()
    template = "userprofile/profile/public.html"
    data = { 'profile': profile, 'GOOGLE_MAPS_API_KEY': GOOGLE_MAPS_API_KEY, 'DEFAULT_AVATAR_SIZE': DEFAULT_AVATAR_SIZE, 'new_home': True}
    return render(request, template, data)


@worker_required
def worker_report_vak_articles(request, worker):
    articles = worker.vak_articles
    template = "workers/vak_articles.html"
    return render(request, template, {"articles": articles, 'worker': worker})
