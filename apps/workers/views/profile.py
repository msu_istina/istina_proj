# -*- coding: utf-8; -*-

##
## Profile editing views
##

import logging

from actstream import action
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse, HttpResponseForbidden, Http404
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import simplejson

from common.utils.user import get_worker
from common.utils.context import render_to_response
from workers.forms import *
from organizations.forms import *
from workers.forms import DegreeForm, OrdinaryRankForm, AcademyRankForm, RankForm, ReportSignForm, EmploymentPositionForm
from workers.models import Degree, ManDegree, Rank, ManRank
from workers.decorators import worker_required
from unified_permissions import has_permission

import datetime
logger = logging.getLogger("workers.views")

def save_workers_alias_formset(user, worker, alias_formset):
    logger.debug("Worker aliases formset data: %s", getattr(alias_formset, "cleaned_data", alias_formset.data))
    for alias_form in alias_formset.deleted_forms:
        try:
            alias = alias_form.cleaned_data['id']
        except:
            alias = None
        if isinstance(alias, WorkerAlias):
            description = u'lastname: %s, firstname: %s, middlename: %s, validated: %s' % (alias.lastname, alias.firstname, alias.middlename, alias.validated)
            action.send(user, verb=u"удалил псевдоним сотрудника", action_object=alias, target=worker, description=description)
    aliases = alias_formset.save(commit=False)
    for alias in aliases:
        if alias.id: # this is edited instance, not a created one
            verb = u"отредактировал псевдоним сотрудника"
            log_verb = u"edited"
            try:
                old_alias = WorkerAlias.objects.get(id=alias.id)
                description = u'lastname: %s, firstname: %s, middlename: %s -> lastname: %s, firstname: %s, middlename: %s' % (old_alias.lastname, old_alias.firstname, old_alias.middlename, alias.lastname, alias.firstname, alias.middlename)
            except:
                description = u'lastname: %s, firstname: %s, middlename: %s' % (alias.lastname, alias.firstname, alias.middlename)
        else:
            verb = u"добавил псевдоним сотрудника в профиле"
            log_verb = u"added"
            description = u'lastname: %s, firstname: %s, middlename: %s' % (alias.lastname, alias.firstname, alias.middlename)
            alias.creator = user
        alias.worker = worker
        alias.validated = True
        alias.save()
        action.send(user, verb=verb, action_object=alias, target=worker, description=description)
        logger.debug("Worker alias %s: %s", log_verb, alias)
    return True

@login_required
def profile_edit(request, form_name=None):
    """
    Edit personal data (main, aliases) of the user profile. form_name can be None, 'main' or 'aliases'.
    """
    from userprofile.views import Profile
    profile, created = Profile.objects.get_or_create(user=request.user)
    worker = profile.worker

    main_form = None # worker main info form
    alias_formset = None

    if request.method == "POST":
        logger.debug("Got POST request: %s", request.POST)
        if form_name == "main":
            old_info = profile.summary # for logging
            main_form = WorkerProfileForm(request.POST, instance=profile)
            if main_form.is_valid():
                if worker: # if profile is linked to worker, save old profile name to worker alias
                    alias, created = worker.add_alias_from_profile()
                    if created and request.user:
                        description = u'lastname: %s, firstname: %s, middlename: %s, validated: %s, создан автоматически при изменении ФИО пользователя (это старый вариант ФИО пользователя)' % (alias.lastname, alias.firstname, alias.middlename, alias.validated)
                        action.send(request.user, verb=u'добавил псевдоним сотрудника в профиле', action_object=alias, target=worker, description=description)
                profile = main_form.save()
                if worker: # if profile is linked to worker, copy new profile name to worker, moving old worker name to alias
                    worker.profile = profile # seems dumb, but actually updates worker's profile link cache (otherwise worker.profile still contains old profile data and worker's name will not be updated with new profile name)
                    alias, created = worker.copy_name_from_profile()
                    if created and request.user:
                        description = u'lastname: %s, firstname: %s, middlename: %s, validated: %s, создан автоматически при изменении ФИО пользователя и копировании нового ФИО в сотрудника (это старый вариант ФИО сотрудника)' % (alias.lastname, alias.firstname, alias.middlename, alias.validated)
                        action.send(request.user, verb=u'добавил псевдоним сотрудника в профиле', action_object=alias, target=worker, description=description)
                if request.user:
                    action.send(request.user, verb=u'изменил данные профиля', target=profile, description=u"%s -> %s" % (old_info, profile.summary))
                messages.success(request, u"Данные вашего профиля успешно сохранены.")
                return redirect("workers_profile_edit")
        elif form_name == "aliases":
            alias_formset = WorkerAliasFormSet(request.POST, instance=worker)
            if worker and alias_formset.is_valid():
                save_workers_alias_formset(request.user, worker, alias_formset)
                messages.success(request, u"Альтернативные имена успешно сохранены.")
                return redirect("workers_profile_edit")

    if not main_form:
        main_form = WorkerProfileForm(instance=profile)
    if not alias_formset and worker:
        alias_formset = WorkerAliasFormSet(instance=worker)

    # form for linking profile to worker
    linking_form = None
    if profile.fullname or profile.worker: # show linking form
        name = lambda person: {'firstname': person.firstname, 'middlename': person.middlename, 'lastname': person.lastname}
        initial_name = name(profile.worker) if profile.worker else {}
        for key, value in name(profile).items(): # take profile names if they exist # FIXME: lastname can be blank (model allows it), and if there is no linked worker, no similar workers will be found
            if value:
                initial_name[key] = value
        linking_form = WorkerForm(initial=initial_name, prefix="my", similar_no_profiles=True, profile_worker=worker)

    template = "workers/profile_edit.html"
    data = {'section': 'personal', 'main_form': main_form, 'worker': worker, 'alias_formset': alias_formset, 'linking_form': linking_form}
    return render_to_response(request, template, data)


@login_required
def profile_link(request):
    '''Links profile to worker using form data.'''
    if request.method == "POST":
        logger.debug("Got POST request: %s", request.POST)
        form = WorkerForm(request.POST, prefix="my") # prefix is needed because otherwise field name 'wid' will be None (in worker_form.html form.prefix|add is used, and prefix should be non-empty)
        if form.is_valid():
            # save worker form, possibly creating a new worker
            form.save()
            profile, created = Profile.objects.get_or_create(user=request.user)
            profile.worker = form.worker_instance
            if form.worker_created:
                action.send(request.user, verb=u'добавил сотрудника', action_object=form.worker_instance, target=profile)
                form.worker_instance.creator = request.user
                form.worker_instance.save()
            if form.worker_alias_created: # profile name is copied to worker alias, if an existing worker is selected
                alias = form.worker_alias_instance
                alias.validated = True # we consider this alias validated, since the user linked his profile to the worker himself.
                alias.creator = request.user
                alias.save()
                description = u'lastname: %s, firstname: %s, middlename: %s, validated: %s, создан автоматически при связывании пользователя с сотрудником (это ФИО пользователя)' % (alias.lastname, alias.firstname, alias.middlename, alias.validated)
                action.send(request.user, verb=u'добавил псевдоним сотрудника в профиле', action_object=alias, target=form.worker_instance, description=description)
            profile.save()
            logger.debug("worker linked to profile: %s (id %s)", form.worker_instance.fullname, form.worker_instance.id)
            if request.user:
                action.send(request.user, verb=u'связал свой профиль с сотрудником', action_object=profile, target=profile.worker)
            # copy linked profile's name to worker, moving old worker name to alias
            alias, created = form.worker_instance.copy_name_from_profile()
            if created and request.user:
                description = u'lastname: %s, firstname: %s, middlename: %s, validated: %s, создан автоматически при связывании пользователя с сотрудником и копировании ФИО пользователя в сотрудника (это старый вариант ФИО сотрудника)' % (alias.lastname, alias.firstname, alias.middlename, alias.validated)
                action.send(request.user, verb=u'добавил псевдоним сотрудника в профиле', action_object=alias, target=form.worker_instance, description=description)
            worker_str = u'<a href="%s" title="Перейти на вашу страницу">%s</a>' % (profile.worker.get_absolute_url(), profile.worker.fullname)
            messages.success(request, u"Вы успешно связали свой профиль с сотрудником %s." % worker_str)
            return redirect("home")
    else:
        return redirect("workers_profile_edit")

@worker_required
def employment_edit(request, worker, employment_id=None):
    from workers.views.registration import save_employment_claim
    if not has_permission(request.user, "edit", worker):
        return redirect("home")
    instance = None
    organization_form = OrganizationForm(request.POST or None)
    # FIXME: the following essentially means "can edit but not just because that is my profile"
    # so that ordinary users can't edit their confirmed employment
    can_edit = worker.user != request.user or request.user.is_superuser or request.user.representatives.filter(is_active=True).exists() or worker.is_managed_in_dissertation_council_by(request.user)
    if employment_id:
        instance=get_object_or_404(Employment, pk=employment_id, worker=worker)
        organization_form.fields['organization'].initial = instance.department.organization
    employment_form = (EmploymentForm if can_edit else EmploymentClaimForm)(request.POST or None,  instance=instance)
    if request.method == "POST":
        if request.POST.get('institution', None):
            worker.institution = request.POST.get('institution')
            worker.save()
            messages.success(request, u'Место работы во внешней организации сохранено.')
            return redirect("workers_employment_edit", worker_id=worker.id)
        if save_employment_claim(request, employment_form, worker, "home"):
            return redirect("workers_employment_edit", worker_id=worker.id)

    return render_to_response(request, "workers/employment_edit.html", {'section': 'employment',
        'organization_form': organization_form,
        'employment_form': employment_form,
        'worker': worker,
        'can_edit': can_edit })

@login_required
def employment_edit_position(request, employment_id):
    employment = get_object_or_404(Employment, pk=employment_id)
    if not has_permission(request.user, "edit", employment.worker):
        return redirect("home")
    if request.method == "POST":
        position_form = EmploymentPositionForm(request.POST, instance=employment)
        if position_form.is_valid():
            position_form.save()
    return redirect('workers_employment_edit', worker_id=employment.worker.id)


def save_rank(request, worker, rank_type=None):
    '''Save submitted rankk forms. rank_type may be "ordinary" or "academy".
    Appropriate forms are processed. Old behaviour apply if no rank_type specified.'''
    if rank_type == "ordinary" and "ordinary-rank_type" not in request.POST:
        return None
    if rank_type == "academy" and "academy-rank_type" not in request.POST:
        return None
    if "ordinary-rank_type" in request.POST or "academy-rank_type" in request.POST:
        degree_form = DegreeForm()
        if 'ordinary-rank_type' in request.POST and (rank_type == "ordinary" or not rank_type):
            rank_form_submited = ordinary_rank_form = OrdinaryRankForm(request.POST, prefix="ordinary")
            academy_rank_form = AcademyRankForm(prefix="academy")
        if 'academy-rank_type' in request.POST and (rank_type == "academy" or not rank_type):
            rank_form_submited = academy_rank_form = AcademyRankForm(request.POST, prefix="academy")
            ordinary_rank_form = OrdinaryRankForm(prefix="ordinary")
        if rank_form_submited.is_valid():
            rank_type = rank_form_submited.cleaned_data['rank_type']
            rank_name = dict(rank_form_submited.rank_choices)[rank_type]
            rank = Rank.objects.get(name=rank_name)
            #department = rank_form_submited.cleaned_data["department"]
            academy = rank_form_submited.cleaned_data.get("academy", None)
            branch_num = rank_form_submited.cleaned_data.get("branch_num", "")
            date = rank_form_submited.cleaned_data["date"]
            man_rank, created = ManRank.objects.get_or_create(rank=rank, date=date, worker=worker,# department=department,
                                                              academy=academy, branch_num=branch_num)
            if created:
                description = {'rank_type': rank, 'date': date}
                action.send(request.user, verb=u'добавил звание', action_object=man_rank, target=worker,
                            description=description)
            messages.success(request, u"Звание успешно добавлено.")
            return True
            return redirect("workers_degree_edit", worker.id)
        return False
    return None

def save_degree(request, worker):
    def bool_to_yes_no(bool):
        return "Y" if bool else "N"

    if "degree_type" in request.POST:
        degree_form = DegreeForm(request.POST)
        ordinary_rank_form = OrdinaryRankForm(prefix="ordinary")
        academy_rank_form = AcademyRankForm(prefix="academy")
        if degree_form.is_valid():
            degree_type = degree_form.cleaned_data["degree_type"]
            is_degree_prior = bool_to_yes_no(degree_type == "degree_prior")
            branch = degree_form.cleaned_data["branch"]
            try:
                degree = Degree.objects.get(is_degree_prior=is_degree_prior, branch=branch)
            except Degree.DoesNotExist:
                messages.error(request, u'Степень не найдена.')
                return False
                return render_to_response(request, "workers/degree_edit.html", {'section': 'degrees','degree_form': degree_form, 'worker': worker})
            year = degree_form.cleaned_data["year"]
            man_degree, created = ManDegree.objects.get_or_create(degree=degree, year=year, worker=worker)
            if created:
                description = {'degree_type': man_degree.degree, 'year': man_degree.year}
                action.send(request.user, verb=u'добавил степень', action_object=man_degree, target=man_degree.worker,
                            description=description)
            messages.success(request, u"Степень успешно добавлена.")
            return True
            return redirect("workers_degree_edit", worker.id)
        return False
    return None

@worker_required
def degree_edit(request, worker):
    if not has_permission(request.user, "edit", worker):
        return redirect("home")
    degree_form = DegreeForm()
    ordinary_rank_form = OrdinaryRankForm(prefix="ordinary")
    academy_rank_form = AcademyRankForm(prefix="academy")

    if request.method == "POST":
        degree_status = save_degree(request, worker)
        academy_status = save_rank(request, worker, "academy")
        ordinary_status = save_rank(request, worker, "ordinary")
        if degree_status == False: degree_form = DegreeForm(request.POST)
        if academy_status == False: academy_rank_form = AcademyRankForm(request.POST, prefix="academy")
        if ordinary_status == False: ordinary_rank_form = OrdinaryRankForm(request.POST, prefix="ordinary")
        logger.debug("Got POST request: %s", request.POST)
        if degree_status or academy_status or ordinary_status:
            return redirect("workers_degree_edit", worker.id)
    return render_to_response(request, "workers/degree_edit.html", {'section': 'degrees', 'degree_form': degree_form,
                                                                    'ordinary_rank_form': ordinary_rank_form,
                                                                    'academy_rank_form': academy_rank_form, 'worker': worker})

def employment_delete(request, employment_id):
    try:
        # no employment_id => remove unclassified employment information from profile's field
        employment_id = int(employment_id)
        if employment_id <= 0 and request.user:
            worker = request.user.get_profile().worker if not employment_id else get_object_or_404(Worker, pk=-employment_id)
            if not has_permission(request.user, "edit", worker):
                return HttpResponseForbidden(simplejson.dumps({'message': u"Недостаточно прав для удаления"}), content_type='application/javascript')
            worker.institution = ''
            worker.save()
            return HttpResponse(simplejson.dumps({'message': "Deleted"}), content_type='application/javascript')
    except:
        pass
    try:
        employment = Employment.objects.get(pk=employment_id)
    except Employment.DoesNotExist:
        # Check if user is trying to delete request for employment
        employment = get_object_or_404(EmploymentClaim, pk=employment_id)
    if has_permission(request.user, "edit", employment.worker):
        description = {'department': employment.department, 'startdate': employment.startdate, 'enddate': employment.enddate, 'position': employment.position, 'parttime': employment.parttime}
        action.send(request.user, verb=u'удалил место работы', action_object=employment, target=employment.worker, description=description)
        employment.delete()
    return HttpResponse(simplejson.dumps({'hello': "helloMan"}), content_type='application/javascript')

def degree_delete(request, degree_id):
    degree = get_object_or_404(ManDegree, id=degree_id)
    if has_permission(request.user, "edit", degree.worker):
        description = {'degree_type': degree.degree, 'year': degree.year}
        action.send(request.user, verb=u'удалил степень', action_object=degree, target=degree.worker, description=description)
        degree.delete()
    return HttpResponse(simplejson.dumps({'hello': "helloMan"}), content_type='application/javascript')

def rank_delete(request, rank_id):
    rank = get_object_or_404(ManRank, id=rank_id)
    if has_permission(request.user, "edit", rank.worker):
        description = {'rank_type': rank.rank, 'date': rank.date}
        action.send(request.user, verb=u'удалил звание', action_object=rank, target=rank.worker, description=description)
        rank.delete()
    return HttpResponse(simplejson.dumps({'hello': "helloMan"}), content_type='application/javascript')
