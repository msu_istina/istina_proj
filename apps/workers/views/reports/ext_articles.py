# -*- coding: utf-8; -*-

from django.shortcuts import render
from publications.models import Article, ArticleAuthorship
from statistics.sqlutils import get_cited_articles_counts
from statistics.sqlutils import get_ext_articles_ids

from workers.decorators import worker_required

@worker_required
def worker_report_ext_articles(request, worker):
    citations = get_cited_articles_counts(worker.id)

    system_short_name =  request.GET.get('system_short_name', None)
    if system_short_name == 'all':
        system_short_name = None
    
    articles_ids = get_ext_articles_ids(worker.id, system_short_name)
    articles = Article.objects.filter(id__in=articles_ids)
    workers_registered = ArticleAuthorship.objects.get_registered_authors(articles_ids)
    template = "statistics/ext_articles_by_worker.html"
    return render(request, template, {"citations": citations,
                                      "articles": articles,
                                      "worker": worker,
                                      "workers_registered": workers_registered})
