# -*- coding: utf-8; -*-
import datetime
from django.template.loader import render_to_string
from django.http import HttpResponse, Http404, HttpResponseForbidden
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages

from workers.decorators import worker_required
from organizations.models import Department
from courses.models import TeachingKind

from organizations.utils import is_representative_for
from organizations.utils import is_representative_for_user
from common.utils.latex import latex_to_pdf
from common.utils.dates import get_academic_range

from statistics.sqlutils import get_worker_citcount


@worker_required
def worker_report_infolist(request, worker, mode='pdf', report_depth=5):    
    is_owner = request.user == worker.user
    if request.user != worker.user and not is_representative_for_user(request.user, worker.user):
        mode = None
        show_pdf_link = False
    else:
        show_pdf_link = True

    if mode == 'pdf' and request.method == 'GET':
        dep_id = None
        for e in worker.current_employments:
            if not e.parttime:
                dep_id = e.department.id
        if not dep_id:
            messages.warning(request, u'У Вас не указано место работы.')
        return render(request, "workers/worker_report_infolist_askparams.html",
                      {"worker": worker,
                       "report_depth": report_depth,
                       "dep_id": dep_id,
                       })

    report_depth = 5
    extra_report_year = 0
    offset = 0
    current_year = -1 # do not include current year
    if request.method == 'POST':
        try:
            report_depth = int(request.POST.get('depth', '5'))
            current_year = int(request.POST.get('current_year', '-1'))
        except ValueError:
            pass
    academic_years = get_academic_range(report_depth)
    if current_year == 1:  # Count current year as full year
        years = academic_years[1:]
    elif current_year == 0:  # Include, but not count
        years = academic_years
        extra_report_year = 1
    else: # Do not include curent year by default
        years = academic_years[:-1]
        offset = 1
    activities = worker.get_activities(years=years)
    
    # count total and average hours for courses
    course_teachings = activities["courses_teachings"]
    if course_teachings:
        # average number of hours of each course teaching_kind in the period
        kinds = TeachingKind.objects.values_list('name', flat=True)
        av_hours = dict([(name, 0) for name in kinds]) # value is sum_of_hours for that kind
        for teaching in course_teachings:
            av_hours[teaching.teaching_kind.name] += teaching.total_hours_in_period(academic_years)
        total_av_hours = 0. # average hours for all course teachings regardless of kind
        for kind in av_hours.keys():
            # replace sum with average number
            av_hours[kind] = (float(av_hours[kind]) / report_depth)
            total_av_hours += av_hours[kind]
    else:
        av_hours = 0
        total_av_hours = 0

    dep_id = request.POST.get('dep_id', None)
    dep = None
    if dep_id:
        try:
            dep = get_object_or_404(Department, pk=dep_id)
        except ValueError:
            dep_id = None
    worker_data = get_worker_citcount(worker.id, report_depth+extra_report_year, offset=offset)
    extra_data = {"birth_year": request.POST.get('birth_year', ''),
                  "grad_institution": request.POST.get('edu_institution', ''),
                  "grad_faculty": request.POST.get('edu_faculty', ''),
                  "grad_year": request.POST.get('edu_year', ''),
                  "post": request.POST.get('post',''),
                  "part_time": True if request.POST.get('parttime',None) else None,
                  "department": dep,
                 }
    template_params = {"worker": worker,
                       "activities": activities,
                       # dissertations for all years, like diplomas
                       "dis_phd": sum(1 for x in worker.dissertations_advised.all() if x.phd == "Y"),
                       "dis_doc": sum(1 for x in worker.dissertations_advised.all() if x.phd == "N"),
                       "av_hours": av_hours,
                       "total_av_hours": total_av_hours,
                       "worker_hirsh": worker_data["worker_hirsh"],
                       "worker_pubstats": worker_data["worker_pubstats"],
                       "worker_pubstats_5y": worker_data["worker_pubstats_5y"],
                       "extra_data": extra_data,
                       "is_owner": is_owner,
                       "show_pdf_link": show_pdf_link,
                       "show_full_teaching": request.POST.get('show_full_teaching', False),
                       "show_dean_signature": request.POST.get('show_dean_signature', False),
                       "min_year": min(years),
                       "max_year": max(years),
                       }

    if mode=='pdf':
        latex = render_to_string("workers/worker_report_infolist.tex",
                                template_params)
        pdf = latex_to_pdf(latex)
        response = HttpResponse(pdf, mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=infolist_' + str(worker.id) + '.pdf'
        return response
    return render(request, "workers/worker_report_infolist.html",
                  template_params)
