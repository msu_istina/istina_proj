# -*- coding: utf-8; -*-
import datetime
from django.template.loader import render_to_string
from django.http import HttpResponse, Http404, HttpResponseForbidden
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages

from workers.decorators import worker_required
from publications.models import Article, ArticleAuthorship
from publications.models import Book, BookAuthorship

from organizations.utils import is_representative_for
from organizations.utils import is_representative_for_user
from common.utils.latex import latex_to_pdf

from collections import namedtuple
from common.db_constants import DB_CONSTANTS
# from django.db.models import Q

# When current year should be counted as a normal year
NEW_YEAR_START=0

@worker_required
def worker_report_publications_list(request, worker, mode='pdf', report_depth=5, count_current_year=None):
    mode = 'pdf'

    if request.method == "GET":
        return render(request, "workers/worker_report_list_of_publications_askparams.html",
                      {"worker": worker,})

    if request.user != worker.user and not is_representative_for_user(request.user, worker.user):
        mode = None
    else:
        mode = 'pdf'
    mode = 'pdf'

    # determine reporting years
    current_year = "do_not_include"
    try:
        report_depth = int(request.POST.get('depth',report_depth))
        current_year = request.POST.get('current_year', 'do_not_include')
        if report_depth <= 0:
            report_depth = 5
    except ValueError:
        report_depth = 5
    # Generate list of years, including the current year
    years = [datetime.date.today().year-report_depth+i for i in range(report_depth+1)]
    if current_year == "do_not_include":
        years = years[:-1]
    elif (current_year == "regular_year" or count_current_year):
        years = years[1:]
    years_str = u'с ' + unicode(min(years)) + u' по ' + unicode(max(years)) + u' гг.'

    activities = worker.get_activities(years=years)

    PublistTuple = namedtuple('PublistTuple', 'title, kind, form, source, npages, coauthors, year')
    data = []
    for a in activities['articles'] + activities['theses']:
        coauthors = []
        for aau in ArticleAuthorship.objects.filter(article = a):
            if not aau.author == worker:
                name = aau.original_name or aau.author.get_fullname(initials=True)
                coauthors.append( name )
        pub_form = u'печатная'
        if a.tags.filter(list_value__id = DB_CONSTANTS["tagtype"]["epub"]):
            pub_form = u'электр.'

        pub_kind = a.nominative_short
        if a.tags.filter(list_value__id = DB_CONSTANTS["tagtype"]["review"]):
            pub_kind = u'рецензия'
        elif a.incollection and a.pure:
            if a.tags.filter(list_value__id = DB_CONSTANTS["tagtype"]["in_encyclopedia"]):
                pub_kind = u'статья в энциклопедии'
            elif a.tags.filter(list_value__id = DB_CONSTANTS["tagtype"]["chapter"]):
                pub_kind = u'глава в книге'
            else:
                pub_kind = u'статья в сборнике'

        data.append( PublistTuple(a.title, pub_kind, pub_form, \
                                  a.source, a.get_number_of_pages(), \
                                  ', '.join(coauthors), a.year) )

    for b in activities["books"]:
        coauthors = []
        for aau in BookAuthorship.objects.filter(book = b):
            if not aau.author == worker:
                name = aau.original_name or aau.author.get_fullname(initials=True)
                coauthors.append( name )
        textbooks = [DB_CONSTANTS["bookgrouptype"]["textbook_grif"],
                     DB_CONSTANTS["bookgrouptype"]["textbook"]]
        pub_kind = u'монография'
        if b.category_memberships.filter(category__id__in=textbooks):
            pub_kind = u'учебник'

        data.append( PublistTuple(b.title, pub_kind, u'печатная', \
                                  b.source, b.pages or '', \
                                  ', '.join(coauthors), b.year) )

    for d in activities["dissertations_defended"]:
        coauthors = '' # Dissertations have no coauthors!
        npages = '' # There is no such data for dissertations
        data.append( PublistTuple(d.title, d.nominative_short, u'на правах рук.', \
                                  d.get_phd_display() + (u' диссертация по специальност%s ' % u'ям' if d.specialty2 else u'и') + d.get_specialty + u' (' + d.domain.shortname + u'),' + str(d.get_year),
                                  npages, \
                                  coauthors, d.get_year) )

    data.sort(key=lambda x: x.year, reverse=False)

    template_params = {"years": years_str,
                       "worker": worker,
                       "data": data,
                       }
    if mode=='pdf':
        latex = render_to_string("workers/worker_report_list_of_publications.tex",
                                template_params)
        pdf = latex_to_pdf(latex)
        response = HttpResponse(pdf, mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=pubslist_' + str(worker.id) + '.pdf'
        return response
    messages.warning(request, u'Извините, у Вас нет прав для просмотра списка работ данного сотрудника.')
    return redirect("home")

