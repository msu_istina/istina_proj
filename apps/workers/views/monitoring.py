# -*- coding: utf-8; -*-
from workers.decorators import worker_required
from django.utils.log import getLogger
from statistics.views.stats import get_report, format_report
from statistics.sqlutils_getdata import get_department_pubs_generic, get_anketa_data
from common.utils.excel import export_to_xlsx_response
from common.utils.user import get_worker
from django.shortcuts import render

logger = getLogger("workers.views.monitoring")

from django.utils.safestring import mark_safe
def safe_link(url, text=None):
    return mark_safe('<a href="%s">%s</a>' % (url, text or url))

@worker_required
def index(request, worker):
    page_owner = get_worker(request.user) == worker
    report_year=2015
    startyear = report_year-4
    endyear = report_year
    return render(request, "workers/report_monitoring.html", {'worker': worker, 'page_owner': page_owner, 'startyear': startyear, 'endyear': endyear})

def make_excel_headers(headers, worker, report_title, add_blank_line=True):
    return [[], [u"Мониторинг по диссертационным советам (ДС)"], [report_title], [worker.fullname], headers, range(1, len(headers)+1)] \
        + ([[]] if add_blank_line else [])

@worker_required
def profile(request, worker, startyear, endyear, export_excel=False):
    title = u"Анкета члена диссертационного совета (Приложение 1)"
    headers = [u'', u'Показатель', u'Значения', u'Детализация показателя']

    keys = [
        u"Наличие степени доктора наук",
        u"""Стаж работы после получения ученой степени доктора наук:
            - научно-педагогическая деятельность
            - другие виды деятельности""",
        u"""Наличие кандидатов и докторов наук, защитившихся под научным руководством за последние 10 лет
            Докторские диссертации/кандидатские диссертации""",
        u"""Наличие за последние 5 лет публикаций по специальности,
            представляемой данным членом совета, в журналах,
            индексируемых в признанных международных системах цитирования (библиографических базах)
            по соответствующим областям науки (WebofScience, Scopus, WebofKnowledge, Astrophysics,
            PubMed, Mathematics, ChemicalAbstracts, Springer, Agris, GeoRef, MathSciNet, BioOne, Compendex, CiteSeerX)""",
        u"""Наличие за последние 5 лет публикаций по специальности,
            представляемой данным членом совета, в ведущих научных журналах,
            входящих в Перечень ведущих рецензируемых научных журналов и изданий,
            рекомендованный ВАК, за исключением журналов из п. 4.""",
        u"""Наличие за последние 10 лет рецензируемых монографий,
            опубликованных в ведущих отечественных или зарубежных издательствах с указанием тиража и объема.
            (Прим.: решение о соответствии тех или иных изданий этому требованию будут приниматься экспертным советом ВАК)""",
        u"Индекс Хирша (Web of Science/РИНЦ)",
        u"Число патентов на изобретения, полученных за последние 10 лет",
        u"""Число цитирований публикаций члена диссертационного совета в журналах,
            индексируемых в базе WebofScience, за последние 5 лет.""",
        u"""За последние 5 лет: участие с докладом на международных конференциях,
            организаторами которых являются ведущие международные профессиональные
            ассоциации в соответствующей дисциплине, а также организации, входящие в перечень,
            утвержденный распоряжением правительства Российской Федерации от 21 мая 2012 г. № 812-р""",
        u"""Участие в законопроектной, экспертно-аналитической работе в интересах
            (по поручениям) органов государственной власти Российской Федерации,
            консультативных органов при Президенте Российской Федерации,
            либо Правительстве Российской Федерации, или при органах государственной
            или региональной власти субъектов Российской Федерации
            (подтверждается приказом (распоряжением) соответствующего органа,
            справками (актами), выданными соответствующими органами)"""
    ]
    details = [
        "", "",
        u"Таблица 5-дс: Кадры высшей научной квалификации, подготовленные под руководством члена диссертационного совета",
        u"Таблица 3-дс:  перечень статей с полным библиографическим описанием",
        u"Таблица 3-дс:  перечень статей с полным библиографическим описанием",
        u"Таблица 2-дс:  перечень монографий с полным библиографическим описанием",
        "",
        u"Таблица 4-дс:  перечень патентов на изобретения",
        u"Таблица 3-дс:  перечень статей с полным библиографическим описанием",
        u"Таблица 6-дс:  перечень конференций с указанием организатора",
        u"Таблица 7-дс:  перечень приказов (распоряжений), справок(актов)"
    ]

    startyear=int(startyear)
    endyear=int(endyear)
    scopus_wos_works_count, vak_works_count, wos_citations_count, wos_hirsh_index = get_anketa_data(worker.id)[1:]
    hirsh_index = "%d/ " % wos_hirsh_index
    is_doctor = u"да" if worker.is_doctor else u"нет"
    activities = worker.get_activities(years=range(endyear-11, endyear)) # 2003-2012, 10 years
    dissers = activities['dissertations_advised']
    doctors = len(filter(lambda disser: disser.phd == 'N', dissers))
    candidates = len(filter(lambda disser: disser.phd == 'Y', dissers))
    doctors_candidates = "%d/%d" % (doctors, candidates)
    books = len(activities['books'])
    patents = len(activities['patents'])
    conferences = len(filter(lambda presentation: presentation.year >= startyear, activities['conference_presentations']))
    values = [is_doctor, "", doctors_candidates, scopus_wos_works_count, vak_works_count, books,
        hirsh_index, patents, wos_citations_count, conferences, ""]

    rows = [[i+1, key, value, detail] for i, key, value, detail in zip(range(len(keys)), keys, values, details)]
    context = {'worker': worker}
    excel_args = {'export_excel': True, 'excel_sheetname': u'Приложение_1',
        'excel_filename': 'monitoring-2013-profile.%d.xls' % worker.id} if export_excel else {}
    if export_excel:
        headers = make_excel_headers(headers, worker, u"Таблица 1 - дс - Анкета", add_blank_line=False)
    return format_report(request, title, headers, rows, context, **excel_args)

@worker_required
def books(request, worker, startyear, endyear, export_excel=False):
    startyear = int(startyear)
    endyear = int(endyear)
    title = u"Монографии, изданные за %d-%d годы (Таблица 2-дс)" % (startyear, endyear)
    headers = [u'№ п\п', u'Год', u'Полное библиографическое описание', u'Электронный адрес размещения']
    make_row = lambda i, work: [i, work.year, 'bibtex', safe_link(work.url) if not export_excel else work.url]
    filters = u"((category = 'Монография' or category = 'Учебное пособие') and year>%d and year<%d)" % (startyear-1, endyear+1)
    excel_args = {'export_excel': True, 'excel_sheetname': u'Таблица_2_дс',
        'excel_filename': 'monitoring-2013-books.%d.xls' % worker.id} if export_excel else {}
    if export_excel:
        headers = make_excel_headers(headers, worker, u"Таблица 2 - дс - Монографии")
    return get_report(request, title, headers, make_row, worker=worker, bibtex_column=2, filters=filters, books=True, **excel_args)


@worker_required
def publications(request, worker, startyear, endyear, export_excel=False):
    startyear = int(startyear)
    endyear = int(endyear)
    title = u'Список публикаций %d-%d годов в журналах, индексируемых в Web of Science/Scopus, а также из перечня ВАК (Таблица 3-дс)' % (startyear, endyear)
    headers = [
        u'Номер по порядку',
        u'Год',
        u'Полное библиографическое описание статьи',
        u'Наименование журнала',
        u'Импакт-фактор журнала',
        u'Библиографическая база, в которой индексируется журнал',
        u'Электронный адрес размещения']
    make_row = lambda i, work: [i, work.year, 'bibtex', work.source, work.impact_factor, work.external_system, safe_link(work.url) if not export_excel else work.url]
    # search by journal in systems, so no russian articles at all
    filters = """(
        (in_scopus = 1 or in_wos = 1
        or wos_id is not null or scopus_id is not null
        or impact_wos is not null or impact_scopus is not null
        or in_vak = 1)
        and year>%d and year<%d)
    """ % (startyear-1, endyear+1)
    excel_args = {'export_excel': True, 'excel_sheetname': u'Таблица_3_дс',
        'excel_filename': 'monitoring-2013-publications.%d.xls' % worker.id} if export_excel else {}
    if export_excel:
        headers = make_excel_headers(headers, worker, u"Таблица 3 - дс - Статьи")
    return get_report(request, title, headers, make_row, worker=worker, bibtex_column=2, filters=filters, **excel_args)

@worker_required
def patents(request, worker, startyear, endyear, export_excel=False):
    startyear = int(startyear)
    endyear = int(endyear)
    title = u"Патенты на изобретения, полученные в %d-%d годах (Таблица 4-дс)" % (startyear, endyear)
    headers = [u'№ п\п', u'Полное библиографическое описание', u'Электронный адрес размещения']
    make_row = lambda i, work: [i, "bibtex", safe_link(work.url) if not export_excel else work.url]
    filters = u"(category = 'Патенты' and year>%d and year<%d)" % (startyear-1, endyear+1)
    post_process = lambda row: [row[0], row[1].replace("Tech. Rep", u"пат"), row[2]]
    excel_args = {'export_excel': True, 'excel_sheetname': u'Таблица_4_дс',
        'excel_filename': 'monitoring-2013-patents.%d.xls' % worker.id} if export_excel else {}
    if export_excel:
        headers = make_excel_headers(headers, worker, u"Таблица 4 - дс - Патенты")
    return get_report(request, title, headers, make_row, worker=worker,
        bibtex_column=1, filters=filters, books=True, post_process=post_process,
        **excel_args)

@worker_required
def publications_template(request, worker, startyear, endyear):
    startyear = int(startyear)
    endyear = int(endyear)
    articles = get_department_pubs_generic({'ALL': 1}, {
        'man': worker.id,
        'FREE': """(
            (in_scopus = 1 or in_wos = 1
            or wos_id is not null or scopus_id is not null
            or impact_wos is not null or impact_scopus is not null
            or in_vak = 1)
            and year>%d and year<%d)
        """ % (startyear-1, endyear+1)
    })
    books = get_department_pubs_generic({'ALL': 1}, {
        'man': worker.id,
        'books': 'yes',
        'FREE': u"((category = 'Монография' or category = 'Учебное пособие') and year>%d and year<%d)" % (startyear-1, endyear+1)
    })
    conferences = get_department_pubs_generic({'ALL': 1}, {
        'man': worker.id,
        'confs': 'yes',
        'FREE': u"(category = 'Конференция' and year>=%d and year<=%d) and scope_name = 'Международная'" % (startyear-1, endyear+1)
    })

    articles = list(articles)
    books = list(books)
    conferences = list(conferences)
    from publications.utils import bibtex2html_multiple
    articles_bibtex = bibtex2html_multiple(articles)
    books_bibtex = bibtex2html_multiple(books)

    # From codes from xlsx template
    # 14 is 'Другая межд. библ. БД'
    extsystem_id = {
        'Web of Science': 2,
        'Scopus': 3,
        u'РИНЦ': 1,
    }
    articles_row = lambda work, bibtex: [work.year, bibtex, work.source, work.impact_factor, work.external_system if work.external_system != u'РИНЦ' else u'Из перечня ВАК', extsystem_id.get(work.external_system, 14)]
    books_row = lambda work, bibtex: [work.year, bibtex]
    conferences_row = lambda work: [work.year, u'%s // %s' % (work.title, unicode(work.instance.conference)), work.conf_name]

    import dissertation_councils
    return export_to_xlsx_response({
        u'1. Научные статьи': {
            'first_row': 2, # skip header
            'first_column': 2, # skip count
            'rows': map(articles_row, articles, articles_bibtex)
        },
        u'2. Монографии': {
            'first_row': 2,
            'first_column': 2,
            'rows': map(books_row, books, books_bibtex)
        },
        u'3. Доклады': {
            'first_row': 2,
            'first_column': 2,
            'rows': map(conferences_row, conferences)
        }
    },
        template='dissertation_councils/report/member-publications.xlsx',
        filename=u'Publications-%s-%s-%s.xlsx' % (startyear, endyear, worker.id)
    )


@worker_required
def dissertations(request, worker, startyear, endyear, export_excel=False):
    startyear = int(startyear)
    endyear = int(endyear)
    title = u"Кандидаты и доктора наук, защитившиеся под руководством сотрудника в %d-%d годах (Таблица 5-дс)" % (startyear, endyear)
    headers = [u'№ п\п',
        u'Шифр и наименование научной специальности',
        u'Ученая степень (кандидат/доктор)',
        u'ФИО соискателя ученой степени (полностью)',
        u'Название диссертационной работы',
        u'Дата защиты',
        u'Серия и номер диплома']

    activities = worker.get_activities(years=range(startyear, endyear+1))
    dissers = activities['dissertations_advised']
    degree = lambda disser: u"кандидат" if disser.phd == 'Y' else u"доктор"
    make_row = lambda disser_tuple: [disser_tuple[0],
        disser_tuple[1].specialty,
        degree(disser_tuple[1]),
        disser_tuple[1].author_original_name or disser_tuple[1].author.fullname,
        disser_tuple[1].title,
        disser_tuple[1].year,
        '']
    rows = map(make_row, [(i, disser) for i, disser in enumerate(dissers, start=1)])
    context = {'worker': worker}
    excel_args = {'export_excel': True, 'excel_sheetname': u'Таблица_5_дс',
        'excel_filename': 'monitoring-2013-dissertations.%d.xls' % worker.id} if export_excel else {}
    if export_excel:
        headers = make_excel_headers(headers, worker, u"Таблица 5 - дс - Кадры")
    return format_report(request, title, headers, rows, context, **excel_args)

@worker_required
def conferences(request, worker, startyear, endyear, export_excel=False):
    startyear = int(startyear)
    endyear = int(endyear)
    title = u"Конференции, в которых участвовал сотрудник в %d-%d годах (Таблица 6-дс)" % (startyear, endyear)
    headers = [
        u'№ п\п',
        u'Год',
        u'Полное библиографическое описание',
        u'Название конференции',
        u'Электронный адрес размещения статьи в трудах или материалах конференции'
    ]
    make_row = lambda i, work: [i, work.year, u'%s // %s' % (work.title, unicode(work.instance.conference)), work.conf_name, safe_link(work.url) if not export_excel else work.url]
    filters = u"(category = 'Конференция' and year>=%d and year<=%d) and scope_name = 'Международная'" % (startyear-1, endyear+1)
    excel_args = {'export_excel': True, 'excel_sheetname': u'Таблица_6_дс',
        'excel_filename': 'monitoring-2013-conferences.%d.xls' % worker.id} if export_excel else {}
    if export_excel:
        headers = make_excel_headers(headers, worker, u"Таблица 6 - дс - Конференции")
    return get_report(request, title, headers, make_row, worker=worker, filters=filters,  conferences=True, **excel_args)

@worker_required
def theses(request, worker, startyear, endyear, export_excel=False):
    startyear = int(startyear)
    endyear = int(endyear)
    title = u"Статьи в сборниках и тезисы докладов сотрудника за %d-%d года (Таблица 6-дс)" % (startyear, endyear)
    headers = [
        u'№ п\п',
        u'Полное библиографическое описание статьи в трудах или материалах конференции',
        u'Электронный адрес размещения статьи в трудах или материалах конференции'
    ]
    make_row = lambda i, work: [i, "bibtex", safe_link(work.url) if not export_excel else work.url]
    filters = u"""(
        (category = 'Статьи в сборниках' or category = 'Тезисы')
        and year>%d and year<%d)
    """ % (startyear-1, endyear+1)
    excel_args = {'export_excel': True, 'excel_sheetname': u'Таблица_6_дс',
        'excel_filename': 'monitoring-2013-theses.%d.xls' % worker.id} if export_excel else {}
    if export_excel:
        headers = make_excel_headers(headers, worker, u"Таблица 6 - дс - Конференции")
    return get_report(request, title, headers, make_row, worker=worker, bibtex_column=1, filters=filters, **excel_args)
