# -*- coding: utf-8; -*-

# Functions for new users registration master

import sys
import logging
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect, get_object_or_404

from actstream import action

from common.utils.context import render_to_response
from common.utils.uniqify import uniqify

from userprofile.views import Profile
from workers.utils import get_initial, get_popular_name_translations
from workers.utils import merge_workers
from workers.models import Degree, ManDegree, Rank, ManRank
from workers.models import WorkerExternalID
from workers.models import Worker, WorkerAlias
from workers.forms import WorkerAliasFormSet
from workers.forms import DegreeForm, OrdinaryRankForm, AcademyRankForm
from workers.forms import EmploymentClaimForm
from workers.forms import WorkerProfileForm
from workers.forms import ExternalIDForm
from workers.forms import inlineformset_factory

from workers.views.profile import save_degree, save_rank
from workers.views.profile import save_workers_alias_formset

from organizations.forms import OrganizationForm
from organizations.models import Department
from unified_permissions import has_permission

logger = logging.getLogger("workers.registration")

debug_logger = logging.getLogger('sentry_debug')


REGISTRATION_STEPS = [
    "workers_registration_fullname",
    "workers_registration_aliases",
    "workers_registration_degree",
    "workers_registration_employment",
    "workers_registration_keywords",
    "workers_registration_other_systems",
    "workers_registration_search",
    "workers_registration_completed"]

NEXT_STEP = dict(zip(REGISTRATION_STEPS[0:-1], REGISTRATION_STEPS[1:]))

def check_registration_step(profile, step_requestd):
    '''Verify that user can open step_requested's page. If not, return redirect object to first valid registration page.'''
    if not profile.registration_completed() and profile.registration_status not in REGISTRATION_STEPS + ['completed']:
        profile.registration_status = REGISTRATION_STEPS[0]
        profile.save()

    if profile.registration_completed() or step_requestd == REGISTRATION_STEPS[0]:
        return None
    if step_requestd not in REGISTRATION_STEPS:
        return redirect(REGISTRATION_STEPS[0])
    if REGISTRATION_STEPS.index(step_requestd) <= REGISTRATION_STEPS.index(profile.registration_status):
        return None
    if REGISTRATION_STEPS.index(step_requestd) == REGISTRATION_STEPS.index(profile.registration_status)+1:
        profile.registration_status = step_requestd
        profile.save()
        return None
    return redirect(NEXT_STEP[profile.registration_status])


class WorkerProfileForm_strict(WorkerProfileForm):
    def __init__(self, *args, **kwargs):
        super(WorkerProfileForm_strict, self).__init__(*args, **kwargs)
        if self.fields.get('lastname', None):
            self.fields['lastname'].required = True
        if self.fields.get('firstname', None):
            self.fields['firstname'].required = True

@login_required
def registration_fullname(request):
    profile, created = Profile.objects.get_or_create(user=request.user)
    worker = profile.worker
    if created:
        profile.registration_status = REGISTRATION_STEPS[0]
        profile.save()

    redirect_or_none = check_registration_step(profile, "workers_registration_fullname")
    if redirect_or_none:
        return redirect_or_none
    main_form = None # worker main info form
    if request.method == "POST":
        main_form = WorkerProfileForm_strict(request.POST, instance=profile)
        if main_form.is_valid():
            profile = main_form.save()
            action.send(request.user, verb=u'добавил данные профиля', target=profile, description=u"%s" % (profile.summary))
            if not worker:
                worker = Worker(creator = request.user,
                                lastname = profile.lastname,
                                firstname = profile.firstname,
                                middlename = profile.middlename)
                worker.save()
                profile.worker = worker
                profile.save()
            messages.success(request, u"Данные успешно сохранены.")
            return redirect(NEXT_STEP["workers_registration_fullname"])

    if not main_form:
        main_form = WorkerProfileForm_strict(instance=profile)

    template = "workers/registration.html"
    data = {'section': 'fullname', 'main_form': main_form, 'worker': worker}
    return render_to_response(request, template, data)


@login_required
def registration_aliases(request):
    profile = get_object_or_404(Profile, user=request.user)
    worker = profile.worker
    alias_formset = None
    aliases_generated = False
    redirect_or_none = check_registration_step(profile, "workers_registration_aliases")
    if redirect_or_none:
        return redirect_or_none

    if request.method == "POST":
        alias_formset = WorkerAliasFormSet(request.POST, instance=worker)
        if worker and alias_formset.is_valid():
            status = save_workers_alias_formset(request.user, worker, alias_formset)
            if status:
                messages.success(request, u'Альтернативные имена сохранены.')
        return redirect(NEXT_STEP["workers_registration_aliases"])

    if worker.aliases.exists():
        alias_formset = WorkerAliasFormSet(None, instance=worker)

    if not alias_formset and worker:
        ts = worker.get_transformations_list()
        popular_name_translations = get_popular_name_translations(worker.firstname)
        # add transliteration
        suggested_aliases = map(lambda(x): {"lastname": x.lastname, "firstname": x.firstname, "middlename": x.middlename}, ts)
        # lastname and full first name, no middle name
        suggested_aliases.extend( map(lambda(x): {"lastname": x.lastname, "firstname": x.firstname, "middlename": None}, ts) )
        for popular_name in popular_name_translations:
            for x in ts:
                if x.lastname != worker.lastname:
                    suggested_aliases.append( {"lastname": x.lastname, "firstname": popular_name, "middlename": None} )
        # add forms with initials
        suggested_aliases.extend( map(lambda(x): {"lastname": x.lastname, "firstname": get_initial(x.firstname), "middlename": get_initial(x.middlename)}, ts) )
        # only first initial
        suggested_aliases.extend( map(lambda(x): {"lastname": x.lastname, "firstname": get_initial(x.firstname), "middlename": None}, ts) )
        suggested_aliases = uniqify(suggested_aliases, idfun=lambda x: unicode(x["lastname"]) + '_' + unicode(x["firstname"]) + '_' + unicode(x["middlename"]))
        WorkerAliasFormSet_factory = inlineformset_factory(Worker, WorkerAlias, fields=('lastname', 'firstname', 'middlename'), extra=len(suggested_aliases))
        alias_formset = WorkerAliasFormSet_factory(initial=suggested_aliases)
        aliases_generated = True
    template = "workers/registration.html"
    data = {'section': 'aliases', 'worker': worker, 'alias_formset': alias_formset, 'aliases_generated': aliases_generated}
    return render_to_response(request, template, data)


def _get_similar_workers(worker):
    '''Find all workers that are similar to one of aliases of the given worker.
    Returns a Worker's queryset.

    Similarity means case-insensitive matching of lastname and firstnames or initials.
    '''
    sql = """
    select distinct *
    from
    (
    select man.f_man_id f_man_id, man.f_man_namel f_man_namel, man.f_man_namef f_man_namef, man.f_man_namem f_man_namem
    from man, manalias
    where  manalias.f_man_id = %s
     and man.f_man_id not in (select worker_id from workers_profile where worker_id is not null)
     and upper(f_manalias_namel)=upper(f_man_namel)
     and (f_man_namef is null or upper(f_manalias_namef)=upper(f_man_namef) or ((length(f_man_namef)=1 or length(f_manalias_namef)=1) and substr(upper(f_manalias_namef),1,1)=substr(upper(f_man_namef),1,1)))
     and (f_man_namem is null or upper(f_manalias_namem)=upper(f_man_namem) or ((length(f_man_namem)=1 or length(f_manalias_namem)=1) and substr(upper(f_manalias_namem),1,1)=substr(upper(f_man_namem),1,1)))
    union all
    select man.f_man_id, man.f_man_namel, man.f_man_namef, man.f_man_namem
    from man, man a
    where  a.f_man_id = %s
     and man.f_man_id in (select man.f_man_id from man minus select worker_id from workers_profile)
     and upper(a.f_man_namel)=upper(man.f_man_namel)
     and (man.f_man_namef is null or upper(a.f_man_namef)=upper(man.f_man_namef) or ((length(man.f_man_namef)=1 or length(a.f_man_namef)=1) and substr(upper(a.f_man_namef),1,1)=substr(upper(man.f_man_namef),1,1)))
     and (man.f_man_namem is null or upper(a.f_man_namem)=upper(man.f_man_namem) or ((length(man.f_man_namem)=1 or length(a.f_man_namem)=1) and substr(upper(a.f_man_namem),1,1)=substr(upper(man.f_man_namem),1,1)))
    )
    """
    return Worker.objects.raw(sql, [worker.id, worker.id])


@login_required
def registration_search(request):
    profile = get_object_or_404(Profile, user=request.user)
    worker = profile.worker
    redirect_or_none = check_registration_step(profile, "workers_registration_search")
    if redirect_or_none:
        return redirect_or_none

    if request.method == 'POST':
        marked_workers = request.POST.getlist("worker_id", [])
        user = request.user
        skip_count = 0
        for w_id in marked_workers:
            try:
                w = Worker.objects.get(pk = w_id)
                if not has_permission(user, "merge_worker", w):
                    skip_count += 1
                    continue
                success, log_list = merge_workers(w, user)
                description = "\n".join(log_list)
                if success:
                    action.send(request.user, verb=u'привязал сотрудника к себе', target=worker, description=description)
                else:
                    print log_list
                    skip_count += 1
            except Exception as e:
                skip_count += 1
        if skip_count == 0 and len(marked_workers)>0:
            messages.success(request, u"Записи успешно объединены с Вашим профилем.")
        elif skip_count > 0:
            debug_logger.error(u'Registration: merge workers error', exc_info=sys.exc_info(),
                               extra={'worker': worker, 'marked_workers': marked_workers})
            messages.error(request, u"Не удалось обработать %s запись(-ей)." % skip_count)
        else:
            return redirect(NEXT_STEP["workers_registration_search"])

    similar_workers = filter(lambda w: has_permission(request.user, "merge_worker", w), list(_get_similar_workers(worker)))
    if len(similar_workers) == 0:
        similar_workers = None
    template = "workers/registration.html"
    data = {'section': 'search', 'worker': worker, 'similar_workers': similar_workers}
    return render_to_response(request, template, data)


@login_required
def registration_degree(request):
    try:
        profile = get_object_or_404(Profile, user=request.user)
        worker = profile.worker
    except:
        return redirect("workers_registration_degree")
    redirect_or_none = check_registration_step(profile, "workers_registration_degree")
    if redirect_or_none:
        return redirect_or_none

    degree_form = DegreeForm()
    ordinary_rank_form = OrdinaryRankForm(prefix="ordinary")
    academy_rank_form = AcademyRankForm(prefix="academy")
    degree_status = academy_status = ordinary_status = None
    show_form = None
    if request.method == "POST":
        if request.POST.get('nextstep', None):
            return redirect(NEXT_STEP["workers_registration_degree"])
        degree_status = save_degree(request, worker)
        academy_status = save_rank(request, worker, "academy")
        ordinary_status = save_rank(request, worker, "ordinary")
        if degree_status == False:
            show_form = 'degree'
            degree_form = DegreeForm(request.POST)
        if academy_status == False:
            show_form = 'academy'
            academy_rank_form = AcademyRankForm(request.POST, prefix="academy")
        if ordinary_status == False:
            show_form = 'ordinary'
            ordinary_rank_form = OrdinaryRankForm(request.POST, prefix="ordinary")
        if degree_status or academy_status or ordinary_status:
            return redirect("workers_registration_degree")
    return render_to_response(request, "workers/registration.html",
                             {'section': 'degrees',
                             'degree_form': degree_form,
                             'ordinary_rank_form': ordinary_rank_form,
                             'academy_rank_form': academy_rank_form,
                             'show_form': show_form,
                             'worker': worker})


@login_required
def registration_keywords(request):
    try:
        profile = request.user.get_profile()
        worker = profile.worker
    except:
        return redirect("workers_registration_keywords")
    redirect_or_none = check_registration_step(profile, "workers_registration_keywords")
    if redirect_or_none:
        return redirect_or_none

    if request.method == "POST":
        profile.keywords = request.POST.get('keywords', '')[:1800]
        profile.engkeywords = request.POST.get('engkeywords', '')[:1800]
        profile.save()
        return redirect(NEXT_STEP["workers_registration_keywords"])

    template = "workers/registration.html"
    data = {'section': 'keywords', 'worker': worker, 'keywords_form': True, 'keywords': profile.keywords, 'engkeywords': profile.engkeywords}
    return render_to_response(request, template, data)


def save_employment_claim(request, employment_form, worker, redirect_on_failure="workers_profile_edit"):
    department = None

    if employment_form.is_valid():
        try:
            department = Department.objects.get(id=employment_form.cleaned_data['dep'])
        except UnboundLocalError:
            return redirect(redirect_on_failure)

        employment = employment_form.save(commit=False)
        create = not bool(employment.id) # are we creating new or editing
        if not create and employment.worker != worker:
            return redirect(redirect_on_failure)
        else:
            employment.worker = worker
        employment.creator = request.user
        employment.department = department
        employment.save()
        if request.user:
            action.send(request.user, verb=(u'добавил' if create else u'изменил') + u' место работы', action_object=employment, target=employment.worker)
        messages.success(request, u"Место работы успешно %s." % (u'добавлено' if create else u'изменено'))
        return True
    return False


@login_required
def registration_employment(request):
    profile = get_object_or_404(Profile, user=request.user)
    worker = profile.worker
    redirect_or_none = check_registration_step(profile, "workers_registration_employment")
    if redirect_or_none:
        return redirect_or_none

    if request.method == "POST":
        if request.POST.get('nextstep', None):
            return redirect(NEXT_STEP["workers_registration_employment"])
        if request.POST.get('institution', None):
            worker.institution = request.POST.get('institution')
            worker.save()
            messages.success(request, u'Место работы во внешней организации сохранено.')
            return redirect("workers_registration_employment")
        employment_form = EmploymentClaimForm(request.POST)
        if save_employment_claim(request, employment_form, worker=worker, redirect_on_failure="workers_registration_employment"):
            return redirect("workers_registration_employment")

    organization_form = OrganizationForm(request.POST or None)
    if not 'employment_form' in locals():
        employment_form = EmploymentClaimForm(request.POST)

    return render_to_response(request, "workers/registration.html",
                              {'section': 'employment',
                               'organization_form': organization_form,
                               'employment_form': employment_form,
                               'worker': worker,
                               'registration': True,
                               'can_edit': False})


@login_required
def delete_external_id(request, pk):
    try:
        profile = get_object_or_404(Profile, user=request.user)
        worker = profile.worker
        eid = WorkerExternalID.objects.get(worker=worker, pk=pk)
        msg = u"Идентификатор в системе '%s' удалён." % eid.system_name
        eid.delete()
        messages.success(request, msg)
    except Exception:
        messages.error(request, u'Не удалось удалить запись')
    return redirect("workers_registration_other_systems")

@login_required
def registration_other_systems(request):
    profile = get_object_or_404(Profile, user=request.user)
    worker = profile.worker
    redirect_or_none = check_registration_step(profile, "workers_registration_other_systems")
    if redirect_or_none:
        return redirect_or_none

    if request.method == "POST" and request.POST.get('nextstep', None):
        return redirect(NEXT_STEP["workers_registration_other_systems"])

    form = ExternalIDForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        new_id = form.save(commit=False)
        new_id.worker = worker
        try:
            eid = WorkerExternalID.objects.get(worker=worker, system=new_id.system, system_other=new_id.system_other)
            eid.delete()
        except WorkerExternalID.DoesNotExist:
            pass
        new_id.save()
        messages.success(request, u"Данные сохранены.")
        form = ExternalIDForm()

    template = "workers/registration.html"
    data = {'section': 'other_systems', 'worker': worker, 'other_systems_form': form}
    return render_to_response(request, template, data)


@login_required
def registration_completed(request):
    profile = get_object_or_404(Profile, user=request.user)
    worker = profile.worker
    redirect_or_none = check_registration_step(profile, "workers_registration_completed")
    if redirect_or_none:
        return redirect_or_none

    profile.registration_status = 'completed'
    profile.save()

    template = "workers/registration.html"
    data = {'section': 'completed', 'worker': worker}
    return render_to_response(request, template, data)
