# -*- coding: utf-8 -*-
import logging

from django.db.models import get_model
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.http import Http404

from common.utils.activities import get_activities
from common.utils.list import flatten, is_tuple_list
from common.utils.user import get_profile

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.template.loader import render_to_string
from common.utils.latex import latex_to_pdf

logger = logging.getLogger("workers.utils")


def mylog(message, log_variable, log_function=logger.debug):
    '''Log a message by log_function and also store to log_variable list.'''
    log_function(message)
    try:
        log_variable.append(message)
    except AttributeError:
        pass

def get_initial(name):
    '''Construct correct initial for given name.'''
    #-- TODO: Yuri -> Yu, etc
    if name:
        return name[0]
    else:
        return name


def get_fullname_raw(lastname=None, firstname=None, middlename=None, initials=False, comma=False):
    '''Returns last name + first name + middle name with left out blank fields.

    If initials is true, only last name and  initials are returned.
    If comma is True, extra comma is added after lastname, e.g. Ivanov, I.A. Needed for meta tags in rector's format.
    '''
    name = ""
    if lastname:
        name = lastname
    if firstname:
        if name:
            if comma:
                name += ","
            name += " "
        if initials:
            firstname = get_initial(firstname)
        if len(firstname) == 1:
            firstname = firstname + "."
        name += firstname
    if middlename:
        if comma and name and not firstname:
            name += ","
        if name and name[-1] != ".": # don't insert a space after firstname initial.
            name += " "
        if initials:
            middlename = get_initial(middlename)
        if len(middlename) == 1:
            middlename = middlename + "."
        name += middlename
    return name

def get_popular_name_translations(firstname, min_length=3, trashold=0.1):
    '''Return popular translations of given name using users' profiles statistics.

    min_length defines minimal required length of an alias selected. trashold
    controls dynamic selection. An alias is included into result only if its
    frequency is at least trashold of the most frequent alias for the firstname.
    '''
    from django.db import connection

    cursor = connection.cursor()
    sql = """
    select name, percent, prev_percent
    from (
        select name, cnt, percent,
               lag(percent, 1, percent)  over (order by cnt desc) as prev_percent,
               lead(percent, 1, 0) over (order by cnt desc) as next_percent,
               first_value(percent) over () first
        from
        (
           /* aliases with frequences */
           with all_aliases as (
             select rtrim(ltrim(manalias.f_manalias_namef)) as name, wp.id as wp_id
             from manalias, workers_profile wp
             where wp.worker_id = manalias.f_man_id
               and upper(wp.firstname) <> upper(manalias.f_manalias_namef)
               and manalias.f_manalias_namef is not null
               and wp.firstname = %s /*(firstname)s*/
           )
           select name, count(distinct wp_id) cnt, count(distinct wp_id)/max(nwp.cnt) percent
           from all_aliases, (select count(distinct wp_id) cnt from all_aliases) nwp
           where length(name) >= %s /* (min_length)s */
           group by name, nwp.cnt
        )
    )
    where percent > first * %s /* (trashold)s */
    order by 2 desc
    """
    cursor.execute(sql, [firstname, min_length, trashold])
    #cursor.execute(sql, {"firstname": firstname, "trashold": 0.1, "min_length": min_length} # django 1.5

    names = [item[0] for item in cursor]
    return names


def get_worker(man, log_list=None):
    '''Take an argument that can be an id of a worker or a worker instance or a username or a user instance.'''

    Worker = get_model('workers', 'Worker')
    User = get_model('auth', 'User')
    worker = None
    if isinstance(man, int): # id of a worker
        try:
            worker = Worker.objects.get(id=man)
        except:
            mylog(u"Worker with id %d not found" % man, log_list, logger.warning)
    elif isinstance(man, Worker):
        worker = man
    elif isinstance(man, (unicode, str)):
        try:
            worker = User.objects.get(username=man).get_profile().worker
        except:
            mylog(u"User with username %s not found" % man, log_list, logger.warning)
    elif isinstance(man, User):
        try:
            worker = man.get_profile().worker
        except:
            mylog(u"User %s not linked to a worker" % man, log_list, logger.warning)
    return worker


def merge_workers(men_from, man_to, delete=True):
    '''Relinks all activities from men_from to man_to. If delete is True, deletes the workers_from.
    men_from is a list/tuple containing man_from. man_from and man_to can be either an id of a worker, a Worker instance, a username or a User instance.
    '''
    from common.models import get_related_fields
    log_list = []
    mylog(u"Args: %s, %s, %s" % (men_from, man_to, delete), log_list)
    if not is_tuple_list(men_from):
        men_from = (men_from,)
    Worker = get_model('workers', 'Worker')
    WorkerAlias = get_model('workers', 'WorkerAlias')
    worker_to = get_worker(man_to)
    if not worker_to:
        mylog(u"Destination worker has not been recognized", log_list)
        return 0, log_list # fail
    for man_from in men_from:
        worker_from = get_worker(man_from)
        if not worker_from:
            mylog(u"Source worker `%s' has not been recognized" % (man_from,), log_list)
            return 0, log_list # fail
        if worker_from == worker_to:
            mylog(u"Can not merge worker to himself", log_list)
            return 0, log_list # fail
        mylog(u"Merging worker %s (id %d) to worker %s (id %d)" % (worker_from, worker_from.id, worker_to, worker_to.id), log_list)
        # if both workers are linked to users, do not do anything, since it can be dangerous
        try:
            user_from = worker_from.profile.user
            user_to = worker_to.profile.user
        except:
            pass
        else:
            mylog(u"ERROR! Both workers are linked to users (%s - %s, %s - %s). Merging is dangerous, skipping the workers." % (worker_from, user_from, worker_to, user_to), log_list)
            continue
        # copy worker attributes, if they are blank in the target worker
        for attr in ['firstname', 'middlename', 'lastname', 'birthdate']:
            if getattr(worker_from, attr) and not getattr(worker_to, attr):
                mylog(u"Copying attr %s=%s" % (attr, getattr(worker_from, attr)), log_list)
                setattr(worker_to, attr, getattr(worker_from, attr))
        # save worker_to
        worker_to.save()
        # move profile
        try:
            profile_from = worker_from.profile
        except:
            profile_from = None
        else:
            try:
                profile_to = worker_to.profile
            except: # target worker is not linked to a profile
                mylog(u"Moving profile %s, id %d" % (profile_from, profile_from.id), log_list)
                profile_from.worker = worker_to
                profile_from.save()
        # move aliases
        for alias in worker_from.aliases.all():
            # filter out duplicate aliases
            params = {'firstname': alias.firstname, 'middlename': alias.middlename, 'lastname': alias.lastname}
            if not worker_to.aliases.filter(**params).exists(): # duplicate alias not exists in worker_to
                mylog(u"Moving alias %s, id %d" % (alias, alias.id), log_list)
                alias.worker = worker_to
                alias.save()
        # create alias with worker_form name, if it does not exist
        params = {'firstname': worker_from.firstname, 'middlename': worker_from.middlename, 'lastname': worker_from.lastname}
        if not worker_to.aliases.filter(**params).exists(): # duplicate alias not exists in worker_to
            mylog(u"Creating alias from worker_from name: %s %s %s" % (params['lastname'], params['firstname'], params['middlename']), log_list)
            params.update({'worker': worker_to, 'validated': False})
            WorkerAlias.objects.create(**params)
        # move employments
        for employment in worker_from.employments.all():
            similar_employments_qs = worker_to.employments.filter(department=employment.department, position=employment.position)
            if similar_employments_qs.exists():
                mylog(u"Deleting employment %s, id %d" % (employment, employment.id), log_list)
                employment.delete()
            else:
                mylog(u"Moving employment %s, id %d" % (employment, employment.id), log_list)
                employment.worker = worker_to
                employment.save()
        # school members
        worker_from.schoolmembers.all().update(worker=worker_to)
        # degrees
        worker_from.degrees.all().update(worker=worker_to)
        # ranks
        worker_from.ranks.all().update(worker=worker_to)
        # achievements
        worker_from.achievement_authorships.all().update(author=worker_to)
        #
        # move activities
        activities = get_activities(worker_from)
        activities = flatten(activities.values()) # transform grouped activities into a flat list
        for activity in activities:
            mylog(u"Moving activity %s of type %s, id %d" % (activity, activity.__class__, activity.id), log_list)
            if hasattr(activity, 'worker_attr'): # workership model, is linked to a single worker
                activity.change_worker(worker_to)
            else:
                activity.change_worker(worker_from, worker_to)
        # move other related objects, that were not covered above
        # this is done not to accidentally delete related objects
        # that we forgot to move manually
        # due to cascade delete by default in django
        for related_name, field_name in get_related_fields(worker_from):
            try:
                # e.g. worker_from.reports.all().update(author=worker_to)
                getattr(worker_from, related_name).all().update(**{field_name: worker_to})
            except:
                # this shouldn't happen, but it happens for manually created tables
                # print related_name, field_name
                pass
        # delete worker_from
        try:
            # do not delete the worker_from if it is still linked to a user
            user_from = Worker.objects.get(id=worker_from.id).profile.user # reselect worker, otherwise its former profile link was not cleared and it seems that two workers have the same profile (due to caching)
        except: # worker_from was not linked to a profile, or the profile has been moved. The worker can be deleted.
            if delete:
                mylog(u"Deleting worker %s, id %d" % (worker_from, worker_from.id), log_list)
                worker_from.delete()
    return 1, log_list # success

def merge_users(username_from, username_to):
    '''Merges user_from to user_to. user_from is set inactive, and its worker is merged to user_to's worker.'''
    User = get_model('auth', 'User')
    user_from = User.objects.get(username=username_from)
    user_to = User.objects.get(username=username_to)
    # if not user_from.email.lower() == user_to.email.lower():
    #     raise Exception("emails are different, merging is not allowed")

    # copy profile fields
    profile_from = user_from.get_profile()
    profile_to = user_to.get_profile()
    for attr in ["firstname", "middlename", "lastname", "researcher_id"]:
        if getattr(profile_from, attr) and not getattr(profile_to, attr):
            setattr(profile_to, attr, getattr(profile_from, attr))

    # move worker, if needed
    if not profile_to.worker and profile_from.worker:
        profile_to.worker = profile_from.worker
        profile_from.worker = None
        profile_from.save()
        profile_to.save()
        # copy new profile name to worker, moving old worker name to alias
        profile_to.worker.copy_name_from_profile()
    profile_to.save()

    # copy avatar if user_to does not have it
    if user_from.avatar_set.exists() and not user_to.avatar_set.exists():
        user_from.avatar_set.all().update(user=user_to)

    # create worker alias from profile_to.fullname
    worker_from = get_worker(user_from)
    worker_to = get_worker(user_to)
    if worker_to and (profile_from.firstname or profile_from.middlename or profile_from.lastname):
        try:
            worker_to.aliases.get_or_create(firstname=profile_from.firstname, middlename=profile_from.middlename, lastname=profile_from.lastname)
        except:
            logger.warning("Duplicated aliases in profiles merge: username_to=%s, username_from=%s", username_to, username_from)

    # merge workers
    if worker_from:
        profile_from.worker = None
        profile_from.save()
        merge_workers(worker_from, worker_to)
    user_from.is_active = False
    user_from.save()

    # update related objects
    from common.models import get_related_fields
    for related_name, field_name in get_related_fields(user_from):
        # exclude some related objects
        if related_name in ["avatar_set", "emailvalidation_set", "profile_set"]:
            continue
        # e.g. user_from.reports_added.all().update(creator=user_to)
        try:
            getattr(user_from, related_name).all().update(**{field_name: user_to})
        except:
            # this shouldn't happen, but it happens for manually created tables
            # print related_name, field_name
            pass
    # invalidate user cache
    from common.utils.cache import invalidate_user
    invalidate_user(user_to.username)

def name_extends(fullname, initials):
    '''Returns True if the triple @fullname=(lastname, firstname, middlename) is an extension of @initials name.'''
    last1, first1, middle1 = fullname
    last2, first2, middle2 = initials
    return last1.startswith(last2) and first1.startswith(first2) and middle1.startswith(middle2)

def update_profile_name(user, lastname, firstname, middlename):
    worker = get_worker(user)
    # create worker alias with old profile name
    worker.add_alias_from_profile()
    # update profile name
    profile = get_profile(user)
    profile.lastname, profile.firstname, profile.middlename = lastname, firstname, middlename
    profile.save()


def get_all_named_representations(worker):
    worker_representations = [worker]
    if worker.get_profile():
        worker_representations.append(worker.profile)
    worker_representations.extend(worker.aliases.all())
    return worker_representations


def get_all_fullnames(worker):
    return [dict((f, getattr(alias, f)) for f in ("lastname", "firstname", "middlename")) for alias in
            get_all_named_representations(worker)]

def make_fullname_from_names(lastname, firstname, middlename):
    delimiter = "" if len(firstname) == 1 else " "  # between firstname and middlename.
    add_dot = lambda x: len(x) == 1 and x + "." or x
    return " ".join(("%s %s%s%s" % (lastname, add_dot(firstname), delimiter, add_dot(middlename))).split())

def make_fullname_from_dict(name_dict):
    """Build fullname from dict. Used also to construct back original name of the worker and save it to Workership."""
    try:
        l, f, m = [name_dict[key] for key in ['lastname', 'firstname', 'middlename']]
    except:
        return None
    return make_fullname_from_names(l, f, m)

def worker_year_report_export_to_pdf(user, worker, years_str, activities, object_list, isTempReport=False):
    latex = render_to_string("workers/worker_report_year.tex",
                            {'worker': worker, 'years_str': years_str, 'activities': activities, 'department': object_list.department})
    pdf = latex_to_pdf(latex, pdflatex='xelatex')

    if isTempReport:
        return pdf

    tempfile = NamedTemporaryFile(delete=True)
    tempfile.write(pdf)
    tempfile.flush()

    filename = "worker_year_report_%d_%d_%s.pdf" % (worker.id, object_list.department.id, years_str)
    AttachmentCategory = get_model("common", "AttachmentCategory")
    category, _ = AttachmentCategory.objects.get_or_create(name=u"PDF-версия отчета")

    object_list.attachments.all().delete()
    attachment = object_list.attachments.create(
        link=File(tempfile, name=filename),
        category=category,
        creator=user)

    return attachment

