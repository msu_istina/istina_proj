# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from common.utils.user import get_worker, get_profile
from workers.models import WorkerAlias
from workers.utils import name_extends

class Command(BaseCommand):
    def handle(self, *args, **options):
        '''If fix argument is provided, change workers' names that are extended by profiles names. Conflicted names are not extended.'''
        fix = (len(args) == 1 and args[0] == "fix") # fix names or just print them
        count = {"extend": 0, "conflict": 0, "equal": 0, "fixed": 0}
        for user in User.objects.all():
            worker = get_worker(user)
            profile = get_profile(user)
            if worker and profile:
                profile_name = profile.lastname.strip(), profile.firstname.strip(), profile.middlename.strip()
                worker_name = worker.lastname.strip(), worker.firstname.strip(), worker.middlename.strip()
                if profile_name != worker_name:
                    if name_extends(profile_name, worker_name):
                        status = "E" # extends
                        count["extend"] += 1
                    else:
                        status = "C" # conflict
                        count["conflict"] += 1
                    print "%s %s %s %s, %s %s %s [%s]" % (status, profile.lastname, profile.firstname, profile.middlename, worker.lastname, worker.firstname, worker.middlename, user.username)
                    if fix and status == "E": # fix extends
                        # create Alias
                        kwargs = {'worker': worker, 'lastname': worker.lastname.strip(), 'firstname': worker.firstname.strip(), 'middlename': worker.middlename.strip()}
                        if not WorkerAlias.objects.filter(**kwargs).exists():
                            WorkerAlias.objects.create(**kwargs)
                        # change name of the worker
                        worker.lastname, worker.firstname, worker.middlename = profile_name
                        worker.save()
                        count["fixed"] += 1
                else:
                    count["equal"] += 1
        for key, value in count.items():
            print "Names %s: %d" % (key, value)
