# -*- coding: utf-8 -*-
"""Update list of workers"""
from django.core.management.base import BaseCommand
import datetime

from workers import wcache


class Command(BaseCommand):
    help = __doc__

    def handle(self, *args, **options):
        options["verbosity"] = int(options["verbosity"])
        last_update_at, list_simplified_workers = wcache.recalculate_worker_list_cache()
        if options["verbosity"] >= 1:
            print("Cached: %d workers" % len(list_simplified_workers))
            print("Last updated worker: %s" % datetime.datetime.fromtimestamp(last_update_at))
