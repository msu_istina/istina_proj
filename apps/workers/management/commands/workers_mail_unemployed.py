# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from common.utils.user import get_worker, get_profile, get_fullname
from workers.models import WorkerAlias
from workers.utils import name_extends
from organizations.models import Department
import datetime
from os import path
from settings import ENV_DIR
from time import sleep

class Command(BaseCommand):
    def handle(self, *args, **options):
        '''Send a mail to all users that have no active employment in a department of a >=2 level.'''
        users = {'unlinked': [], 'unemployed': [], 'work_in_root': [], 'no_children': [], 'ok': []}
        users_count = User.objects.filter(is_active=True).count()
        for i, user in enumerate(User.objects.filter(is_active=True)):
            print "%d of %d" % (i+1, users_count)
            worker = get_worker(user)
            if not worker:
                users['unlinked'].append(user)
            else:
                employments = worker.employments.exclude(enddate__lt=datetime.date.today())
                if not employments.exists():
                    users['unemployed'].append(user)
                else:
                    departments = Department.objects.filter(id__in=employments.values_list('department', flat=True))
                    # there should exist an employment in a department which level is > 0 (i.e. not root department)
                    # or all the departments should not contain any children
                    if not any([department.level > 0 for department in departments]):
                        if not any([department.current_children for department in departments]):
                            users['no_children'].append(user)
                        else:
                            users['work_in_root'].append(user)
                    else:
                        users['ok'].append(user)
        f = open(path.join(ENV_DIR, "var", "log", "unemployed.txt"), 'w')
        if users['unlinked']:
            message = u"Пользователи, не связанные с сотрудником (%d):\n%s\n\n" % (len(users['unlinked']), "\n".join(user.username for user in users['unlinked']))
            f.write(message.encode('utf8'))
        if users['unemployed']:
            message = u"Пользователи, не указавшие место работы (%d): \n%s\n\n" % (len(users['unemployed']), "\n".join(user.username for user in users['unemployed']))
            f.write(message.encode('utf8'))
        if users['no_children']:
            message = u"Пользователи, работающие в подразделениях без детей (%d): \n%s\n\n" % (len(users['no_children']), "\n".join(user.username for user in users['no_children']))
            f.write(message.encode('utf8'))
        if users['work_in_root']:
            message = u"Пользователи, работающие в корневых подразделениях (%d): \n%s\n\n" % (len(users['work_in_root']), "\n".join(user.username for user in users['work_in_root']))
            f.write(message.encode('utf8'))
        if users['ok']:
            message = u"Пользователей, которые указали нормальное место работы: %d" % len(users['ok'])
            f.write(message.encode('utf8'))
        f.close()

        # mail users
        subject = u"Просьба ввести информацию о месте работы в систему Наука-МГУ (ИСТИНА)"
        from_email = u"ИСТИНА <info@istina.msu.ru>"
        logfile = open(path.join(ENV_DIR, 'var', 'log', 'email_unemployed.log'), 'w')
        keys = ['unlinked', 'unemployed', 'work_in_root']
        templates = ['workers/emails/unlinked.txt', 'workers/emails/unemployed.txt', 'workers/emails/work_in_root.txt'] # hardcoded deliberately to make it easier to later find references to files in source code
        for key, template in zip(keys, templates):
            for user in users[key]:
                name = get_fullname(user)
                message = render_to_string(template, {'name': name})
                #send_mail(subject, message, from_email, [user.email])
                log_message = u"%s %s %s\n" % (key, name, user.email)
                logfile.write(log_message.encode('utf8'))
                print log_message,
                sleep(1)
        logfile.close()
