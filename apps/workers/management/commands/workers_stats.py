# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
# from common.utils.user import get_worker, get_profile
# from workers.utils import name_extends
# from organizations.models import Department
# from workers.models import Employment
from django.db.models.loading import get_model
import datetime

class Command(BaseCommand):
    def handle(self, *args, **options):
        '''If fix argument is provided, change workers' names that are extended by profiles names. Conflicted names are not extended.'''
        Worker = get_model("workers", "Worker")
        Employment = get_model("workers", "Employment")
        Department = get_model("organizations", "Department")
        department_ids = [275695, 275572]
        position_id = 405497
        for dep_id in department_ids:
            department = Department.objects.get(id=dep_id)
            department_tree = department.descendants_ids(include_self=True)
            worker_ids = Employment.objects.exclude(enddate__lt=datetime.date.today()).filter(position=position_id, department__in=department_tree).values_list('worker', flat=True)
            workers = Worker.objects.filter(id__in=worker_ids)
            sums = [0]*15
            for i, worker in enumerate(workers, start=1):
                values = [
                    i,
                    len(worker.activities['articles']),
                    len(worker.activities['books']),
                    len(worker.activities['conference_presentations']),
                    len(worker.activities['journal_editorial_board_memberships']),
                    len(worker.activities['conference_committee_memberships']),
                    len(worker.activities['dissertation_councils_memberships']),
                    len(worker.activities['dissertations_advised']),
                    len(worker.activities['diplomas']),
                    len(worker.activities['courses']),
                    len(set(teaching.course for teaching in worker.activities['courses_teachings'])),
                    [value['value'] for value in worker.citation_counts_dict() if value['system__full_name'] == 'Web of Science'][0],
                    [value['value'] for value in worker.citation_counts_dict() if value['system__full_name'] == 'Scopus'][0],
                    len(worker.ext_articles_ids),
                    len(worker.vak_articles),
                ]
                for j, value in enumerate(values):
                    sums[j] += value
            for i, value in enumerate(sums):
                sums[i] = sums[i] / float(len(workers))
            sums[0] = len(workers)
            print dep_id
            for value in sums:
                print value

