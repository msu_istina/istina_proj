# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from common.utils.user import get_worker, get_profile, get_fullname
from workers.models import WorkerAlias, Worker
from workers.utils import name_extends
from organizations.models import Department, Organization
import datetime
from os import path
from settings import ENV_DIR
from time import sleep

def get_users():
    workers_ids = Department.objects.get(id=275774).get_workers(return_ids=True) # ISAA workers
    users_ids = Worker.objects.filter(id__in=workers_ids).values_list('profile__user', flat=True)
    users = User.objects.filter(id__in=users_ids, is_active=True)
    return users

def get_msu_users():
    msu = Organization.objects.get(id=214524)
    workers = msu.get_workers(state='all')
    users_ids = workers.values_list('profile__user', flat=True)
    users = User.objects.filter(id__in=users_ids, is_active=True)
    return users

class Command(BaseCommand):
    def handle(self, *args, **options):
        '''Send a mail to all users that satisfy certain condition.'''
        subject = u"Формирование отчетов в системе Наука МГУ - ИСТИНА"
        from_email = u"ИСТИНА <info@istina.msu.ru>"
        logfile = open(path.join(ENV_DIR, 'var', 'log', 'email_add_your_data.log'), 'w')
        template = 'workers/emails/add_your_data.txt'
        users = get_msu_users()
        for user in users:
            name = get_fullname(user)
            message = render_to_string(template, {'name': name, 'username': user.username})
            #send_mail(subject, message, from_email, [user.email])
            log_message = u"%s %s\n" % (name, user.email)
            logfile.write(log_message.encode('utf8'))
            print log_message,
            #print message
            sleep(1)
        logfile.close()
