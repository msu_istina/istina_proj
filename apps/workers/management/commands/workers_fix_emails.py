# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from common.utils.user import get_worker, get_profile
from settings import ENV_DIR
from workers.models import WorkerAlias
from workers.utils import name_extends
import re
from os import path
from collections import namedtuple

class Command(BaseCommand):
    def handle(self, *args, **options):
        '''Change emails for users provided in a text file.'''
        f = open(path.join(ENV_DIR, "tmp", "emails.txt"))
        NewData = namedtuple('NewData', 'username, user, new_email')
        data = []
        for line in f:
            if not line.strip():
                continue
            try:
                username, email = re.match(r'^([^ ]+)\ ([^\s]+)\s$', line.decode("utf8"), re.UNICODE|re.DOTALL).groups()
            except AttributeError:
                import pdb; pdb.set_trace()
            else:
                try:
                    user = User.objects.get(username=username)
                except:
                    user = None
                data.append(NewData(username, user, email))
        f.close()
        for item in data:
            if not item.user:
                print "E user %s not found" % item.username
            elif item.user.email != 'gala@imec.msu.ru':
                if item.user.email.lower() == item.new_email.lower():
                    print "OK user %s already has the same email: %s" % (item.username, item.user.email)
                else:
                    print "E user %s has an email: %s, new email: %s" % (item.username, item.user.email, item.new_email)
            else:
                print "OK email for user %s changed from %s to %s" % (item.username, item.user.email, item.new_email)
                item.user.email = item.new_email
                item.user.save()

