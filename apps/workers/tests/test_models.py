# -*- coding: utf-8 -*-
from django.test import TestCase
from common.utils.factories import WorkerFactory, WorkerAliasFactory, SimilarWorkerFactory, UserFactory
from workers.models import Worker


class WorkerMergeSimilarityTests(TestCase):
    def setUp(self):
        self.worker_1 = WorkerFactory()
        self.worker_2 = WorkerFactory()

    def assert_similar(self):
        self.assertTrue(self.worker_1.is_similar_to(self.worker_2))
        self.assertTrue(self.worker_2.is_similar_to(self.worker_1))

    def assert_not_similar(self):
        self.assertFalse(self.worker_1.is_similar_to(self.worker_2))
        self.assertFalse(self.worker_2.is_similar_to(self.worker_1))

    def test_worker_same_lastnames(self):
        self.worker_1.lastname = self.worker_2.lastname
        self.worker_1.save()
        self.assert_similar()

    def test_worker_same_aliases(self):
        alias_1 = WorkerAliasFactory(worker=self.worker_1)
        alias_2 = WorkerAliasFactory(lastname=alias_1.lastname, worker=self.worker_2)
        self.assert_similar()

    def test_worker_swapped_name_and_lastname(self):
        self.worker_2.lastname = self.worker_1.firstname
        self.worker_2.firstname = self.worker_1.lastname
        self.worker_2.save()
        self.assert_similar()

    def test_worker_different_lastnames_aliases(self):
        alias_1, alias_2 = WorkerAliasFactory(worker=self.worker_1), WorkerAliasFactory(worker=self.worker_2)
        self.assert_not_similar()

    def test_merge_list_contains_similar(self):
        workers_num = 3
        workers = SimilarWorkerFactory.create_batch(workers_num)
        user = UserFactory()
        user.save()
        user_worker = workers[0]
        user.get_profile().worker = user_worker
        for worker in workers:
            if worker is not user_worker:
                self.assertIn(worker, Worker.objects.get_merge_candidates(user))
                self.worker_1 = user_worker
                self.worker_2 = worker
                self.assert_similar()




