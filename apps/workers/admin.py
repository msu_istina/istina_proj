from django.contrib import admin
from common.utils.admin import register_with_versions, MyModelAdmin
from .models import Employment, Profile, Worker, WorkerAlias, Academy, Degree, ManDegree, Rank, ManRank, Position


class PersonAdmin(MyModelAdmin):
    list_display = ['__unicode__', 'lastname', 'firstname', 'middlename']
    search_fields = ['lastname', 'firstname', 'middlename']


class ProxyWorkerAdmin(PersonAdmin):
    list_display = ['__unicode__', 'lastname', 'firstname', 'middlename', 'created_at', 'updated_at']

class ManRankAdmin(MyModelAdmin):
    list_display = ['worker', 'department', 'rank', 'academy', 'date', 'branch_num']

class ManDegreeAdmin(MyModelAdmin):
    list_display = ['worker', 'degree', 'year']
    
admin.site.register(Profile, PersonAdmin)
admin.site.register(Worker, ProxyWorkerAdmin)
admin.site.register(WorkerAlias, ProxyWorkerAdmin)
register_with_versions(Employment)
register_with_versions(Academy)
register_with_versions(Degree)
admin.site.register(ManDegree, ManDegreeAdmin)
register_with_versions(Rank)
admin.site.register(ManRank, ManRankAdmin)
register_with_versions(Position)