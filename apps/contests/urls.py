# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from contests.views import SurveyWizard

urlpatterns = patterns(
    'contests.views',
    url(r'^$', 'contests_list', name='contests_list'),
    url(r'^(?P<slug>[\w-]+)/$', 'contest_detail', name='contests_detail'),
    url(r'^(?P<contest_id>\d+)/submit/$', 'submit_application', name="contests_application_submit"),
    url(r'^(?P<contest_id>\d+)/cancel/$', 'cancel_application', name="contests_application_cancel"),
    url(r'^(?P<contest_id>\d+)/sign/$', 'signSurvey', name="contests_application_sign_survey"),
    url(r'^(?P<contest_id>\d+)/applications/$', 'contests_view_applications', name="contests_applications_list"),
    # url(r'^sp/(?P<sp_id>\d+)/$', 'showsurvey', name="surveypage"),
    url(r'^(?P<slug>[\w-]+)/survey/',"surveyWizardView",name="surveywizard"),
    url(r'^(?P<slug>[\w-]+)/print/',"printSurvey",name="printSurvey"),
    url(r'^(?P<slug>[\w-]+)/results/',"showExpertConclusions",name="contest_results"),
    # expert
    url(r'^expert/list/$', 'list_expert_contests', name='list_expert_contests'),
    url(r'^expert/(?P<slug>[\w-]+)/survey/(?P<contestmanlink_id>\d+)/$', 'expertsurvey', name="expertSurvey" ),
    url(r'^expert/(?P<slug>[\w-]+)/survey/(?P<contestmanlink_id>\d+)/application/$', 'showSurvey', name="expertShowApplication" ),
    url(r'^expert/(?P<slug>[\w-]+)/conclusions/(?P<conclusion_id>\d+)/$', 'showConclusion', name="expertShowConclusion" ),
    url(r'^expert/(?P<slug>[\w-]+)/conclusion/(?P<contestmanlink_id>\d+)/$', 'contests_expert_conclusion', name='contests_expert_conclusion'),
    url(r'^expert/(?P<slug>[\w-]+)/conclusion/(?P<contestmanlink_id>\d+)/(?P<department_id>\d+)/$', 'contests_expert_conclusion', name='contests_expert_conclusion'),
    url(r'^expert/(?P<slug>[\w-]+)/((?P<department_group>\w+)/)?((?P<department_id>\d+)/)?((?P<status>[\w\+]+)/)?$', 'contests_expert', name='contests_expert'),
    url(r'^applications/(?P<application_id>\d+)/set_status/$', 'application_set_status', name='contests_application_set_status'),

    # temporary
    url(r'^verify/(?P<slug>[\w-]+)/projects/$', 'contests_verify_projects', name='contests_verify_projects')
)
