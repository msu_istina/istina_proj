# -*- coding: utf-8 -*-
import datetime
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from common.managers import MyManager


class ContestManager(MyManager):
    '''Model manager for Contest model.'''

    def current(self):
        today = datetime.date.today()
        return self.filter(parent__isnull=True).filter(Q(startdate__isnull=True) | Q(startdate__lte=today)).filter(Q(deadline__isnull=True) | Q(deadline__gte=today) | Q(deadline_extended__gte=today))

    def recent(self):
        today = datetime.datetime.today()
        recent = today - datetime.timedelta(days=60)
        return self.filter(
            parent__isnull=True).filter(
            Q(deadline__isnull=True) | Q(deadline__gte=recent) | Q(deadline_extended__gte=recent))

    def clean_duplicates(self):
        for contest in self.all():
            for application in contest.applications.all():
                other_apps = contest.applications.filter(worker__id=application.worker.id).exclude(id=application.id)
                if other_apps.exists():
                    print contest.slug, application.worker.id, application.worker.fullname, application.status,
                    equal = True
                    other_app = other_apps[0]
                    if application.status != other_app.status:
                        equal = False
                        print other_app.status
                    if set(application.experts_conclusions.all()) != set(other_app.experts_conclusions.all()):
                        equal = False
                        print application.experts_conclusions.all(), other_app.experts_conclusions.all(), 
                    if equal:
                        if application.id < other_app.id:
                            other_app.delete()
                        print "REMOVED"
                    else:
                        print "NOT EQUAL"

