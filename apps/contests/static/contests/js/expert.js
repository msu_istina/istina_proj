$(function() {
    get_datatables_simple('#tblData', {"bFilter": true, "aaSorting": [[2,'asc']]});

    // bind change event to select
    $('#department_group_select, #dep_select, #status_select').bind('change', function () {
        var department_group = $("#department_group_select").val();
        var depid = $('#dep_select').val();
        var status = $('#status_select').val();
        var url = $(this).closest("form").data("base-url") + department_group + "/" + depid + "/" + status + "/";
        if (url) { // require a URL
          window.location = url; // redirect
        }
        return false;
    });

    $("a.change_status").click(function(ev) {
        ev.preventDefault();
        // set worker name to dialog
        var worker_name = $(this).closest("tr").find("td a[href^='/workers/']").text();
        $("#change_status_worker").text(worker_name);

        // set current status in dialog
        var status = $(this).text();
        // get status code by text
        var status_code = $("#status_select option").filter(function () { return $(this).text() == status; }).val();
        $("#change_status_select").val(status_code);

        var application_id = $(this).closest("tr").data("application-id");
        $("#change_status_error").hide();

        $("#change_status_dialog").dialog({
            draggable: true,
            modal: true,
            title: "Изменить статус заявки",
            buttons: [{
                text: "Сохранить",
                click: function() {
                    var set_status_url = $("#tblData").data("set-status-url").replace("0", application_id);
                    var status_code = $("#change_status_select").val();
                    var dialog = $(this);
                    $.getJSON(set_status_url, {
                        status: status_code
                    }).done(function(data) {
                        var status = $("#change_status_select option:selected").text();
                        // update text in all status links for the current worker (there can be multiple rows for one worker)
                        $("#tblData td a[href^='/workers/']:contains('" + worker_name + "')").closest('tr').find("td a.change_status").text(status);
                        dialog.dialog("close");
                    }).fail(function(data) {
                        $("#change_status_error").show();
                    });
                }
            }]
        });
    });

});
