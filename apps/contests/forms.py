# -*- coding: utf-8; -*-

from django import forms
from django.db import models
from django.db.models import Q, Count
from django.db.models.fields import BLANK_CHOICE_DASH
from .models import ExpertConclusion, ContestApplication, SurveyPage, SurveyField, SurveyTextContent, ContestCriterion
from common.forms import Select2ModelField, Select2ModelMultipleField, Select2SingleWorkerField
from django.core.validators import MaxLengthValidator
from common.utils.files import validate_uploaded_file
from publications.models import Article
from patents.models import Patent
from workers.models import Worker
from common.models import ObjectListItem, Attachment, ObjectList
from django.utils.html import escape, conditional_escape
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe
from django.contrib.contenttypes.models import ContentType

from journals.models import JournalRubric

class ExpertConclusionForm(forms.ModelForm):
    #mark = forms.ChoiceField(label=u'Оценка', choices=POSSIBLE_MARKS)
    #comment = forms.CharField(label=u'Комментарий', widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(ExpertConclusionForm, self).__init__(*args, **kwargs)
        self.fields["comment"].widget = forms.widgets.Textarea()

    class Meta:
        model = ExpertConclusion
        fields = ('mark', 'comment')



class ScientificTopicManager(models.Manager):
    def get_query_set(self):
        qs = super(ScientificTopicManager, self).get_query_set()
        return qs.filter(Q(categorization__code='NOMENKL'), ~Q(code__icontains='00.00')).order_by('code')

class ScientificTopic(JournalRubric):
    objects = ScientificTopicManager()
    def __unicode__(self):
        return self.code + " " + self.name
    class Meta:
        proxy = True

class TopicSelect2Field(Select2ModelField):
    def __init__(self, *args, **kwargs):
        qset = ScientificTopic.objects.all() #filter(categorization__code='NOMENKL')
        self.search_fields = ['name__icontains', 'code__startswith']
        super(TopicSelect2Field, self).__init__(self, *args, add_option=False, queryset=qset, **kwargs)
        self.widget.options['placeholder'] = u'Введите часть названия или кода специальности, например, 01.01'
        
    def get_title(self, obj):
        return obj.code + " --> " + obj.name

class ExtraDataForm(forms.Form):
    keywords = forms.CharField(label=u'Ключевые слова', widget=forms.Textarea)
    engkeywords = forms.CharField(label=u'Ключевые слова', widget=forms.Textarea)
    topics = TopicSelect2Field(label=u'Тематика', required=False)
    description = forms.CharField(label=u'Краткое описание области исследования', widget=forms.Textarea, validators=[MaxLengthValidator(1800)])


class SurveyArticleSelect2Field(Select2ModelMultipleField):
    search_fields = ['title__icontains','authors__lastname__icontains']
    # search_fields duplicates records due to wrong SQL code
    def __init__(self, *args, **kwargs):
        super(SurveyArticleSelect2Field, self).__init__(self, *args, add_option=False, model=Article, **kwargs)
        self.widget.options['placeholder'] = u'Введите часть названия статьи или часть фамилии одного из авторов'

    def get_title(self, obj):
        try:
            return obj.formatted_references.all()[0].reference
        except Exception as e:
            return obj.title


class SurveyPatentSelect2Field(Select2ModelMultipleField):
     search_fields = ['title__icontains','authors__lastname__icontains', 'number__icontains']
     def __init__(self, *args, **kwargs):
         super(SurveyPatentSelect2Field, self).__init__(self, *args, add_option=False, model=Patent, **kwargs)
         self.widget.options['placeholder'] = u'Введите часть названия, номера патента или фамилии автора'

     def init_options(self):
         super(SurveyPatentSelect2Field, self).init_options()
         self.options['placeholder'] = u'Введите часть названия или номера патента'

     def get_title(self, obj):
         return obj.title

     def get_description(self, obj):
         try:
             return obj.number

         except AttributeError:
             return u''


class PlainTextWidget(forms.Widget):
    def __init__(self,attrs=None):
        super(PlainTextWidget,self).__init__(attrs)

    def render(self, name, value, attrs=None):
        return u""


class MyClearableFileInput(forms.ClearableFileInput):

    template_with_initial = u'%(initial_text)s: %(initial)s<br/> %(input_text)s: %(input)s'

    def render(self, name, value, attrs=None):
        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
        }
        template = '%(input)s'
        substitutions['input'] = super(MyClearableFileInput, self).render(name, value, attrs)

        if self.attrs.has_key('file') and hasattr(self.attrs['file'], "url"):
            template = self.template_with_initial
            substitutions['initial'] = (u'<a href="%s">%s</a>'
                                        % (escape(self.attrs['file'].url),
                                           escape(force_unicode(self.attrs['file']).split('/')[-1])))

        return mark_safe(template % substitutions)


publicationForm = SurveyArticleSelect2Field(label=u"5.1.2. Ссылки на научные публикации Участника (-ов) Проекта по теме заявки за последние 5 лет", widget_kwargs={'width': 700}, required=False)
patentForm = SurveyPatentSelect2Field(label=u"5.1.3. Патенты и/или заявки на выдачу патентов за авторством Участника (-ов) Проекта, имеющие непосредственное отношение к Проекту, и описание прав на них", widget_kwargs={'width': 700}, required=False)


def getSurveyPageForm(surveypage):
    class SurveyPageForm(forms.ModelForm):

        def __init__(self,*args,**kwargs):
            val_dict = kwargs.pop("val_dict")
            super(SurveyPageForm,self).__init__(*args,**kwargs)
            surveyfields = SurveyField.objects.raw("""select * from SURVEYFIELD where F_SURVEYPAGE_ID=%s
start WITH F_SURVEYFIELD_PARENT is null
CONNECT BY prior F_SURVEYFIELD_ID=F_SURVEYFIELD_PARENT
order SIBLINGS BY F_SURVEYFIELD_NUM""" % surveypage.id)

            for i, question in enumerate(surveyfields):
                req = bool(question.isrequired)
                id = question.id
                model = question.contenttype.model_class()
                if model == SurveyField:
                    self.fields['custom_%s' %id] = forms.CharField(label=question, required=False)
                    self.fields['custom_%s' %id].widget = PlainTextWidget()
                elif model == SurveyTextContent:
                    self.fields['custom_%s' %id] = forms.CharField(label=question, widget=forms.Textarea, required=req, validators=[MaxLengthValidator(4000)])
                    self.fields['custom_%s' %id].widget.attrs['class'] = 'wide'
                    if val_dict.has_key('custom_%s' %id) and len(val_dict['custom_%s' %id])>0:
                        self.fields['custom_%s' %id].initial = val_dict['custom_%s' %id][0].value
                elif model == Worker:
                    self.fields['custom_%s' %id] = Select2SingleWorkerField(label=question, widget_kwargs={'width': 700}, required=req)
                    if val_dict.has_key('custom_%s' %id) and len(val_dict['custom_%s' %id])>0:
                        self.fields['custom_%s' %id].initial = val_dict['custom_%s' %id][0]
                elif model == Article:
                    self.fields['custom_%s' %id] = publicationForm
                    if val_dict.has_key('custom_%s' %id) and len(val_dict['custom_%s' %id])>0:
                        self.fields['custom_%s' %id].initial = val_dict['custom_%s' %id]
                elif model == Patent:
                    self.fields['custom_%s' %id] = patentForm
                    if val_dict.has_key('custom_%s' %id) and len(val_dict['custom_%s' %id])>0:
                        self.fields['custom_%s' %id].initial = val_dict['custom_%s' %id]
                elif question.contenttype.id == 441:
                    self.fields['custom_%s' %id] = forms.FileField(label=question, required=False, validators=[validate_uploaded_file])
                    self.fields['custom_%s' %id].widget = MyClearableFileInput()
                    if val_dict.has_key('custom_%s' %id) and len(val_dict['custom_%s' %id])>0:
                        self.fields['custom_%s' %id].widget.attrs['file'] = val_dict['custom_%s' %id][0].link
                        # self.fields['custom_%s' %id].initial = val_dict['custom_%s' %id][0].link

        def save(self,request,application, commit=True):
            items = application.items
            for field in self.changed_data:
                surveyfield = SurveyField.objects.get(pk=field.split('_')[1])
                model = surveyfield.contenttype.model_class()
                if model == SurveyField:
                    continue
                if not ObjectListItem.objects.filter(object_list=items,group=surveyfield.id).exists():
                    if model == SurveyTextContent:
                        item = SurveyTextContent.objects.create(value=self.cleaned_data[field])
                        ObjectListItem.objects.create(object_id = item.id, object_list=items,group=surveyfield.id, class_name=surveyfield.contenttype.name, content_type=surveyfield.contenttype, title=item.__unicode__()[:255])
                    elif model == Worker:
                        item = self.cleaned_data[field]
                        ObjectListItem.objects.create(object_id = item.id, object_list=items,group=surveyfield.id, class_name=surveyfield.contenttype.name, content_type=surveyfield.contenttype, title=item.__unicode__())
                    elif model == Attachment:
                        file = self.files["%s-%s" % (surveyfield.surveypage.order - 1, field)]
                        application_type = ContentType.objects.get_for_model(ContestApplication)
                        item = Attachment.objects.create(link=file,description=field,object_id=application.id, content_type=application_type,  creator=request.user)
                        ObjectListItem.objects.create(object_id = item.id, object_list=items,group=surveyfield.id, class_name=surveyfield.contenttype.name, content_type=surveyfield.contenttype, title=item.__unicode__())
                    else:
                        for item in self.cleaned_data[field]:
                            ObjectListItem.objects.create(object_id = item.id, object_list=items,group=surveyfield.id, class_name=surveyfield.contenttype.name, content_type=surveyfield.contenttype, title=item.__unicode__())
                else:
                    if model == SurveyTextContent:
                        item = ObjectListItem.objects.get(object_list=items,group=surveyfield.id)
                        stc = SurveyTextContent.objects.get(pk=item.object_id)
                        item.title = self.cleaned_data[field][:255]
                        stc.value = self.cleaned_data[field]
                        stc.save()
                        item.save()
                    elif model == Article or model == Patent:
                        old_objects = self.get_old_select2_ids(field)
                        new_objects = self.get_new_select2_ids(field)
                        del_objects = [x for x in old_objects if x not in new_objects]
                        add_objects = [x for x in new_objects if x not in old_objects]
                        for x in add_objects:
                            obj = model.objects.get(pk=x)
                            ObjectListItem.objects.create(object_id = x, object_list=items,group=surveyfield.id, class_name=surveyfield.contenttype.name, content_type=surveyfield.contenttype, title=obj.__unicode__())
                        if len(del_objects) > 0:
                            ObjectListItem.objects.filter(object_list=items,group=surveyfield.id).filter(object_id__in=del_objects).delete()
                    elif model == Worker:
                        item = ObjectListItem.objects.get(object_list=items,group=surveyfield.id)
                        worker = self.cleaned_data[field]
                        item.title = worker.__unicode__()
                        item.object_id = worker.id
                        item.save()
                    elif model == Attachment:
                        file = self.files["%s-%s" % (surveyfield.surveypage.order - 1, field)]
                        application_type = ContentType.objects.get_for_model(ContestApplication)
                        old_file_li = ObjectListItem.objects.get(object_list=items,group=surveyfield.id)
                        old_file = Attachment.objects.get(pk=old_file_li.object_id)
                        old_file.delete()
                        new_file = Attachment.objects.create(link=file,description=field,object_id=application.id, content_type=application_type, creator=request.user)
                        old_file_li.object_id = new_file.id
                        old_file_li.title = new_file.__unicode__()
                        old_file_li.save()
                    else:
                        continue

        def get_new_select2_ids(self,name):
            prefixed_name = self.add_prefix(name)
            new_ids = self.fields[name].widget.value_from_datadict(self.data, self.files, prefixed_name)
            return new_ids

        def get_old_select2_ids(self,name):
            old_ids=[]
            initial_value = self.fields[name].initial
            for member in initial_value:
                old_ids.append(unicode(member.id))
            return old_ids

        class Meta:
            model = ContestApplication
            fields = ()

    return SurveyPageForm

def getExpertConclusionForm(contest, worker, application):
    class ExpertConclusionSurveyForm(forms.ModelForm):

        def __init__(self, *args, **kwargs):
            super(ExpertConclusionSurveyForm,self).__init__(*args,**kwargs)
            criteria = ContestCriterion.objects.filter(contest=contest)
            try:
                conclusion = ExpertConclusion.objects.get(worker=worker, application=application)
            except ExpertConclusion.DoesNotExist:
                conclusion = None
            if conclusion:
                items = conclusion.items
            for i, question in enumerate(criteria):
                id = question.id
                self.fields['comment_%s' %id] = forms.CharField(label=question, widget=forms.Textarea, required=True, validators=[MaxLengthValidator(4000)])
                self.fields['comment_%s' %id].widget.attrs['class'] = 'comment'
                self.fields['mark_%s' %id] = forms.ChoiceField(label=u"Оценка", required=True, choices=BLANK_CHOICE_DASH + [(x,x) for x in range(0,question.maxmark+1)])
                if conclusion:
                    initialstring = SurveyTextContent.objects.get(pk=items.items.get(group=id).object_id).value
                    self.fields['mark_%s' %id].initial = initialstring.split(';')[0]
                    self.fields['comment_%s' %id].initial = initialstring.split(';')[1]


            self.fields['summary'] = forms.CharField(label=u'Мнение эксперта', widget=forms.Textarea, required=True, validators=[MaxLengthValidator(4000)])
            self.fields['summary'].widget.attrs['class'] = 'wide'
            if conclusion:
                self.fields['summary'].initial = conclusion.comment

        class Meta:
            model = ExpertConclusion
            fields = ()

    return ExpertConclusionSurveyForm