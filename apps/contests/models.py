# -*- coding: utf-8 -*-
from django.db import models
from django.template import Template, Context
from django.template.loader import render_to_string
import logging
from common.models import MyModel
from common.utils.user import get_worker
from .managers import ContestManager
import datetime

from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from common.utils.user import get_fullname
from django.core.mail import send_mail
from settings import ENV_DIR, DEFAULT_FROM_EMAIL
from os.path import join
from time import sleep
from organizations.models import Organization
from common.utils.choices import DATATYPES

from django.core.validators import MaxLengthValidator

logger = logging.getLogger("contests.models")

# TODO
# сделать список прошедших конкурсов
# показывать дату конкурса в общем списке
# для прошедших запретить подачу заявок, в том числе в view

class Contest(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_contest_id")
    name = models.CharField(u"Название", max_length=1024, db_column="f_contest_name")
    slug = models.SlugField(u"Кодовое название", max_length=128, db_column="f_contest_code", unique=True)
    parent = models.ForeignKey('self', related_name="children", null=True, blank=True, verbose_name=u"Родительский конкурс", db_column='con_f_contest_id', on_delete=models.PROTECT)
    organization = models.ForeignKey("organizations.Organization", verbose_name=u"Организация", related_name="contests", db_column="f_organization_id")
    startdate = models.DateField(u"Дата начала приема заявок", db_column="f_contest_start", null=True, blank=True)
    deadline = models.DateField(u"Дата окончания приема заявок", db_column="f_contest_deadline", null=True, blank=True)
    deadline_extended = models.DateField(u"Продленная дата окончания приема заявок", db_column="f_contest_extdeadline", null=True, blank=True)
    description = models.TextField(u"Описание", db_column="f_contest_text", blank=True)
    email_body = models.TextField(u"Текст электронного письма", db_column="f_contest_emailbody", blank=True)
    metrics = models.ManyToManyField("Metric", related_name="contests", through="ContestMetric")
    creator = models.ForeignKey(to='auth.User', related_name="contests_added", db_column="f_contest_user")

    objects = ContestManager()

    nominative_en = "contest"
    genitive = u"конкурса"
    accusative = u"конкурс"
    accusative_short = u"конкурс"
    instrumental = u"конкурсом"
    locative = u"конкурсе"

    class Meta:
        db_table = "contest"
        verbose_name = u"конкурс"
        verbose_name_plural = u"конкурсы"
        ordering = ('-deadline', 'name')

    def __unicode__(self):
        return u"%s" % self.name

    @models.permalink
    def get_absolute_url(self):
        return ('contests_detail', (), {'slug': self.slug})

    def worker_has_applied(self, worker):
        return self.applications.filter(worker=worker).exists()

    @property
    def has_dates(self):
        return any((self.startdate, self.deadline, self.deadline_extended))

    @property
    def has_deadline(self):
        return any((self.deadline, self.deadline_extended))

    def get_root(self):
        current = self
        while current.parent:
            current = current.parent
        return current

    @property
    def is_active(self):
        # if parent contest is not active, this is not too
        if self.parent and not self.parent.is_active:
            return False
        today = datetime.date.today()
        return ((not self.startdate or self.startdate <= today)
                and (not self.deadline or self.deadline >= today
                     or (self.deadline_extended and self.deadline_extended >= today)))

    def send_emails(self, subject=None, template_filename=None, users="no", really_send=False):
        usernames_excluded = ['Barmin', 'Buchin', 'Falunin', 'Glagolev', 'Kuznetsov', 'Panob', 'Pavelyev', 'Savinov', 'Stepanov', 'Yakimov', 'Nekrasov', 'Cherny']
        users_excluded = User.objects.filter(email="gala@imec.msu.ru", username__in=usernames_excluded)

        if users == "no":
            users = [] # no users, default
        elif users == "goldan":
            users = User.objects.filter(email__in=["denis.golomazov@gmail.com"]) # just me
        elif users == "admins":
            users = User.objects.filter(email__in=["denis.golomazov@gmail.com", "serg@msu.ru"]) # admins to test
        elif users == "msu":
            msu = Organization.objects.get_msu()
            users = msu.users # all active employees in msu, use with caution!
        elif users == "active":
            users = User.objects.filter(is_active=True) # all active users, use with caution!
        elif users == "all":
            users = User.objects.all() # all users, use with caution!
        else:
            raise

        if not template_filename:
            template = Template(self.email_body)
        log_filename = "%s.log" % self.slug
        logfile = open(join(ENV_DIR, 'var', 'log', log_filename), 'w')

        from_email = DEFAULT_FROM_EMAIL
        subject = subject or self.name

        for user in users:
            name = get_fullname(user)
            log_message = u"%d %s %s %s\n" % (user.id, user.username, user.email, name)
            if user in users_excluded or not user.email:
                log_message = "SKIP: %s" % log_message
            else:
                context = Context({'name': name, 'username': user.username})
                if template_filename:
                    message = render_to_string(template_filename, context)
                else:
                    message = template.render(context)
                if really_send:
                    send_mail(subject, message, from_email, [user.email])
            logfile.write(log_message.encode('utf8'))
            print log_message,
            sleep(1)
        logfile.close()


APPLICATION_STATUSES = [
    ("CONFIRM", u"подтверждена, проверена"),
    ("REEDIT", u"подтверждена, корректируется"),
    ("REJECTED", u"отклонена"),
    ("SIGN", u"подана"),
    ("EDIT", u"редактируется")
]


class ContestApplication(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_contestmanlink_id")
    worker = models.ForeignKey(to="workers.Worker", related_name="contests", db_column="f_man_id")
    contest = models.ForeignKey(to=Contest, related_name="applications", db_column="f_contest_id")
    submit_date = models.DateField(u"Дата подачи заявки", db_column="f_contestmanlink_submitted", auto_now_add=True)
    status = models.CharField(u"Статус", max_length=255, db_column="F_CONTESTMANLINK_STATUS", blank=True, choices=APPLICATION_STATUSES)
    items = models.ForeignKey(to="common.ObjectList", related_name="contest_applications", db_column="f_markedlist_id", null=True, blank=True)
    creator = models.ForeignKey(to='auth.User', related_name="applications_added", db_column="f_contestmanlink_user")

    nominative_short = u"заявка"
    genitive = u"заявки на конкурс"
    genitive_short = u"заявки"
    accusative_short = u"заявку"
    instrumental = u"заявкой на конкурс"
    locative = u"заявке на конкурс"
    
    class Meta:
        db_table = "contestmanlink"
        verbose_name = u"заявка на конкурс"
        verbose_name_plural = u"заявки на конкурсы"
        ordering = ('worker',)

    def __unicode__(self):
        return u"заявка %s на конкурс %s" % (self.worker, self.contest)


POSSIBLE_MARKS=(
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5))


class ExpertConclusion(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_contestconclusion_id")
    application = models.ForeignKey(to=ContestApplication, related_name="experts_conclusions", db_column="f_contestmanlink_id")
    mark = models.IntegerField(u'Оценка', choices=POSSIBLE_MARKS, db_column="f_contestconclusion_mark")
    comment = models.TextField(u"Комментарий", validators=[MaxLengthValidator(2000)], db_column="f_contestconclusion_comment")
    worker = models.ForeignKey(to="workers.Worker", related_name="contest_expert_conclusions", db_column="f_man_id")
    creator = models.ForeignKey(to='auth.User', related_name="contest_expert_conclusions", db_column="f_contestconclusion_user")
    items = models.ForeignKey(to='common.ObjectList', related_name="expert_conclusions", db_column="f_markedlist_id", null=True, blank=True)

    class Meta:
        db_table = "contestconclusion"
        verbose_name = u"Экспертное заключение заявки на конкурс"
        verbose_name_plural = u"Экспертные заключения заявок на конкурсы"

    def __unicode__(self):
        return u"Экспертное заключение заявки %s на конкурс %s" % (self.worker, self.application.contest)


class ContestCriterion(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_CONTESTCRITERION_ID")
    contest = models.ForeignKey(to=Contest, related_name="contest_criteria", db_column="F_CONTEST_ID")
    name = models.CharField(u"Название", max_length=255, db_column="F_CONTESTCRITERION_NAME")
    order = models.IntegerField(u"Порядок критерия", db_column="F_CONTESTCRITERION_NUM")
    maxmark = models.IntegerField(u"Максимальная оценка", db_column="F_CONTESTCRITERION_MAXMARK")

    class Meta:
        db_table = "CONTESTCRITERION"
        verbose_name = u"Критерий экспертного заключения"
        verbose_name_plural = u"Критерии экспертного заключения"
        ordering=["contest","order"]

    def __unicode__(self):
        return u"%d. %s" % (self.order, self.name)


class ContestWorkerScore(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_CONDATA_ID")
    worker = models.ForeignKey("workers.Worker", related_name="contest_scores", verbose_name=u"сотрудник", db_column="F_MAN_ID")
    metric = models.ForeignKey("Metric", related_name="scores", verbose_name=u"метрика", db_column="F_CONDATATYPE_ID")
    score = models.CharField(u"значение", max_length=255, db_column="F_CONDATA_VALUE")

    class Meta:
        db_table = "CONDATA"
        verbose_name = u"значение показателя сотрудника на конкурсе"
        verbose_name_plural = u"значения показателей сотрудников на конкурсах"


class Metric(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_CONDATATYPE_ID")
    name = models.CharField(u"название", max_length=255, db_column="F_CONDATATYPE_NAME")
    datatype = models.CharField(u"тип данных", max_length=30, choices=DATATYPES, db_column="F_CONDATATYPE_TYPE")
    show_on_site = models.BooleanField(u"показывать на сайте?", db_column="F_CONDATATYPE_ONLINE")
    sql = models.TextField(u"SQL запроса", db_column="F_CONDATATYPE_SQL", blank=True)

    class Meta:
        db_table = "CONDATATYPE"
        verbose_name = u"показатель эффективности"
        verbose_name_plural = u"показатели эффективности"


class ContestMetric(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_CONDATATYPELINK_ID")
    contest = models.ForeignKey("Contest", related_name="contest_metrics", db_column="F_CONTEST_ID")
    metric = models.ForeignKey("Metric", related_name="contest_metrics", db_column="F_CONDATATYPE_ID")

    class Meta:
        db_table = "CONDATATYPELINK"
        verbose_name = u"показатель эффективности на конкурсе"
        verbose_name_plural = u"показатели эффективности на конкурсах"


class SurveyPage(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SURVEYPAGE_ID")
    contest = models.ForeignKey("Contest", related_name="contest_surveypage", db_column="F_CONTEST_ID")
    name = models.CharField(u"Название", max_length=255, db_column="F_SURVEYPAGE_NAME")
    comment = models.CharField(u"Пояснение", max_length=2000, db_column="F_SURVEYPAGE_COMMENT", blank=True, null=True)
    order = models.IntegerField(u"Порядок группы", db_column="F_SURVEYPAGE_NUM")

    class Meta:
        db_table = "SURVEYPAGE"
        verbose_name = u"Группа полей анкеты"
        verbose_name_plural = u"Группы полей анкеты"
        ordering = ("-contest","order")

    def __unicode__(self):
            return u"%s. %s" % (self.order, self.name)


class SurveyField(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SURVEYFIELD_ID")
    surveypage = models.ForeignKey("SurveyPage", related_name="surveypage_field", db_column="F_SURVEYPAGE_ID")
    name = models.CharField(u"Название", max_length=255, db_column="F_SURVEYFIELD_NAME")
    comment = models.CharField(u"Пояснение", max_length=2000, db_column="F_SURVEYFIELD_COMMENT", blank=True, null=True)
    order = models.IntegerField(u"Порядок поля", db_column="F_SURVEYFIELD_NUM")
    contenttype = models.ForeignKey(ContentType, verbose_name=u"Тип объекта", db_column='F_SURVEYFIELD_DJANGOTYPE')
    parentfield = models.ForeignKey("self", verbose_name=u"Родительское поле", db_column='F_SURVEYFIELD_PARENT', blank=True, null=True)
    isrequired = models.IntegerField(u"Обязательное",db_column='F_SURVEYFIELD_ISREQUIRED',default=0)

    class Meta:
        db_table = "SURVEYFIELD"
        verbose_name = u"Поле анкеты"
        verbose_name_plural = u"Поля анкеты"
        ordering = ("surveypage__contest","surveypage__order","order")

    def __unicode__(self):
        try:
            return u"%s.%s.%s. %s" % (self.surveypage.order, self.parentfield.order, self.order, self.name)
        except:
            return u"%s.%s. %s" % (self.surveypage.order, self.order, self.name)

class SurveyTextContent(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SURVEYTEXTCONTENT_ID")
    value = models.TextField(u"Значение", db_column="F_SURVEYTEXTCONTENT_VAL", blank=True)

    class Meta:
        db_table = "SURVEYTEXTCONTENT"
        verbose_name = u"Значение текстового поля"
        verbose_name_plural = u"Значения текстовых полей"

    def __unicode__(self):
        return u"%s" % self.value
