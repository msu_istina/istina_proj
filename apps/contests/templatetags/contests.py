# -*- coding: utf-8 -*-

from django import template

register = template.Library()

@register.assignment_tag
def worker_has_applied(contest, worker):
	return contest.worker_has_applied(worker)