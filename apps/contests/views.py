# -*- coding: utf-8; -*-
from celery.app import _app_or_default
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.utils import simplejson
from common.utils.list import is_tuple_list
from common.utils.user import get_worker
from actstream import action
from workers.decorators import worker_required
from contests.models import Contest, APPLICATION_STATUSES, ContestApplication, SurveyPage, SurveyField, SurveyTextContent, ContestCriterion, ExpertConclusion
from contests.forms import ExpertConclusionForm, getSurveyPageForm, getExpertConclusionForm
from unified_permissions import has_permission
from common.views import MyModelWizardView

from collections import namedtuple
from django.db import connection
from django.db.models import Avg
from sqlreports.views import sign_url, generate_adata
from django.utils.decorators import classonlymethod
from contests.forms import getSurveyPageForm
from common.models import ObjectList, Attachment, ObjectListItem
from django.db.models import get_model,Q
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
import os
from workers.models import Worker
from publications.models import Article
from patents.models import Patent
from datetime import date

EXPERTS = ['safonin', 'S.Y.Egorov', 'goldan', 'sasha','andrey.zenzinov']
EXPERTS_VIEW = ['pnkostylev', 'vasilchuk']
CONTEST_ADMINS = ['obogomol', 'chertovich',  'KhokhlovAR', 'maxxk', 'andrey.zenzinov']

def is_admin_user(user):
    return user.username in CONTEST_ADMINS or user.is_superuser

def contest_departments(contest, department_group):
    u'''Возвращает список всех факультетов, сотрудники которых подали заявку на конкурс.'''
    sql = '''
      select distinct d.f_department_id faculty_id, substr(d.f_department_name,1,40) faculty_name,   f_depdata_value faculty_type
      from contestmanlink
         join contest on contest.f_contest_id = contestmanlink.f_contest_id
         join man using (f_man_id)
         left join manspost using (f_man_id)
         left join root_department rd on (rd.f_department_id = manspost.f_department_id)
         left join department d on d.f_department_id = rd.root_f_department_id
         left join (select f_depdata_value, f_department_id from depdata where f_depdatatype_id=5) dep_dat on (d.f_department_id=dep_dat.f_department_id)
       where /*
              (nvl(manspost.f_manspost_part, 0) = 0
              and
              (manspost.f_manspost_end is null or manspost.f_manspost_end >= nvl(contest.f_contest_deadline,sysdate))
             )
             and
             */
                contest.f_contest_id = %s
                   and  (f_depdata_value = %s or %s = 'ALL')
       order by faculty_name
     '''
    cursor = connection.cursor()
    cursor.execute(sql, [contest.id, department_group, department_group])

    ContestApplicationTuple = namedtuple('Departments', 'f_department_id, f_department_name, faculty_type')
    deps = [ContestApplicationTuple(*item) for item in cursor]
    return deps


def contest_applications(contest, expert, department_group=None, department_id=None, statuses='ALL'):
    u'''Возвращает список всех участников конкурса с заданными условиями фильтрации.'''
    applications = []
    get_applications_sql = u'''
    select members.f_man_id, members.f_contest_id, fio,
           members.faculty, members.department,
           LISTAGG( nvl(f_post_name, '-'), ',') WITHIN GROUP (order by f_post_name)  "Position",
           f_contestmanlink_id,
           f_contestconclusion_mark expert_mark,
           f_contestmanlink_status,
           max(ws),
           max(rinc),
           max(members.f_depdata_value) faculty_type,
           max(pos) pos
    from
    (
      select distinct
          f_man_id, contest.f_contest_id, f_man_namel || ' ' || f_man_namef || ' ' || f_man_namem fio,
          d.f_department_name Faculty, real_dep.f_department_name department,
          f_post_id, f_contestmanlink_id,
          d.f_department_id faculty_id,
          f_contestmanlink_status,
          ws,
          rinc,
          f_depdata_value
       from contestmanlink
         join man on (contestmanlink.f_man_id=man.f_man_id)
         join contest on contest.f_contest_id = contestmanlink.f_contest_id
         left join manspost on (manspost.f_man_id=contestmanlink.f_man_id)
         left join root_department rd on (rd.f_department_id = manspost.f_department_id)
         left join department d on d.f_department_id = rd.root_f_department_id
         left join department real_dep on (real_dep.f_department_id = manspost.f_department_id)
         left join (select f_condata_value ws, f_man_id from condata where f_condatatype_id=1) D1 on (D1.f_man_id=contestmanlink.f_man_id)
         left join (select f_condata_value rinc, f_man_id from condata where f_condatatype_id=2) D2 on (D2.f_man_id=contestmanlink.f_man_id)
         left join (select f_depdata_value, f_department_id from depdata where f_depdatatype_id=5) dep_dat on (d.f_department_id=dep_dat.f_department_id)
       where (nvl(manspost.f_manspost_part, 0) = 0
                or not exists(select 1 from manspost M where m.f_man_id=contestmanlink.f_man_id and nvl(m.f_manspost_part, 0) = 0)
              )
             and (manspost.f_manspost_end is null or manspost.f_manspost_end >= nvl(contest.f_contest_deadline,sysdate))
             and
             contest.f_contest_id = %s
             and  (f_contestmanlink_status in (%s, %s) or %s = 'ALL')
             and  (f_depdata_value = %s or %s = 'ALL')
    ) members
     left join post on (members.f_post_id = post.f_post_id)
     left join (select f_contestmanlink_id, f_contestconclusion_mark
                from contestconclusion
                where f_man_id = %s
               ) contestconclusion
           using (f_contestmanlink_id)
     left join tmp_concurs_res_11_14 on (members.f_man_id=tmp_concurs_res_11_14.f_man_id and members.f_contest_id=tmp_concurs_res_11_14.f_contest_id)
    where (members.faculty_id = %s or %s = 0)
    group by members.f_man_id, members.f_contest_id, fio,
             f_contestmanlink_id,
             f_contestconclusion_mark,
             faculty, members.department, f_contestmanlink_status
    order by 4, 2, 3
    '''
    department_id = department_id or -1
    cursor = connection.cursor()
    if not is_tuple_list(statuses):
      statuses = (statuses, statuses)
    cursor.execute(get_applications_sql, [
      contest.id, statuses[0], statuses[1], statuses[0], department_group, department_group, expert.id, department_id, department_id])

    ContestApplicationTuple = namedtuple('ContestApplication',
      'f_man_id, f_contest_id, fio, faculty, department, positions, f_contestmanlink_id, expert_mark, status, impact_sum_wos, impact_sum_rinc, faculty_type, group')
    statuses = dict(APPLICATION_STATUSES)
    applications = []
    for item in cursor:
      application = list(item)
      # impact factors
      if application[9] is not None:
        application[9] = float(application[9])
      if application[10] is not None:
        application[10] = float(application[10])
      # status
      application[8] = statuses[application[8]]
      applications.append(ContestApplicationTuple(*application))
    return applications

@worker_required
def contests_expert(request, worker, slug=None, department_group=None, department_id=None, status=None):
    if request.user.username not in EXPERTS + EXPERTS_VIEW:
        return redirect("contests_list")

    if not department_id:
      department_id = '0'

    if not status:
      status = 'confirm+reedit'
    status = status.upper()
    if "+" in status:
      statuses = status.split("+")
    else:
      statuses = (status, status)

    DEPARTMENT_GROUPS = [
      ("human", u"Гуманитарные науки"),
      ("earth", u"Науки о Земле"),
      ("natural", u"Естественные науки"),
      ("math", u"Математика, механика, вычислительная математика")
    ]
    if department_group in [None, 'all']:
      department_group_name = "ALL"
    else:
      department_group_name = dict(DEPARTMENT_GROUPS)[department_group]

    all_statuses = [('CONFIRM+REEDIT', u'подтверждена')] + APPLICATION_STATUSES

    contest = get_object_or_404(Contest, slug=slug)
    participants = contest_applications(contest, expert=worker, department_group=department_group_name, department_id=department_id, statuses=statuses)
    workers_count = len(set([p.f_man_id for p in participants]))
    context = {
        'participants': participants,
        'workers_count': workers_count,
        'deps': contest_departments(contest, department_group_name),
        'contest': contest,
        'department_groups': DEPARTMENT_GROUPS,
        'department_group': department_group,
        'department_id': department_id,
        'statuses': all_statuses,
        'status': status
    }
    return render(request, "contests/contest_expert.html", context)

@worker_required
def list_expert_contests(request, worker):
    if request.user.username not in EXPERTS + EXPERTS_VIEW:
        return redirect("contests_list")

    context = {
        'contests': Contest.objects.all()
        }
    return render(request, "contests/list_expert_contests.html", context)



@worker_required
def contests_expert_conclusion(request, worker, slug, contestmanlink_id, department_id=None):
    if request.user.username not in EXPERTS + EXPERTS_VIEW:
        return redirect("contests_list")

    contest = get_object_or_404(Contest, slug=slug)
    applications = contest.applications.filter(contest=contest, id=contestmanlink_id)
    application = applications[0] if applications else None

    conclusion = None
    if application:
        conclusions = application.experts_conclusions.filter(worker=worker)
        conclusion = conclusions[0] if conclusions else None

    form = ExpertConclusionForm(request.POST or None, instance=conclusion)
    if request.method == "POST" and request.POST.get("cancel", None):
        print department_id
        if department_id:
            return redirect("contests_expert", slug, department_id)
        return redirect("contests_expert", slug)

    if request.method == "POST" and form.is_valid():
        conclusion = form.save(commit=False)
        conclusion.creator = request.user
        conclusion.contest = contest
        conclusion.worker = worker
        conclusion.application = application
        conclusion.save()
        if department_id:
            return redirect("contests_expert", slug, department_id)
        return redirect("contests_expert", slug)


    context = {
        'contest': contest,
        'application': application,
        'form': form,
        }
    return render(request, "contests/expert_conclusion.html", context)


@worker_required
def contests_list(request, worker):
    if not worker.user:
        messages.error(request, u"Только зарегистрированные пользователи могут участвовать в конкурсах.")
        return redirect("home")
    context = {
        'contests': Contest.objects.recent(),
        'worker': worker,
        'page_owner': get_worker(request.user) == worker
    }
    return render(request, "contests/contests_list.html", context)

@worker_required
def contest_detail(request, worker, slug):
    if not worker.user:
        messages.error(request, u"Только зарегистрированные пользователи могут участвовать в конкурсах.")
        return redirect("home")
    contest = get_object_or_404(Contest, slug=slug)
    has_survey = SurveyPage.objects.filter(contest=contest).exists()
    context = {
        'contest': contest,
        'worker': worker,
        'page_owner': get_worker(request.user) == worker,
        'is_admin_user': is_admin_user(request.user),
        'has_survey': has_survey,
        'in_edit' : ContestApplication.objects.filter(contest=contest,worker=worker, status=u"EDIT").exists()
            # ObjectList.objects.filter(contest_applications__worker=worker, contest_applications__contest=contest).exists()
    }
    if has_survey and ContestApplication.objects.filter(contest=contest,worker=worker).exists():
        items = contest.applications.get(worker=worker).items.items.filter(content_type=ContentType.objects.get_for_model(Attachment)).values_list('object_id')
        attachments = Attachment.objects.filter(pk__in=items)
        context['attachments'] = attachments
    if has_survey:
        applications = ContestApplication.objects.raw('''select * from CONTESTMANLINK cml
join V_GRANTED_PERMISSIONS gp on cml.F_CONTESTMANLINK_ID=gp.OBJECT_ID and gp.F_PERMISSIONSTYPES_NAME='review_contest_application'
where cml.F_CONTESTMANLINK_STATUS='SIGN' and USER_ID=%s and cml.F_CONTEST_ID=%s''' % (request.user.id, contest.id))
        if list(applications):
            conclusions = ExpertConclusion.objects.filter(worker=worker, application__in=applications)
            if conclusions:
                reviewed = [(x.application,x) for x in conclusions]
                unreviewed = [x for x in applications if x not in [i for i,j in reviewed]]
            else:
                reviewed = []
                unreviewed = applications
            context['reviewed'] = reviewed
            context['unreviewed'] = unreviewed

    return render(request, "contests/contest_detail.html", context)

@worker_required
def submit_application(request, worker, contest_id):
    if not worker.user:
        messages.error(request, u"Только зарегистрированные пользователи могут участвовать в конкурсах.")
        return redirect("home")
    if not has_permission(request.user, "edit", worker):
        messages.error(request, u"У вас нет прав редактировать заявки сотрудника.")
        return redirect("home")
    contest = get_object_or_404(Contest, pk=contest_id)
    if not contest.is_active and not is_admin_user(request.user):
        messages.error(request, u"Прием заявок на конкурс закрыт.")
        return redirect("home")
    redirect_kwargs = {'slug': contest.get_root().slug}
    #
    # Non-general code follows
    #
    # show additional data from, if needed
    profile = worker.profile
    from forms import ExtraDataForm, ScientificTopic
    from workers.models import UserTopic
    from journals.models import JournalRubric
    if request.method == 'GET' or not request.POST.get('keywords', None):
        users_topics = UserTopic.objects.filter(user=request.user).values_list('rubric__id', flat=True)
        initial = {
            'keywords': profile.keywords,
            'engkeywords': profile.engkeywords,
            'description': profile.sciarea,
            'topics': users_topics[0] if users_topics else None,
            }
        context = {'contest': contest,
                   'form': ExtraDataForm(initial=initial),
                   }
        return render(request, "contests/apply_details.html", context)
    # save additional data
    if request.method == 'POST' and request.POST.get('keywords', None):
        data_form = ExtraDataForm(request.POST or None)
        profile.keywords = request.POST.get('keywords', '')[:1800]
        profile.engkeywords = request.POST.get('engkeywords', '')[:1800]
        profile.sciarea = request.POST.get('description', '')[:1800]
        profile.save()
        UserTopic.objects.filter(user=request.user, rubric__in=ScientificTopic.objects.all()).delete();
        try:
            topic = request.POST.get('topics', None)
            rub = JournalRubric.objects.get(id=int(topic))
            t = UserTopic(rubric=rub, user=request.user)
            t.save()
        except (ValueError, TypeError):
            pass
    # ============ End of non-general code ============
    if not contest.applications.filter(worker=worker).exists():
        application = contest.applications.create(worker=worker, creator=request.user, status="SIGN")
        if worker != get_worker(request.user):
            description = u"Сотрудник: %s" % worker.fullname
            additional_message = u" (сотрудник %s)" % worker.fullname
            redirect_kwargs['worker_id'] = worker.id
        else:
            description = ""
            additional_message = ""
        action.send(request.user, verb=u"подал заявку на конкурс", action_object=contest, target=application, description=description)
        messages.success(request, u"Заявка на конкурс подана%s." % additional_message)
    else:
        messages.warning(request, u"Заявка на конкурс уже была подана ранее.")
    return redirect('contests_detail', **redirect_kwargs)

@worker_required
def cancel_application(request, worker, contest_id):
    if not worker.user:
        messages.error(request, u"Только зарегистрированные пользователи могут участвовать в конкурсах.")
        return redirect("home")
    if not has_permission(request.user, "edit", worker):
        messages.error(request, u"У вас нет прав редактировать заявки сотрудника.")
        return redirect("home")
    contest = get_object_or_404(Contest, pk=contest_id)
    if not contest.is_active and not is_admin_user(request.user):
        messages.error(request, u"Прием заявок на конкурс закрыт.")
        return redirect("home")
    applications = contest.applications.filter(worker=worker)
    redirect_kwargs = {'slug': contest.get_root().slug}
    if applications:
        if worker != get_worker(request.user):
            description = u"Сотрудник: %s" % worker.fullname
            additional_message = u" (сотрудник %s)" % worker.fullname
            redirect_kwargs['worker_id'] = worker.id
        else:
            description = ""
            additional_message = ""
        action.send(request.user, verb=u"отозвал заявку на конкурс", action_object=contest, target=applications[0], description=description)
        applications.delete()
        messages.success(request, u"Заявка на конкурс отозвана%s." % additional_message)
    else:
        messages.warning(request, u"Заявка на конкурс не была подана.")
    return redirect('contests_detail', **redirect_kwargs)


def application_set_status(request, application_id):
    if request.user.username not in EXPERTS:
        return redirect("contests_list")

    status = request.GET.get('status', '').upper()
    if status:
      application = get_object_or_404(ContestApplication, pk=application_id)
      if application.status != status:
        description = u"%s -> %s" % (application.status, status)
        application.status = status
        application.save()
        action.send(request.user, verb=u'изменил статус заявки на конкурс', action_object=application.contest, target=application, description=description)
      return HttpResponse(simplejson.dumps({'result': 'ok'}), content_type='application/javascript')


def contests_verify_projects(request, slug):
    if not request.user.granted_permissions.filter(Q(codename='edit_all_estimates')|Q(codename='can_edit_all_estimates')).exists() and not is_admin_user(request.user):
        return redirect("contests_list")
    return redirect(sign_url('/sqlreports/createreport/contest_applicant_projects/?slug=%s' % get_object_or_404(Contest, slug=slug).slug, generate_adata(request)))


def contests_view_applications(request, contest_id):
    contest = get_object_or_404(Contest, pk=contest_id)
    if not has_permission(request.user,"view_contest_applications", contest) and not is_admin_user(request.user):
        return redirect("contests_list")
    return redirect(sign_url('/sqlreports/createreport/contest_applications/?contest_id=%s' % contest.id, generate_adata(request)))


class SurveyWizard(MyModelWizardView):
    model = ContestApplication
    form_list = ()
    no_previous_forms = True
    file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'contests/tmpfiles'))

    @classonlymethod
    def as_view(self, form_list=None,steps_info=None, **kwargs):
        return super(MyModelWizardView, self).as_view(form_list,steps_info, **kwargs)

    def process_step(self, form):
        if self.steps.current == u'0':
            contest = self.kwargs.get('contest',None)
            worker = self.kwargs.get('worker',None)
            if not contest.applications.filter(worker=worker).exists():
                items = ObjectList.objects.create(name=u"Заявка на конкурс %s" % worker.fullname,category=u"Заявки",code=contest.slug,user=worker.user)
                application = contest.applications.create(worker=worker, creator=self.request.user, status="EDIT", items=items)
            else:
                application = contest.applications.get(worker=worker)
            # application = form.save(self.request)
            # self.set_extra_data('department', form.cleaned_data['department'])
            form.save(self.request, application)
            self.set_extra_data('application_instance', application)
        else:
            application = self.get_extra_data('application_instance')
            # notice, that we store project only from first step,
            # because we don't need updated during wizard versions
            form.save(self.request, application)
        return super(SurveyWizard, self).process_step(form)

    def get_template_names(self):
        return 'contests/add_edit_object_wizard.html'

    def get_context_data(self, form, **kwargs):
        context = super(SurveyWizard, self).get_context_data(form, **kwargs)
        contest = self.kwargs.get('contest',None)
        context['surveypage'] = SurveyPage.objects.get(contest=contest,order=1+int(self.steps.current))
        context['nested_fields'] = ["id_%s-custom_%i" % (self.steps.current,x.id) for x in list(SurveyField.objects.filter(surveypage__order=1+int(self.steps.current),surveypage__contest=contest, parentfield__isnull=True))]
        field_comment = {}
        for x in list(SurveyField.objects.filter(surveypage__order=1+int(self.steps.current),surveypage__contest=contest,comment__isnull=False)):
            field_comment["id_%s-custom_%i" % (self.steps.current,x.id)] = x.comment
        context['field_comment'] = field_comment

        return context

    def get_form_kwargs(self, step=None):
        kwargs = super(SurveyWizard,self).get_form_kwargs(step)
        contest = self.kwargs.get('contest',None)
        worker = self.kwargs.get('worker',None)
        try:
            application = contest.applications.get(worker=worker)
            if application:
                val_dict = {}
                fields = contest.contest_surveypage.get(order=int(step)+1).related_objects
                for field in fields:
                    model = field.contenttype.model_class()
                    field_items = list(application.items.items.filter(group=field.id))
                    objs = []
                    for item in field_items:
                        objs.append(model.objects.get(id=item.object_id))
                    val_dict['custom_%i' % field.id] = objs
            kwargs['val_dict'] = val_dict
        except:
            kwargs['val_dict'] = {}
        return kwargs
    def done(self, form_list, **kwargs):
        for step in self.storage.data['step_files']:
            for file in self.storage.data['step_files'][step].keys():
                self.file_storage.delete(self.storage.data['step_files'][step][file]['name'])
        contest = self.kwargs.get('contest',None)
        worker = self.kwargs.get('worker',None)
        redirect_kwargs = {'slug': contest.get_root().slug}
        # additional_message = ""
        if worker != get_worker(self.request.user):
            # additional_message = u" (сотрудник %s)" % worker.fullname
            redirect_kwargs['worker_id']=worker.id
        # messages.success(self.request, u"Анкета заполнена%s." % additional_message)
        return redirect('contests_detail', **redirect_kwargs)


@worker_required
def surveyWizardView(request,worker,slug):
    contest = get_object_or_404(Contest, slug=slug)
    if ContestApplication.objects.filter(contest=contest,worker=worker).exclude(status=u"EDIT").exists():
        redirect_kwargs = {'slug': contest.get_root().slug}
        if worker != get_worker(request.user):
            redirect_kwargs['worker_id']=worker.id
        return redirect('contests_detail', **redirect_kwargs)
    surveypages = SurveyPage.objects.filter(contest=contest)
    form_list = []
    for sp in surveypages:
        form_list.append(getSurveyPageForm(sp))
    return SurveyWizard.as_view(form_list)(request, contest=contest,worker=worker)

@worker_required
def printSurvey(request,worker,slug):
    contest = get_object_or_404(Contest, slug=slug)
    redirect_kwargs = {'slug': contest.get_root().slug}

    if worker != get_worker(request.user):
        redirect_kwargs['worker_id']=worker.id
    application = contest.applications.get(worker=worker)
    if not application or application.status == u'EDIT':
        return redirect('contests_detail', **redirect_kwargs)

    surveypages, fieldsDict, valuesDict = getApplicationDicts(contest,application)

    return render(request, "contests/printsurvey.html",{'contest':contest,'surveypage':surveypages,'values_dict':valuesDict, 'fields_dict':fieldsDict, 'date':date.today()})


@worker_required
def showSurvey(request,worker,slug,contestmanlink_id):
    contest = get_object_or_404(Contest, slug=slug)
    redirect_kwargs = {'slug': contest.get_root().slug}

    if worker != get_worker(request.user):
        redirect_kwargs['worker_id']=worker.id
    application = contest.applications.get(id=contestmanlink_id)
    if not application or application.status == u'EDIT':
        return redirect('contests_detail', **redirect_kwargs)

    surveypages, fieldsDict, valuesDict = getApplicationDicts(contest,application,links=True)
    return render(request, "contests/showsurvey.html",{'contest':contest,'surveypage':surveypages,'values_dict':valuesDict, 'fields_dict':fieldsDict})

def getApplicationDicts(contest,application,links=False):
    surveypages = SurveyPage.objects.filter(contest=contest).select_related('surveypage_field')
    fieldsDict = {}
    sql = '''select sf.F_SURVEYFIELD_ID, sf.F_SURVEYFIELD_NAME, ml.F_MARKEDLISTID_DJANGOTYPE, ml.F_MARKEDLISTID_VAL, ml.F_MARKEDLISTID_VALSTR from SURVEYFIELD sf
left join (select * from MARKEDLISTID where F_MARKEDLIST_ID=%s) ml on sf.F_SURVEYFIELD_ID=ml.F_MARKEDLISTID_GROUP
join SURVEYPAGE sp on sp.F_SURVEYPAGE_ID = sf.F_SURVEYPAGE_ID
start WITH sf.F_SURVEYFIELD_PARENT is null
CONNECT BY prior sf.F_SURVEYFIELD_ID=sf.F_SURVEYFIELD_PARENT
order SIBLINGS BY sp.F_SURVEYPAGE_NUM, sf.F_SURVEYFIELD_NUM
''' % application.items.id
    cursor = connection.cursor()
    cursor.execute(sql)
    valuesDict = {}
    for item in cursor:
        if not item[2]:
            continue
        model = ContentType.objects.get(pk=item[2]).model_class()
        if model == SurveyField:
            continue
        elif model == SurveyTextContent:
            valuesDict[item[0]] = [model.objects.get(pk=item[3]).value,]
        elif model == Worker:
            man = model.objects.get(pk=item[3])
            employments = []
            for employment in man.employments.all():
                if employment.position:
                    employments.append(u"%s, %s, " % (employment.position.shortname, employment.department) + ', '.join([u"%s"%x for x in employment.department.ancestors]))

            if links:
                if man.user:
                    valuesDict[item[0]] = [u"<a href='%s' target='_blank'>%s</a>%s,<br>%s<br>Страница в ИАС \"ИСТИНА\": //istina.msu.ru%s<br>Email: %s" % (man.get_absolute_url(),man, man.degree_rank_str_with_comma , u'<br>'.join(employments), man.get_absolute_url(), man.user.email),]
                else:
                    valuesDict[item[0]] = [u"<a href='%s' target='_blank'>%s</a>%s,<br>%s<br>Страница в ИАС \"ИСТИНА\": //istina.msu.ru%s" % (man.get_absolute_url(),man, man.degree_rank_str_with_comma , u'<br>'.join(employments), man.get_absolute_url()),]

            else:
                if man.user:
                    valuesDict[item[0]] = [u"%s%s,<br>%s<br>Страница в ИАС \"ИСТИНА\": //istina.msu.ru%s<br>Email: %s" % (man, man.degree_rank_str_with_comma , u'<br>'.join(employments), man.get_absolute_url(), man.user.email),]
                else:
                    valuesDict[item[0]] = [u"%s%s,<br>%s<br>Страница в ИАС \"ИСТИНА\": //istina.msu.ru%s" % (man, man.degree_rank_str_with_comma , u'<br>'.join(employments), man.get_absolute_url()),]

        elif model == Article:
            if not valuesDict.has_key(item[0]):
                valuesDict[item[0]] = []
            article = model.objects.get(pk=item[3])
            if links:
                valuesDict[item[0]].append(u"%s <a href='%s' target='_blank'>Перейти на страницу публикации</a>" % (article.get_citation(),article.get_absolute_url()))
            else:
                valuesDict[item[0]].append(u"%s" % (article.get_citation()))
        elif model == Patent:
            if not valuesDict.has_key(item[0]):
                valuesDict[item[0]] = []
            obj = model.objects.get(pk=item[3])
            if links:
                valuesDict[item[0]].append(u"%s. %s <a href='%s' target='_blank'>Перейти на страницу патента</a>" %(obj.authors_string, obj, obj.get_absolute_url()))
            else:
                valuesDict[item[0]].append(u"%s. %s" %(obj.authors_string, obj))
        elif model == Attachment:
            attachment = model.objects.get(pk=item[3])
            if links:
                valuesDict[item[0]] = [u"<a target='_blank' href='/media/%s'>Загрузить файл</a>" % attachment.link,]
            else:
                valuesDict[item[0]] = [u"Распечатать отдельно"]

    for sp in surveypages:
        fieldsDict[sp] = list(SurveyField.objects.raw("""select * from SURVEYFIELD where F_SURVEYPAGE_ID=%s
start WITH F_SURVEYFIELD_PARENT is null
CONNECT BY prior F_SURVEYFIELD_ID=F_SURVEYFIELD_PARENT
order SIBLINGS BY F_SURVEYFIELD_NUM""" % sp.id))
    return surveypages, fieldsDict, valuesDict

    return render(request, "contests/printsurvey.html",{'contest':contest,'surveypage':surveypages,'values_dict':valuesDict, 'fields_dict':fieldsDict, 'date':date.today()})



@worker_required
def signSurvey(request,worker,contest_id):
    contest = Contest.objects.get(pk=contest_id)
    application = contest.applications.get(worker=worker)
    application.status = u'SIGN'
    application.save()
    redirect_kwargs = {'slug': contest.get_root().slug}
    if worker != get_worker(request.user):
        description = u"Сотрудник: %s" % worker.fullname
        additional_message = u" (сотрудник %s)" % worker.fullname
        redirect_kwargs['worker_id'] = worker.id
    else:
        description = ""
        additional_message = ""
    action.send(request.user, verb=u"подписал заявку на конкурс", action_object=contest, target=application, description=description)
    messages.success(request, u"Заявка на конкурс подписана%s." % additional_message)
    return redirect('contests_detail', **redirect_kwargs)


@worker_required
def expertsurvey(request, worker, slug,contestmanlink_id):
    contest = get_object_or_404(Contest, slug=slug)
    application = get_object_or_404(ContestApplication, pk=contestmanlink_id)
    form = getExpertConclusionForm(contest, worker, application)(request.POST or None)
    if request.POST:
        if form.is_valid():
            criteria = ContestCriterion.objects.filter(contest=contest)
            surveyTexstFieldContentType = ContentType.objects.get_for_model(SurveyTextContent)
            if not ExpertConclusion.objects.filter(worker=worker, application=application).exists():
                items = ObjectList.objects.create(name=u"Экспертное заключение на заявку %s" %application.id,category=u"Экспертные заключения",code=slug,user=request.user)
                totalmark = 0
                for i, question in enumerate(criteria):
                    id = question.id
                    mark = form.cleaned_data['mark_%s'%id]
                    totalmark += int(mark)
                    comment = form.cleaned_data['comment_%s'%id]
                    item = SurveyTextContent.objects.create(value="%s;%s"%(mark,comment))
                    ObjectListItem.objects.create(object_id = item.id, object_list=items,group=id, class_name=surveyTexstFieldContentType.name, content_type=surveyTexstFieldContentType, title=item.__unicode__()[:255])
                ExpertConclusion.objects.create(application=application,mark=totalmark,comment=form.cleaned_data['summary'],worker=worker,items=items,creator=worker.user)
            else:
                conclusion = ExpertConclusion.objects.get(worker=worker,application=application)
                updatelist = []
                for field in form.changed_data:
                    if field == 'summary':
                        updatelist.append(field)
                    elif field.split('_')[1] not in updatelist:
                        id = field.split('_')[1]
                        updatelist.append(str(id))
                for field in updatelist:
                    if field == 'summary':
                        conclusion.comment = form.cleaned_data[field]
                    else:
                        textitem = SurveyTextContent.objects.get(pk=conclusion.items.items.get(group=field).object_id)
                        markedlistitem = conclusion.items.items.get(object_id=textitem.id)
                        oldmark = textitem.value.split(';')[0]
                        newmark = form.cleaned_data["mark_%s" % field]
                        conclusion.mark = conclusion.mark - int(oldmark) + int(newmark)
                        newstring = "%s;%s" %(newmark,form.cleaned_data["comment_%s" % field])
                        textitem.value = newstring
                        markedlistitem.title=newstring[:255]
                        textitem.save()
                        markedlistitem.save()
                conclusion.save()
            return redirect('contests_detail',slug=slug)


    return render(request, "contests/expert.html",{'form':form, 'contest':contest, 'application':application})


@login_required
def showExpertConclusions(request, slug):
    contest = get_object_or_404(Contest, slug=slug)
    if not has_permission(request.user, "view_contest_results",contest):
        return redirect('contests_detail', slug=slug)
    applications = ContestApplication.objects.filter(contest=contest,status='SIGN', experts_conclusions__mark__isnull=False)
    appdict = {}
    for app in applications:
        if not appdict.has_key(app):
            appdict[app] = {}
        conclusions = ExpertConclusion.objects.filter(application=app)
        appdict[app]['conclusions'] = conclusions
        appdict[app]['avgmark'] = conclusions.aggregate(Avg('mark'))['mark__avg']

    return render(request,"contests/results.html", {'contest':contest,'appdict':appdict})

@login_required
def showConclusion(request,slug,conclusion_id):
    contest = get_object_or_404(Contest, slug=slug)
    if not has_permission(request.user, "view_contest_results",contest):
        return redirect('contests_detail', slug=slug)
    criteria = ContestCriterion.objects.filter(contest=contest)
    conclusion = get_object_or_404(ExpertConclusion,pk=conclusion_id)
    items = conclusion.items.items
    valdict = {}
    for criterion in criteria:
        item = items.get(group=criterion.id)
        stritem = SurveyTextContent.objects.get(pk=item.object_id).value
        mark = stritem.split(';')[0]
        comment = stritem[len(mark)+1:]
        valdict[criterion] = {'comment':comment,'mark':mark}
        context = {'contest':contest,
                   'application':conclusion.application,
                   'criteria':criteria,
                   'valdict':valdict,
                   'comment':conclusion.comment,
                   'expert':conclusion.worker}

    return render(request,"contests/showconclusion.html",context)


