# -*- coding: utf-8; -*-
from common.forms import MyModelForm
from organizations.forms import DepartmentMultipleChoiceField
from .models import Event


class EventForm(MyModelForm):
    '''A simple form for events.'''
    departments = DepartmentMultipleChoiceField(label=u'Подразделения-организаторы')
    css_classes = [('location', 'wide'), ('departments', 'wide'), ('participants_count', 'narrow'), ('description', 'wide')]

    class Meta:
        model = Event
        exclude = ("creator", )

    def commit_save(self, request, object):
        '''Method that is executed just before final commiting the object into a database.'''
        super(EventForm, self).commit_save(request, object)
        departments = self.cleaned_data['departments']
        object.event_departments.exclude(department__in=departments).delete()
        for department in departments:
            object.event_departments.get_or_create(department=department)
