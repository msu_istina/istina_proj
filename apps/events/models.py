# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import date as date_filter
from common.models import MyModel


class Event(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_EVENT_ID")
    name = models.CharField(u"Название", max_length=255, db_column="F_EVENT_NAME")
    category = models.ForeignKey("EventCategory", related_name="events", verbose_name=u"Тип мероприятия", db_column="F_EVENTCATEGORY_ID")
    departments = models.ManyToManyField("organizations.Department", related_name="events", verbose_name=u"Подразделения-организаторы", through="EventDepartment")
    location = models.CharField(u"Место проведения", max_length=1000, blank=True, db_column="F_EVENT_LOCATION")
    startdate = models.DateField(u"Дата начала", db_column="F_EVENT_STARTDATE")
    enddate = models.DateField(u"Дата окончания", blank=True, null=True, db_column="F_EVENT_ENDDATE")
    participants_count = models.PositiveIntegerField(u"Количество участников", blank=True, null=True, db_column="F_EVENT_PARTICIPANTS")
    description = models.TextField(u"Описание", blank=True, db_column="F_EVENT_DESCRIPTION")
    creator = models.ForeignKey("auth.User", related_name="events_added", null=True, blank=True, db_column="F_EVENT_USER")

    nominative_en = "event"
    genitive = u"мероприятия"
    genitive_plural_full = u"мероприятий"
    instrumental = u"мероприятием"
    locative = u"мероприятии"
    gender = u"neuter"

    class Meta:
        db_table = "EVENT"
        verbose_name = u"мероприятие"
        verbose_name_plural = u"мероприятия"
        get_latest_by = "startdate"
        ordering = ('-startdate', 'category')

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('events_detail', (), {'object_id': self.id})

    @property
    def dates_text(self):
        text = date_filter(self.startdate, "j E Y")
        if self.enddate and self.enddate != self.startdate:
            text += " - " + date_filter(self.enddate, "j E Y")
        return text

    @property
    def title(self):
        return self.name



class EventCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_EVENTCATEGORY_ID")
    name = models.CharField(u"Название", max_length=255, db_column="F_EVENTCATEGORY_NAME")

    class Meta:
        db_table = "EVENTCATEGORY"
        verbose_name = u"Тип мероприятия"
        verbose_name_plural = u"Типы мероприятий"
        ordering = ('name', )

    def __unicode__(self):
        return self.name


class EventDepartment(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_EVENTDEP_ID")
    event = models.ForeignKey("Event", related_name="event_departments", verbose_name=u"событие", db_column="F_EVENT_ID")
    department = models.ForeignKey("organizations.Department", related_name="event_departments", verbose_name=u"подразделение-организатор", db_column="F_DEPARTMENT_ID")

    class Meta:
        db_table = "EVENTDEP"
        verbose_name = u"подразделение-организатор мероприятия"
        verbose_name_plural = u"подразделения-организаторы мероприятий"

    def __unicode__(self):
        return u"%s: %s" % (self.department, self.event)

from .admin import *