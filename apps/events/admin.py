from common.utils.admin import register_with_versions
from .models import Event, EventCategory

register_with_versions(Event)
register_with_versions(EventCategory)
