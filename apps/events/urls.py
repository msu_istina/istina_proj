# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_detail
from common.views import MyModelWizardView as Wizard
from .forms import EventForm
from .models import Event
from organizations.models import Department

EventWizard = Wizard.create_wizard_from_form(EventForm)

options_base = {'model': Event}
options_detail = dict(options_base.items() + [('template_name', 'events/detail.html')])

urlpatterns = patterns('common.views',
    url(r'^add/$', EventWizard.as_view(), name='events_add'),
    url(r'^(?P<object_id>\d+)/$', 'detail', options_detail, name='events_detail'),
    url(r'^(?P<object_id>\d+)/edit/$', EventWizard.as_view(), name='events_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', options_base, name='events_delete'),
# ) + patterns('events.views',
    url(r'^department/(?P<object_id>\d+)/$', object_detail, {
	    	'queryset': Department.objects.all(),
	    	'template_object_name': 'department',
	    	'template_name': 'events/list_department.html'},
		name='events_list_department'),
)
