# -*- coding: utf-8; -*-
from django import forms
from django.forms import ModelForm

from common.widgets import LightSelect2ChoiceWidget
from workers.models import Worker
from workers.models import Worker, Position
from organizations.models import Department

from depadmin.models import Manlist, ManlistItem

class ManlistForm(forms.ModelForm):
    class Meta:        
        model = Manlist
        fields = ('title',)


from common.forms import Select2SingleWorkerField

class StaffSelect2Field(Select2SingleWorkerField):
    def __init__(self, *args, **kwargs):
        super(StaffSelect2Field, self).__init__(self, *args, add_option=False, model=Worker, **kwargs)
        self.widget.options['placeholder'] = u'Введите фамилию'


class ManlistItemSearchForm(forms.Form):
    worker = StaffSelect2Field(label=u'Поиск сотрудников',
                               queryset=Worker.objects.all(),
                               required=True)


class PostCancelationForm(forms.Form):
    enddate = forms.DateField(label=u'Дата окончания работы', required=True)
    
    def __init__(self, *args, **kwargs):
        super(PostCancelationForm, self).__init__(*args, **kwargs)
        self.fields['enddate'].widget.attrs['class'] = 'narrow datepicker'
        self.fields['enddate'].widget.attrs['placeholder'] = '21.05.2015'

class AddWorkerOrUserForm(forms.Form):
    workplace = forms.ModelChoiceField(label=u'Место работы', required=True, queryset=Department.objects.all(), widget=LightSelect2ChoiceWidget(width=520))
    position = forms.ModelChoiceField(label=u'Должность', required=True, queryset=Position.objects.all(), widget=LightSelect2ChoiceWidget(width=520))
    fullname = forms.CharField(label=u'Фамилия, имя, отчество')
    #digest = forms.CharField(required=False)#, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        managable_deps = kwargs.pop('deps', None)
        super(AddWorkerOrUserForm, self).__init__(*args, **kwargs)
        self.fields['workplace'].queryset = managable_deps
