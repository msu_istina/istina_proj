# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from common.utils.context import render_to_response
from django.contrib import messages

from depadmin.utils import check_access_permissions
from sqlreports import generate_adata


@login_required
def datacleaning_bulk_edit(request):    
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'datacleaning_bulkedit')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    context = {}
    context["adata"] = generate_adata(request)
    context["depadmin_permissions"] = depadmin_permissions
    return render_to_response(request, "depadmin/datacleaning/bulk_edit.html",
                              context)
