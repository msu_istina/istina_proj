# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from common.utils.context import render_to_response
from django.contrib import messages

from django.db.models import Max
from collections import namedtuple
from django.db import connection

from depadmin.utils import check_access_permissions

@login_required
def security_index(request):
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'security')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    return render_to_response(request, "depadmin/security/index.html",
                              {"requester": user,
                               "depadmin_permissions": depadmin_permissions,})

@login_required
@user_passes_test(lambda u: u.is_superuser or u.representatives.organization_level(), login_url="/home/")
def security_permissions_index(request):
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'security')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    return render_to_response(request, "depadmin/security/permissions_index.html",
                              {"requester": user,
                               "depadmin_permissions": depadmin_permissions,})

@login_required
@user_passes_test(lambda u: u.is_superuser or u.representatives.organization_level(), login_url="/home/")
def security_representatives_index(request):
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'security')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    return render_to_response(request, "depadmin/security/representatives_index.html",
                              {"requester": user,
                               "depadmin_permissions": depadmin_permissions,})

