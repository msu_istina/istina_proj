# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from common.utils.context import render_to_response
from django.contrib import messages
from django.db.models import Q
import datetime
import hashlib

from unified_permissions.models import AllGrantedPermissions

from organizations.models import Department
from organizations.utils import is_representative_for
from organizations.utils import get_representatives_departments

from workers.models import Worker
from workers.models import EmploymentClaim
from workers.models import Employment
from workers.forms import EmploymentForm

from depadmin.utils import check_access_permissions

from depadmin.forms import PostCancelationForm
from depadmin.forms import AddWorkerOrUserForm


def get_human_resoures_departments(user):
    return AllGrantedPermissions.objects.current.\
           filter(user=user,
                  role_name__in=('human_resources','is_representative_for')).\
           values('department')

def can_confirm_employment(user, employment_or_claim):
    deps = get_human_resoures_departments(user).filter(department=employment_or_claim.department)
    return deps.exists() or user.is_superuser

def get_unconfirmed_employments(user, include_declined=False):
    '''Returns all employment claims that are related to user managable departments.'''
    if user.is_superuser:
        return EmploymentClaim.objects.all()
    deps = get_human_resoures_departments(user)
    return EmploymentClaim.objects.filter(department__in=deps)

@login_required
def accept_user_registration(request, emp_claim_id):
    employment_claim = get_object_or_404(EmploymentClaim, pk=emp_claim_id)
    if can_confirm_employment(request.user, employment_claim):
        emp = employment_claim.accept()
        messages.success(request, u"Место работы подтверждено.")
    else:
        messages.success(request, u"Нет прав для выполнения данной операции.")
    return redirect('depadmin_users_registration')


@login_required
def decline_user_registration(request, emp_claim_id):
    employment_claim = get_object_or_404(EmploymentClaim, pk=emp_claim_id)
    if can_confirm_employment(request.user, employment_claim):
        employment_claim.delete()
        messages.success(request, u"Место работы отклонено.")
    else:
        messages.success(request, u"Нет прав для выполнения данной операции.")
    return redirect('depadmin_users_registration')


@login_required
def users_registration(request):
    "Домашняя страница для подтверждения места работы."
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'edit_users_posts')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("home")
    include_declined = False
    if request.method == 'POST' and not request.POST.get('include_declined', None):
        include_declined = True
    emplyments_to_confirm = get_unconfirmed_employments(user, include_declined=include_declined)
    return render_to_response(request, "depadmin/users_registration.html",
                              {"requester": user,
                               "employments_to_confirm": emplyments_to_confirm,
                               "depadmin_permissions": depadmin_permissions,})


@login_required
def users_posts(request):
    "Домашняя страница для редактирования должностей. Просмотр списка и поиск по фамилии"
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'edit_users_posts')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("home")

    nrows_limit = 1000
    employees = None
    if user.is_superuser:
        employees = Employment.objects.all()
    else:
        subtree = get_human_resoures_departments(user)
        employees = Employment.objects.filter(department__in=subtree)
    
    search_term = request.POST.get('search_term', None)
    if search_term:
        employees = employees.filter(Q(worker__lastname__istartswith=search_term) | Q(worker__profile__lastname__istartswith=search_term))
        nrows_limit = 10000
    else:
        employees = employees.filter(Q(enddate__gte=datetime.date.today()) | Q(enddate__isnull=True))
    employees = employees.select_related('worker', 'worker__profile').order_by('worker__profile__lastname')[:nrows_limit]
    return render_to_response(request, "depadmin/users_posts.html",
                              {"requester": user,
                               "employees": employees,
                               "nrows_limit": nrows_limit,
                               "search_term": search_term,
                               "depadmin_permissions": depadmin_permissions,
                               })


@login_required
def xhr_editable_list_of_positions(request):
    from workers.models import Position
    from django.utils import simplejson

    data = {}
    for p in Position.objects.all():
        data[p.id] = p.shortname
        
    return HttpResponse(simplejson.dumps(data))

@login_required
def user_post_save_jseditable(request):
    from workers.models import Position
    if request.method == 'POST':
        position_id = request.POST.get('newvalue', None)
        emp_id = request.POST.get('elementid', None)
        employment = Employment.objects.get(pk=emp_id)
        position = Position.objects.get(pk=position_id)
        if can_confirm_employment(request.user, employment):
            employment.position = position
            employment.save()
            return HttpResponse(position.name)
        else:
            return HttpResponse(u"Нет прав для изменения")
    return HttpResponse(u"Ошибка")


@login_required
def user_post_edit(request, emp_id):
    user = request.user
    employment = get_object_or_404(Employment, pk=emp_id)
    if not can_confirm_employment(request.user, employment):
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_users_posts")

    form = EmploymentForm(request.POST or None, instance=employment)

    if request.method == 'POST' and form.is_valid():
        if request.POST.get('fix', None):
            employment = form.save()
            messages.success(request, u"Данные о месте работы сохранены.")
        elif request.POST.get('change_position', None):
            old_employment = get_object_or_404(Employment, pk=emp_id)
            new_form = EmploymentForm(request.POST)
            new_employment = new_form.save(commit=False)
            
            new_employment.worker = employment.worker
            new_employment.department = employment.department
            old_employment.enddate = new_employment.startdate
            old_employment.save()
            new_employment.save()
            messages.success(request, u"Сотрудник переведен на новую должность.")
        elif request.POST.get('delete_position', None):
            employment.delete()
            messages.success(request, u"Данные о месте работы удалены.")
        else:
            pass # by the way, this should not be possible
        return redirect("depadmin_users_posts")

    depadmin_permissions = check_access_permissions(user, 'edit_users_posts')
    return render_to_response(request, "depadmin/user_post_edit.html",
                              {"requester": user,
                               "employment": employment,
                               "form": form,
                               "depadmin_permissions": depadmin_permissions,
                               })

@login_required
def user_post_cancel(request, emp_id):
    u"Увольнение сотрудника."
    user = request.user
    employment = get_object_or_404(Employment, pk=emp_id)
    if not can_confirm_employment(request.user, employment):
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_users_posts")

    form = PostCancelationForm(request.POST or None)

    if request.method == 'POST' and form.is_valid():
        employment.enddate = form.cleaned_data['enddate']
        employment.save()
        messages.success(request, u"Дата окончания работы %s изменена." % employment.worker)
        return redirect("depadmin_users_posts")

    depadmin_permissions = check_access_permissions(user, 'edit_users_posts')
    return render_to_response(request, "depadmin/users/confirm_post_cancelation.html",
                              {"requester": user,
                               "employment": employment,
                               "form": form,
                               "depadmin_permissions": depadmin_permissions,
                               })


@login_required
def users_profiles(request):
    "Домашняя страница для работы с профилям пользователй (объединеие, блокировка)."
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'merge_profile_request')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("home")
    messages.info(request, u" На странице своих сотрудников Вы можете видеть ссылку \"Возможные дубликаты\". Эта ссылка запускает поиск дубликатов для конкретного сотрудника. На страницах дубликатов Вам будет предложено объединить их с основным профилем.")

    context = {}

    context["depadmin_permissions"] = depadmin_permissions
    return render_to_response(request, "depadmin/users/profiles_index.html",
                              context)

@login_required
def users_add_worker(request):
    depadmin_permissions = check_access_permissions(request.user, 'edit_users_posts')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("home")

    deps = get_human_resoures_departments(request.user)
    managable_deps = Department.objects.filter(pk__in=deps)
    post_data = request.POST.copy() if request.POST else None
    form = AddWorkerOrUserForm(post_data, deps=managable_deps)

    worker = None
    emp = None
    digest = ''
    if request.method == 'POST' and form.is_valid():
        confirmation_required = False
        department = form.cleaned_data['workplace']
        position = form.cleaned_data['position']
        fullname = form.cleaned_data['fullname']
        supplied_digest = request.POST.get("digest", "")
        string_to_hash = (fullname + u"NOT_SO_SECRET_SECRET").encode('utf-8')
        digest = hashlib.sha1(string_to_hash).hexdigest()
        today = datetime.date.today()
        
        worker = Worker.objects.create_from_fullname(fullname, save=False)
        if not digest == supplied_digest:
            # New name; check for similar records in thr database
            for coworker_emp in Employment.objects.filter(Q(worker__lastname__iexact=worker.lastname),
                                                          Q(department=department),
                                                          Q(enddate__isnull=True) | Q(enddate__gte=today)):
                coworker = coworker_emp.worker
                if coworker.firstname and worker.firstname \
                       and (coworker.firstname.upper() == worker.firstname.upper()):
                    messages.warning(request, u'В данном подразделении уже работает <a href="/workers/%s/" target="_blank">%s</a>. Нажмите на кнопку "Создать" еще раз для добавления нового сотрудника.' % (coworker_emp.worker.id, coworker_emp.worker))
                    confirmation_required = True
        if not confirmation_required:
            worker.save()
            emp = Employment.objects.create(worker=worker, department=department, position=position, startdate=today)
            form.data['fullname'] = None
            digest = ''
    context = {"addworker_form": form,
               "digest": digest,
               "added_worker": worker,
               "emp": emp,
               "depadmin_permissions": depadmin_permissions,
               }
    return render_to_response(request, "depadmin/users/add_worker_or_user.html",
                              context)
