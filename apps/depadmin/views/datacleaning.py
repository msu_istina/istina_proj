# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from common.utils.context import render_to_response
from django.contrib import messages
import datetime

from collections import namedtuple
from django.db import connection

from sqlreports import generate_adata

from organizations.models import Department, Organization
from organizations.utils import is_representative_for, get_representatives_departments
from organizations.utils import is_representative_for_whole_organization

from depadmin.utils import check_access_permissions
from sqlreports.views import createreport


@login_required
def datacleaning_workers_overview(request):
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'datacleaning_merge_workers')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    org_id = 214524 # MSU
    
    # Запрос, который выбирает представителей из классов потенциальных
    # дубликатов. Авторы считаются похожими, если у них совпадают
    # фамилии и похожи инициалы. Учитываются только те дубликаты,
    # которые были созданы пользователми заданного подразделения
    # (после увольнения их записи показываются еще 100 дней).
    overview_query = u'''
        select f_man_id,
               f_man_namel, f_man_namef, f_man_namem,
               nduplicates, last_created
        from
        (
        select min(m1.f_man_id) as f_man_id, m1.f_man_namel, m1.f_man_namef, m1.f_man_namem,
               count(distinct m2.f_man_id) as nduplicates, max(m2.f_man_create) last_created
        from man m1
        join man m2 on (upper(m2.f_man_namel) = upper(m1.f_man_namel))
        join workers_profile wp2 on (m1.f_man_user = wp2.user_id)
        where
          (
          (/* first name exact match */
          (length(m1.f_man_namef)>2 and length(m2.f_man_namef)>2 and upper(m1.f_man_namef) = upper(m2.f_man_namef))
           or
           ((length(m1.f_man_namef)<=2 or length(m2.f_man_namef)<=2) and substr(upper(m1.f_man_namef),1,1) = substr(upper(m2.f_man_namef),1,1))
           or
           (
           /* first name not specified */
           -- m1.f_man_namef is null or m2.f_man_namef is null
           1=0
           )
          )
          and 
          (m1.f_man_namem is null or m2.f_man_namem is null or
           m1.f_man_namem = m2.f_man_namem)
          ) /* end of name matching */
          /* and duplicate was created by "my" user */
          and wp2.user_id in (select workers_profile.user_id
                                from manspost
                                join workers_profile on (manspost.f_man_id = workers_profile.worker_id)
                                join ( select f_department_id
                                       from v_granted_roles
                                       where user_id = %s /* REQUESTER */
                                         and F_PERMISSIONSTYPES_NAME = %s /* ROLE FOR THIS TASK */
                                         and (f_department_id = %s or %s is NULL ) /* DEPARTMENT */
                                     ) gr on manspost.f_department_id = gr.f_department_id
                                where nvl(manspost.f_manspost_end, sysdate+1) >= sysdate-100
                             )
           and (nvl(length(%s),0)=0 or upper(m1.f_man_namel) = upper(%s))
        group by m1.f_man_namel, m1.f_man_namef, m1.f_man_namem
        having count(m2.f_man_id)>1
        order by last_created desc, nduplicates desc
        )
        where rownum<=%s
    '''
    nrows_limit = 200
    department_id = None # 275220
    role = 'is_representative_for'
    requester = user.id

    # Составим список подразделений, для которых пользователь является ответственным
    deps = []
    dep = None
    try:
        org = Organization.objects.get(pk=org_id)
        if is_representative_for_whole_organization(user, org) or user.is_superuser:
            deps.extend( Department.objects.filter(organization=org) )
        else:
            deps.extend( get_representatives_departments(user, recursive=False) )

        department_id = deps[0].id

        if request.method == 'POST' and 'department_id' in request.POST:
            user_provided_depid = int(request.POST.get('department_id', None))
            if user_provided_depid in [dep.id for dep in deps]:
                department_id = user_provided_depid
        dep = Department.objects.get(pk=department_id)
    except KeyError:
        messages.error(request, u"Не найдено подразделения, для которого запрошен список дубликатов авторов.")
        return redirect("depadmin_index")

    lastname = ''
    if request.method == 'POST':
        if "merge_by_name" in request.POST:
            show_free_merge_link = True
            lastname_rus = request.POST.get('lastname_rus', 'NULL')
            lastname_eng = request.POST.get('lastname_eng', 'NULL')

            #--- FIXME: call merge_candidates_list merge wizard
            GET = request.GET.copy()
            GET.update({
                '__checkbox_fn': 'merge_processor',
                '__checkbox_app_label': 'workers',
                '__checkbox_model_name': 'worker',
                'lastname_rus': lastname_rus,
                'lastname_eng': lastname_eng,
                'f_man_id': 0,
                })
            request.GET = GET
            request.method = 'GET'
            repid = 'workers_worker'
            return createreport(request, repid, check_signatures=False)
        else:
            lastname = request.POST.get('lastname', '')
            department_id = None # Поиск по всем своим организациям
            nrows_limit = 2000 # Указана фамилия, Выбираем практически все записи
    
    department_id = None

    cursor = connection.cursor()
    cursor.execute(overview_query, [requester, role, department_id, department_id, lastname, lastname, nrows_limit])
    MergeWorkersOverivewTuple = namedtuple('MergeWorkersOverivew', 'man_id, lastname, firstname, middlename, nduplicates, created')
    data = [MergeWorkersOverivewTuple(*item) for item in cursor]

    context = {}
    context["data"] = data
    context["adata"] = generate_adata(request)
    context["deps"] = deps if len(deps)>1 else None
    context["current_dep"] = dep
    context["lastname"] = lastname
    context["depadmin_permissions"] = depadmin_permissions

    return render_to_response(request, "depadmin/datacleaning/workers_overview.html",
                              context)


@login_required
def merge_wizard_done(request, app_label, model_name, button_name=None):
    """Find the most appropriate redirect when user completed merge_wizard for app/model and pressed button named button_name."""
    if app_label == 'workers':
        if model_name == 'worker':
            return redirect("depadmin_datacleaning_workers_overview")
        elif model_name == 'profile':
            return redirect("depadmin_users_profiles")
    return redirect("depadmin_datacleaning_index")
   
