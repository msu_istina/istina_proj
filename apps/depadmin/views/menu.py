# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from common.utils.context import render_to_response
from django.contrib import messages
from depadmin.utils import check_access_permissions

def show_menu_page(request, template, redirect_to, function):
    user = request.user
    depadmin_permissions = check_access_permissions(user, function)
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect_to
    messages.info(request, u" На странице своих сотрудников Вы можете видеть ссылку \"Возможные дубликаты\". Эта ссылка запускает поиск дубликатов для конкретного сотрудника. На страницах дубликатов Вам будет предложено объединить их с основным профилем.")

    return render_to_response(request, template,
                              {"depadmin_permissions": depadmin_permissions,})
    

@login_required
def index_page(request):
    return show_menu_page(request, "depadmin/index.html", redirect("home"), 'index_page')


@login_required
def index_page_nonrepr(request):
    return show_menu_page(request, "depadmin/index_norepr.html", redirect("depadmin_index"), 'datacleaning')

@login_required
def datacleaning_index(request):
    return show_menu_page(request, "depadmin/datacleaning/index.html", redirect("depadmin_index"), 'datacleaning')


