# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from common.utils.context import render_to_response
from django.contrib import messages
import datetime

from collections import namedtuple
from django.db import connection

from depadmin.models import Manlist, ManlistItem
from depadmin.forms import ManlistForm, ManlistItemSearchForm

from depadmin.utils import check_access_permissions

@login_required
def reports_groups(request, group_id=None):
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'groups')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    user = request.user
    context = {}
    my_lists = Manlist.objects.filter(creator=user)
    the_list = Manlist.objects.get(pk=group_id) if group_id else None
    list_form = ManlistForm()

    if the_list:
        search_form = ManlistItemSearchForm()
        context["search_form"] = search_form

    context["requester"] = user
    context["my_lists"] = my_lists
    context["list_form"] = list_form
    context["the_list"] = the_list
    context["depadmin_permissions"] = depadmin_permissions
    return render_to_response(request, "depadmin/reports/groups.html",
                              context)


@login_required
def reports_group_create(request):
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'groups')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    user = request.user
    context = {}
    context["depadmin_permissions"] = depadmin_permissions
    if request.method == 'POST':
        list_form = ManlistForm(request.POST)
        if list_form.is_valid():
            new_list = list_form.save(commit=False)
            new_list.creator = user
            new_list.creation_date = datetime.datetime.now().replace(microsecond=0)
            new_list.save()
            return redirect("depadmin_reports_groups")
        else:
            context["list_form"] = list_form
    return render_to_response(request, "depadmin/reports/groups.html",
                              context)

@login_required
def reports_group_delete(request, group_id):
    'Add one more worker to the manslist specified by group_id.'
    the_list = Manlist.objects.get(pk=group_id)
    if the_list.creator != request.user:
        messages.error(request, u"Этот список составлен другим пользователем")
    else:
        the_list.delete()
    return redirect("depadmin_reports_groups")
    


@login_required
def reports_group_add_memeber(request, group_id):
    'Add one more worker to the manslist specified by group_id.'
    the_list = Manlist.objects.get(pk=group_id)
    if the_list.creator != request.user:
        messages.error(request, u"Этот список составлен другим пользователем")
        return redirect("depadmin_reports_groups")

    if request.method == 'POST':
        form = ManlistItemSearchForm(request.POST)
        if form.is_valid():
            worker_id = form.cleaned_data["worker"]
            list_item = ManlistItem.objects.create(manlist=the_list,
                                                   worker=worker_id)
    return redirect("depadmin_reports_groups", group_id)


@login_required
def reports_group_remove_memeber(request, group_id, listitem_id):
    'Remove a worker worker from the manslist specified by group_id.'
    the_list = Manlist.objects.get(pk=group_id)
    if the_list.creator != request.user:
        messages.error(request, u"Этот список составлен другим пользователем")
        return redirect("depadmin_reports_groups")

    ManlistItem.objects.filter(manlist=the_list, pk=listitem_id).delete()
    return redirect("depadmin_reports_groups", group_id)
