# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from common.utils.context import render_to_response
from django.contrib import messages

from django.db.models import Max
from collections import namedtuple
from django.db import connection

from datetime import datetime

from unified_permissions import departments_ids_user_has_permission_in, has_permission
from workers.models import PersonalReport

from projects.models import Project, ProjectStatus
from organizations.utils import get_representatives_departments

from depadmin.utils import check_access_permissions
from sqlreports import generate_adata

@login_required
def reports_index(request):
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'reports_index')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    return render_to_response(request, "depadmin/reports/index.html",
                              {"requester": user,
                               "depadmin_permissions": depadmin_permissions,
                               })


@login_required
def reports_report(request):
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'reports_report')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")
    return render_to_response(request, "depadmin/reports/report.html",
                              {"requester": user,
                               "depadmin_permissions": depadmin_permissions,
                               })

@login_required
def reports_confirmation(request):
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'reports_confirmation')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    year = int(datetime.now().year)
    if datetime.now().month < 5:
        year = year-1
    return render_to_response(request, "depadmin/reports/confirmation.html",
                              {"requester": user,
                               "year": year,
                               "depadmin_permissions": depadmin_permissions,
                               "adata": generate_adata(request),
                               })



@login_required
def reports_confirm_projects(request):
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'reports_confirm_projects')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    def get_projects_to_confirm(user, status=None, limit=None):
        sql = u"""
        select p.f_project_id, s.f_pstepstatus_status, p.f_project_name, p.f_department_id, s.f_pstepstatus_create,
               m.f_man_namel || ' ' || m.f_man_namef || ' ' || m.f_man_namem as fio,
               decode(nvl(s.f_pstepstatus_status,1), 'SIGN', 'Подписан', 'CONFIRM', 'Подтвержден', 'Редактируется') status
        from project p
        inner join projstep ps on p.f_project_id=ps.f_project_id and extract(year from ps.f_projstep_end)=%s
        left join pstepstatus s on ps.f_projstep_id=s.f_projstep_id
        left join (select f_projstep_id, min(f_man_id) f_man_id
            from projstepman
            where nvl(f_projstepman_head, 0)=1
            group by f_projstep_id
        ) pm on (pm.f_projstep_id = ps.f_projstep_id)
        left join man m on (pm.f_man_id = m.f_man_id)
        where p.f_department_id
        in (select f_department_id
              from v_granted_roles
              where user_id = %s and f_permissionstypes_name in ('is_representative_for', 'confirm_projects'))
        order by decode(s.f_pstepstatus_status, 'SIGN', 0, 1), s.f_pstepstatus_create desc, s.f_project_id
        """
        year = int(datetime.now().year)
        if datetime.now().month < 5:
            year = year-1
        query_params = [year,user.id]

        if limit:
            sql = "select * from (" + sql + ") where ROWNUM<%s"
            query_params.append(int(limit))

        cursor = connection.cursor()
        cursor.execute(sql, query_params)
        ProjectWithStatus = namedtuple('ProjectWithStatus', 'project_id, status, name, department_id, date, leader_fio, status_name')
        projects = [ProjectWithStatus(*item) for item in cursor]
        return projects

    projects = get_projects_to_confirm(user)

    return render_to_response(request, "depadmin/reports/confirm_projects.html",
                              {"requester": user,
                               "projects": projects,
                               "depadmin_permissions": depadmin_permissions
                               })


@login_required
def reports_confirm_personal(request):
    u'''Показывает ссылки на все подписаннные, но не утвержденные персональные
отчеты, которые были созданы пользователем по одному из подразделений
ответственного.
    '''
    user = request.user
    depadmin_permissions = check_access_permissions(user, 'confirm_personal_reports')
    if not depadmin_permissions:
        messages.error(request, u"У Вас нет прав для выполнения данной операции.")
        return redirect("depadmin_index")

    if user.is_superuser: # and not user.representatives.exists():
        personal_reports = PersonalReport.objects.all()
    else:
        subtree = []
        subtree.extend( departments_ids_user_has_permission_in(user, 'is_representative_for') )
        subtree.extend( departments_ids_user_has_permission_in(user, 'confirm_personal_reports') )
        subtree = list(set(subtree))
        personal_reports = PersonalReport.objects.filter(department__in=subtree)

    personal_reports = personal_reports.filter(status="SIGN").select_related('department').order_by('-date')[:500]

    return render_to_response(request, "depadmin/reports/confirm_personal_reports.html",
                              {"requester": user,
                               "personal_reports": personal_reports,
                               "depadmin_permissions": depadmin_permissions
                               })
