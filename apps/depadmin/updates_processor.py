# -*- coding: utf-8; -*-

import logging
import sys

from django.db import connection, transaction

from django.core.exceptions import ObjectDoesNotExist
from sqlreports.models import CheckboxesProcessor

from icollections.models import Collection
from conferences.models import Conference
from unified_permissions import has_permission

logger = logging.getLogger('sentry_debug')

from django.contrib import messages

class XHRUpdatesProcessor(CheckboxesProcessor):
    codename = 'updates_processor'

    def update_field(cls, sqlreport, record_id, column_code, new_value, request):
        try:
            if column_code.upper() == 'F_COLLECTION_NAME':
                #print "Updating collection %s with new title: '%s'" % (record_id, new_value)
                Collection.objects.filter(pk=record_id).update(title=new_value)
            elif column_code.upper() == 'F_CONF_NAME':
                nupdated = Conference.objects.filter(pk=record_id).update(name=new_value)
            elif column_code.upper() == 'CONF_PLACE':
                nupdated = Conference.objects.filter(pk=record_id).update(location=new_value)
            elif column_code.upper() == 'ARTICLE_KEYWORDS':
                from publications.models import Article, ArticleKeyword
                a = Article.objects.get(pk=record_id)
                if not has_permission(request.user, "edit", a):
                    return u"Извините, у Вас нет прав для редактирования данной статьи."
                elif new_value != 'None':
                    naffected = ArticleKeyword.objects.filter(article=a).count()
                    if naffected == 1:
                        nupdated = ArticleKeyword.objects.filter(article=a).update(keyword=new_value)
                    elif not naffected or naffected == 0:
                        ArticleKeyword.objects.create(article=a, keyword=new_value)
                    else:
                        return "Sorry, too many rows were affected (%s), no data processed." % naffected
            elif column_code.upper() == 'F_CORRECTOR_ROLE':
                from icollections.models import CollectionEditorship
                naffected = CollectionEditorship.objects.filter(pk=record_id).update(role=new_value)
            else:
                return "Request for update of unknown field."
        except ObjectDoesNotExist:
            return "Record not found."
        except Exception as e:
            logger.error(u'UpdatesProcessor: unable to save: report=%s, record_id=%s, code="%s"' % (sqlreport.id, record_id, column_code),
                         extra={'error': e, 'request': request})
            error_message = u"При выполнения данной операции произошла непредвиденная ошибка."
            if request.user.id and request.user.is_superuser:
                error_message += (" %s" % e)
            return error_message
        return "ok"

class ArticleUnlinkProcessor(CheckboxesProcessor):
    codename = 'unlink_external_article_cbprocessor'

    def should_display_checkboxes(cls, report, request):
        return True

    def get_form_fields(cls, report_params, signer):
        from django import forms
        fields = {}
        try:
            bibsource = report_params["bibsource"]
            fields["bibsource"] = forms.CharField(widget = forms.HiddenInput(),
                                                  initial = bibsource,
                                                  label = '')
        except KeyError:
            pass
        return fields

    def do(cls, request, checkboxes_marked, checkboxes_shown):
        from statistics.models import ExternalArticle
        from publications.models import Article
        bibsource = request.POST.get("bibsource", None)
        if not bibsource:
            messages.warning(request, u"Не указан источник библиографических ссылок.")
            return None
        for article_id in checkboxes_marked:
            article = Article.objects.get(pk=article_id)
            ExternalArticle.objects.filter(article=article, category=bibsource).delete()
        return None



import re
from django.core.exceptions import ValidationError
from projects.models import Project, ProjectStep, Estimate, EstimateCategory
from unified_permissions import has_permission_to_department

class XHRPlanfinProcessor(CheckboxesProcessor):
    "Обработчик запросов для отчета редактирования смет проектов (функции планфина)."
    codename = 'planfin_processor'

    ESTIMATE_NAME_TO_ORD = {
        "TOTAL": 10,
        "SELF": 20,
        "SALERY": 30,
        "SALERY_INT": 40,
        "EQUIP": 50,
        "OTHER": 60,
        }

    def update_field(cls, sqlreport, record_id, column_code, new_value, request):
        try:
            ids = record_id.split("_")
            project = Project.objects.get(pk=ids[0])
            projstep = ProjectStep.objects.get(project=project, pk=ids[1]) if len(ids)>1 and len(ids[1])>0 else None

            order = cls.ESTIMATE_NAME_TO_ORD.get(column_code.upper(), None)
            if order:
                estimate_qs = Estimate.objects.filter(project=project, project_step=projstep, category__order=order)
                naffected = estimate_qs.count()
                # normalize user's input
                new_value = re.sub(r",", ".", new_value)
                new_value = re.sub(r"[ \n]+", "", new_value)

                if not has_permission_to_department(request.user, 'edit_all_estimates', project.department):
                    return u"У Вас нет прав для изменения данных."

                if naffected == 1:
                    nupdated = estimate_qs.update(value=new_value)
                elif naffected == 0: # Создаем новую запись со сметой
                    category = EstimateCategory.objects.get(order=order)
                    est = Estimate.objects.create(project=project,
                                                  project_step=projstep,
                                                  category=category,
                                                  value=float(new_value))
                else:
                    logger.error(u'PlnfinProcessor: attempt to update several records. record_id=%s, code="%s"' % (record_id, column_code),
                                 extra={'request': request})
                    return "Update of %s records is not allowed." % naffected
            else:
                return "Request for update of an unknown field."
        except ObjectDoesNotExist as e:
            logger.error(u'UpdatesProcessor: record not found: report=%s, record_id=%s, code="%s"' % (sqlreport.id, record_id, column_code),
                         extra={'error': e, 'request': request})
            return "Record not found."
        except ValidationError as e:
            return u"Ошибка: " + u'; '.join(e.messages)
        except Exception as e:
            logger.error(u'UpdatesProcessor: unable to save: report=%s, record_id=%s, code="%s"' % (sqlreport.id, record_id, column_code),
                         extra={'error': e, 'request': request})
            error_message = u"При выполнения данной операции произошла непредвиденная ошибка."
            if request.user.id and request.user.is_superuser:
                error_message = error_message + u'%s' % e
            return error_message
        return "ok"



from organizations.models import Department
from unified_permissions.models import AllGrantedPermissions

class XHRConfEditorProcessor(CheckboxesProcessor):
    "Обработчик запросов для отчета редактирования конференций."
    codename = 'confeditor_processor'

    def update_field(cls, sqlreport, record_id, column_code, new_value, request):
        try:
            conf = Conference.objects.get(pk=record_id)
            conf_id = conf.id

            #if not has_permission_to_department(request.user, 'edit_all_estimates', project.department):
            #    return u"У Вас нет прав для изменения данных."
            savepoint = transaction.savepoint()
            cursor = connection.cursor()

            cursor.execute("select nvl(f_department_id, 0) from t_confs where conf_id =%s", [int(record_id)])
            old_depid = cursor.fetchone()[0]
            if old_depid > 0:
                dep = Department.objects.filter(id=old_depid)
                #if not has_permission_to_department(request.user, 'is_representative_for', dep):
                granted = AllGrantedPermissions.objects.current \
                          .filter(user=request.user,
                                  role_name='is_representative_for',
                                  department = dep)
                if not granted:
                    return u"Извините, но у Вас нет прав для изменения данных. Конференция относится к другому подразделению."

            # Обнудение данных, если пользователь выбрал первый элемент списка
            if int(new_value) <= 0:
                if column_code.upper() == 'F_DEPARTMENT_NAME':
                    cursor.execute("""delete from conforg where conf_id = %s
                    and f_department_id in (select f_department_id from t_confs where conf_id = %s) """, [conf_id, conf_id])
                    #cursor.execute("update t_confs set f_department_id = null where conf_id = %s", [conf_id])
                    #return "ok"
                if column_code.upper() in ('F_DEPARTMENT_NAME', 'F_CONFSCOPE_NAME', 'F_CONFKIND_NAME'):
                    query = "update t_confs set %s = null where conf_id =%%s" % column_code.upper().replace('_NAME', '_ID')
                    cursor.execute(query, [conf_id])
                    return "ok"

            if column_code.upper() == 'F_DEPARTMENT_NAME':
                new_depid = int(new_value)
                if Department.objects.filter(id=new_depid).count() == 0:
                    return u"Подразделение с кодом '%s' не найдено" % new_depid
                #nupdated = Conference.objects.filter(pk=record_id).update(department=new_value)
                cursor.execute("delete from conforg where conf_id =%s and f_department_id = %s", [conf_id, old_depid])
                cursor.execute(u"insert into conforg (F_CONFORG_ID, CONF_ID, F_DEPARTMENT_ID, F_CONFORG_ORGNAME) " + \
                               u" values (seq_new_id.nextval, %s, %s, 'МГУ')",
                               [conf_id, new_depid])
                cursor.execute("update t_confs set f_department_id = %s where conf_id =%s", [new_depid, conf_id])
            elif column_code.upper() == 'F_CONFSCOPE_NAME':
                #nupdated = Conference.objects.filter(pk=record_id).update(scope=new_value)
                cursor.execute("update t_confs set f_confscope_id = %s where conf_id =%s", [int(new_value), conf_id])
            elif column_code.upper() == 'F_CONFKIND_NAME':
                cursor.execute("update t_confs set f_confkind_id = %s where conf_id =%s", [int(new_value), conf_id])
                #nupdated = Conference.objects.filter(pk=record_id).update(kind=new_value)
            elif column_code.upper() in ('CONF_COUNTMAN', 'CONF_COUNTFMAN', 'CONF_COUNTSELFMAN'):
                if int(new_value) >= 0:
                    sql =  ("update t_confs set %s = %%s where conf_id = %%s" % column_code)
                    cursor.execute(sql, [int(new_value), conf_id])
                else:
                    return u"Отрицательное число участников!"
            else:
                return u"Вы не должны видеть это сообщение. Поле '%s' не найдено." % column_code

            naffected = cursor.rowcount
            if not naffected == 1:
                transaction.savepoint_rollback(savepoint)
                return "Update of %s records is not allowed." % naffected
        except ValueError as e:
            return u"Несоответствие типа: %s" % e
        except ObjectDoesNotExist as e:
            logger.error(u'UpdatesProcessor: record not found: report=%s, record_id=%s, code="%s"' % (sqlreport.id, record_id, column_code),
                         extra={'error': e, 'request': request})
            return u"Вы не должны видеть это сообщени. Информация об ошибке сохранена."
        return "ok"


from patents.models import Patent
from organizations.models import Organization
class XHRPatentsProcessor(CheckboxesProcessor):
    "Обработчик запросов для отчета редактирования патентов."
    codename = 'patents_processor'

    def should_display_checkboxes(cls, report, request):
        return True

    def do(cls, request, checkboxes_marked, checkboxes_shown):
        msu = Organization.objects.get(pk = 214524) # id of MSU
        for pat_id in checkboxes_marked:
            try:
                patent = Patent.objects.get(pk = pat_id)
                if patent.rights_owner:
                    patent.rights_owner = None
                else:
                    patent.rights_owner = msu
                patent.save()
            except Patent.DoesNotExist:
                messages.error(request, u"Патент '%s' не найден!" % pat_id)
        return None


from certificates.models import RegistrationCertificate
class XHRSoftwareRegistrationProcessor(CheckboxesProcessor):
    "Обработчик запросов для отчета редактирования свидетельств о регистрации."
    codename = 'patprogram_processor'

    def should_display_checkboxes(cls, report, request):
        return True

    def do(cls, request, checkboxes_marked, checkboxes_shown):
        msu = Organization.objects.get(pk = 214524) # id of MSU
        for pat_id in checkboxes_marked:
            try:
                patent = RegistrationCertificate.objects.get(pk = pat_id)
                if patent.rights_owner:
                    patent.rights_owner = None
                else:
                    patent.rights_owner = msu
                patent.save()
            except RegistrationCertificate.DoesNotExist:
                messages.error(request, u"Свидетельство '%s' не найдено!" % pat_id)
        return None



from publications.models import Book, BookCategory, BookCategoryMembership
from unified_permissions import has_permission
class XHRBookEditorProcessor(CheckboxesProcessor):
    "Обработчик запросов для отчетов по редактированию монографий и сборников."
    codename = 'books_edit_processor'

    def should_display_checkboxes(cls, report, request):
        return False

    def do(cls, request, checkboxes_marked, checkboxes_shown):
        msu = Organization.objects.get(pk = 214524) # id of MSU
        for book_id in checkboxes_marked:
            pass
        return None

    def update_field(cls, sqlreport, record_id, column_code, new_value, request):
        def can_edit_value(user, book, field):
            if has_permission(user, "edit", book):
                return True
            if not getattr(book, field):
                return True

        try:
            user = request.user
            if column_code.upper().startswith('F_BOOK_'):
                book_id = int(record_id)
                book = Book.objects.get(pk = book_id)
                if not can_edit_value(user, book, 'pages'):
                    return u"Вы не можете редактировать это значение"
                if column_code.upper() == 'F_BOOK_REDPAGE':
                    new_value = re.sub(r",", ".", new_value)
                    Book.objects.filter(pk=book_id).update(redpages=float(new_value))
                elif column_code.upper() == 'F_BOOK_PAGES':
                    Book.objects.filter(pk=book_id).update(pages=int(new_value))
                elif column_code.upper() == 'F_BOOK_NCOPIES':
                    Book.objects.filter(pk=book_id).update(ncopies=int(new_value))
                elif column_code.upper() == 'F_BOOK_PLACE':
                    Book.objects.filter(pk=book_id).update(location=new_value)
                elif column_code.upper() in ['F_BOOK_TYPE', 'F_BOOK_GRIF']:
                    # delete old values in corresponding group of tags
                    cats_qs = BookCategory.objects.none()
                    if column_code.upper() == 'F_BOOK_TYPE':
                        cats_qs = BookCategory.objects.book_types()
                    elif column_code.upper() == 'F_BOOK_GRIF':
                        cats_qs = BookCategory.objects.book_grifs()
                    current_categories_qs = BookCategoryMembership.objects.filter(book=book, category__in=cats_qs)
                    count = current_categories_qs.count()
                    if count > 1:
                        return u"Найдено %s меток. Для редактирования этого поля, пожалуйста, перейдите на страницу объекта." % count
                    current_categories_qs.delete()
                    # create new record
                    cat_id = int(new_value)
                    if cat_id > 0:
                        new_category = BookCategory.objects.get(pk=cat_id)
                        obj = BookCategoryMembership.objects.create(book=book,
                                                                    category=new_category,
                                                                    creator=user)
                else:
                    return u"Редактирование поля '%s' не реализовано!" % column_code.upper()
        except ValueError as e:
            return u"Несоответствие типа: %s" % e
        return "ok"


from journals.models import JournalCategorization, JournalRubric
from projects.models import ProjectRubric
class XHRConfEditorProcessor(CheckboxesProcessor):
    "Обработчик запросов для отчета редактирования классификаторов проектов."
    codename = 'projtype_processor'

    def update_field(cls, sqlreport, record_id, column_code, new_value, request):
        try:
            project = Project.objects.get(pk=record_id)
            proj_id = project.id

            #if not has_permission_to_department(request.user, 'edit_all_estimates', project.department):
            #    return u"У Вас нет прав для изменения данных."
            savepoint = transaction.savepoint()
            cursor = connection.cursor()

            category = JournalCategorization.objects.get(code='UDC')
            for rubric in new_value.split(";"):
                try:
                    rub = JournalRubric.objects.get(code=rubric)
                    ProjectRubric.objects.get_or_create(project=project, journal_category=category, journal_rubric=rub)
                except (JournalRubric.DoesNotExist, ValueError, TypeError):
                    ProjectRubric.objects.create(project=project, journal_category=category, alternate_name=rubric)
            return "ok"
        except Exception as e:
            return u"Не удалось сохранить данные: %s" % e
