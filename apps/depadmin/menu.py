# -*- coding: utf-8; -*-
from django.core.urlresolvers import reverse
from depadmin.utils import check_access_permissions

def make_menu(user, permissions_checker):
    def make_item(title, url, comment='', id='', functions=[]):
        return {'id': id, 'title': title, 'comment': comment, 'url': url, 'children': [], 'functions': functions}

    def add_child(item, subitem):
        item['children'].append(subitem)

    # Годовой отчет
    reports_menu = make_item(u'Отчеты и рейтинги', reverse('depadmin_reports_report'), comment=u'Подготовка годового отчета и персональных рейтингов', id='reports')
    add_child(reports_menu,
              make_item(u'Годовой отчет', reverse('depadmin_reports_report'), functions=['reports_report']))
    add_child(reports_menu,
              make_item(u'Подтверждение данных', reverse('depadmin_reports_confirmation'), functions=['reports_confirmation']))
    add_child(reports_menu,
              make_item(u'Группы', reverse('depadmin_reports_groups'), functions=['reports_confirm_projects']))
    add_child(reports_menu,
              make_item(u'Рейтинги', '/pmodel/', functions=['pmodel']))

    # Security
    security_menu = make_item(u'Управление доступом', reverse('depadmin_security_index'), comment=u'Назначение ответственных и выдача прав доступа', id='security')
    ## if request.user.representatives.organization_level
    add_child(security_menu,
              make_item(u'Добавить разрешение', '/unified_permissions/role_permission/department/add/', functions= ['security']))
    add_child(security_menu,
              make_item(u'Назначить ответственного', '/organizations/representative/department/add/', functions= ['security']))
    #  /organizations/representative/department/add/
    add_child(security_menu,
              make_item(u'Список выданных разрешений', reverse('associates_repr_permissions_list'), functions= ['security']))
    add_child(security_menu,
              make_item(u'История изменений', '/unified_permissions/history/', functions= ['security']))

    # Users
    users_menu = make_item(u'Пользователи', reverse('depadmin_users_registration'), comment=u'Изменение должностей и подтверждение регистрации', id='manage_users')
    add_child(users_menu,
              make_item(u'Подтверждение регистрации', reverse('depadmin_users_registration'),
                        functions=['edit_users_posts']))
    add_child(users_menu,
              make_item(u'Изменение должностей', reverse('depadmin_users_posts'),
                        functions=['edit_users_posts']))
    add_child(users_menu,
              make_item(u'Профили', reverse('depadmin_users_profiles'), functions= ['merge_profile_request'],
                        comment=u'Объединение и удаление профилей (зарегистрированных пользователей)'))
    add_child(users_menu,
              make_item(u'Регистрировать', reverse('depadmin_users_registration_add'),
                        functions=['edit_users_posts']))


    # Исправление данных
    datacleaning_menu = make_item(u'Исправление данных', reverse('depadmin_datacleaning_index'), comment=u'Изменение и объеинение данных', id='data_quality')
    add_child(datacleaning_menu,
              make_item(u'Слияние дубликатов', reverse('depadmin_datacleaning_index'), functions= ['datacleaning_merge_workers']))
    add_child(datacleaning_menu,
              make_item(u'Редактирование', reverse('depadmin_datacleaning_bulk_edit'), functions= ['security'],
                        comment=u'Изменение типа монографий, присвоение клчевых слов и т.п.'))

    # Редактирование справочников
    dataedit_menu = make_item(u'Редактирование справочников', None, id='data_edit')
    
    menu_structure = [
        reports_menu,
        security_menu,
        users_menu,
        datacleaning_menu,
        dataedit_menu
        ]

    # disable items that are not accessable by the caller
    for item in menu_structure:
        top_level_satisfied = False
        if any(map(lambda f: permissions_checker.has_access(f), item.get('functions', []))):
            top_level_satisfied = True
            item['enabled'] = True
        for subitem in item.get('children', []):
            if (top_level_satisfied or any(map(lambda f: permissions_checker.has_access(f), subitem['functions']))):
                subitem['enabled'] = True
                item['enabled'] = True

    return menu_structure
