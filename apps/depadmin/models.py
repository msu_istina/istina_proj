# -*- coding: utf-8 -*-

from django.db import models, connection
from django.core.validators import MaxLengthValidator

class Manlist(models.Model):
    "List of workers created by user for grouping purpose, e.g. reporting."
    id = models.AutoField(primary_key=True, db_column="F_MANLIST_ID")
    creator = models.ForeignKey(to="auth.User", related_name="manlists_created",
                                db_column="F_MANLIST_USER",
                                null=True, blank=True)
    title = models.CharField(u"Название", max_length=255, # validators=[MaxLengthValidator(255)],
                             db_column="F_MANLIST_NAME",
                             null=False, blank=False)
    category = models.CharField(u"Тип", max_length=255, #validators=[MaxLengthValidator(255)],
                                db_column="F_MANLIST_TYPE",
                                null=True, blank=True)
    creation_date = models.DateTimeField(u"Дата создания",
                                db_column="F_MANLIST_CREATE",
                                blank=True, null=True, auto_now_add=False)
    date = models.DateTimeField(u"Дата модификации",
                                db_column="F_MANLIST_DATE",
                                blank=True, null=True,
                                auto_now_add=False)
    class Meta:
        db_table = "MANLIST"
        verbose_name = u"список сотрудников"
        verbose_name_plural = u"списки сотрудников"

class ManlistItem(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_MANLISTID_ID")
    manlist = models.ForeignKey(to=Manlist, related_name="items", verbose_name=u"Список", db_column="F_MANLIST_ID")
    worker = models.ForeignKey(to="workers.Worker", related_name="listed_in", verbose_name=u"Сотрудник",
                               db_column="F_MAN_ID", on_delete=models.PROTECT)

    class Meta:
        db_table = "MANLISTID"
        verbose_name = u"элемент списка сотрудников"
        verbose_name_plural = u"элементы списка сотрудников"

