# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

from updates_processor import XHRUpdatesProcessor

urlpatterns = patterns(
    'depadmin.views',
    url(r'^$', 'index_page', name='depadmin_index'),
    
    url(r'^usersreg/$', 'users_registration', name='depadmin_users_registration'),
    url(r'^usersreg/(?P<emp_claim_id>\d+)/accept/$', 'accept_user_registration', name='depadmin_accept_user_registration'),
    url(r'^usersreg/(?P<emp_claim_id>\d+)/decline/$', 'decline_user_registration', name='depadmin_decline_user_registration'),
    url(r'^usersreg/posts/$', 'users_posts', name='depadmin_users_posts'),
    url(r'^usersreg/posts/(?P<emp_id>\d+)/edit/$', 'user_post_edit', name='depadmin_users_posts_edit'),
    url(r'^usersreg/posts/(?P<emp_id>\d+)/cancel/$', 'user_post_cancel', name='depadmin_users_posts_cancel'),
    url(r'^usersreg/posts/jseditable/save/$', 'user_post_save_jseditable', name='depadmin_user_post_save_jseditable'),
    url(r'^usersreg/profiles/$', 'users_profiles', name='depadmin_users_profiles'),
    url(r'^usersreg/add/$', 'users_add_worker', name='depadmin_users_registration_add'),

    #reports
    url(r'^reports/$', 'reports_index', name='depadmin_reports_index'),
    url(r'^reports/report/$', 'reports_report', name='depadmin_reports_report'),
    url(r'^reports/confirmation/$', 'reports_confirmation', name='depadmin_reports_confirmation'),
    url(r'^reports/confirmation/projects/$', 'reports_confirm_projects', name='depadmin_reports_confirm_projects'),
    url(r'^reports/confirmation/personal/$', 'reports_confirm_personal', name='depadmin_reports_confirm_personal'),

    # reports groups
    url(r'^reports/groups/$', 'reports_groups', name='depadmin_reports_groups'),
    url(r'^reports/groups/(?P<group_id>\d+)/$', 'reports_groups', name='depadmin_reports_groups'),
    url(r'^reports/groups/create/$', 'reports_group_create', name='depadmin_reports_group_create'),
    url(r'^reports/groups/delete/(?P<group_id>\d+)/$', 'reports_group_delete', name='depadmin_reports_group_delete'),
    url(r'^reports/groups/(?P<group_id>\d+)/addmember/$', 'reports_group_add_memeber', name='depadmin_reports_group_add_member'),
    url(r'^reports/groups/(?P<group_id>\d+)/removemember/(?P<listitem_id>\d+)/$', 'reports_group_remove_memeber', name='depadmin_reports_group_remove_member'),
    
    # data cleaning
    url(r'^datacleaning/$', 'datacleaning_index', name='depadmin_datacleaning_index'),
    url(r'^datacleaning/bulkedit/$', 'datacleaning_bulk_edit', name='depadmin_datacleaning_bulk_edit'),
    url(r'^datacleaning/workers/$', 'datacleaning_workers_overview', name='depadmin_datacleaning_workers_overview'),
    url(r'^datacleaning/workers/free/$', direct_to_template, {'template': 'depadmin/datacleaning/workers_free_merge.html'}, name='depadmin_datacleaning_workers_free_merge'),

    # Merge wizard final redirct
    url(r'^merge_wizard/(?P<app_label>\w+)/(?P<model_name>\w+)/done/(?P<button_name>\w+)/$', 'merge_wizard_done', name='merge_wizard_done'),

    # security
    url(r'^security/$', 'security_index', name='depadmin_security_index'),
    url(r'^security/permissions/$', 'security_permissions_index', name='depadmin_security_permissions_index'),
    url(r'^security/representatives/$', 'security_representatives_index', name='depadmin_security_representatives_index'),

    #xhr
    url(r'^xhr/list_positions/$', 'xhr_editable_list_of_positions', name='xhr_editable_list_of_positions'),
)
