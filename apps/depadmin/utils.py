# -*- coding: utf-8; -*-
from itertools import chain
from collections import namedtuple
from django.db import connection
from django.core.urlresolvers import reverse
    
# Пользователь должен иметь по крайней мере одну роль для выполнения операции
FUNCTION_TO_ROLES = {
    'index_page': [],

    'datacleaning':  ['is_representative_for'],
    'datacleaning_bulkedit':  ['is_representative_for'],
    'datacleaning_merge_workers': ['is_representative_for'],
    
    'merge_profile_request': ['is_representative_for'],
    'edit_users_posts': ['is_representative_for', 'human_resources'],

    'reports_confirmation': ['is_representative_for', 'confirm_personal_reports', 'confirm_achievements', 'confirm_projects', 'edit_all_estimates'],
    'confirm_personal_reports': ['is_representative_for', 'confirm_personal_reports'],
    'reports_confirm_projects': ['is_representative_for', 'confirm_projects'],
    'reports_index': ['is_representative_for', 'review_depreports', 'edit_all_estimates'],
    'reports_report': ['is_representative_for', 'review_depreports'],
    'groups': ['is_representative_for', 'review_depreports'],

    'security': ['is_representative_for'],

    'pmodel': ['is_representative_for', 'can_view_ratings'],
}


class DepadminPermissionsInfo:
    granted_roles = [] # list of roles' names
    is_superuser = False
    user = None

    def __init__(self, granted_roles, is_superuser=False, user=None):
        self.granted_roles = granted_roles
        self.is_superuser = is_superuser
        self.user = user

    def has_access(self, function):
        granted = self.is_superuser \
                  or any(map(lambda required: self.granted_roles.get(required, None),
                             FUNCTION_TO_ROLES[function])) \
                  or not FUNCTION_TO_ROLES[function]
        # print "Has access to %s? %s" % (function, granted)
        return granted

    def granted_functions(self):
        granted = []
        for f in FUNCTION_TO_ROLES.keys():
            if self.has_access(f):
                granted.append(f)
        return granted

    def menu_structure(self):
        from depadmin.menu import make_menu
        return make_menu(self.user, self)


def check_access_permissions(user, function):

    get_granted_permissions_sql = """
    SELECT DISTINCT f_permissionstypes_name
    FROM v_granted_roles
    WHERE user_id = %s AND nvl(approved, 0)=1
      AND nvl(f_permissionwithlabel_end, sysdate+1) >= sysdate
      AND nvl(f_permissionwithlabel_begin, sysdate-1) <= sysdate
    """
    cursor = connection.cursor()
    cursor.execute(get_granted_permissions_sql, [user.id])

    granted_roles = {}
    for item in cursor:
        granted_roles[item[0]] = True

    dpi = DepadminPermissionsInfo(granted_roles, is_superuser=user.is_superuser, user=user)

    if dpi.has_access(function):
        return dpi
    return None
