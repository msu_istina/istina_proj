# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *

from common.views import autocomplete_search
from common.views import LinkedToWorkersModelWizardView as Wizard
from intrelations.models import SocietyMembership, HonoraryMembership, Traineeship
from intrelations.forms import SocietyMembershipForm, HonoraryMembershipForm, TraineeshipForm

SocietyMembershipFormWizard = Wizard.create_wizard_from_form(SocietyMembershipForm)
HonoraryMembershipFormWizard = Wizard.create_wizard_from_form(HonoraryMembershipForm)
TraineeshipFormWizard = Wizard.create_wizard_from_form(TraineeshipForm)

society_membership_options_base = {'model': SocietyMembership}
society_membership_options_detail = dict(society_membership_options_base.items() + \
    [('template_name', 'intrelations/society_membership_detail.html')])
honorary_membership_options_base = {'model': HonoraryMembership}
honorary_membership_options_detail = dict(honorary_membership_options_base.items() + \
    [('template_name', 'intrelations/honorary_membership_detail.html')])
traineeship_options_base = {'model': Traineeship}
traineeship_options_detail = dict(traineeship_options_base.items() + \
    [('template_name', 'intrelations/traineeship_detail.html')])


urlpatterns = patterns('common.views',
    url(r'^memberships/societies/add/$',
        SocietyMembershipFormWizard.as_view(), name='intrelations_society_membership_add'),
    url(r'^memberships/societies/(?P<object_id>\d+)/$',
        'detail', society_membership_options_detail, name='intrelations_society_membership_detail'),
    url(r'^memberships/societies/(?P<object_id>\d+)/edit/$',
        SocietyMembershipFormWizard.as_view(), name='intrelations_society_membership_edit'),
    url(r'^memberships/societies/(?P<object_id>\d+)/delete/$',
        'delete', society_membership_options_base, name='intrelations_society_membership_delete'),
    url(r'^societies/search/$',
        autocomplete_search, society_membership_options_base, name="intrelations_societies_search"),

    url(r'^memberships/honorary/add/$',
        HonoraryMembershipFormWizard.as_view(), name='intrelations_honorary_membership_add'),
    url(r'^memberships/honorary/(?P<object_id>\d+)/$',
        'detail', honorary_membership_options_detail, name='intrelations_honorary_membership_detail'),
    url(r'^memberships/honorary/(?P<object_id>\d+)/edit/$',
        HonoraryMembershipFormWizard.as_view(), name='intrelations_honorary_membership_edit'),
    url(r'^memberships/honorary/(?P<object_id>\d+)/delete/$',
        'delete', honorary_membership_options_base, name='intrelations_honorary_membership_delete'),
    url(r'^honorary/organizations/search/$', autocomplete_search,
        honorary_membership_options_base, name="intrelations_honorary_organizations_search"),

    url(r'^traineeships/add/$',
        TraineeshipFormWizard.as_view(), name='intrelations_traineeships_add'),
    url(r'^traineeships/(?P<object_id>\d+)/$',
        'detail', traineeship_options_detail, name='intrelations_traineeships_detail'),
    url(r'^traineeships/(?P<object_id>\d+)/edit/$',
        TraineeshipFormWizard.as_view(), name='intrelations_traineeships_edit'),
    url(r'^traineeships/(?P<object_id>\d+)/delete/$',
        'delete', traineeship_options_base, name='intrelations_traineeships_delete'),
    url(r'^traineeships/organizations/search/$', autocomplete_search,
        traineeship_options_base, name="intrelations_traineeship_organizations_search"),
)
