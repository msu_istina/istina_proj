# -*- coding: utf-8 -*-
from django.db import models
from common.models import MyModel, WorkershipModel, Country, Activity
from organizations.models import Department

class SocietyMembershipCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_socmembertype_id")
    code = models.CharField(u"Код типа членства в научном обществе", max_length=255, db_column="f_socmembertype_code")
    name = models.CharField(u"Тип членства в научном обществе", max_length=255, db_column="f_socmembertype_name")

    class Meta:
        db_table = u'socmembertype'
        verbose_name = u"тип членства в научном обществе"
        verbose_name_plural = u"типы членств в научных обществах"

    def __unicode__(self):
        return self.name


class SocietyMembership(WorkershipModel, Activity):
    u"""Membership in (mostly international) societies."""
    id = models.AutoField(primary_key=True, db_column="f_socmember_id")
    member = models.ForeignKey(to="workers.Worker", verbose_name=u"ФИО члена общества",
                               related_name="society_memberships", db_column="f_man_id",
                               null=True, blank=True)
    category = models.ForeignKey(SocietyMembershipCategory, verbose_name=u"Тип членства",
                                 db_column="f_socmembertype_id", null=True, blank=True)
    society = models.CharField(u"Название общества", max_length=1000, db_column="f_socmember_name")
    country = models.ForeignKey(Country, verbose_name=u"Страна общества",
                                related_name="society_memberships", db_column="f_country_id")
    year = models.BigIntegerField(u"Год вступления", db_column="f_socmember_year", null=True, blank=True)
    comment = models.TextField(u"Дополнительная информация", db_column="f_socmember_comment", blank=True)
    original_name = models.CharField(max_length=255, db_column="f_socmember_membername", blank=True)

    search_attr = 'society'
    worker_attr = 'member'
    workers_verbose_name_single = u"Член научного общества (ФИО)"
    workers_verbose_name_plural = u"Члены научных обществ"
    workers_required_error_msg = u"Укажите ФИО члена научного общества"

    nominative_en = "society_membership"
    nominative_short = u"членство в обществе"
    nominative_plural = u"членство в научных обществах"
    genitive = u"членства в научном обществе"
    genitive_short = u"членства в обществе"
    genitive_plural = u"членства в научных обществах"
    genitive_plural_full = u"членств в научных обществах"
    accusative_short = u"членство в обществе"
    instrumental = u"членством в научном обществе"
    locative = u"членстве в научном обществе"
    gender = "neuter"

    class Meta:
        db_table = u'socmember'
        verbose_name = u"членство в научном обществе"
        verbose_name_plural = u"членства в научных обществах"
        get_latest_by = "-year"
        ordering = ('-year', 'country')

    def __unicode__(self):
        return u"Членство в научном обществе %s, %s" % (self.society, self.workers_string)

    @models.permalink
    def get_absolute_url(self):
        return ('intrelations_society_membership_detail', (), {'object_id': self.id})

    @property
    def title(self):
        return u"%s в научном обществе %s" % (self.workers_string, self.society)

    @property
    def startyear(self):
        """Used in check_year_range method to determine that this activity should be included in
            year report. This activity has only year, and not startdate, so by default it's not included,
            though should be, because it's like membership in journal editorial board (valid for all years
            till the start year.
        """
        return self.year


class HonoraryMembershipCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_honorstype_id")
    name = models.CharField(u"Тип почетного членства", max_length=1000, db_column="f_honorstype_name")

    class Meta:
        db_table = u'honorstype'
        verbose_name = u"тип почетного членства в организации"
        verbose_name_plural = u"типы почетных членств в организациях"

    def __unicode__(self):
        return self.name


class HonoraryMembership(WorkershipModel, Activity):
    id = models.AutoField(primary_key=True, db_column="f_honors_id")
    member = models.ForeignKey(to="workers.Worker", verbose_name=u"ФИО почетного члена",
                               related_name="honorary_memberships", db_column="f_man_id",
                               null=True, blank=True)
    category = models.ForeignKey(HonoraryMembershipCategory, verbose_name=u"Тип почетного членства",
                                 db_column="f_honorstype_id")
    organization = models.CharField(u"Название организации", db_column="f_honors_organ", max_length=1000, blank=True)
    country = models.ForeignKey(Country, verbose_name=u"Страна организации", related_name="honorary_memberships", db_column="f_country_id")
    year = models.BigIntegerField(u"Год присуждения", db_column="f_honors_year", null=True, blank=True)
    comment = models.TextField(u"Дополнительная информация", db_column="f_honors_comment", blank=True)
    original_name = models.CharField(max_length=255, db_column="f_honors_name", blank=True)

    search_attr = 'organization'
    worker_attr = 'member'
    workers_verbose_name_single = u"Почетный член организации (ФИО)"
    workers_verbose_name_plural = u"Почетные члены организаций"
    workers_required_error_msg = u"Укажите ФИО почетного члена организаций"

    nominative_en = "honorary_membership"
    nominative_short = u"почетное членство в организации"
    nominative_plural = u"почетные членства в организациях"
    genitive = u"почетного членства в организации"
    genitive_short = u"почетного членства в организации"
    genitive_plural = u"почетного членства в организациях"
    genitive_plural_full = u"почетных членств в организациях"
    accusative_short = u"почетное членство в организации"
    instrumental = u"почетным членством в организации"
    locative = u"почетном членстве в организации"
    gender = "neuter"

    class Meta:
        db_table = u'honors'
        verbose_name = u"почетное членство в организации"
        verbose_name_plural = u"почетные членства в организациях"
        get_latest_by = "-year"
        ordering = ('-year', 'category')

    def __unicode__(self):
        return u"Почетное членство в организации %s, %s" % (self.organization, self.workers_string)

    @models.permalink
    def get_absolute_url(self):
        return ('intrelations_honorary_membership_detail', (), {'object_id': self.id})

    @property
    def title(self):
        return u"%s, %s в %s" % (
            self.workers_string,
            self.category,
            self.organization)

    @property
    def startyear(self):
        """Used in check_year_range method to determine that this activity should be included in
            year report. This activity has only year, and not startdate, so by default it's not included,
            though should be, because it's like membership in journal editorial board (valid for all years
            till the start year.
        """
        return self.year


class Traineeship(WorkershipModel, Activity):
    id = models.AutoField(primary_key=True, db_column="f_traineeship_id")
    trainee = models.ForeignKey(to="workers.Worker", verbose_name=u"ФИО стажера",
                                related_name="traineeships", db_column="f_man_id",
                                null=True, blank=True)
    department = models.ForeignKey(Department, verbose_name=u"Подразделение (внутреннее)", db_column="f_department_id")
    organization = models.CharField(u"Организация (внешняя)", max_length=1000, db_column="f_traineeship_organ", blank=True)
    country = models.ForeignKey(Country, verbose_name=u"Страна организации", related_name="traineeships", db_column="f_country_id")
    importing = models.NullBooleanField(u"Направление", db_column="f_traineeship_inout", null=True, blank=True)
    task = models.TextField(u"Цель стажировки", db_column="f_traineeship_action", blank=True)
    startdate = models.DateField(u"Дата начала", db_column="f_traineeship_begin", null=True, blank=True)
    enddate = models.DateField(u"Дата окончания", db_column="f_traineeship_end", null=True, blank=True)
    original_name = models.CharField(max_length=255, db_column="f_traineeship_name", blank=True)

    search_attr = 'organization'
    worker_attr = 'trainee'
    workers_verbose_name_single = u"Стажер (ФИО)"
    workers_verbose_name_plural = u"Стажеры"
    workers_required_error_msg = u"Укажите ФИО стажера"

    nominative_en = "traineeship"
    nominative_short = u"стажировка"
    nominative_plural = u"стажировки"
    genitive = u"стажировки"
    genitive_short = u"стажировки"
    genitive_plural = u"стажировки"
    genitive_plural_full = u"стажировок"
    accusative_short = u"стажировку"
    instrumental = u"стажировкой"
    locative = u"стажировке"
    gender = "feminine"

    direction_import_text = u"Стажировка внешнего сотрудника в подразделении"
    direction_export_text = u"Стажировка сотрудника подразделения во внешней организации"

    class Meta:
        db_table = u'traineeship'
        verbose_name = u"стажировка"
        verbose_name_plural = u"стажировки"
        get_latest_by = "-startdate"
        ordering = ('-startdate', '-enddate')

    def __unicode__(self):
        return u"Стажировка в %s, %s" % (self.organization, self.workers_string)

    @models.permalink
    def get_absolute_url(self):
        return ('intrelations_traineeships_detail', (), {'object_id': self.id})

    @property
    def title(self):
        return u"%s, стажировка %s %s" % (
            self.workers_string,
            u"из" if self.importing else u"в",
            self.organization)

    @property
    def direction(self):
        return self.direction_import_text if self.importing else self.direction_export_text

    @property
    def organization_from(self):
        return self.organization if self.importing else self.department.organization

    @property
    def organization_to(self):
        return self.department.organization if self.importing else self.organization

    @property
    def country_from(self):
        return self.country if self.importing else None

    @property
    def country_to(self):
        return self.country if not self.importing else None
