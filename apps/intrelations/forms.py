# -*- coding: utf-8; -*-
from actstream import action
from django import forms
from django.core import serializers

from common.forms import MyForm, MyModelForm, WorkershipModelForm, CountryChoiceField
from organizations.forms import DepartmentChoiceField
from intrelations.models import SocietyMembership, HonoraryMembership, Traineeship


class SocietyMembershipForm(WorkershipModelForm):
    country = CountryChoiceField(label=u"Страна общества")
    fields_order = ["member_str", "society", "country", "year", "category", "comment"]
    css_classes = [('society', 'wide autocomplete_society')]

    class Meta:
        model = SocietyMembership
        exclude = ('member',)


class HonoraryMembershipForm(WorkershipModelForm):
    country = CountryChoiceField(label=u"Страна организации")
    fields_order = ["member_str", "category", "organization", "country", "year", "comment"]
    css_classes = [('organization', 'wide autocomplete_honorary_organization')]

    class Meta:
        model = HonoraryMembership
        exclude = ('member',)

    def __init__(self, *args, **kwargs):
        super(HonoraryMembershipForm, self).__init__(*args, **kwargs)
        # it is already set in common/forms
        self.fields['organization'].initial = ""


class TraineeshipForm(WorkershipModelForm):
    DIRECTION_CHOICES = (
        (0, Traineeship.direction_export_text),
        (1, Traineeship.direction_import_text)
    )
    direction = forms.ChoiceField(choices=DIRECTION_CHOICES, label=u"Направление")
    department = DepartmentChoiceField(label="Подразделение (внутреннее)")
    country = CountryChoiceField(label=u"Страна организации")
    fields_order = ["trainee_str", "department", "organization",
        "country", "direction", "startdate", "enddate", "task"]
    css_classes = [('organization', 'wide autocomplete_traineeship_organization'), ('department', 'wide')]

    class Meta:
        model = Traineeship
        exclude = ('trainee', 'direction_in')

    def __init__(self, *args, **kwargs):
        super(TraineeshipForm, self).__init__(*args, **kwargs)
        # it is already set in common/forms
        self.fields['organization'].initial = ""
        if self.instance:
            self.fields['direction'].initial = int(self.instance.importing) if self.instance.importing is not None else ""

    def commit_save(self, request, object, workers):
        '''Method that is executed just before final commiting the object into a database.'''
        object.importing = bool(int(self.cleaned_data['direction']))
        super(TraineeshipForm, self).commit_save(request, object, workers)
