from common.utils.admin import register_with_versions, MyModelAdmin
from models import Journal, JournalAlias, JournalAliasCategory, JournalEditorialBoardMembership, ISSN, ISSNCategory, JournalIndexMembership, JournalIndex, JournalRubric, JournalCategorization, JournalRubricMembership, JournalRankValue, JournalSource, JournalSourceMembership

register_with_versions(Journal)
register_with_versions(JournalAlias)
register_with_versions(JournalAliasCategory)
register_with_versions(JournalEditorialBoardMembership)
register_with_versions(ISSN)
register_with_versions(ISSNCategory)
register_with_versions(JournalIndexMembership)
register_with_versions(JournalIndex)
register_with_versions(JournalCategorization)
register_with_versions(JournalRubricMembership)
register_with_versions(JournalRankValue)
register_with_versions(JournalSource)
register_with_versions(JournalSourceMembership)

class JournalRubricAdmin(MyModelAdmin):
    '''Admin class representing JournalRubric class.'''
    list_display = ('categorization', 'parent', 'code', 'name',)
    list_display_links = ('name',)
    list_filter = ('categorization',)

register_with_versions(JournalRubric, JournalRubricAdmin)


