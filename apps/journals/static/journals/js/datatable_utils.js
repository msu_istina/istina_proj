function check_status ( data, type, row ) {
    span = '<span class="ui-icon '
    if (data == "checked") {
        span += 'check'
    }
    if (data == "auto_added") {
        span += 'cj_class auto_added' /*add fake cj_class so sorting by class looked as checked - auto - not_checked */
    }
    if (data == "not_checked") {
        span += 'close'
    }
    span += '"></span'
    return span
};
function BoolToIcon ( data, type, row ) {
    span = '<span class="ui-icon '
    if (data == true) {
        span += 'check'
    } else {
        span += 'close'
    }
    span += '"></span'
    return span
};

