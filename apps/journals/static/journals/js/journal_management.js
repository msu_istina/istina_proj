var delay = (function() {
	var timer = 0;
	return function(callback, ms) {
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	};
})();

function search_string_data( aoData ) {
             var  search_field =$("#tblJournals_filter").find("input")
             var search_string = search_field.val()
             aoData.push( { "name": "search_string", "value": search_string } );
};


function fill_input(data,type,row) {
    return "<input type=text value=" + row[0] + " name=" + row[0] + ">"
}

function add_row(event) {
       if( $(event.target).is("a") ) return;
       var JournalTable = $('#tblJournals').dataTable();
       var MergeJournalTable = $('#tblMergeJournals').dataTable();
       $(this).addClass('current_row_selected')
       var data = JournalTable._('tr.current_row_selected')
       $(this).removeClass('current_row_selected')
       var id = data[0][0]
       var rows = MergeJournalTable._('tr')
       var include_row = true
       $.each(rows, function(index,value) {
            if (value[0]==id) {
                include_row = false
            }
       })
       if(include_row) {
          $(this).toggleClass('selected')
          data[0][6]=""
          MergeJournalTable.fnAddData(data);
       }

       var selected_num = MergeJournalTable._('tr').length
       $('#candidates_num').html(selected_num)

       ShowOrHideSubmit()
};
function delete_row() {
       var MergeJournalTable = $('#tblMergeJournals').dataTable();
       var row = $(this).closest("tr")
       id = row.attr("id")
       MergeJournalTable.fnDeleteRow(row[0])
       var selected_num = MergeJournalTable._('tr').length
       $('#candidates_num').html(selected_num)

       var JournalTable = $('#tblJournals').dataTable();
       var JournalTable_row = JournalTable.$('tr#'+id)
       JournalTable_row.toggleClass('selected')

       ShowOrHideSubmit()
};
function select_on_reload( nRow, aData, iDisplayIndex ){
    var ids = get_candidates_ids()
    if ( jQuery.inArray(aData.DT_RowId, ids) !== -1 ) {
        $(nRow).addClass('selected');
    }
}

function select_main(event){
    if( $(event.target).is(".delete") ) return;
    var MergeJournalTable = $('#tblMergeJournals').dataTable();
    MergeJournalTable.$('tr.main_selected').toggleClass('main_selected')
    $(this).toggleClass('main_selected')
    ShowOrHideSubmit()
}
function ShowOrHideSubmit() {
    var MergeJournalTable = $('#tblMergeJournals').dataTable();
    var selected_num = MergeJournalTable._('tr').length
    var main = $(".main_selected").length
    if (selected_num > 1 && main) {
        $('#id_merge_submit').show()
    } else {
        $('#id_merge_submit').hide()
    }
}

function on_submit() {
    var ids = get_candidates_ids();
    $.each(ids, function(index,value) {
        var input = $("<input>").attr("type", "hidden").attr("name", "candidate_" + value).val(value);
        $('#id_merge_form').append($(input));
    })

    $("#id_candidates_ids").attr('value', ids + "")
    var main = $("tr.main_selected").attr('id')
    $("#main_candidate").attr('value', main)
    return true;
}


function get_candidates_ids() {
    var MergeJournalTable = $('#tblMergeJournals').dataTable();
    var rows = MergeJournalTable.$('tr')
    var ids = []
    rows.each(function(){ids.push(parseInt(this.id))})
    return ids
};
function create_delete_button(nTd, sData, oData, iRow, iCol) {
    var input = $("<input type='submit'>")
    input.attr("title", "Убрать из списка объединяемых").attr("value","Убрать").addClass("delete")
    $(nTd).append(input)
}

$(function() {
    $('#id_merge_submit').hide()
    $('.merge_count').html("Отобрано на слияние: ")
})