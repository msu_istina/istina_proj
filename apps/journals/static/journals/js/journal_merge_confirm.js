function select_main_on_load( nRow, aData, iDisplayIndex ) {
    var id = $("#main_candidate").val()
    if (aData.DT_RowId == id ) {
        $(nRow).addClass('main_selected');
    }
}

function search_journals_by_ids( aoData ) {
    var ids = []
    $(".candidate").each(function(){ ids.push($(this).val()) })
    aoData.push( { "name": "ids", "value": ids } );
};

function generate_python_merge() {
    var main_id  = $("#main_candidate").val()
    var from_ids = []
    $(".candidate").each(function() {
        if ($(this).val() != main_id) {
            from_ids.push($(this).val())
        }
    })
    var username = $(".username").val()
    var from_ids_tuple ='(' + from_ids.join(',') + ')'
    return 'Journal.objects.merge("' + username + '", ' + from_ids_tuple + ', ' + main_id + ')'
}

$(function() {
    $(document).ready(function () {
        $(".python_merge").text('Код для слияния:  ' + generate_python_merge());
    });
});