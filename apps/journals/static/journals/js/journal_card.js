$(function() {
//    $('.formset_add_form').click(function() { // FIXME add event.preventDefault()?
//        insert_place = '#journal_aliases_formset div.form_in_formset:last'
//        parent_form_id = $(this).closest('form')[0].id
//        insert_place = '#'+ parent_form_id + ' div.form_in_formset:last'
//        formset_add_form(insert_place, 'form');
//    });

    $(".journal_index").click(function() {
        if ($(this).hasClass('check')) {
            $(this).removeClass('check');
            $(this).addClass('close')
        } else {
            $(this).removeClass('close');
            $(this).addClass('check')
        }
    });

    $("#journal_card_save").submit(function() {
        var ids = []
        $(".journal_index").each(function(){ids.push(parseInt(this.id))})
        $.each(ids, function(index, value) {
            if ($("#" + value).hasClass('check')) {
                var has_index = true
            } else {
                var  has_index = false
            }
            var input = $("<input>").attr("type", "hidden").attr("name", "index_" + value).val(has_index);
            $('#journal_card_save').append($(input));
        })
        $('#journal_aliases_formset :input').not(':submit').clone().hide().appendTo('#journal_card_save');
        $('#issn_formset :input').not(':submit').clone().hide().appendTo('#journal_card_save');
        $('#ranks_formset :input').not(':submit').clone().hide().appendTo('#journal_card_save');
        $('#source_memberships_formset :input').not(':submit').clone().hide().appendTo('#journal_card_save');
        $('#rubric_memberships_formset :input').not(':submit').clone().hide().appendTo('#journal_card_save');
        var selects = $("form select");
        $(selects).each(function(i) {
            var select = this;
            $('#journal_card_save').find("#" + $(select).attr("id")).val($(select).val());
        });
        return true;
    });
     $(".datepicker").datepicker({ changeMonth: true, changeYear: true, yearRange: '1930:2020' });
});

