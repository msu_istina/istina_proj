$(document).ready( function() {
    $('#journals').dataTable({
        "oLanguage": {
            "sLengthMenu": "Показывать по _MENU_ записей на странице",
            "sZeroRecords": "Нет записей, удовлетворяющих условиям поиска",
            "sInfo": "Показаны записи с _START_ по _END_. Всего: _TOTAL_.",
            "sInfoEmpty": "Показаны строки с 0 по 0 из 0",
            "sInfoFiltered": "(отобрано по условию из _MAX_ записей)",
            "oPaginate": {
                "sFirst": "Начало&nbsp;",
                "sLast": "&nbsp;Конец",
                "sNext": "&nbsp;&nbsp;Следующая&nbsp;&nbsp;",
                "sPrevious": "&nbsp;&nbsp;Предыдущая&nbsp;&nbsp;"
            },
            "sSearch": "Поиск:"
        },
        "sPaginationType": "full_numbers",
        "iFullNumbersShowPages": 10,
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Все"]],
        "iDisplayLength": 25
   });
    
});
