# -*- coding: utf-8 -*-
from django.db.models import get_model
from django.contrib.auth.models import User
import operator
import re
from actstream import action
from common.managers import SimilarFieldManager, MyManager
from common.utils.uniqify import uniqify
from django.db.models import Q

JOURNALS_ADMINS=['MBolshova']

class JournalManager(SimilarFieldManager):
    """Model manager for Journal model."""

    @staticmethod
    def build_abbr_re(abbr):
        """Build 'expansion' regular expression for abbreviature abbr"""
        re_int = r".*".join(map(lambda s: r"\b" + s, re.findall("\w+", abbr, re.UNICODE)))
        return re.compile(r"^\W*%s\w*\W*$" % re_int, re.IGNORECASE | re.UNICODE)

    @classmethod
    def is_abbr(klass, abbr, full):
        """Checks weather abbr is abbreviature for full"""
        return bool(klass.build_abbr_re(abbr).match(full))

    def guess_alias(self, name):
        qs = self
        JournalAlias = get_model('journals', 'JournalAlias')
        aliases_qs = JournalAlias.objects
        prefix = re.search("^\w+", name, re.UNICODE)
        if prefix is not None:
            qs = qs.filter(name__istartswith=prefix.group(0))
            aliases_qs = aliases_qs.filter(alias__istartswith=prefix.group(0))

        name_re = self.build_abbr_re(name)
        candidatates = filter(lambda i: name_re.match(i), qs.values_list("name", flat=True))
        aliases_candidates = filter(lambda i: name_re.match(i), aliases_qs.values_list("alias", flat=True))
        return self.filter(Q(name__in=candidatates) | Q(aliases__alias__in=aliases_candidates))

    def get_journals_with_similar_alias(self, name):
        JournalAlias = get_model('journals', 'JournalAlias')
        similar_aliases = JournalAlias.objects.get_similar_by_field('alias', name, cutoff=0.8,
                                                                    n=10, sort=True)
        return self.filter(aliases__alias__in=similar_aliases)

    def get_similar(self, name, return_queryset=False, get_all_variants=False):
        """Get similar journals without taking into account publisher

        Returns list of Journal's
        """
        JournalAlias = get_model('journals', 'JournalAlias')
        sources = (
            ("exact", lambda: self.filter(name__iexact=name)),
            ("alias", lambda: map(
                operator.attrgetter("journal"),
                JournalAlias.objects.filter(alias__iexact=name))
            ),
            ("guess_alias_or_similar", lambda:
            list(self.guess_alias(name)) +
            list(self.get_similar_by_field('name', name, cutoff=0.8, n=10, sort=True))
            ),
            ("with_similar_alias", lambda:
            list(self.get_journals_with_similar_alias(name))
            ),

        )
        all_variants_ids = []
        for source_type, getter in sources:
            objects = getter()
            if not get_all_variants:
                if objects or source_type == "with_similar_alias":
                    objects = uniqify(objects, idfun=operator.attrgetter("id"))
                    if not return_queryset:
                        return {"objects": objects,
                                "very_similar": source_type in ("exact", "alias") and len(objects) == 1
                        }
                    else:
                        ids = [journal.id for journal in objects]
                        return self.filter(id__in=ids)
            else:
                all_variants_ids += [journal.id for journal in objects]
        if get_all_variants:
            return self.filter(id__in=all_variants_ids)

    def merge(self, username, from_ids, to_id):
        '''Merge journal with from_ids to journal with to_id.
            from_id can be a sequence of ids or a single id.
        '''
        # user exists
        user = User.objects.get(username=username)
        if not (user.is_superuser or user.username in JOURNALS_ADMINS):
            raise Exception("user must be superuser")

        # journal exist
        j_to = self.get(id=to_id)

        if isinstance(from_ids, int):
            from_ids = (from_ids,)

        for from_id in from_ids:

            if from_id == to_id:
                raise Exception("journals must differ")

            # journal exist
            j_from = self.get(id=from_id)

            # the journal being deleted (j_from) is not a 'good' journal
            if not j_from.can_be_merged:
                raise Exception("the journal %s can not be merged" % unicode(j_from))

            # move related objects
            j_from.aliases.all().update(journal=j_to) # journal aliases
            j_from.articles.all().update(journal=j_to) # articles in journal
            j_from.rewarded_articles.all().update(journal=j_to) # rewarded articles in journal
            j_from.editorial_board_memberships.all().update(journal=j_to) # editorial board memberships in journal
            j_from.issns.all().update(journal=j_to) # issns in journal
            j_from.source_memberships.all().update(journal=j_to) # source links
            j_from.rank_values.all().update(journal=j_to) # rangs
            j_from.rubric_memberships.all().update(journal=j_to)
            j_from.index_memberships.all().update(journal=j_to) # inclusion in journal indexes (lists)

            # Journal fields
            # copy name of j_from to an alias of j_to
            alias, created = j_to.aliases.get_or_create(alias=j_from.name)
            if created:
                action.send(user, verb=u'добавил псевдоним журнала', action_object=alias, target=j_to)
                # if some fields of j_to are empty, move them from j_from
            for field in ["publisher", "url", "language", "abstract", "xml"]:
                if getattr(j_from, field) and not getattr(j_to, field):
                    setattr(j_to, field, getattr(j_from, field))

            # log action
            description = u"Удаленный журнал: %s (id %d). Его данные: \n%s" % (
                j_from.name, j_from.id, j_from.get_xml().decode('utf8'))
            action.send(user, verb=u'объединил журналы', action_object=j_from, target=j_to, description=description)

            # delete j_from
            j_from.delete()

        # this save is used not only to save publisher/url/abstract/... moving
        # it also emits a signal to update cache for all articles in j_to
        # so it is always necessary (even if publisher/.. have not been moved), because update() query does not emit any signals.
        j_to.save()


class JournalIndexManager(MyManager):
    """Model manager for JournalIndex model."""

    def get_vak_indexes(self):
        return self.filter(name__icontains=u"Список ВАК")

    def get_wos_or_scopus_indexes(self):
        return self.filter(Q(name=u"Scopus") | Q(name=u"JCR") \
                           | Q(name__startswith=u"Science Citation Index") \
                           | Q(name__startswith=u"Arts & Humanities Citation Index"))



class JournalAliasManager(SimilarFieldManager):
    pass
