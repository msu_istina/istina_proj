# -*- coding: utf-8; -*-
def get_journal_json(journal): # FIXME-greg why not make this a Journal method "as_json" ?
    # FIXME-greg add docstring. What data is presented and for what page?
    try:
        publisher = journal.publisher.name
    except AttributeError:
        publisher = ""
    count = journal.articles.count()
    try:
        username = journal.creator.username # FIXME-greg why is this named 'username'? It has now lost all meaning (username of who?). How is it displayed?
    except AttributeError:
        username = ""
    if journal.checked:
        status = "checked"
    elif journal.auto_added:
        status = "auto_added"
    else:
        status = "not_checked"
    # FIXME-greg just as suggestion: 
    # why not write 'has_issn = journal.issns.exists()' ?
    has_issn = True if journal.issns.exists() else False 
    has_vaks = True if journal.indexes.exists() else False
    all_issns = ", ".join([x.value for x in journal.issns.all()])

    link = "<a href=" + journal.get_absolute_url() + " target='_blank' >" + str(journal.id) + "</a>"
    data_list = [link, journal.name, publisher, count, has_issn, has_vaks, username, status, all_issns]
    data = dict(enumerate(data_list))
    data["DT_RowId"] = journal.id
    return data
