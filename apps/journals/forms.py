# -*- coding: utf-8; -*-
from actstream import action
from django import forms
from django.core import serializers
from django.forms.models import inlineformset_factory

from common.forms import WorkershipModelForm, MyForm, MyModelForm, Select2ModelField, Select2ModelMultipleField
from journals.models import Journal, JournalEditorialBoardMembership, JournalAlias, ISSN, JournalRankValue, JournalRubric, \
    JournalSourceMembership, JournalRubricMembership
from publishers.models import  Publisher
from django.contrib import messages
from django.db import connections
from django.db.models import DateField

class JournalEditorialBoardMembershipSimilarJournalsForm(MyForm):
    journal = forms.ModelChoiceField(label=u"Журнал", queryset=Journal.objects.all(), empty_label="добавить новый журнал", required=False)


class JournalEditorialBoardMembershipForm(WorkershipModelForm):
    '''A simple form for journal editorial board membership.'''
    journal_str = forms.CharField(label=u"Журнал", max_length=255)
    fields_order = ["member_str", "journal_str", "role", "startdate", "enddate"]
    css_classes = [('journal_str', 'wide autocomplete_journal')]

    class Meta:
        model = JournalEditorialBoardMembership
        exclude = ('member', 'journal', 'creator')

    def __init__(self, *args, **kwargs):
        super(JournalEditorialBoardMembershipForm, self).__init__(*args, **kwargs)
        if self.instance_linked(): # initialize journal_str field
            self.fields['journal_str'].initial = self.instance.journal.name

    def clean_member_str(self):
        # only one member is allowed
        value = self.cleaned_data['member_str'].strip(", ")
        if "," in value:
            raise forms.ValidationError(u"Пожалуйста, укажите ФИО только одного члена редколлегии. В строке не должно быть запятых.")
        return value

    def clean(self):
        # check that enddate is greater than the startdate
        startdate = self.cleaned_data.get("startdate")
        enddate = self.cleaned_data.get("enddate")
        if startdate and enddate and startdate > enddate:
            raise forms.ValidationError(u"Дата вступления в редколлегию должна предшествовать дате выхода.")
        return self.cleaned_data

    def save(self, request, workers, journal, commit=True):
        """
        Save JournalEditorialBoardMembershipForm. Saves journal to database, if needed.
        Sets creator in newly added journal to request.user.
        """
        membership = super(JournalEditorialBoardMembershipForm, self).save(request, commit=False)
        journal_created = False
        # link to journal
        if not journal: # create a new journal
            properties = {
                'name': self.cleaned_data['journal_str'],
                'creator': request.user
            }
            journal = Journal(**properties)
            journal.xml = journal.get_xml()
            journal.save()
            journal_created = True
        membership.journal = journal
        if commit:
            self.commit_save(request, membership, workers)
            if journal_created:
                action.send(request.user, verb=u'добавил журнал', action_object=journal, target=membership)
        return membership

class JournalCardForm(MyModelForm):
    """
    FIXME-greg What is this form for? Why is it named "JournalCardForm"? What is the purpose of it?
    Is it used to create or modify a journal? On what page is it displayed?
    """
    publisher_str = forms.CharField(label=u'Издательство', max_length=255)
    css_classes = [('name', 'autocomplete_journal'), ('publisher_str', 'autocomplete_publisher wide')]

    def __init__(self, *args, **kwargs):
            super(JournalCardForm, self).__init__(*args, **kwargs)
            if "instance" in kwargs:
                instance = kwargs['instance']
                if instance.publisher:
                 self.fields["publisher_str"].initial = instance.publisher.name
                self.old_name = instance.name

    def save(self, request,  publisher, commit=True, creator=None):
        """
        Create Journal instance. Set publisher and creator.
        Save into database, if commit=True.
        """
        journal = super(JournalCardForm, self).save(request, commit=False)
        journal.publisher = publisher
        if creator:
                journal.creator = creator
        if commit:
            self.commit_save(request, journal)
        return journal

    def commit_save(self, request, object): # FIXME-greg Do not name a variable 'object'
        super(JournalCardForm, self).commit_save(request, object)
        if self.edit and hasattr(self, 'old_name'):
            alias, created = JournalAlias.objects.get_or_create(alias=self.old_name, journal=object, creator=request.user)
            alias.save()
            action_str = (u"добавил", u"добавлен" + alias.gender_suffix)
            action.send(request.user, verb=u'%s %s' % (action_str[0], alias.accusative), target=alias)
    class Meta:
        model = Journal
        fields = ('name', 'url', 'abstract')

class SimilarPublisherChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, publisher):
        return "%s (%s)" % (publisher.name, publisher.city)

class JournalCardFormSimilarPublisher(MyForm):
    publisher = SimilarPublisherChoiceField(label=u"Издательство", queryset=Publisher.objects.all(), empty_label="добавить новое издательство", required=False)

JournalAliasFormSet = inlineformset_factory(Journal, JournalAlias, fields=('alias',), extra=1)

ISSNFormSet = inlineformset_factory(Journal, ISSN, fields=('category', 'value'), extra=1)

SourcesMembershipFormset = inlineformset_factory(Journal, JournalSourceMembership, fields=('source',), extra=1)


def set_widgets_to_formset(**kwargs):
    def formfield_callback(model_field):
        if model_field.name in kwargs:
            form_field = model_field.formfield()
            widget = kwargs[model_field.name]
            if isinstance(widget, forms.Widget):
                form_field.widget = widget
            else:
                form_field.widget.attrs['class'] = widget
            return form_field
        else:
            return model_field.formfield()
    return formfield_callback

class RubricMembershipForm(forms.ModelForm):
    rubric_str = forms.CharField(label=u"Рубрика",
                                 widget=forms.TextInput(attrs={'class': 'autocomplete_rubric'}))

    class Meta:
        model = JournalRubricMembership
        fields = ()

    def __init__(self, *args, **kwargs):
        super(RubricMembershipForm, self).__init__(*args, **kwargs)
        try:
            self.fields["rubric_str"].initial = self.instance.rubric.name
        except JournalRubric.DoesNotExist:
            pass

    def clean(self):
        cleaned_data = super(RubricMembershipForm, self).clean()
        rubric_name = cleaned_data["rubric_str"]
        if rubric_name != self.fields['rubric_str'].initial:
            try:
                rubric = JournalRubric.objects.get(name=rubric_name)
                self.instance.rubric = rubric
            except JournalRubric.DoesNotExist:
                raise forms.ValidationError(u"Введите название существуюшего рубрикатора")
        return cleaned_data

RubricMembershipFormset = inlineformset_factory(Journal, JournalRubricMembership, form=RubricMembershipForm, extra=1, )

RanksFormset = inlineformset_factory(Journal, JournalRankValue,  fields=('name', 'year', 'value', 'date'), extra=1,
                                     formfield_callback = set_widgets_to_formset(date='datepicker'))

class ChoiceJournalForm(forms.Form):
    def  __init__(self, *args, **kwargs):
        queryset = kwargs.pop('queryset', Journal.objects.none())
        super(ChoiceJournalForm, self).__init__(*args, **kwargs)
        self.fields['journals'].queryset = queryset

class JournalMergeCandidates(ChoiceJournalForm):
    journals = forms.ModelMultipleChoiceField(label=u'Журналы для объединения', queryset=Journal.objects.none(),
                                                 widget=forms.CheckboxSelectMultiple())
class JournalMergeSelectMain(ChoiceJournalForm):
    def save(self, request, journals=None):
                if self.is_valid():
                    if not journals:
                        journals = self.fields["journals"].queryset
                    main_journal = self.cleaned_data["journals"]
                    for journal in journals:
                        if journal != main_journal:
                            Journal.objects.merge(request.user.username, journal.id, main_journal.id)
                    messages.success(request, u"Журналы успешно объединены.")

class JournalMergeSelectMainFromQueryset(JournalMergeSelectMain):
    journals = forms.ModelChoiceField(label=u'Выберите главный журнал', queryset=Journal.objects.none(), empty_label=u"Нет в списке",
                                      required=False)
    def clean(self):
        cleaned_data = super(JournalMergeSelectMain, self).clean()
        journal = cleaned_data["journals"]
        if journal and self.fields["journals"].queryset.count() == 1:
            raise forms.ValidationError(u"Для слияния необходимо хотя бы два журнала")
        return cleaned_data


class JournalMergeSelectMainFromAll(JournalMergeSelectMain):
    journals = forms.ModelChoiceField(label=u'Выберите главный журнал', queryset=Journal.objects.none())


class JournalChoiceField(Select2ModelField):
    model = Journal
    search_fields = ['name__icontains', 'aliases__alias__icontains']

    def get_title(self, journal):
        return journal.name

    def get_description(self, journal):
        sql_query = """SELECT journal.f_journal_id
        , NVL(issn_res.min_issn, '-')
        , NVL(info_res.in_scopus, 0)
        , NVL(info_res.in_wos, 0)
        , NVL(info_res.in_vak, 0)
        , NVL(info_res.in_top25, 0)
        , NVL(rang_res.f_journalrang_val, -2)
        FROM journal
        LEFT JOIN (SELECT f_journal_id, MIN(f_issn_value) min_issn
                   FROM issn
                   GROUP BY f_journal_id) issn_res
           ON (issn_res.f_journal_id = journal.f_journal_id)
        LEFT JOIN (SELECT f_journal_id, in_scopus, in_wos, in_vak, in_top25
                   FROM v_journals_info) info_res
           ON (info_res.f_journal_id = journal.f_journal_id)
        LEFT JOIN (SELECT f_journal_id, f_journalrang_val
                   FROM journalrang
                   WHERE f_journalrang_type like 'RINC%%') rang_res
           ON (rang_res.f_journal_id = journal.f_journal_id)
        WHERE journal.f_journal_id = %s
        """

        cursor = connections['default'].cursor()
        cursor.execute(sql_query, [journal.id])
        id, issn, in_scopus, in_wos, in_vak, in_top25, rang = cursor.fetchone()
        strings = []
        if issn != '-':
            strings.append(u'ISSN: %s' % issn)
        in_strings = []
        if in_scopus:
            in_strings.append(u'Scopus')
        if in_wos:
            in_strings.append(u'WOS')
        if in_vak:
            in_strings.append(u'списке ВАК')
        if in_top25:
            in_strings.append(u'Top-25%')
        if in_strings:
            strings.append(u'в ' + u', '.join(in_strings))
        if rang > -1:
            strings.append(u'импакт-фактор РИНЦ: %s' % rang)
        return u'; '.join(strings)


class JournalRubricChoiceField(Select2ModelField):
    model = JournalRubric
    search_fields = ['name__icontains', 'code__icontains']

    def __init__(self, *args, **kwargs):
        self.include_code = kwargs.pop("include_code", False)
        return super(JournalRubricChoiceField, self).__init__(*args, **kwargs)

    def get_title(self, rubric):
        title = rubric.name
        if self.include_code:
            title = u"%s " % rubric.code + title
        return title

    def get_description(self, rubric):
        ancestors = rubric.ancestors
        ancestors.reverse()
        description = ""
        for parent in ancestors:
            if self.include_code:
                description += u"%s " % parent.code
            description += u'%s, ' % parent
        return description[:-2]


class JournalRubricMultipleChoiceField(Select2ModelMultipleField, JournalRubricChoiceField):
    pass

