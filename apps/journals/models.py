# -*- coding: utf-8 -*-
from django.db import models
import logging

from collections import namedtuple
from common.models import MyModel, LinkedToWorkersModel, WorkershipModel, Activity
from managers import JournalManager, JournalIndexManager, JournalAliasManager
from unified_permissions import has_permission
from common.utils.models_related import has_related_objects as _has_related_objects

logger = logging.getLogger("journals.models")


class Journal(LinkedToWorkersModel):
    id = models.AutoField(primary_key=True, db_column="F_JOURNAL_ID")
    name = models.CharField(u"Название журнала", max_length=255, db_column="F_JOURNAL_NAME")
    editors = models.ManyToManyField("workers.Worker", related_name="journals_edited", through="JournalEditorialBoardMembership")
    publisher = models.ForeignKey(to="publishers.Publisher", related_name="journals", db_column="F_PUBLISHING_ID", null=True, blank=True)
    url = models.URLField(u"Портал журнала", max_length=255, db_column="F_JOURNAL_URL", null=True, blank=True)
    language = models.ForeignKey(to="publications.Language", related_name="journals", db_column="LANG_ID", null=True, blank=True)
    abstract = models.TextField(u"Аннотация журнала", db_column="F_JOURNAL_ABSTRACT", blank=True)
    xml = models.TextField(u"XML журнала", db_column="F_JOURNAL_XML", null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="journals_added", db_column="F_JOURNAL_USER", null=True, blank=True)
    #F_JOURNAL_RUS = IntegerField Российский

    objects = JournalManager()

    search_attr = 'name'
    workers_attr = 'editors'
    workerships_attr = 'editorial_board_memberships'
    workers_verbose_name_single = u"ФИО члена редколлегии"
    workers_verbose_name_plural = u"ФИО членов редколлегии"
    workers_required_error_msg = u"Укажите хотя бы одного члена редколлегии журнала"

    locative = u"журнале"
    genitive = u"журнала"
    instrumental = u"журналом"
    nominative_en = "journal"

    class Meta:
        db_table = "JOURNAL"
        verbose_name = u"журнал"
        verbose_name_plural = u"журналы"
        unique_together = ("name", "publisher")
        ordering = ('name', 'publisher__name')

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('journals_detail', (), {'object_id': self.id})

    @property
    def aliases_list(self):
        return ", ".join([alias.full for alias in self.aliases.all()])

    @property
    def aliases_list_iterable(self):
        return [alias.full for alias in self.aliases.all()]

    @property
    def authors(self):
        '''Return a list of all distinct workers that are authors of articles in the journal.'''
        authors = []
        for article in self.articles.all():
            authors.extend(article.authors.all())
        return list(set(authors))

    @property
    def auto_added(self):  # FIXME-greg here should definitely be a docstring. What is auto-added?
        # FIXME-greg Would it be simplier?
        # return not bool(self.creator)
        # (and the same comment for the next 2 methods)
        creator = self.creator
        return True if not creator else False

    @property
    def checked(self):  # FIXME-greg Meaningless name 'checked'. No explanation again. The name should be changed, docstring added.
        # FIXME-greg check that journal is 'good' (validated) through actstream (#1121)
        # if an editor has modified the journal, then it is validated
        creator = self.creator
        return False if not creator else has_permission(creator, "edit_journals")

    @property
    def not_checked(self):  # FIXME-greg Meaningless name again. No explanation. The name should be changed, docstring added.
        # However, it is a good idea to add a separate method to check this information.
        return True if not (self.auto_added or self.checked) else False

    @property
    def can_be_merged(self):
        """
        This means that this journal must have no rewarded articles,
        issns, rank_values, rubric_memberships, indexes etc.
        And the journal must have a creator (this means that it hasn't been added automatically via script)
        """
        has_related_objects = any(
            (
                self.issns.exists(),
                self.index_memberships.exists(),
                self.rubric_memberships.exists(),
                self.rank_values.exists(),
                self.source_memberships.exists(),
                self.rewarded_articles.exists(),
            )
        )
        has_creator = bool(self.creator)
        return (not has_related_objects) and has_creator

    @property
    def is_vak(self):
        vak_indexes = JournalIndex.objects.get_vak_indexes()
        return self.index_memberships.filter(index__in=vak_indexes).exists()

    @property
    def is_wos_or_scopus(self):
        vak_indexes = JournalIndex.objects.get_wos_or_scopus_indexes()
        return self.index_memberships.filter(index__in=vak_indexes).exists()


    @property
    def title(self):
        return self.name

    @property
    def impact_factor(self):
        return next(self.get_latest_impact_factors(), None)

    def get_impact_factors(self, name=None, latest_only=False):
        known_factors = ['Impact Factor', 'SJR', 'RINC', 'Science Index']
        ImpactFactorData = namedtuple('ImpactFactor', 'type, year, value')
        for name in ([name] if name else known_factors):
            impacts_for_name = self.rank_values.filter(name=name, year__isnull=False).order_by('-year')
            for row in impacts_for_name:
                yield ImpactFactorData(name, row.year, row.value)
                if latest_only:
                    break
        raise StopIteration

    def get_latest_impact_factors(self):
        return self.get_impact_factors(latest_only=True)

    @property
    def has_related_objects(self):
        return _has_related_objects(self,exceptions=['aliases'])

class JournalAliasCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_ALIAS_ID")
    name = models.CharField(u"Название типа", max_length=255, unique=True, db_column="F_ALIAS_NAME")

    class Meta:
        db_table = "ALIAS"
        verbose_name = u"Тип псевдонима"
        verbose_name_plural = u"Типы псевдонимов"
        ordering = ('name',)

    def __unicode__(self):
        return "'%s'" % self.name


class JournalAlias(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_JALIAS_ID")
    journal = models.ForeignKey(to="Journal", related_name="aliases", verbose_name=u"журнал", db_column="F_JOURNAL_ID")
    alias = models.CharField(u"Псевдоним", max_length=255, db_column="F_JALIAS_NAME")
    category = models.ForeignKey(to="JournalAliasCategory", related_name="aliases", verbose_name=u"тип псевдонима", db_column="F_ALIAS_ID", null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="journal_aliases_added", db_column="F_JALIAS_USER", null=True, blank=True)

    objects = JournalAliasManager()

    class Meta:
        db_table = "JALIAS"
        verbose_name = u"Псевдоним журнала"
        verbose_name_plural = u"Псевдонимы журналов"
        unique_together = ("journal", "alias", "category")
        ordering = ('journal__name', 'alias')

    def __unicode__(self):
        return "'%s', alias of %s" % (self.alias, unicode(self.journal))

    @property
    def full(self):
        s = self.alias
        if self.category:
            s += ' (' + self.category.name + ')'
        return s


class JournalEditorialBoardMembership(WorkershipModel, Activity):
    id = models.AutoField(primary_key=True, db_column="F_REDJOURNAL_ID")
    member = models.ForeignKey(to='workers.Worker', related_name="journal_editorial_board_memberships", db_column="F_MAN_ID", null=True, blank=True)
    journal = models.ForeignKey(to='Journal', related_name="editorial_board_memberships", db_column="F_JOURNAL_ID")
    role = models.CharField(u"Тип участия", max_length=255, choices=(
        ("member", u"член редколлегии"),
        ("chief", u"главный редактор"),
        ("reviewer", u"рецензент")
    ), default="member", db_column="F_REDJOURNAL_ROLE")
    startdate = models.DateField(u"Дата вступления", db_column="F_REDJOURNAL_BEGIN")
    enddate = models.DateField(u"Дата выхода", db_column="F_REDJOURNAL_END", null=True, blank=True)
    original_name = models.CharField(max_length=255, db_column="F_REDJOURNAL_NAME", blank=True)
    creator = models.ForeignKey(to='auth.User', related_name="journal_editorial_board_memberships_added", db_column="F_REDJOURNAL_USER", null=True, blank=True)

    worker_attr = "member"
    workers_verbose_name_single = u"ФИО члена редколлегии"
    workers_required_error_msg = u"Укажите члена редколлегии журнала"

    nominative_en = "journal_editorial_board_membership"
    nominative_short = u"членство в редколлегии"
    nominative_plural = u"членство в редколлегиях журналов"
    genitive = u"членства в редколлегии журнала"
    genitive_short = u"членства в редколлегии"
    genitive_plural = u"членства в редколлегиях журналов"
    genitive_plural_full = u"членств в редколлегиях журналов"
    accusative_short = u"членство в редколлегии"
    instrumental = u"членством в редколлегии журнала"
    locative = u"членстве в редколлегии журнала"
    gender = "neuter"

    class Meta:
        db_table = "REDJOURNAL"
        get_latest_by = "-startdate"
        ordering = ('-startdate', '-enddate')
        verbose_name = u"членство в редколлегии журнала"
        verbose_name_plural = u"членства в редколлегиях журналов"

    def __unicode__(self):
        return u"Членство в редколлегии журнала %s, %s" % (unicode(self.journal), self.workers_string)

    @models.permalink
    def get_absolute_url(self):
        return ('journals_membership_detail', (), {'object_id': self.id})

    @property
    def title(self):
        return u"%s в редколлегии журнала %s" % (self.workers_string, self.journal)

    @property
    def show_role_in_snippet(self):
        return self.role == "chief"


class ISSNCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_ISSNTYPE_ID')
    name = models.CharField(u"Название", max_length=1020, db_column='F_ISSNTYPE_NAME')

    class Meta:
        db_table = u'ISSNTYPE'
        verbose_name = u" Категория ISSN"
        verbose_name_plural = u"Категории ISSN"

    def __unicode__(self):
        return u"%s" % self.name


class ISSN(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_ISSN_ID')
    journal = models.ForeignKey(to='Journal', related_name="issns", verbose_name=u"Журнал", db_column='F_JOURNAL_ID', on_delete=models.PROTECT)
    category = models.ForeignKey(to='ISSNCategory', related_name="issns", null=True, blank=True, verbose_name=u"Категория", db_column='F_ISSNTYPE_ID', on_delete=models.PROTECT)
    value = models.CharField(u"Номер", max_length=1020, unique=True, db_column='F_ISSN_VALUE')

    class Meta:
        db_table = u'ISSN'
        verbose_name = u"ISSN"
        verbose_name_plural = u"ISSN"

    def __unicode__(self):
        return u"%s, %s, '%s'" % (self.value, self.category, self.journal)


class JournalIndex(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_JOURNALVAKTYPE_ID')
    name = models.CharField(u'Название', max_length=1020, db_column='F_JOURNALVAKTYPE_NAME')
    journals = models.ManyToManyField(to='Journal', through="JournalIndexMembership", related_name="indexes", verbose_name=u"Журналы")

    objects = JournalIndexManager()

    class Meta:
        db_table = u'JOURNALVAKTYPE'
        verbose_name = u"Список журналов"
        verbose_name_plural = u"Списки журналов"

    def __unicode__(self):
        return u"%s" % self.name

    @models.permalink
    def get_absolute_url(self):
        return ('journals_index_detail', (), {'object_id': self.id})


class JournalIndexMembership(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_JORNALVAK_ID')
    journal = models.ForeignKey(to='Journal', related_name="index_memberships", verbose_name=u"Журнал", db_column='F_JOURNAL_ID', on_delete=models.PROTECT)
    index = models.ForeignKey(to='JournalIndex', related_name="memberships", verbose_name=u"Список", db_column='F_JOURNALVAKTYPE_ID', on_delete=models.PROTECT)
    startdate = models.DateField(u"Дата включения", db_column="F_JORNALVAK_BEGIN")
    enddate = models.DateField(u"Дата исключения", db_column="F_JORNALVAK_END", null=True, blank=True)

    class Meta:
        db_table = u'JORNALVAK'
        verbose_name = u"Список журналов"
        verbose_name_plural = u"Списки журналов"

    def __unicode__(self):
        return u"%s, '%s'" % (self.index, self.journal)


class JournalCategorization(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_JOURNALRUBTYPE_ID')
    name = models.CharField(u'Название', max_length=1020, unique=True, db_column='F_JOURNALRUBTYPE_NAME')
    code = models.CharField(u'Код классификатора', max_length=100, db_column='F_JOURNALRUBTYPE_CODE')

    class Meta:
        db_table = u'JOURNALRUBTYPE'
        verbose_name = u"Классификатор журналов"
        verbose_name_plural = u"Классификаторы журналов"

    def __unicode__(self):
        return u"%s" % self.name


class JournalRubric(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_JOURNALRUB_ID')
    categorization = models.ForeignKey(to='JournalCategorization', related_name='rubrics', verbose_name=u"Классификатор", db_column='F_JOURNALRUBTYPE_ID', on_delete=models.PROTECT)
    parent = models.ForeignKey('self', null=True, blank=True, verbose_name=u"Родительская рубрика", db_column='JOU_F_JOURNALRUB_ID', on_delete=models.PROTECT)
    name = models.CharField(u"Название", max_length=1000, blank=True, db_column='F_JOURNALRUB_NAME')
    actual = models.CharField(u'Актуальность', max_length=1, blank=True, db_column='F_JOURNALRUB_ACTUAL')
    code = models.CharField(u'Код', max_length=255, blank=True, db_column='F_JOURNALRUB_CODE')
    description = models.CharField(u'Описание', max_length=4000, blank=True, db_column='F_JOURNALRUB_DESCRIPTION')
    url = models.URLField(u'URL', max_length=2000, blank=True, db_column='F_JOURNALRUB_URL')

    search_attr = 'name'

    class Meta:
        db_table = u'JOURNALRUB'
        verbose_name = u"Рубрика журнала"
        verbose_name_plural = u"Рубрики журналов"

    def __unicode__(self):
        return self.name

    @property
    def ancestors(self):
        ancestors = []
        if self.parent:
            ancestors.append(self.parent)
            ancestors.extend(self.parent.ancestors)
        return ancestors


class JournalRubricMembership(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_JOURNALRUBLINK_ID')
    rubric = models.ForeignKey(to='JournalRubric', related_name='memberships', verbose_name=u'Рубрика', db_column='F_JOURNALRUB_ID', on_delete=models.PROTECT)
    journal = models.ForeignKey(to='Journal', related_name="rubric_memberships", verbose_name=u"Журнал", db_column='F_JOURNAL_ID', on_delete=models.PROTECT)
    value = models.DecimalField(u'Значение', max_digits=14, decimal_places=8, null=True, blank=True, db_column='F_JOURNALRUBLINK_VAL')

    class Meta:
        db_table = u'JOURNALRUBLINK'
        verbose_name = u"Принадлежность журнала рубрике"
        verbose_name_plural = u"Принадлежности журналов рубрикам"

    def __unicode__(self):
        return u"%s журнала %s" % (self.rubric, self.journal)


class JournalRankValue(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_JOURNALRANG_ID')
    journal = models.ForeignKey(to='Journal', related_name="rank_values", verbose_name=u"Журнал", db_column='F_JOURNAL_ID', on_delete=models.PROTECT)
    name = models.CharField(u'Название', max_length=1020, db_column='F_JOURNALRANG_TYPE')  # TODO make this a ForeignKey to a separate model
    value = models.DecimalField(u'Значение', null=True, max_digits=14, decimal_places=8, blank=True, db_column='F_JOURNALRANG_VAL')
    date = models.DateField(u'Дата вычисления', db_column='F_JOURNALRANG_DATE')
    year = models.IntegerField(u'Год', db_column='F_JOURNALRANG_YEAR', null=True, blank=True)

    class Meta:
        db_table = u'JOURNALRANG'
        verbose_name = u'Ранг журнала'
        verbose_name_plural = u'Ранги журналов'

    def __unicode__(self):
        return u"'%s', %s, %s - %s" % (self.journal, self.name, self.date, self.value)


class JournalSource(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_JOURNSOURCE_ID')
    name = models.CharField(u'Название', max_length=1020, db_column='F_JOURNSOURCE_NAME')

    class Meta:
        db_table = u'JOURNSOURCE'
        verbose_name = u'Источник журнала'
        verbose_name_plural = u'Источники журналов'

    def __unicode__(self):
        return self.name


class JournalSourceMembership(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_JOURNSOURCELINK_ID')
    journal = models.ForeignKey(to='Journal', related_name="source_memberships", verbose_name=u"Журнал", db_column='F_JOURNAL_ID', on_delete=models.PROTECT)
    source = models.ForeignKey(to='JournalSource', related_name="memberships", verbose_name=u"Источник", db_column='F_JOURNSOURCE_ID', on_delete=models.PROTECT)
    external_id = models.CharField(max_length=1020, blank=True, db_column='F_JOURNSOURCELINK_FOREIGNKEY')
    created = models.DateField(u'Дата создания', db_column='F_JOURNSOURCELINK_CREATE', auto_now_add=True)

    class Meta:
        db_table = u'JOURNSOURCELINK'
        verbose_name = u'Принадлежность журнала источнику'
        verbose_name_plural = u'Принадлежности журналов источникам'

    def __unicode__(self):
        return u"'%s' из '%s'" % (self.journal, self.source)

from journals.admin import *
