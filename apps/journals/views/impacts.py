# -*- coding: utf-8; -*-

#
# Views related to impact factors processing
#

from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from common.utils.context import render_to_response
from django.db.models import Q
from django.db import connection
from collections import namedtuple

from unified_permissions.decorators import permission_abstract_required
from journals.models import Journal


@login_required
def journal_impacts(request, object_id):
    user = request.user
    journal = get_object_or_404(Journal, pk=object_id)
    if not user:
        return redirect('home')
    return render_to_response(request, 'journals/journal_impacts.html', {'journal': journal})


@permission_abstract_required("edit_journals")
def journal_impacts_search(request, object_id):
    journal = get_object_or_404(Journal, pk=object_id)

    cursor = connection.cursor()
    
    if request.method == 'POST':
        for jrangraw_id in request.POST.getlist('rawid', []):
            sql = u"""
            INSERT INTO journalrang (f_journal_id, f_journalrang_type, f_journalrang_year, f_journalrang_val, f_journalrang_date)
            (SELECT %s, f_jrangraw_name, f_jrangraw_year, f_jrangraw_value, sysdate
             FROM jrangraw
             WHERE f_jrangraw_id = %s)
            """
            cursor.execute(sql, [journal.id, int(jrangraw_id)])
            
    sql = u"""
    SELECT f_jrangraw_journal_name, f_jrangraw_name, f_jrangraw_year, f_jrangraw_value, f_jrangraw_id
           , registered.f_journalrang_id
           , decode(registered.f_journalrang_val - f_jrangraw_value, 0, NULL, registered.f_journalrang_val) unmatched_value
    FROM jrangraw
         LEFT JOIN (SELECT max(f_journalrang_id) f_journalrang_id, f_journalrang_type, f_journalrang_year, f_journalrang_val
                    FROM journalrang
                    WHERE journalrang.f_journal_id = %(f_journal_id)s
                    GROUP BY f_journalrang_type, f_journalrang_year, f_journalrang_val
             ) registered ON ( registered.f_journalrang_type = f_jrangraw_name
                               AND (registered.f_journalrang_year = f_jrangraw_year
                                    OR (registered.f_journalrang_year IS NULL AND f_jrangraw_year IS NULL)))
    WHERE (
      EXISTS (SELECT 1 FROM issn
              WHERE f_journal_id = %(f_journal_id)s
                AND f_issn_value IN (f_jrangraw_issn, f_jrangraw_issn2, f_jrangraw_issn3))
      OR
      upper(f_jrangraw_journal_name) IN (SELECT upper(f_jalias_name) FROM jalias WHERE f_journal_id = %(f_journal_id)s)
      OR
      upper(f_jrangraw_journal_name) IN (SELECT upper(f_journal_name) FROM journal WHERE f_journal_id = %(f_journal_id)s)
      )
      AND f_jrangraw_value >= 0
    ORDER BY 6 NULLS FIRST, f_jrangraw_name, f_jrangraw_year DESC
    """ % {'f_journal_id': journal.id}
    cursor.execute(sql)
    JournalImpactTuple = namedtuple('JournalImpactTuple', 'jname, iftype, year, value, jrangraw_id, journalrang_id, unmatched_value')
    data = [ JournalImpactTuple(*item) for item in cursor ]
    return render_to_response(request, 'journals/journal_impacts_search.html', {'journal': journal, 'data': data})

