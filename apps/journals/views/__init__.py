# -*- coding: utf-8; -*-
from django.utils.decorators import classonlymethod
import logging

from django.views.generic.list_detail import object_detail
from django.contrib.auth.decorators import login_required

from actstream import action
from django.contrib import messages

from django.db.models import Q

from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator
from django.utils import simplejson
from django.contrib.formtools.wizard.views import SessionWizardView
from django.views.generic.base import RedirectView
from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from common.views import LinkedToWorkersModelWizardView, MyModelWizardView
from journals.forms import JournalEditorialBoardMembershipForm, JournalEditorialBoardMembershipSimilarJournalsForm, \
    JournalCardForm, JournalCardFormSimilarPublisher, JournalMergeSelectMainFromQueryset, JournalMergeSelectMainFromAll, \
    ISSNFormSet, RanksFormset, SourcesMembershipFormset, RubricMembershipFormset
from journals.models import Journal
from publishers.models import Publisher
from publishers.forms import PublisherModelForm
from journals.models import JournalEditorialBoardMembership, JournalIndex, JournalIndexMembership, ISSN
from journals.forms import JournalAliasFormSet, JournalMergeCandidates
from journals.utils import get_journal_json
from workers.forms import WorkerFormSet
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from common.utils.context import render_to_response
from datetime import datetime
from unified_permissions import has_permission
from unified_permissions.decorators import permission_abstract_required

logger = logging.getLogger("journals.views")


class JournalEditorialBoardMembershipWizardView(LinkedToWorkersModelWizardView):
    form_list = [
        (LinkedToWorkersModelWizardView.main_step, JournalEditorialBoardMembershipForm),
        ('similar_journals', JournalEditorialBoardMembershipSimilarJournalsForm),
        (LinkedToWorkersModelWizardView.workers_steps[0], WorkerFormSet)
    ]
    condition_dict = LinkedToWorkersModelWizardView.make_condition_dict(
        'similar_journals')
    model = JournalEditorialBoardMembership  # used in templates

    def save_object(self, form_list):
        '''This function is executed during done() method, and used to save main form object.
        Workers objects has been already saved.
        '''
        journal = self.get_extra_data('journal_instance',
                                      Journal) # this is a journal found by name or a selected similar journal
        return super(JournalEditorialBoardMembershipWizardView, self).save_object(form_list, journal)

    def process_step(self, form):
        journal = None
        if self.steps.current == self.main_step:
            journal_str = form.cleaned_data['journal_str']
            old_journal_str = self.get_extra_data('journal_str')
            if journal_str != old_journal_str: # journal_str has changed
                self.set_extra_data('journal_str', journal_str) # for later use in creating similar journals form
                self.delete_extra_data('similar_journals') # now they should be calculated again
                try:
                    # try to select journal by name
                    journal = Journal.objects.get(name=journal_str)
                except:
                    # calculate similar journals
                    similar_journals = Journal.objects.get_similar(journal_str, return_queryset=True)
                    # store similar journals not to calculate them twice (on step 2 and on step 3).
                    self.set_extra_data('similar_journals', similar_journals)
                    self.skip_or_include_steps('similar_journals', not similar_journals)
                else: # journal selected by name
                    self.skip_steps('similar_journals')
        elif self.steps.current == 'similar_journals':
            journal = form.cleaned_data['journal']
        if isinstance(journal, Journal): # journal found by name or similar journal selected
            self.set_extra_data('journal_instance', journal)
        return super(JournalEditorialBoardMembershipWizardView, self).process_step(form)

    def get_form(self, step=None, data=None, files=None):
        form = super(JournalEditorialBoardMembershipWizardView, self).get_form(step, data, files)
        if not step:
            step = self.steps.current
        if step == 'similar_journals':
            # the real step can be 3, but this function can be called to render previous forms
            # get the similar journals basing on journal_str entered at 'main' step.
            # if this is really step 2, and it's not skipped, similar journals should be  calculated (again)
            similar_journals, journal_str = self.get_extra_data('similar_journals', 'journal_str')
            if not similar_journals:
                if self.steps.current == step: # this is really step 2
                    similar_journals = Journal.objects.get_similar(journal_str, return_queryset=True)
                    # store similar journals not to calculate them twice (on step 2 and on step 3).
                    self.set_extra_data('similar_journals', similar_journals)
                else: # this is just a previous form for step 3
                    similar_journals = Journal.objects.none()
            form.fields['journal'].queryset = similar_journals
            form.fields['journal'].empty_label = journal_str + u" (добавить новый журнал)"
        return form

    def get_context_data(self, form, **kwargs):
        context = super(JournalEditorialBoardMembershipWizardView, self).get_context_data(form, **kwargs)
        forms = context['previous_forms']
        self.hide_form_fields(forms, 'main', 'journal_str', 'similar_journals')
        if 'similar_journals' in self.steps.all:
            self.hide_form_fields(forms, 'main', 'journal_str', self.workers_steps)
        return context


@permission_abstract_required("edit_journals")
def journal_card(request, journal_id, formsets=None):
    journal = get_object_or_404(Journal, pk=journal_id)
    vak_type_list = JournalIndex.objects.all()
    journal_vak_types = [vak.index for vak in journal.index_memberships.all()]
    if request.method == 'POST':
        formsets_dict = formsets
    else:
        formsets_dict = {'journal_aliases_formset': JournalAliasFormSet(instance=journal),
                         'issn_formset': ISSNFormSet(instance=journal), 'ranks_formset': RanksFormset(instance=journal),
                         'sources_formset': SourcesMembershipFormset(instance=journal),
                         'rubric_formset': RubricMembershipFormset(instance=journal)}
    extra_context = {'vak_type_list': vak_type_list,
                     'journal_vak_types': journal_vak_types}
    extra_context.update(formsets_dict)
    return object_detail(request, queryset=Journal.objects.all(), template_object_name='journal',
                         extra_context=extra_context,
                         template_name='journals/journal_card.html', object_id=journal_id)


@permission_abstract_required("edit_journals")
def journal_card_save(request, journal_id):
    journal = get_object_or_404(Journal, pk=journal_id)

    def change_indexes(request, journal):
        indexes_ids = []
        for key, value in request.POST.iteritems():
            if key.startswith('index_'):
                index_id = int(key.split('index_')[1])
                if value == 'true':
                    indexes_ids.append(index_id)
        indexes_to_delete = journal.index_memberships.exclude(index__pk__in=indexes_ids)
        for index in indexes_to_delete:
            description = u'journal index: %s deleted' % index
            action.send(request.user, verb=u"удалил индекс журнала", action_object=index, target=journal,
                        description=description)
        indexes_to_delete.delete()
        remained_indexes_ids = journal.index_memberships.all().values_list('index__id', flat=True)
        for index_id in indexes_ids:
            if index_id not in remained_indexes_ids:
                journal_membership = JournalIndexMembership.objects.create(journal=journal, index_id=index_id,
                                                                           startdate=datetime.today())
                description = u'journal index: %s created' % journal_membership.index
                action.send(request.user, verb=u"добавил индекс журнала", action_object=journal_membership.index,
                            target=journal,
                            description=description)

    def validate_formset(formset):
        if formset.is_valid():
            model = formset.model
            model_name = model.__name__
            verbose_name = model._meta.verbose_name
            parent_model = formset.instance.__class__
            parent_model_name = parent_model.__name__
            logger.debug("%s for %s formset data: %s", model_name, parent_model_name, getattr(formset, "cleaned_data",
                                                                                              formset.data))

            for form in formset.deleted_forms:
                try:
                    related_object = form.cleaned_data['id']
                except:
                    related_object = None
                if isinstance(related_object, model):
                    description = u'%s: %s ' % (model_name, related_object.get_xml().decode("utf-8"),)
                    action.send(request.user, verb=u"удалил %s" % verbose_name, action_object=related_object,
                                target=formset.instance, description=description)
            remained_related_objects = formset.save(commit=False)
            for related_object in remained_related_objects:
                if related_object.id: # this is edited instance, not a created one
                    verb = u"отредактировал %s" % verbose_name
                    log_verb = u"edited"
                    old_related_object = model.objects.get(id=related_object.id)
                    description = u'%s: %s -> %s: %s. Old xml: %s ' % (
                        model_name, old_related_object, model_name, related_object,
                        old_related_object.get_xml().decode("utf-8"))

                else:
                    verb = u"добавил %s" % verbose_name
                    log_verb = u"added"
                    description = u'%s: %s' % (model_name, related_object )
                    related_object.creator = request.user

                for field in model._meta.fields:
                    if getattr(field.rel, 'to', None) is parent_model:
                        field_name = field.name
                        break

                setattr(related_object, field_name, formset.instance)
                related_object.validated = True
                related_object.save()
                action.send(request.user, verb=verb, action_object=related_object, target=formset.instance,
                            description=description)
                logger.debug("%s %s: %s", model_name, log_verb, related_object)
        return formset

    if request.method == "POST":
        logger.debug("Got POST request: %s", request.POST)
        change_indexes(request, journal)
        formsets_to_validate = [JournalAliasFormSet, ISSNFormSet, RanksFormset, SourcesMembershipFormset,
                                RubricMembershipFormset]
        formsets_names = ('journal_aliases_formset',
                          'issn_formset',
                          'ranks_formset',
                          'sources_formset',
                          'rubric_formset')
        formsets = {}
        for name, formset in zip(formsets_names, formsets_to_validate):
            validated_formset = validate_formset(formset(request.POST, instance=journal))
            if validated_formset.is_valid():
                formsets[name] = formset(instance=journal)
            else:
                formsets[name] = validated_formset
    else:
        formsets = None
    return journal_card(request, journal_id=journal_id, formsets=formsets)


class JournalCardWizard(MyModelWizardView):
    '''For comments see courses.views.CourseTeachingWizardView, this wizard is almost the same'''
    # FIXME-greg Where is this wizard used and what for? What form steps does it contain in the general case? Add this to docstring.
    # FIXME-greg Can a new journal be added through this wizard?
    form_list = [
        (MyModelWizardView.main_step, JournalCardForm),
        ('similar_publishers', JournalCardFormSimilarPublisher),
        ('new_publisher', PublisherModelForm),
    ]
    condition_dict = MyModelWizardView.make_condition_dict(
        'similar_publishers', 'new_publisher')
    model = Journal

    def has_edit_permission(self, user, instance):
        return True # because we've already checked it in dispatch decorator

    @permission_abstract_required("edit_journals")
    def dispatch(self, request, *args, **kwargs):
        return super(JournalCardWizard, self).dispatch(request, *args, **kwargs)

    def get_form_initial(self, step):
        initial = super(JournalCardWizard, self).get_form_initial(step)
        if step == 'new_publisher':
            initial['name'] = self.get_extra_data('publisher_str')
        return initial

    def get_form(self, step=None, data=None, files=None):
        form = super(JournalCardWizard, self).get_form(step, data, files)
        if not step:
            step = self.steps.current
        if step == 'similar_publishers':
            similar_publishers, publisher_str = self.get_extra_data('similar_publishers', 'publisher_str')
            if not similar_publishers:
                if self.steps.current == step: # this is really step 2
                    similar_publishers = Publisher.objects.get_similar(publisher_str)
                    # store similar courses not to calculate them twice (on step 2 and on step 3).
                    self.set_extra_data('similar_publishers', similar_publishers)
                else: # this is just a previous form for step 5
                    similar_publishers = Publisher.objects.none()
            form.fields['publisher'].queryset = similar_publishers
            form.fields['publisher'].empty_label = publisher_str + u" (добавить новое издательство)"
        return form

    def get_context_data(self, form, **kwargs):
        context = super(JournalCardWizard, self).get_context_data(form, **kwargs)
        forms = context['previous_forms']
        self.hide_form_fields(forms, 'main', 'publisher_str', ('new_publisher', 'similar_publishers'))
        self.hide_form_fields(forms, 'similar_publishers', 'publisher', 'new_publisher')
        return context

    def process_step(self, form):
        publisher = None
        if self.steps.current == self.main_step:
            publisher_str = form.cleaned_data['publisher_str']
            old_publisher_str = self.get_extra_data('publisher_str')
            if publisher_str != old_publisher_str:
                self.set_extra_data('publisher_str', publisher_str)
                self.delete_extra_data('similar_publishers')
                try:
                    publisher = Publisher.objects.get(name=publisher_str)
                except:
                    similar_publishers = Publisher.objects.get_similar(publisher_str, return_queryset=True)
                    self.set_extra_data('similar_publishers', similar_publishers)
                    self.skip_or_include_steps('similar_publishers', not similar_publishers)
                    if not similar_publishers:
                        self.include_steps('new_publisher')
                else: # course selected by name, go to the last step
                    self.skip_steps('similar_publishers', 'new_publisher')
        elif self.steps.current == 'similar_publishers':
            publisher = form.cleaned_data['publisher']
            self.skip_or_include_steps('new_publisher', isinstance(publisher, Publisher))
        if isinstance(publisher, Publisher): # course found by name or similar course selected
            self.set_extra_data('publisher_instance', publisher)
        return super(JournalCardWizard, self).process_step(form)

    def save_object(self, form_list):
        if 'new_publisher' in self.steps.all:
            new_publisher_form = form_list[self.get_step_index('new_publisher')]
            publisher = new_publisher_form.save(self.request)
        else:
            publisher = self.get_extra_data('publisher_instance', Publisher)
            # editor = request.user does not replace here creator anymore, because it is misuse of creator attribute (any questions to goldan)
        return form_list[0].save(self.request, publisher=publisher)

    def done(self, form_list, **kwargs):
        journal = self.save_object(form_list)
        return redirect('journals_card', journal_id=journal.id)


def journal_detail(request, object_id):
    user = request.user
    if has_permission(user, "edit_journals"):
        return redirect('journals_card', journal_id=object_id)
    return object_detail(request, queryset=Journal.objects.all(), template_object_name='journal', object_id=object_id)


@permission_abstract_required("edit_journals")
def journals_search(request):
    max_journals_limit = 100
    if request.method == "POST":
        search_string = request.POST.get("journal_name", "")
    else:
        search_string = request.GET.get("s", "")
    journals_list = []
    search_string_too_short = False
    search_string = search_string.strip()
    if search_string:
        journals_list = Journal.objects.filter(name__icontains=search_string).extra(order_by=['name'])
        journals_list = list(journals_list.prefetch_related("articles")[:max_journals_limit])
        empty_journals_list = []
        not_empty_journals_list = []
        for journal in journals_list:
            if journal.articles.exists():
                not_empty_journals_list.append(journal)
            else:
                empty_journals_list.append(journal)
        journals_list = not_empty_journals_list + empty_journals_list
    else:
        search_string_too_short = True
    return render_to_response(
        request, "journals/journal_search.html",
        {
            'journals_list': journals_list,
            'search_string': search_string,
            'search_string_too_short': search_string_too_short,
        })


class JournalMergeView(RedirectView):
    url = reverse_lazy("journals_management")

    @permission_abstract_required("edit_journals")
    def dispatch(self, request, *args, **kwargs):
        return super(JournalMergeView, self).dispatch(request, *args, **kwargs)

    def post(self, *args, **kwargs):
        POST = self.request.POST
        if "submit" in POST:
            main_id = POST["main_candidate"]
            candidate_ids = []
            for key, value in POST.items():
                if key.startswith("candidate_") and value != main_id:
                    candidate_ids.append(value)
            candidate_ids = candidate_ids

            try:
                Journal.objects.get(pk=main_id)
            except Journal.DoesNotExist:
                logger.debug(u"Tried to merge not existing journals:  %s into %s",
                             (u",".join(str(id) for id in candidate_ids), main_id))
                messages.error(self.request, u"Журнала, в который выполняется объединение, не существует.")
                return super(JournalMergeView, self).post(self, *args, **kwargs)

            journals_from = Journal.objects.filter(pk__in=candidate_ids)
            if not journals_from:
                logger.debug(u"Tried to merge not existing journals:  %s into %s",
                             (u",".join(str(id) for id in candidate_ids), main_id))
                messages.error(self.request, u"Ни одного журнала из тех, что подлежат объединению, не существует.")
                return super(JournalMergeView, self).post(self, *args, **kwargs)
            elif journals_from.count() < len(candidate_ids):
                # journals with some ids do not exists, inform user and proceed with existing journals
                missing_ids = [pk for pk in candidate_ids if pk not in journals_from.values_list('id', flat=True)]
                messages.warning(self.request,
                                 u"Журналов с id %s не существует." % ", ".join(str(pk) for pk in missing_ids))

            journals_to_merge = []
            for journal in journals_from:
                if journal.can_be_merged:
                    journals_to_merge.append(journal)
                else:
                    messages.info(self.request, u"Журнал %s не может быть объединен в другой." % unicode(journal))
            if not journals_to_merge:
                messages.error(self.request,
                               u"Ни один журнал из тех, что подлежат объединению, не может быть объединен в другой журнал.")
                return super(JournalMergeView, self).post(self, *args, **kwargs)

            for journal in journals_to_merge:
                if journal.id != main_id:
                    Journal.objects.merge(self.request.user.username, journal.id, main_id)
                # editor = self.request.user does not replace here main_journal.creator anymore, because it is misuse of creator attribute (any questions to goldan)
            messages.success(self.request, u"Журналы успешно объединены.")
        return super(JournalMergeView, self).post(self, *args, **kwargs)


class JournalMergeConfirmView(TemplateView):
    template_name = "journals/journal_merge_confirm.html"

    @permission_abstract_required("edit_journals")
    def dispatch(self, request, *args, **kwargs):
        return super(JournalMergeConfirmView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(JournalMergeConfirmView, self).get_context_data(**kwargs)
        context['candidate_ids'] = self.candidate_ids
        context['main_id'] = self.main_id
        return context

    def get(self, *args, **kwargs):
        return redirect("journals_management")

    def post(self, *args, **kwargs):
        POST = self.request.POST
        candidate_ids = []
        for key, value in POST.items():
            if key.startswith("candidate_"):
                candidate_ids.append(value)
        self.candidate_ids = candidate_ids
        self.main_id = POST["main_candidate"]
        return super(JournalMergeConfirmView, self).get(self, *args, **kwargs)


class JournalMergeWizard(SessionWizardView):
    form_list = [
        ("select_candidates", JournalMergeCandidates),
        ("select_main_candidate", JournalMergeSelectMainFromQueryset),
        ("select_main_candidate_all", JournalMergeSelectMainFromAll),
    ]
    condition_dict = {'select_main_candidate_all': False}

    @classonlymethod
    def as_view(cls):
        return super(JournalMergeWizard, cls).as_view(
            form_list = cls.form_list,
            condition_dict = cls.condition_dict)

    @permission_abstract_required("edit_journals")
    def dispatch(self, request, *args, **kwargs):
        response = super(JournalMergeWizard, self).dispatch(request, *args, **kwargs)
        if not self.get_extra_data("queryset"):
            return redirect("journals_search")
        return response

    def post(self, *args, **kwargs):
        if "merge" in self.request.POST:
            ids = []
            for key in self.request.POST.keys():
                if key.isdigit():
                    ids.append(key)
            self.storage.reset()
            self.set_extra_data("queryset", Journal.objects.filter(id__in=ids))
            return self.get(reset_storage=False, *args, **kwargs)
        else:
            return super(JournalMergeWizard, self).post(self, *args, **kwargs)

    def get(self, request, reset_storage=True, *args, **kwargs):
        if reset_storage:
            self.storage.reset()
        self.storage.current_step = self.steps.first
        return self.render(self.get_form())

    def get_template_names(self):
        return 'journals/journal_merge_wizard.html'

    def get_form_initial(self, step):
        initial = super(JournalMergeWizard, self).get_form_initial(step)
        if step == 'select_candidates':
            initial['journals'] = self.get_extra_data("queryset")
        return initial

    def get_form_kwargs(self, step=None):
        data = {}
        if step == 'select_candidates':
            data['queryset'] = self.get_extra_data("queryset")
        if step == 'select_main_candidate':
            select_candidates_data = self.storage.get_step_data('select_candidates')
            ids = select_candidates_data.getlist('select_candidates-journals')
            data['queryset'] = Journal.objects.filter(id__in=ids)
        if step == 'select_main_candidate_all':
            data['queryset'] = Journal.objects.all()
        return data

    def set_extra_data(self, key, value):
        extra_data = self.storage._get_extra_data()
        extra_data[key] = value
        self.storage._set_extra_data(extra_data)

    def process_step(self, form):
        if self.steps.current == 'select_main_candidate':
            main_candidate_choice = form.cleaned_data['journals']
            self.condition_dict['select_main_candidate_all'] = False if main_candidate_choice else True
        return super(JournalMergeWizard, self).process_step(form)

    def get_extra_data(self, key):
        return self.storage._get_extra_data().get(key, None)

    def done(self, form_list, **kwargs):
        last_step = self.steps.current
        select_main_candidate_form = form_list[self.get_step_index(last_step)]
        journals = self.get_extra_data("queryset")
        select_main_candidate_form.save(self.request, journals)
        return redirect("journals_management")


@permission_abstract_required("edit_journals")
def journals_management(request):
    return render_to_response(request, "journals/journal_management.html", )


@permission_abstract_required("edit_journals")
def journals_last_added(request):
    return render_to_response(request, "journals/journal_last_added.html", )


def search_journals_json(request):
    max_search_result_limit = 300
    search_string = request.GET.get('search_string', '')
    search_string = search_string.strip()
    if search_string:
        journals_by_issn = ISSN.objects.all().filter(value=search_string).values('journal')
        journals = Journal.objects.all().prefetch_related('articles', 'publisher', 'creator', 'issns',
                                                          'indexes').filter(Q(name__icontains=search_string) | Q(id__in=journals_by_issn))
        journals = list(journals[:max_search_result_limit])
    else:
        journals = []
    message_data = {}
    message_data["aaData"] = [get_journal_json(journal) for journal in journals]
    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')


def journals_by_id_json(request):
    ids = request.GET.get('ids', "")
    if ids:
        ids = ids.split(",")
        # FIXME-greg DRY. This method call is repeated at least 3 times, why not make it a new Manager method?
        journals = Journal.objects.all().prefetch_related('articles', 'publisher', 'creator', 'issns', 'indexes')
        journals = journals.filter(pk__in=ids)
    else:
        journals = []
    message_data = {}
    message_data["aaData"] = [get_journal_json(journal) for journal in journals]
    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')


def last_added_journals_json(request):
    last_journals_limit = 300
    journals = Journal.objects.all().prefetch_related('articles', 'publisher', 'creator', 'issns', 'indexes')
    journals = journals.order_by('-pk')[:last_journals_limit]
    message_data = {}
    message_data["aaData"] = [get_journal_json(journal) for journal in journals]
    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')


@user_passes_test(lambda u: u.is_superuser)
def journal_aliases_test(request):
    search_string = request.POST.get("journal_name", "").strip()
    if search_string:
        last_journals = Journal.objects.filter(name__icontains=search_string)
    else:
        last_journals = Journal.objects.all()
    results_num = 30
    last_journals = list(last_journals.order_by('-pk')[:results_num])
    journal_names_similar = []
    if search_string:
        search_string_similar = Journal.objects.get_similar(search_string, return_queryset=True,
                                                            get_all_variants=True)
    else:
        search_string_similar = []
    for journal in last_journals:
        journal_name = journal.name
        similar_names = [x for x in
                         Journal.objects.get_similar(journal_name, return_queryset=True, get_all_variants=True)
                         if x != journal]
        journal_names_similar.append([journal, similar_names])
    return render_to_response(
        request, "journals/journal_aliases_test.html",
        {
            'journal_names_similar': journal_names_similar,
            'search_string': search_string,
            'search_string_similar': search_string_similar
        })

from impacts import *
