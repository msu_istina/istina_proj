# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_detail

from common.views import autocomplete_search
from journals.models import Journal, JournalEditorialBoardMembership, JournalIndex, JournalRubric
from journals.views import JournalEditorialBoardMembershipWizardView as Wizard
from journals.views import journal_detail, journal_card,  JournalCardWizard, journals_search, \
    JournalMergeView, JournalMergeConfirmView, journals_management, search_journals_json, journals_by_id_json, \
    journals_last_added, last_added_journals_json, journal_card_save, journal_aliases_test, \
    journal_impacts, journal_impacts_search
from publications.views import object_publications_in_style


# journals
urlpatterns = patterns('journals.views',
    url(r'^(?P<object_id>\d+)/$', journal_detail, name='journals_detail'),
    url(r'^(?P<object_id>\d+)/impacts/$', journal_impacts, name='journals_impacts'),
    url(r'^(?P<object_id>\d+)/impacts/search/$', journal_impacts_search, name='journals_impacts_search'),
    url(r'^(?P<object_id>\d+)/articles.bib$', object_detail, {'queryset': Journal.objects.all(), 'template_name': 'publications/publication_list.bib', 'mimetype': 'text/plain'}, name='journals_journal_articles_bibtex'),
    url(r'^(?P<object_id>\d+)/style/(?P<style>.+)/$', object_publications_in_style, {'model': Journal, 'template_name': 'journals/journal_detail.html', 'template_object_name': 'journal'}, name='journals_journal_detail_in_style'),
    url(r'^(?P<journal_id>\d+)/admin/$', journal_card,  name='journals_card'),
    url(r'^(?P<object_id>\d+)/admin/edit/$', JournalCardWizard.as_view(), name='journals_card_edit'),
    url(r'^(?P<journal_id>\d+)/admin/edit/aliases$', journal_card, {'form_name': 'aliases'}, name='journals_card_edit_aliases'),
    url(r'^(?P<journal_id>\d+)/admin/edit/save$', journal_card_save, name='journal_card_save'),
    url(r'^search/autocomplete/$', autocomplete_search, {'model': Journal}, name="journals_search_autocomplete"),
    url(r'^search/autocomplete_rubrics/$', autocomplete_search, {'model': JournalRubric}, name="rubric_search_autocomplete"),
    url(r'^journals_search/$', journals_search, name="journals_search"),
    url(r'^index/(?P<object_id>\d+)/$', object_detail, {'queryset': JournalIndex.objects.all(), 'template_name': 'journals/journal_index_detail.html', 'template_object_name': 'index'}, name='journals_index_detail'),
    url(r'^journals_search_json/$', search_journals_json, name="journals_search_json"),
    url(r'^journals_json_by_id/$', journals_by_id_json, name="journals_json_by_id"),
    url(r'^journals_json_last_added/$', last_added_journals_json, name="last_added_journals_json"),
    url(r'^journals_management/$', journals_management, name="journals_management"),
    url(r'^journals_management/merge$', JournalMergeView.as_view(), name="journals_merge"),
    url(r'^journals_management/merge_confirm$', JournalMergeConfirmView.as_view(), name="journals_merge_confirm"),
    url(r'^journals_management/last_added', journals_last_added, name="journals_last_added"),
    url(r'^aliases_test/$', journal_aliases_test, name="journals_aliases_test"),
)

# memberships in journals' editorial boards
membership_options_base = {'model': JournalEditorialBoardMembership}
membership_options_detail = dict(membership_options_base.items() + [('template_name', 'journals/journal_editorial_board_membership_detail.html')])

urlpatterns += patterns('common.views',
    url(r'^memberships/add/$', Wizard.as_view(), name='journals_membership_add'),
    url(r'^memberships/(?P<object_id>\d+)/$', 'detail', membership_options_detail, name='journals_membership_detail'),
    url(r'^memberships/(?P<object_id>\d+)/edit/$', Wizard.as_view(), name='journals_membership_edit'),
    url(r'^memberships/(?P<object_id>\d+)/delete/$', 'delete', membership_options_base, name='journals_membership_delete'),
)


