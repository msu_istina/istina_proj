# -*- coding: utf-8; -*-
from authority.models import Permission as AuthorityPermission
from django.contrib.auth.models import Group, User
from django.db import models
from common.models import MyModel, ProxyContentType
from common.utils.user import get_profile
from datetime import datetime
from .managers import PermissionWithLabelManager, AllGrantedPermissionsManager, AllManagedWorkersManager

__author__ = 'Vladimir'


class PermissionLabel(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_PERMISSIONSTYPES_ID')
    name = models.CharField(max_length=100, db_column='F_PERMISSIONSTYPES_NAME', unique=True, verbose_name=u'Имя')
    rus_info = models.CharField(max_length=100, db_column='F_PERMISSIONSTYPES_RUSINFO', blank=True, null=True,
                                verbose_name=u'Русскоязычное название')
    enabled_content_types = models.ManyToManyField("common.ProxyContentType", related_name="permission_labels",
                                                   through="PermissionLabelForContentType")
    is_role = models.NullBooleanField(db_column="F_PERMISSIONSTYPES_ISROLE", blank=True, default=True, verbose_name=u'Роль')

    class Meta:
        db_table = 'PERMISSIONSTYPES'
        verbose_name = u"Тип разрешений"
        verbose_name_plural = u"Типы разрешений"
        ordering = ('name',)

    def __str__(self):
        if self.rus_info:
            return self.rus_info
        else:
            return self.name

    def __unicode__(self):
        if self.rus_info:
            return unicode(self.rus_info)
        else:
            return unicode(self.name)

    def get_parents(self):
        """This function returns the list of all permission_label's parents."""
        parents_ids = self.parent_relations.values_list('parent', flat=True)
        return PermissionLabel.objects.filter(id__in=parents_ids)

    def get_ancestors(self):
        """This function returns the list of all permission_label's ancestors."""
        ancestors = []
        parents = self.get_parents()
        for parent in parents:
            ancestors += parent.get_ancestors()
        ancestors += parents
        return ancestors


class PermissionLabelRelation(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_PERMISSIONSTYPESLINK_ID")
    parent = models.ForeignKey(to="PermissionLabel", related_name="children_relations", verbose_name=u"родительский тип",
                               db_column="PER_F_PERMISSIONSTYPES_ID")
    child = models.ForeignKey(to="PermissionLabel", related_name="parent_relations", verbose_name=u"дочерний тип",
                              db_column="F_PERMISSIONSTYPES_ID")
    startdate = models.DateField(u"Начало действия связи", db_column="F_PERMISSIONSTYPESLINK_BEGIN", default=datetime.now)
    enddate = models.DateField(u"Окончание действия связи", db_column="F_PERMISSIONSTYPESLINK_END", null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="permission_label_relations_added",
                                db_column="F_PERMISSIONSTYPESLINK_USER", null=True, blank=True, verbose_name=u'Создатель')

    class Meta:
        db_table = "PERMISSIONSTYPESLINK"
        verbose_name = u"Связь между типами разрешений"
        verbose_name_plural = u"Связи между типами разрешений"
        unique_together = ("parent", "child", "startdate")
        ordering = ("-startdate", "-enddate")

    def __unicode__(self):
        return "%s is a parent of %s" % (unicode(self.parent), unicode(self.child))


class PermissionLabelForContentType(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_PERMCONTTYPE_ID')
    label = models.ForeignKey(to=PermissionLabel, verbose_name=u"тип разрешения", db_column='F_PERMISSIONSTYPES_ID')
    cont_type = models.ForeignKey(to=ProxyContentType, verbose_name=u"тип объекта", db_column='F_PERMCONTTYPE_DJCONTID')

    class Meta:
        db_table = 'PERMCONTTYPE'
        verbose_name = u"Тип разрешений для типов объектов"
        verbose_name_plural = u"Типы разрешений для типов объектов"
        ordering = ('cont_type', 'label')

    def __str__(self):
        return self.label.name + " " + self.cont_type.name

    def __unicode__(self):
        return unicode(self.label.name + " " + self.cont_type.name)


class PermissionWithLabel(AuthorityPermission, MyModel):
    primary_key = models.AutoField(primary_key=True, db_column="F_PERMISSIONWITHLABEL_ID")
    auth_perm = models.OneToOneField(to=AuthorityPermission, related_name="label_link",
                                     db_column="F_PERMISSIONWITHLABEL_AUTPERMI", parent_link=True)
    label = models.ForeignKey(to=PermissionLabel, related_name="permissions_links", verbose_name=u"метка",
                              db_column="F_PERMISSIONSTYPES_ID")
    startdate = models.DateField(u"Начало срока действия разрешения", db_column="F_PERMISSIONWITHLABEL_BEGIN", default=datetime.now)
    enddate = models.DateField(u"Окончание срока действия разрешения", db_column="F_PERMISSIONWITHLABEL_END", null=True, blank=True)

    objects = PermissionWithLabelManager()

    search_attr = "codename"
    nominative_en = "permission"
    nominative = u"разрешение"
    genitive = u"разрешения"
    genitive_plural_full = u"разрешениям"
    dative = u"разрешению"
    accusative = u"разрешение"
    accusative_short = u"разрешение"
    instrumental = u"разрешением"
    locative = u"разрешении"
    gender = 'neuter'
    gender_suffix = u"о"

    class Meta:
        verbose_name = u"Разрешение"
        verbose_name_plural = u"Разрешения"
        ordering = ('content_type',)
        db_table = 'PERMISSIONWITHLABEL'

    @models.permalink
    def get_absolute_url(self):
        if self.user:
            return ('user_profile_public', (), {'username': self.user.username})
        else:
            return ('user_profile_public', (), {'username': self.group.user_set.all()[0].username})

    def __unicode__(self):
        return u"%s %s %s" % (self.fullname_short,
                              unicode(self.russian_codename) if self.russian_codename else unicode(self.codename),
                              unicode(self.content_object))

    @property
    def russian_codename(self):
        return self.label.rus_info

    @property
    def fullname_short(self):
        if self.user:
            profile = get_profile(self.user)
            return profile.fullname_short if profile else u"<не зарегистрирован в системе>"
        if self.group:
            return self.group.name
        return u"<не зарегистрирован в системе>"

    def save(self, *args, **kwargs):
        if not self.codename:
            self.codename = self.label.name
        return super(PermissionWithLabel, self).save(*args, **kwargs)


class ProxyGroup(Group):
    search_attr = "name"
    nominative_en = "group"
    nominative = u"группа"
    genitive = u"группы"
    genitive_plural_full = u"группас"
    dative = u"группе"
    accusative = u"группу"
    accusative_short = u"группу"
    instrumental = u"группой"
    locative = u"группе"
    gender = 'neuter'
    gender_suffix = u"а"

    @models.permalink
    def get_absolute_url(self):
        if self.user_set.all():
            return ('user_profile_public', (), {'username': self.user_set.all()[0].username})
        else:
            return ('user_profile_public', (), {'username': "vladimir_bukhonov"})

    class Meta:
        proxy = True

# ================ Models for security related database views ======================

class AllGrantedPermissions(models.Model):
    # Both representatives rights and normal roles are included;
    # recursion by departments' structure and roles inclusion.
    user = models.ForeignKey(to="auth.User", related_name="granted_department_roles",
                             db_column="USER_ID",
                             on_delete=models.DO_NOTHING,
                             primary_key=True) # Is not a primary key actually, required by django

    department = models.ForeignKey(to="organizations.Department", related_name="granted_department_roles",
                                   db_column="F_DEPARTMENT_ID",
                                   on_delete=models.DO_NOTHING,
                                   null=True, blank=True)

    exact_department_grant = models.IntegerField("Разрешение выдано именно на это подразделение",
                                                 db_column="EXACT_DEPARTMENT_GRANT",
                                                 null=False, blank=False)

    role_name = models.CharField(u"Имя разрешения", max_length=255,
                                 db_column="F_PERMISSIONSTYPES_NAME",
                                 null=True, blank=True)

    is_active = models.NullBooleanField(u"Флаг активности",
                                        db_column="APPROVED",
                                        default=False, blank=True)

    start_date = models.DateTimeField(u"Дата начала действия",
                                      db_column="F_PERMISSIONWITHLABEL_BEGIN",
                                      blank=True, null=True, auto_now_add=False)

    end_date = models.DateTimeField(u"Дата окончания действия",
                                    db_column="F_PERMISSIONWITHLABEL_END",
                                    blank=True, null=True, auto_now_add=False)

    objects = AllGrantedPermissionsManager()

    def save(self, **kwargs):
        raise NotImplementedError() # Disable insertion/deletion

    class Meta:
        db_table = "V_GRANTED_ROLES"
        managed = False


class AllManagedWorkers(models.Model):
    user = models.ForeignKey(to="auth.User", related_name="managed_by", db_column="USER_ID", primary_key=True)
    # Is not a primary key actually, required by django
    worker = models.ForeignKey(to="workers.Worker", related_name="managed_by", verbose_name=u"Сотрудник",
                               db_column="F_MAN_ID", on_delete=models.DO_NOTHING)
    department = models.ForeignKey(to="organizations.Department", related_name="managed",
                                   db_column="F_DEPARTMENT_ID",
                                   on_delete=models.DO_NOTHING,
                                   null=True, blank=True)
    role_holder = models.ForeignKey(to="auth.User", related_name="managed_workers", db_column="ROLE_USER_ID", on_delete=models.DO_NOTHING)
    label = models.ForeignKey(to=PermissionLabel, related_name="managed_via_role", verbose_name=u"метка",
                              db_column="F_PERMISSIONSTYPES_ID")
    role_name = models.CharField(u"Имя разрешения", max_length=255, db_column="F_PERMISSIONSTYPES_NAME")
    expiration_delay = models.FloatField(db_column="EXPIRATION_DELAY")
    # How many days have passed from worker's retirement from the 'managable' department

    objects = AllManagedWorkersManager()

    def save(self, **kwargs):
        raise NotImplementedError() # Disable insertion/deletion

    def delete(self, **kwargs):
        raise NotImplementedError() # Disable insertion/deletion

    class Meta:
        db_table = "V_MY_USERS_VIA_ROLE"
        managed = False
