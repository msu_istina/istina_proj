# -*- coding: utf-8 -*-

__author__ = 'Vladimir'

import logging
from functools import wraps
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import available_attrs, method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.http import Http404, HttpRequest
from organizations.models import Department
from unified_permissions import has_permission
from unified_permissions import has_permission_to_department

logger = logging.getLogger("unified_permissions.decorators")


def permission_required(operation, model=None, object_variable_name='object_id', abstract=False):
    """
    Decorator for views (function- or class-based) that checks that a user has a permission to fulfill an operation.
    """
    def decorator(func):
        @wraps(func, assigned=available_attrs(func))
        def _wrapped(*args, **kwargs):
            if isinstance(args[0], HttpRequest):
                request = args[0]
                method = False
            else: # this is a class-based view
                view = args[0]
                request = args[1]
                method = True
            if not request.user.is_authenticated():
                if method:
                    return method_decorator(login_required)(func)(*args, **kwargs)
                else:
                    return login_required(func)(*args, **kwargs)
            if not abstract:
                # this is important that we raise errors if abstract is False, but object_id, model or content_type id are missing.
                # we don't want to fallback to abstract permission checking in this case.
                # so if we intend to check permission on the given object, not abstract permission, we have to raise errors
                # or otherwise we will be silently checking abstract permissions, i.e. that user has this permission for ANY object,
                # which is not restrictive. Fortunately, abstract permissions checking works only for role labels, which is more error-proof.
                object_id = kwargs[object_variable_name]
                local_model = model or kwargs.get("model") or get_object_or_404(ContentType, pk=int(kwargs["content_type_id"])).model_class()
                obj = get_object_or_404(local_model, pk=int(object_id))
            else:
                obj = None
            if not has_permission(request.user, operation, obj):
                messages.error(request, u"У вас нет права на доступ к данной странице.")
                return redirect("startpage")
            return func(*args, **kwargs)
        return _wrapped

    return decorator


def permission_abstract_required(operation):
    """
    Decorator for views (function- or class-based) that checks that a user has a permission to fulfill an operation.
    It is a separate decorator for abstract permissions (i.e. not requiring an object).
    It is separated because we don't want to confuse the main decorator for abstract permission and for object permission
    with some parameters (model, content type) given subtly in kwargs.
    So to ensure security and not accidentally check abstract permission when we don't want to,
    we need to instantly know if this is an abstract permission checking or not.
    """
    return permission_required(operation, abstract=True)


def permission_to_department_required(operation, object_variable_name='department_id'):
    """
    Decorator for views that checks that a user has a permission to fulfill an operation in specific Department.
    """
    return permission_required(operation, model=Department, object_variable_name=object_variable_name)
