var oTable;
$(document).ready(function() {
   oTable = $('#tblData').DataTable({
        "oLanguage": {
            "sLengthMenu": "Показывать по _MENU_ записей на странице",
            "sZeroRecords": "Нет записей, удовлетворяющих условиям поиска",
            "sInfo": "Показаны записи с _START_ по _END_. Всего: _TOTAL_",
            "sInfoEmpty": "Показаны строки с 0 по 0 из 0",
            "sInfoFiltered": "(отобрано по условию из _MAX_ записей)",
            "oPaginate": {
               "sNext": "&nbsp;&nbsp;&nbsp;&nbsp;Следующая страница >>",
               "sPrevious": "<< Предыдущая страница"
            },
            "sSearch": "Поиск (по всем полям):"
        },
       "deferRender": true,
       "iDisplayLength": 25
   });

});

//this is function for changing "active/approved" status

function activateApprovementSelector(event, element, id_number, typevalue, csrftoken, rowIndex)
    {
        $(element).editable("/unified_permissions/change_approve_status/",
           {
               tooltip: 'Нажмите, чтобы переключить статус активности',
               loadtext: 'Загрузка...',
               type: 'select',
               event: 'click',
               onblur: 'submit',
               data: "{ '1': 'Да', '0': 'Нет' }",
               submitdata: {
                             type_value: typevalue,
                             csrfmiddlewaretoken: csrftoken,
                             id_num: id_number
                           },
               placeholder: "No value"
           }
        );
    }