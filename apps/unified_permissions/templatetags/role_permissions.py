# -*- coding: utf-8; -*-
__author__ = 'goldan'

from django import template
from unified_permissions import has_permission
from authority.templatetags.permissions import ResolverNode
from django.core.exceptions import ImproperlyConfigured

register = template.Library()

class PermissionComparisonNode(ResolverNode):
    """
    Implements a node to provide an "if user/group has permission on object"
    """
    @classmethod
    def handle_token(cls, parser, token):
        bits = token.contents.split()
        if len(bits) not in [3, 4, 5, 6]:
            raise template.TemplateSyntaxError("'%s' tag takes three to six arguments" % bits[0])
        end_tag = 'endifhaspermission'
        assignment_tag = len(bits) in [5,6] and bits[-2] == 'as'
        if assignment_tag:
            nodelist_true = None
            nodelist_false = None
        else:
            nodelist_true = parser.parse(('else', end_tag))
            token = parser.next_token()
            if token.contents == 'else': # there is an 'else' clause in the tag
                nodelist_false = parser.parse((end_tag,))
                parser.delete_first_token()
            else:
                nodelist_false = template.NodeList()
        user = bits[1]
        operation = bits[2]
        obj = bits[3] if len(bits) in [4,6] else None
        var_name = bits[-1] if assignment_tag else None
        return cls(user, operation, obj, nodelist_true, nodelist_false, var_name)

    def __init__(self, user, operation, obj, nodelist_true, nodelist_false, var_name):
        self.user = user
        self.operation = operation
        self.obj = obj
        self.nodelist_true = nodelist_true
        self.nodelist_false = nodelist_false
        self.var_name = var_name

    def render(self, context):
        try:
            user = self.resolve(self.user, context)
            operation = self.resolve(self.operation, context)
            obj = self.resolve(self.obj, context)
            allowed = has_permission(user, operation, obj)
            if self.var_name:
                context[self.var_name] = allowed
                return ''
            if allowed:
                return self.nodelist_true.render(context)
        # If the app couldn't be found
        except (ImproperlyConfigured, ImportError):
            return ''
        # If either variable fails to resolve, return nothing.
        except template.VariableDoesNotExist:
            return ''
        # If the types don't permit comparison, return nothing.
        except (TypeError, AttributeError):
            return ''
        return self.nodelist_false.render(context)

@register.tag
def ifhaspermission(parser, token):
    """
    This function provides functionality for the 'ifhaspermission' template tag

    Syntax::

        {% ifhaspermission user operation obj %}
            lalala
        {% else %}
            meh
        {% endifhaspermission %}

        {% ifhaspermission user operation %}
            lalala
        {% else %}
            meh
        {% endifhaspermission %}

        {% ifhaspermission user operation obj as allowed %}
        {% if allowed %}
            lalala
        {% endif %}

        {% ifhaspermission user operation as allowed %}
        {% if allowed %}
            lalala
        {% endif %}
    """
    return PermissionComparisonNode.handle_token(parser, token)