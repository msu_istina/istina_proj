# -*- coding: utf-8; -*-
from datetime import datetime
from django import forms
from django.db.models import Q
from django.forms import ModelChoiceField, ModelMultipleChoiceField
from workers.models import Profile
from common.forms import Select2ModelFormMixin, Select2ModelField
from common.models import ProxyContentType
from unified_permissions.models import PermissionWithLabel, PermissionLabel, ProxyGroup
from common.widgets import LightSelect2ChoiceWidget
from django.contrib.auth.models import User, Group
from workers.forms import ProfileChoiceField

from organizations.models import Department, Organization
from common.dynatree_widgets import DynatreeWidget
from common.utils.user import get_profile
from django.contrib.contenttypes.models import ContentType
from organizations.utils import is_representative_for, is_representative_for_whole_organization
from django.shortcuts import get_object_or_404

from common.widgets import MyFilteredSelectMultiple

__author__ = 'Vladimir'


class UserRolePermissionCreateEditForm(Select2ModelFormMixin):
    label = ModelChoiceField(label=u"Наименование разрешения", #widget=LightSelect2ChoiceWidget(width=320),
                             empty_label=None, required=True,
                             queryset=PermissionLabel.objects.exclude(name=u"is_representative_for").distinct().order_by("rus_info"))
    profile = ProfileChoiceField(label=u"Пользователь", empty_label=None, required=True, widget_kwargs={'width': 320})
    department = ModelChoiceField(label=u"Подразделение", queryset=Department.objects.all())
    organization = ModelChoiceField(label=u"Организация", queryset=Organization.objects.all(), empty_label=None)

    class Meta:
        model = PermissionWithLabel
        fields = ['profile', 'label', 'department', 'organization']#, 'startdate', 'enddate']

    def __init__(self, request_user=None, edit=False, org_level=False, *args, **kwargs):
        super(UserRolePermissionCreateEditForm, self).__init__(*args, **kwargs)
        self.request_user = request_user
        self.org_level = org_level
        self.edit = edit
        self.add = not self.edit
        self.organization_field_data = None
        if org_level:
            self.fields['label'].queryset = self.fields['label'].queryset.filter(enabled_content_types__model=u"organization")
            orgs_qs = Organization.objects.none()
            if self.request_user.is_superuser:
                orgs_qs = Organization.objects.all()
            else:
                # Non-root user can only assign representatives from their own organizations
                repr_qs = self.request_user.representatives.filter(department__isnull=True)
                orgs_qs = Organization.objects.filter(id__in=repr_qs.values_list('organization', flat=True))
            self.fields['organization'].queryset = orgs_qs
            if len(orgs_qs) == 1:
                self.fields['organization'] = forms.CharField(label=u"Организация")
                self.fields['organization'].widget.attrs['readonly'] = 'readonly'
                self.fields['organization'].initial = Organization.objects.get(id=repr_qs.values_list('organization', flat=True)[0])
                self.organization_field_data = Organization.objects.get(id=repr_qs.values_list('organization', flat=True)[0])
            del self.fields['department']
        else:
            self.fields['label'].queryset = self.fields['label'].queryset.filter(enabled_content_types__model=u"department")
            departments_sql = request_user.representatives.create_sql_statement_for_departments_dynatree(request_user)
            self.fields['department'].widget = DynatreeWidget(select_mode=1, initial_sql=departments_sql)
            self.fields['department'].required = True
            del self.fields['organization']
        if self.edit:
            self.fields['profile'].initial = get_profile(self.instance.user)
            if org_level:
                self.fields['organization'].initial = get_object_or_404(Organization, id=self.instance.object_id)
            else:
                self.fields['department'].initial = get_object_or_404(Department, id=self.instance.object_id)
            self.fields['label'].initial = get_object_or_404(PermissionLabel, name=self.instance.codename)

    def clean_department(self):
        department = self.cleaned_data.get('department')
        if department is not None:
            if not (is_representative_for(self.request_user, department) or self.request_user.is_superuser):
                raise forms.ValidationError(u"У Вас не хватает полномочий, чтобы выдать роль сотруднику в рамках данного подразделения.")
        return department

    def clean_organization(self):
        organization = self.cleaned_data.get('organization')
        if type(organization) == type(u"") or type(organization) == type(''):
            organization = self.organization_field_data
        if organization is not None:
            if not (is_representative_for_whole_organization(self.request_user, organization) or self.request_user.is_superuser):
                raise forms.ValidationError(u"У Вас не хватает полномочий, чтобы выдать роль сотруднику в рамках данного подразделения.")
        return organization

    def clean(self):
        cleaned_data = super(UserRolePermissionCreateEditForm, self).clean()
        content_type_dep = ContentType.objects.get(model=u"department", app_label=u"organizations")
        content_type_org = ContentType.objects.get(model=u"organization", app_label=u"organizations")
        department = cleaned_data.get('department')
        organization = self.cleaned_data.get('organization')
        if type(organization) == type(u"") or type(organization) == type(''):
            organization = self.organization_field_data
        codename_label = cleaned_data.get('label')
        profile = cleaned_data.get('profile')
        if profile is not None:
            user = profile.user
            if department is not None:
                department_id = department.id
                organization_id = department.organization.id
                try:
                    obj = PermissionWithLabel.objects.get(object_id=department_id, content_type=content_type_dep,
                                                          label=codename_label, user=user)
                except PermissionWithLabel.DoesNotExist:
                    obj = None
                if obj is not None:
                    if not self.edit and obj.id != self.instance.id:
                        raise forms.ValidationError("У этого пользователя уже есть такое разрешение.")
                else:
                    qs_anc_dep = PermissionWithLabel.objects.filter(object_id__in=[anc.id for anc in department.ancestors],
                                                                    content_type=content_type_dep,
                                                                    label=codename_label, user=user)
                    if self.edit:
                        qs_anc_dep = qs_anc_dep.exclude(id=self.instance.id)
                    if qs_anc_dep.exists():
                        raise forms.ValidationError('У этого пользователя уже есть такое разрешение в "родительском" подразделении.')
                    qs_par_org = PermissionWithLabel.objects.filter(object_id=organization_id,
                                                                    content_type=content_type_org,
                                                                    label=codename_label, user=user)
                    if qs_par_org.exists():
                        raise forms.ValidationError('У этого пользователя уже есть такое разрешение в рамках всей организации.')
            else:
                if organization is not None:
                    try:
                        obj = PermissionWithLabel.objects.get(object_id=organization.id, content_type=content_type_org,
                                                              label=codename_label, user=user)
                    except PermissionWithLabel.DoesNotExist:
                        obj = None
                    if obj is not None:
                        if not self.edit and obj.id != self.instance.id:
                            raise forms.ValidationError("У этого пользователя уже есть такое разрешение.")
        return cleaned_data

    def save(self, request, commit=True):
        user_permission = super(UserRolePermissionCreateEditForm, self).save(request=request, commit=False)
        user_permission.user = self.cleaned_data['profile'].user
        user_permission.codename = self.cleaned_data['label'].name
        user_permission.creator = request.user
        user_permission.approved = True
        user_permission.date_approved = datetime.now()
        if self.org_level:
            user_permission.content_type = ContentType.objects.get(model=u"organization", app_label=u"organizations")
            organization = self.cleaned_data['organization']
            if type(organization) == type(u"") or type(organization) == type(''):
                organization = self.organization_field_data
            user_permission.object_id = organization.id
        else:
            user_permission.content_type = ContentType.objects.get(model=u"department", app_label=u"organizations")
            user_permission.object_id = self.cleaned_data['department'].id
        if commit:
            self.commit_save(request, user_permission)
        return user_permission


class UserRolePermissionDepartmentEditForm(Select2ModelFormMixin):
    permission_labels = ModelMultipleChoiceField(label=u"",
                                                 queryset=PermissionLabel.objects.exclude(name=u"is_representative_for").
                                                                                  distinct().order_by("rus_info"),
                                                 required=False,
                                                 widget=MyFilteredSelectMultiple(verbose_name=u"разрешения",
                                                                                 is_stacked=False))

    class Meta:
        model = PermissionWithLabel
        fields = ['permission_labels']

    def __init__(self, request_user=None, auth_user=None, org_level=False, auth_object=None, *args, **kwargs):
        super(UserRolePermissionDepartmentEditForm, self).__init__(*args, **kwargs)
        self.request_user = request_user
        self.auth_user = auth_user
        self.org_level = org_level
        if org_level:
            self.auth_organization = auth_object
            self.fields['permission_labels'].queryset = self.fields['permission_labels'].queryset.filter(enabled_content_types__model=u"organization")
            self.fields['permission_labels'].initial = [unicode(perm.label_link.label.id)
                                                        for perm in auth_user.granted_permissions.filter(
                                                                                object_id=auth_object.id,
                                                                                content_type__model=u'organization',
                                                                                content_type__app_label=u'organizations')]
        else:
            self.auth_department = auth_object
            self.fields['permission_labels'].queryset = self.fields['permission_labels'].queryset.filter(enabled_content_types__model=u"department")
            self.fields['permission_labels'].initial = [unicode(perm.label_link.label.id)
                                                        for perm in auth_user.granted_permissions.filter(
                                                                                    object_id=auth_object.id,
                                                                                    content_type__model=u'department',
                                                                                    content_type__app_label=u'organizations')]

    def clean(self):
        cleaned_data = super(UserRolePermissionDepartmentEditForm, self).clean()
        if not self.org_level:
            content_type_dep = ContentType.objects.get(model=u"department", app_label=u"organizations")
            content_type_org = ContentType.objects.get(model=u"organization", app_label=u"organizations")
            if self.auth_department is not None:
                organization_id = self.auth_department.organization.id
                labels_list = cleaned_data.get('permission_labels', [])
                #codename_list = [perm_label.name for perm_label in cleaned_data.get('permission_labels', [])]
                error_message = u"У этого пользователя уже есть "
                error_flag = False
                for label in labels_list:
                #for codename in codename_list:
                    for anc in self.auth_department.ancestors:
                        try:
                            anc_perm = PermissionWithLabel.objects.get(object_id=anc.id,
                                                                       content_type=content_type_dep,
                                                                       label=label, user=self.auth_user)
                        except PermissionWithLabel.DoesNotExist:
                            anc_perm = None
                        try:
                            anc_org_perm = PermissionWithLabel.objects.get(object_id=organization_id,
                                                                           content_type=content_type_org,
                                                                           label=label, user=self.auth_user)
                        except PermissionWithLabel.DoesNotExist:
                            anc_org_perm = None
                        if anc_perm is not None:
                            error_flag = True
                            if anc_perm.russian_codename:
                                anc_perm_codename = anc_perm.russian_codename
                            else:
                                anc_perm_codename = anc_perm.codename
                            error_message += u'разрешение "' + unicode(anc_perm_codename) + \
                                             u'" в "родительском" подразделении "' + unicode(anc.name) + u'", '
                        if anc_org_perm is not None:
                            error_flag = True
                            error_message += u'разрешение "' + unicode(anc_perm_codename) + \
                                             u'" в организации "' + unicode(anc.organization.name) + u'", '
                if error_flag:
                    error_message = error_message[:-2] + u'.'
                    raise forms.ValidationError(error_message)
        return cleaned_data

    def save(self, request, commit=True):
        user_permission = super(UserRolePermissionDepartmentEditForm, self).save(request=request, commit=False)
        labels_list = self.cleaned_data['permission_labels']
        if self.org_level:
            auth_object = self.auth_organization
            auth_object_content_type = ContentType.objects.get(model=u"organization", app_label=u"organizations")
        else:
            auth_object = self.auth_department
            auth_object_content_type = ContentType.objects.get(model=u"department", app_label=u"organizations")
        PermissionWithLabel.objects.filter(
            user=self.auth_user, object_id=auth_object.id).exclude(label__in=labels_list).delete()
        granted_permissions_labels_list = self.auth_user.granted_permissions.all().values_list('label_link__label__id', flat=True)
        for label in labels_list:
            if label.id not in granted_permissions_labels_list:
                user_permission = PermissionWithLabel(user=self.auth_user,
                                                      content_type=auth_object_content_type,
                                                      object_id=auth_object.id,
                                                      codename=label.name,
                                                      label=label, creator=request.user,
                                                      approved=True, date_approved=datetime.now())
                if commit:
                    self.commit_save(request, user_permission)
        return user_permission
