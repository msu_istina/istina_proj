# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q
from common.managers import MyManager
from authority.models import Permission as AuthorityPermission
import datetime


class PermissionWithLabelManager(MyManager):

    def create_for_authority_permissions(self):
        from .models import PermissionLabel, PermissionWithLabel
        def create_permission_with_label(authority_permission):
            perm = authority_permission
            print perm.codename, perm.content_object, perm.object_id
            label_name = perm.codename.lstrip("can_")
            try:
                label = PermissionLabel.objects.get(name=label_name)
            except models.ObjectDoesNotExist:
                print "PermissionLabel does not exist: %s" % label_name
                return False
            else:
                # http://stackoverflow.com/a/4065189/304209
                permission_with_label = PermissionWithLabel(label=label, auth_perm=perm)
                permission_with_label.__dict__.update(perm.__dict__)
                permission_with_label.save()
                return True

        count = 0
        print "%d authority.Permission objects exist." % AuthorityPermission.objects.count()
        print "%d PermissionWithLabel objects exist." % PermissionWithLabel.objects.count()
        for authority_permission in AuthorityPermission.objects.all():
            created = False
            try:
                permission_with_label = authority_permission.label_link
            except models.ObjectDoesNotExist:
                created = create_permission_with_label(authority_permission)
            else:
                if not permission_with_label:
                    created = create_permission_with_label(authority_permission)
            if created:
                count += 1

        print "%d PermissionWithLabel objects created." % count
        print "%d authority.Permission objects exist." % AuthorityPermission.objects.count()
        print "%d PermissionWithLabel objects exist." % PermissionWithLabel.objects.count()


class AllGrantedPermissionsManager(MyManager):

    @property
    def current(self):
        return self.filter(Q(is_active=True),
                           Q(end_date__isnull=True) | Q(end_date__gte=datetime.date.today()))


class AllManagedWorkersManager(models.Manager):

    def current(self, delay=0):
        return self.filter(expiration_delay__lte=delay)
