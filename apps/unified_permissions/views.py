# -*- coding: utf-8; -*-
__author__ = 'Vladimir'

from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Q
from django.shortcuts import get_object_or_404, render, redirect
from django.template import RequestContext
from django.contrib.auth.models import User
from unified_permissions.forms import UserRolePermissionCreateEditForm, UserRolePermissionDepartmentEditForm
from unified_permissions.models import PermissionWithLabel, PermissionLabel
from common.utils.context import render_to_response
from django.contrib.contenttypes.models import ContentType
from organizations.models import Representative, Organization, Department
from datetime import datetime
from django.http import HttpResponse, HttpResponseRedirect, Http404

from reversion.models import Version
from reversion import create_revision
from common.utils.reversion_utils import get_version_comment, get_admin_url
from common.utils.user import get_profile_url

from unified_permissions import has_permission

from django.db import connection, transaction
from django.core.exceptions import ObjectDoesNotExist
import json

@login_required
def index(request):
    '''This function returns the list of control hyperlinks'''
    if not has_permission(request.user, "edit_permissions_and_roles"):
        messages.warning(request, u'Эта страница доступна только для ответственных по подразделению. '
                                  u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.'
                                  u'Если Вы ответственный по организации, Вам требуется специальное разрешение на получение доступа.')
        return redirect("/home/")
    return render_to_response(request, "permissions_index.html")


@login_required
@transaction.commit_on_success()
@create_revision()
def user_role_permission_add(request, level):
    '''This function is for adding the PermissionWithLabel objects,
    which has Department or Organization as their target.'''
    if level == 'organization':
        if not request.user.representatives.organization_level():
            messages.warning(request, u'Эта страница доступна только для ответственных по организации. '
                                      u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.')
            return redirect("/home/")
        org_level = True
    elif level == 'department':
        if not has_permission(request.user, "edit_permissions_and_roles"):
            messages.warning(request, u'Эта страница доступна только для ответственных по подразделению. '
                                      u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.'
                                      u'Если Вы ответственный по организации, Вам требуется специальное разрешение на получение доступа.')
            return redirect("/home/")
        org_level = False
    else:
        raise Http404
    if request.method == "POST":
        redirect_url = request.GET.get("redirect_url")
        if request.POST.get("cancel_button"):
            return redirect(redirect_url)
        form = UserRolePermissionCreateEditForm(request.user, False, org_level, request.POST)
        if form.is_valid():
            form.save(request)
            return redirect("/unified_permissions/user/department/list/")
    else:
        redirect_url = request.META.get('HTTP_REFERER')
        form = UserRolePermissionCreateEditForm(request_user=request.user, org_level=org_level)
    return render(request, "user_role_permission_add_edit.html",
                  {'form': form, 'edit_mode': False, 'redirect_url': redirect_url, 'org_level': org_level,},
                  context_instance=RequestContext(request))

@login_required
@transaction.commit_on_success()
@create_revision()
def user_role_permission_edit(request, level, role_permission_id):
    '''This function is for editing the PermissionWithLabel objects,
    which has Department or Organization as their target.'''
    instance = get_object_or_404(PermissionWithLabel, id=role_permission_id)
    if level == 'organization':
        if not request.user.representatives.organization_level():
            messages.warning(request, u'Эта страница доступна только для ответственных по организации. '
                                      u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.')
            return redirect("/home/")
        if not request.user.representatives.filter(department__isnull=True, organization=instance.content_object):
                messages.warning(request, u'Эта страница доступна только для ответственных по организации "' +
                                  unicode(instance.organization.name) + u'". ')
        org_level = True
    elif level == 'department':
        if not has_permission(request.user, "edit_permissions_and_roles"):
            messages.warning(request, u'Эта страница доступна только для ответственных по подразделению. '
                                      u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.'
                                      u'Если Вы ответственный по организации, Вам требуется специальное разрешение на получение доступа.')
            return redirect("/home/")
        if not request.user.representatives.filter(Q(department=instance.content_object) |
                                                   Q(department__in=instance.content_object.get_ancestors_flat()) |
                                                  (Q(department__isnull=True) & Q(organization=instance.content_object.organization))):
            messages.warning(request, u'Эта страница доступна только для ответственных по подразделению "' + unicode(instance.content_object.name) + u'" '
                                      u'и ответственных по вышестоящим подразделениям. '
                                      u'Если Вы ответственный по организации, Вам требуется специальное разрешение на получение доступа.')
            return redirect("/home/")
        org_level = False
    else:
        raise Http404
    if request.method == "POST":
        redirect_url = request.GET.get("redirect_url")
        if request.POST.get("cancel_button"):
            return redirect(redirect_url)
        form = UserRolePermissionCreateEditForm(request.user, True, org_level, request.POST or None, instance=instance)
        if form.is_valid():
            form.save(request)
            return redirect("/unified_permissions/user/department/list/")
    else:
        redirect_url = request.META.get('HTTP_REFERER')
        form = UserRolePermissionCreateEditForm(request.user, True, org_level, instance=instance)
    return render(request, "user_role_permission_add_edit.html", {'form': form, 'role_permission_id': role_permission_id,
                                                                  'edit_mode': True, 'redirect_url': redirect_url,'org_level': org_level},
                  context_instance=RequestContext(request))

approvement_sorting = 1
@login_required
def associates_repr_permissions_list(request):
    '''This function returns the page for lists of all PermissionWithLabel objects and Representative objects targeting
        Organizations and Departments (including child Departments), which you are representative for, excluding you.'''
    if not has_permission(request.user, "edit_permissions_and_roles"):
        messages.warning(request, u'Эта страница доступна только для ответственных по подразделению. '
                                  u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.'
                                  u'Если Вы ответственный по организации, Вам требуется специальное разрешение на получение доступа.')
        return redirect("/home/")
    if request.method == 'POST':
        global approvement_sorting
        if request.POST.get('sorting_button'):
            approvement_sorting = int(request.POST.get('approvement_sorting'))
        perm_delete_list = [int(perm_id) for perm_id in request.POST.getlist('perm_delete_list')]
        repr_delete_list = [int(repr_id) for repr_id in request.POST.getlist('repr_delete_list')]
        if perm_delete_list or repr_delete_list:
            PermissionWithLabel.objects.filter(id__in=perm_delete_list).delete()
            Representative.objects.filter(id__in=repr_delete_list).delete()
            messages.success(request, u"Выбранные объекты успешно удалены")
        return HttpResponseRedirect("/unified_permissions/user/department/list/")
    cursor = connection.cursor()

    if request.user.representatives.exists():
        representatives_ids_string_list = "(" + ", ".join(str(representative_id)
                                                          for representative_id in
                                                          request.user.representatives.all().values_list("id", flat=True))\
                                        + ")"
    else:
        representatives_ids_string_list = "(NULL)"
    perm_sorting_condition = """"""
    repr_sorting_condition = """"""
    if approvement_sorting == 1:
        perm_sorting_condition = """ AND ap.APPROVED = 1;"""
        repr_sorting_condition = """ AND fellow_repr.F_REPRESENTATIVE_ISACTIVE = 1;"""
    if approvement_sorting == 0:
        perm_sorting_condition = """ AND ap.APPROVED = 0;"""
        repr_sorting_condition = """ AND fellow_repr.F_REPRESENTATIVE_ISACTIVE = 0;"""
    sql_perm_str ="""SELECT ap.ID AS perm_id,
                            ap.USER_ID AS perm_user_id,
                            pt.F_PERMISSIONSTYPES_NAME AS perm_type_name,
                            pt.F_PERMISSIONSTYPES_RUSINFO AS perm_type_rusinfo,
                            wp.LASTNAME || ' ' || wp.FIRSTNAME || ' ' || wp.MIDDLENAME AS perm_user_fullname,
                            '/profile/' || au.USERNAME AS perm_username,
                            (SELECT LISTAGG(('/organizations/department/' || mp.F_DEPARTMENT_ID ||
                                             '\\\\' || work_dep.F_DEPARTMENT_NAME), '||')
                             WITHIN GROUP (ORDER BY mp.F_DEPARTMENT_ID)
                             FROM MANSPOST mp
                                  LEFT OUTER JOIN DEPARTMENT work_dep ON work_dep.F_DEPARTMENT_ID = mp.F_DEPARTMENT_ID
                             WHERE mp.F_MAN_ID = wp.WORKER_ID
                             GROUP BY mp.F_MAN_ID
                            ) AS perm_workerships,
                            perm_dep.F_DEPARTMENT_ID AS perm_dep_id,
                            perm_dep.F_DEPARTMENT_NAME AS perm_dep_name,
                            '/organizations/department/' || perm_dep.F_DEPARTMENT_ID AS perm_dep_url,
                            perm_org.F_ORGANIZATION_ID AS perm_org_id,
                            perm_org.F_ORGANIZATION_NAME AS perm_org_name,
                            '/organizations/' || perm_org.F_ORGANIZATION_ID AS perm_org_url,
                            ap.APPROVED as perm_is_active
                    FROM AUTHORITY_PERMISSION ap
                         INNER JOIN PERMISSIONWITHLABEL pwl ON pwl.F_PERMISSIONWITHLABEL_AUTPERMI = ap.ID
                         LEFT OUTER JOIN PERMISSIONSTYPES pt ON pwl.F_PERMISSIONSTYPES_ID = pt.F_PERMISSIONSTYPES_ID
                         LEFT OUTER JOIN PERMCONTTYPE pct ON pct.F_PERMISSIONSTYPES_ID = pt.F_PERMISSIONSTYPES_ID
                         LEFT OUTER JOIN AUTH_USER au ON au.ID = ap.USER_ID
                         LEFT OUTER JOIN WORKERS_PROFILE wp ON wp.USER_ID = au.ID
                         LEFT OUTER JOIN DEPARTMENT perm_dep ON perm_dep.F_DEPARTMENT_ID = ap.OBJECT_ID
                                                            AND pct.F_PERMCONTTYPE_DJCONTID = (SELECT ID FROM DJANGO_CONTENT_TYPE
                                                                                              WHERE APP_LABEL = 'organizations'
                                                                                                    AND MODEL = 'department')
                         LEFT OUTER JOIN ORGANIZATION perm_org ON perm_org.F_ORGANIZATION_ID = ap.OBJECT_ID
                                                              AND pct.F_PERMCONTTYPE_DJCONTID = (SELECT ID FROM DJANGO_CONTENT_TYPE
                                                                                                WHERE APP_LABEL = 'organizations'
                                                                                                      AND MODEL = 'organization')
                    WHERE ((ap.OBJECT_ID IN (
                            SELECT F_DEPARTMENT_ID FROM (
                              SELECT r.F_REPRESENTATIVE_ID AS PARENT_F_REPRESENTATIVE_ID,
                                     dep.F_DEPARTMENT_ID AS F_DEPARTMENT_ID
                              FROM REPRESENTATIVE r
                                   left outer join DEPARTMENT dep ON dep.F_ORGANIZATION_ID = r.F_ORGANIZATION_ID
                              WHERE r.F_DEPARTMENT_ID IS NULL
                            UNION
                              SELECT r.F_REPRESENTATIVE_ID AS PARENT_F_REPRESENTATIVE_ID,
                                     apd.F_DEPARTMENT_ID AS F_DEPARTMENT_ID
                              FROM REPRESENTATIVE r
                                   LEFT OUTER JOIN ALL_PARENT_DEPARTMENT apd ON apd.PARENT_F_DEPARTMENT_ID = r.F_DEPARTMENT_ID
                              WHERE r.F_DEPARTMENT_ID IS NOT NULL
                              )
                              WHERE PARENT_F_REPRESENTATIVE_ID IN """ + representatives_ids_string_list +\
                            """)
                            AND
                            pct.F_PERMCONTTYPE_DJCONTID = (SELECT ID FROM DJANGO_CONTENT_TYPE
                                                          WHERE APP_LABEL = 'organizations'
                                                                AND MODEL = 'department'))
                            OR
                            (ap.OBJECT_ID IN (
                            SELECT F_ORGANIZATION_ID
                            FROM REPRESENTATIVE
                            WHERE F_DEPARTMENT_ID IS NULL AND F_REPRESENTATIVE_ID IN """ + representatives_ids_string_list +\
                            """)
                            AND
                            pct.F_PERMCONTTYPE_DJCONTID = (SELECT ID FROM DJANGO_CONTENT_TYPE
                                                          WHERE APP_LABEL = 'organizations'
                                                                AND MODEL = 'organization')
                            ))
                            AND ap.OBJECT_ID IS NOT NULL AND NOT (ap.USER_ID = """ + str(request.user.id) + """)""" \
                            + perm_sorting_condition
    cursor.execute(sql_perm_str)
    i = 1
    authority_permissions_sql = []
    for row in cursor.fetchall():
        if row[6] is not None:
            workerships = row[6].split("||")
        else:
            workerships = ["\\\\"]
        authority_permissions_sql.append(
            {
                'num': i,
                'id':  row[0],
                'name': row[3] if row[3] is not None else row[2] if row[2] is not None else "",
                'user_profile_dict': {
                                        'id': row[1] if row[1] is not None else "",
                                        'name': row[4] if row[1] is not None else "",
                                        'url': row[5] if row[1] is not None else "",
                                        'workership_departmemts_dicts': [{'name': workership.split("\\\\")[1],
                                                                          'url': workership.split("\\\\")[0]}
                                                                         for workership in workerships]
                                     } if row[1] is not None else "",
                'object_dict': {'id': row[7] if row[7] is not None else row[10] if row[10] is not None else "",
                                'name': row[8] if row[8] is not None else row[11] if row[11] is not None else "",
                                'url': row[9] if row[9] is not None else row[12] if row[12] is not None else ""},
                'is_active': row[13],
                'org_level': True if row[10] is not None else False
            }
        )
        i += 1
    sql_repr_str = """SELECT  fellow_repr.F_REPRESENTATIVE_ID AS fellow_repr_id,
                              fellow_repr.F_REPRESENTATIVE_USER AS fellow_repr_user_id,
                              wp.LASTNAME || ' ' || wp.FIRSTNAME || ' ' || wp.MIDDLENAME AS fellow_repr_user_fullname,
                              '/profile/' || au.USERNAME AS fellow_repr_username,
                              (SELECT LISTAGG(('/organizations/department/' || mp.F_DEPARTMENT_ID ||
                                               '\\\\' || work_dep.F_DEPARTMENT_NAME), '||')
                               WITHIN GROUP (ORDER BY mp.F_DEPARTMENT_ID)
                               FROM MANSPOST mp
                                    LEFT OUTER JOIN DEPARTMENT work_dep ON work_dep.F_DEPARTMENT_ID = mp.F_DEPARTMENT_ID
                               WHERE mp.F_MAN_ID = wp.WORKER_ID
                               GROUP BY mp.F_MAN_ID
                              ) AS fellow_repr_workerships,
                              repr_org.F_ORGANIZATION_NAME AS fellow_repr_org_name,
                              '/organizations/' || repr_org.F_ORGANIZATION_ID AS fellow_repr_org_url,
                              repr_dep.F_DEPARTMENT_NAME AS fellow_repr_dep_name,
                              '/organizations/department/' || repr_dep.F_DEPARTMENT_ID AS fellow_repr_dep_url,
                              fellow_repr.F_REPRESENTATIVE_ISACTIVE AS fellow_repr_is_active
                      FROM
                      (SELECT r1.F_REPRESENTATIVE_ID AS PARENT_F_REPRESENTATIVE_ID,
                              r2.F_REPRESENTATIVE_ID,
                              r2.F_REPRESENTATIVE_USER,
                              r2.F_DEPARTMENT_ID,
                              r2.F_ORGANIZATION_ID,
                              r2.F_REPRESENTATIVE_SCOPE,
                              r2.F_REPRESENTATIVE_POST,
                              r2.F_REPRESENTATIVE_EMAIL,
                              r2.F_REPRESENTATIVE_ADDRESS,
                              r2.F_REPRESENTATIVE_COMMENT,
                              r2.F_REPRESENTATIVE_ISACTIVE,
                              r2.F_REPRESENTATIVE_BEGIN,
                              r2.F_REPRESENTATIVE_END
                       FROM REPRESENTATIVE r1
                            LEFT OUTER JOIN REPRESENTATIVE r2 on r2.F_ORGANIZATION_ID = r1.F_ORGANIZATION_ID
                       WHERE r1.F_DEPARTMENT_ID IS NULL
                      UNION
                       SELECT r1.F_REPRESENTATIVE_ID as PARENT_F_REPRESENTATIVE_ID,
                              r2.F_REPRESENTATIVE_ID,
                              r2.F_REPRESENTATIVE_USER,
                              r2.F_DEPARTMENT_ID,
                              r2.F_ORGANIZATION_ID,
                              r2.F_REPRESENTATIVE_SCOPE,
                              r2.F_REPRESENTATIVE_POST,
                              r2.F_REPRESENTATIVE_EMAIL,
                              r2.F_REPRESENTATIVE_ADDRESS,
                              r2.F_REPRESENTATIVE_COMMENT,
                              r2.F_REPRESENTATIVE_ISACTIVE,
                              r2.F_REPRESENTATIVE_BEGIN,
                              r2.F_REPRESENTATIVE_END
                       FROM REPRESENTATIVE r1
                            LEFT OUTER JOIN DEPARTMENT dep on r1.F_ORGANIZATION_ID = dep.F_ORGANIZATION_ID
                            LEFT OUTER JOIN REPRESENTATIVE r2 on r2.F_DEPARTMENT_ID = dep.F_DEPARTMENT_ID
                       WHERE r1.F_DEPARTMENT_ID IS NULL
                      UNION
                       SELECT r1.F_REPRESENTATIVE_ID as PARENT_F_REPRESENTATIVE_ID,
                              r2.F_REPRESENTATIVE_ID,
                              r2.F_REPRESENTATIVE_USER,
                              r2.F_DEPARTMENT_ID,
                              r2.F_ORGANIZATION_ID,
                              r2.F_REPRESENTATIVE_SCOPE,
                              r2.F_REPRESENTATIVE_POST,
                              r2.F_REPRESENTATIVE_EMAIL,
                              r2.F_REPRESENTATIVE_ADDRESS,
                              r2.F_REPRESENTATIVE_COMMENT,
                              r2.F_REPRESENTATIVE_ISACTIVE,
                              r2.F_REPRESENTATIVE_BEGIN,
                              r2.F_REPRESENTATIVE_END
                       FROM REPRESENTATIVE r1
                            LEFT OUTER JOIN ALL_PARENT_DEPARTMENT apd ON apd.PARENT_F_DEPARTMENT_ID = r1.F_DEPARTMENT_ID
                            LEFT OUTER JOIN REPRESENTATIVE r2 ON apd.F_DEPARTMENT_ID = r2.F_DEPARTMENT_ID
                      ) fellow_repr
                        LEFT OUTER JOIN AUTH_USER au ON au.ID = fellow_repr.F_REPRESENTATIVE_USER
                        LEFT OUTER JOIN WORKERS_PROFILE wp ON wp.USER_ID = au.ID
                        LEFT OUTER JOIN DEPARTMENT repr_dep on repr_dep.F_DEPARTMENT_ID = fellow_repr.F_DEPARTMENT_ID
                        LEFT OUTER JOIN ORGANIZATION repr_org on repr_org.F_ORGANIZATION_ID = fellow_repr.F_ORGANIZATION_ID
                      WHERE NOT (fellow_repr.PARENT_F_REPRESENTATIVE_ID = fellow_repr.F_REPRESENTATIVE_ID) AND
                            NOT (fellow_repr.F_REPRESENTATIVE_USER = """ + str(request.user.id) + \
                        """ AND fellow_repr.F_REPRESENTATIVE_USER IS NOT NULL)
                            AND fellow_repr.PARENT_F_REPRESENTATIVE_ID IN """ + representatives_ids_string_list + repr_sorting_condition
    cursor.execute(sql_repr_str)
    fellow_representatives_sql = []
    for row in cursor.fetchall():
        if row[4] is not None:
            workerships = row[4].split("||")
        else:
            workerships = ["\\\\"]
        fellow_representatives_sql.append(
            {'num': i,
             'id': row[0],
             'name': u"Ответственный",
             'user_profile_dict': {'id': row[1] if row[1] is not None else "",
                                   'name': row[2] if row[1] is not None else "",
                                   'url': row[3] if row[1] is not None else "",
                                   'workership_departmemts_dicts': [{'name': workership.split("\\\\")[1],
                                                                     'url': workership.split("\\\\")[0]}
                                                for workership in workerships]}
                                            if row[1] is not None else "",
             'object_dict': {'name': row[7] if row[7] is not None else row[5] if row[5] is not None else "",
                             'url': row[8] if row[7] is not None else row[6] if row[6] is not None else ""},
             'is_active': row[9],
             'org_level': False if row[7] else True
            })
        i += 1
    selector = """<select id="approvement" name="approvement_sorting">
                    <option value = "-1">Все</option>
                    <option value = "1" selected="selected">Только активные</option>
                    <option value = "0">Только неактивные</option>
                  </select>"""
    if approvement_sorting == -1:
        selector = """<select id="approvement" name="approvement_sorting">
                        <option value = "-1" selected="selected">Все</option>
                        <option value = "1">Только активные</option>
                        <option value = "0">Только неактивные</option>
                      </select>"""
    if approvement_sorting == 0:
        selector = """<select id="approvement" name="approvement_sorting">
                        <option value = "-1">Все</option>
                        <option value = "1">Только активные</option>
                        <option value = "0" selected="selected">Только неактивные</option>
                      </select>"""
    return render(request, "associates_representative_permissions_list.html",
                  {
                    'selector': selector,
                    'authority_permissions_custom_dict_objects': authority_permissions_sql,
                    'fellow_representatives_custom_dict_objects': fellow_representatives_sql,
                  }, context_instance=RequestContext(request))

@transaction.commit_on_success()
@create_revision()
def change_approve_status(request):
    '''This function is for instant change the status of permission or representative via list pages'''
    if request.method == 'POST':
        id_num = request.POST.get('id_num', None)
        key = int(id_num)
        if request.POST.get('value', None) == '1':
            approved = True
        else:
            approved = False
        if request.POST.get('type_value', None) == 'permission':
            obj = PermissionWithLabel.objects.get(id=key)
            obj.approved = approved
            if obj.approved:
                obj.date_approved = datetime.now()
        else:
            if request.POST.get('type_value', None) == 'representative':
                obj = Representative.objects.get(id=key)
                obj.is_active = approved
        obj.save()
        if approved:
            return HttpResponse("Да")
        else:
            return HttpResponse("Нет")
    return HttpResponse()


@login_required
@transaction.commit_on_success()
@create_revision()
def user_org_dep_roles_permissions_list_edit(request, auth_user_id, level, auth_object_id):
    '''This function is for editing the list of permissions for fixed User and Department/Organization.'''
    auth_user = get_object_or_404(User, id=auth_user_id)
    if level == 'organization':
        if not request.user.representatives.organization_level():
            messages.warning(request, u'Эта страница доступна только для ответственных по организации. '
                                      u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.')
            return redirect("/home/")
        auth_object = get_object_or_404(Organization, id=auth_object_id)
        org_level = True
        if not request.user.representatives.filter(department__isnull=True, organization=auth_object):
                messages.warning(request, u'Эта страница доступна только для ответственных по организации "' +
                                  unicode(auth_object.name) + u'". ')
    elif level == 'department':
        if not has_permission(request.user, "edit_permissions_and_roles"):
            messages.warning(request, u'Эта страница доступна только для ответственных по подразделению. '
                                      u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.'
                                      u'Если Вы ответственный по организации, Вам требуется специальное разрешение на получение доступа.')
            return redirect("/home/")
        auth_object = get_object_or_404(Department, id=auth_object_id)
        org_level = False
        if not request.user.representatives.filter(Q(department=auth_object) |
                                                  (Q(department__isnull=True) & Q(organization=auth_object.organization))):
            messages.warning(request, u'Эта страница доступна только для ответственных по подразделению "' + unicode(auth_object.name) + u'" ')
            return redirect("/home/")
    else:
        raise Http404
    if request.method == 'POST':
        redirect_url = request.GET.get("redirect_url")
        if request.POST.get("cancel_button"):
            return redirect(redirect_url)
        form = UserRolePermissionDepartmentEditForm(request.user, auth_user, org_level, auth_object, request.POST or None)
        if form.is_valid():
            form.save(request)
            return redirect("/unified_permissions/user/department/list/")
    else:
        redirect_url = request.META.get('HTTP_REFERER')
        form = UserRolePermissionDepartmentEditForm(request.user, auth_user, org_level, auth_object)
    return render(request, "user_org_dep_roles_permissions_list_edit.html",
                  {'form': form, 'auth_user': auth_user, 'auth_object': auth_object, 'redirect_url': redirect_url,
                   'org_level': org_level},
                  context_instance=RequestContext(request))


@login_required
def history_view(request, about_flag=None, username=None):
    '''This functions returns last "versions_num_limit (here it's 100)" actions,
    which the representative (if you define username in the address) or your reprentative associates
    have made lately.'''
    def get_russian_version_comment(local_version):
        '''This function makes all comments look the same and translate English ones to Russian'''
        local_repr_cont_type = get_object_or_404(ContentType, model=u'representative', app_label=u'organizations')
        if local_version.content_type == local_repr_cont_type:
            if u"Initial version" in get_version_comment(local_version) or u"Создан" in get_version_comment(local_version):
                return u"Назначен ответственным сотрудником"
            if u"Deleted" in get_version_comment(local_version) or u"Удален" in get_version_comment(local_version):
                return u"Разжалован из ответственных сотрудников"
            if u"Отредактирован" in get_version_comment(local_version):
                return u"Отредактированы параметры ответственного сотрудника"
            if u"Изменен" in get_version_comment(local_version):
                comment = get_version_comment(local_version)
                return u"У ответственного сотрудника был изменен " + comment[8:]
        local_authority_perm_cont_type = get_object_or_404(ContentType, model=u'permission', app_label=u'authority')
        local_perm_cont_type = get_object_or_404(ContentType, model=u'permissionwithlabel', app_label=u'unified_permissions')
        if local_version.content_type == local_authority_perm_cont_type or local_version.content_type == local_perm_cont_type:
            if u"Initial version" in get_version_comment(local_version) or u"Создан" in get_version_comment(local_version):
                return u"Выдано разрешение"
            if u"Deleted" in get_version_comment(local_version) or u"Удален" in get_version_comment(local_version):
                return u"Удалено разрешение "
            if u"Отредактирован" in get_version_comment(local_version):
                return u"Отредактированы параметры выданного разрешения"
            if u"Изменен" in get_version_comment(local_version):
                comment = get_version_comment(local_version)
                return u"Были изменены параметры " + comment[8:-1] + u" у разрешения"
        if u"Deleted" in get_version_comment(local_version):
            return u"Удален"
        if u"Initial version" in get_version_comment(local_version):
            return u"Инициализирован"
        return get_version_comment(local_version)

    if not has_permission(request.user, "edit_permissions_and_roles"):
        messages.warning(request, u'Эта страница доступна только для ответственных по подразделению. '
                                  u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.'
                                  u'Если Вы ответственный по организации, Вам требуется специальное разрешение на получение доступа.')
        return redirect("/home/")
    versions_num_limit = 100
    repr_cont_type = get_object_or_404(ContentType, model=u'representative', app_label=u'organizations')
    authority_perm_cont_type = get_object_or_404(ContentType, model=u'permission', app_label=u'authority')
    perm_cont_type = get_object_or_404(ContentType, model=u'permissionwithlabel', app_label=u'unified_permissions')
    label_content_type = get_object_or_404(ContentType, model=u'permissionlabel', app_label=u'unified_permissions')
    objects_versions = Version.objects.filter(Q(content_type=repr_cont_type) | Q(content_type=authority_perm_cont_type))
    permissionwithlabel_versions = Version.objects.filter(content_type=perm_cont_type)
    label_versions = Version.objects.filter(content_type=label_content_type, serialized_data__contains='"rus_info"')
    user = None
    if username is not None:
        user = get_object_or_404(User, username=username)
        if about_flag is not None:
            if about_flag == "about":
                objects_versions = objects_versions.filter(serialized_data__contains='"user": ' + str(user.id) + ', ')
            else:
                raise Http404
        else:
            objects_versions = objects_versions.filter(revision__user=user)
    else:
        cursor = connection.cursor()
        sql_fellow_repr_user_str = """
        SELECT associate_user FROM
            (SELECT r1.F_REPRESENTATIVE_USER as representative_user,
                    r2.F_REPRESENTATIVE_USER as associate_user
            FROM
              REPRESENTATIVE r1
              LEFT OUTER JOIN ALL_PARENT_DEPARTMENT apd on r1.F_DEPARTMENT_ID = apd.PARENT_F_DEPARTMENT_ID
              LEFT OUTER JOIN REPRESENTATIVE r2 on apd.F_DEPARTMENT_ID = r2.F_DEPARTMENT_ID
            WHERE r1.F_DEPARTMENT_ID IS NOT NULL /*AND
                  r1.F_REPRESENTATIVE_ID <> r2.F_REPRESENTATIVE_ID*/
            UNION
            SELECT r1.F_REPRESENTATIVE_USER as representative_user,
                   r2.F_REPRESENTATIVE_USER as associate_user
            FROM
              REPRESENTATIVE r1
              LEFT OUTER JOIN REPRESENTATIVE r2 on r2.F_ORGANIZATION_ID = r2.F_ORGANIZATION_ID
            WHERE r1.F_DEPARTMENT_ID IS NULL /*AND
                  r1.F_REPRESENTATIVE_ID <> r2.F_REPRESENTATIVE_ID*/) repr_assoc
        WHERE repr_assoc.associate_user IS NOT NULL AND
              repr_assoc.representative_user = """ + str(request.user.id)
        cursor.execute(sql_fellow_repr_user_str)
        objects_versions = objects_versions.filter(revision__user__in=[row[0] for row in cursor.fetchall()])
    objects_action_list = []
    for version in objects_versions.order_by('-revision__date_created')[:versions_num_limit].select_related("revision__user", "content_type").prefetch_related("object__user"):
        permissionwithlabel_version = None
        comment = get_russian_version_comment(version)
        if version.content_type == authority_perm_cont_type:
            auth_perm_id = json.loads(version.serialized_data)[0]["pk"]
            try:
                permissionwithlabel_version = permissionwithlabel_versions.get(revision=version.revision, serialized_data__contains='"auth_perm": ' + str(auth_perm_id) + ', ')
                comment = get_russian_version_comment(permissionwithlabel_version)
                label_id = json.loads(permissionwithlabel_version.serialized_data)[0][u"fields"][u"label"]
                label_rus_infos = [json.loads(label_version.serialized_data)[0][u"fields"][u"rus_info"] for label_version in label_versions.filter(serialized_data__contains='"pk": ' + str(label_id) + ', ').order_by('-revision__date_created')]
                if len(label_rus_infos) > 0:
                    label_rus_info = label_rus_infos[0]
                else:
                    try:
                        label_rus_info = PermissionLabel.objects.get(id=label_id).rus_info
                    except ObjectDoesNotExist:
                        label_rus_info = json.loads(version.serialized_data)[0][u"fields"][u"codename"]
            except ObjectDoesNotExist:
                label_rus_info = json.loads(version.serialized_data)[0][u"fields"][u"codename"]
        action = {
                "revision": version.revision,
                "permission_russian_codename": label_rus_info if permissionwithlabel_version is not None else "",
                "object": version.object or version.object_repr,
                "object_target": version.object.content_object if version.object and hasattr(version.object, "content_object") else "",
                "comment": comment,
                "model": getattr(version.object, 'verbose_name', None) or version.content_type,
                "profile_url": get_profile_url(version.revision.user) if version.revision.user else None,
            }
        objects_action_list.append(action)
    context = {"object_action_list": objects_action_list, "about_flag": about_flag, "analized_user": user}
    return render_to_response(request, 'representative_permissions_actions_history.html', context)


@login_required
@user_passes_test(lambda u: u.username == "Vladimir_Bukhonov")
def create_representative_department_report(request):
    #if you need that function, run "pip install docx" command first
    '''This function is for building docx document, containing the list of department sections,
    each having a table with the list of representatives for this department'''
    cursor = connection.cursor()
    sql_dep_hierarchy_str = """
        select d.f_department_id as f_department_id,
               level
        from (select f_department_id, dep_f_department_id from departmentlink
             union all
              select f_department_id, NULL from department d2 where not exists
                  (select 1 from departmentlink dl2 where dl2.f_department_id=d2.f_department_id)
              ) dl, department d
        where d.f_department_id = dl.f_department_id
              and d.f_organization_id = 214524
        start with dl.dep_f_department_id is null
                          connect by PRIOR dl.f_department_id = dl.dep_f_department_id
                          order siblings by d.f_department_name
        """
    cursor.execute(sql_dep_hierarchy_str)
    from docx import Document
    document = Document()
    document.add_heading(u"Ответственные", 0)
    for id_elem in cursor.fetchall():
        department = Department.objects.get(id=int(id_elem[0]))
        if int(id_elem[1]) == 1 and (u"Факультет" in department.name or u"факультет" in department.name):
            document.add_heading(department.name, level=int(id_elem[1]))
            if department.representatives.exists():
                table = document.add_table(rows=1, cols=6)
                table.style = 'TableGrid'
                hdr_cells = table.rows[0].cells
                hdr_cells[0].text = u"№"
                hdr_cells[1].text = u"ФИО"
                hdr_cells[2].text = u"Место работы"
                hdr_cells[3].text = u"Должность"
                hdr_cells[4].text = u"Сфера компетенции"
                hdr_cells[5].text = u"E-mail"
                i = 1
                for representative in department.representatives.all():
                    if representative.user:
                        row_cells = table.add_row().cells
                        row_cells[0].text = unicode(i)
                        i += 1
                        if representative.user.get_profile():
                            if representative.user.get_profile().worker:
                                row_cells[1].text = representative.user.get_profile().lastname + u" " +\
                                                    representative.user.get_profile().firstname + u" " +\
                                                    representative.user.get_profile().middlename
                                row_cells[2].text = u""
                                for employment in representative.user.get_profile().worker.current_employments.all():
                                    row_cells[2].text += employment.department.name + u"; "
                                row_cells[2].text = row_cells[2].text[:-2]
                        if representative.post:
                            row_cells[3].text = representative.post
                        else:
                            row_cells[3].text = u""
                        if representative.scope:
                            row_cells[4].text = representative.scope
                        else:
                            row_cells[4].text = u""
                        if representative.email:
                            row_cells[5].text = representative.email
                        else:
                            if representative.user.email:
                                row_cells[5].text = representative.user.email + u" (п.)"
                            else:
                                row_cells[5].text = u""
                document.add_page_break()
    document.save("representatives_faculties.docx")
    return HttpResponse("Success")
