# -*- coding: utf-8; -*-
__author__ = 'Vladimir'
from django.conf.urls.defaults import *
from unified_permissions.views import (index, associates_repr_permissions_list, change_approve_status,
                                       user_role_permission_add, user_role_permission_edit,
                                       user_org_dep_roles_permissions_list_edit, history_view,
                                       create_representative_department_report,
                                       )

urlpatterns = patterns('unified_permissions.views',
                       url(r'^$', index, name="index_page"),

                       url(r'^role_permission/(?P<level>\w+)/add/$', user_role_permission_add,
                           name="user_role_permission_add"),
                       url(r'^role_permission/(?P<level>\w+)/(?P<role_permission_id>\d+)/edit/$', user_role_permission_edit,
                           name="user_role_permission_edit"),

                       url(r'^user/department/list/$', associates_repr_permissions_list,
                           name="associates_repr_permissions_list"),
                       url(r'^user/(?P<auth_user_id>\d+)/(?P<level>\w+)/(?P<auth_object_id>\d+)/edit/$',
                           user_org_dep_roles_permissions_list_edit, name="user_org_dep_roles_permissions_list_edit"),

                       url(r'^change_approve_status/$', change_approve_status,
                           name="change_approve_status"),
                       url(r'^history/$', history_view,
                           name="unified_permissions_your_history"),
                       url(r'^history/(?P<username>\w+)/$', history_view,
                           name="unified_permissions_user_history"),
                       url(r'^history/(?P<about_flag>\w+)/(?P<username>\w+)/$', history_view,
                           name="unified_permissions_history_about_user"),

                       url(r'^report/$', create_representative_department_report,
                           name="unified_permissions_report"),
                      )
