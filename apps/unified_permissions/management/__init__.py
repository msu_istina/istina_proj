from django.db.models.signals import post_syncdb
import inspect
from importlib import import_module
from unified_permissions.models import PermissionLabel

operations_found = [] # not to check them multiple times

def create_permission_labels(sender, **kwargs):
    # sender is e.g. apps.publications.models
    permissions_module_name = ".".join(sender.__name__.split(".")[:-1] + ['permissions'])
    try:
        permissions_module = import_module(permissions_module_name)
    except ImportError:
        pass
    else:
        classes = inspect.getmembers(permissions_module, inspect.isclass)
        classes = filter(lambda (class_name, cls): class_name.endswith("Permission"), classes)
        for class_name, cls in classes:
            try:
                operations = set(cls.operations) - set(operations_found)
            except AttributeError:
                pass
            else:
                for name, rus_info, is_role in operations:
                    permission_label, created = PermissionLabel.objects.get_or_create(name=name)
                    if created:
                        permission_label.rus_info = rus_info
                        permission_label.is_role = is_role
                        permission_label.save()
                        print u"PermissionLabel created: %s, %s, is_role: %s" % (name, rus_info, is_role)
                operations_found.extend(operations)

post_syncdb.connect(create_permission_labels)