# -*- coding: utf-8; -*-
__author__ = 'Vladimir'

from common.utils.admin import register_with_versions, MyModelAdmin, make_RelatedOnlyFieldListFilter
from unified_permissions.models import PermissionLabel, PermissionLabelRelation, PermissionLabelForContentType, PermissionWithLabel
from django.contrib import admin
import authority.admin # so that it is registered before the current file
from authority.models import Permission as AuthorityPermission

admin.site.unregister(AuthorityPermission) # we don't want to clutter admin because we have our wonderful PermissionWithLabel subclassing AuthorityPermission

class PermissionWithLabelAdmin(MyModelAdmin):
    '''Admin class representing PermissionWithLabel class.'''
    fieldsets = (
        (None, {
            'fields': ('user', 'label', 'content_type', 'object_id', 'approved')
        }),
        ('Timestamps', {
            'fields': ('date_requested', 'date_approved', 'startdate', 'enddate', 'creator')
        }),
    )
    exclude = ('codename', 'group')
    list_display = ('user', 'label_name', 'label', 'content_type', 'content_object', 'object_id', 'approved')
    list_filter = (
		make_RelatedOnlyFieldListFilter("label", u"Метка"),
		make_RelatedOnlyFieldListFilter("content_type", u"Тип объекта"),
		'approved'
	)

    def label_name(self, obj):
    	return obj.label.name
    label_name.short_description = u"Код метки"

register_with_versions(PermissionWithLabel, PermissionWithLabelAdmin)


class PermissionLabelRelationAdmin(MyModelAdmin):
    '''Admin class representing PermissionLabelRelation class.'''
    list_display = ('parent', 'child')

register_with_versions(PermissionLabelRelation, PermissionLabelRelationAdmin)


class PermissionLabelForContentTypeAdmin(MyModelAdmin):
    '''Admin class representing PermissionLabelForContentType class.'''
    list_display = ('label', 'cont_type')

register_with_versions(PermissionLabelForContentType, PermissionLabelForContentTypeAdmin)


class PermissionLabelAdmin(MyModelAdmin):
    '''Admin class representing PermissionLabel class.'''
    list_display = ('name', 'rus_info', 'is_role')
    list_filter = ('is_role',)

register_with_versions(PermissionLabel, PermissionLabelAdmin)

