# -*- coding: utf-8; -*-
__author__ = 'Vladimir'

from importlib import import_module
import re
from unified_permissions.models import PermissionLabel
from django.contrib.contenttypes.models import ContentType
import sys
import settings
import logging

logger = logging.getLogger('sentry_debug')

def get_permission_class(obj=None):
    '''Returns the name of a default permission class for current object'''
    def _get_permission_class(obj_class):
        # e.g. self is DissertationConcil
        # here self.__class__ = *full name of class* = dissertation_councils.models.DissertationCouncil;
        # here self.__class__.__module__ = *full module name* = dissertation_councils.models
        str_search = re.search('(.)*\.', obj_class.__module__)
        # here package_name = *full package name* = dissertation_councils
        package_name = str_search.group(0)[:-1]
        try:
            permissions_module = import_module(package_name + '.permissions')
            return getattr(permissions_module, obj_class.__name__ + "Permission")
        except (ImportError, AttributeError):
            return _get_permission_class(obj_class.__bases__[0])
    if obj is None:
        return getattr(import_module("common.permissions"), "AbstractPermission")
    return _get_permission_class(obj.__class__)


def has_permission(user, operation, obj=None):
    """Checks if user has a right to fulfill the operation."""
    try:
        args = [obj] if obj is not None else []
        perm_class_instance = get_permission_class(*args)(user)
        preemptive_check = getattr(perm_class_instance, "preemptive_check", lambda a, b=None: None)(operation, *args)
        if preemptive_check is not None:
            return preemptive_check
        return getattr(perm_class_instance, "check_" + operation, lambda a=None: False)(*args) \
            or getattr(perm_class_instance, "unified_check", lambda a, b=None: False)(operation, *args)
    except PermissionLabel.DoesNotExist as e:
        if settings.DEBUG:
            raise
        logger.error("PermissionLabel %s Does not Exist" % (operation,), exc_info=sys.exc_info(), extra={'caught_exception': e})
    # Exception occured; return False by default
    return False

def has_permission_to_department(user, operation, department):
    """Checks if user has a right to fulfill the operation in specific department."""
    return has_permission(user, operation, department)


def departments_ids_user_has_permission_in(user, operation):
    """Returns the set of departments' ids where USER has right to perform OPERATION.
    The 'ancestors' of the OPERATION are also taken into consideration.
    If user has right to perform OPERATION for whole organization,
    it means that he has this right for every department of this organization.
    Child departments are also included in final list."""
    permission_label = PermissionLabel.objects.get(name=operation)
    content_type_dep = ContentType.objects.get(model=u"department", app_label=u"organizations")
    content_type_org = ContentType.objects.get(model=u"organization", app_label=u"organizations")
    full_labels_list = permission_label.get_ancestors() + [permission_label]
    labels_names = [label.name for label in full_labels_list]
    from organizations.models import Department, Organization
    departments_ids = set()
    if "is_representative_for" in labels_names:
        for representative in user.representatives.filter(is_active=True):
            if representative.department:
                departments_ids |= set(representative.department.get_descendants_queryset(include_self=True).values_list('id', flat=True))
            else:
                departments_ids |= set(representative.organization.departments.all().values_list('id', flat=True))
    dep_permissions_object_ids_list = user.granted_permissions.filter(content_type=content_type_dep, approved=True,
                                                                      label_link__label__in=full_labels_list).\
        values_list('object_id', flat=True)
    for permission_object_id in dep_permissions_object_ids_list:
        departments_ids |= set(Department.objects.get(pk=permission_object_id).get_descendants_queryset(include_self=True).
                               values_list('id', flat=True))
    departments_ids |= set(Department.objects.filter(organization__in=
                                                     Organization.objects.filter(pk__in=
                                                                                 user.granted_permissions.filter(content_type=content_type_org,
                                                                                                                 approved=True,
                                                                                                                 label_link__label__in=full_labels_list).
                                                                                 values_list('object_id', flat=True))).
                           values_list('id', flat=True))
    return departments_ids


def departments_queryset_user_has_permission_in(user, operation):
    """Returns the list of departments' ids where USER has right to perform OPERATION.
    The 'ancestors' of the OPERATION are also taken into consideration.
    If user has right to perform OPERATION for whole organization,
    it means that he has this right for every department of this organization.
    Child departments are also included in final queryset."""
    from organizations.models import Department
    return Department.objects.filter(pk__in=departments_ids_user_has_permission_in(user, operation))
