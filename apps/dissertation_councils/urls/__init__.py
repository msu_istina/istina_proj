# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *

from common.views import autocomplete_search
from dissertation_councils.models import DissertationCouncil, DissertationCouncilMembership
from dissertation_councils.views import DissertationCouncilMembershipWizardView, DissertationCouncilWizardView

membership_options_base = {'model': DissertationCouncilMembership}
membership_options_detail = dict(membership_options_base.items() + [('template_name', 'dissertation_councils/membership_detail.html')])

council_options_base = {'model': DissertationCouncil}
council_options_detail = dict(council_options_base.items() + [('template_name', 'dissertation_councils/council_detail.html')])

urlpatterns = patterns('common.views',
    url(r'^memberships/add/$', DissertationCouncilMembershipWizardView.as_view(), name='dissertation_councils_membership_add'),
    url(r'^memberships/(?P<object_id>\d+)/$', 'detail', membership_options_detail, name='dissertation_councils_membership_detail'),
    url(r'^memberships/(?P<object_id>\d+)/edit/$', DissertationCouncilMembershipWizardView.as_view(), name='dissertation_councils_membership_edit'),
    url(r'^memberships/(?P<object_id>\d+)/delete/$', 'delete', membership_options_base, name='dissertation_councils_membership_delete'),

    url(r'^councils/(?P<object_id>\d+)/edit/$', DissertationCouncilWizardView.as_view(), name='dissertation_councils_council_edit'),
    url(r'^councils/(?P<object_id>\d+)/delete/$', 'delete', council_options_base, name='dissertation_councils_council_delete'),

    (r'^councils/(?P<pk>\d+)/report/(?P<year>\d{4})/', include('dissertation_councils.urls.report')),

    url(r'^search/$', autocomplete_search, {'model': DissertationCouncil}, name="dissertation_councils_council_search"),
) + patterns('dissertation_councils.views',
	url(r'^by_organization/(?P<organization_id>\d+)/$', 'list_by_organization', name='dissertation_councils_list_by_organization'),
    url(r'^by_organization/(?P<organization_id>\d+)/stats/$', 'councils_stats', name='dissertation_councils_councils_stats'),
    url(r'^councils/(?P<object_id>\d+)/$', 'detail', council_options_detail, name='dissertation_councils_council_detail'),
)
