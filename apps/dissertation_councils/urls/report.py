# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from dissertation_councils.views import DissertationCouncilReportView, DissertationCouncilReportPublicationsView, DissertationCouncilReportDissertationsView

urlpatterns = patterns('',
    url(r'^$', DissertationCouncilReportView.as_view(), name='dissertation_councils_council_report_index'),
    url(r'^card/$', DissertationCouncilReportView.as_view(template_name='dissertation_councils/report/card.html'), name='dissertation_councils_council_report_card'),
    url(r'^members/$', DissertationCouncilReportView.as_view(template_name='dissertation_councils/report/members.html'), name='dissertation_councils_council_report_members'),
    url(r'^publications/$', DissertationCouncilReportPublicationsView.as_view(), name='dissertation_councils_council_report_publications'),
    url(r'^dissertations/$', DissertationCouncilReportDissertationsView.as_view(), name='dissertation_councils_council_report_dissertations'),
)
