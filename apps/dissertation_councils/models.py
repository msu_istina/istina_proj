# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
import datetime
from django.template.defaultfilters import date as date_filter
from common.models import MyModel, LinkedToWorkersModel, WorkershipModel, Activity
from dissertation_councils.managers import DissertationCouncilManager


class DissertationCouncil(LinkedToWorkersModel):
    '''A dissertation council.'''
    id = models.AutoField(primary_key=True, db_column="F_BOARD_ID")
    members = models.ManyToManyField("workers.Worker", related_name="dissertation_councils", through="DissertationCouncilMembership")
    number = models.CharField(u"Шифр совета", max_length=255, db_column="F_BOARD_NUM", unique=True)
    organization_str = models.CharField(u"Организация", max_length=255, db_column="F_BOARD_NAME", blank=True)
    organization = models.ForeignKey(to="organizations.Organization", related_name="dissertation_councils", db_column="F_ORGANIZATION_ID", verbose_name=u"Организация", blank=True, null=True)
    department = models.ForeignKey(to="organizations.Department", related_name="dissertation_councils", db_column="F_DEPARTMENT_ID", verbose_name=u"Подразделение", blank=True, null=True)
    address = models.CharField(u"Адрес совета", max_length=1000, db_column="f_board_address", blank=True)
    place = models.CharField(u"Адрес места заседаний", max_length=1000, db_column="f_board_place", blank=True)
    phone = models.CharField(u"Телефон совета", max_length=1000, db_column="f_board_phone", blank=True)
    email = models.EmailField(u"Электронная почта email", max_length=255, db_column="f_board_email", blank=True)
    xml = models.TextField(u"XML совета", db_column="F_BOARD_XML", blank=True)
    creator = models.ForeignKey(to=User, related_name="dissertation_councils_added", db_column="F_BOARD_USER",
                                null=True, blank=True)
    specialties = models.ManyToManyField("dissertations.Specialty", related_name="councils",
                                         verbose_name=u"Специальности", through="DissertationCouncilSpecialty",
                                         blank=True, null=True)
    approved = models.BooleanField(u"Подтвержден", db_column="F_BOARD_APPROVED", default=False)

    objects = DissertationCouncilManager()

    search_attr = 'number'
    workers_attr = 'members'
    workerships_attr = 'memberships'  # attribute pointing to intermediate model instances
    workers_verbose_name_single = u"Член совета"
    workers_verbose_name_plural = u"Члены совета"
    workers_required_error_msg = u"Укажите хотя бы одного члена совета"

    nominative_en = "dissertation_council"
    nominative_short = u"совет"
    genitive = u"диссертационного совета"
    genitive_short = u"совета"
    accusative_short = u"совет"
    instrumental = u"диссертационным советом"
    locative = u"диссертационном совете"

    class Meta:
        db_table = "BOARD"
        verbose_name = u"диссертационный совет"
        verbose_name_plural = u"диссертационные советы"
        get_latest_by = "-number"
        ordering = ('number',)

    def __unicode__(self):
        return u"Диссертационный совет %s при %s" % (self.number, self.organization_name)

    @models.permalink
    def get_absolute_url(self):
        return ('dissertation_councils_council_detail', (), {'object_id': self.id})

    @property
    def organization_name(self):
        return self.organization and self.organization.name or self.organization_str

    @property
    def organization_name_prepositioned(self):
        return (u"при " + self.organization_name) if self.organization_name else ""

    @property
    def title(self):
        return u"Диссертационный совет %s при %s" % (self.number, self.organization_name)

    @property
    def current_memberships(self):
        memberships_startdate = self.memberships.filter(startdate__lte=datetime.datetime.now())
        memberships = memberships_startdate.filter(Q(enddate__isnull=True) | Q(enddate__gte=datetime.datetime.now()))
        return memberships.select_related('member', 'post')

    @property
    def chiefs(self):
        return self.current_memberships.filter(post__name=u"председатель")

    @property
    def secretaries(self):
        return self.current_memberships.filter(post__name=u"ученый секретарь")

    @property
    def contacts_specified(self):
        fields = map(lambda f: getattr(self, f), ["address", "place", "phone", "email"])
        if len(filter(bool, fields)) == len(fields):
            return 1 # fully specified
        elif len(filter(bool, fields)) > 0:
            return 0.5 # partly specified
        return 0 # not specified

    @property
    def information_specified(self):
        return {
            'members': self.current_memberships.count() >= 19, # 19 members is the minimum set by VAK
            'chiefs': self.chiefs.exists(),
            'secretaries': self.secretaries.exists(),
            'specialties': self.specialties.exists(),
            'contacts': self.contacts_specified == 1,
            'attachments': self.attachments.exists(),
        }

    @property
    def fullness_rate(self):
        parameters = self.information_specified.values()
        return float(len(filter(bool, parameters))) / len(parameters)


class DissertationCouncilMembership(WorkershipModel, Activity):
    id = models.AutoField(primary_key=True, db_column="F_INBOARD_ID")
    member = models.ForeignKey(to="workers.Worker", related_name="dissertation_councils_memberships", db_column="F_MAN_ID", null=True, blank=True)
    dissertation_council = models.ForeignKey(to="DissertationCouncil", related_name="memberships", db_column="F_BOARD_ID")
    post = models.ForeignKey(to="DissertationCouncilPost", related_name="memberships",
                             verbose_name="Должность", db_column="F_INBOARDROLE_ID",
                             blank=True, null=True)
    specialty = models.ForeignKey("dissertations.Specialty", related_name="dissertation_councils_memberships",
                                  verbose_name=u"Специальность в совете", db_column="F_JOURNALRUB_ID", null=True)
    domain = models.ForeignKey("dissertations.Domain", related_name="dissertation_councils_memberships",
                               verbose_name=u"Область знаний", db_column="F_BRANCH_ID", null=True)
    startdate = models.DateField(u"Дата вступления", db_column="F_INBOARD_BEGIN")
    enddate = models.DateField(u"Дата выхода", db_column="F_INBOARD_END", null=True, blank=True)
    original_name = models.CharField(max_length=255, db_column="F_INBOARD_NAME", blank=True)
    creator = models.ForeignKey(to=User, related_name="dissertation_council_memberships_added", db_column="F_INBOARD_USER", null=True, blank=True)

    worker_attr = 'member'
    workers_verbose_name_single = u"ФИО члена совета"
    workers_required_error_msg = u"Укажите члена совета"

    nominative_en = "dissertation_council_membership"
    nominative_short = u"членство в совете"
    nominative_plural = u"членство в диссертационных советах"
    genitive = u"членства в диссертационном совете"
    genitive_short = u"членства в совете"
    genitive_plural = u"членства в диссертационных советах"
    genitive_plural_full = u"членств в диссертационных советах"
    accusative_short = u"членство в совете"
    instrumental = u"членством в диссертационном совете"
    locative = u"членстве в диссертационном совете"
    gender = "neuter"

    class Meta:
        db_table = "INBOARD"
        verbose_name = u"членство в диссертационном совете"
        verbose_name_plural = u"членства в диссертационных советах"
        get_latest_by = "-startdate"
        ordering = ('-post', 'member__lastname', '-startdate', '-enddate')

    def __unicode__(self):
        return u"Членство в диссертационном совете №%s, %s" % (self.dissertation_council.number, self.workers_string)

    @models.permalink
    def get_absolute_url(self):
        return ('dissertation_councils_membership_detail', (), {'object_id': self.id})

    @property
    def title(self):
        return u"%s в совете #%s" % (self.workers_string, self.dissertation_council.number)

    @property
    def show_post(self):
        return unicode(self.post) if self.post.name != u'член совета' else ''

    @property
    def status_text(self):
        if not self.enddate or self.enddate >= datetime.datetime.now():
            text = u"работает по настоящее время"
        else:
            text = u"работал по %s" % date_filter(self.enddate, "j E Y")
        return text

    @property
    def specialty_name(self):
        if not self.specialty:
            return ""
        text = unicode(self.specialty)
        if self.domain:
            text += " (%s)" % self.domain.shortname
        return text


class DissertationCouncilSpecialty(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_boardspecialty_id")
    council = models.ForeignKey(DissertationCouncil, related_name="council_specialties",
                                db_column="F_BOARD_ID", verbose_name=u"Совет")
    specialty = models.ForeignKey("dissertations.Specialty", related_name="council_specialties",
                                  db_column="F_JOURNALRUB_ID", verbose_name=u"Специальность")
    domain = models.ForeignKey("dissertations.Domain", related_name="council_specialties",
                               db_column="F_BRANCH_ID", verbose_name=u"Область знаний")

    class Meta:
        db_table = u'BOARDSPECIALTY'
        verbose_name = u"специальность диссертационного совета"
        verbose_name_plural = u"специальности диссертационных советов"

    def __unicode__(self):
        return u"Специальность %s в совете %s" % (self.specialty.number, self.council.number)

    @property
    def name(self):
        try:
            return unicode(self.specialty) + " (%s)" % self.domain.shortname
        except models.ObjectDoesNotExist:
            return unicode(self.specialty)


class DissertationCouncilPost(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_INBOARDROLE_ID")
    name = models.CharField(u"Должность", max_length=255, db_column="F_INBOARDROLE_NAME", unique=True)

    class Meta:
        db_table = u'INBOARDROLE'
        ordering = ('id',)
        verbose_name = u"должность в диссертационном совете"
        verbose_name_plural = u"должности в диссертационных советах"

    def __unicode__(self):
        return u"%s" % self.name


class DissertationCouncilSite(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_BOARDURL_ID")
    council = models.ForeignKey(DissertationCouncil, related_name="sites",
                                verbose_name=u"Совет", db_column="F_BOARD_ID")
    url = models.URLField(u"Адрес сайта", max_length=1000, db_column="F_BOARDURL_URL")
    enabled = models.BooleanField(u"Сайт доступен", default=True, db_column="F_BOARDURL_ENABLE")

    class Meta:
        db_table = u'BOARDURL'
        ordering = ('council',)
        verbose_name = u"сайт диссертационного совета"
        verbose_name_plural = u"сайты диссертационных советов"

    def __unicode__(self):
        return u"%s: %s" % (self.council, self.url)


from dissertation_councils.admin import *
