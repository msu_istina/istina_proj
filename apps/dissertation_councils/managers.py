# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db.models import Q
from actstream import action
from common.managers import MyManager
from common.models import get_related_fields
from common.utils.list import is_tuple_list
import collections
from itertools import product

class DissertationCouncilManager(MyManager):
    """Model manager for DissertationCouncil model."""

    def find(self, keywords):
        if not is_tuple_list(keywords):
            keywords = [keywords]
        queryset = self
        try:
            main_number = int(keywords[0])
        except ValueError:
            main_number = None
        for keyword in keywords:
            queryset = queryset.filter(number__icontains=keyword)
        councils = sorted(queryset, key=lambda council: council.dissertations.count(), reverse=True)
        if main_number:
            councils = [council for council in councils if u'к' not in council.number.split(str(main_number))[0].lower()] # exclude candidate councils
        return councils

    def merge(self, username, from_ids, to_id, quiet=True):
        '''Merge DissertationCouncil with from_ids to DissertationCouncil with to_id.
            from_id can be a sequence of ids or a single id.
        '''
        # user exists
        user = User.objects.get(username=username)
        if not user.is_superuser:
            raise Exception("user must be superuser")

        # DissertationCouncil exists
        obj_to = self.get(id=to_id)

        if isinstance(from_ids, int):
            from_ids = (from_ids,)

        for from_id in from_ids:

            if from_id == to_id:
                raise Exception("DissertationCouncils must differ")

            # DissertationCouncil exists
            obj_from = self.get(id=from_id)

            if obj_from.approved:
                raise Exception("Can't merge approved DissertationCouncil")

            # move related objects
            obj_from.dissertations.all().update(council=obj_to)
            obj_from.memberships.all().update(dissertation_council=obj_to)
            obj_from.council_specialties.all().update(council=obj_to)
            obj_from.sites.all().update(council=obj_to)

            # move all related objects
            # this is done not to accidentally delete related objects
            # that we forgot to move manually
            # due to cascade delete by default in django
            for related_name, field_name in get_related_fields(obj_from):
                if not quiet:
                    print related_name, field_name,
                try:
                    # e.g. worker_from.reports.all().update(author=worker_to)
                    getattr(obj_from, related_name).all().update(**{field_name: obj_to})
                    if not quiet:
                        print "OK"
                except:
                    # this shouldn't happen, but it happens for manually created tables
                    if not quiet:
                        print "FAIL"

            for field in ["number", "organization", "department", "address", "place", "phone", "email", "xml"]:
                if getattr(obj_from, field) and not getattr(obj_to, field):
                    setattr(obj_to, field, getattr(obj_from, field))
            if not obj_to.organization and not obj_to.organization_str and obj_from.organization_str:
                obj_to.organization_str = obj_from.organization_str

            # log action
            description = u"Удаленный диссертационный совет: %s (id %d). Его данные: \n%s" % (
                obj_from.title, obj_from.id, obj_from.get_xml().decode('utf8'))
            action.send(user,
                verb=u'объединил диссертационные советы',
                action_object=obj_from,
                target=obj_to,
                description=description)

            # delete obj_from
            obj_from.delete()

        # obj_to.approved = True

        # necessary at least to emit a signal to update cache
        # because update() query does not emit any signals.
        obj_to.save()
        # update dissertation cache,
        # import must be here, or there will be problems, see comments somewhere else
        from common.templatetags.activity_description import invalidate_activity_description
        for dissertation in obj_to.dissertations.all():
            invalidate_activity_description(dissertation, silient=False)

    def merge_duplicates(self, username):
        numbers = self.all().values_list('number', flat=True)
        duplicates = [x for x, y in collections.Counter(numbers).items() if y > 1]
        for dup_number in duplicates:
            councils = self.filter(number=dup_number)
            max_dissertation_count = 0
            main_council_id = None
            for council in councils:
                if council.dissertations.count() > max_dissertation_count:
                    max_dissertation_count = council.dissertations.count()
                    main_council_id = council.id
            if not main_council_id:
                main_council_id = councils[0].id
            other_councils_ids = [council.id for council in councils if council.id != main_council_id]
            self.merge(username, other_councils_ids, main_council_id)

    def merge_similar(self, username):
        councils = self.filter(approved=True)
        count = 0
        for council in councils:
            query = Q()
            for char1, char2 in product(['', '.', '-', None], ['', ' ', '-', None]):
                number = council.number
                if char1 is not None:
                    number = number.replace(" ", char1)
                if char2 is not None:
                    number = number.replace(".", char2)
                query |= Q(number__icontains=number)
            duplicates = self.filter(approved=False).filter(query)
            for duplicate in duplicates:
                print duplicate
            duplicates_ids = [dup.id for dup in duplicates]
            if duplicates:
                self.merge(username, duplicates_ids, council.id)
            count += len(duplicates_ids)
        print "%d councils merged" % count
