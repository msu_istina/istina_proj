# -*- coding: utf-8; -*-
from actstream import action
from django import forms
from django.core import serializers
from django.forms.models import inlineformset_factory

from common.forms import MyForm, MyModelForm, WorkershipModelForm
from dissertations.forms import SpecialtyChoiceField, init_council_number_non_standard_numeration, clean_council_number
from dissertation_councils.models import DissertationCouncil, DissertationCouncilMembership, \
    DissertationCouncilSpecialty, DissertationCouncilSite
from organizations.models import Organization
from organizations.forms import DepartmentChoiceField


class DissertationCouncilForm(MyModelForm):
    '''A simple form for dissertation councils.'''
    department = DepartmentChoiceField(label=u"Подразделение", required=False, help_text=u"необязательно")
    fields_order = ["number", "organization_str", "department", "address", "place", "phone", "email"]
    css_classes = [
        ('number', 'wide'),
        ('organization_str', 'wide autocomplete_organization'),
        ('address', 'wide'),
        ('place', 'wide'),
        ('phone', 'wide'),
        ('email', 'wide')
    ]

    class Meta:
        model = DissertationCouncil
        exclude = ('members', 'organization', 'specialties', 'creator', 'xml', 'approved')

    def __init__(self, *args, **kwargs):
        super(DissertationCouncilForm, self).__init__(*args, **kwargs)
        if self.instance_linked(): # set organization
            self.initial['organization_str'] = self.instance.organization_name

    def save(self, request, commit=True):
        council = super(DissertationCouncilForm, self).save(request, commit=False)
        # try to retrieve the organization with this name
        try:
            council.organization = Organization.objects.get(name=council.organization_str)
            council.organization_str = ""
        except:
            council.organization = None
        if commit:
            self.commit_save(request, council)
        return council

DissertationCouncilSpecialtiesFormSet = inlineformset_factory(DissertationCouncil, DissertationCouncilSpecialty)

DissertationCouncilSitesFormSet = inlineformset_factory(DissertationCouncil, DissertationCouncilSite)


class DissertationCouncilOrganizationForm(MyForm):
    organization_str = forms.CharField(label=u"Организация, к которой прикреплен совет", max_length=255)
    department = DepartmentChoiceField(label=u"Подразделение", required=False, help_text=u"необязательно")
    css_classes = [('organization_str', 'wide autocomplete_organization')]


class DissertationCouncilMembershipForm(WorkershipModelForm):
    '''A simple form for dissertation councils membership.'''
    council_number = forms.CharField(label=u"Шифр совета", max_length=255)
    specialty = SpecialtyChoiceField()
    non_standard_numeration = forms.BooleanField(label=u"Нестандартная нумерация совета", required=False)
    fields_order = ["member_str", "council_number", "non_standard_numeration", "post", "specialty", "domain", "startdate", "enddate"]
    css_classes = [('council_number', 'wide autocomplete_dissertation_council'), ('specialty', 'wide')]

    class Meta:
        model = DissertationCouncilMembership
        exclude = ('member', 'dissertation_council', 'creator')

    def __init__(self, *args, **kwargs):
        super(DissertationCouncilMembershipForm, self).__init__(*args, **kwargs)
        init_council_number_non_standard_numeration(self)
        if self.instance_linked(): # initialize council_number field
            self.fields['council_number'].initial = self.instance.dissertation_council.number

    def clean_member_str(self):
        # only one member is allowed
        value = self.cleaned_data['member_str'].strip(", ")
        if "," in value:
            raise forms.ValidationError(u"Пожалуйста, укажите ФИО только одного члена совета. В строке не должно быть запятых.")
        return value

    def clean_council_number(self):
        value = self.cleaned_data['council_number'].strip()
        if not value:
            raise forms.ValidationError("Укажите шифр совета")
        return value

    def clean(self):
        cleaned_data = super(DissertationCouncilMembershipForm, self).clean()
        # check that enddate is greater than the startdate
        enddate = self.cleaned_data.get("enddate")
        if enddate:
            startdate = self.cleaned_data.get("startdate")
            if startdate > enddate:
                raise forms.ValidationError(u"Дата вступления в совет должна предшествовать дате выхода.")
        clean_council_number(self)
        return cleaned_data

    def save(self, request, workers, council, organization_str, department, commit=True):
        membership = super(DissertationCouncilMembershipForm, self).save(request, commit=False)
        council_created = False
        # link to council
        if not council: # create a new council
            properties = {
                'number': self.cleaned_data['council_number'],
                'department': department,
                'creator': request.user
            }
            # try to retrieve the organization with this name
            try:
                properties['organization'] = Organization.objects.get(name=organization_str)
            except:
                properties['organization_str'] = organization_str
            council = DissertationCouncil(**properties)
            council.xml = council.get_xml()
            council.save()
            council_created = True
        membership.dissertation_council = council
        if commit:
            self.commit_save(request, membership, workers)
            if council_created:
                action.send(request.user, verb=u'добавил диссертационный совет', action_object=council, target=membership)
        return membership
