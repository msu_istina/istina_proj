# -*- coding: utf-8; -*-
from common.utils.admin import register_with_versions, MyModelAdmin, make_RelatedOnlyFieldListFilter
from models import DissertationCouncil, DissertationCouncilMembership, \
    DissertationCouncilPost, DissertationCouncilSpecialty, DissertationCouncilSite
from django.shortcuts import redirect


class DissertationCouncilAdmin(MyModelAdmin):
    '''Admin class representing DissertationCouncil class.'''
    list_display = ('number', 'department', 'organization_name', 'approved')
    list_filter = (
        'approved',
        make_RelatedOnlyFieldListFilter("organization", u"Организация"),
        make_RelatedOnlyFieldListFilter("department", u"Подразделение"),
    )

    def changelist_view(self, request, extra_context=None):
        referrer = request.META.get('HTTP_REFERER', '')
        if len(request.GET) == 0 and '?' not in referrer:
            return redirect("%s?approved__exact=1" % request.path)
        return super(DissertationCouncilAdmin, self).changelist_view(request, extra_context=extra_context)

register_with_versions(DissertationCouncil, DissertationCouncilAdmin)

register_with_versions(DissertationCouncilMembership)
register_with_versions(DissertationCouncilPost)
register_with_versions(DissertationCouncilSpecialty)
register_with_versions(DissertationCouncilSite)
