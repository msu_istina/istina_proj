$(function() {

    var wrapper = $('#dissertations_defended > ul');

    $("#dissertations_defended .sort").click(function() {
        $("#dissertations_defended .sort").toggle();
    });

    $("#sort_dissertations_by_author").click(function() {
        wrapper.find('ul.activity').closest('li').sort(function (a, b) {
            return ($(a).find("li:contains('Автор') a").text() > $(b).find("li:contains('Автор') a").text()) ? 1 : -1;
        }).appendTo(wrapper);
    });

    $("#sort_dissertations_by_year").click(function() {
        wrapper.find('ul.activity').closest('li').sort(function (a, b) {
            return (parseInt($(a).find("li").first().find("b").text()) < parseInt($(b).find("li").first().find("b").text())) ? 1 : -1;
        }).appendTo(wrapper);
    });

    $("#filter_dissertations").bind('keyup', function() {
        var query = $(this).val().trim();
        if (query.length) {
            var found = wrapper.find("ul.activity li:icontains('" + query + "')").parent();
            wrapper.find('ul.activity').not(found).hide();
            found.show();
        }
        else {
            wrapper.find('ul.activity').show();
        }
    });

});
