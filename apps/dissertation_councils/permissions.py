# -*- coding: utf-8; -*-
__author__ = 'goldan'
import authority
from common.permissions import MyModelPermission
from dissertation_councils.models import DissertationCouncilMembership
from unified_permissions import has_permission


class DissertationCouncilMembershipPermission(MyModelPermission):
    label = 'dissertation_council_membership_permission'

    def check_edit(self, membership):
        '''Additionally allow editors of the dissertation council to edit the membership.'''
        return super(DissertationCouncilMembershipPermission, self).check_edit(membership) \
            or has_permission(self.user, "edit", membership.dissertation_council)

    def check_delete(self, membership):
        '''Additionally allow editors of the dissertation council to delete the membership.'''
        return super(DissertationCouncilMembershipPermission, self).check_delete(membership) \
            or has_permission(self.user, "edit", membership.dissertation_council)


authority.register(DissertationCouncilMembership, DissertationCouncilMembershipPermission)
