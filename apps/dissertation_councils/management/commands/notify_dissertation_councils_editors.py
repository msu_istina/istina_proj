# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.core.mail import send_mail
from django.template.loader import render_to_string
from common.utils.user import get_fullname
from organizations.models import Representative
from os import path
from settings import ENV_DIR
from time import sleep
from authority.models import Permission
from dissertation_councils.models import DissertationCouncil
from django.contrib.auth.models import User

class Command(BaseCommand):
    def handle(self, *args, **options):
        '''Send a mail to all unapproved dissertation councils editors and approve them.
           Note that the mail will not be sent to approved permission owners.
           So when you add a new permission, do not approve it. Then run this command,
           and the permission will become approved and the mail will be sent.
        '''
        from_email = u"ИСТИНА <info@istina.msu.ru>"
        logfile = open(path.join(ENV_DIR, 'var', 'log', 'notify_dissertation_councils_editors.log'), 'w')
        template = 'dissertation_councils/emails/notify_dissertation_councils_editors.txt'
        creator_name = raw_input("Please enter your username in the system (for logging): ")
        creator = User.objects.get(username=creator_name)

        permissions = Permission.objects.get_for_model(DissertationCouncil).filter(
            codename='edit',
            approved=False
        )
        for permission in permissions:
            council = permission.content_object
            user = permission.user
            subject = u"Информация о назначении редактором диссертационного совета %s в системе Наука-МГУ (ИСТИНА)" % council.number
            name = " ".join(get_fullname(user).split()[1:])
            message = render_to_string(template, {'name': name, 'username': user.username, 'council_number': council.number, 'council_url': council.get_full_url()})

            # Here the mails are sent! Be careful!
            send_mail(subject, message, from_email, [user.email])

            # approve the permission not to send the email for the second time
            permission.approve(creator)

            log_message = u"%s %s\n" % (name, user.email)
            logfile.write(log_message.encode('utf8'))
            print log_message,
            print message
            sleep(1)
        logfile.close()
