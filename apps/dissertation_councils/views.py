# -*- coding: utf-8; -*-
import logging
from collections import Counter
from django.contrib import messages
from django.shortcuts import redirect, render, get_object_or_404
from django.views.generic.detail import DetailView
from django.templatetags.static import static
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from common.views import LinkedToWorkersModelWizardView, MyModelWizardView, detail as common_detail_view
from dissertation_councils.forms import DissertationCouncilMembershipForm, \
    DissertationCouncilOrganizationForm, DissertationCouncilForm, DissertationCouncilSpecialtiesFormSet, \
    DissertationCouncilSitesFormSet
from dissertation_councils.models import DissertationCouncil, DissertationCouncilMembership
from workers.forms import WorkerFormSet
from organizations.models import Organization
from unified_permissions.decorators import permission_required
from unified_permissions import has_permission
from common.utils.templates import get_icon_by_bool, get_icon_by_level

logger = logging.getLogger("dissertation_councils.views")

class DissertationCouncilMembershipWizardView(LinkedToWorkersModelWizardView):
    form_list = [
        (LinkedToWorkersModelWizardView.main_step, DissertationCouncilMembershipForm),
        ('organization', DissertationCouncilOrganizationForm),
        (LinkedToWorkersModelWizardView.workers_steps[0], WorkerFormSet)
    ]
    condition_dict = LinkedToWorkersModelWizardView.make_condition_dict(
        'organization')
    model = DissertationCouncilMembership  # used in templates

    def save_object(self, form_list):
        '''This function is executed during done() method, and used to save main form object.
        Workers objects has been already saved.
        '''
        council = self.get_extra_data('council_instance', DissertationCouncil)
        organization_str = form_list[1].cleaned_data['organization_str'] if not council else ''
        department = form_list[1].cleaned_data['department'] if not council else None
        return super(DissertationCouncilMembershipWizardView, self).save_object(form_list, council, organization_str, department)

    def process_step(self, form):
        if self.steps.current == self.main_step:
            council_number = form.cleaned_data['council_number']
            old_council_number = self.get_extra_data('council_number')
            if council_number != old_council_number: # council_number has changed
                self.set_extra_data('council_number', council_number) # store it to detect changes in number after pressing 'back' button and not make db query if the number has not changed
                try:
                    council = DissertationCouncil.objects.get(number=council_number)
                except:
                    self.include_steps('organization')
                else:
                    self.set_extra_data('council_instance', council)
                    self.skip_steps('organization')
        return super(DissertationCouncilMembershipWizardView, self).process_step(form)


class DissertationCouncilWizardView(MyModelWizardView):
    form_list = [
        (MyModelWizardView.main_step, DissertationCouncilForm),
        ('specialties', DissertationCouncilSpecialtiesFormSet),
        ('sites', DissertationCouncilSitesFormSet),
    ]
    model = DissertationCouncil  # used in templates

    def get_form_kwargs(self, step=None):
        """
        Returns the keyword arguments for instantiating the form
        (or formset) on given step.
        """
        kwargs = super(DissertationCouncilWizardView, self).get_form_kwargs(step)
        if self.edit and step in ['specialties', 'sites']:
            kwargs.update({
                'instance': self.instance_dict[self.main_step],
            })
        return kwargs

    def done(self, form_list, **kwargs):
        # all forms are submitted and validated, now process the data
        # save main object form
        council = form_list[0].save(self.request)
        specialties = form_list[1].save(self.request)
        sites = form_list[2].save(self.request)
        return redirect(council)


def list_by_organization(request, organization_id):
    organization = get_object_or_404(Organization, pk=organization_id)
    context = {
        'organization': organization,
        'councils': organization.dissertation_councils.filter(approved=True)
    }
    return render(request, "dissertation_councils/list_by_organization.html", context)

@permission_required("view_councils_stats", Organization, "organization_id")
def councils_stats(request, organization_id):
    organization = get_object_or_404(Organization, pk=organization_id)
    headers = [u"Шифр", u"Подразделение", u"Общая заполненность страницы", u"% заполненности", u"Количество членов", u"Председатель", u"Ученый секретарь", u"Количество специальностей",
        u"Контакты", u"Документы", u"Предстоящие защиты", u"Приняты к рассмотрению", u"Защищенные диссертации"]
    councils = organization.dissertation_councils.filter(approved=True)
    data = [map(mark_safe, [
            u'<a href="%s" target="_blank">%s</a>' % (c.get_absolute_url(), c.number),
            (u'<a href="%s" target="_blank">%s</a>' % (c.department.get_absolute_url(), c.department)) if c.department else u"",
            get_icon_by_level(c.fullness_rate),
            int(c.fullness_rate*100),
            c.current_memberships.count(),
            get_icon_by_bool(c.chiefs.exists()),
            get_icon_by_bool(c.secretaries.exists()),
            c.specialties.count(),
            get_icon_by_level(c.contacts_specified),
            get_icon_by_bool(c.attachments.exists()),
            c.dissertations.to_defend().count(),
            c.dissertations.to_consider().count(),
            c.dissertations.defended().count(),
        ]) for c in councils]
    fullness_rates = [int(c.fullness_rate*100) for c in councils]
    stats = {
        'good': len([i for i in fullness_rates if i >= 80]),
        'medium': len([i for i in fullness_rates if 30 <= i < 80]),
        'bad': len([i for i in fullness_rates if i < 30]),
        'average': int(sum(fullness_rates) / len(fullness_rates)),
    }
    stats_by_level = sorted(list(Counter(fullness_rates).items()), key=lambda x: x[0], reverse=True)
    return render(request, "dissertation_councils/councils_stats.html",
        {"data": data, "headers": headers, 'stats': stats, 'stats_by_level': stats_by_level, 'organization': organization})


class DissertationCouncilReportView(DetailView):
    model = DissertationCouncil
    context_object_name = "council"
    template_name = 'dissertation_councils/report/index.html'

    @permission_required("edit", DissertationCouncil, "pk")
    def dispatch(self, *args, **kwargs):
        return super(DissertationCouncilReportView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs = super(DissertationCouncilReportView, self).get_context_data(**kwargs)
        kwargs.update({
            'year': self.kwargs['year'],
            'dissertations': self.object.dissertations.filter(status='defended', year=self.kwargs['year'])
        })
        return kwargs


class DissertationCouncilReportPublicationsView(DissertationCouncilReportView):
    template_name = 'dissertation_councils/report/publications.html'

    def get_context_data(self, **kwargs):
        kwargs = super(DissertationCouncilReportPublicationsView, self).get_context_data(**kwargs)
        memberships = self.object.memberships.all()
        for membership in memberships:
            membership.publications = membership.member.publications_for_dissertation_council([kwargs['year']])
        kwargs['memberships'] = memberships
        return kwargs


class DissertationCouncilReportDissertationsView(DissertationCouncilReportView):
    template_name = 'dissertation_councils/report/dissertations.html'

    def get_context_data(self, **kwargs):
        kwargs = super(DissertationCouncilReportDissertationsView, self).get_context_data(**kwargs)
        council = self.object
        get_queryset = lambda phd: council.dissertations.filter(year=self.kwargs['year'], phd=phd)
        tables = [
            {
                'title': u"Данные о рассмотренных диссертациях на соискание ученой степени доктора наук",
                'queryset': get_queryset('N')
            },
            {
                'title': u"Данные о рассмотренных диссертациях на соискание ученой степени кандидата наук",
                'queryset': get_queryset('Y')
            },
        ]
        filter_qs = lambda queryset, specialty, status: queryset.filter(specialty=specialty.specialty, domain=specialty.domain, status=status)
        all_count = lambda queryset, specialty, status: filter_qs(queryset, specialty, status).count()
        external_count = lambda queryset, specialty, status: len([dis for dis in filter_qs(queryset, specialty, status) if not dis.is_done_in_council_organization])
        for table in tables:
            queryset = table['queryset']
            table['specialties'] = []
            for specialty in council.council_specialties.all():
                table['specialties'].append({
                    'number': specialty.specialty.number,
                    'domain': specialty.domain,
                    'positive': all_count(queryset, specialty, 'defended'),
                    'positive_external': external_count(queryset, specialty, 'defended'),
                    'negative': all_count(queryset, specialty, 'defence_failed'),
                    'negative_external': external_count(queryset, specialty, 'defence_failed')
                })
        kwargs['tables'] = tables
        return kwargs


def detail(request, object_id, model, template_name):
    council = get_object_or_404(model, pk=object_id)
    if has_permission(request.user, "edit", council):
        information_specified = council.information_specified
        help_messages = (
            ('members', u'Добавьте <a href="%s" target="_blank">информацию о членах</a> совета.' % (reverse('dissertation_councils_membership_add'))),
            ('chiefs', u'Укажите <a href="%s" target="_blank">председателя</a> совета.' % (reverse('dissertation_councils_membership_add'))),
            ('secretaries', u'Укажите <a href="%s" target="_blank">ученого секретаря</a> совета.' % (reverse('dissertation_councils_membership_add'))),
            ('contacts', u'Укажите <a href="%s">контактную информацию</a> совета.' % (reverse('dissertation_councils_council_edit', kwargs={'object_id': object_id}))),
            ('specialties', u'Укажите <a href="%s">специальности</a> совета.' % (reverse('dissertation_councils_council_edit', kwargs={'object_id': object_id}))),
            ('attachments', u'Прикрепите <a class="show_attachment_edit" href="#attachment_container">документы</a> совета.')
        )
        all_specified = True
        for key, message in help_messages:
            if not information_specified[key]:
                messages.info(request, message)
                all_specified = False
        if all_specified and not request.session.get('council_%d_all_specified' % council.id):
            messages.success(request, u"Спасибо, информация о диссертационном совете указана в полном объеме.")
            request.session['council_%d_all_specified' % council.id] = True
    return common_detail_view(request, object_id, model, template_name)


