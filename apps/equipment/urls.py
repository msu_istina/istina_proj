# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from django.views.generic.base import RedirectView
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from common.dynatree_widgets import dynatree_widget_helper
from journals.models import JournalRubric, JournalCategorization
from organizations.models import Organization
from models import EquipmentType

#tree_data_editor_params = {
#    'model': EquipmentType,
#    'field_name_to_set': 'name',
#    'view_name': 'equipment_types',
#    'page_title': u'Редактирование типов оборудования',
#    'go_back_url': '/equipment/', #reverse('equipment_index'),
#    'go_back_label': u'Вернуться к списку оборудования'
#    }

# Use hardcoded ids because Django can not generate correct query from
# complex_types_qs = EquipmentType.objects.filter(parent__isnull=True, name__startswith=u'Комплексы')
# 6515460 is a key from production database, 10000257 - from development one.
complex_types_qs = EquipmentType.objects.filter(Q(parent__isnull=True), Q(pk=6515460)|Q(pk=10000257))
device_types_qs = EquipmentType.objects.filter(Q(parent__isnull=True), ~Q(pk=6515460), ~Q(pk=10000257))

def rub_roots_sqlgen(request):
    class EmptyCode:
        pass

    try:
        code = request.GET.get('rubcode')
        if not code:
            raise EmptyCode
        dummy = JournalCategorization.objects.get(code=code)
        return '''
        select f_journalrub_id
        from journalrub jr, journalrubtype jrt
        where jr.f_journalrubtype_id = jrt.f_journalrubtype_id
            and jrt.f_journalrubtype_code = '%s'
            and jr.jou_f_journalrub_id is null
        ''' % code
    except (JournalCategorization.DoesNotExist, EmptyCode, JournalCategorization.MultipleObjectsReturned):
        return '''select 1 from dual where 1=0'''

urlpatterns = patterns(
    'equipment.views',

    # public pages
    # FIXME: remove this after making equipment public or write a redirect by user' organization
    url(r'^$', RedirectView.as_view(url=reverse_lazy('equipment_by_organization', kwargs={'organization_id': '214524'}), permanent=True), name='equipment_index'),
    url(r'^by_organization/(?P<organization_id>\d+)/$', 'index', name='equipment_by_organization'),
    url(r'^by_organization/(?P<organization_id>\d+)/report/$', 'report_equipment_monitoring', name='equipment_monitoring_report'),
    url(r'^by_department/(?P<department_id>\d+)/report/$', 'report_equipment_department', name='equipment_department_report'),
    url(r'^by_department/(?P<department_id>\d+)/citations/$', 'report_equipment_citations', name='equipment_department_citations'),

    url(r'^card/(?P<equip_id>\d+)/$', 'card', name='equipment_card'),
    url(r'^results/(?P<equip_id>\d+)/$', 'results', name='add_results'),
    url(r'^results/unlink/(?P<equip_id>\d+)/(?P<result_id>\d+)/$', 'unlink_result', name='equipment_unlink_results'),
    url(r'^doc/(?P<equip_id>\d+)/(?P<doc_id>\d+)/$', 'doc', name='equipment_doc'),
    url(r'^add/part/(?P<equip_id>\d+)/$', 'add_part', name='equipment_add_part'),
    #
    url(r'^add/$', 'add_card', name='equipment_add'),
    url(r'^edit/$', 'edit_card', name='equipment_edit'),
    url(r'^edit/(?P<equip_id>\d+)/$', 'edit_card', name='equipment_edit'),
    url(r'^edit/(?P<equip_id>\d+)/classifiers/$', 'edit_classifiers', name='equipment_edit_classifiers'),
    url(r'^edit/(?P<equip_id>\d+)/staff/$', 'edit_staff', name='equipment_edit_staff'),
    url(r'^edit/(?P<equip_id>\d+)/staff/unlink/(?P<staff_id>\d+)/$', 'unlink_staff', name='equipment_unlink_staff'),
    url(r'^edit/(?P<equip_id>\d+)/docs/$', 'edit_docs', name='equipment_edit_docs'),
    url(r'^edit/(?P<equip_id>\d+)/docs/(?P<doc_id>\d+)/remove/$', 'remove_doc', name='equipment_remove_doc'),
    url(r'^edit/(?P<equip_id>\d+)/results/$', 'edit_results', name='equipment_edit_results'),
    url(r'^edit/(?P<equip_id>\d+)/techdata/$', 'edit_techdata', name='equipment_edit_techdata'),
    url(r'^edit/(?P<equip_id>\d+)/parts/$', 'edit_parts', name='equipment_edit_parts'),
    url(r'^delete/(?P<equip_id>\d+)/$', 'equipment_delete', name='equipment_delete'),
    url(r'^remove_from_complex/(?P<complex_id>\d+)/(?P<equip_id>\d+)/(?P<link_id>\d+)/$', 'remove_from_complex', name='equipment_remove_from_complex'),
    # dynatree support urls
    url(r'^xdr_equipment_types/$', dynatree_widget_helper, {'model': EquipmentType, 'title_fieldname': 'name', 'toplevel_qs': device_types_qs}, name='xdr_equipment_types'),
    url(r'^xdr_equipment_types_complex/$', dynatree_widget_helper, {'model': EquipmentType, 'title_fieldname': 'name', 'toplevel_qs': complex_types_qs}, name='xdr_equipment_types_complex'),
    url(r'^xdr_equipment_rubs/$', dynatree_widget_helper, {'model': JournalRubric, 'title_fieldname': 'name', 'toplevel_sqlgen': rub_roots_sqlgen}, name='xdr_equipment_rubs'),
    #
    # admin functions
    url(r'^edit_types/by-organization/(?P<organization_id>\d+)$', 'tree_editor_wrapper', name='equipment_types'),
    url(r'^edit_types/by-organization/(?P<organization_id>\d+)/(?P<item_id>\d+)/$', 'tree_editor_wrapper', name='equipment_types'),
    url(r'^edit_types/by-organization/(?P<organization_id>\d+)/(?P<item_id>\d+)/(?P<operation>\w+)/$', 'tree_editor_wrapper',  name='equipment_types'),

    # TODO: organization-based type classifier
    url(r'^edit_types/$', 'tree_editor_wrapper', name='equipment_types_msu'),
    url(r'^edit_types/(?P<item_id>\d+)/$', 'tree_editor_wrapper', name='equipment_types_msu'),
    url(r'^edit_types/(?P<item_id>\d+)/(?P<operation>\w+)/$', 'tree_editor_wrapper', name='equipment_types_msu'),

    url(r'^staff_types/(?P<organization_id>\d+)/$', 'staff_types', name='equipment_staff_types' ),
    url(r'^staff_types/(?P<organization_id>\d+)/(?P<id>\d+)/$', 'edit_staff_type', name='equipment_edit_staff_type'),

    url(r'^responsible/(?P<organization_id>\d+)/$', 'responsible', name='equipment_responsible'),
    url(r'^responsible/(?P<organization_id>\d+)/add/$', 'responsible_add', name='equipment_responsible_add'),
    url(r'^responsible/(?P<organization_id>\d+)/email/$', 'responsible_email', name='equipment_responsible_email'),

    url(r'^edit/(?P<equip_id>\d+)/sign/$', 'equipment_sign', name='equipment_sign'),
    url(r'^results/(?P<act_id>\d+)/sign/$', 'activity_sign',
    name='equipment_activity_sign'),

    url(r'^report/(?P<organization_id>\d+)/$', 'report_equipment', name='report_equipment'),

    # Temporary
    url(r'^edit/(?P<equip_id>\d+)/own/$', 'equipment_own', name='equipment_own'),
    url(r'^stats/$', 'stats', name='equipment_stats'),
)
