# -*- coding: utf-8 -*-

from organizations.models import Department
from django.contrib.auth.models import User

from django.db import models, connection
from workers.models import Worker
from django.core.validators import MaxLengthValidator
from common.utils.files import validate_uploaded_file

from common.models import MyModel, STATUS_CHOICES
from publications.models import Article, Book
from journals.models import JournalRubric

from managers import EquipmentIndexEntryManager

import imghdr
import os

class EquipmentType(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEVICETYPE_ID")
    name = models.CharField(u"Название типа", max_length=255, db_column="F_DEVICETYPE_NAME")
    parent = models.ForeignKey("EquipmentType", related_name="subtypes", db_column="DEV_F_DEVICETYPE_ID", null=True, blank=True)

    def __unicode__(self):
        return self.name

    def get_fullname(self):
        name = self.name
        if self.parent:
            name = self.parent.get_fullname() + ' / ' + name
        return name

    class Meta:
        db_table = "DEVICETYPE"
        verbose_name = u"Тип оборудования"
        verbose_name_plural = u"Типы оборудования"

class Equipment(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEVICE_ID")
    creator = models.ForeignKey(to=User, related_name="devices_added", db_column="F_DEVICE_USER", null=True, blank=True)
    fullname = models.CharField(u"Название (полное)", max_length=255, db_column="F_DEVICE_FULLNAME")
    shortname = models.CharField(u"Название (краткое)", max_length=255, db_column="F_DEVICE_SHORTNAME", null=True, blank=True)
    equiptype = models.ForeignKey(to=EquipmentType, db_column="F_DEVICETYPE_ID", related_name="equipment", verbose_name=u"Тип")
    regnumber = models.CharField(u"Регистрационный (инвентарный) номер", max_length=255, db_column="F_DEVICE_REGNUMBER", null=True, blank=True)
    number = models.CharField(u"Серийный (заводской) номер", max_length=255, db_column="F_DEVICE_NUMBER", null=True, blank=True)
    release_year = models.IntegerField(u"Год выпуска", db_column="F_DEVICE_RELEASEYEAR", null=True, blank=True)
    department = models.ForeignKey(to=Department, db_column="F_DEPARTMENT_ID", related_name="equipment", verbose_name=u"Подразделение")
    place = models.TextField(u"Место расположения", validators=[MaxLengthValidator(4200)], db_column="F_DEVICE_PLACE", null=True, blank=True)
    price = models.IntegerField(u"Цена на дату приобретения (в рублях)", db_column="F_DEVICE_PRICE", null=True, blank=True)
    moneysource = models.ForeignKey(to=JournalRubric, db_column="F_DEVICE_MONEYSOURCE", related_name="equipment", verbose_name=u"Источник финансирования", null=True, blank=True)
    moneysource_comment = models.CharField(u"Ист. фин. (комментарий)", max_length=1024, db_column="F_DEVICE_MONEYSOURCECOMMENT", null=True, blank=True)
    buydate = models.DateField(u"Дата приобретения", db_column="F_DEVICE_BUY", null=True, blank=True)
    startdate = models.DateField(u"Дата начала эксплуатации", db_column="F_DEVICE_STARTDATE", null=True, blank=True)
    warrantydate = models.DateField(u"Дата окончания гарантии", db_column="F_DEVICE_WARRANTY", null=True, blank=True)
    techdata = models.TextField(u"Технические характеристики", validators=[MaxLengthValidator(4200)], db_column="F_DEVICE_TECHN", null=True, blank=True)
    schedule = models.TextField(u"Расписание и условия доступа", validators=[MaxLengthValidator(4200)], db_column="F_DEVICE_SCHEDULE", null=True, blank=True)
    status = models.CharField(u"Статус", max_length=255, choices=STATUS_CHOICES, default="EDIT", db_column="F_DEVICE_STATUS", blank=True)
    #parts = models.ManyToManyField("self", through='EquipmentLink', related_name='subdevices', symmetrical=False)

    def is_complex(self):
        # serial number may only be assigned to devices; no number -> complex
        if self.number is None or len(self.number)==0:
            return True
        return False

    def get_complex(self):
        '''Retruns the reference to containing complex, if any.'''
        for link in self.parent_links.all():
            return link.container
        return None

    def get_ancestors_flat(self):
        ancestors = []
        parent = self.get_complex()
        if parent:
            ancestors += parent.get_ancestors_flat()
            ancestors += [parent]
        return ancestors

    def is_signed(self):
        return bool(self.status and not self.status in ("EDIT", "REEDIT"))

    @property
    def signed_activities(self):
        return self.activities.filter(status__in=("SIGN", "CONFIRM"))

    search_attr = 'fullname'

    def __unicode__(self):
        return self.shortname or self.fullname

    class Meta:
        db_table = "DEVICE"
        verbose_name = u"Оборудование"
        verbose_name_plural = u"Оборудование"

class EquipmentRublink(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEVICERUBLINK_ID")
    equipment = models.ForeignKey(to=Equipment, related_name="rubrics", db_column="F_DEVICE_ID", null=False, blank=False)
    rubric = models.ForeignKey(to=JournalRubric, related_name='equipments', verbose_name=u'Рубрика', db_column='F_JOURNALRUB_ID', on_delete=models.PROTECT)
    class Meta:
        db_table = "DEVICERUBLINK"
        verbose_name = u"Классификация оборудования"
        verbose_name_plural = u"Классификация оборудования"


class EquipmentLink(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEVICELINK_ID")
    container = models.ForeignKey(to=Equipment, related_name="item_links", db_column="DEV_F_DEVICE_ID", null=False, blank=False)
    equipment = models.ForeignKey(to=Equipment, related_name="parent_links", db_column="F_DEVICE_ID", null=False, blank=False)
    class Meta:
        db_table = "DEVICELINK"



class EquipmentAct(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEVICEACT_ID")
    equipment = models.ForeignKey(to=Equipment, related_name="activities", db_column="F_DEVICE_ID", null=False, blank=False)
    creator = models.ForeignKey(to=User, related_name="deviceacts_added", db_column="F_DEVICEACT_USER", null=True, blank=True)
    activity_type = models.CharField(u"Тип", max_length=255, db_column="F_DEVICEACT_TYPE")
    activity_id = models.IntegerField(u"Ключ публикации", max_length=255, db_column="F_DEVICEACT_KEY")
    status = models.CharField(u"Статус", max_length=255, choices=STATUS_CHOICES, default="EDIT", db_column="F_DEVICEACT_STATUS", blank=True)

    def reference(self):
        if self.activity_type == 'article':
            object = Article.objects.get(pk=self.activity_id)
            return object.get_citation()
        if self.activity_type == 'book':
            object = Book.objects.get(pk=self.activity_id)
            return object.get_citation()
        return "---"

    def get_object_url(self):
        if self.activity_type == 'article':
            return '/publications/article/%s/' % self.activity_id
        elif self.activity_type == 'book':
            return '/publications/book/%s/' % self.activity_id
        return ''

    def get_object(self):
        if self.activity_type == 'article':
            return Article.objects.get(pk=self.activity_id)
        elif self.activity_type == 'book':
            return Book.objects.get(pk=self.activity_id)

    def is_signed(self):
        return bool(self.status and not self.status in ("EDIT", "REEDIT"))

    class Meta:
        db_table = "DEVICEACT"
        verbose_name = u"Публикация"
        verbose_name_plural = u"Публикации"


class EquipmentStaffType(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_RESPONSIBLETYPE_ID")
    name = models.CharField(u"Название роли", max_length=255, db_column="F_RESPONSIBLETYPE_NAME")

    class Meta:
        db_table = "RESPONSIBLETYPE"
        verbose_name = u"Роль сотрудника"
        verbose_name_plural = u"Роли сотрудников"
    def __unicode__(self):
        return self.name


class EquipmentStaff(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_RESPONSIBLE_ID")
    equipment = models.ForeignKey(to=Equipment, related_name="staff", db_column="F_DEVICE_ID", null=False, blank=False)
    role = models.ForeignKey(to=EquipmentStaffType, related_name="members", db_column="F_RESPONSIBLETYPE_ID", verbose_name=u'Роль', null=False, blank=False)
    lastname = models.CharField(u"Фамилия", max_length=255, db_column="F_RESPONSIBLE_NAMEL", null=True, blank=True)
    firstname = models.CharField(u"Имя", max_length=255, db_column="F_RESPONSIBLE_NAMEF", null=True, blank=True)
    middlename = models.CharField(u"Отчество", max_length=255, db_column="F_RESPONSIBLE_NAMEM", null=True, blank=True)
    phone = models.CharField(u"Телефон (рабочий)", max_length=255, db_column="F_RESPONSIBLE_WORKPHONE", null=True, blank=True)
    phone_mobile = models.CharField(u"Телефон (мобильный)", max_length=255, db_column="F_RESPONSIBLE_MOBPHONE", null=True, blank=True)
    email = models.CharField(u"E-mail", max_length=255, db_column="F_RESPONSIBLE_EMAIL", null=True, blank=True)
    worker = models.ForeignKey(to=Worker, related_name="managed_equipment", db_column="F_MAN_ID", null=True, blank=True)
    user = models.ForeignKey(to=User, related_name="managed_equipment", db_column="F_RESPONSIBLE_USER", null=True, blank=True)

    class Meta:
        db_table = "RESPONSIBLE"
        verbose_name = u"Ответственный за оборудование"
        verbose_name_plural = u"Ответственные за оборудование"

    def __unicode__(self):
        fullname = " ".join([self.lastname or '', self.firstname or '', self.middlename or ''])
        items = [unicode(self.role) or '', unicode(self.worker) or fullname, self.phone or '']
        return u", ".join(items)

class EquipmentDocType(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEVICEDOCTYPE_ID")
    name = models.CharField(u"Название типа", max_length=255, db_column="F_DEVICEDOCTYPE_NAME", null=True, blank=True)
    code = models.CharField(u"Код типа", max_length=255, db_column="F_DEVICEDOCTYPE_CODE", null=True, blank=True)
    class Meta:
        db_table = "DEVICEDOCTYPE"
        verbose_name = u"Тип документа"
        verbose_name_plural = u"Типы документов"

    def __unicode__(self):
        return self.name

class EquipmentDoc(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEVICEDOC_ID")
    doctype = models.ForeignKey(to=EquipmentDocType, related_name="docs", db_column="F_DEVICEDOCTYPE_ID", null=False, blank=False)
    equipment = models.ForeignKey(to=Equipment, related_name="docs", db_column="F_DEVICE_ID", null=False, blank=False)
    docfile = models.FileField(u"Файл", upload_to="equipment/files/%Y/%m/%d",
                               max_length=255, validators=[validate_uploaded_file],
                               db_column="F_DEVICEDOC_FILE")
    name = models.TextField(u"Описание файла", max_length = 250, validators=[MaxLengthValidator(1000)],
                            db_column="F_DEVICEDOC_NAME")
    annotation = models.TextField(u"Аннотация", validators=[MaxLengthValidator(1000)],
                                  db_column="F_DEVICEDOC_TEXT", null=True, blank=True)

    def get_absolute_url(self):
        return self.docfile.url

    def __unicode__(self):
        return unicode(self.doctype) + ": " + self.name

    def is_image(self):
        return os.path.splitext(self.docfile.path)[1] in ['.jpg', '.jpeg', '.png', '.gif', '.bmp', '.svg'] and imghdr.what(self.docfile.path)

    class Meta:
        db_table = "DEVICEDOC"

class EquipmentIndexEntry(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEVICE_ID")
    creator = models.ForeignKey(to=User, db_column="F_DEVICE_USER", related_name="+", null=True, blank=True)
    fullname = models.CharField(u"Название (полное)", max_length=255, db_column="F_DEVICE_FULLNAME")
    shortname = models.CharField(u"Название (краткое)", max_length=255, db_column="F_DEVICE_SHORTNAME", null=True, blank=True)
    equiptype = models.ForeignKey(to=EquipmentType, db_column="F_DEVICETYPE_ID", related_name="+", verbose_name=u"Тип")
    department = models.ForeignKey(to=Department, db_column="F_DEPARTMENT_ID", related_name="+", verbose_name=u"Подразделение")
    status = models.CharField(u"Статус", max_length=255, choices=STATUS_CHOICES, default="EDIT", db_column="F_DEVICE_STATUS", blank=True)

    equiptype_name = models.CharField(u"Название типа", max_length=255, db_column="F_DEVICETYPE_NAME")
    equiptype_parent = models.ForeignKey("EquipmentType", db_column="DEV_F_DEVICETYPE_ID", related_name="+", null=True, blank=True)

    department_name = models.CharField(u"Название подразделения", max_length=255, db_column="F_DEPARTMENT_NAME")

    is_complex = models.BooleanField(u"Комплекс научного оборудования", db_column="IS_COMPLEX")

    objects = EquipmentIndexEntryManager()

    class Meta:
        managed = False
