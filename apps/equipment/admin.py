from common.utils.admin import register_with_versions
from models import Equipment, EquipmentType, EquipmentRublink, EquipmentLink, \
     EquipmentAct, EquipmentStaffType, EquipmentStaff, EquipmentDocType, \
     EquipmentDoc

map(register_with_versions,
    [Equipment, EquipmentType, EquipmentRublink, EquipmentLink, EquipmentAct,
        EquipmentStaffType, EquipmentStaff, EquipmentDocType, EquipmentDoc])
