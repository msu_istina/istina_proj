# -*- coding: utf-8; -*-
import authority
import operator
from common.permissions import MyModelPermission, LinkedToWorkersModelPermission
from unified_permissions import has_permission

def equipment_role_content(user, equipment):
    return equipment.staff.filter(worker__profile__user=user, role__name="Ответственный за информацию").exists()

def equipment_role_activity(user, equipment):
    return equipment.staff.filter(worker__profile__user=user, role__name="Научный руководитель").exists()

class EquipmentPermission(MyModelPermission):
    label = 'equipment_permission'

    def check_edit(self, equipment):
        return self.check_sign(equipment) or self.user == equipment.creator or equipment_role_content(self.user, equipment) or (equipment.get_complex() and self.check_edit(equipment.get_complex()))

    def check_delete(self, equipment):
        return self.check_edit(equipment) and not equipment.is_signed()

    def check_sign(self, equipment):
        return has_permission(self.user, 'confirm_equipment', equipment.department)


class EquipmentLinkPermission(MyModelPermission):
    label = 'equipment_link_permission'

    def check_delete(self, link):
        return (
            has_permission(self.user, 'confirm_equipment', link.container.department)
            or has_permission(self.user, 'confirm_equipment', link.equipment.department)
        )

class EquipmentActPermission(MyModelPermission):
    label = 'equipment_act_permission'

    checks = ('check_delete', 'check_sign', )

    def check_delete(self, act):
        return (
            self.check_sign(act)
            or has_permission(self.user, 'unauthor', act.get_object())
        )

    def check_sign(self, act):
        return (
            equipment_role_activity(self.user, act.equipment)
            or has_permission(self.user, 'confirm_equipment', act.equipment.department)
        )

class EquipmentStaffPermission(MyModelPermission):
    label = 'equipment_staff_permission'

    def check_delete(self, staff):
        return (
            EquipmentPermission(user=self.user).check_edit(staff.equipment)
            or staff.user == self.user
        )
