from common.managers import MyManager

class EquipmentIndexEntryManager(MyManager):
    def all_raw(self, organization_id):
        select_equipment_index_sql = '''
        select
            i.F_DEVICE_ID,
            i.F_DEVICE_USER,
            i.F_DEVICE_FULLNAME,
            i.F_DEVICE_SHORTNAME,
            i.F_DEVICETYPE_ID,
            i.F_DEPARTMENT_ID,
            i.F_DEVICE_STATUS,
            t.F_DEVICETYPE_NAME,
            t.DEV_F_DEVICETYPE_ID,
            e.F_DEPARTMENT_NAME,
            NVL2(i.F_DEVICE_REGNUMBER, 0, 1) IS_COMPLEX
        from (select f_device_id, dev_f_device_id from devicelink
              union all
              select d.f_device_id, null from device d where d.f_device_id not in (select f_device_id from devicelink)
             ) dl
        join device i on dl.f_device_id=i.f_device_id
        join department e on i.f_department_id = e.f_department_id
        join devicetype t on i.f_devicetype_id = t.f_devicetype_id
        where e.F_ORGANIZATION_ID = %s
        connect by dl.dev_f_device_id = prior dl.f_device_id
        start with dev_f_device_id is null
        '''
        return self.raw(select_equipment_index_sql, [organization_id])

    def count(self):
        return self.raw('''select count(*) from devices''')
