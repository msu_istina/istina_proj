# -*- coding: utf-8; -*-

from django import forms

from common.forms import Select2ModelField, Select2ModelMultipleField
from common.forms import Select2SingleWorkerField
from common.dynatree_widgets import DynatreeWidget, DynatreeLazyWidget

from unified_permissions.forms import UserRolePermissionCreateEditForm

from publications.models import Article
from workers.models import Worker
from models import EquipmentAct
from models import Equipment
from models import EquipmentType
from models import EquipmentStaff, EquipmentStaffType
from models import EquipmentDoc
from organizations.models import Department
from django.core.urlresolvers import reverse


from journals.models import JournalRubric

class ArticleSelect2Field(Select2ModelField):
    def __init__(self, *args, **kwargs):
        super(ArticleSelect2Field, self).__init__(self, *args, add_option=False, model=Article, **kwargs)
        self.widget.options['placeholder'] = u'Введите часть названия статьи'

    def init_options(self):
        super(ArticleSelect2Field, self).init_options()
        self.options['placeholder'] = u'Введите часть названия статьи'

    def get_title(self, obj):
        return obj.title

    def get_description(self, article):
        try:
            return article.get_citation()
        except AttributeError:
            return u''

class StaffSelect2Field(Select2SingleWorkerField):
    def __init__(self, *args, **kwargs):
        super(StaffSelect2Field, self).__init__(self, *args, add_option=False, model=Article, **kwargs)
        self.widget.options['placeholder'] = u'Введите фамилию'

class EquipmentSelect2Field(Select2ModelField):
    def __init__(self, *args, **kwargs):
        super(EquipmentSelect2Field, self).__init__(self, *args, add_option=False, model=Equipment, **kwargs)
        self.widget.options['placeholder'] = u'Введите часть полного названия'

class EquipmentSelect2MultipleField(Select2ModelMultipleField):
    def __init__(self, *args, **kwargs):
        super(EquipmentSelect2MultipleField, self).__init__(self, *args, add_option=False, model=Equipment, **kwargs)
        self.widget.options['placeholder'] = u'Введите часть полного названия'


class AddResultForm(forms.ModelForm):
    class Meta:
        model = EquipmentAct
        exclude = ('id', 'creator', 'equipment', 'activity_type', 'activity_id', 'status')

    article = ArticleSelect2Field(label=u'Добавить статью',
                                  widget_kwargs={'width': 640},
                                  required=True)

class MoneySourceRub(JournalRubric):
    def __unicode__(self):
        return u"%s" % (self.name)
    class Meta:
        proxy = True


valid_types = EquipmentType.objects.all()
departments_sql = '''
     select d.f_department_id as f_department_id,
            d.f_department_name as f_department_name,
            dl.dep_f_department_id parent,
            level,
            SYS_CONNECT_BY_PATH(d.f_department_id, '/') as url
     from
     (select f_department_id, dep_f_department_id, f_departmentlink_end from departmentlink
     union all
     select f_department_id, NULL, NULL from department d2 where not exists (select 1 from departmentlink dl2 where dl2.f_department_id=d2.f_department_id)
     ) dl,
     department d
     where  d.f_department_id = dl.f_department_id
     and d.f_organization_id = 214524
     and (dl.f_departmentlink_end is null
         or dl.f_departmentlink_end > sysdate )
     start with dl.dep_f_department_id is null
     connect by PRIOR dl.f_department_id = dl.dep_f_department_id
     order siblings by d.f_department_name
    '''

def get_EquipmentForm(name, extra_excludes=()):
    exclude_list=('status',)
    is_complex = False
    types_xdr_url = 'xdr_equipment_types'
    if name == 'complex':
        exclude_list = ('number', 'regnumber', 'price', 'warrantydate', 'release_year', 'moneysource', 'moneysource_comment')
        is_complex = True
        types_xdr_url = 'xdr_equipment_types_complex'
    class EquipmentForm(forms.ModelForm):
        class Meta:
            model = Equipment
            exclude = ('id', 'creator', 'parts', 'techdata') + exclude_list + extra_excludes

        moneysource = forms.ModelChoiceField(
            label = u'Источник финансирования',
            required=False,
            queryset=MoneySourceRub.objects.filter(categorization__code='equip_money_source'))

        def __init__(self, *args, **kwargs):
            super(EquipmentForm, self).__init__(*args, **kwargs)
            if self.fields.get('price', None):
                self.fields['price'].widget.attrs['placeholder'] = u'Укажите цену в рублях, например, 12876001'
            self.fields['place'].widget.attrs['placeholder'] = u'Пожалуйста, укажите номер комнаты, телефон или электронную почту, чтобы с Вами могли связаться при необходимости использования данного оборудования. Эти данные будут видны всем пользователям системы.'
            self.fields['buydate'].widget.attrs['placeholder'] = u'Например: 27.03.2001'
            self.fields['startdate'].widget.attrs['placeholder'] = u'Например: 28.03.2001'
            if self.fields.get('release_year', None):
                self.fields['release_year'].widget.attrs['placeholder'] = u'Например: 2011'
            if self.fields.get('warrantydate', None):
                self.fields['warrantydate'].widget.attrs['placeholder'] = u'Например: 27.03.2015'
            self.fields['equiptype'].widget = DynatreeWidget(select_mode=1, choices=valid_types, ajax_url=reverse(types_xdr_url))
            if self.fields.get('department', None):
                self.fields['department'].widget = DynatreeWidget(select_mode=1, initial_sql=departments_sql)
            if self.fields.get('techdata', None):
                self.fields['techdata'].widget.attrs['placeholder'] = u'Укажите основные технические характеристики данного оборудования.'
            if not is_complex: self.fields['number'].required = True

            if 'moneysource' in exclude_list:
                self.fields.pop('moneysource')

            if 'status' in self.fields:
                self.fields.pop('status')
    return EquipmentForm

EquipmentForm = get_EquipmentForm("device")
EquipmentComplexForm = get_EquipmentForm("complex")

class EquipmentTechdataForm(forms.ModelForm):
    class Meta:
        model = Equipment
        fields = ('techdata', )

    def __init__(self, *args, **kwargs):
        super(EquipmentTechdataForm, self).__init__(*args, **kwargs)
        self.fields['techdata'].widget.attrs['placeholder'] = u'Укажите основные технические характеристики данного оборудования.'


from journals.models import JournalRubric
class EquipmentRubForm(forms.Form):
    rub_pnr = forms.CharField(max_length=4000, required=False, label=u'Приоритетные направления развития РФ')
    rub_pnrmsu = forms.CharField(max_length=4000, required=False, label=u'Приоритетные направления развития МГУ')
    rub_grnti = forms.CharField(max_length=4000, required=False, label=u'ГРНТИ')

    def __init__(self, *args, **kwargs):
        super(EquipmentRubForm, self).__init__(*args, **kwargs)
        self.fields['rub_pnr'].widget = DynatreeWidget(select_mode=2,
                                                       ajax_url=reverse('xdr_equipment_rubs'),
                                                       ajax_extra_data={"rubcode": "PNR-RF"},
                                                       collapsable_tree=True)
        self.fields['rub_pnrmsu'].widget = DynatreeWidget(select_mode=1,
                                                          ajax_url=reverse('xdr_equipment_rubs'),
                                                          ajax_extra_data={"rubcode": "PNR-MSU"},
                                                          collapsable_tree=True)
        self.fields['rub_grnti'].widget = DynatreeWidget(select_mode=2,
                                                         ajax_url=reverse('xdr_equipment_rubs'),
                                                         ajax_extra_data={"rubcode": "GRNTI"},
                                                         collapsable_tree=True)



class ResponsibleForm(UserRolePermissionCreateEditForm):
    def __init__(self, *args, **kwargs):
        self.organization_id = kwargs.pop('organization_id')
        self.label = kwargs.pop('label')
        super(ResponsibleForm, self).__init__(*args, **kwargs)
        self.fields.pop('label')
        self.fields['department'].widget = DynatreeWidget(select_mode=1, initial_sql=departments_sql)

    def clean_label(self):
        return PermissionLabel.objects.get(name=self.label)

    def clean_organization(self):
        return Organization.objects.get(pk=self.organization_id)

    def clean_department(self):
        department = self.cleaned_data.get('department')
        if department.organization.id != self.organization_id:
            raise forms.ValidationError(u"У вас не хватает полномочий, чтобы выдать сотруднику роль в рамках данного подразделения")
        return department

class EquipmentStaffForm(forms.ModelForm):
    class Meta:
        model = EquipmentStaff
        fields = ('worker', 'role', 'phone', 'phone_mobile', 'email') # 'lastname', 'firstname', 'middlename',

    worker = StaffSelect2Field(label=u'Поиск сотрудников',
                               queryset=Worker.objects.all(),
                               required=True)
    def __init__(self, *args, **kwargs):
        super(EquipmentStaffForm, self).__init__(*args, **kwargs)
        self.fields['phone'].widget.attrs['placeholder'] = u'Показывается всем пользователям'
        self.fields['phone_mobile'].widget.attrs['placeholder'] = u'Показывается только ответственным'
        self.fields['email'].widget.attrs['placeholder'] = u'Показывается только ответственным'


class EquipmentDocForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EquipmentDocForm, self).__init__(*args, **kwargs)
        self.fields["name"].widget = forms.TextInput()
        self.fields["doctype"].label = u'Тип документа'

    class Meta:
        model = EquipmentDoc
        fields =('doctype', 'docfile', 'name', 'annotation')


class EquipmentPartForm(forms.ModelForm):
    class Meta:
        model = Equipment
        fields = ()

    part = EquipmentSelect2Field(label=u'Добавить прибор из базы',
                                 widget_kwargs={'width': 640},
                                 required=True)

class EquipmentReportForm(forms.Form):
    devices = EquipmentSelect2MultipleField(label=u'Выбрать приборы из базы', widget_kwargs={'width': 640})
