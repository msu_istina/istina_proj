# -*- coding: utf-8 -*-

from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.db.models import ObjectDoesNotExist
from sqlreports.views import sign_url, generate_adata

from organizations.models import Organization, Department
from unified_permissions import has_permission
from unified_permissions.decorators import permission_required
from equipment.forms import EquipmentReportForm
from equipment.models import EquipmentType, EquipmentStaffType
from common.tree_data_editor import tree_data_editor

from equipment.forms import ResponsibleForm

@permission_required('confirm_equipment', Organization, 'organization_id')
def staff_types(request, organization_id):
    # create
    if request.method == 'POST':
        name = request.POST.get('name', '')
        if name == '':
            messages.error(request, u'Введите название роли')
            return _show(request)

        staff_type = EquipmentStaffType(name=name)
        if EquipmentStaffType.objects.filter(name=name).exists():
            messages.error(request, u'Роль с таким названием уже существует')
            return _show(request)

        staff_type.save()

    return _show(request, True)


def _show(request, is_admin):
    staffTypes = EquipmentStaffType.objects.all().order_by('id')
    return render(request, "equipment/staff_types.html", {
        'staff_types': staffTypes,
        'is_admin': is_admin
    })

@permission_required('confirm_equipment', Organization, 'organization_id')
def edit_staff_type(request, organization_id, id=''):
    try:
        id = int(id)
        name = request.POST.get('name', '')
        if name == '': raise ValueError
        if EquipmentStaffType.objects.filter(name=name).exclude(pk=id).exists():
            return HttpResponseBadRequest(u'Роль с таким именем уже существует')
        staff_type = EquipmentStaffType.objects.get(pk=id)
        staff_type.name = name
        staff_type.save()
        return HttpResponse(name)
    except ValueError:
        return HttpResponseBadRequest(u'Введите название роли')
    except ObjectDoesNotExist:
        return HttpResponseBadRequest(u'Роль для редактирования не найдена id=' + str(id))

def tree_editor_wrapper(request, item_id=None, operation=None, organization_id=214524):
    if not has_permission(request.user, 'confirm_equipment', get_object_or_404(Organization, pk=int(organization_id))):
        messages.error(request, u"У вас нет права на доступ к данной странице.")
        return redirect("startpage")
    return tree_data_editor(request,
        model=EquipmentType,
        field_name_to_set='name',
        view_name='equipment_types_msu',
        item_id=item_id,
        operation=operation,
        page_title=u'Редактирование типов оборудования',
        go_back_url='/equipment/by_organization/%s/' % organization_id)

@permission_required('confirm_equipment', Organization, 'organization_id')
def report_equipment(request, organization_id):
    form = EquipmentReportForm(request.POST or None)
    if request.method == 'POST':
        devices = request.POST.getlist('devices', [])
        return redirect(sign_url('/sqlreports/createreport/equipment_manual_price_citation/?devices=%s' % (",".join(devices))))
    return render(request, "equipment/report.html", {
        'form': form
    })

@permission_required('confirm_equipment', Organization, 'organization_id')
def report_equipment_monitoring(request, organization_id):
    return redirect(sign_url('/sqlreports/createreport/equipment_monitoring_msu/?organization_id=214524', generate_adata(request)))

@permission_required('confirm_equipment', Department, 'department_id')
def report_equipment_department(request, department_id):
    return redirect(sign_url('/sqlreports/createreport/equipment_department_complex_single/?root_department_id=%s' % department_id, generate_adata(request)))

@permission_required('confirm_equipment', Department, 'department_id')
def report_equipment_citations(request, department_id):
    return redirect(sign_url('/sqlreports/createreport/equipment_department_citations/?root_department_id=%s' % department_id, generate_adata(request)))

from equipment.forms import departments_sql
from django.contrib.contenttypes.models import ContentType
from collections import namedtuple
from django.db import connection
def get_users_with_permission_in(organization_id, label):
    # required data:
    #  user id
    #  user full name
    #  department id
    #  department name
    department_type = ContentType.objects.get(model=u"department", app_label=u"organizations").id
    sql = '''
    select da.*, man.F_MAN_NAMEL, man.F_MAN_NAMEF, man.F_MAN_NAMEM, profile.WORKER_ID from (select auth.user_id, deps.F_DEPARTMENT_ID, deps.F_DEPARTMENT_NAME from
        (''' + departments_sql + ''') deps
           join AUTHORITY_PERMISSION auth
               on (CONTENT_TYPE_ID = ''' + str(department_type) + '''
               and CODENAME = ''' + "'" + label + "'" + '''
               and deps.F_DEPARTMENT_ID = auth.OBJECT_ID)) da
           join WORKERS_PROFILE profile
               on da.USER_ID = profile.USER_ID
           join MAN man
                on man.F_MAN_ID = profile.WORKER_ID
    '''
    cursor = connection.cursor()
    cursor.execute(sql)
    tuple = namedtuple('UserWithPermissionInDepartment', ['user_id', 'department_id', 'department_name', 'last_name', 'first_name', 'middle_name', 'worker_id'])
    return map(tuple._make, cursor.fetchall())

@permission_required('confirm_equipment', Organization, 'organization_id')
def responsible(request, organization_id, label='confirm_equipment'):
    # label is default-only here
    return render(request, "equipment/responsible.html", {
        'responsible': get_users_with_permission_in(organization_id, label),
        'organization_id': organization_id
    })

@permission_required('confirm_equipment', Organization, 'organization_id')
def responsible_add(request, organization_id, label='confirm_equipment'):
    return render(request, "equipment/responsible_add.html", {
        'organization_id': organization_id,
        'responsible_form': ResponsibleForm(request.user, False, False, organization_id=organization_id, label=label)
    })

@permission_required('confirm_equipment', Organization, 'organization_id')
def responsible_remove(request, organization, label='confirm_equipment'):
    pass

@permission_required('confirm_equipment', Organization, 'organization_id')
def responsible_email(request, organization_id, label='confirm_equipment'):
    pass
