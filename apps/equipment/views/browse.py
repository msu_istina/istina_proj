# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404

import random

from equipment.models import Equipment, EquipmentIndexEntry, EquipmentDoc
from organizations.models import Organization, Department
from unified_permissions import has_permission
from unified_permissions.decorators import permission_required
from unified_permissions import departments_ids_user_has_permission_in
from unified_permissions.models import PermissionLabel

@login_required
def card(request, equip_id):
    theEquipment = get_object_or_404(Equipment.objects.select_related( 'department', 'activities', 'docs').prefetch_related('item_links', 'staff'), pk=equip_id)
    title_image_url = None
    # pick up a random image
    for doc in theEquipment.docs.filter(doctype__code='PHOTO'):
        if random.randint(0,1) == 1 or not title_image_url:
            title_image_url = doc.get_absolute_url()
    onlyDevice = None
    if theEquipment.item_links.count() == 1:
        # single-device complex
        onlyDevice = theEquipment.item_links.all()[0].equipment
    total_activities = theEquipment.signed_activities.count()
    activities = theEquipment.signed_activities.order_by('-id')[:25]
    for c, act in enumerate(activities):
        act.order = total_activities - c
    return render(request, "equipment/card.html",
                  {'equipment': theEquipment,
                   'only_device': onlyDevice,
                   'activities': reversed(activities),
                   'title_image_url': title_image_url,
                   'can_confirm_any': has_permission(request.user, 'confirm_equipment', theEquipment.department.organization) or bool(departments_ids_user_has_permission_in(request.user, "confirm_equipment")),
                   'can_edit': has_permission(request.user, 'edit', theEquipment),
                   'can_sign': has_permission(request.user, 'sign', theEquipment)
                   })


@login_required
def index(request, organization_id):
    organization = get_object_or_404(Organization.objects, pk=organization_id)
    equipment = EquipmentIndexEntry.objects.all_raw(organization_id)
    confirm_equipment = PermissionLabel.objects.get(name="confirm_equipment")
    departments_responsible = set(map(lambda x: x.content_object,
        confirm_equipment.permissions_links.filter(user=request.user,
            content_type=ContentType.objects.get_for_model(Department))))
    return render(request, "equipment/index.html",
                  {'equipment': equipment,
                   'is_admin': has_permission(request.user, 'confirm_equipment', organization),
                   'departments_responsible': departments_responsible,
                   'total': EquipmentIndexEntry.objects.count(),
                   'organization_id': organization_id
                   })

@login_required
def doc(request, equip_id, doc_id):
    equipment = get_object_or_404(Equipment.objects, pk=equip_id)
    doc = get_object_or_404(EquipmentDoc.objects, pk=doc_id)
    if not doc.is_image:
        return redirect(doc.get_absolute_url())
    else:
        return render(request, "equipment/file_annotation.html",
                    { 'equipment': equipment,
                        'doc': doc})

from django.db import connection
from django.db.models import Count, Q
from equipment.models import EquipmentLink, EquipmentAct, EquipmentStaff
@permission_required('confirm_equipment', Organization, 'organization_id')
def stats(request, organization_id):
    model = {}
    model['total_equipment'] = Equipment.objects.count()
    complexes = Equipment.objects.filter(regnumber='')
    model['total_complexes'] = complexes.count()
    model['total_links'] = EquipmentLink.objects.count()
    model['total_activities'] = EquipmentAct.objects.count()
    model['confirmed_activities'] = EquipmentAct.objects.filter(Q(status='SIGN') | Q(status='CONFIRM')).count()
    model['total_staff'] = EquipmentStaff.objects.count()

    # annotate(Count(related)) not working for Oracle provider
    #model['complexes_without_equipment'] = complexes.annotate(num_items=Count('item_links')).filter(num_items=0).count()
    cursor = connection.cursor()
    cursor.execute("select count(*) from (select DEVICE.F_DEVICE_ID, count(DEVICELINK.F_DEVICE_ID) ci from DEVICE left join DEVICELINK on DEVICE.F_DEVICE_ID=DEVICELINK.DEV_F_DEVICE_ID group by DEVICE.F_DEVICE_ID) where ci=0")
    model['complexes_without_equipment'] = cursor.fetchone()[0]

    cursor.execute("select count(*) from (select DEVICE.F_DEVICE_ID, count(DEVICEACT.F_DEVICE_ID) ci from DEVICE left join DEVICEACT on DEVICE.F_DEVICE_ID=DEVICEACT.F_DEVICE_ID group by DEVICE.F_DEVICE_ID) where ci=0")
    model['complexes_without_activities'] = cursor.fetchone()[0]

    cursor.execute("select count(*) from (select DEVICE.F_DEVICE_ID, count(RESPONSIBLE.F_DEVICE_ID) ci from DEVICE left join RESPONSIBLE on DEVICE.F_DEVICE_ID=RESPONSIBLE.F_DEVICE_ID group by DEVICE.F_DEVICE_ID) where ci=0")
    model['complexes_without_staff'] = cursor.fetchone()[0]

    template = '''
Внутренняя статистика по модулю оборудования

Всего оборудования:\t{total_equipment}
Всего комплексов:\t{total_complexes}
Всего устройств:\t{total_links}
Всего сотрудников:\t{total_staff}
Всего статей:\t\t{total_activities}
Подтверждено статей:\t{confirmed_activities}

Комплексов без устройств:\t{complexes_without_equipment}
Комплексов без статей:\t\t{complexes_without_activities}
Комплексов без персонала:\t{complexes_without_staff}
'''
    return HttpResponse(template.format(**model), content_type='text/plain; charset=utf8')
