# -*- coding: utf-8; -*-
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib import messages

import os
import math

from common.views import delete_attachments
from unified_permissions.decorators import permission_required
from unified_permissions import has_permission, departments_ids_user_has_permission_in

import browse

from equipment.models import Equipment, EquipmentRublink, EquipmentLink, EquipmentAct, EquipmentStaff, EquipmentDoc
from journals.models import JournalRubric

from equipment.forms import EquipmentForm, EquipmentComplexForm, get_EquipmentForm, AddResultForm, EquipmentStaffForm, EquipmentDocForm, EquipmentTechdataForm, EquipmentPartForm, EquipmentRubForm


@login_required
def results(request, equip_id, has_menu=False):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)
    form = AddResultForm(request.POST or None)

    if request.method == 'POST': # If the form has been submitted...
        if form.is_valid():
            act = form.save(commit=False)
            act.creator = request.user
            act.equipment = theEquipment
            article = form.cleaned_data['article']
            if article:
                act.activity_id = article.id
                act.activity_type = 'article'

            if has_permission(request.user, 'sign', act):
                act.status = 'SIGN'

            if theEquipment.activities.filter(activity_id=act.activity_id).exists():
                messages.info(request, u'Статья уже прикреплена к данному устройству')
            else:
                act.save()
                messages.success(request, u'Данные сохранены.')
            form = AddResultForm()
        else:
            messages.error(request, u'Данные НЕ сохранены.')

    to_sign = theEquipment.activities.filter(status__in=('EDIT','REEDIT', '')).all()
    to_sign = filter(lambda a: has_permission(request.user, 'sign', a)
         or has_permission(request.user, 'delete', a), to_sign)

    if 'p' not in request.GET:
        get = request.GET.copy()
        get['p'] = math.ceil(theEquipment.signed_activities.count() / 25.0)
        request.GET = get

    return render(request, "equipment/results.html", {
        'equipment': theEquipment,
        'form': form,
        'to_sign': to_sign,
        'signed_activities': theEquipment.signed_activities.order_by('id'),
        'has_menu': has_menu
    })


@permission_required('edit', Equipment, 'equip_id')
def edit_results(request, equip_id):
    return results(request, equip_id, True)


@permission_required('delete', EquipmentAct, 'result_id')
def unlink_result(request, equip_id, result_id):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)
    result = get_object_or_404(EquipmentAct, pk=result_id)
    result.delete()
    messages.success(request, u'Информация о результате удалена.')
    return redirect("add_results", theEquipment.id)


@login_required
def add_card(request):
    if request.method == 'GET':
        return redirect("equipment_index")
    parentComplex = None

    complex_id = None
    is_complex = False
    if request.method == 'POST' and request.POST.get('parent_complex_id', None):
        complex_id = request.POST.get('parent_complex_id')
        parentComplex = get_object_or_404(Equipment, pk=complex_id)
        is_complex = False
    if request.method == 'POST' and request.POST.get('add_complex', None):
        is_complex = True

    form_class = EquipmentComplexForm if is_complex else EquipmentForm
    form = form_class(initial={'department': parentComplex.department} if parentComplex else {})
    return render(request, "equipment/add.html", {
        'equipment': None,
        'equip_form': form,
        'is_complex': 'yes' if is_complex else 'no',
        'parentComplex': parentComplex,
        })


@login_required
def edit_card(request, equip_id=None, tab_forms={}):
    theEquipment = None
    if equip_id:
        theEquipment = get_object_or_404(Equipment, pk=equip_id)
    if theEquipment and not has_permission(request.user, 'edit', theEquipment):
        messages.error(request, u'У Вас нет прав для редактирования данной записи.')
        return redirect("equipment_card", theEquipment.id)

    parent_complex_id = None
    parentComplex = None
    excluded_fields = ()
    if request.method == 'POST' and request.POST.get('parent_complex_id', None):
        parent_complex_id = request.POST.get('parent_complex_id')
        parentComplex = get_object_or_404(Equipment, pk=parent_complex_id)

    # do not allow to edit department if the device is installed inside a complex
    #if parentComplex or (theEquipment and theEquipment.get_complex):
    #    excluded_fields = ('department',)

    is_complex = False
    form_class = get_EquipmentForm("device", excluded_fields)
    if (theEquipment and theEquipment.is_complex()) \
           or (request.method == 'POST' and request.POST.get('is_complex', None) == 'yes'):
        is_complex = True
        form_class = get_EquipmentForm("complex")

    form = form_class(request.POST or None, instance=theEquipment)
    if request.method == 'POST':
        if request.POST.get('save', None) and form.is_valid():
            theEquipment = form.save(commit=False)
            if not equip_id: theEquipment.creator = request.user
            if parentComplex:
                theEquipment.department = parentComplex.department
            theEquipment.save()
            if parentComplex:
                link = EquipmentLink(container=parentComplex, equipment=theEquipment)
                link.save()
            messages.success(request, u'Данные успешно сохранены.')
            if is_complex and not equip_id:
                # Creation of new complex => ask to add devices
                messages.success(request, u'Пожалуйста, добавьте данные об оборудовании в составе комплекса.')
            return redirect("equipment_edit", theEquipment.id)
        elif request.POST.get('cancel', None):
            if theEquipment:
                return redirect("equipment_edit", theEquipment.id)
            return redirect("equipment_index")
    return render(request, "equipment/add.html", {
        'equipment': theEquipment,
        'equip_form': form,
        'is_complex': 'yes' if is_complex else 'no',
        'parentComplex': parentComplex,
        })


@permission_required('edit', Equipment, 'equip_id')
def edit_classifiers(request, equip_id):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)

    def toint(s):
        try:
            return int(s)
        except ValueError:
            return None

    rubric_fields  = ['rub_pnr', 'rub_pnrmsu', 'rub_grnti']
    all_rubrics = ','.join( map(lambda rl: str(rl.rubric.id), theEquipment.rubrics.all()) )
    initials = {}
    for f in rubric_fields:
        initials[f] = all_rubrics

    form = EquipmentRubForm(request.POST or None, initial=initials)
    if request.method == 'POST' and form.is_valid():
        all_values = [] # list of all submitted classification codes
        for f in rubric_fields:
            data = form.data[f].split(',')
            all_values += filter(lambda x: x is not None, map(toint, data))
        EquipmentRublink.objects.filter(equipment=theEquipment).delete()
        for rubid in all_values:
            try:
                rubric = JournalRubric.objects.get(pk=rubid)
                rl = EquipmentRublink(equipment=theEquipment, rubric=rubric)
                rl.save()
            except JournalRubric.DoesNotExist:
                messages.error(request, u'Ошибка сохранения рубрики #%s.' % rubid)
    return render(request, "equipment/edit_classifiers.html", {
        'equipment': theEquipment,
        'form': form
        })


@permission_required('edit', Equipment, 'equip_id')
def edit_staff(request, equip_id):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)

    form = EquipmentStaffForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        staff = form.save(commit=False)
        staff.equipment = theEquipment
        staff.user = request.user
        staff.save()
        messages.success(request, u'Данные сохранены.')
        form = EquipmentStaffForm()
    return render(request, "equipment/edit_staff.html", {
        'equipment': theEquipment,
        'form': form
        })


@permission_required('edit', Equipment, 'equip_id')
def edit_techdata(request, equip_id):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)

    form = EquipmentTechdataForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        theEquipment.techdata = form.cleaned_data['techdata']
        theEquipment.save()
        messages.success(request, u'Данные сохранены.')
        form = EquipmentTechdataForm()
        form.fields['techdata'].initial = theEquipment.techdata
    else:
        form.fields['techdata'].initial = theEquipment.techdata
    return render(request, "equipment/edit_techdata.html", {
        'equipment': theEquipment,
        'techdata_form': form
        })


@permission_required('delete', EquipmentStaff, 'staff_id')
def unlink_staff(request, equip_id, staff_id):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)
    EquipmentStaff.objects.filter(equipment=theEquipment, pk=staff_id).delete()
    return redirect("equipment_edit_staff", theEquipment.id)


@permission_required('delete', Equipment, 'equip_id')
def equipment_delete(request, equip_id):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)
    theEquipment.delete()
    return redirect("equipment_index")

@permission_required('delete', EquipmentLink, 'link_id')
def remove_from_complex(request, equip_id, complex_id, link_id):
    parentComplex = get_object_or_404(Equipment, pk=complex_id)
    theEquipment = get_object_or_404(Equipment, pk=equip_id)
    link = get_object_or_404(EquipmentLink, pk=link_id)

    if link:
        link.delete()
        messages.success(request, u'Прибор исключен из состава комплекса.')
    else:
        messages.warning(request, u'Данный прибор не входит в состав комплекса!')
    return redirect("equipment_edit_parts", parentComplex.id)


@permission_required('edit', Equipment, 'equip_id')
def edit_docs(request, equip_id):
    template = "equipment/edit_files.html"
    theEquipment = get_object_or_404(Equipment, pk=equip_id)

    form = EquipmentDocForm()
    if request.method == 'POST':
        form = EquipmentDocForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = form.save(commit=False)
            newdoc.equipment = theEquipment
            newdoc.save()
            form = EquipmentDocForm()

    return render(request, template,
                  {"equipment": theEquipment, "form": form})

@permission_required('edit', Equipment, 'equip_id')
def remove_doc(request, equip_id, doc_id):
    template = "equipment/edit_files.html"
    theEquipment = get_object_or_404(Equipment, pk=equip_id)
    doc = get_object_or_404(EquipmentDoc, pk=doc_id, equipment=theEquipment)
    if os.path.isfile(doc.docfile.path):
        os.remove(doc.docfile.path)
        doc.delete()
    return redirect("equipment_edit_docs", theEquipment.id)


@permission_required('edit', Equipment, 'equip_id')
def edit_parts(request, equip_id):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)
    form = EquipmentPartForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        messages.success(request, u'Данные сохранены.')
        form = EquipmentPartForm()
    return render(request, "equipment/edit_parts.html", {
        'equipment': theEquipment,
        'part_form': form
        })


@permission_required('edit', Equipment, 'equip_id')
def add_part(request, equip_id):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)
    form = EquipmentPartForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        try:
            device_to_add = form.cleaned_data['part']
            if device_to_add == theEquipment or \
                EquipmentLink.objects.filter(equipment=device_to_add).exists() or \
                device_to_add in theEquipment.get_ancestors_flat():
                messages.error(request, u'Невозможно добавить указанное устройство: оно является комплексом или уже входит в состав другого комплекса.')
            else:
                link = EquipmentLink(container=theEquipment, equipment=form.cleaned_data['part'])
                link.save()
                messages.success(request, u'Данные сохранены.')
        except Equipment.DoesNotExist:
            messages.error(request, u'Указанный неверный прибор (не найден в базе).')
        return redirect("equipment_edit_parts", theEquipment.id)
    return render(request, "equipment/edit_parts.html", {
        'equipment': theEquipment,
        'part_form': form
        })

def _do_sign(request, object, organization, name_prep):
    if request.method == 'PUT' or request.method == 'GET':
        if object.status == 'SIGN':
            if has_permission(request.user, 'confirm_equipment', organization):
                object.status = 'CONFIRM'
                messages.success(request, u'Информация %s подтверждена ректоратом.' % name_prep)
            else:
                messages.warning(request, u'Информация %s уже подтверждена.' % name_prep)
        else:
            object.status = 'SIGN'
            messages.success(request, u'Информация %s подтверждена.' % name_prep)

    elif request.method == 'DELETE':
        if object.status == 'CONFIRM' and not has_permission(request.user, 'confirm_equipment', organization):
            messages.error(request, u'Информация %s подтверждена ректоратом. Недостаточно прав для снятия отметки.' % name_prep)
        else:
            object.status = 'REEDIT'
            messages.success(request, u'Отметка о подтверждении успешно снята.')

    else:
        messages.warning(request, u'Некорректное действие для подтверждения.')


@permission_required('sign', Equipment, 'equip_id')
def equipment_sign(request, equip_id):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)
    _do_sign(request, theEquipment, theEquipment.department.organization, u'об устройстве')
    theEquipment.save()
    if theEquipment.is_signed:
        for link in theEquipment.item_links.all():
            link.equipment.status = theEquipment.status
            link.equipment.save()

    return browse.card(request, equip_id)

@permission_required('sign', EquipmentAct, 'act_id')
def activity_sign(request, act_id):
    theAct = get_object_or_404(EquipmentAct, pk=act_id)
    theOrg = theAct.equipment.department.organization
    _do_sign(request, theAct, theOrg, u'о результате')
    theAct.save()
    return results(request, theAct.equipment.id)


@user_passes_test(lambda u: bool(departments_ids_user_has_permission_in(u, 'confirm_equipment')))
def equipment_own(request, equip_id):
    theEquipment = get_object_or_404(Equipment, pk=equip_id)
    if request.method == 'PUT':
        if theEquipment.creator:
            messages.error(request, u'У устройства уже задан владелец.')
        else:
            theEquipment.creator = request.user
            messages.success(request, u'Установлен владелец устройства.')
    elif request.method == 'DELETE':
        theEquipment.creator = None
        messages.success(request, u'Владелец устройства удалён.')
    else:
        messages.warning(request, u'Некорректное действие.')
    theEquipment.save()

    return browse.card(request, equip_id)
