$(function() {

   // remove submit on enter for worker autocomplete field
    $(".autocomplete_workers, .autocomplete_publication").live("keypress", function(e){
        if(e.which == 13) {
            e.preventDefault();
            return false
        }
        return true
    });


    function generate_results_autocomplete_options(source){
        return {
            minLength: 0,
            source: function(request, response){
                $.getJSON(source, {
                    term: extractLast(request.term)
                }, response);
            },
            search: function(){
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 5) {
                    return false;
                }
            },
            focus: function(){
                return false
            },
            select: function(event, ui){
                //$(this).closest('div .form').find('.p_activity_id').value = ui.item.value.id;
                //$(this).closest('div .form').find('.p_activity_type').value = ui.item.value.type;
                $('.p_activity_id')[0].value = ui.item.value.id;
                $('.p_activity_type')[0].value = ui.item.value.type;
                this.value = ui.item.value.citation;
                return false;
            }
        }
    }

$(document).ready(function() {
  setTimeout(function() {
     var selectedNode = $('#id_department').dynatree('getTree')
     .getNodeByKey($("#id_department_checkboxes").val().split(",")[0]);
     if (selectedNode)
      selectedNode.li.scrollIntoView();
  }, 50)
})

    // add autocomplete for workers and other fields
    $(".autocomplete_publication").autocomplete(generate_results_autocomplete_options("/equipment/search/pub/"));

    $('#id_buydate, #id_warrantydate, #id_startdate').datepicker({ changeMonth: true, changeYear: true,
                                                                   yearRange: '1930:2050' })

});
