# -*- coding: utf-8; -*-
from django import forms #, ModelForm, CharField, ModelChoiceField
#from django import forms
from django.views.generic import DetailView
from django.forms.widgets import TextInput, Textarea
from models import PNR, JournalRubricPNR, PNRPlan
from common.forms import MyModelForm
from organizations.models import Department

class PNRForm(MyModelForm):
    pnrmsu = forms.ModelChoiceField(
        label=u'Приоритетные направления развития МГУ до 2020 года',
        required=False,
        queryset=JournalRubricPNR.objects.filter(categorization__code='PNR-MSU'))
    pnrrf = forms.ModelChoiceField(
        label = u'Приоритетные направления (ПН) развития  науки, технологий и техники в Российской Федерации',
        required=False,
        queryset=JournalRubricPNR.objects.filter(categorization__code='PNR-RF'))
    tprf = forms.ModelChoiceField(
        label = u'Приоритетные направления модернизации и технологического развития (технологического прорыва ТП) экономики России',
        required=False,
        queryset=JournalRubricPNR.objects.filter(categorization__code='TP-RF'))
    nomenkl = forms.ModelChoiceField(
        label = u'Шифр отрасли науки',
        required=False,
        queryset=JournalRubricPNR.objects.filter(categorization__code='NOMENKL').order_by('code', 'name'))
    actual_date = forms.DateField(label=u'Окончание срока ПННИ',required=False)

    class Meta:
        model = PNR
        
    def __init__(self, *args, **kwargs):
        super(PNRForm, self).__init__(*args, **kwargs)

class PNRPlanForm(PNRForm):
    
    class Meta:
        model = PNRPlan
        fields = ('pnrmsu', 'pnrrf', 'tprf', 'nomenkl','actual_date','planYear','totalTopics','gosTopics','nsStaffer','nsSovmestit','nsNotBudget','supportStaffer','supportSovmestit','supportNotBudget','ppsStaffer','ppsSovmestit','ppsNotBudget')
    def __init__(self, *args, **kwargs):
        super(PNRPlanForm, self).__init__(*args, **kwargs)
        self.fields['planYear'].widget.attrs['readonly'] = True
        if len(args) == 0:
            try:
                plan = PNRPlan.objects.get(pnr=kwargs['instance'],planYear=kwargs['initial']['planYear'])
            except:
                return
            if plan:
                self.fields['totalTopics'].initial = plan.totalTopics
                self.fields['gosTopics'].initial = plan.gosTopics
                self.fields['nsStaffer'].initial = plan.nsStaffer
                self.fields['nsSovmestit'].initial = plan.nsSovmestit
                self.fields['nsNotBudget'].initial = plan.nsNotBudget
                self.fields['supportStaffer'].initial = plan.supportStaffer
                self.fields['supportSovmestit'].initial = plan.supportSovmestit
                self.fields['supportNotBudget'].initial = plan.supportNotBudget
                self.fields['ppsStaffer'].initial = plan.ppsStaffer
                self.fields['ppsSovmestit'].initial = plan.ppsSovmestit
                self.fields['ppsNotBudget'].initial = plan.ppsNotBudget

class PNRAddForm(PNRForm):
    department = forms.ModelChoiceField(label=u'Подразделение', empty_label=None, queryset=Department.objects.all())
    name = forms.CharField(label=u'Название ПННИ', required=True, widget=forms.Textarea())
    
    class Meta:
        model = PNRPlan
        fields = ('department','name','pnrmsu', 'pnrrf', 'tprf', 'nomenkl','actual_date','planYear','totalTopics','gosTopics','nsStaffer','nsSovmestit','nsNotBudget','supportStaffer','supportSovmestit','supportNotBudget','ppsStaffer','ppsSovmestit','ppsNotBudget')
    def __init__(self, *args, **kwargs):
        super(PNRAddForm, self).__init__(*args, **kwargs)
        self.fields['planYear'].widget.attrs['readonly'] = True
        try:
            depids = kwargs['initial']['departments']
            self.fields["department"].queryset = Department.objects.filter(id__in=depids)
            self.fields["department"].initial = depids[0]
        except:
            pass


WORK_CATEGORIES = (
    ('all', u'Любой'),
    ('article', u'Статья'),
    ('book', u'Книга (монография, учебник)'),
    ('project', u'Проект (НИР, договор, ...)')
    )

class ExclusionForm(forms.Form):
    author_name = forms.CharField(label=u'Фамилия автора', required=False)
    work_category = forms.ChoiceField(label=u'Тип работы', choices=WORK_CATEGORIES, required=True)
    title = forms.CharField(label=u'Название работы', required=False)
