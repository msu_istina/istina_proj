# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
import datetime
from django.contrib import messages

from common.utils.user import get_worker
from workers.decorators import worker_required
from organizations.models import Department
from organizations.utils import is_representative_for
from django.template.loader import render_to_string
from django.shortcuts import render_to_response
from common.models import  Attachment
from django.core.files import File
from django.contrib.contenttypes.models import ContentType
from django.utils import simplejson
from django.core.files.temp import NamedTemporaryFile
import os.path, time

from django.db import connection, transaction
from collections import namedtuple

from django.db import connection, transaction
from django.db.models import Q
from organizations.utils import is_representative_for
from django.contrib.auth.models import User

from unified_permissions import has_permission_to_department, has_permission
from unified_permissions.models import AllGrantedPermissions

from projects.models import PNRRubricMembership,PNRPlan

from scireport.models import PNR, SciReportStatus, JournalRubricPNR
from scireport.forms import PNRPlanForm,PNRForm,PNRAddForm
from scireport.utils import default_report_year

from sqlreports import generate_adata
from depadmin.utils import check_access_permissions
import sys
import logging
logger = logging.getLogger('sentry_debug')

SCIREPORT_ADMINS = ['korsh', 'smamakina', 'Morganna', 'safonin', 'S.Y.Egorov',
                    'Reznikova_Alexandra','SvetaZyk', 'NSPetuhova', 'KlimentovaEA',
                    'obogomol', 'burmistova', 'GaldinaAS', 'karpushina']

def get_sponsors_ids():
    result = {}

    cursor = connection.cursor()
    code_name = {'RFFI':  u'грант РФФИ',
                 'RGNF':  u'грант РГНФ',
                 'RUGRANT': u'другие гранты РФ',
                 'FCP': u'ФЦП: Федеральная целевая программа',
                 'NTP': u'НТП: Научно-техническая программа',
                 'DOG': u'Хоздоговор',
                 'MIN': u'Контракт с гос. корпорациями, министерствами и ведомствами, кроме ФЦП',
                 'FOREING': u'Международная организация/программа',
                 'G_PRESIDENT_RF': u'грант Президента РФ',
                 'G_RNF': u'грант РНФ',
                 }
    for code in code_name:
        name = code_name[code]
        result[code] = -1
        try:
            cursor.execute("""
            select f_sponsortype_id
            from sponsortype where f_sponsortype_name=%s
            """, [name])
            for row in list(cursor):
                result[code] = row[0]
        except:
            pass
    cursor.close()
    return result

def user_has_access(user, dep, for_admin_actions = False):
    if user.is_superuser\
           or is_representative_for(user, dep)\
           or user.username in SCIREPORT_ADMINS:
        return True
    if for_admin_actions:
        return False
    if dep:
        return has_permission_to_department(user, 'review_depreports', dep)

    return has_permission(user, 'review_depreports')

def get_top_departments_ids(org_id=214524):
    result = list()

    cursor = connection.cursor()
    cursor.execute("""
    select department.f_department_id
    from department, departmentlink
    where department.f_department_id = departmentlink.f_department_id (+)
      and department.f_organization_id = %s
      and departmentlink.f_department_id is null
    """, [org_id])
    for row in cursor:
        result.extend(row)
    cursor.close()
    return result

def get_isTop(dep_id,org_id=214524):
    isTop = False
    if int(dep_id) in get_top_departments_ids(org_id):
        isTop = True
    return isTop

@worker_required
def index_page(request, worker):
    depids = []
    deps = []

    user = request.user
    is_reports_admin = user.is_superuser or user.username in SCIREPORT_ADMINS
    if is_reports_admin:
        dset = worker.current_main_departments_ids
        depids.extend(dset)
        depids.extend( get_top_departments_ids() )
    # Check granted permitions
    granted = AllGrantedPermissions.objects.current \
          .filter(Q(user=user),
                  Q(role_name__in=('review_depreports', 'is_representative_for'))) \
          .values_list('department', flat=True)

    depids.extend(granted)
    depids = list(set(depids))
    deps = Department.objects.filter(id__in=depids)

    depadmin_permissions = check_access_permissions(user, 'reports_report')

    return render(request, "scireport/index.html",
                  {"deps": deps,
                   "sponsors": get_sponsors_ids(),
                   "depadmin_permissions": depadmin_permissions,
                   "is_reports_admin": is_reports_admin,
                   })


def user_can_unsign_report(user, status):
    "Проверяет, что пользователь может отменить подпись отчета подразделения."
    if status.department.is_root:
        return user.is_superuser
    parent = status.department.current_parents[0]
    if has_permission(user, "is_representative_for", parent):
        return True
    return False

@login_required
def department_scireports(request, depid, report_year=None):
    user = request.user
    dep = None
    has_access = False
    if str(depid) == '-1':
        if user.is_superuser or user.username in SCIREPORT_ADMINS:
            has_access = True
    else:
        dep = get_object_or_404(Department, id=depid)
        has_access = user_has_access(user, dep)
    can_revoke = False
    if not has_access:
        messages.warning(request, u'У Вас нет прав доступа к отчетам подразделения "%s".' % dep or u'')
        return redirect('scireport_index')

    sign_type=0
    if request.method == 'POST':
        try:
            sign_type = int(request.POST.get('sign_type', '0'))
            report_year = int(request.POST.get('report_year', None))
        except Exception:
            sign_type=0
            report_year = None

    if sign_type == 3:
        messages.info(request, u"Вы просматриваете отчет, построенный только по утвержденным данным.")
    else:
        messages.warning(request, u"Внимание! Вы просматриваете отчет по неутвержденным данным.")

    report_year = report_year or default_report_year()
    report_was_signed = False
    sign_info = None
    can_unsign = False
    if depid == '-1':
        report_was_signed = True
    elif SciReportStatus.objects.filter(department=dep, year=report_year).exists():
        sign_info = SciReportStatus.objects.filter(department=dep, year=report_year)[0]
        can_unsign = user_can_unsign_report(user, sign_info)
        report_was_signed = True

    current_year = datetime.datetime.today().year
    possible_report_years = map(str, range(current_year, 2009, -1))


    filename = "dep_scireport_%s_%s_%s_%s.pdf" % (str(depid), str(sign_type), str(-1), str(report_year))
    _filename = ''
    _created_ts = None
    attachments = Attachment.objects.filter(
        content_type=ContentType.objects.get_for_model(Department), object_id=depid)
    for attachment in attachments:
        if filename in attachment.link.url:
            _filename = attachment.link.url;
            _created_ts = time.strftime("%d.%m.%Y %H:%M:%S", time.localtime(os.path.getmtime(attachment.link.file.name)));
            break;

    return render(request, "scireport/reports_index.html",
                  {"dep": dep,
                   "depid": depid,
                   "sponsors": get_sponsors_ids(),
                   "adata": generate_adata(request),
                   "sign_type": sign_type,
                   "report_was_signed": report_was_signed,
                   "sign_info": sign_info,
                   "can_unsign": can_unsign,
                   "report_year": str(report_year),
                   "next_year": str(int(report_year)+1),
                   "possible_report_years": possible_report_years,
                   "isTop": get_isTop(depid),
                   "sci_report_pdf_url": _filename,
                   "sci_report_pdf_created_date":_created_ts
                   })


from sqlreports.views import run_report
from sqlreports.models import SQLReport
from common.utils.latex import latex_to_pdf
from django.template.loader import render_to_string
from django.conf import settings # to distinguish between development and production
@login_required
def create_department_pdf(request, depid, sign_type=0, manlist_id=-1, report_year=None, pdf_in_response=1):
    user = request.user
    dep = None
    if depid == '-1' or depid == -1:
        has_access = (user.is_superuser or user.username in SCIREPORT_ADMINS)
    else:
        dep = get_object_or_404(Department, id=depid)
        has_access = user_has_access(user, dep)

    if not has_access:
        messages.warning(request, u'У Вас нет прав доступа к отчетам подразделения "%s".' % dep or u'')
        return redirect('scireport_index')

    report_year = report_year or default_report_year()

    default_params = {}
    default_params["f_department_id"] = str(depid)
    #-- FIXME: generate dynamically
    default_params["manlist_id"] = str(manlist_id)
    default_params["f_organization_id"] = '214524' # MSU ID
    default_params["sign_type"] = str(sign_type)
    default_params["report_year"] = str(report_year)
    default_params["year"] = str(report_year)

    reports_restriction = None
    if settings.DEBUG or user.is_superuser:
        default_params["manlist_id"] = '-1'
        if settings.DEBUG: # development database
            reports_restriction = [#'editorial_board', 'proj_foreign_org', 'proj_foreign_grant', 'conf_foreign', 'traineeship_out', 'traineeship_in',
                #'pnr_fin',
                #'pnr_performance',
                #'patents',
                #'patents_list',
                #'nirs',
                'books_list',
                #'confs_organized',
                #'confs_committee',
                #'textbooks_count',
                                   #'teaching_out',
                #'foreign_awards', 'socmembers',
                #'honorary_members',
                #'intercoop_indicators',
                #'teaching_in',
                ]
            default_params["f_department_id"] = '-1'
        elif user.is_superuser:
            pass


    # maps names to tuples of the form (report_name, dictionary of report's extra parameters)
    reports_to_run = {
        'pnr_performance': ('pnr', None),
        'pnr_fin': ['pnr_fin', None],
        # section 2
        'awards': ('awards', None),
        'awards_list': ('awards_list', None),
        'pubs_overview': ('public_list', {'postkind_list': '0'}),
        'pubs_textbooks': ('textbooks', {'russian': '0'}),
        'textbooks_count': ('textbooks_count', {'russian': '0'}),
        'books_list': ('books', {'russian': '0', 'if_science': '0', 'postkind_list': '0', 'if_shtat': '0', 'exclude_textbooks': 'no'}), #+
        'editorial_board': ('editorial_board', None),
        'nirs': ('nirs', None),
        'phd': ('podgotovka_kadrov', None), #+
        'phd_list': ('disser_man', None),   #+
        'confs': ('conf_doklad', None),
        'confs_organized': ('org_conf', None),
        'confs_committee': ('org_conf_man', None),
        'patents': ('patents_tab26', None),
        'patents_list': ('patent_list', None),
        # section 3
        'proj_total_fin': ('proj_total_fin', None),
        'proj_fin_grants_rf': ('proj_fin',
                               {'f_sponsortype_id': '5217040, 5217042, 5328822, 8045520, 5217044'}),
        'proj_fin_fcp_ntp': ('proj_fin',
                             {'f_sponsortype_id': '5217046, 5217048'}),
        'proj_fin_ministry': ('proj_fin',
                              {'f_sponsortype_id': '5217050'}),
        'proj_fin_agreements': ('proj_fin',
                              {'f_sponsortype_id': '5217052'}),
        'proj_fin_foreign': ('proj_fin',
                              {'f_sponsortype_id': '5217054'}),
        'proj_fin_other': ('proj_fin_3_7', None),
        'proj_fin_by_social_topics': ('proj_fin_3_8', None),
        # section 4
        'proj_foreign_org': ('proj_foreign_org', None),
        'proj_foreign_grant': ('proj_foreign_grant', None),
        'foreign_comp': ('foreign_comp', None),
        'conf_foreign': ('conf_foreign', None),
        'traineeship_out': ('traineeship_out', {'to_msu': 0}),
        'traineeship_in': ('traineeship_out', {'to_msu': 1}),
        'teaching_out': ('courceforeign', {'to_msu': 0}),
        'teaching_in': ('courceforeign', {'to_msu': 1}),
        'foreign_awards': ('award_foreign_dep', None),
        'socmembers': ('socmemeber_foreign_dep', None),
        'honorary_members': ('honors_foreign_dep', None),
        'intercoop_indicators': ('tab_4_7', None),
        }
    data = {}

    for repname in reports_to_run.keys():
        #-- FIXME: Remove this DEBUG code
        if reports_restriction and repname not in reports_restriction:
            continue
        params = default_params.copy()
        report_code, extra_params = reports_to_run[repname]
        if extra_params:
            for p, val in extra_params.iteritems():
                params[p] = val

        try:
            report = SQLReport.objects.get(code=report_code)
            data[repname] = run_report(report, params)
            if settings.DEBUG:
                print data[repname].data
        except Exception as e:
            data[repname] = None
            logger.error(u'scireport: error run_report for %s' % (repname), exc_info=sys.exc_info(),
                         extra={'sqlrep_report': report, 'sqlrep_params': params, 'sqlrep_error': e})

    context = {}
    context["dep"] = dep
    context["reports"] =  data
    context["report_year"] = report_year

    latex = render_to_string("scireport/department_pdf.tex", context)
    pdf = latex_to_pdf(latex)


    #here we should create attachment and link it to department


    tempfile = NamedTemporaryFile(delete=True)
    tempfile.write(pdf)
    tempfile.flush()

    filename = u"dep_scireport_%s_%s_%s_%s.pdf" % (str(depid), str(sign_type), str(manlist_id), str(report_year))



    attachments = Attachment.objects.filter(
        content_type=ContentType.objects.get_for_model(Department), object_id=depid)
    for attachment in attachments:
        if filename in attachment.link.url:
            attachment.delete();



    attachment = Attachment.objects.create(
        link=File(tempfile, name=filename),
        content_type=ContentType.objects.get_for_model(Department), object_id=depid)

    #end region for attachment

    return HttpResponse(simplejson.dumps({'attachment_url': ''}), content_type='application/javascript')

@login_required
def get_department_pdf(request, depid, sign_type=0, manlist_id=-1, report_year=None, pdf_in_response=1):
    user = request.user
    dep = None
    if depid == '-1' or depid == -1:
        has_access = (user.is_superuser or user.username in SCIREPORT_ADMINS)
    else:
        dep = get_object_or_404(Department, id=depid)
        has_access = user_has_access(user, dep)

    if not has_access:
        messages.warning(request, u'У Вас нет прав доступа к отчетам подразделения "%s".' % dep or u'')
        return redirect('scireport_index')

    report_year = report_year or default_report_year()





    filename = u"dep_scireport_%s_%s_%s_%s.pdf" % (str(depid), str(sign_type), str(manlist_id), str(report_year))

    # here we try to find report. if we find it -- return this is response. If not -- we should generate new one and return this to response.
    attachments = Attachment.objects.filter(
        content_type=ContentType.objects.get_for_model(Department), object_id=depid)
    for attachment in attachments:
        if filename in attachment.link.url:
            txt = attachment.link.read()
            response = HttpResponse(txt, mimetype='application/pdf')
            response['Content-Disposition'] = 'attachment; filename=' + filename
            return response


    return None




# NOMENKL, PNR-RF, PNR-MSU, TP-RF

from unified_permissions.models import AllGrantedPermissions

@login_required
def pnreditor(request,dep_id=None):
    user = request.user
    pnrs = None
    departments = []
    result = list()
    if not dep_id:
        if request.method == 'GET' and request.GET.get('f_department_id', None):
            dep_id = request.GET.get('f_department_id', None)
    if not dep_id:
        return redirect('scireport_index')
    departments.append(dep_id)
    department = Department.objects.get(pk=dep_id)
    top_list = get_top_departments_ids()
    if int(dep_id) in top_list:
        isTop = True
    else:
        isTop = False
    isPdf = False
    if request.method == 'GET' and request.GET.get('output', None):
        isPdf = True
    if isPdf:
        typePdf = request.GET.get('output', None)
        context = {}
        context["dep"] = department
        params = {}
        params['report_year'] = request.GET.get('report_year', '2016')
        params['f_department_id'] = dep_id
        params['sign_type'] = '0'
        context["report_year"] = request.GET.get('report_year', '2016')
        if typePdf == 'pdf1':
            try:
                report = SQLReport.objects.get(code='pnr_plan_1')
                plan = run_report(report, params)
            except Exception as e:
                plan = None
            context["plan"] = plan
            latex = render_to_string("pnreditor/pnr_plan_1.tex", context)
            pdf = latex_to_pdf(latex)
            response = HttpResponse(pdf, mimetype='application/pdf')
            response['Content-Disposition'] = 'attachment; filename=pnr_plan_1_' + str(dep_id) + '.pdf'
        else:
            params['f_organization_id'] = '214524'
            try:
                report = SQLReport.objects.get(code='pnr_plan_2')
                plan = run_report(report, params)
            except Exception as e:
                plan = None
            context["plan"] = plan
            latex = render_to_string("pnreditor/pnr_plan_2.tex", context)
            pdf = latex_to_pdf(latex)
            response = HttpResponse(pdf, mimetype='application/pdf')
            response['Content-Disposition'] = 'attachment; filename=pnr_plan_2_' + str(dep_id) + '.pdf'

        return response
    if isTop:
        pnrs = PNR.objects.filter(department__in=departments).prefetch_related('rubrics')
    else:
        cursor = connection.cursor()
        cursor.execute("""
        select rownum, f_pnr_name from pnr where f_department_id in (select parent_f_department_id from all_parent_department where f_department_id = %s) 
        """, [dep_id])
        for row in cursor:
            t = {"NN":row[0], "name":row[1]}
            result.append(t)
        cursor.close()

    return render(request, "pnreditor/list.html",
                  {"pnrs": pnrs,
                   "result": result,
                   "isTop": isTop,
                   "dep_id": dep_id,
                   "department": department,
                   "message": ""})


@login_required
def editpnr(request, pnrid):
    thepnr = get_object_or_404(PNR, pk=pnrid)
    old_date = thepnr.actual_date
    if request.method == 'POST':
        post = request.POST
        form = PNRPlanForm(post, instance=thepnr)
        new_date = post['actual_date']
        try:
            if new_date:
                new_date = datetime.date(int(new_date[6:10]),int(new_date[3:5]),int(new_date[0:2]))
            else:
                new_date = None
        except:
            new_date = None
        if new_date <> old_date:
            PNR.objects.filter(id=pnrid).update(actual_date=new_date,creator=request.user)
        try:
            plan = PNRPlan.objects.get(pnr=thepnr,planYear=post['planYear'])
        except:
            plan = PNRPlan(pnr=thepnr,planYear=post['planYear'])
        plan = fillplan(post,plan)
        plan.save()
 
        PNRRubricMembership.objects.filter(pnr = thepnr).delete()
        for fld in ['pnrmsu', 'pnrrf', 'tprf', 'nomenkl']:
            if post[fld] <> '':
                r = JournalRubricPNR.objects.get(pk=post[fld])
                rublink = PNRRubricMembership.objects.create(pnr = thepnr, journal_rubric = r)
                rublink.save()
        return pnreditor(request,dep_id=int(post['dep_id']))
    else:
        rublinks = PNRRubricMembership.objects.filter(pnr=thepnr)
        init = {}
        code_to_field = {'PNR-MSU': 'pnrmsu',
                         'PNR-RF': 'pnrrf',
                         'TP-RF': 'tprf',
                         'NOMENKL': 'nomenkl'}
        for rublink in rublinks:
            rub = rublink.journal_rubric
            if code_to_field.get(rub.categorization.code, None) is not None:
                init[code_to_field[rub.categorization.code]] = rub.id
        init['actual_date'] = thepnr.actual_date 
        init['planYear'] = 2016
        form = PNRPlanForm(instance=thepnr, initial = init)
#    children = PNR.objects.filter(parent = thepnr)
    return render(request, "pnreditor/edit_pnr.html",
                  {"form": form,
                   "pnr": thepnr,
#                   "children": children,
                   "message": ''})

@login_required
def pnradd(request,dep_id=None):
    if not dep_id:
        return redirect('scireport_index')
    user = request.user
    worker = get_worker(user)
    departments = []
    departments.append(dep_id)
    init = {}
    init['planYear'] = 2016
    init['departments'] = departments
    if request.method == 'POST':
        post = request.POST
        form = PNRAddForm(post, instance=None, initial = init)
        new_date = post['actual_date']
        try:
            if new_date:
                new_date = datetime.date(int(new_date[6:10]),int(new_date[3:5]),int(new_date[0:2]))
            else:
                new_date = None
        except:
            new_date = None
        departments_id=post['department']
        department = Department.objects.filter(pk=departments_id)[0]

#        thepnr = PNR.objects.create(name=name,department=department,actual_date=new_date,creator=user)
        thepnr = PNR(name=post['name'],department=department,actual_date=new_date,creator=user)
        thepnr.save()
        plan = PNRPlan(pnr=thepnr,planYear=post['planYear'])
        plan = fillplan(post,plan)
        plan.save()
         
        for fld in ['pnrmsu', 'pnrrf', 'tprf', 'nomenkl']:
            if post[fld] <> '':
                r = JournalRubricPNR.objects.get(pk=post[fld])
                rublink = PNRRubricMembership.objects.create(pnr = thepnr, journal_rubric = r)
                rublink.save()
        return pnreditor(request,dep_id=dep_id)
    else:
        form = PNRAddForm(instance=None, initial = init)
    return render(request, "pnreditor/add_pnr.html",
                  {"form": form,
                   "dep_id": dep_id,
                   "message": ''})

def fillplan(post, plan):
    try:
        plan.totalTopics = int(post['totalTopics'])
    except:
        plan.totalTopics = 0
    try:
        plan.gosTopics = int(post['gosTopics'])
    except:
        plan.gosTopics = 0
    try:
        plan.nsStaffer = int(post['nsStaffer'])
    except:
        plan.nsStaffer = 0
    try:
        plan.nsSovmestit = int(post['nsSovmestit'])
    except:
        plan.nsSovmestit = 0
    try:
        plan.nsNotBudget = int(post['nsNotBudget'])
    except:
        plan.nsNotBudget = 0
    try:
        plan.supportStaffer = int(post['supportStaffer'])
    except:
        plan.supportStaffer = 0
    try:
        plan.supportSovmestit = int(post['supportSovmestit'])
    except:
        plan.supportSovmestit = 0
    try:
        plan.supportNotBudget = int(post['supportNotBudget'])
    except:
        plan.supportNotBudget = 0
    try:
        plan.ppsStaffer = int(post['ppsStaffer'])
    except:
        plan.ppsStaffer = 0
    try:
        plan.ppsSovmestit = int(post['ppsSovmestit'])
    except:
        plan.ppsSovmestit = 0
    try:
        plan.ppsNotBudget = int(post['ppsNotBudget'])
    except:
        plan.ppsNotBudget = 0
    return plan
    
@login_required
def addchild(request, pnrid):
    thepnr = get_object_or_404(PNR, pk=pnrid)
    if request.method == 'POST' and request.POST.get('name', None):
        name = request.POST.get('name', None)
        child = PNR.objects.create(creator=request.user,
                                   parent = thepnr,
                                   name = name)
        child.save()
        pnrid = child.id
    return redirect("editpnr", pnrid=pnrid)



@login_required
def report_completed(request, depid, report_year=None):
    dep = get_object_or_404(Department, id=depid)
    if not report_year:
        report_year = default_report_year()

    if not has_permission(request.user, "is_representative_for", dep):
        messages.error(request, u"У Вас нет прав для выполнения данной операции")
    elif request.method == 'POST':
        if SciReportStatus.objects.filter(department=dep, year=report_year).exists():
            messages.info(request, u"Отчёт уже подписан.")
        else:
            status = SciReportStatus.objects.create(department=dep,
                                                    year=report_year,
                                                    creator=request.user,
                                                    date=datetime.datetime.now().replace(microsecond=0))
            messages.success(request, u"Спасибо. Информация сохранена.")
        return redirect("department_scireports", depid=depid)
    filename = "dep_scireport_%s_%s_%s_%s.pdf" % (str(depid), str(3), str(-1), str(report_year))

    attachments = Attachment.objects.filter(
        content_type=ContentType.objects.get_for_model(Department), object_id=depid)
    _filename = ''
    _created_ts = None
    for attachment in attachments:
        if filename in attachment.link.url:
            _filename = attachment.link.url;
            _created_ts = time.strftime("%d.%m.%Y %H:%M:%S", time.localtime(os.path.getmtime(attachment.link.file.name)));
            break;
    return render(request, "scireport/confirm_completion.html",
                  {"dep": dep,
                   "report_year": report_year,
                    "sci_report_pdf_url": _filename,
                    "sci_report_pdf_created_date":_created_ts
                   })


@login_required
def report_unsign(request, depid, statusid):
    dep = get_object_or_404(Department, id=depid)
    statuses = SciReportStatus.objects.filter(department=dep, pk=statusid)
    if statuses:
        status = statuses[0]
        if user_can_unsign_report(request.user, status):
            if request.method == 'POST':
                status.delete()
                messages.info(request, u"Статус отчета 'подписан' был отменён.")
            else:
                return render(request, "scireport/confirm_unsignment.html",
                              {"status": status,
                               "department": dep})
        else:
            messages.error(request, u"У Вас нет прав для изменения статуса данного отчёта.")
    else:
        messages.error(request, u"Нет такого отчета.")
    return redirect("department_scireports", depid)

