# -*- coding: utf-8; -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages

from django.http import HttpResponse, HttpResponseForbidden
from django.utils import simplejson
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from collections import namedtuple

from unified_permissions import has_permission_to_department, has_permission
from unified_permissions.models import AllGrantedPermissions

from publications.models import Article, Book
from projects.models import Project
from common.models import ObjectList, ObjectListItem
from organizations.models import Department, NestedParentDepartment

from scireport.forms import ExclusionForm

CATEGORY_TO_MODEL = {
    'article': Article,
    'book': Book,
    'project': Project
    }

ActivityInfo = namedtuple('ActivityInfoTuple', 'title, id, category, category_name, url')


def get_stoplist(department, year, user):
    """Find or create a stoplist associated with the DEPARTMENT's report for YEAR.
    USER parameter used for logging stop-list creator only."""
    years_str = str(year)
    object_list = None
    try:
        object_list = ObjectList.objects.get(name=u"Исключения", category=u"Стоп-листы", code=years_str, department=department)
    except ObjectList.DoesNotExist:
        object_list, _ = ObjectList.objects.get_or_create(
            name=u"Исключения", category=u"Стоп-листы", code=years_str, user=user, department=department)
    return object_list

def list_excluded(department, year, user):
    object_list = get_stoplist(department, year, user)
    excluded_works = []
    for item in object_list.items.all():
        obj = item.content_type.get_object_for_this_type(pk=item.object_id)
        #content_type, class_name
        category_name = item.class_name
        excluded_works.append(ActivityInfo(obj.title, item.object_id, 'category', category_name, obj.get_absolute_url()))
    return excluded_works

@login_required
def report_exclude(request, depid, year):
    dep = get_object_or_404(Department, id=depid)

    if not has_permission_to_department(request.user, "confirm_personal_reports", dep):
        messages.error(request, u"У Вас нет прав для редактирования списка исключенных работ")
        return redirect("department_scireports_year", dep.id, year)

    exclusion_form = ExclusionForm(request.POST or None)

    selected_activities = []
    subdepartments_qs = NestedParentDepartment.objects.filter(parent=dep).values_list('department')
    signed_reports = ObjectList.objects.signed_reports(year=year).filter(department__in=subdepartments_qs)
    stop_lists = ObjectList.objects.filter(name=u"Исключения", category=u"Стоп-листы", code=str(year), department__in=subdepartments_qs)
    stop_lists_items = ObjectListItem.objects.filter(object_list__in=stop_lists).values('object_id')
    report_objects = ObjectListItem.objects.filter(object_list__in=signed_reports) \
                     .exclude(object_id__in=stop_lists_items) \
                     .values('object_id')

    mode = "do_exclude"
    if request.method == 'POST' and exclusion_form.is_valid():
        title = exclusion_form.cleaned_data['title']
        authors = exclusion_form.cleaned_data['author_name']
        category = exclusion_form.cleaned_data['work_category']

        if category in ['all', 'article']:
            articles = Article.objects.filter(year=year, id__in=report_objects)
            if title:
                articles = articles.filter(title__icontains=title)
            if authors:
                articles = articles.filter(Q(authors__lastname__iexact=authors))
                # | Q(authorships__original_name__icontains=authors)
            #articles.prefetch_related('formatted_references__reference')
            selected_activities.extend( [ActivityInfo(a.formatted_references.all()[0].reference, a.id, 'article', u'Статья', a.get_absolute_url()) for a in articles[:50]] )
        if category in ['all', 'book']:
            books = Book.objects.filter(year=year, id__in=report_objects)
            if title:
                books = books.filter(title__icontains=title)
            if authors:
                books = books.filter(Q(authors__lastname__iexact=authors))
            selected_activities.extend( [ActivityInfo(b.get_citation(short=True), b.id, 'book', u'Книга', b.get_absolute_url()) for b in books[:50]] )
        if category in ['all', 'project']:
            projects = Project.objects.filter(id__in=report_objects)
            if title:
                projects = projects.filter(name__icontains=title)
            if authors:
                projects = projects.filter(members__lastname__iexact=authors)
            selected_activities.extend( [ActivityInfo(p.name, p.id, 'project', u'Проект', p.get_absolute_url()) for p in projects[:50]] )

    if request.method == 'GET':
        mode = "browse_excluded"
        selected_activities = list_excluded(dep, year, request.user)

    return render(request, "scireport/exclude.html",
                  {"department": dep,
                   "exclusion_form": exclusion_form,
                   "selected_activities": selected_activities,
                   "year": year,
                   "mode": mode,
                   })

@login_required
def xhr_report_exclude_item(request, depid, year, category, itemid):
    if request.method != 'POST':
        return HttpResponseForbidden("Incorrect request for item exclusion", content_type='text/plain')
    
    department = get_object_or_404(Department, id=depid)
    if not has_permission_to_department(request.user, "confirm_personal_reports", department):
        return HttpResponseForbidden(u"Недостаточно прав для выполнения данной операции", content_type='text/plain')
    
    object_list = get_stoplist(department, year, request.user)

    model = CATEGORY_TO_MODEL[category]
    activity = model.objects.get(pk=itemid)

    objectitem = ObjectListItem.objects.get_or_create(object_list=object_list, object_id=activity.id,
                                                      class_name=ContentType.objects.get_for_model(activity).name,
                                                      content_type=ContentType.objects.get_for_model(activity),
                                                      title=activity.title)
    message = simplejson.dumps("OK")
    return HttpResponse(message)


@login_required
def xhr_report_undo_exclude_item(request, depid, year, category, itemid):
    if request.method != 'POST':
        return HttpResponseForbidden("Incorrect request for item inclusion", content_type='text/plain')

    department = get_object_or_404(Department, id=depid)
    if not has_permission_to_department(request.user, "confirm_personal_reports", department):
        return HttpResponseForbidden(u"Недостаточно прав для выполнения данной операции", content_type="text/plain")

    object_list = get_stoplist(department, year, request.user)

    #model = CATEGORY_TO_MODEL[category]
    #activity = model.objects.get(pk=itemid)

    try:
        #objectitem = ObjectListItem.objects.get(object_list=object_list, object_id=activity.id,
        #                                        class_name=ContentType.objects.get_for_model(activity).name,
        #                                        content_type=ContentType.objects.get_for_model(activity))
        objectitem = ObjectListItem.objects.get(object_list=object_list, object_id=itemid) #--- FIXME(serg) Check that category matches
        objectitem.delete()
        message = "OK"
        return HttpResponse(message, content_type='text/plain')
    except ObjectListItem.DoesNotExist:
        message = u"Нет такого объекта. Возможно он был добавлен в список исключений дочернего подразделения."
    except ObjectListItem.MultipleObjectsReturned:
        message = u"Объект не может быть удален (в списке присутствует несколько записей с совпадающими параметрами)."
    return HttpResponseForbidden(message, content_type='text/plain')
