# -*- coding: utf-8 -*-

from django import forms
from workers.models import Position, Employment, Worker
from organizations.models import Department
from django.contrib import messages
from django.db import DatabaseError
from django.db import connection, transaction
from datetime import date
from django.shortcuts import get_object_or_404

from sqlreports.models import CheckboxesProcessor

class bulk_position_assignment(CheckboxesProcessor):
    codename = 'assign_position'
    def get_form_fields(self, report_params, signer):
        dep_id = report_params.get('f_department_id', None)
        if not dep_id:
            dep_id = report_params.get('__checkbox_f_department_id', None)
        fields = {}
        fields["dep"] = forms.CharField(widget = forms.HiddenInput(),
                                        initial = dep_id,
                                        label = '')
        fields["post"] = forms.ModelChoiceField(
            required = False,
            queryset = Position.objects.all(),
            label = u'Должность'
            )
        return fields
    
    def do(cls, request, checkboxes_marked, checkboxes_shown):
        post_id = request.POST.get('post', None)
        dep_id = int(request.POST.get('dep', 0))

        if not post_id:
            messages.error(request, u'Должность не указана.')
            return False
        dep = get_object_or_404(Department, pk=dep_id)
        cursor = connection.cursor()        
        for cb_value in checkboxes_marked:
            man_id, manspost_id = cb_value.split('.')
            man_id = int(man_id)
            try:
                manspost_id = int(manspost_id)
            except ValueError:
                manspost_id = None
            if not manspost_id:
                worker = Worker.objects.get(pk = man_id)
                emp = Employment.objects.create(worker = worker,
                                                department = dep,
                                                creator = request.user,
                                                startdate = date.today())
                emp.save()
                manspost_id = emp.id
            cursor.execute("update manspost set f_post_id = %s where f_manspost_id = %s" % (post_id, manspost_id))
        cursor.close()
