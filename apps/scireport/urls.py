# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *

urlpatterns = patterns('scireport.views',

    # public pages
    url(r'^$', 'index_page', name='scireport_index'),
    url(r'^department/(?P<depid>-?\d+)/$', 'department_scireports', name='department_scireports'),
    url(r'^department/(?P<depid>-?\d+)/(?P<report_year>[12][0-9]{3})/$', 'department_scireports', name='department_scireports_year'),
    url(r'^department/(?P<depid>-?\d+)/pdf/(?P<sign_type>\d+)/(?P<manlist_id>[-]?\d+)/(?P<report_year>\d+)/$', 'get_department_pdf', name='scireport_department_pdf'),
    url(r'^department/(?P<depid>-?\d+)/pdf/(?P<sign_type>\d+)/(?P<manlist_id>[-]?\d+)/(?P<report_year>\d+)/create/$', 'create_department_pdf', name='create_scireport_department_pdf'),

    url(r'^department/(?P<depid>-?\d+)/completed/(?P<report_year>[12][0-9]{3})/$', 'report_completed', name='scireport_report_completed'),
    url(r'^department/unsign/(?P<depid>-?\d+)/(?P<statusid>\d+)/$', 'report_unsign', name='scireport_report_unsign'),

    url(r'^department/(?P<depid>-?\d+)/exclude/(?P<year>\d+)/$', 'report_exclude', name='scireport_exclude'),
    url(r'^department/(?P<depid>-?\d+)/exclude/(?P<year>\d+)/(?P<category>[a-z]+)/(?P<itemid>\d+)/$', 'xhr_report_exclude_item', name='scireport_exclude_item'),
    url(r'^department/(?P<depid>-?\d+)/exclude/(?P<year>\d+)/(?P<category>[a-z]+)/(?P<itemid>\d+)/undo/$', 'xhr_report_undo_exclude_item', name='scireport_undo_exclude_item'),

    url(r'^pnreditor/$', 'pnreditor', name="pnreditor"),
    url(r'^pnradd/(?P<dep_id>\d+)/$', 'pnradd', name='pnradd'),
    url(r'^pnreditor/(?P<pnrid>\d+)/$', 'editpnr', name="editpnr"),
    #url(r'^pnreditor/(?P<pnrid>\d+)/addchild/$', 'addchild', name="addchild"),
)


