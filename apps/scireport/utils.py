# -*- coding: utf-8; -*-
import datetime

def default_report_year():
    report_year = datetime.date.today().year
    if datetime.date.today().month < 6:
        # Пусть отчетный год начинается в июне
        report_year -= 1
    return report_year
