# -*- coding: utf-8 -*-

from datetime import date

from organizations.models import Department
from django.contrib.auth.models import User

from django.db import models, connection

from workers.models import Worker

from django.core.validators import MaxLengthValidator

#from publications.models import Language
#from common.models import AuthoredModel;
from common.models import MyModel
from journals.models import JournalRubric, JournalCategorization

from projects.models import PNR, PNRPlan

class JournalRubricPNR(JournalRubric):
    def __unicode__(self):
        return u"%s: %s" % (self.code, self.name)
    class Meta:
        proxy = True    


class SciReportStatus(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_SCIREPSTATUS_ID")
    creator = models.ForeignKey(to=User, related_name="scireports_signed", db_column="F_SCIREPSTATUS_USER", null=False, blank=False)
    department = models.ForeignKey(to="organizations.Department", related_name="scireports_statuses", verbose_name=u"Подразделение", db_column="F_DEPARTMENT_ID")
    year = models.IntegerField(u"Отчётный год", db_column="F_SCIREPSTATUS_YEAR", blank=False, null=False)
    date = models.DateTimeField(u"Дата присвоения", db_column="F_SCIREPSTATUS_DATE", blank=False, null=False, auto_now_add=False)
    
    class Meta:
        db_table = "SCIREPSTATUS"
        verbose_name = u"Статус годового отчета"
        verbose_name_plural = u"Статусы годовых отчетов"



class Grants(models.Model):
    "Permitions, granted by one user to another"
    id = models.AutoField(primary_key=True, db_column="F_GRANTS_ID")
    creator = models.ForeignKey(to=User, related_name="delegated_permitions",
                                db_column="F_GRANTS_USER",
                                null=True, blank=True)
    userto = models.ForeignKey(to=User, related_name="granted_permitions",
                                db_column="F_GRANTS_USERTO",
                                null=True, blank=True)
    code = models.TextField(u"Код", validators=[MaxLengthValidator(255)],
                            db_column="F_GRANTS_CODE",
                            null=True, blank=True)
    value = models.IntegerField(db_column="F_GRANTS_VAL",
                                verbose_name=u"Параметр",
                                blank=True, null=True)
    date = models.DateTimeField(u"Дата присвоения",
                                db_column="F_GRANTS_CREATE",
                                blank=True, null=True, auto_now_add=False)
    expire_date = models.DateTimeField(u"Дата окончания действия",
                                       db_column="F_GRANTS_CLOSEDATE",
                                       blank=True, null=True,
                                       auto_now_add=False)
    @property
    def is_valid(self):
        if self.expire_date is None or self.expire_date > date.today():
            return True
        return False
    
    class Meta:
        db_table = "GRANTS"
        verbose_name = u"Выданная привеления"
        verbose_name_plural = u"Выданные привелегии"
    
