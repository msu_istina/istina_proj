# -*- coding: utf-8 -*-
#
# Отношения между подразделениями
#

from django.db import models
from django.core.validators import MaxLengthValidator

class RelationExpertMark(models.Model):
    id = models.AutoField(primary_key=True, db_column="F_SEMEXPDEPREL_ID")
    main_department = models.ForeignKey(to="organizations.Department", related_name='evaluated_department_relations_master', db_column="F_DEPARTMENT_ID_MAIN", verbose_name=u'Подразделение', blank=True, null=True)
    related_department = models.ForeignKey(to="organizations.Department", related_name='evaluated_department_relations_child', db_column="F_DEPARTMENT_ID_RELATED", verbose_name=u'Подразделение', blank=True, null=True)
    evaluation_date = models.DateField(u"Дата проведения экспертизы", db_column="F_SEMEXPDEPREL_DATE", blank=False, null=True)
    value = models.DecimalField(u'Значение', max_digits=30, decimal_places=3, db_column='F_SEMEXPDEPREL_VALUE')
    creator = models.ForeignKey(to="auth.User", related_name="semantic_deprel_added", db_column="F_SEMEXPDEPREL_USER", null=True, blank=True)

    def symbolic_value(self):
        if self.value < 1.0:
            return u"Отсутствует"
        elif self.value >= 1.0 and self.value < 2.0:
            return u"Слабое"
        elif self.value >= 2.0 and self.value < 3.0:
            return u"Среднее"
        elif self.value >= 3.0:
            return u"Сильное"

    class Meta:
        db_table = "SEMEXPDEPREL"
        verbose_name = u"Экспертная оценка наличия связи"
        verbose_name_plural = u"Экспертные оценки наличия связи"
