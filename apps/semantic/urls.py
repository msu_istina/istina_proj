# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template


urlpatterns = patterns('semantic.views',

    # public pages
    url(r'^$', direct_to_template, {'template': 'semantic/index.html'}, name='semantic_index'),

    # departmnt connctions
    url(r'^dep_relations/$', 'dep_relations.show_departments_list', name="semantic_dep_relations"),
    url(r'^dep_relations/descriptive/$', 'dep_relations.show_descriptive_statistics', name="semantic_dep_relations_descriptive_stats"),
    url(r'^dep_relations/choose/(?P<depid>\d+)/$', 'dep_relations.show_departments_list', name="semantic_dep_relations_choose"),
    url(r'^dep_relations/(?P<depid>\d+)/$', 'dep_relations.show_relations', name="semantic_dep_relations_detail"),
    url(r'^dep_relations/(?P<depid>\d+)/(?P<related_depid>\d+)/proof/(?P<parent_depid>\d+)/$', 'dep_relations.show_relation_proof', name="semantic_dep_relations_proof"),
    url(r'^dep_relations/(?P<depid>\d+)/(?P<related_depid>\d+)/value/(?P<value>\d+)/(?P<id_to_return>\d+)/$', 'dep_relations.evaluate_relation', name="semantic_dep_relations_evaluate"),
    url(r'^dep_relations/experts/$', 'dep_relations.show_experts_list', name="semantic_dep_relations_experts_list"),
    url(r'^dep_relations/expert/(?P<userid>\d+)/report/$', 'dep_relations.show_expert_report', name="semantic_dep_relations_expert_report"),
)
