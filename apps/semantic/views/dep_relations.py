# -*- coding: utf-8 -*-
#
# Отношения между подразделениями
#
from collections import namedtuple

from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse, Http404, HttpResponseForbidden
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.db import connections
from django.db.models import Q
import datetime
from json import dumps, loads

from django.contrib.auth.models import User

from organizations.models import Department
from semantic.models.dep_relations import RelationExpertMark

DEFAULT_MIN_YEAR=2010
DEFAULT_ORGANIZATION=214524 # MSU ID
#REPORTS_SERVER='reports_server'
REPORTS_SERVER='default'

def gather_by(list_of_items, key=lambda x: x, compare=None):
    """Collect items with identical values of key(item) into sublists.
    Example: [1, 3, 1, 2] ==> [[1,1], [2], [3]]
    """
    last_key_value = None
    current_sublist = None
    result = []
    for (index, item) in enumerate(sorted(list_of_items, key=key, cmp=compare)):
        key_value = key(item)
        if index == 0 or (compare or cmp)(last_key_value, key_value) != 0:
            # New sublist should be added
            if index > 0:
                result.append(current_sublist)
            current_sublist = [item]
            last_key_value = key_value
        else:
            current_sublist.append(item)
    if current_sublist:
        result.append(current_sublist)
    return result

def departmentArticleWeightSQL(only_roots=False, depid=None):
    root_departments_sql = u""" /* Keep root departments only */
                 SELECT d.f_department_id
                 FROM department d
                      LEFT JOIN departmentlink dl ON (d.f_department_id = dl.f_department_id)
                 WHERE dl.f_department_id IS NULL
                   AND f_organization_id=%s
                 """ % (DEFAULT_ORGANIZATION,)
    all_departments_sql = u"""
                 SELECT d.f_department_id
                 FROM department d
                 WHERE f_organization_id=%s
    """ % (DEFAULT_ORGANIZATION,)
    selected_departments_sql = root_departments_sql if only_roots else all_departments_sql

    if depid:
        selected_departments_sql = u" SELECT %(depid)s AS f_department_id FROM dual " % {'depid': depid }
    
    return u"""
    SELECT da.f_activity_id AS f_article_id
         , da.f_department_id AS f_department_id
         , da.weight
         , da.activity_type
         , da.year
     FROM mv_department_activities da
     WHERE da.nauthors > 1
       AND da.f_department_id IN (""" + selected_departments_sql + u""")
     """


def getRelationsByArticlesSQL(firstYear=DEFAULT_MIN_YEAR, minPapers=3, articleType="OnlyJournals", depid=None, include_subdepartments=False):
    "Evaluates year, ncommon article, coefficients for two deps, departments' names. onlyJournals: 0-any, 1-journals,2-not journals."

    supported_types = {"OnlyJournals": 1, "OnlyNotJournals": 2}
    paperSelector = supported_types.get(articleType, 0)
    filtering = u""
    only_roots = False if include_subdepartments else True
    if depid is not None:
        if not include_subdepartments:
            filtering = u"AND daw1.f_department_id = %s" % depid
        else:
            filtering = u"""
            AND daw1.f_department_id IN (SELECT f_department_id FROM all_parent_department WHERE parent_f_department_id = %s)
            """ % depid
    
    return """
    WITH department_article_weight_1 as (""" + departmentArticleWeightSQL(only_roots=False) + u""")
       , department_article_weight_2 as (""" + departmentArticleWeightSQL() + u""")
    SELECT /*** a.f_article_year ***/
           -- count(distinct daw1.f_article_id) ncommon_articles
           trunc(sum(daw1.weight * daw2.weight), 3) ncommon_articles
           , trunc(sum(daw1.weight),3) d1_weight
           , trunc(sum(daw2.weight),3) d2_weight
           , d1.f_department_name f_department_name_1
           , d2.f_department_name f_department_name_2
           , daw1.f_department_id f_department_id_1
           , daw2.f_department_id f_department_id_2
           /*** , count(distinct daw1.f_article_id) ncommon_articles ***/
    FROM department_article_weight_1 daw1
      JOIN department_article_weight_2 daw2 ON daw2.f_article_id = daw1.f_article_id    
      JOIN department d1 ON d1.f_department_id = daw1.f_department_id
      JOIN article a ON a.f_article_id = daw1.f_article_id
      JOIN department d2 ON d2.f_department_id = daw2.f_department_id
    WHERE daw1.f_department_id <> daw2.f_department_id
      AND (daw1.f_department_id, daw2.f_department_id)
            NOT IN (SELECT f_department_id, parent_f_department_id
                    FROM all_parent_department)
      AND (daw2.f_department_id, daw1.f_department_id)
            NOT IN (SELECT f_department_id, parent_f_department_id
                    FROM all_parent_department)                    
      AND a.f_article_year >= %(p1)s
      AND ((1=%(p3)s AND a.f_journal_id IS NOT NULL)
            OR (2=%(p3)s AND a.f_journal_id IS NULL)
            OR 0=%(p3)s)
      /**** and a.f_journal_id in (select f_journal_id from journalrublink) ***/
      /* extra filtering */ %(filtering)s
    GROUP BY /*** a.f_article_year, ***/ daw1.f_department_id, daw2.f_department_id, d1.f_department_name,  d2.f_department_name
    HAVING count(distinct daw1.f_article_id) >= %(p2)s
    ORDER BY d1.f_department_name, d2.f_department_name, ncommon_articles desc
    """ % {'p1': firstYear, 'p2': minPapers,'p3': paperSelector,
           'filtering': filtering}

def getRootDepartmentsWithRelationsByArticlesSQL(firstYear=DEFAULT_MIN_YEAR, minPapers=3, articleType="OnlyJournals"):
    return u"""
    SELECT f_department_id_1, f_department_name_1
    FROM (""" + getRelationsByArticlesSQL(firstYear=firstYear, minPapers=minPapers, articleType=articleType, depid=None) + u""")
    WHERE f_department_id_1 NOT IN (SELECT f_department_id FROM departmentlink)
    GROUP BY f_department_id_1, f_department_name_1
    ORDER BY f_department_name_1
    """

def getDescendantDepartmentsWithRelationsByArticlesSQL(depid, firstYear=DEFAULT_MIN_YEAR, minPapers=3, articleType="OnlyJournals"):
    return (u"""
    SELECT department.f_department_id, department.f_department_name
    FROM all_parent_department
         JOIN department ON (department.f_department_id = all_parent_department.f_department_id)
    WHERE
      all_parent_department.parent_f_department_id=%(depid)s
      AND all_parent_department.parent_f_department_id <> all_parent_department.f_department_id
      AND department.f_department_id IN (
            SELECT f_department_id_1
            FROM (""" + getRelationsByArticlesSQL(firstYear=firstYear, minPapers=minPapers, articleType=articleType, depid=depid, include_subdepartments=True) + u""")
            WHERE f_department_id_2 NOT IN (SELECT parent_f_department_id
                                            FROM all_parent_department
                                            WHERE f_department_id = %(depid)s)
          )
    GROUP BY department.f_department_id, department.f_department_name
    ORDER BY department.f_department_name
    """) % {'depid': depid}



def commonArticlesSQL(dep_1, dep_2, firstYear=DEFAULT_MIN_YEAR):
    return (u"""
    WITH first_department_articles as (SELECT f_article_id, activity_type, year, weight FROM (""" + departmentArticleWeightSQL(depid=dep_1) + u""")),
         second_department_articles as (SELECT f_article_id, weight FROM (""" + departmentArticleWeightSQL(depid=dep_2) + u"""))
    SELECT
        first_department_articles.f_article_id
        , references.name
        , decode(first_department_articles.activity_type, 'article', 'Статья', 'book', 'Книга', 'presentation', 'Доклад') activity_type
        , nvl(references.authors, '---') AS authors
        , first_department_articles.weight * second_department_articles.weight weight
        , first_department_articles.year
    FROM
        first_department_articles
        JOIN second_department_articles ON (second_department_articles.f_article_id = first_department_articles.f_article_id)
        JOIN mv_activity_reference references ON (references.f_activity_id = first_department_articles.f_article_id)
        /* ****
        JOIN (
          SELECT authora.f_article_id
                 , article.f_article_name AS name
                 , LISTAGG(CASE
                           WHEN f_authora_ord <= 10 OR f_authora_ord IS NULL THEN f_authora_name
                           WHEN f_authora_ord =  11 THEN 'et.al.'
                           ELSE NULL
                         END, ', ')
                    WITHIN GROUP (ORDER BY f_authora_ord, f_authora_id) AS authors
          FROM   authora JOIN article ON (article.f_article_id = authora.f_article_id)
          GROUP BY authora.f_article_id, article.f_article_name
          UNION ALL
          SELECT authorb.f_book_id
                 , book.f_book_name AS name
                 , LISTAGG(CASE
                           WHEN f_authorb_ord <= 10 OR f_authorb_ord IS NULL THEN f_authorb_name
                           WHEN f_authorb_ord =  11 THEN 'et.al.'
                           ELSE NULL
                         END, ', ')
                    WITHIN GROUP (ORDER BY f_authorb_ord, f_authorb_id) AS authors
          FROM   authorb JOIN book ON (book.f_book_id = authorb.f_book_id)
          GROUP BY authorb.f_book_id, book.f_book_name          
          UNION ALL
          SELECT authord.f_presentation_id
                 , presentation.f_presentation_name
                 , LISTAGG(CASE
                           WHEN f_authord_ord <= 10 OR f_authord_ord IS NULL THEN f_authord_name
                           WHEN f_authord_ord =  11 THEN 'et.al.'
                           ELSE NULL
                         END, ', ')
                    WITHIN GROUP (ORDER BY f_authord_ord, f_authord_id) AS authors
          FROM   authord JOIN presentation ON (presentation.f_presentation_id = authord.f_presentation_id)
                         JOIN t_confs ON (t_confs.conf_id = presentation.conf_id)
          GROUP BY authord.f_presentation_id, presentation.f_presentation_name
        ) references ON (references.f_article_id = first_department_articles.f_article_id)
        ** */
    WHERE first_department_articles.year >= %(first_year)s
    """) % {
        'dep1': dep_1,
        'dep2': dep_2,
        'first_year': firstYear,
        }


def coAuthorsSQL(dep_1, dep_2):
    return (u"""
    WITH first_department_articles as (SELECT f_article_id, activity_type, year, weight FROM (""" + departmentArticleWeightSQL(depid=dep_1) + u""") WHERE f_department_id=%(dep1)s),
         second_department_articles as (SELECT f_article_id, weight FROM (""" + departmentArticleWeightSQL(depid=dep_2) + u""") WHERE f_department_id=%(dep2)s)
    SELECT
        department.f_department_name
        , man.f_man_namel || ' ' || man.f_man_namef || ' ' || man.f_man_namem
        , count(DISTINCT aa.f_article_id) narticles
    FROM
        first_department_articles
        JOIN second_department_articles ON (second_department_articles.f_article_id = first_department_articles.f_article_id)
        JOIN (
           SELECT f_man_id, f_article_id, year
           FROM (
                 SELECT authora.f_man_id, authora.f_article_id, article.f_article_year AS year
                 FROM authora JOIN article ON (article.f_article_id = authora.f_article_id)
                 WHERE article.f_article_year >= %(min_year)s
                 UNION ALL
                 SELECT authorb.f_man_id, authorb.f_book_id, book.f_book_year
                 FROM authorb JOIN book ON (book.f_book_id = authorb.f_book_id)
                 WHERE book.f_book_year >= %(min_year)s
                 UNION ALL
                 SELECT authord.f_man_id, authord.f_presentation_id, t_confs.year
                 FROM authord JOIN presentation ON (presentation.f_presentation_id = authord.f_presentation_id)
                              JOIN t_confs ON (t_confs.conf_id = presentation.conf_id)
                 WHERE t_confs.year >= %(min_year)s
             )
           WHERE f_man_id IN (SELECT f_man_id FROM v_man_workplaces WHERE f_department_id IN (%(dep1)s, %(dep2)s))
        ) aa ON (aa.f_article_id = first_department_articles.f_article_id)
        JOIN v_man_workplaces mwp ON (aa.f_man_id = mwp.f_man_id)
        JOIN v_coworker_status ON (v_coworker_status.f_man_id = aa.f_man_id)
        JOIN man ON (man.f_man_id = aa.f_man_id)
        JOIN department ON (department.f_department_id = mwp.f_department_id)
    WHERE mwp.f_department_id IN ( %(dep1)s, %(dep2)s )
          AND (
                 (nvl(f_manspost_part, 0) = 0 OR v_coworker_status.status='ext_coworker')
                 AND (aa.year >= extract(year from mwp.f_manspost_begin) AND
                     (mwp.f_manspost_end IS NULL OR aa.year <= extract(year from mwp.f_manspost_end)))
              )
    GROUP BY department.f_department_name
             , man.f_man_namel || ' ' || man.f_man_namef || ' ' || man.f_man_namem
    ORDER BY department.f_department_name, narticles DESC
    """) % {
        'dep1': dep_1,
        'dep2': dep_2,
        'min_year': DEFAULT_MIN_YEAR,
        }



@login_required
def show_departments_list(request, depid=None):
    "Lists all departments with interrelations."
    department = None
    if depid:
        department = Department.objects.get(pk=depid)
    cursor = connections[REPORTS_SERVER].cursor()
    if not depid:
        get_relations_sql = getRootDepartmentsWithRelationsByArticlesSQL()
    else:
        get_relations_sql = getDescendantDepartmentsWithRelationsByArticlesSQL(depid=depid)
    cursor.execute(get_relations_sql)
    DepWithRelationsTuple = namedtuple('DepRelations', 'department_id, department_name')
    departments = [DepWithRelationsTuple(item[0], item[1]) for item in cursor]

    links_to_details = False
    if department: # and department.is_root:
        links_to_details = True
    links_to_details = True

    return render(request, "semantic/dep_relations/list.html",
                  {'departments': departments,
                   'links_to_details': links_to_details})

@login_required
def show_relations(request, depid):
    department = Department.objects.get(pk=depid)

    def find_mark(main_department, related_department, user):
        try:
            return RelationExpertMark.objects.get(main_department=main_department, related_department=related_department, creator=user)
        except RelationExpertMark.DoesNotExist:
            return None

    cursor = connections[REPORTS_SERVER].cursor()
    get_relations_sql = getRelationsByArticlesSQL(depid=depid, include_subdepartments=True)
    cursor.execute(get_relations_sql)
    DepRelationsTuple = namedtuple('DepRelations', 'department, department_id, related_department, related_department_id, strength, mark')
    all_relations = []
    main_department = None
    relations = {'department': None, 'links': []}
    for item in cursor:
        main_department_name = item[3]
        main_department_id = int(item[5])

        related_department_name = item[4]
        related_department_id = item[6]
        strength = item[0]

        # Check for new main department; push collected relation into result
        if not main_department or main_department_id != main_department.id:
            if main_department:
                all_relations.append(relations)
            main_department = Department.objects.get(pk=main_department_id)
            relations = {'department': main_department, 'links': []}
        
        related_department = Department.objects.get(pk=related_department_id)
        mark = find_mark(main_department, related_department, request.user)
        relations['links'].extend([DepRelationsTuple(main_department, main_department.id, related_department, related_department.id, strength, mark)])
    # Append last item, if needed
    if relations['links']:
        all_relations.append(relations)

    return render(request, "semantic/dep_relations/details_for_department.html",
                  {'department': department,
                   'all_relations': all_relations})

@login_required
def show_relation_proof(request, depid, related_depid, parent_depid):
    compute_coauthors = False
    
    "Show proofs of relation between two departments."
    department = Department.objects.get(pk=depid)
    related_department = Department.objects.get(pk=related_depid)
    cursor = connections[REPORTS_SERVER].cursor()
    common_articles_sql = commonArticlesSQL(depid, related_depid)
    cursor.execute(common_articles_sql)
    CommonActivitiesTuple = namedtuple('CommonActivities', 'id, name, type, authors, weight, year')
    common_activities = [CommonActivitiesTuple(item[0], item[1], item[2], item[3], item[4], item[5]) for item in cursor]

    # Find authors of common articles
    coauthors = None
    if compute_coauthors:
        coauthors_sql = coAuthorsSQL(depid, related_depid)
        cursor.execute(coauthors_sql)
        CoauthorsTuple = namedtuple('Coauthors', 'department_name, man_fullname, narticles')
        coauthors = [CoauthorsTuple(item[0], item[1], item[2]) for item in cursor]
        coauthors = gather_by(coauthors, key=lambda x: x.department_name)

    return render(request, "semantic/dep_relations/proof.html",
                  {'department': department,
                   'parent_depid': parent_depid,
                   'related_department': related_department,
                   'common_activities': common_activities,
                   'coauthors': coauthors})

@login_required
def evaluate_relation(request, depid, related_depid, value, id_to_return=None):
    department = Department.objects.get(pk=depid)
    related_department = Department.objects.get(pk=related_depid)
    RelationExpertMark.objects.filter(
        main_department=department,
        related_department=related_department,
        creator=request.user).delete()
    mark = RelationExpertMark.objects.create(
        main_department=department,
        related_department=related_department,
        value=value,
        creator=request.user)
    # mark.save()
    messages.info(request, u"Спасибо, Ваша оценка сохранена.")
    return redirect("semantic_dep_relations_detail", id_to_return or department.id)


@login_required
def show_experts_list(request):
    from django.db.models import Count
    experts = User.objects.all().annotate(num_marks=Count('semantic_deprel_added')).filter(num_marks__gt=0)
    
    return render(request, "semantic/dep_relations/list_experts.html",
                  {'experts': experts,
                   })
    

@login_required
def show_expert_report(request, userid):
    user = get_object_or_404(User, pk=userid)
    marks = RelationExpertMark.objects.filter(creator=user).order_by('main_department')

    marks = gather_by(marks, key=lambda m: m.main_department.id)
    
    return render(request, "semantic/dep_relations/expert_reviews.html",
                  {'expert': user,
                   'marks': marks})
    

@login_required
def show_descriptive_statistics(request):
    firstYear = DEFAULT_MIN_YEAR
    minPapers = 5
    query = ("""
    WITH department_article_weight as (""" + departmentArticleWeightSQL(only_roots=False) + u""")
    SELECT count(daw1.f_article_id) ncommon_articles
           , trunc(sum(daw1.weight * daw2.weight), 3) interaction_weight
           , daw1.f_department_id f_department_id_1
           , daw2.f_department_id f_department_id_2
    FROM department_article_weight daw1
      JOIN department_article_weight daw2 ON (daw2.f_article_id = daw1.f_article_id)
      JOIN root_department root_1 ON (root_1.f_department_id = daw1.f_department_id)
      JOIN root_department root_2 ON (root_2.f_department_id = daw2.f_department_id)
    WHERE daw1.f_department_id <> daw2.f_department_id
      AND (daw1.f_department_id, daw2.f_department_id)
            NOT IN (SELECT f_department_id, parent_f_department_id
                    FROM all_parent_department)
      AND (daw2.f_department_id, daw1.f_department_id)
            NOT IN (SELECT f_department_id, parent_f_department_id
                    FROM all_parent_department)
      AND root_1.root_f_department_id <> root_2.root_f_department_id
      AND daw1.year >= %(p1)s      
    GROUP BY daw1.f_department_id, daw2.f_department_id
    HAVING count(distinct daw1.f_article_id) >= %(p2)s
    """)  % {'p1': firstYear, 'p2': minPapers,}

    cursor = connections[REPORTS_SERVER].cursor()
    cursor.execute(query)
    data = list(cursor)
    nrelations = len(data)

    return render(request, "semantic/dep_relations/descriptive_statistics.html",
                  {'data': data,
                   'number_of_relations': nrelations,
                   })


# 1. Metodicheskij bla-bla
# 2. Dva varianta; istina ili dannye; massivy
# 3. byli obrabotany takie dannye
# 4. Ekspertizy
