col f_department_name format a60
set colsep ";"

-- Основные подразделения
SELECT 
  department.f_department_name
  , count(*) nlinks_out
  , count(DISTINCT related_department.f_department_id) nlinks_in
FROM
   semexpdeprel
      JOIN root_department ON (root_department.f_department_id = semexpdeprel.f_department_id_main)
      JOIN department ON (department.f_department_id = root_department.root_f_department_id)
      JOIN root_department related_root ON (related_root.f_department_id = semexpdeprel.f_department_id_related)
      JOIN department related_department ON (related_department.f_department_id = related_root.root_f_department_id)
GROUP BY department.f_department_name
/


-- dep-dep relations
WITH department_article_weight AS (
    SELECT da.f_activity_id
         , da.f_department_id AS f_department_id
         , da.weight
         , da.activity_type
         , da.year
     FROM mv_department_activities da
     WHERE da.nauthors > 1
       AND da.f_department_id IN (
                 SELECT d.f_department_id
                 FROM department d
                 WHERE f_organization_id=214524
           )
    )
SELECT daw1.year
       , daw1.activity_type
       , decode(a.f_journal_id, NULL, NULL, 1) in_journal
       , count(DISTINCT daw1.f_activity_id) ncommon_articles
       , trunc(sum(daw1.weight * daw2.weight), 3) ncommon_articles_weighted
       , trunc(sum(daw1.weight),3) d1_weight
       , trunc(sum(daw2.weight),3) d2_weight
       , daw1.f_department_id f_department_id_1
       , daw2.f_department_id f_department_id_2
FROM department_article_weight daw1
  JOIN department_article_weight daw2 ON (daw2.f_activity_id = daw1.f_activity_id)
  LEFT JOIN article a ON a.f_article_id = daw1.f_activity_id
WHERE daw1.f_department_id <> daw2.f_department_id
  AND (daw1.f_department_id, daw2.f_department_id)
        NOT IN (SELECT f_department_id, parent_f_department_id
                FROM all_parent_department)
  AND (daw2.f_department_id, daw1.f_department_id)
        NOT IN (SELECT f_department_id, parent_f_department_id
                FROM all_parent_department)
GROUP BY daw1.year
       , daw1.activity_type
       , decode(a.f_journal_id, NULL, NULL, 1)
       , daw1.f_department_id, daw2.f_department_id
/
