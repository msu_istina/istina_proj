CREATE MATERIALIZED VIEW mv_department_activities
 -- REFRESH START WITH SYSDATE NEXT ROUND(SYSDATE + 7) + 3/24
AS
    SELECT da.f_article_id AS f_activity_id
         , da.f_department_id AS f_department_id
         , max(ndepauthors) / max(na.nauthors) AS weight
         , na.activity_type
         , na.nauthors
         , da.year
     FROM (
         SELECT DISTINCT f_article_id, f_department_id, year, count(distinct f_man_id) ndepauthors
         FROM (
             SELECT mwp.f_department_id, aa.f_article_id, aa.f_man_id, mwp.exact_workplace, year
             FROM (
                 SELECT authora.f_man_id, authora.f_article_id, article.f_article_year AS year
                 FROM authora JOIN article ON (article.f_article_id = authora.f_article_id)
                 UNION ALL
                 SELECT authorb.f_man_id, authorb.f_book_id, book.f_book_year
                 FROM authorb JOIN book ON (book.f_book_id = authorb.f_book_id)
                 UNION ALL
                 SELECT authord.f_man_id, authord.f_presentation_id, t_confs.year
                 FROM authord JOIN presentation ON (presentation.f_presentation_id = authord.f_presentation_id)
                              JOIN t_confs ON (t_confs.conf_id = presentation.conf_id)
               ) aa
               JOIN v_man_workplaces mwp ON (aa.f_man_id = mwp.f_man_id)
               JOIN v_coworker_status ON (v_coworker_status.f_man_id = aa.f_man_id)
             WHERE ((nvl(f_manspost_part, 0) = 0 OR v_coworker_status.status='ext_coworker')
                    AND (aa.year >= extract(year from mwp.f_manspost_begin) AND
                        (mwp.f_manspost_end IS NULL OR aa.year <= extract(year from mwp.f_manspost_end)))
                   )
            GROUP BY aa.f_article_id, mwp.f_department_id, aa.f_man_id, mwp.exact_workplace, year
         ) V_DEPARTMENT_ARTICLE_AUTHORS
         GROUP BY f_article_id, f_department_id, year
         ) da
     JOIN (
             SELECT 'article' AS activity_type, f_article_id, count(*) AS nauthors FROM authora GROUP BY f_article_id
             UNION ALL
             SELECT 'book', f_book_id, count(*) AS nauthors FROM authorb GROUP BY f_book_id
             UNION ALL
             SELECT 'presentation', f_presentation_id, count(*) AS nauthors FROM authord GROUP BY f_presentation_id
         ) na
         ON na.f_article_id = da.f_article_id
     WHERE na.nauthors > 1
     GROUP BY da.f_article_id, da.f_department_id, na.activity_type, na.nauthors, da.year
/

create index i_mv_depact_department_id on mv_department_activities(f_department_id)
/

create index i_mv_depact_activity_id on mv_department_activities(f_activity_id)
/


CREATE MATERIALIZED VIEW mv_activity_reference
 -- REFRESH START WITH SYSDATE NEXT ROUND(SYSDATE + 7) + 3/24
AS
          SELECT authora.f_article_id AS f_activity_id
                 , article.f_article_name AS name
                 , LISTAGG(CASE
                           WHEN f_authora_ord <= 10 OR f_authora_ord IS NULL THEN f_authora_name
                           WHEN f_authora_ord =  11 THEN 'et.al.'
                           ELSE NULL
                         END, ', ')
                    WITHIN GROUP (ORDER BY f_authora_ord, f_authora_id) AS authors
          FROM   authora JOIN article ON (article.f_article_id = authora.f_article_id)
          GROUP BY authora.f_article_id, article.f_article_name
          UNION ALL
          SELECT authorb.f_book_id
                 , book.f_book_name AS name
                 , LISTAGG(CASE
                           WHEN f_authorb_ord <= 10 OR f_authorb_ord IS NULL THEN f_authorb_name
                           WHEN f_authorb_ord =  11 THEN 'et.al.'
                           ELSE NULL
                         END, ', ')
                    WITHIN GROUP (ORDER BY f_authorb_ord, f_authorb_id) AS authors
          FROM   authorb JOIN book ON (book.f_book_id = authorb.f_book_id)
          GROUP BY authorb.f_book_id, book.f_book_name          
          UNION ALL
          SELECT authord.f_presentation_id
                 , presentation.f_presentation_name
                 , LISTAGG(CASE
                           WHEN f_authord_ord <= 10 OR f_authord_ord IS NULL THEN f_authord_name
                           WHEN f_authord_ord =  11 THEN 'et.al.'
                           ELSE NULL
                         END, ', ')
                    WITHIN GROUP (ORDER BY f_authord_ord, f_authord_id) AS authors
          FROM   authord JOIN presentation ON (presentation.f_presentation_id = authord.f_presentation_id)
                         JOIN t_confs ON (t_confs.conf_id = presentation.conf_id)
          GROUP BY authord.f_presentation_id, presentation.f_presentation_name
/

CREATE INDEX i_mv_actref_activity_id ON mv_activity_reference(f_activity_id)
/


CREATE MATERIALIZED VIEW mv_semantic_depdep_rel
  REFRESH START WITH SYSDATE NEXT ROUND(SYSDATE + 30) + 6/24
AS
WITH department_article_weight AS (
    SELECT da.f_activity_id
         , da.f_department_id AS f_department_id
         , da.weight
         , da.activity_type
         , da.year
     FROM mv_department_activities da
     WHERE da.nauthors > 1
       AND da.f_department_id IN (
                 SELECT d.f_department_id
                 FROM department d
                 WHERE f_organization_id=214524
           )
    )
SELECT daw1.year
       , daw1.activity_type
       , decode(a.f_journal_id, NULL, NULL, 1) in_journal
       , count(DISTINCT daw1.f_activity_id) ncommon_articles
       , trunc(sum(daw1.weight * daw2.weight), 3) ncommon_articles_weighted
       , trunc(sum(daw1.weight),3) d1_weight
       , trunc(sum(daw2.weight),3) d2_weight
       , daw1.f_department_id f_department_id_main
       , daw2.f_department_id f_department_id_related
FROM department_article_weight daw1
  JOIN department_article_weight daw2 ON (daw2.f_activity_id = daw1.f_activity_id)
  LEFT JOIN article a ON a.f_article_id = daw1.f_activity_id
WHERE daw1.f_department_id <> daw2.f_department_id
  AND (daw1.f_department_id, daw2.f_department_id)
        NOT IN (SELECT f_department_id, parent_f_department_id
                FROM all_parent_department)
  AND (daw2.f_department_id, daw1.f_department_id)
        NOT IN (SELECT f_department_id, parent_f_department_id
                FROM all_parent_department)
GROUP BY daw1.year
       , daw1.activity_type
       , decode(a.f_journal_id, NULL, NULL, 1)
       , daw1.f_department_id, daw2.f_department_id
/

CREATE INDEX i_mv_semdepdeprel_dep_main ON mv_semantic_depdep_rel(f_department_id_main)
/

CREATE INDEX i_mv_semdepdeprel_dep_related ON mv_semantic_depdep_rel(f_department_id_related)
/

CREATE INDEX i_mv_semdepdeprel_deps ON mv_semantic_depdep_rel(f_department_id_main, f_department_id_related)
/
