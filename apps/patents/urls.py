# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *

from common.models import AuthorRole
from common.views import LinkedToWorkersModelWizardView as Wizard
from patents.models import Patent
from patents.forms import PatentForm

PatentFormWizard = Wizard.create_wizard_from_form(PatentForm)

options_base = {'model': Patent}
options_detail = dict(options_base.items() + [('template_name', 'patents/detail.html'),
    ('extra_context', {'roles': AuthorRole.objects.filter(authorroleobj__code="patent")})])

urlpatterns = patterns('common.views',
    url(r'^add/$', PatentFormWizard.as_view(), name='patents_add'),
    url(r'^(?P<object_id>\d+)/$', 'detail', options_detail, name='patents_detail'),
    url(r'^(?P<object_id>\d+)/edit/$', PatentFormWizard.as_view(), name='patents_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', options_base, name='patents_delete'),
)
