# -*- coding: utf-8; -*-
from common.forms import LinkedToWorkersModelForm
from patents.models import Patent


class PatentForm(LinkedToWorkersModelForm):
    '''A simple form for patents.'''
    fields_order = ["authors_str", "title", "number", "date", "description"]

    class Meta:
        model = Patent
        exclude = ('authors', 'creator', 'xml')
