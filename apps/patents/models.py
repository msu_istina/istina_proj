# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

from common.models import AuthoredModel, AuthorshipPositionedModel, Activity
from common.utils.slughifi import slughifi
from django.template.loader import render_to_string
from workers.utils import get_fullname_raw
from publications.utils import split_authors, guess_language


class Patent(AuthoredModel, Activity):
    '''A Patent.'''
    id = models.AutoField(primary_key=True, db_column="F_PATENT_ID")
    title = models.CharField(u"Название патента", max_length=255, db_column="F_PATENT_NAME")
    authors = models.ManyToManyField("workers.Worker", related_name="patents", through="PatentAuthorship")
    number = models.CharField(u"Номер", max_length=255, db_column="F_PATENT_NUM")
    date = models.DateField(u"Дата публикации патента", db_column="F_PATENT_DATE")
    description = models.TextField(u"Реферат (описание изобретения)", db_column="F_PATENT_FORMULA", blank=True, help_text=u"например, укажите формулу изобретения")
    xml = models.TextField(u"XML патента", db_column="F_PATENT_XML", blank=True)
    creator = models.ForeignKey(to=User, related_name="patents_added", db_column="F_PATENT_USER", null=True, blank=True)
    rights_owner = models.ForeignKey(to="organizations.Organization", related_name="patents", db_column="F_ORGANIZATION_ID", verbose_name=u"Организация-правообладатель", blank=True, null=True)

    nominative_en = "patent"
    genitive = u"патента"
    genitive_plural_full = u"патентов"
    instrumental = u"патентом"
    locative = u"патенте"

    class Meta:
        db_table = "PATENT"
        verbose_name = u"патент"
        verbose_name_plural = u"патенты"
        get_latest_by = "date"
        ordering = ('-date', '-number')

    def __unicode__(self):
        return "%s (# %s)" % (self.title, self.number)

    @models.permalink
    def get_absolute_url(self):
        return ('patents_detail', (), {'object_id': self.id})

    def get_bibtex(self, patent=None):
        '''
        Returns bibtex string of a patent.
        publication argument has been added, so that the function can be called
        for fake Publication objects created in Python, see WorkTuple class,
        not duplicating the function code.
        '''
        if patent is None:
            patent = self
        return render_to_string("publications/publication_detail.bib", {'publication': patent})

    def get_bibtex_id(self, title=None, year=None, first_author_lastname=None):
        '''
        Returns bibtex id of the publication.
        title, year and first_author_lastname arguments have been added, so that the function can be called
        for fake Publication objects created in Python, see WorkTuple class,
        not duplicating the function code.
        '''
        if title is None:
            title = self.title
        if year is None:
            year = self.date.year
        if first_author_lastname is None:
            # note that slughifi("") = ""
            first_author_lastname = self.authors.order_by("lastname")[0].lastname if self.authors.exists() else ""
        short_title = title.split()[0]
        if len(short_title) < 4:
            short_title = " ".join(title.split()[:2])
        return u"%s%s%s%s" % (slughifi(first_author_lastname), year, slughifi(short_title), self.id)

    @property
    def bib_type(self):
        return "techreport"

    def get_authors_string_bibtex(self, authors_string=None):
        '''
        Return a string of authors formatted for bibtex entry.
        First, get original authors string and split it into triples (lastname, firstname, middlename).
        Then transform each triple in a fullname string formatted for bibtex.

        authors_string argument has been added, so that the function can be called
        for fake Publication objects created in Python, see WorkTuple class,
        not duplicating the function code.
        '''
        if not authors_string:
            authors_string = self.authors_string
        return " and ".join(
            get_fullname_raw(
                lastname=lastname, firstname=firstname, middlename=middlename, comma=True)
            for (lastname, firstname, middlename) in split_authors(authors_string))

    def get_language_func(self, title=None):
        '''
        Returns a string indicating publication language. Currently only russian and english are supported. Used in bibtex.
        title argument has been added, so that the function can be called
        for fake Publication objects created in Python, see WorkTuple class,
        not duplicating the function code.
        '''
        if title is None:
            title = self.title
        return guess_language(title)


class PatentAuthorship(AuthorshipPositionedModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORP_ID")
    author = models.ForeignKey(to="workers.Worker", related_name="patent_authorships", db_column="F_MAN_ID", null=True, blank=True)
    patent = models.ForeignKey(to="Patent", related_name="authorships", db_column="F_PATENT_ID")
    original_name = models.CharField(max_length=255, db_column="F_AUTHORP_NAME", blank=True)
    position = models.IntegerField(db_column="F_AUTHORP_ORD", default=0, null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="patent_authorships_added", db_column="F_AUTHORP_USER", null=True, blank=True)

    class Meta(AuthorshipPositionedModel.Meta):
        db_table = "AUTHORP"
        verbose_name = u"авторство патента"
        verbose_name_plural = u"авторства патентов"

    def __unicode__(self):
        return u"%s, автор патента '%s'" % (self.workers_string_full, self.patent.title)

from patents.admin import *
