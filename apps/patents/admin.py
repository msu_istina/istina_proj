from common.utils.admin import register_with_versions
from models import Patent, PatentAuthorship


register_with_versions(Patent)
register_with_versions(PatentAuthorship)
