# -*- coding: utf-8 -*-
from django.core.cache import get_cache
from django.db import models
from common.managers import SimilarFieldManager
from common.utils.cache import invalidate_template_cache
from publications.models import Article

class LinkedToArticleManager(SimilarFieldManager):
    '''Model manager for models linked to article via onetoonefield or foreign key.'''

    def select_from(self, article_ids, **kwargs):
        return self.filter(article__in=article_ids, **kwargs).values_list('article', flat=True)

    def get_similar(self, article, threshold=0.7):
        return self.get_similar_by_field("article__title", article.article.title, threshold, 1000)

    def print_duplicates(self, year, threshold=0.7):
        for article in self.filter(article__year=year):
            similars = self.get_similar(article, threshold)
            similars = [similar for similar in similars if similar.article.id > article.article.id] # don't show pairs twice
            if similars:
                print "%d: %s" % (article.article.id, ", ".join(str(similar.article.id) for similar in similars))


class RankedArticleManager(LinkedToArticleManager):
    '''Model manager for RankedArticle model.'''

    def select_unrewarded(self, article_ids, worker, all_rewarded=False):
        """
        Return subset of article_ids which contains unrewarded articles for worker.
        all_rewarded:
            if True, all article_ids are rewarded, no need to do additional filter.
        """
        rewarded_ids = self.select_from(article_ids) if not all_rewarded else article_ids
        worker_rewarded_ids = worker.rewarded_users.values_list('rewarded_authorships__article__article', flat=True)
        return list(set(rewarded_ids) - set(worker_rewarded_ids))
        
class TopArticleManager(LinkedToArticleManager):
    '''Model manager for TopArticle model.'''        

    def invalidate_cache(self):
        '''Reset cache of all top articles.'''
        cache = get_cache("bigdata")
        cache.delete('statistics_top_articles_articles')
        cache.delete('statistics_top_articles_users')
        invalidate_template_cache('statistics_top_articles_template', 0)

