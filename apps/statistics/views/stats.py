# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse, Http404, HttpResponseForbidden
from django.shortcuts import render, redirect, get_object_or_404
from django.template.defaultfilters import slugify
from django.utils import simplejson
from django.core.cache import get_cache
from django.contrib import messages
from django.views.generic import TemplateView, ListView
from django.db.models import Q
from paging.helpers import paginate
import datetime

from common.utils.activities import get_activities_last_added, fetch_activities_compact
from common.utils.user import get_worker
from common.utils.uniqify import uniqify
from statistics.models import SeenArticle, ConfirmedArticle, RankingProject, TopArticle, DepartmentStatistics
from publications.models import Article, ArticleAuthorship
from statistics.sqlutils import get_top_journals, get_top_journals_rubrics_stats, get_top_articles_by_rubrics, get_top_workers, get_top_articles_ids, get_top_articles_ids_by_worker, get_department_activity, get_publications_stat, get_top_workers_for_dep, get_popular_journals_for_dep, get_workers_hirsh_for_dep, get_worker_citcount, get_ext_articles_ids, get_department_citcounts_by_year, get_cited_articles_counts
from statistics.sqlutils_getdata import get_department_data, get_department_pubs_generic
from workers.models import Worker
from workers.decorators import worker_required
from organizations.models import Organization, Department
from organizations.utils import is_representative_for
from common.utils.latex import latex_to_pdf
from common.utils.list import flatten
from common.utils.templates import get_icon_by_bool
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from common.utils.excel import export_to_excel_response
from publications.utils import bibtex2html_multiple
from unified_permissions import has_permission

from django.utils import simplejson
import collections
from common.models import get_activity_models
from django.core.cache import cache
from unified_permissions.decorators import permission_abstract_required, permission_required
from dissertations.models import Specialty

from sqlreports import generate_adata

bigdata_cache = get_cache("bigdata")

@login_required
def dynamic(request):
    report_depth = 4 # show last REPORT_DEPTH years, including the current, in article selection filter
    years = [datetime.date.today().year-i for i in range(report_depth)]
    return render(request, "statistics/dynamic.html", {"years": years})


def depactivity2gc_row(depactivity):
    return {"c":[
               {"v": depactivity.id, "f": str(depactivity.id)},
               {"v": depactivity.name},
               {"v": float(depactivity.value)}
            ]}


def xhr_depactivity_articles(request):
    # Genereate JSON data with department article statistics suitable for Goolgle chart tables

    # import logging
    message = "Hello"
    ids = request.GET.get('id', '')
    metric = request.GET.get('metric', 'count')
    year = request.GET.get('year', 'All')
    confirmed = request.GET.get('confirmed', '0')
    pub_type = request.GET.get('pub_type', 'all')

    rub_ids = request.GET.get('rubid', '')
    group_by = request.GET.get('group_by', 'department')
    filter_deps_by_rub = request.GET.get('filter_deps_by_rub', '0')
    rub_type = request.GET.get('rubtype', 'GRNTI')
    try:
        ids = map(int, filter(None, ids.split(",")))
        rub_ids = map(int, filter(None, rub_ids.split(",")))
    except ValueError:
        return HttpResponse("bad ids")

    try:
        year = int(year)
    except (TypeError, ValueError):
        year = None

    try:
        confirmed = int(confirmed)
    except (TypeError, ValueError):
        confirmed = 1 if confirmed=='on' else 0

    try:
        filter_deps_by_rub = int(filter_deps_by_rub)
    except (TypeError, ValueError):
        filter_deps_by_rub = 0


    #logging.getLogger("xhr_test").warning("GET: %s", ids)
    depactivity = 0
    if request.is_ajax() or True:
        if group_by == 'rub':
            rubmetric = request.GET.get('rubmetric', metric)
            if len(rubmetric) > 0:
                metric = rubmetric
            depactivity = get_department_activity(year, ids, metric, pub_type, confirmed, rub_ids, 'rub', filter_deps_by_rub, rub_type)
        else:
            # group_by == 'deparment':
            depactivity = get_department_activity(year, ids, metric, pub_type, confirmed, rub_ids, 'department', filter_deps_by_rub, rub_type)
        message = {"cols": [
           {"id": "A", "label": "ID", "type": "number"},
           {"id": "B", "label": "Name", "type": "string"},
           {"id": "C", "label": "Value", "type": "number"}
        ], "rows": map(depactivity2gc_row, depactivity)}
        message = simplejson.dumps(message)
    return HttpResponse(message)


def index(request):
    projects = RankingProject.objects.all()
    organization = Organization.objects.get_msu()
    return render(request, "statistics/index.html", {"projects": projects, "organization": organization})


def top_journals(request, rubric_name=None, all=False):
    # Show list of top25 journals
    journals = get_top_journals()
    rubrics = uniqify([journal.rubric for journal in journals])
    if rubric_name:
        journals = [journal for journal in journals if slugify(journal.rubric) == rubric_name]
        if journals:
            rubric_name = journals[0].rubric # slugified name -> normal name
        else:
            raise Http404
    index_page = not rubric_name and not all
    return render(request, "statistics/top_journals.html", {"journals": journals, "rubrics": rubrics, "index_page": index_page, "rubric_name": rubric_name, "all": all})


@permission_abstract_required("analyze_top_articles")
def top_journals_rubrics_stats(request):
    rubrics = get_top_journals_rubrics_stats()
    return render(request, "statistics/top_journals_rubrics_stats.html", {"rubrics": rubrics})


@permission_abstract_required("analyze_top_articles")
def top_articles_by_rubrics(request):
    artsbyrubs = get_top_articles_by_rubrics()
    return render(request, "statistics/top_articles_by_rubrics.html", {"artsbyrubs": artsbyrubs})


def top_workers(request, year='2012', showall=None):
    try:
        year = int(year)
    except (TypeError, ValueError):
        year = 2012 # int(datetime.datetime.now().year)
    workers = get_top_workers(year, showall)
    return render(request, "statistics/top_workers.html",
                  {"workers": workers,
                   "year": year})


class TopWorkersBySpecialtyIndexView(ListView):
    model = Specialty
    template_name = "statistics/top_workers_by_specialty_index.html"

    def get_context_data(self, **kwargs):
        context = super(TopWorkersBySpecialtyIndexView, self).get_context_data(**kwargs)
        context['organization'] = get_object_or_404(Organization, pk=self.kwargs['organization_id'])
        return context


class TopWorkersBySpecialtyView(TemplateView):
    template_name = "statistics/top_workers_by_specialty.html"

    @permission_required("view_top_workers_by_specialty", Organization, "organization_id")
    def dispatch(self, request, *args, **kwargs):
        """organization_id is used only for permission check,
        but it should also be used in data retrieving in the View methods.
        """
        return super(TopWorkersBySpecialtyView, self).dispatch(request, *args, **kwargs)

    def get_top_workers_by_specialty(self, specialty):
        """Return top workers by specialty.
        How to find workers:
            has rank with specialty
            memberships with specialty
            authors/advisers/opponents for dissertations with specialty
            filter only Doctors of Science
            filter only registered users
        How to rank: by default formula points
        Take Top 25 workers for each department
        """
        limit = 25
        workers = Worker.objects.filter_doctors().filter(profile__isnull=False).filter(
            Q(ranks__branch_num__startswith=specialty.number) \
            | Q(dissertation_councils_memberships__specialty=specialty) \
            | Q(dissertations_defended__specialty=specialty) | Q(dissertations_defended__specialty2=specialty) \
            | Q(dissertations_advised__specialty=specialty)  | Q(dissertations_advised__specialty2=specialty) \
            | Q(dissertations_opposed__specialty=specialty)  | Q(dissertations_opposed__specialty2=specialty))
        for worker in workers:
            points = worker.profile.get_points_for_default_formula()
            worker._points = float(points) if points else None
            worker._dissertations_advised = worker.dissertations_advised.filter(Q(specialty=specialty) | Q(specialty2=specialty)).distinct().count()
        workers = filter(lambda worker: worker._points, workers)
        # If not equal, the sequences are ordered the same as their first differing elements
        return sorted(workers, key=lambda worker: (worker._dissertations_advised, worker._points), reverse=True)

    def get_context_data(self, **kwargs):
        context = super(TopWorkersBySpecialtyView, self).get_context_data(**kwargs)
        specialty = get_object_or_404(Specialty, number=self.kwargs['specialty_number'])
        headers = [u"Номер", u"Сотрудник", u"Подразделение", u"Должность",
            u"Звание по спец.", u"Чл. в совете по спец.", u"Соотв. спец. по дисс.", u"Руковод.", u"Оппон.",
            u"Индекс Хирша", u"Цит. WoS", u"Цит. Scopus", u"Баллы по ф-ле подразд."]
        cache_key = "statistics_top_workers_by_specialty_v3_%d" % specialty.id
        rows = cache.get(cache_key)
        if not rows:
            rows = []
            workers = self.get_top_workers_by_specialty(specialty)
            for i, worker in enumerate(workers, start=1):
                citcount = get_worker_citcount(worker.id)
                rows.append((
                    i,
                    mark_safe('<a href="%s" target="_blank">%s</a>' % (worker.get_absolute_url(), worker.fullname)),
                    worker.current_main_first_root_department,
                    worker.main_position,
                    mark_safe(get_icon_by_bool(worker.ranks.filter(branch_num__startswith=specialty.number).exists(), only_true=True)),
                    mark_safe(get_icon_by_bool(worker.dissertation_councils_memberships.filter(specialty=specialty).exists(), only_true=True)),
                    mark_safe(get_icon_by_bool(worker.dissertations_defended.filter(Q(specialty=specialty) | Q(specialty2=specialty)).exists(), only_true=True)),
                    worker._dissertations_advised,
                    worker.dissertations_opposed.filter(Q(specialty=specialty) | Q(specialty2=specialty)).distinct().count(),
                    citcount['worker_hirsh'].hirsh if citcount['worker_hirsh'] else 0,
                    citcount['worker_hirsh'].total if citcount['worker_hirsh'] else 0,
                    getattr(worker.citation_counts.getdefault(system__full_name="Scopus"), "value", 0),
                    worker._points
                ))
            cache.set(cache_key, rows, 60*60*12) # 12 hours
        context.update({
            'specialty': specialty,
            'headers': headers,
            'data': rows
        })
        return context

def top_articles(request, worker=None, my=False, worker_id=None, year=None, edit=False, by_department=False):
    '''
    If edit=False, show only confirmed articles. If true (and the user has access), show all top articles and enable editing.
    If by_department=True, show only articles by workers in request.user's main department(s).
    If edit=True and user has only permission confirm_department_article, then the department presented is the department
    for which he is a representative for. If he is not representative for any department, then his root department is taken.
    '''
    can_edit = has_permission(request.user, 'confirm_any_article') \
        or (by_department and has_permission(request.user, 'confirm_department_articles'))
    if edit and not can_edit: # access control
        edit = False
    only_confirmed = not edit and not by_department # in edit mode or by_department mode, show all articles; in normal mode - only confirmed ones
    use_cache = not edit and not by_department
    # if this is all top articles edit page (not by department),
    # and user has rights to do that,
    # then deconfirmation deletes all confirmations by all users
    # otherwise user can delete only his confirmation for the article
    deconfirm_all = int(bool(edit and not by_department))
    if not year:
        # can't set it as an argument default value, because if it is not set, urls sends None, bypassing default value
        import datetime
        year = (datetime.date.today()-datetime.timedelta(60)).year

    if worker or worker_id or my: # articles by worker
        if not worker:
            return worker_required(top_articles)(request, my=my, worker_id=worker_id, edit=edit)
        articles_ids = get_top_articles_ids_by_worker(worker.id, year=year)
        template = "statistics/top_articles_by_worker.html"
        articles = Article.objects.filter(id__in=articles_ids)
        workers_registered = ArticleAuthorship.objects.get_registered_authors(articles_ids)
    else: # all top articles
        template = "statistics/top_articles.html"
        articles = []
        workers_registered = []
        if use_cache:
            articles = bigdata_cache.get('statistics_top_articles_articles_%d' % int(year))
            workers_registered = bigdata_cache.get('statistics_top_articles_users')
        if not articles or not workers_registered:
            kwargs = {}
            if by_department:
                departments = []
                worker = get_worker(request.user)
                if worker:
                    if edit and not has_permission(request.user, 'confirm_any_article'):
                        # try to take the department for which the user is representative for
                        # if not found, fallback for his current_main_root_departments_ids
                        departments = request.user.representatives.filter(
                            is_active=True).values_list('department', flat=True)
                    if not departments:
                        departments = worker.current_main_root_departments_ids
                if not departments:
                    if not request.user.is_authenticated():
                        messages.error(request, u'Для просмотра статей сотрудников подразделения необходимо войти в систему')
                        return redirect("startpage")
                    else:
                        messages.error(request, u'Для просмотра статей сотрудников подразделения необходимо в профиле указать место работы')
                        return redirect("home")
                else:
                    kwargs['department_ids'] = departments
            articles_ids = get_top_articles_ids(year, **kwargs)
            if only_confirmed:
                confirmed_articles_ids = ConfirmedArticle.objects.select_from(articles_ids)
                articles = list(Article.objects.filter(id__in=confirmed_articles_ids))
            else:
                articles = list(Article.objects.filter(id__in=articles_ids))
            articles.sort(key=lambda article: articles_ids.index(article.id)) # we want to preserve order of ids returned from sql query
            workers_registered = ArticleAuthorship.objects.get_registered_authors(confirmed_articles_ids if only_confirmed else articles_ids)
            if use_cache:
                bigdata_cache.set('statistics_top_articles_articles_%d' % int(year), articles, 60*60*24) # 1 day
                bigdata_cache.set('statistics_top_articles_users', workers_registered, 60*60*24) # 1 day

    if my: # this is my top articles page
        for article in articles:
            SeenArticle.objects.create(user=request.user, article=article)
        if not articles:
            SeenArticle.objects.create(user=request.user)

    return render(request, template, {"articles": articles, 'worker': worker,
        'edit': edit, 'can_edit': can_edit, 'workers_registered': workers_registered,
        "year": year, "by_department": by_department, 'deconfirm_all': deconfirm_all})


def top_article_confirm(request, article_id, confirm, deconfirm_all=False):
    """
    @deconfirm_all is used only when confirm=False. If deconfirm_all=True, delete *all* confirmations for article
    by any user (if current user has a permission to do that),
    otherwise delete only confirmations by current user.
    """
    can_confirm = has_permission(request.user, 'confirm_any_article') \
        or (not deconfirm_all and has_permission(request.user, 'confirm_department_articles'))
    if not can_confirm:
        return HttpResponseForbidden()
    article = Article.objects.get(id=article_id)
    if confirm:
        confirmed_article, _ = ConfirmedArticle.objects.get_or_create(article=article, user=request.user)
    else:
        if deconfirm_all:
            articles_to_delete = ConfirmedArticle.objects.filter(article=article)
        else:
            articles_to_delete = ConfirmedArticle.objects.filter(article=article, user=request.user)
        for article_to_delete in articles_to_delete.all():
            article_to_delete.deleting_user = request.user
            article_to_delete.delete()
    TopArticle.objects.invalidate_cache()
    return HttpResponse(simplejson.dumps({'status': "ok"}), content_type='application/javascript')


def last_month_activities(request):
    from django.template.loader import render_to_string
    from django.utils.safestring import mark_safe
    month_ago = datetime.datetime.now() - datetime.timedelta(30)
    raw_activities = get_activities_last_added(from_date=month_ago, raw=True)
    context = paginate(request, raw_activities, per_page=30, endless=False)
    context_p = context["paginator"]
    if "objects" in context_p:
        context_p["objects"] = fetch_activities_compact(context_p["objects"])
    context["paging"] = mark_safe(render_to_string('paging/pager.html', context))
    return render(request, "statistics/last_month_activities.html", context)



@login_required
def department_stats(request, dep_id, year=None):
    isRepresentative = False
    department = get_object_or_404(Department, pk=dep_id)
    years_for_filter = [datetime.date.today().year-i for i in range(6)] # 6 last years to show as filter values
    if (request.user.is_authenticated() and is_representative_for(request.user, department)) or request.user.is_superuser or request.user.username in ['safonin', 'voroninaen', 'shibaev','smamakina']:
        isRepresentative = True
    pubtypesstat = get_publications_stat(dep_id, year)
    workers = get_top_workers_for_dep(dep_id, year)
    popularjournals = get_popular_journals_for_dep(dep_id, year)
    depdata_req =  request.GET.get('data', 'none')
    export_excel = request.GET.get('excel', False)
    title = u"Статистика подразделения"
    context = {'department': department}

    if depdata_req in ['conf', 'conflist', 'confcomm', 'pubs', 'jboards', 'dissovets']:
        headers, data = get_department_data(dep_id, year, depdata_req)
        if isinstance(data, HttpResponse):
            return data
        excel_args = {'export_excel': True, 'excel_sheetname': u'Статистика',
            'excel_filename': 'department-statistics-%s.%s.%s.xls' % (depdata_req, dep_id, str(year) if year else "all")} if export_excel else {}
        if export_excel:
            headers = [[department.name, title], headers]
        return format_report(request, title, headers, data, context, **excel_args)
    else:
        return render(request, "statistics/department_stats.html",
                  {"pubtypesstat": pubtypesstat,
                   "department": department,
                   "workers": workers,
                   "popularjournals": popularjournals,
                   "year": year,
                   "years_for_filter": years_for_filter,
                   "isRepresentative": isRepresentative,
                   "adata": generate_adata(request),
                   })


def top_departments(request):
    return render(request, "statistics/topdep.html")

# Hirsh index
def department_hirsh(request, dep_id):
    department = get_object_or_404(Department, pk=dep_id)
    workers = get_workers_hirsh_for_dep(dep_id)
    return render(request, "statistics/department_hirsh.html",
                  {"workers": workers,
                   "department": department,
                  })

def department_citcount_by_year(request, dep_id):
    department = get_object_or_404(Department, pk=dep_id)
    headers, data = get_department_citcounts_by_year(dep_id)
    return render(request, "statistics/generic_table.html", {"data": data, "headers": headers,
                                                             "department": department})


def get_org_tree(request):
    '''Return organization tree as json for dynatree selector
    '''
    from django.db import connection, transaction

    org_id = request.GET.get('org_id', None)
    try:
        org_id = int(org_id)
    except:
        return HttpResponse(simplejson.dumps([]), content_type='application/javascript')
        #org_id = 214524 # MSU ID


    # build complete tree
    message_data = list()
    try:
        cursor = connection.cursor()

        sql = """
        select id, title, tree_level
        from (
        select d.F_DEPARTMENT_ID as id, d.F_DEPARTMENT_NAME as title, level as tree_level
        from (select * from department where f_organization_id = %s) d,
             (select F_DEPARTMENT_ID, DEP_F_DEPARTMENT_ID
              from departmentlink
              UNION ALL
              (select F_DEPARTMENT_ID, NULL
               from department
               where not exists (select 1 from departmentlink where departmentlink.f_department_id = department.F_DEPARTMENT_ID)
              )) dl
        where dl.F_DEPARTMENT_ID =  d.F_DEPARTMENT_ID
        connect by prior dl.F_DEPARTMENT_ID = dl.DEP_F_DEPARTMENT_ID
        start with dl.DEP_F_DEPARTMENT_ID is NULL
        order siblings by d.F_DEPARTMENT_NAME
        )
        UNION ALL
        select 1, 'dummy', 1 from dual
        """
        cursor = connection.cursor()
        cursor.execute(sql, [org_id])


        # convert into json representation accepted by jquery.dynatree
        prev = None
        stack = [] # stack contains pairs (m, i), where m is a list of completed descriptions
                   # of all nodes at the same level and i is the last item that should be
                   # appended to m when its children field will be constructed.
        count = 0
        for key, title, level in cursor:
            if prev:
                if level == prev["level"]:
                    message_data.append({"title": prev['title'], "key": prev['key'], "isFolder": False, "isLazy": False})
                elif level > prev["level"]:
                    item = {"title": prev['title'], "key": prev['key'], "isFolder": True, "unselectable": False, "isLazy": False}
                    stack.append((message_data, item))
                    message_data = []
                elif level < prev["level"]:
                    message_data.append({"title": prev['title'], "key": prev['key'], "isFolder": False, "isLazy": False})
                    while level < prev["level"]:
                        last_list, last_item = stack.pop()
                        last_item["children"] = message_data
                        message_data = last_list
                        message_data.append(last_item)
                        prev["level"] = prev["level"]-1
            prev = {"key": key, "title": title, "level": level}
    except:
        transaction.rollback()

    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')


def rewarded_articles(request, project_id):
    '''Show all rewarded articles by rewarded project.'''
    project = get_object_or_404(RankingProject, pk=project_id)
    articles_ids = project.rewarded_articles.values_list('article', flat=True)
    articles = Article.objects.filter(id__in=articles_ids).order_by('title')
    workers_registered = ArticleAuthorship.objects.get_registered_authors(articles_ids)

    return render(request, "statistics/rewarded_articles.html", {"project": project, "articles": articles, 'workers_registered': workers_registered})



def departments_achievements(request):
    departments = DepartmentStatistics.objects.all()
    return render(request, "statistics/departments_achievements.html", {"departments": departments})


def get_report(request, report_title, headers, make_row,
        department_id=None, worker=None,
        bibtex_column=None, filters={},
        export_excel=False, excel_sheetname=u'Отчет', excel_filename=None,
        books=False, conferences=False, post_process=None):
    """
    bibtex_column is the 0-based number of column in which bibtex-formatted html should be displayed.
    """
    if department_id:
        department = get_object_or_404(Department, pk=department_id)
        conditions = {'department': department_id}
        context = {'department': department}
    elif worker:
        conditions = {'man': worker.id}
        context = {'worker': worker}
    else:
        raise Http404
    if filters:
        conditions['FREE'] = filters
    if books:
        conditions['books'] = 'yes'
    elif conferences:
        conditions['confs'] = 'yes'
    generator = get_department_pubs_generic({"ALL": 1}, conditions)
    works = []
    rows = []
    for i, work in enumerate(generator, start=1):
        works.append(work)
        rows.append(make_row(i, work))
    if bibtex_column:
        html_records = bibtex2html_multiple(works)
        rows = map(lambda item: item[0][:bibtex_column] + [mark_safe(item[1])] + item[0][bibtex_column+1:], zip(rows, html_records))
    if post_process:
        rows = map(post_process, rows)
        # mark every cell safe again after post-processing
        rows = map(lambda row: map(lambda cell: mark_safe(cell), row), rows)
    return format_report(request, report_title, headers, rows, context,
        export_excel, excel_sheetname, excel_filename)

def format_report(request, report_title, headers, rows, context={},
        export_excel=False, excel_sheetname=u'Отчет', excel_filename=None):
    if export_excel:
        if not isinstance(headers[0], list):
            # headers is a single list, make it a list of lists
            headers = [headers]
        return export_to_excel_response(headers+rows, excel_sheetname, excel_filename)
    else:
        context.update({
            "report_title": report_title,
            "headers": headers,
            "data": rows
        })
        return render(request, "statistics/generic_table.html", context)


def activity_stats(request):
    from investments.models import Investment
    from django.contrib.auth.models import User
    from organizations.models import Organization
    from actstream.models import Action

    rows = cache.get('statistics_activity_rows')

    if not rows:
        rows = []
        activity_models = get_activity_models()
        exclude_models = [Investment]
        cap_first = lambda s: s[0].upper()+s[1:]
        for model in activity_models:
            if isinstance(model.genitive_plural_full, unicode) and not model in exclude_models:
                rows.append([cap_first(model.genitive_plural_full), model.objects.count()])
            else:
                # print model
                pass

        msu = Organization.objects.get_msu()
        one_week_ago = datetime.datetime.now() - datetime.timedelta(7)

        rows.extend([
            [u"Статей", Article.objects_pure.count()],
            [u"Тезисов докладов", Article.objects_theses.count()],
            [u"Пользователей", User.objects.filter(is_active=True).count()],
            [u"Пользователей из МГУ", msu.workers_count],
            [u"Пользователей, новых за последнюю неделю",
                Action.objects.filter(timestamp__gte=one_week_ago,verb=u"зарегистрировался").count()],
            [u"Соавторов", Worker.objects.count()],
            [u"Докторов наук", Worker.objects.filter_doctors().count()],
            [u"Кандидатов наук", Worker.objects.filter_candidates().count()],
        ])

        # cache for 1 hour
        cache.set('statistics_activity_rows', rows, 60 * 60)

    context = {
        "report_title": "Статистика системы",
        "headers": ["Тип объекта", "Количество объектов"],
        "data": rows
    }
    return render(request, "statistics/generic_table.html", context)




def format_pubs_generic_conditions(request):
    params_dict = {}
    if request.method == "GET":
        params_dict = request.GET
    if request.method == "POST":
        params_dict = request.POST
    ids = params_dict.get('department_id', '')
    rub_ids = params_dict.get('rubid', '')
    metric = params_dict.get('metric', 'count')
    #year = params_dict.get('year', 'All')
    confirmed = params_dict.get('confirmed', None)
    pub_type = params_dict.get('pub_type', 'all')
    title_filter = params_dict.get("publication_name", "")
    if title_filter == "":
        title_filter = request.GET.get("s", title_filter)
    topic_id = params_dict.get("rub_id", None)
    dep_id = None


    try:
        ids = filter(None, ids.split(","))
        map(int, ids) # side-effect: maybe ValueError
        if len(ids)>0:
            dep_id = ids[0]
        rub_ids = map(int, filter(None, rub_ids.split(",")))
    except ValueError:
        return HttpResponse("1=0 /* bad ids */")

    try:
        if topic_id:
            topic_id = int(topic_id)
    except ValueError:
        pass

    try:
        if confirmed:
            confirmed = int(confirmed)
    except ValueError:
        pass

    free_cond = "1=1"
    #try:
    #    free_cond = free_cond + ' and year = ' + str(int(year))
    #except ValueError:
    #    pass


        #new block for processing list of years from request
    #selectedYearClusterCategories -- field name from publication_search_result.html template
    try:
        years = dict(request.GET).get("year", {})
        if years:
            yearlist = "0";
            for _year in years:
                if _year.isdigit():
                    yearlist = yearlist + "," + str(_year)
            yearlist = " and year in (" + yearlist + ")"
            free_cond = free_cond + yearlist
    except:
        pass

    try:
        departments = dict(request.GET).get("selectedDepartment", {})
        if departments:
            deplist = "0";
            for _dep in departments:
                if _dep.isdigit():
                    deplist = deplist + "," + str(_dep)
            dep_id = deplist
    except:
        pass
    # end of yearlist creatin block

    if pub_type == 'top25j':
        free_cond = free_cond + ' and in_top25=1'
    elif pub_type == 'wosj':
        free_cond = free_cond + ' and in_wos=1'
    elif pub_type == 'scopus':
        free_cond = free_cond + ' and in_scopus=1'
    elif pub_type == 'vak':
        free_cond = free_cond + ' and in_vak=1'
    elif pub_type == 'allj':
        free_cond = free_cond + u" and category='Статьи в журналах'"
    elif pub_type == 'onlyp':
        free_cond = free_cond + u" and category='Статьи в сборниках'"
    elif pub_type == 'theses':
        free_cond = free_cond + u" and category='Тезисы'"

    #print free_cond
    free_cond_nv = free_cond
    if len(title_filter)>3:
        free_cond = free_cond + u" and (name like '%%' || '" + title_filter.replace("'", "''") + "' || '%%')"
    #print free_cond, dep_id, topic_id
    return {"department": dep_id,
            "topic": topic_id,
            "confirmed": confirmed,
            "FREE": free_cond,
            "FREE_NEW": free_cond_nv}
