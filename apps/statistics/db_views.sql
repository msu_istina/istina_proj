create or replace view duplicated_men
as
select man2.f_man_id, man2.f_man_create,
       prof.user_id as assigned_to, man2.f_man_user as added_by,
       nvl(act_a.cnt,0) narticles,  nvl(act_bcd.cnt,0) nothers, nvl(nma.cnt,0) as naliases, mandub.*
from man man2, workers_profile prof,
(
  select f_man_id, count(*) cnt
  from authora
  group by f_man_id
) act_a,
(
  select f_man_id, count(*) cnt
  from (
    select f_man_id from authorb UNION ALL
    select f_man_id from authorc UNION ALL
    select f_man_id from authord UNION ALL
    select f_man_id from authorp UNION ALL   
    select f_man_id from authorpog UNION ALL
    select f_man_id from authorrep UNION ALL
    select f_man_id from disser UNION ALL
    select f_man_id from diserhead UNION ALL
    select f_man_id from diplomhead UNION ALL
    select f_man_id from redjournal UNION ALL
    select f_man_id from manspost UNION ALL
    select f_man_id from manalias
  )
  group by f_man_id
) act_bcd,
(
    select f_man_id, count(*) cnt from manalias group by f_man_id
) nma,
( 
  select f_man_namel, f_man_namef, f_man_namem, min(f_man_id) as rn
    from man
    group by f_man_namel, f_man_namef, f_man_namem
    having count(*)>1
) mandub
where man2.f_man_namel=mandub.f_man_namel
  and (man2.f_man_namef=mandub.f_man_namef or (man2.f_man_namef is null and mandub.f_man_namef is null))
  and (man2.f_man_namem=mandub.f_man_namem or (man2.f_man_namem is null and mandub.f_man_namem is null))
  and act_a.f_man_id(+) = man2.f_man_id
  and act_bcd.f_man_id(+) = man2.f_man_id
  and prof.worker_id(+) = man2.f_man_id
  and nma.f_man_id(+) = man2.f_man_id
;


-- old version of this view
create or replace view v_journal_standings
as
select jrl.f_journal_id, jrl.f_journalrub_id, jrg.f_journalrang_val,
jrg.f_journalrang_type,
trunc(PERCENT_RANK() OVER (PARTITION BY jrl.f_journalrub_id, f_journalrang_type ORDER BY jrg.f_journalrang_val desc),2) r
from journalrang jrg, journalrublink jrl
where jrl.f_journal_id = jrg.f_journal_id
  and jrl.f_journalrub_id in
      (select f_journalrub_id
       from journalrubtype jrt, journalrub jrub
       where jrub.f_journalrubtype_id = jrt.f_journalrubtype_id
         and f_journalrubtype_name in ('Web of Science','Medline'))
  and jrg.f_journalrang_type in ('5-Year Impact Factor 2010','Impact Factor 2010')
UNION ALL
select jrl.f_journal_id, jrl.f_journalrub_id, jrg.f_journalrang_val,
jrg.f_journalrang_type,
0.25 r
from journalrang jrg, journalrublink jrl
where jrl.f_journal_id = jrg.f_journal_id
  and jrl.f_journal_id in (83226, 76376, 57245, 73924) --journals erroneousely inserted before
  and jrl.f_journalrub_id in
      (select f_journalrub_id
       from journalrubtype jrt, journalrub jrub
       where jrub.f_journalrubtype_id = jrt.f_journalrubtype_id
         and f_journalrubtype_name in ('Web of Science','Medline'))
  and jrg.f_journalrang_type in ('5-Year Impact Factor 2010','Impact Factor 2010')  
;


-- HACK: 2011 IF looks like 2010
create or replace view v_journal_standings
as
select jrl.f_journal_id, jrl.f_journalrub_id, jrg.f_journalrang_val,
jrg.f_journalrang_type,
trunc(PERCENT_RANK() OVER (PARTITION BY jrl.f_journalrub_id, f_journalrang_type ORDER BY jrg.f_journalrang_val desc),2) r
from journalrang jrg, journalrublink jrl
where jrl.f_journal_id = jrg.f_journal_id
  and jrl.f_journalrub_id in
      (select f_journalrub_id
       from journalrubtype jrt, journalrub jrub
       where jrub.f_journalrubtype_id = jrt.f_journalrubtype_id
         and f_journalrubtype_name in ('Web of Science','Medline'))
  and jrg.f_journalrang_type in ('5-Year Impact Factor 2010','Impact Factor 2010')
UNION ALL
select jrl.f_journal_id, jrl.f_journalrub_id, jrg.f_journalrang_val,
replace(jrg.f_journalrang_type, '2011', '2010') f_journalrang_type,
trunc(PERCENT_RANK() OVER (PARTITION BY jrl.f_journalrub_id, f_journalrang_type ORDER BY jrg.f_journalrang_val desc),2) r
from journalrang jrg, journalrublink jrl
where jrl.f_journal_id = jrg.f_journal_id
  and jrl.f_journalrub_id in
      (select f_journalrub_id
       from journalrubtype jrt, journalrub jrub
       where jrub.f_journalrubtype_id = jrt.f_journalrubtype_id
         and f_journalrubtype_name in ('Web of Science','Medline'))
  and jrg.f_journalrang_type in ('5-Year Impact Factor 2011','Impact Factor 2011')
UNION ALL
select jrl.f_journal_id, jrl.f_journalrub_id, jrg.f_journalrang_val,
jrg.f_journalrang_type,
0.25 r
from journalrang jrg, journalrublink jrl
where jrl.f_journal_id = jrg.f_journal_id
  and jrl.f_journal_id in (83226, 76376, 57245, 73924) --journals erroneousely inserted before
  and jrl.f_journalrub_id in
      (select f_journalrub_id
       from journalrubtype jrt, journalrub jrub
       where jrub.f_journalrubtype_id = jrt.f_journalrubtype_id
         and f_journalrubtype_name in ('Web of Science','Medline'))
  and jrg.f_journalrang_type in ('5-Year Impact Factor 2010','Impact Factor 2010', '5-Year Impact Factor 2010','Impact Factor 2010')  
;


create materialized view mv_journal_standings
  REFRESH START WITH SYSDATE NEXT ROUND(SYSDATE + 1) + 3/24
as
select jrl.f_journal_id, jrl.f_journalrub_id, jrg.f_journalrang_val,
jrg.f_journalrang_type,
trunc(PERCENT_RANK() OVER (PARTITION BY jrl.f_journalrub_id, f_journalrang_type
                            ORDER BY jrg.f_journalrang_val desc),2) r
from journalrang jrg, journalrublink jrl
where jrl.f_journal_id = jrg.f_journal_id
  and jrl.f_journalrub_id in
      (select f_journalrub_id
       from journalrubtype jrt, journalrub jrub
       where jrub.f_journalrubtype_id = jrt.f_journalrubtype_id
         and f_journalrubtype_name in ('Web of Science'))
  and (jrg.f_journalrang_type like '5-Year Impact Factor 201_'
      OR jrg.f_journalrang_type like 'Impact Factor 201_')
;

create index i_mv_jstand_journal_id on mv_journal_standings(f_journal_id);
create index i_mv_jstand_journalrang_type on mv_journal_standings(f_journalrang_type);
create index i_mv_jstand_r on mv_journal_standings(r);
create index i_mv_jstand_jrr on mv_journal_standings(f_journalrang_type, r, f_journal_id);


create or replace view v_journal_standings
as
select f_journal_id, f_journalrub_id, f_journalrang_val,
       substr(f_journalrang_type, 0, length(f_journalrang_type)-1) || '0' as f_journalrang_type,
       r
from mv_journal_standings
UNION ALL
select jrl.f_journal_id, jrl.f_journalrub_id, jrg.f_journalrang_val,
       jrg.f_journalrang_type, 0.25 r
from journalrang jrg, journalrublink jrl
where jrl.f_journal_id = jrg.f_journal_id
  and jrl.f_journal_id in (83226, 76376, 57245, 73924, --journals erroneousely inserted before
                51348, 54969, 55060, 57245, 63408, 64073, 79742, 91763)
  and jrl.f_journalrub_id in
      (select f_journalrub_id
       from journalrubtype jrt, journalrub jrub
       where jrub.f_journalrubtype_id = jrt.f_journalrubtype_id
         and f_journalrubtype_name in ('Web of Science'))
  and jrg.f_journalrang_type in ('5-Year Impact Factor 2010','Impact Factor 2010')
;


create or replace view v_journal_standings_all_year
as
select jrl.f_journal_id, jrl.f_journalrub_id, jrub.f_journalrubtype_id, jrg.f_journalrang_val,
       jrg.f_journalrang_type, jrg.f_journalrang_year,
       trunc(PERCENT_RANK() OVER (PARTITION BY jrl.f_journalrub_id, f_journalrang_type, jrg.f_journalrang_year
                                  ORDER BY jrg.f_journalrang_val desc),4) r
from journalrang jrg, journalrublink jrl, journalrub jrub
where jrl.f_journal_id = jrg.f_journal_id
  and jrl.f_journalrub_id = jrub.f_journalrub_id
;


create or replace view v_journal_standings_all
as
select jrl.f_journal_id, jrl.f_journalrub_id, jrub.f_journalrubtype_id, jrg.f_journalrang_val,
jrg.f_journalrang_type,
trunc(PERCENT_RANK() OVER (PARTITION BY jrl.f_journalrub_id, f_journalrang_type ORDER BY jrg.f_journalrang_val desc),4) r
from journalrang jrg, journalrublink jrl, journalrub jrub --, journalrubtype jrt
where jrl.f_journal_id = jrg.f_journal_id
  and jrl.f_journalrub_id = jrub.f_journalrub_id
;


create or replace view v_wos_top25_journals
as
select jsa.f_journal_id
from v_journal_standings jsa
where
  (jsa.f_journalrang_type like '5-Year Impact Factor 201_'
   or jsa.f_journalrang_type like 'Impact Factor 201_')
  and trunc(jsa.r,2)<=0.25
UNION  
   select jv.f_journal_id
   from jornalvak jv
   where jv.f_journalvaktype_id=(select f_journalvaktype_id from journalvaktype
                                  where f_journalvaktype_name='Arts & Humanities Citation Index')
;



create or replace view v_wos_top25_articles
as
    select a.f_article_id
    from article a
    where
       a.f_journal_id in (select f_journal_id from v_wos_top25_journals)
;



-- lost articles
 create or replace view v_lost_articles as
 select distinct aa.f_authora_id, aa.f_article_id, user_aliases.f_man_id, user_aliases.user_id, aa.f_authora_name, aa.f_authora_user 
 from  authora aa,
 (select ma.f_man_id, wp.user_id,
         '%' || nls_upper(ma.f_manalias_namel) || '%' f_manalias_pattern,
         '('
         || translate(nls_upper(ma.f_manalias_namel), 'a().?+[]', 'a')
         || '[ ]+'
         || translate(nls_upper(substr(ma.f_manalias_namef,1,1)), 'a().?+[]','a') || '.*)'
         || '|('
         || translate(nls_upper(substr(ma.f_manalias_namef,1,1)), 'a().?+[]', 'a')
         || '[^ .]*[ .]?'
         || translate(nls_upper(ma.f_manalias_namel), 'a().?+[]', 'a')
         || ')' AS  f_manalias_regex
   from manalias ma, workers_profile wp
   where length(ma.f_manalias_namel) > 2
     --and exists (select 1 from manspost mp where mp.f_man_id = m2.f_man_id)
     and wp.worker_id = ma.f_man_id
 ) user_aliases
 where
   (aa.f_man_id is null
      or (aa.f_man_id <> user_aliases.f_man_id and aa.f_man_id not in (select worker_id from workers_profile where worker_id is not null))
   )
   and
   1=
   case when
     nls_upper(aa.f_authora_name) like user_aliases.f_manalias_pattern
   then
     case when regexp_like(nls_upper(aa.f_authora_name), user_aliases.f_manalias_regex) then
      1
     else
      0
     end
   else 0
   end
;


-- select top level department ids for f_man_id
create or replace view
v_man_top_department
as
select man.*,
       (
        select dep_f_department_id
        from departmentlink
        start with f_department_id = A.f_department_id
        connect by prior dep_f_department_id = f_department_id
        minus
        select f_department_id
        from departmentlink
        start with f_department_id = A.f_department_id
        connect by prior dep_f_department_id = f_department_id
      ) top_level_department_id
from man,
     (
       select f_man_id, f_department_id
       from manspost
       where f_manspost_begin<sysdate 
         and (f_manspost_end>sysdate or f_manspost_end is null)
     ) A
where man.f_man_id=A.f_man_id;


create or replace view v_man_workplaces as
   select distinct
     mp.f_man_id,
     nvl(mp.f_manspost_part, 0) f_manspost_part,
     d.f_department_id,
     mp.f_manspost_begin, mp.f_manspost_end,
     decode(d.f_department_id, mp.f_department_id, 1, 0) exact_workplace
     from manspost mp, department d, departmentlink dl1, departmentlink dl2
     where dl1.f_department_id (+) = mp.f_department_id
       and dl2.f_department_id (+) = dl1.dep_f_department_id
       and ( d.f_department_id = mp.f_department_id
             or d.f_department_id = dl1.dep_f_department_id
             or d.f_department_id = dl2.dep_f_department_id
           )
;

create or replace view all_man_department as
select manspost.f_man_id,
       parent_f_department_id department_id,
       greatest(nvl(f_manspost_begin,to_date('01.01.1755','dd.mm.yyyy')), nvl(b,to_date('01.01.1755','dd.mm.yyyy'))) b,
       case when least(nvl(f_manspost_end,to_date('01.01.3000','dd.mm.yyyy')), nvl(e,to_date('01.01.3000','dd.mm.yyyy')))=to_date('01.01.3000','dd.mm.yyyy') then
        null
       else
        least(nvl(f_manspost_end,to_date('01.01.3000','dd.mm.yyyy')), nvl(e,to_date('01.01.3000','dd.mm.yyyy')))
       end e,
       l,
       f_manspost_part
from manspost,
(
select D.parent_f_department_id, D.f_department_id,
max(departmentlink.f_departmentlink_begin) over (partition by D.f_department_id order by l) b,
min(departmentlink.f_departmentlink_end) over (partition by D.f_department_id order by l) e,
l
from
departmentlink,
(
select f_departmentlink_id, dep_f_department_id parent_f_department_id, CONNECT_BY_ROOT f_department_id f_department_id, level l
from departmentlink
connect by departmentlink.f_department_id=prior departmentlink.dep_f_department_id
union all
select null, f_department_id, f_department_id, 0 l from department
) D
where d.f_departmentlink_id=departmentlink.f_departmentlink_id(+)
) M
where manspost.f_department_id=m.f_department_id
and greatest(nvl(f_manspost_begin,to_date('01.01.1755','dd.mm.yyyy')), nvl(b,to_date('01.01.1755','dd.mm.yyyy')))<
       least(nvl(f_manspost_end,to_date('01.01.3000','dd.mm.yyyy')), nvl(e,to_date('01.01.3000','dd.mm.yyyy')))
;


-- view crated for backward compatibility
create or replace view v_man_workplaces as
select f_man_id, nvl(f_manspost_part, 0) AS f_manspost_part,
  department_id AS f_department_id,
  b as f_manspost_begin, e as f_manspost_end,
  decode(l, 0, 1, 0) as exact_workplace,
  l
from all_man_department
;


-- select all articles with authorsw ids written by department members
create or replace view v_department_article_authors as
select mwp.f_department_id, aa.f_article_id, aa.f_man_id, mwp.exact_workplace
from authora aa, article a, v_man_workplaces mwp
where 
  aa.f_article_id = a.f_article_id
  and aa.f_man_id = mwp.f_man_id
  -- проверка того, что сотрудник работал в подразделении
  -- в момент выхода публикации
  and (mwp.f_manspost_end is null
       or
       (a.f_article_year >= extract(year from mwp.f_manspost_begin) and
        a.f_article_year <= extract(year from mwp.f_manspost_end)))
group by aa.f_article_id, mwp.f_department_id, aa.f_man_id, mwp.exact_workplace
;

-- select all articles written by department members
create or replace view v_department_articles as
select mwp.f_department_id, aa.f_article_id, a.f_article_year  --, aa.f_man_id 
from authora aa, article a, v_man_workplaces mwp
where 
  aa.f_article_id = a.f_article_id
  and aa.f_man_id = mwp.f_man_id
  -- проверка того, что сотрудник работал в подразделении
  -- в момент выхода публикации
  and (mwp.f_manspost_end is null
       or
       (a.f_article_year >= extract(year from mwp.f_manspost_begin) and
        a.f_article_year <= extract(year from mwp.f_manspost_end)))
group by aa.f_article_id, mwp.f_department_id, a.f_article_year
;


-- Old slow version
-- create or replace view v_article_coefficient as
       select f_article_id, 1/sum(nauthors) coeff, sum(nauthors) nauthors
        from (
          select aa.f_article_id,  
            case
              when nls_upper(aa.f_authora_name) like '%ATLAS COLLABORATION%' then 2900
              when nls_upper(aa.f_authora_name) like '%CLAS COLLABORATION%' then 150
              when nls_upper(aa.f_authora_name) like '%CMS COLLABORATION%' then 2200
              when nls_upper(aa.f_authora_name) like '%COLLABORATION D0%' then 400
              when nls_upper(aa.f_authora_name) like '%D0 COLLABORATION%' then 400              
              when nls_upper(aa.f_authora_name) like '%OPERA COLLABORATION%' then 190
              when nls_upper(aa.f_authora_name) like '%ZEUS COLLABORATION%' then 300
              when nls_upper(aa.f_authora_name) like '%DIRAC COLLABORATION%' then 80
              when nls_upper(aa.f_authora_name) like '%LHCB COLLABORATION%' then 600
              when nls_upper(aa.f_authora_name) like '%ALICE COLLABORATION%' then 1000
              when aa.f_authora_name like 'ALICE C%' then 1000
              when nls_upper(aa.f_authora_name) like '%ET AL%' then 20
              when nls_upper(aa.f_authora_name) like '%ET.%AL%' then 20
              when nls_upper(aa.f_authora_name) like '%OTHERS%' then 20
              when nls_upper(aa.f_authora_name) like '%COLLABORATION%' then 199
              else 1
            end nauthors
          from authora aa
        )
        group by f_article_id;


create or replace view v_article_coefficient
as
select f_article_id, 1/nauthors coeff, nauthors
from (
  select f_article_id, sum(nvl(collaboration.f_collaboration_count, 1)) nauthors
  from authora
    left join collaboration on (collaboration.f_collaboration_id = authora.f_collaboration_id)
  group by f_article_id
);


-- coauthors
create or replace view v_coauthors
as
select m.f_man_id as author_man_id,
       coaaa.f_man_id as coauthor_man_id,
       count(*) n_works, 'article' as work_type
from authora aaa, man m, authora coaaa
where m.f_man_id = aaa.f_man_id
  and coaaa.f_article_id = aaa.f_article_id
  and aaa.f_man_id <> coaaa.f_man_id
group by m.f_man_id, coaaa.f_man_id
UNION ALL
select m.f_man_id as author_man_id,
       coaab.f_man_id as coauthor_man_id,
       count(*) n_works, 'book' as work_type
from authorb aab, man m, authorb coaab
where m.f_man_id = aab.f_man_id
  and coaab.f_book_id = aab.f_book_id
  and aab.f_man_id <> coaab.f_man_id
group by m.f_man_id, coaab.f_man_id
;



--
-- Calculate hirsh-index based on citcount table
--
CREATE OR REPLACE VIEW v_citcount AS
  SELECT eid.f_activity_id AS f_article_id,
       eid.f_bibsource_id AS f_citcount_type,
       ecc.f_extcitcount_externalid AS f_extcitcount_externalid,
       nvl(max(ecc.f_extcitcount_val), 0) AS f_citcount_val,
       max(ecc.f_extcitcount_date) AS f_citcount_date
FROM externalid eid
  LEFT JOIN extcitcount ecc ON (ecc.f_extcitcount_externalid = eid.f_externalid_externalid
                                AND ecc.f_bibsource_id = eid.f_bibsource_id)
  JOIN bibsource ON (bibsource.f_bibsource_id = eid.f_bibsource_id)
WHERE bibsource.f_bibsource_iscitindex = '1'
GROUP BY  eid.f_activity_id, eid.f_bibsource_id, ecc.f_extcitcount_externalid
/


create or replace view v_citcount_only_wos as
select f_article_id, f_citcount_type, f_extcitcount_externalid, f_citcount_val, f_citcount_date
from v_citcount
where f_citcount_type='WOS';


-- unique articles for man
create or replace view v_citcount_man as
select f_man_id, f_extcitcount_externalid, f_citcount_type, max(cc.f_article_id) f_article_id, NVL(max(f_citcount_val),0) as f_citcount_val
from authora, v_citcount cc
where cc.f_article_id = authora.f_article_id
  and authora.f_man_id is not null
group by f_man_id, f_extcitcount_externalid, f_citcount_type
;


-- hirsh without duplicate articles
create or replace view v_citcount_hirsh as
select f_man_id, f_citcount_type as f_bibsource_id, count(*) hirsh_index
from (
  select f_man_id, f_citcount_type, f_article_id, f_citcount_val,
         row_number() over (partition by f_man_id, f_citcount_type order by f_citcount_val desc NULLS LAST) rn
  from v_citcount_man
)
where f_citcount_val>=rn
group by f_man_id, f_citcount_type
;


-- total cites
--create or replace view v_citcount_total as
select f_man_id, sum(cc.f_citcount_val) total_cites
from authora, v_citcount cc
where cc.f_article_id = authora.f_article_id
  and authora.f_man_id is not null
group by f_man_id
;

-- total cites without duplicates, grouped by citcount_type
create or replace view v_citcount_total_all
as
select f_man_id, f_citcount_type, sum(f_citcount_val) total_cites
from v_citcount_man
group by f_man_id, f_citcount_type
;

comment on table v_citcount_total_all
is 'Выбирает все ссылки на работы сотрудника с разбивкой по источнику цитирования.';

-- total cites in Web of Science
create or replace view v_citcount_total
as
select f_man_id, f_citcount_type, total_cites
from v_citcount_total_all
where f_citcount_type='WOS'
;



-- views for departent data extraction

-- последние значения импвакт факторов журналов
create or replace view v_journal_impacts as
select f_journal_id, f_journalrang_val, f_journalrang_type 
from (
select f_journal_id, f_journalrang_type,
       first_value(f_journalrang_val)
            over (partition by f_journal_id, f_journalrang_type
                  order by f_journalrang_date desc nulls last) f_journalrang_val
from journalrang
)
group by f_journal_id, f_journalrang_type, f_journalrang_val;


-- Анкета для данных по совету
create or replace view v_anketa_sovet
as
select
  man.f_man_id, nvl(scopus_wos.cnt, 0) as papers_in_scopus_wos, nvl(russian.cnt,0) papers_in_vak, nvl(citcount5.cnt,0) as ncitations_wos, nvl(hirsh.HIRSH_INDEX,0) as hirsh_index
from
  man,
--scopus
  (select aa.f_man_id, count(distinct a.f_article_id) cnt
   from
     article a, authora aa, externalid eid
   where a.f_article_id = aa.f_article_id
     and a.f_article_id = eid.f_activity_id (+)
     and a.f_article_year >= 2008 and a.f_article_year <= 2012
     and
     (  a.f_journal_id in (
             select f_journal_id
             from jornalvak
             where f_journalvaktype_id in (select f_journalvaktype_id from journalvaktype where f_journalvaktype_name='Scopus')
             UNION ALL
             select distinct f_journal_id from journalrang where f_journalrang_type like '%Impact Factor%20%'
             UNION ALL
             select distinct f_journal_id from journalrang where f_journalrang_type like '%SJR-%'
         )
        or
        eid.f_externalid_externalid is not null
     )
    group by aa.f_man_id
  ) scopus_wos,
-- russian
  (select aa.f_man_id, count(*) cnt
   from
     article a, authora aa, externalid eid 
   where a.f_article_id = aa.f_article_id
     and a.f_article_year >= 2008 and a.f_article_year <= 2012
     and a.f_article_id = eid.f_activity_id (+)     
     and a.f_journal_id in (
             select f_journal_id
             from jornalvak
             where f_journalvaktype_id in (select f_journalvaktype_id from journalvaktype where f_journalvaktype_name='Список ВАК')
         )
     and a.f_journal_id not in (
             select f_journal_id
             from jornalvak
             where f_journalvaktype_id in (select f_journalvaktype_id from journalvaktype where f_journalvaktype_name='Scopus')
             UNION ALL
             select distinct f_journal_id from journalrang where f_journalrang_type like '%Impact Factor%20%'
             UNION ALL
             select distinct f_journal_id from journalrang where f_journalrang_type like '%SJR-%'
         )
     and eid.f_externalid_externalid is null
    group by aa.f_man_id
  ) russian,
  ( 
    select aa.f_man_id, sum(cc.F_CITCOUNT_VAL) cnt
    from
      article a, authora aa, v_citcount cc
    where a.f_article_id = aa.f_article_id
      and a.f_article_year >= 2008 and a.f_article_year <= 2012
      and a.f_article_id = cc.f_article_id
      and cc.f_citcount_type='WOS'
    group by aa.f_man_id
  ) citcount5,
  v_citcount_hirsh hirsh
where 1 = 1
  -- and man.f_man_id = 1
  and man.f_man_id = scopus_wos.f_man_id (+)
  and man.f_man_id = russian.f_man_id (+)
  and man.f_man_id = hirsh.f_man_id (+)
  and man.f_man_id = citcount5.f_man_id (+)
  and man.f_man_id = hirsh.f_man_id (+)
/



create or replace view V_PERSONAL_REPORTS
as
select ml.f_markedlist_id, ml.f_markedlist_user,
       ml.f_markedlist_signtype,
       ml.f_markedlist_date,
       ml.f_department_id,
       wp.worker_id as f_man_id,
       ml.f_markedlist_value as personalreport_year
from markedlist ml, workers_profile wp
where ml.f_markedlist_user = wp.user_id
  and f_markedlist_type = 'Отчеты'
  and f_markedlist_signtype = 'SIGN'
/



-- select all departments managed by a representative user
create /*or replace*/ view V_REPRESENTATIVES
as
select r.f_representative_id, apd.f_department_id, r.f_representative_user
from representative r
     join all_parent_department apd on (r.f_department_id = parent_f_department_id)
/

comment on table V_REPRESENTATIVES
is 'Выбирает все подразделения, для которых пользователь является ответственным.'
/



create or replace view V_ARTICLE_REFERENCE
as
WITH
 reference_parts AS
 (
  SELECT a.f_article_id,
         aa.authors,
         rtrim(a.f_article_name) f_article_name,
         rtrim(ltrim(
           nvl(j.f_journal_name,
               CASE
                 WHEN s.f_seria_name IS NOT NULL THEN c.f_collection_name || ', ' || f_seria_name
                 ELSE c.f_collection_name
                END)
           || ' ' || case when a.f_article_vol is not null then '(' || a.f_article_vol || ')' end
           || a.f_article_num
         )) source,
         CASE 
           WHEN (a.f_article_lastpage IS NOT NULL)
             THEN a.f_article_firstpage || '-' || a.f_article_lastpage
           ELSE
              a.f_article_firstpage
         END pages,
         a.f_article_year
  FROM article a
       JOIN (
          SELECT f_article_id,
                 LISTAGG(CASE
                           WHEN f_authora_ord <= 10 OR f_authora_ord IS NULL THEN f_authora_name
                           WHEN f_authora_ord =  11 THEN 'et.al.'
                           ELSE NULL
                         END, ', ')
                    WITHIN GROUP (ORDER BY f_authora_ord, f_authora_id) as authors
          FROM   authora
          GROUP BY f_article_id
       ) aa ON (a.f_article_id = aa.f_article_id)
       LEFT JOIN journal j ON (a.f_journal_id = j.f_journal_id)
       LEFT JOIN collection c ON (a.f_collection_id = c.f_collection_id)
       LEFT JOIN seria s ON (c.f_seria_id = s.f_seria_id)
  )
SELECT r.f_article_id, r.authors, r.f_article_name, r.source, r.pages, r.f_article_year,
       r.authors || ', ' || r.f_article_name || '. '  || r.source || ', ' || r.pages || ', ' || r.f_article_year formatted_reference
FROM reference_parts r
/


CREATE OR REPLACE VIEW v_granted_roles_noreps
AS
WITH ALL_PARENT_PERMISSIONSTYPES AS (
  select PER_F_PERMISSIONSTYPES_ID parent_f_permissionstypes_id,
          CONNECT_BY_ROOT F_PERMISSIONSTYPES_ID  f_permissionstypes_id
    from PERMISSIONSTYPESLINK ptl
      connect by prior ptl.PER_F_PERMISSIONSTYPES_ID = ptl.F_PERMISSIONSTYPES_ID
  union all
  select F_PERMISSIONSTYPES_ID, F_PERMISSIONSTYPES_ID from PERMISSIONSTYPES
  )
SELECT a_p.ID AS AUTHORITY_PERMISSION_ID, a_p.DATE_REQUESTED, a_p.APPROVED, a_p.DATE_APPROVED,
       a_u.USERNAME, a_u.ID AS USER_ID,
       appt.PARENT_F_PERMISSIONSTYPES_ID, appt.F_PERMISSIONSTYPES_ID,
       pt.F_PERMISSIONSTYPES_NAME, pt.F_PERMISSIONSTYPES_RUSINFO, pt.F_PERMISSIONSTYPES_ISROLE,
       pwl.F_PERMISSIONWITHLABEL_BEGIN, pwl.F_PERMISSIONWITHLABEL_END,
       apd.PARENT_F_DEPARTMENT_ID, apd.F_DEPARTMENT_ID
FROM  AUTHORITY_PERMISSION a_p
     inner join AUTH_USER a_u on a_u.ID = a_p.USER_ID
     left outer join DJANGO_CONTENT_TYPE dct on a_p.CONTENT_TYPE_ID = dct.ID
     inner join PERMISSIONWITHLABEL pwl on a_p.ID = pwl.F_PERMISSIONWITHLABEL_AUTPERMI
     left outer join ALL_PARENT_PERMISSIONSTYPES appt on appt.PARENT_F_PERMISSIONSTYPES_ID = pwl.F_PERMISSIONSTYPES_ID
     inner join PERMISSIONSTYPES pt on appt.F_PERMISSIONSTYPES_ID = pt.F_PERMISSIONSTYPES_ID
     left outer join ALL_PARENT_DEPARTMENT apd on apd.PARENT_F_DEPARTMENT_ID = a_p.OBJECT_ID
WHERE dct.APP_LABEL='organizations' and dct.MODEL='department'
/


CREATE OR REPLACE VIEW v_all_granted_roles
AS
WITH ALL_PARENT_PERMISSIONSTYPES AS (
  select PER_F_PERMISSIONSTYPES_ID parent_f_permissionstypes_id,
          CONNECT_BY_ROOT F_PERMISSIONSTYPES_ID  f_permissionstypes_id
    from PERMISSIONSTYPESLINK ptl
      connect by prior ptl.PER_F_PERMISSIONSTYPES_ID = ptl.F_PERMISSIONSTYPES_ID
  union all
  select F_PERMISSIONSTYPES_ID, F_PERMISSIONSTYPES_ID from PERMISSIONSTYPES
  )
SELECT a_u.ID AS USER_ID,
       pt.F_PERMISSIONSTYPES_ID, pt.F_PERMISSIONSTYPES_NAME, pt.F_PERMISSIONSTYPES_ISROLE,
       a_p.APPROVED, pwl.F_PERMISSIONWITHLABEL_BEGIN, pwl.F_PERMISSIONWITHLABEL_END,
       apd.F_DEPARTMENT_ID
       , decode(a_p.OBJECT_ID, apd.f_department_id, 1, 0) AS exact_department_grant
FROM AUTHORITY_PERMISSION a_p
     inner join AUTH_USER a_u on a_u.ID = a_p.USER_ID
     left outer join DJANGO_CONTENT_TYPE dct on a_p.CONTENT_TYPE_ID = dct.ID
     inner join PERMISSIONWITHLABEL pwl on a_p.ID = pwl.F_PERMISSIONWITHLABEL_AUTPERMI
     left outer join ALL_PARENT_PERMISSIONSTYPES appt on appt.PARENT_F_PERMISSIONSTYPES_ID = pwl.F_PERMISSIONSTYPES_ID
     inner join PERMISSIONSTYPES pt on appt.F_PERMISSIONSTYPES_ID = pt.F_PERMISSIONSTYPES_ID
     left outer join ALL_PARENT_DEPARTMENT apd on apd.PARENT_F_DEPARTMENT_ID = a_p.OBJECT_ID
WHERE dct.APP_LABEL='organizations' and dct.MODEL='department'
UNION ALL /* Add all representatives to 'is_representative_for' role for all relevant departments */
SELECT rep.F_REPRESENTATIVE_USER,
       pt.F_PERMISSIONSTYPES_ID, pt.F_PERMISSIONSTYPES_NAME, pt.F_PERMISSIONSTYPES_ISROLE,
       rep.F_REPRESENTATIVE_ISACTIVE, rep.F_REPRESENTATIVE_BEGIN, rep.F_REPRESENTATIVE_END,
       dep.F_DEPARTMENT_ID
       , decode(rep.f_department_id, dep.f_department_id, 1, 0)
FROM representative rep,
     (SELECT pt.F_PERMISSIONSTYPES_ID, pt.F_PERMISSIONSTYPES_NAME, pt.F_PERMISSIONSTYPES_ISROLE
      FROM PERMISSIONSTYPES pt
           JOIN PERMCONTTYPE pct ON pct.F_PERMISSIONSTYPES_ID = pt.F_PERMISSIONSTYPES_ID
           JOIN DJANGO_CONTENT_TYPE dct ON pct.F_PERMCONTTYPE_DJCONTID = dct.ID
      WHERE dct.APP_LABEL='organizations' AND dct.MODEL='department'
        AND pt.F_PERMISSIONSTYPES_NAME='is_representative_for'
     ) pt,
     (SELECT d.f_organization_id, d.f_department_id, apd.parent_f_department_id
      FROM department d JOIN all_parent_department apd ON d.f_department_id = apd.f_department_id
     ) dep
WHERE (
   (rep.f_department_id is null and dep.f_organization_id = rep.f_organization_id)
   OR (dep.parent_f_department_id = rep.f_department_id)
  )
/

CREATE OR REPLACE VIEW v_granted_roles
AS
SELECT *
FROM v_all_granted_roles
WHERE nvl(approved, 0)=1 and nvl(F_PERMISSIONWITHLABEL_END, sysdate) >= sysdate
/



create or replace view v_my_users_via_role as
select wp.user_id, wp.worker_id as f_man_id, mp.f_department_id,
       gr.user_id as role_user_id, gr.F_PERMISSIONSTYPES_NAME, gr.F_PERMISSIONSTYPES_ID,
       sysdate - nvl(mp.f_manspost_end, sysdate) expiration_delay
from workers_profile wp
  join manspost mp on (wp.worker_id = mp.f_man_id)
  join (select distinct f_department_id, user_id, F_PERMISSIONSTYPES_NAME, F_PERMISSIONSTYPES_ID
        from v_all_granted_roles
        where nvl(approved, 0)=1 and nvl(F_PERMISSIONWITHLABEL_END, sysdate) <= sysdate
       ) gr on mp.f_department_id = gr.f_department_id
/


create or replace view v_step_estimate_wide
as
select p.f_project_id,
       ps.f_projstep_id,
       nvl(ps.f_projstep_begin, p.f_project_begin) step_begin,
       nvl(ps.f_projstep_end, p.f_project_end) step_end,
       nvl(est_vsego.f_estimate_value, 0) total,
       nvl(est_self.f_estimate_value, 0) self,
       nvl(est_trud.f_estimate_value, 0) salery,
       nvl(est_truddog.f_estimate_value, 0) salery_int,
       nvl(est_equip.f_estimate_value, 0) equip,
       nvl(est_other.f_estimate_value, 0) other
from project p
  left join projstep ps on (ps.f_project_id = p.f_project_id)
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=10) est_vsego
       on (est_vsego.f_project_id=p.f_project_id and (est_vsego.f_projstep_id=ps.f_projstep_id or ps.f_projstep_id is null))
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=20) est_self
       on (est_self.f_project_id=p.f_project_id and (est_self.f_projstep_id=ps.f_projstep_id or ps.f_projstep_id is null))
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=30) est_trud
       on (est_trud.f_project_id=p.f_project_id and (est_trud.f_projstep_id=ps.f_projstep_id or ps.f_projstep_id is null))
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=40) est_truddog
       on (est_truddog.f_project_id=p.f_project_id and (est_truddog.f_projstep_id=ps.f_projstep_id or ps.f_projstep_id is null))
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=50) est_equip
       on (est_equip.f_project_id=p.f_project_id and (est_equip.f_projstep_id=ps.f_projstep_id or ps.f_projstep_id is null))
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=60) est_other
       on (est_other.f_project_id=p.f_project_id and (est_other.f_projstep_id=ps.f_projstep_id or ps.f_projstep_id is null))
/


-- faster version of the above query, assuming there are no project-level estimates
select p.f_project_id,
       ps.f_projstep_id,
       nvl(ps.f_projstep_begin, p.f_project_begin) step_begin,
       nvl(ps.f_projstep_end, p.f_project_end) step_end,
       nvl(est_vsego.f_estimate_value, 0) total,
       nvl(est_self.f_estimate_value, 0) self,
       nvl(est_trud.f_estimate_value, 0) salery,
       nvl(est_truddog.f_estimate_value, 0) salery_int,
       nvl(est_equip.f_estimate_value, 0) equip,
       nvl(est_other.f_estimate_value, 0) other
from project p
  left join projstep ps on (ps.f_project_id = p.f_project_id)
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=10) est_vsego
       on (est_vsego.f_projstep_id=ps.f_projstep_id)
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=20) est_self
       on (est_self.f_projstep_id=ps.f_projstep_id)
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=30) est_trud
       on (est_trud.f_projstep_id=ps.f_projstep_id)
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=40) est_truddog
       on (est_truddog.f_projstep_id=ps.f_projstep_id)
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=50) est_equip
       on (est_equip.f_projstep_id=ps.f_projstep_id)
  left join (select * from estimate join estimatetype using (f_estimatetype_id) where f_estimatetype_ord=60) est_other
       on (est_other.f_projstep_id=ps.f_projstep_id)



create or replace view V_JOURNALS_INFO AS
select j.f_journal_id, 
       nvl(scopus_journals.value,0) as in_scopus,
       nvl(wos_journals.value,0) as in_wos,
       nvl(vak_journals.value,0) as in_vak,
       nvl(top25_journals.value,0) as in_top25
from journal j
left join
         (
           select f_journal_id, max(value) as value
           from
           (
             select f_journal_id, 1 as value
             from jornalvak
             where f_journalvaktype_id in
                      (select f_journalvaktype_id from journalvaktype
                       where f_journalvaktype_name='Scopus')
            UNION ALL
             select f_journal_id, 1 as value
             from journalrang
             where f_journalrang_type like '%SJR-%'
           )
           group by f_journal_id
         ) scopus_journals on (j.f_journal_id = scopus_journals.f_journal_id)
left join
         (
           select distinct f_journal_id, 1 as value from
            (
              select f_journal_id from journalrang
              where f_journalrang_type like '%Impact Factor%20%'
               UNION ALL
              select jv.f_journal_id
              from jornalvak jv
              where jv.f_journalvaktype_id=(select f_journalvaktype_id from journalvaktype
                                      where f_journalvaktype_name='Arts & Humanities Citation Index')
            )
         ) wos_journals  on (j.f_journal_id = wos_journals.f_journal_id)
left join
         (
           select f_journal_id, 1 as value from v_wos_top25_journals group by f_journal_id
         ) top25_journals on (j.f_journal_id = top25_journals.f_journal_id)
left join
         (
           select distinct f_journal_id, 1 as value
           from jornalvak
           where f_journalvaktype_id in (select f_journalvaktype_id from journalvaktype where f_journalvaktype_name='Список ВАК')
         ) vak_journals  on (j.f_journal_id = vak_journals.f_journal_id)
/



create or replace view V_COWORKER_STATUS
AS
select f_man_id, department_id as f_department_id,
       case
         when part_time = 0 then 'full_time'
         when part_time = 1 and org_full_worker = 1 then 'int_coworker'
         else 'ext_coworker'
       end AS status
from (
select amd.f_man_id, amd.department_id, min(d.f_organization_id) f_organization_id,
       min(amd.f_manspost_part) part_time, max(decode(main_workplace.f_man_id, null, 0, 1)) org_full_worker
          from all_man_department amd
               join department d on (amd.department_id = d.f_department_id)
               left join (select distinct f_man_id, f_organization_id
                          from manspost
                              join department on (manspost.f_department_id = department.f_department_id)
                          where (f_manspost_end is null or f_manspost_end > sysdate)
                            and nvl(f_manspost_part,0) = 0
                    ) main_workplace on (main_workplace.f_organization_id = d.f_organization_id
                                         and main_workplace.f_man_id = amd.f_man_id)
          where (e is null or e>sysdate)
          group by amd.f_man_id, amd.department_id
)
/


CREATE OR REPLACE VIEW v_department_fullname
AS
WITH connect_by_data AS 
    (
    SELECT
      department.f_department_id
      , SYS_CONNECT_BY_PATH(departmentlink.f_department_id, '/') PathID
      , SYS_CONNECT_BY_PATH(department.f_department_name, '/') Path
      , LEVEL AS path_length
    FROM (
         SELECT departmentlink.dep_f_department_id, departmentlink.f_department_id FROM departmentlink
         UNION ALL
         SELECT DISTINCT NULL, root_department.root_f_department_id FROM root_department
       ) departmentlink
    JOIN department ON (department.f_department_id = departmentlink.f_department_id)
    CONNECT BY departmentlink.dep_f_department_id = PRIOR departmentlink.f_department_id
    )
SELECT connect_by_data.f_department_id, path, PathID
FROM connect_by_data
     JOIN (SELECT f_department_id, MAX(path_length) max_path_length
           FROM connect_by_data
           GROUP BY f_department_id
      ) mpd ON (mpd.f_department_id = connect_by_data.f_department_id
                AND mpd.max_path_length = connect_by_data.path_length)
/
CREATE OR REPLACE VIEW v_granted_permissions
AS
WITH ALL_PARENT_PERMISSIONSTYPES AS (
  select PER_F_PERMISSIONSTYPES_ID parent_f_permissionstypes_id,
          CONNECT_BY_ROOT F_PERMISSIONSTYPES_ID  f_permissionstypes_id
    from PERMISSIONSTYPESLINK ptl
      connect by prior ptl.PER_F_PERMISSIONSTYPES_ID = ptl.F_PERMISSIONSTYPES_ID
  union all
  select F_PERMISSIONSTYPES_ID, F_PERMISSIONSTYPES_ID from PERMISSIONSTYPES
  )
SELECT a_p.ID AS AUTHORITY_PERMISSION_ID, a_p.OBJECT_ID, a_p.DATE_REQUESTED, a_p.APPROVED, a_p.DATE_APPROVED,
       a_u.USERNAME, a_u.ID AS USER_ID,
       appt.PARENT_F_PERMISSIONSTYPES_ID, appt.F_PERMISSIONSTYPES_ID,
       pt.F_PERMISSIONSTYPES_NAME, pt.F_PERMISSIONSTYPES_RUSINFO, pt.F_PERMISSIONSTYPES_ISROLE,
       pwl.F_PERMISSIONWITHLABEL_BEGIN, pwl.F_PERMISSIONWITHLABEL_END,
       apd.PARENT_F_DEPARTMENT_ID, apd.F_DEPARTMENT_ID
FROM  AUTHORITY_PERMISSION a_p
     inner join AUTH_USER a_u on a_u.ID = a_p.USER_ID
     left outer join DJANGO_CONTENT_TYPE dct on a_p.CONTENT_TYPE_ID = dct.ID
     inner join PERMISSIONWITHLABEL pwl on a_p.ID = pwl.F_PERMISSIONWITHLABEL_AUTPERMI
     left outer join ALL_PARENT_PERMISSIONSTYPES appt on appt.PARENT_F_PERMISSIONSTYPES_ID = pwl.F_PERMISSIONSTYPES_ID
     inner join PERMISSIONSTYPES pt on appt.F_PERMISSIONSTYPES_ID = pt.F_PERMISSIONSTYPES_ID
     left outer join ALL_PARENT_DEPARTMENT apd on apd.PARENT_F_DEPARTMENT_ID = a_p.OBJECT_ID
/
