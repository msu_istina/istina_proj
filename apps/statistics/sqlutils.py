# -*- coding: utf-8; -*-

from collections import namedtuple
from django.db import connection

from common.db_constants import DB_CONSTANTS

def get_top_journals(threshold=0.25):

    sql = """
    select distinct jrub.f_journalrub_name, j.f_journal_name jname, j.f_journal_id id
    from v_journal_standings js, journalrub jrub, journal j
    where j.f_journal_id = js.f_journal_id
    and jrub.f_journalrub_id  = js.f_journalrub_id
    and (js.f_journalrang_type like '5-Year Impact Factor 201_' or
         js.f_journalrang_type like 'Impact Factor 201_')
    and js.r<=%s
    and f_journalrubtype_id in (select f_journalrubtype_id from journalrubtype where f_journalrubtype_name='Web of Science')
    order by jrub.f_journalrub_name, j.f_journal_name
    """

    cursor = connection.cursor()
    cursor.execute(sql, [threshold])
    JournalTuple = namedtuple('JournalTuple', 'rubric, name, id')
    journals = [JournalTuple(*item) for item in cursor]
    return journals

def get_top_journals_rubrics_stats():

    sql = """
    select jrub.f_journalrub_name, count(*) cnt
    from v_journal_standings js, article a, journalrub jrub
    where a.f_journal_id = js.f_journal_id
    and jrub.f_journalrub_id  = js.f_journalrub_id
    and (js.f_journalrang_type like '5-Year Impact Factor 201_' or
         js.f_journalrang_type like 'Impact Factor 201_')
    and r<=%s
    and a.f_article_year=%s
    group by jrub.f_journalrub_name
    order by cnt desc
    """

    cursor = connection.cursor()
    cursor.execute(sql, [0.25, 2012])
    RubricTuple = namedtuple('RubricTuple', 'name, journals_count')
    rubrics = [RubricTuple(*item) for item in cursor]
    return rubrics

def get_top_workers(year=2012, showall=None):
    """
    Возвращает список наборов, каждый из которых содержит:
    полное имя сотрудника, его id, место работы (факультет, строкой)
    и суммарный коэффициент, рассчитанный по статьям, опубликованным в year
    Если showall=='all', то при расчете коэффициента учитываются все статьи.
    Если showall=='raw', то при расчете коэффициента учитываются все статьи,
    включая те, которые еще не были подтверждены.
    Иначе - только те статьи, которые не вошли в список премирования project=1.
    Сортировка результата по месту работы и полному имени.
    """

    sql = """
    select wp.lastname || ' ' || wp.firstname || ' ' || wp.middlename, wtotal,
           subq.f_man_id, nvl(man_work_places.work_places, '-') work_places
    from
    workers_profile wp,
    ( -- authors weight
      select aa.f_man_id, trunc(sum(ac.coeff),2) wtotal
      from authora aa, v_article_coefficient ac
      where ac.f_article_id = aa.f_article_id
        and ac.f_article_id in (
            select f_article_id from article a
            where a.f_article_year = %s
            and a.f_article_id in (select f_article_id from v_wos_top25_articles)"""
    if showall != 'raw':
        sql +="""
            and a.f_article_id in (select f_article_id from confirmed_articles)"""
    sql += """
            )"""
    # note different sematics of user_id and article_id in statistics_ tables below
    if showall != 'all':
        sql += """
        and (
          not exists (select ruser.worker_id
                             from STATISTICS_RANKEDUSER ruser, STATISTICS_RANKEDARTICLEAUF89F rauthor, STATISTICS_RANKEDARTICLE ra
                             where ruser.id = rauthor.user_id
                               and rauthor.article_id = ra.id
                               and ra.project_id = 1
                               and aa.f_man_id = ruser.worker_id
                               and aa.f_article_id = ra.article_id)
        )
        """
    sql += """
      group by aa.f_man_id
    ) subq,
    ( -- aggregated workplaces
      SELECT tld.f_man_id, LISTAGG(d.f_department_name, ', ') WITHIN GROUP (ORDER BY f_manspost_part) as work_places
       FROM ( -- a kind of top-level departments of workers
            select distinct mp.f_man_id, nvl(mp.f_manspost_part, 0) f_manspost_part, mp.f_department_id,
                nvl(dl2.dep_f_department_id,
                    nvl(dl1.dep_f_department_id, mp.f_department_id)) top_department_id
            from manspost mp, departmentlink dl1, departmentlink dl2
            where mp.f_department_id = dl1.f_department_id (+)
              and dl1.dep_f_department_id = dl2.f_department_id (+)
              and (mp.f_manspost_end is null or mp.f_manspost_end>sysdate)
           ) tld,
           department d
           where d.f_department_id = tld.top_department_id
          GROUP BY tld.f_man_id) man_work_places
    where subq.f_man_id = wp.worker_id
    and man_work_places.f_man_id (+) = wp.worker_id
    order by decode(man_work_places.work_places, NULL, 1, 0), 1 -- man_work_places.work_places, 2 desc
    """

    cursor = connection.cursor()
    cursor.execute(sql, [year])
    WorkerTuple = namedtuple('WorkerTuple', 'name, rating, id, department')
    workers = [WorkerTuple(*item) for item in cursor]
    return workers


def get_top_articles_ids(year, worker_id=None, department_ids=None, include_WOS=None):

    sql = """
    select aa.authors,
           a.f_article_name || ' // ' || ' ' || j.f_journal_name || ' ' || a.f_article_num || ' ' || a.f_article_vol || ' ' || a.f_article_doi art,
           a.f_article_id,
           extid.f_externalid_externalid wos_id
    from article a, journal j,
         (SELECT f_article_id, LISTAGG(f_authora_name, ', ') WITHIN GROUP (ORDER BY f_authora_ord) as authors
          FROM   authora
          GROUP BY f_article_id) aa,
         (select js.f_journal_id, min(r) r, max(f_journalrang_val) rangval
          from v_journal_standings js group by js.f_journal_id) bestjs,
         (select f_activity_id, f_externalid_externalid
          from externalid where f_bibsource_id='WOS') extid
    where a.f_journal_id = j.f_journal_id
    and j.f_journal_id = bestjs.f_journal_id (+)
    and aa.f_article_id = a.f_article_id
    and a.f_article_year=%s
    and a.f_article_id in (select f_article_id from v_wos_top25_articles)
    and a.f_article_id = extid.f_activity_id (+)
    """
    if worker_id:
        sql += """
        and exists (select 1 from authora aa2 where aa2.f_man_id = %s and aa2.f_article_id = a.f_article_id)
        """
    if department_ids:
        sql += """
        and a.f_article_id in (select f_article_id from v_department_articles where f_department_id in (%s))
        """ % ", ".join(map(str, department_ids))
    sql += """
    -- order by bestjs.r NULLS LAST, bestjs.rangval desc
    order by bestjs.rangval desc NULLS LAST, extid.f_externalid_externalid NULLS LAST
    """

    cursor = connection.cursor()
    cursor.execute(sql, [year, int(worker_id)] if worker_id else [year])
    if include_WOS:
        articles_ids = [(item[2],item[3]) for item in cursor]
    else:
        articles_ids = [item[2] for item in cursor]

    return articles_ids

def get_top_articles_by_rubrics(year=2012, rubtype='Web of Science'):

    sql = """
    select aa.authors,
           a.f_article_name || ' // ' || ' ' || j.f_journal_name || ' ' || a.f_article_num || ' ' || a.f_article_vol art,
           a.f_article_doi doi,
           a.f_article_id,
           jrub.f_journalrub_name
    from article a, journal j, journalrublink jrl, journalrub jrub, journalrubtype jrubt,
         (SELECT f_article_id, LISTAGG(f_authora_name, ', ') WITHIN GROUP (ORDER BY f_authora_ord) as authors
          FROM   authora
          GROUP BY f_article_id) aa,
         (select js.f_journal_id, min(r) r from v_journal_standings js group by js.f_journal_id) bestjs
    where a.f_journal_id = j.f_journal_id
    and j.f_journal_id = bestjs.f_journal_id (+)
    and aa.f_article_id = a.f_article_id
    and a.f_article_year=%s
    and a.f_article_id in (select f_article_id from v_wos_top25_articles)
    and a.f_article_id in (select f_article_id from confirmed_articles)
    and jrl.f_journal_id (+) = j.f_journal_id
    and jrub.f_journalrub_id = jrl.f_journalrub_id
    and jrub.f_journalrubtype_id = jrubt.f_journalrubtype_id and jrubt.f_journalrubtype_name = %s
    order by jrub.f_journalrub_name, bestjs.r NULLS LAST, art
    """

    cursor = connection.cursor()
    cursor.execute(sql, [year, rubtype])
    TopArticlesByRubricTuple = namedtuple('TopArticlesByRubric', 'authors, name, doi, article_id, rubname')
    data = [TopArticlesByRubricTuple(*item) for item in cursor]
    return data


def get_top_articles_ids_by_worker(worker_id, year):

    sql = """
    select a.f_article_id
    from v_wos_top25_articles ta, article a, authora aa
    where a.f_article_id=ta.f_article_id and a.f_article_year=%s and aa.f_article_id=a.f_article_id and aa.f_man_id=%s
    """
    cursor = connection.cursor()
    cursor.execute(sql, [year, int(worker_id)])
    articles_ids = [item[0] for item in cursor]
    return articles_ids


# Generate triplets (id, name, value) for specified grouping/metric conditions
def get_department_activity(year=2012, parent_ids=None, metric='count', pub_type='all', confirmed=0, rub_ids=None, query_type='department', filter_deps_by_rub=0, rub_type='scopus'):
    metric_field = {"count": "count(distinct(daa.f_article_id))",
                    "coef": "trunc(sum(acf.coeff),2)",
                    "nauthors": "count(distinct(daa.f_man_id))",
                    "weightedsum": "trunc(sum(acf.coeff * (1-NVL(js.r,1))),2)",
                    "citcount_ktu": "trunc(sum(nvl(cc.f_citcount_val,0)/acf.nauthors),2)",
                    "citcount": "a.f_article_id"
                    }
    pub_type_sqlfilter = {
        "all": "",
        #"top25j": " and a.f_article_id in (select f_article_id from v_wos_top25_articles) ",
        "top25j": " and a.f_journal_id in (select f_journal_id from v_wos_top25_journals) ",
        "allj": " and a.f_journal_id is not null ",
        "onlyp": """ and (a.f_journal_id is null
                     and a.f_collection_id in (select f_collection_id from collection col where nvl(f_collection_tesis,0)=1)) """,
        "wosj": " and a.f_journal_id in (select f_journal_id from jornalvak where f_journalvaktype_id = (select f_journalvaktype_id from journalvaktype where f_journalvaktype_name='JCR'))",
        "scopus": " and a.f_journal_id in (select f_journal_id from jornalvak where f_journalvaktype_id = (select f_journalvaktype_id from journalvaktype where f_journalvaktype_name='Scopus'))",
        "vak": " and a.f_journal_id in (select f_journal_id from jornalvak where f_journalvaktype_id = (select f_journalvaktype_id from journalvaktype where f_journalvaktype_name='Список ВАК'))",
        }

    select_fields = {
        "department": "d.f_department_id AS id, d.f_department_name AS name",
        "rub": " nvl(jrub.parent_f_journalrub_id,0) AS id, nvl(jrub.parent_f_journalrub_name,'*** Неизвестная ***') AS name"
        }
    group_fields = { # FIXME: should be the same as above, but with no AS clause; one list shuld be build from another automaically
        "department": "d.f_department_id, d.f_department_name",
        "rub": " jrub.parent_f_journalrub_id, jrub.parent_f_journalrub_name"
        }
    if rub_ids:
        select_fields["rub"] = " nvl(jrub.f_journalrub_id,0) as ID, nvl(jrub.f_journalrub_name, 'Unknown') as NAME"
        group_fields["rub"] = " nvl(jrub.f_journalrub_id,0), nvl(jrub.f_journalrub_name, 'Unknown')"
    select_tables = {
        "department":"",
        "rub": """ , journalrublink jrl,
           (
           select journalrub.*,
                  CONNECT_BY_ROOT f_journalrub_id AS parent_f_journalrub_id,
                  CONNECT_BY_ROOT jou_f_journalrub_id AS parent_jou_f_journalrub_id,
                  CONNECT_BY_ROOT f_journalrub_name AS parent_f_journalrub_name
              from journalrub
              connect by prior f_journalrub_id = jou_f_journalrub_id
              start with jou_f_journalrub_id is null
              ) jrub
           """
        }
    rubsel = {
        "scopus": " and (jrub.f_journalrubtype_id = (select f_journalrubtype_id from journalrubtype where f_journalrubtype_name='Scopus') or jrub.f_journalrubtype_id is null) ",
        "GRNTI": " and (jrub.f_journalrubtype_id = (select f_journalrubtype_id from journalrubtype where f_journalrubtype_name='GRNTI') or jrub.f_journalrubtype_id is null) "
        }
    joins = {
        "department": "",
        "rub": """ and a.f_journal_id = jrl.f_journal_id (+)
            and jrub.f_journalrub_id (+) = jrl.f_journalrub_id
            """
        }

    sql = """
    select """ + select_fields[query_type] + "," + metric_field[metric] + """ value
    from department d, v_department_article_authors daa,
         article a, v_article_coefficient acf,
         (select cc2.f_article_id, max(nvl(cc2.f_citcount_val,0)) AS f_citcount_val
            from v_citcount cc2 where cc2.f_citcount_type='WOS' and cc2.f_citcount_val is not null
            group by cc2.f_article_id) cc,
         (select f_journal_id, min(r) r from v_journal_standings group by f_journal_id) js""" + select_tables[query_type] + """
    where d.f_department_id = daa.f_department_id
      and a.f_article_id = daa.f_article_id
      and daa.f_article_id = acf.f_article_id
      and a.f_journal_id = js.f_journal_id (+)
      and a.f_article_id = cc.f_article_id (+)
    """ + joins[query_type]

    if year:
        # and (a.f_article_year = %s OR %s IS NULL)
        sql += """
        and (a.f_article_year = %s)
        """ % int(year)


    if confirmed != 0:
        sql += """
        and a.f_article_id in (select f_article_id from confirmed_articles)
        """

    sql += pub_type_sqlfilter[pub_type]

    if query_type=='rub':
        sql += rubsel[rub_type];
        if rub_ids:
            rubids_str = ','.join(map(str, rub_ids))
            sql += " and (jrub.parent_jou_f_journalrub_id in (" + rubids_str + ") "
            sql += "     or jrub.jou_f_journalrub_id in (" + rubids_str + ")) "
        else:
            sql += " and jrub.parent_jou_f_journalrub_id is null "

    if rub_ids and query_type=='department' and filter_deps_by_rub == 1:
        # filter out journals by rubric ids
        rubids_str = ','.join(map(str, rub_ids))
        sql += """ and a.f_journal_id in (
           select jrl.f_journal_id
              from (
                select journalrub.*,
                  CONNECT_BY_ROOT f_journalrub_id AS parent_f_journalrub_id,
                  CONNECT_BY_ROOT jou_f_journalrub_id AS parent_jou_f_journalrub_id,
                  CONNECT_BY_ROOT f_journalrub_name AS parent_f_journalrub_name
                from journalrub
                connect by prior f_journalrub_id = jou_f_journalrub_id
                start with jou_f_journalrub_id is null
                ) jrub,
                journalrublink jrl
            where jrl.f_journalrub_id = jrub.f_journalrub_id """ + rubsel[rub_type] + """
              and (jrub.parent_f_journalrub_id in (""" + rubids_str + ")" + """
                   or jrub.f_journalrub_id in (""" + rubids_str + """))
            ) """

    # FIXME: remove hardcoded MSU-filter
    sql += """
    and d.f_organization_id = 214524
    """

    if parent_ids:
        ids_str = ','.join(map(str, parent_ids))
        sql += """
        and (daa.f_department_id in (select f_department_id
                                   from departmentlink
                                   connect by prior f_department_id = dep_f_department_id
                                   start with dep_f_department_id in (""" + ids_str + """)
                                   )
             or daa.f_department_id in (""" + ids_str + ")" + """
            )
        and daa.exact_workplace = 1
        -- filter out faculty-level stuff
        and daa.f_department_id not in
           (select d2.f_department_id
            from department d2
            where not exists (select 1 from departmentlink dl where dl.f_department_id = d2.f_department_id))
        """
    else:
        # choose only top level departments (faculties)
        sql += """
        and daa.f_department_id in
           (select d2.f_department_id
            from department d2
            where not exists (select 1 from departmentlink dl where dl.f_department_id = d2.f_department_id))
        """

    sql += " group by " + group_fields[query_type]
    if metric == 'citcount':
        sql += ", " + metric_field[metric] # add another group field

    # citcount is a per-article query. Inner query selects distinct articles assigned
    # to department_id or rub_id. Outer query joins citcount values and compute total sum.
    if metric == 'citcount':
        sql = """select q.id, q.name, sum(nvl(cc.f_citcount_val,0)) value
        from v_citcount cc, (""" + sql + """) q
        where q.value = cc.f_article_id
        group by q.id, q.name
        """

    sql += """ order by value desc
    """

    #f = open("/tmp/dynamic-" + query_type + ".sql", "wa")
    #f.write(sql)
    #f.close()

    cursor = connection.cursor()
    cursor.execute(sql) #, [year, year])
    DepActivityTuple = namedtuple('DepActivityTuple', 'id, name, value')
    depactivity = [DepActivityTuple(*item) for item in cursor]
    return depactivity


def get_publications_stat(dep_id, year=None):

    sql = u"""
    select  pubtype, count(distinct id)
    from (
      select 'Патенты' as pubtype, extract(year from p.f_patent_date) as year, p.f_patent_id as id, mwp.f_department_id as depid
            from authorp ap, v_man_workplaces mwp, patent p
            where ap.f_man_id=mwp.f_man_id and p.f_patent_id = ap.f_patent_id
      UNION ALL
      select 'Доклады на конференциях' as pubtype, c.year as year, p.f_presentation_id as id, mwp.f_department_id as depid -- FIXME: joint talks
            from v_man_workplaces mwp, presentation p, t_confs c, workers_profile wp
            where p.f_presentation_user=wp.user_id and wp.worker_id=mwp.f_man_id and p.conf_id = c.conf_id
    )
    where depid = %s
      and year = NVL(%s, year)
    group by pubtype
    """

    art_sql = sql_article_stat(u"f_department_id", u"depid=%s and year=NVL(%s, year)")
    sql += u"""
    UNION ALL
    SELECT decode(pubtype,
                  'JALL', 'Статьи в журналах  (всего)',
                  'JVAK', 'Статьи в журналах из списка ВАК',
                  'BOOK', 'Монографии',
                  'TEXTBOOK', 'Учебные пособия',
                  'BOOK_REPRINT', 'Стереотипные издания',
                  'SCOPUS', 'Статьи в журналах из списков SCOPUS, WoS',
                  'TOP25', 'Статьи в журналах из Top-25',
                  'PROC', 'Статьи в сборниках',
                  'ABSTRACT', 'Тезисы',
                  pubtype) AS pubtype, cnt
    FROM (""" + art_sql + ") order by 1"

    cursor = connection.cursor()
    cursor.execute(sql, [int(dep_id), year, int(dep_id), year])
    PubStatsTuple = namedtuple('DepActivityTuple', 'typename, value')
    pubstats = [PubStatsTuple(*item) for item in cursor]
    return pubstats


def get_top_workers_for_dep(dep_id, year=None):

    # almost the same query as in top_workers. Diffs: removed top level dep printing and confirmed articles filter
    sql = """
    select wp.lastname || ' ' || wp.firstname || ' ' || wp.middlename, wtotal,
           subq.f_man_id, nvl(man_work_places.work_places, '-') work_places
    from
    workers_profile wp,
    ( -- authors weight
      select daa.f_man_id, trunc(sum(ac.coeff),2) wtotal
      from v_department_article_authors daa
        join article a on (a.f_article_id = daa.f_article_id)
        join v_article_coefficient ac on (ac.f_article_id = daa.f_article_id)
        join v_wos_top25_articles wos_topa on (wos_topa.f_article_id = daa.f_article_id)
      where (a.f_article_year = %s OR %s IS NULL)
        and daa.f_department_id = %s
      group by daa.f_man_id
    ) subq,
    ( -- aggregated workplaces
      SELECT mp.f_man_id, LISTAGG(d.f_department_name, ', ') WITHIN GROUP (ORDER BY f_manspost_part) as work_places
      from manspost mp, department d
      where %s in (select dep_f_department_id
                   from departmentlink
                   start with f_department_id = mp.f_department_id
                   connect by prior dep_f_department_id = f_department_id
                   UNION ALL
                   select mp.f_department_id from dual
                  )
      and d.f_department_id = mp.f_department_id
      group by mp.f_man_id
    ) man_work_places
    where subq.f_man_id = wp.worker_id
    and man_work_places.f_man_id (+) = wp.worker_id
    and wp.worker_id in (select mwp.f_man_id from v_man_workplaces mwp where mwp.f_department_id = %s)
    order by decode(man_work_places.work_places, NULL, 1, 0), 1 -- man_work_places.work_places, 2 desc
    """

    cursor = connection.cursor()
    cursor.execute(sql, [year, year, dep_id, dep_id, dep_id])
    WorkerTuple = namedtuple('WorkerTuple', 'name, rating, id, department')
    workers = [WorkerTuple(*item) for item in cursor]
    return workers


def get_popular_journals_for_dep(dep_id, year=None, limit=None):
    """ Список журналов в которых публикуются сотрудники подразделения """
    sql = """
    select j.f_journal_id, j.f_journal_name, popular_journals.narticles, popular_journals.nauthors
    from
     ( select * from
      (select a.f_journal_id, count(distinct daa.f_article_id) narticles, count(distinct daa.f_man_id) nauthors
       from v_department_article_authors daa, article a
       where a.f_article_id = daa.f_article_id
       and daa.f_department_id=%s
       and a.f_article_year = NVL(%s, a.f_article_year)
       group by a.f_journal_id
       order by narticles desc
      )
      where ROWNUM<=NVL(%s, 99999999)
     ) popular_journals
      , journal j
      where popular_journals.f_journal_id = j.f_journal_id
        and narticles>1
      order by narticles desc
    """

    cursor = connection.cursor()
    cursor.execute(sql, [dep_id, year, limit])
    PopularJournalsTuple = namedtuple('PopularJournalsTuple', 'id, name, narticles, nauthors')
    popjournals = [PopularJournalsTuple(*item) for item in cursor]
    return popjournals



def sql_workers_hirsh(extra_condition = '1=1'):
    return """
    select p.lastname || ' ' || p.firstname || ' ' || p.middlename AS full_name,
           wp.f_man_id AS f_man_id,
           nvl(h.hirsh_index,0) AS wos_hirsh,
           nvl(t.total_cites,0) AS wos_total_cites,
           nvl(cc5.total_latest, 0) AS wos_5y_cites,
           nvl(nta.n_top_articles, 0) AS n_top_articles,
           nvl(cc5.n_latest_wos, 0) AS n_latest_wos,
           t.f_citcount_type,
           found_articles.n_articles
    from (select distinct f_man_id, f_department_id from v_man_workplaces) wp
         left join v_citcount_hirsh h on (wp.f_man_id = h.f_man_id)
         left join (select f_man_id, f_citcount_type, total_cites
               from v_citcount_total_all
               where f_citcount_type='WOS'
               ) t on (t.f_man_id = wp.f_man_id)
         left join (select f_man_id, f_citcount_type, count(*) n_articles
            from  v_citcount_man
            group by f_man_id, f_citcount_type
         ) found_articles on (wp.f_man_id = found_articles.f_man_id)
         left join (
          select f_man_id, cc.f_citcount_type, sum(f_citcount_val) total_latest, count(distinct a.f_article_id) n_latest_wos
          from v_citcount cc, authora aa, article a
          where cc.f_article_id = a.f_article_id
            and a.f_article_year >= to_number(to_char(sysdate,'yyyy'))-5
            and aa.f_article_id = cc.f_article_id
            and cc.f_citcount_type='WOS'
          group by f_man_id, cc.f_citcount_type
         ) cc5 on (wp.f_man_id = cc5.f_man_id)
         left join (select f_man_id, count(*) n_top_articles
          from authora aa, v_wos_top25_articles ta
          where aa.f_article_id = ta.f_article_id
          group by f_man_id
         ) nta on (wp.f_man_id = nta.f_man_id)
         left join workers_profile p on (wp.f_man_id = p.worker_id)
    where
      t.f_citcount_type='WOS'
      and (t.f_citcount_type = cc5.f_citcount_type or cc5.f_citcount_type is null)
      and (h.f_bibsource_id = t.f_citcount_type or h.f_bibsource_id is null or t.f_citcount_type is null)
      and (found_articles.f_citcount_type = t.f_citcount_type or t.f_citcount_type is null)
      and (""" + extra_condition + ")"

#
def get_workers_hirsh_for_dep(dep_id):
    sql = sql_workers_hirsh("wp.f_department_id=%s")

    cursor = connection.cursor()
    cursor.execute(sql, [dep_id])
    WorkerHirshTuple = namedtuple('WorkerHirshTuple', 'name, id, hirsh, total, total5, ntop, nlatest_wos, bibsource, narticles')
    workers = [WorkerHirshTuple(*item) for item in cursor]
    return workers

def get_department_citcounts_by_year(dep_id):
    sql = u"""
    select da.f_article_year pub_year, sum(f_citcount_val) ncitations
    from v_citcount cc, v_department_articles da
    where cc.f_article_id = da.f_article_id
    and da.f_department_id = %s
    group by da.f_article_year
    order by da.f_article_year
    """

    cursor = connection.cursor()
    cursor.execute(sql, [int(dep_id)])
    return ([u'Год публикации статьи', u'Число ссылок'], list(cursor))


def sql_article_stat(idfield, cond):
    # idfield in (f_department_id, f_man_id)
    #
    # FIXME: hardcoded id of book category
    #
    sql = u"""
    select  pubtype, max(cnt) cnt
    from
    ((
    select  pubtype, count(distinct id) cnt
    from (
      select 'JALL' as pubtype, a.f_article_year as year, a.f_article_id as id, mwp.""" + idfield + u""" as depid
            from authora aa, v_man_workplaces mwp, article a
            where aa.f_man_id=mwp.f_man_id and a.f_article_id = aa.f_article_id and f_journal_id is not null
      UNION ALL
      select 'JVAK' as pubtype, a.f_article_year as year, a.f_article_id as id, mwp.""" + idfield + u""" as depid
            from authora aa, v_man_workplaces mwp, article a, jornalvak jvak
            where aa.f_man_id=mwp.f_man_id and a.f_article_id = aa.f_article_id
              and (a.f_journal_id = jvak.f_journal_id
                   and jvak.f_journalvaktype_id in
                     (select f_journalvaktype_id from journalvaktype where f_journalvaktype_name='Список ВАК'))
      UNION ALL
      select 'SCOPUS' as pubtype, a.f_article_year as year, a.f_article_id as id, mwp.""" + idfield + u""" as depid
            from authora aa
                 join v_man_workplaces mwp on (aa.f_man_id=mwp.f_man_id)
                 join article a on (a.f_article_id = aa.f_article_id)
                 left join jornalvak jvak on (a.f_journal_id = jvak.f_journal_id)
                 left join externalid on (externalid.f_activity_id = a.f_article_id)
            where
               (jvak.f_journalvaktype_id in
                  (select f_journalvaktype_id
                  from journalvaktype
                  where f_journalvaktype_name in ('Scopus','JCR','Social Sciences Citation Index','Arts & Humanities Citation Index'))
               )
               OR
               (a.f_journal_id is not null and externalid.f_activity_id is not null and externalid.f_bibsource_id in ('WOS', 'Scopus'))
      UNION ALL
      select 'TOP25' as pubtype, a.f_article_year as year, a.f_article_id as id, mwp.""" + idfield + u""" as depid
            from authora aa, v_man_workplaces mwp, article a, (select distinct f_journal_id from v_wos_top25_journals) top_journals
            where aa.f_man_id=mwp.f_man_id and a.f_article_id = aa.f_article_id
              and a.f_journal_id = top_journals.f_journal_id
      UNION ALL
      select decode( nvl(col.f_collection_tesis,0), 0, 'PROC', 1, 'ABSTRACT') as pubtype,
             a.f_article_year as year, a.f_article_id as id,  mwp.""" + idfield + u""" as depid
            from authora aa, v_man_workplaces mwp, article a, collection col
            where aa.f_man_id=mwp.f_man_id and a.f_article_id = aa.f_article_id and f_journal_id is null
            and a.f_collection_id = col.f_collection_id
            -- and nvl(f_collection_tesis,0) = 0
      UNION ALL
      select 'BOOK' as pubtype, b.f_book_year as year, b.f_book_id as id, mwp.""" + idfield + u""" as depid
            from authorb ab
                 join book b on (b.f_book_id = ab.f_book_id)
                 join v_man_workplaces mwp on (mwp.f_man_id = ab.f_man_id)
                 left join bookgroup bg on (bg.f_book_id = b.f_book_id)
            where bg.f_bookgrouptype_id is NULL or bg.f_bookgrouptype_id in (""" + DB_CONSTANTS["bookgrouptype"]["all_monographs_str"] + """)
      UNION ALL
      select 'TEXTBOOK' as pubtype, b.f_book_year as year, b.f_book_id as id, mwp.""" + idfield + u""" as depid
            from authorb ab, v_man_workplaces mwp, book b,
                 (select f_book_id from bookgroup where f_bookgrouptype_id in (""" + DB_CONSTANTS["bookgrouptype"]["all_textbooks_str"] + """)
                  minus
                  select f_book_id from bookgroup where f_bookgrouptype_id in (""" + DB_CONSTANTS["bookgrouptype"]["all_reprints_str"] + """)
                 ) bg
            where ab.f_man_id=mwp.f_man_id
              and b.f_book_id = ab.f_book_id and bg.f_book_id = b.f_book_id
      UNION ALL
      select 'BOOK_REPRINT' as pubtype, b.f_book_year as year, b.f_book_id as id, mwp.""" + idfield + u""" as depid
            from authorb ab, v_man_workplaces mwp, book b,
                 (
                  select f_book_id from bookgroup
                  where f_bookgrouptype_id IN (""" + DB_CONSTANTS["bookgrouptype"]["all_reprints_str"] + u""")
                 ) bg
            where ab.f_man_id=mwp.f_man_id
              and b.f_book_id = ab.f_book_id and bg.f_book_id = b.f_book_id
      ) """
    if cond:
        sql += u" where (" + cond + u") "
    sql += u"""
    group by pubtype
    )
    UNION ALL select 'JALL' as pubtype, 0 from dual
    UNION ALL select 'JVAK' as pubtype, 0 from dual
    UNION ALL select 'SCOPUS' as pubtype, 0 from dual
    UNION ALL select 'TOP25' as pubtype, 0 from dual
    UNION ALL select 'PROC' as pubtype, 0 from dual
    UNION ALL select 'ABSTRACT' as pubtype, 0 from dual
    UNION ALL select 'BOOK' as pubtype, 0 from dual
    UNION ALL select 'TEXTBOOK' as pubtype, 0 from dual
    UNION ALL select 'BOOK_REPRINT' as pubtype, 0 from dual
    )
    group by pubtype
    """
    return sql



def get_worker_citcount(worker_id, depth=5, offset=0):
    """
    Depth and offset controls years interval. Depth defines number of
    years included into report, offset - difference between current
    year and last year in the interval.

    Value depath=1 and offset=0 selects only current year.
    """
    sql_wh = sql_workers_hirsh("(wp.f_man_id=%s and t.f_citcount_type='WOS')")
    sql = "select full_name, f_man_id, wos_hirsh, wos_total_cites, wos_5y_cites, n_top_articles, n_latest_wos from (" + sql_wh + ") where rownum = 1"

    cursor = connection.cursor()
    cursor.execute(sql, [worker_id])
    WorkerHirshTuple = namedtuple('WorkerHirshTuple', 'name, id, hirsh, total, total5, ntop, nlatest_wos')
    worker_hirsh = [WorkerHirshTuple(*item) for item in cursor]
    if worker_hirsh:
        worker_hirsh = worker_hirsh[0]
#    if cursor.rowcount>0:
#        worker_hirsh = [WorkerHirshTuple(*item) for item in cursor][0]

    # article publications
    cursor = connection.cursor()
    stat_sql = sql_article_stat(u"f_man_id",
                                u"depid=%s " + u"and (year > to_number(to_char(sysdate,'yyyy'))-%(depth)s-%(offset)s and year <= to_number(to_char(sysdate,'yyyy'))-%(offset)s  )" % {'depth': depth, 'offset': offset})
    cursor.execute(stat_sql, [worker_id])
    PubStatsTuple = namedtuple('DepActivityTuple', 'typename, value')
    worker_pubstats_5y = {}
    for key, val in cursor:
        worker_pubstats_5y[key]=val

    cursor = connection.cursor()
    stat_sql = sql_article_stat(u"f_man_id", u"depid=%s")
    cursor.execute(stat_sql, [worker_id])
    worker_pubstats = {}
    for key, val in cursor:
        worker_pubstats[key]=val

    return {"worker_hirsh": worker_hirsh,
            "worker_pubstats": worker_pubstats,
            "worker_pubstats_5y": worker_pubstats_5y}


def get_ext_articles_ids(worker_id, system_short_name=None):

    sql = """
    select f_article_id
    from v_citcount_man
    where f_man_id=%s
    """
    query_params = [int(worker_id)]
    if system_short_name:
        sql += """
        and f_citcount_type = %s
        """
        query_params.append(system_short_name)
    cursor = connection.cursor()
    cursor.execute(sql, query_params)
    articles_ids = [item[0] for item in cursor]
    return articles_ids


def get_cited_articles_counts(worker_id):
    """Возврящает для заданного сотрудника список записей (краткое название источника,
    полное название источника, число статей, общее число ссылок), отсортированный в порядке
    убывания числа статей."""

    sql = u"""
    select f_citcount_type, f_bibsource_descr, cnt, total
    from
    (
    select f_citcount_type, f_bibsource_descr, count(distinct f_article_id) as cnt, sum(F_CITCOUNT_VAL) total
    from v_citcount_man, bibsource
    where f_man_id = %s
      and f_citcount_type = f_bibsource_id
    group by f_citcount_type, f_bibsource_descr
    UNION ALL
    select 'all', 'Во всех системах', count(distinct f_article_id), sum(F_CITCOUNT_VAL)
    from v_citcount_man
    where f_man_id = %s
    )
    order by cnt desc
    """
    cursor = connection.cursor()
    cursor.execute(sql, [int(worker_id), int(worker_id)])

    CACT = namedtuple('CitedArticlesCountTuple', 'system_short_name, system_full_name, number_of_papers, total_citations')
    result = [CACT(*item) for item in cursor]
    return result
