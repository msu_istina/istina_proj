# -*- coding: utf-8; -*-

from collections import namedtuple
from django.db import connections
from django.template.loader import render_to_string
from publications.utils import bibtex2html
import re
from publications.utils import split_authors
from workers.utils import get_fullname_raw
from statistics.models import WorkTuple
from organizations.models import Department


BOOKTYPE_TEXTBOOK = 217739
BOOKTYPE_TEXTBOOK_NOGRIF = 6054932

def get_department_data(dep_id, year=None, type=''):
    if type == 'conf':
        return ([u'Докладов', u'Соавторов', u'Сотрудников', u'Страна', u'Название'], get_department_confs(dep_id, year))
    elif type == 'conflist':
        return ([u'Сотрудник', u'Степень', u'Страна', u'Название', u'Год'], get_department_confs_list(dep_id, year))
    elif type == 'confcomm':
        return ([u'Сотрудников', u'Страна', u'Название'], get_department_committee(dep_id, year))
    elif type == 'pubs':
        return ([u'Тип', u'Авторы', u'Название', u'Источник', u'Год'], get_department_pubs(dep_id, year))
    elif type == 'jboards':
        return ([u'Журнал', u'Сотр. в редколлегии'], get_department_jboards(dep_id, year))
    elif type == 'dissovets':
        return ([u'Название', u'Сотрудников'], get_department_dissovets(dep_id, year))

def get_department_confs(dep_id, year=None):
    sql = """
    select count(distinct p.f_presentation_id) talks,
           count(distinct f_authord_id) coauthors, count(distinct amd.f_man_id) persons, cc.country_name, c.name
    from t_confs c, authord ad, all_man_department amd, presentation p,
         (select c.conf_id, cc.id, NVL(NVL(country.f_country_rusname, cc.country_name_ru), u'Россия') AS country_name
          from t_confs c
          LEFT JOIN (
          select conf_id, country_.id as id, country_name_ru
          from t_confs cf, country_
          where nls_upper(conf_place) like '%%' || nls_upper(country_name_en) || '%%'
          or nls_upper(conf_place) like '%%' || nls_upper(country_name_ru) || '%%'
          ) cc ON (c.conf_id = cc.conf_id)
          LEFT JOIN country ON (country.f_country_id = c.f_country_id)
         ) cc
    where c.conf_id = p.conf_id
      and ad.f_presentation_id = p.f_presentation_id
      and ad.f_man_id = amd.f_man_id
      and amd.department_id = %s
      and c.conf_id = cc.conf_id
    """
    if year:
        sql += """
        and c.year = %s
        """ % int(year)
    sql += """
    group by c.name, cc.country_name
    order by 1 desc
    """

    cursor = connections['reports_server'].cursor()
    cursor.execute(sql, [int(dep_id)])
    return list(cursor)


def get_department_confs_list(dep_id, year=None):
    '''Список всех сотрудников DEP_ID, которые участвовали в конференциях.
    '''
    sql = u"""
    select prof.lastname || ' ' || prof.firstname || ' ' || prof.middlename,
           nvl(deg.f_degree_name, 'Без степени'),
           cc.country_name,
           c.name,
           c.year
    from authord ad, t_confs c, all_man_department amd, presentation p, workers_profile prof,
         (select c.conf_id, cc.id, NVL(cc.country_name_ru, u'Россия') AS country_name
          from t_confs c,
          (
          select conf_id, country_.id as id, country_name_ru
          from t_confs cf, country_
          where nls_upper(conf_place) like '%%' || nls_upper(country_name_en) || '%%'
          or nls_upper(conf_place) like '%%' || nls_upper(country_name_ru) || '%%'
          ) cc
          where
          c.conf_id = cc.conf_id (+)
         ) cc,
         (SELECT md.f_man_id, d.f_degree_name f_degree_name
          FROM (select f_man_id, actual_degree_id
                from
                (select f_man_id, first_value(f_degree_id) over (partition by f_man_id order by f_mandegree_year desc nulls last) actual_degree_id
                from mandegree)
                group by f_man_id, actual_degree_id
               ) md,
               degree d
          WHERE md.actual_degree_id = d.f_degree_id
         ) deg
    where c.conf_id = p.conf_id
      and ad.f_presentation_id = p.f_presentation_id
      and ad.f_man_id = amd.f_man_id
      and amd.department_id = %s
      and c.conf_id = cc.conf_id
      and prof.worker_id = ad.f_man_id
      and ad.f_man_id = deg.f_man_id (+)
    """
    if year:
        sql += """
        and c.year = %s
        """ % int(year)
    sql += """
    order by 1 desc
    """

    cursor = connections['reports_server'].cursor()
    cursor.execute(sql, [int(dep_id)])
    return list(cursor)



def get_department_committee(dep_id, year=None):
    sql = """
    select count(distinct comm.f_man_id) authors, cc.country_name, c.name
    from t_confs c, all_man_department amd, committee comm,
         (select c.conf_id, cc.id, NVL(cc.country_name_ru, u'Россия') AS country_name -- u'Россия') country_name
          from t_confs c,
          (
          select conf_id, country_.id as id, country_name_ru
          from t_confs cf, country_
          where nls_upper(conf_place) like '%%' || nls_upper(country_name_en) || '%%'
          or nls_upper(conf_place) like '%%' || nls_upper(country_name_ru) || '%%'
          ) cc
          where
          c.conf_id = cc.conf_id (+)
         ) cc
    where c.conf_id = comm.conf_id
      and comm.f_man_id = amd.f_man_id
      and amd.department_id = %s
      and c.conf_id = cc.conf_id
    """
    if year:
        sql += """
        and c.year = %s
        """ % int(year)
    sql += """
    group by c.name, cc.country_name
    order by 1 desc
    """
    cursor = connections['reports_server'].cursor()
    cursor.execute(sql, [int(dep_id)])
    return list(cursor)


def get_department_pubs(dep_id, year=None):
    sql = u"""
    select case
             when a.f_collection_id IS NOT NULL and col.f_collection_tesis=1 then 'Тезисы'
             when a.f_journal_id IS NULL and a.f_collection_id IS NOT NULL then 'Статьи в сборниках'
             else 'Статьи в журналах'
           end AS type,
           aa.authors AS authors,
           a.f_article_name AS name,
           case
             when a.f_journal_id IS NOT NULL then j.f_journal_name || ' ' || a.f_article_num || ' ' || a.f_article_vol
             when a.f_collection_id IS NOT NULL then col.f_collection_name
           end || ' ' || a.f_article_doi AS source,
           a.f_article_year AS year
    from article a, journal j, collection col,
         (SELECT f_article_id, LISTAGG(f_authora_name, ', ') WITHIN GROUP (ORDER BY f_authora_ord) as authors
          FROM   authora
          GROUP BY f_article_id
         ) aa
    where a.f_journal_id = j.f_journal_id (+)
    and a.f_collection_id = col.f_collection_id (+)
    and aa.f_article_id = a.f_article_id
    and a.f_article_id in (select f_article_id from authora x, all_man_department amd where amd.f_man_id = x.f_man_id and amd.department_id = %s)
    """
    sql += u"""
    UNION ALL
    """
    sql += u"""
    select 'Книги' AS type,
           ba.authors AS authors,
           b.f_book_name AS name,
           p.f_publishing_name || ' ' || b.f_book_place AS source,
           b.f_book_year AS year
    from book b, publishing p,
         (SELECT f_book_id, LISTAGG(f_authorb_name, ', ') WITHIN GROUP (ORDER BY f_authorb_ord) as authors
          FROM   authorb
          GROUP BY f_book_id
         ) ba
    where ba.f_book_id = b.f_book_id
    and b.f_publishing_id = p.f_publishing_id (+)
    and b.f_book_id in (select f_book_id from authorb x, all_man_department amd where amd.f_man_id = x.f_man_id and amd.department_id = %s)
    """

    # Patents
    sql += u"UNION ALL "
    sql += u"""
    select 'Патенты' AS type,
           pa.authors AS authors,
           p.f_patent_name AS name,
           ' ' AS source,
           to_char(p.f_patent_date, 'yyyy')+0 AS year
    from patent p,
         (SELECT f_patent_id, LISTAGG(f_authorp_name, ', ') WITHIN GROUP (ORDER BY f_authorp_ord) as authors
          FROM   authorp
          GROUP BY f_patent_id
         ) pa
    where pa.f_patent_id = p.f_patent_id
    and p.f_patent_id in (select f_patent_id from authorp x, all_man_department amd where amd.f_man_id = x.f_man_id and amd.department_id = %s)
    """

    if year:
        sql = """
        SELECT type, authors, name, source, year
        FROM (""" + sql + """)
        WHERE year = %s
        """ % int(year)

    cursor = connections['reports_server'].cursor()
    cursor.execute(sql, [int(dep_id), int(dep_id), int(dep_id)])
    return list(cursor)

import logging
logger = logging.getLogger("sentry_debug")
def get_department_pubs_generic(addon_fields={},
                                conditions={},
                                sqlparam_list=[],
                                order_by=[u'year', u'name']):
    sql = ""
    fields = ("category", "pk", "title", "user_doi", "year", "doi", "authors",
        "source", "number", "volume", "firstpage", "lastpage",
        "impact_wos", "impact_scopus", "impact_rinc",
        "in_scopus", "in_wos", "in_top25", "in_vak",
        "citcount_wos", "citcount_scopus", "wos_id", "scopus_id")



    cursor = connections['reports_server'].cursor()
    if conditions.get("use_material_represent"):
        if conditions.get("books"):
          sql = book_material_representation_sql(conditions)#.get("title_filter"))""
          try:
            cursor.execute(sql,   sqlparam_list)
          except Exception:
            logger.warning("Exception in books search. Tried to find %s" % sqlparam_list)
        else:
          sql = article_material_representation_sql(conditions)#.get("title_filter"))
          try:
            cursor.execute(sql,   sqlparam_list)
          except Exception:
            logger.warning("Exception in articles search. Tried to find %s" % sqlparam_list)
    elif conditions.get("books"):
        sql = get_department_generate_nonarticles_pubs_query(addon_fields, conditions)
        fields = ("category", "pk", "title", "user_doi", "year", "doi",
                  "source", "npages", "authors", "isbn", "redpages", "ncopies")
        cursor.execute(sql)
    elif conditions.get("confs"):
        sql = get_department_generate_confs_query(addon_fields, conditions)
        fields = ("category", "pk", "title", "authors", "conf_name", "place", "country", "year")
        cursor.execute(sql)
    else:
        sql = get_department_generate_pubs_query(addon_fields, conditions)
        cursor.execute(sql)



    try:
      for item in cursor:
        yield WorkTuple(**dict(zip(fields, item)))
    except:
        yield WorkTuple()


def article_material_representation_sql(conditions = {}):
  if len(conditions.get("title_filter")) >= 3:
    sql = u"SELECT main_query.* FROM ( SELECT * FROM ARTICLE_SEARCH   WHERE  catsearch(name , %s, '') > 0) main_query    WHERE  ROWNUM <= 50000 " #% conditions.get("title_filter")
  else:
    sql = u"SELECT main_query.* FROM ( SELECT * FROM ARTICLE_SEARCH ) main_query    WHERE  ROWNUM <= 50000  " 

  if conditions.get("FREE_NEW"):
    sql += "AND " +  conditions.get("FREE_NEW")
  if conditions.get("department"):
    department_id =  conditions.get("department")
    # запрос, который выбирает статьи нужных подразделений
    dep_subquery = u""" select f_article_id
                        from v_department_articles da
                       where da.f_department_id"""  # + условие = или in
    dep_subquery += u" in (%s)"
    # здесь необходимо создать больше %s, по одному на каждый номер подразделения http://stackoverflow.com/questions/2253494/passing-param-to-db-execute-for-where-in-int-list
    dep_subquery = dep_subquery % ','.join('%s' for i in department_id.split(","))

    sql += u"""
    and main_query.pk in (""" + dep_subquery + u")"
  #sql += u" ORDER BY year, name"  
  #print sql
  return sql


def book_material_representation_sql(conditions = {}):
  if len(conditions.get("title_filter")) >= 3:
    sql = u"SELECT main_query.* FROM ( SELECT * FROM BOOK_SEARCH   WHERE  catsearch(name ,%s, '') > 0) main_query    WHERE  ROWNUM <= 50000 "# % conditions.get("title_filter")
  else:
    sql = u"SELECT main_query.* FROM ( SELECT * FROM BOOK_SEARCH  ) main_query    WHERE  ROWNUM <= 50000 "# % conditions.get("title_filter")

  if conditions.get("FREE_NEW"):
    sql += "AND " +  conditions.get("FREE_NEW")
  #sql += u" ORDER BY year, name"  
  #print sql
  return sql



def get_department_generate_pubs_query(addon_fields={},
                                       conditions={},
                                       order_by=[u'year', u'name']):
    """Возвращает текст sql запроса, который выбирает список всех
    публикаций с полями category, pk, name, user_doi, year, doi.

    ADDON_FIELDS определяет список дополнительных полей, которые
    необходимо включить в выдачу. Возможные значения ADDON_FIELDS:
      authors - список авторов (authora) строкой;
      source - название журнала или сборника source, num, vol,
               firstpage, lastpage;
      journal_wos - добавляет три поля: in_scopus, in_wos, in_vak, in_top25 со
                    значениями 0 или 1;
      citcount - добавляет поля: citcount_wos и citcount_scopus, scopus_id,
               wos_id, external_doi
      impacts - добавляет поля impact_wos, impact_scopus, impact_rinc
      ALL - добавляет все.

    CONDITIONS - словарь, который отображает тип условия (строка) в его текст.
    Возможные ключи и их значений:
      department: значением является либо целое число (f_department_id),
                  либо sql запрос, выбирающий одно поле, например,
         select f_department_id from department where f_department_name like '%%факультет%%'
      topic: целое число - первичный ключ записи в journalrub
      man: аналогично, но по id автора

      FREE: значением является строка, которая может содержать произвольное логическое
      выражение над названиями выбираемых по запросу полей. Например:
        '((year=2011 or year=2013) and in_scopus = 1) and citcount_wos>0)'
      выбирет только статьи из скопус за 2011 или 2013 год и на которые есть ссылки из WoS.
      ROWNUM: значение - максимальное число записей

      Условия, которые задаются различными ключами, объединяются через AND.

    ORDER_BY - список полей по которым производится сортировка
    результата. Поля обязательно должны быть включены в список выбираемых данных.
    """

    sql =u"""    SELECT case
             when a.f_collection_id IS NOT NULL and col.f_collection_tesis=1 then 'Тезисы'
             when a.f_journal_id IS NULL and a.f_collection_id IS NOT NULL then 'Статьи в сборниках'
             else 'Статьи в журналах'
           end AS category,
           a.f_article_id as pk,
           nvl(a.f_article_name, ' ') AS name,
           regexp_substr(a.f_article_doi, '10[^ ]*') AS user_doi,
           a.f_article_year AS year"""
    sql_from = u"""
    FROM article a, collection col"""
    sql_where = u"""
    WHERE a.f_collection_id = col.f_collection_id (+)"""

    # add external_doi
    sql += u""",
               external_doi.doi AS doi"""
    sql_from += u""",
      (select distinct * from
        (select f_activity_id,
                first_value(f_externalid_externalid)
                over (partition by f_activity_id order by f_externalid_externalid NULLS LAST) as doi
         from externalid
         where f_bibsource_id = 'DOI' and f_externalid_externalid is not NULL
        )) external_doi"""
    sql_where += u"""
            and a.f_article_id = external_doi.f_activity_id (+) """

    if addon_fields.get("authors") or addon_fields.get("ALL"):
        sql += u""",
           aa.authors AS authors"""

        sql_from += u""",
        (SELECT f_article_id, LISTAGG(f_authora_name, ', ') WITHIN GROUP (ORDER BY f_authora_ord) as authors
          FROM   authora
          GROUP BY f_article_id
         ) aa"""

        sql_where += u"""
        and aa.f_article_id = a.f_article_id"""

    if addon_fields.get("source") or addon_fields.get("ALL"):
        sql += u""",
           case
             when a.f_journal_id IS NOT NULL then j.f_journal_name
             when a.f_collection_id IS NOT NULL then col.f_collection_name
           end AS source,
           a.f_article_num as num,
           a.f_article_vol as vol,
           a.f_article_firstpage as firstpage,
           a.f_article_lastpage as lastpage"""
        sql_from += u", journal j"
        sql_where += u"""
        and a.f_journal_id = j.f_journal_id (+)"""

    if addon_fields.get("impacts") or addon_fields.get("ALL"):
        # impact_factor отображает суффикс псевдонима таблицы в тип рейтинга.
        impact_factors = {u'wos' : u'Impact Factor',
                          u'scopus': u'SJR',
                          u'rinc': u'RINC'
                          }
        # для каждого типа добавляем таблицу impacts_<тип>
        for impact_type in (u'wos', u'scopus', u'rinc'): # нужен фиксированный порядок
            table_alias = u"impacts_" + impact_type
            sql += u""",
            NVL(%s.value, %s_default.last_if) AS impact_%s""" % (table_alias, table_alias, impact_type)

            sql_from += u""",
            (select f_journal_id, max(F_JOURNALRANG_VAL) value,
f_journalrang_year
                from journalrang
                where F_JOURNALRANG_TYPE = '%s'
                group by f_journal_id, f_journalrang_year) %s,
                 (
              SELECT f_journal_id, f_journalrang_type
                    , min(f_journalrang_val) KEEP (DENSE_RANK FIRST
ORDER BY f_journalrang_year) first_if
                    , min(f_journalrang_year) first_if_year
                    , min(f_journalrang_val) KEEP (DENSE_RANK LAST ORDER
BY f_journalrang_year) last_if
                    , max(f_journalrang_year) last_if_year
              FROM journalrang
              WHERE f_journalrang_type = '%s'
              GROUP BY f_journal_id, f_journalrang_type
            ) %s_default"""  % (impact_factors[impact_type], table_alias, impact_factors[impact_type], table_alias)

            sql_where += u"""
            and a.f_journal_id = %s.f_journal_id (+)
            and a.f_article_year = %s.f_journalrang_year (+)
            and a.f_journal_id = %s_default.f_journal_id (+)""" % (table_alias, table_alias, table_alias)

    if addon_fields.get("journal_wos") or addon_fields.get("ALL"):
        sql += u""",
          nvl(scopus_journals.value, 0) as in_scopus,
          nvl(wos_journals.value, 0) as in_wos,
          nvl(top25_journals.value, 0) as in_top25,
          nvl(vak_journals.value, 0) as in_vak"""
        sql_from += u""",
         (
           select f_journal_id, max(value) as value
           from
           (
             select f_journal_id, 1 as value
             from jornalvak
             where f_journalvaktype_id in
                      (select f_journalvaktype_id from journalvaktype
                       where f_journalvaktype_name='Scopus')
            UNION ALL
             select f_journal_id, 1 as value
             from journalrang
             where f_journalrang_type like '%%SJR-%%'
           )
           group by f_journal_id
         ) scopus_journals,
         (
           select distinct f_journal_id, 1 as value from
            (
              select f_journal_id from journalrang
              where f_journalrang_type like '%%Impact Factor%%'
               UNION ALL
              select jv.f_journal_id
              from jornalvak jv
              where jv.f_journalvaktype_id=(select f_journalvaktype_id from journalvaktype
                                      where f_journalvaktype_name='Arts & Humanities Citation Index')
            )
         ) wos_journals,
         (
           select f_journal_id, 1 as value from v_wos_top25_journals group by f_journal_id
         ) top25_journals,
         (
           select distinct f_journal_id, 1 as value from jornalvak
           where f_journalvaktype_id in (select f_journalvaktype_id from journalvaktype where f_journalvaktype_name='Список ВАК')
         ) vak_journals"""
        sql_where += u"""
        and a.f_journal_id = scopus_journals.f_journal_id (+)
        and a.f_journal_id = wos_journals.f_journal_id (+)
        and a.f_journal_id = top25_journals.f_journal_id (+)
        and a.f_journal_id = vak_journals.f_journal_id (+)"""

    if addon_fields.get("citcount") or addon_fields.get("ALL"):
        sql += u""",
          citcount_wos.f_citcount_val as citcount_wos,
          citcount_scopus.f_citcount_val as citcount_scopus,
          citcount_wos.f_extcitcount_externalid as wos_id,
          citcount_scopus.f_extcitcount_externalid as scopus_id"""
        sql_from += u""",
          (select f_article_id, f_citcount_val, f_extcitcount_externalid from v_citcount where f_citcount_type='WOS') citcount_wos,
          (select f_article_id, f_citcount_val, f_extcitcount_externalid from v_citcount where f_citcount_type='Scopus') citcount_scopus"""
        sql_where += u"""
          and a.f_article_id = citcount_wos.f_article_id (+)
          and a.f_article_id = citcount_scopus.f_article_id (+)"""

    #
    # Построим основной запрос, который выбирает все данные с наложеннымии "свободными" ограничениями.
    # Новый уровень вложенности необходим, так как условия используют псевдонимы атрибутов.
    #
    main_query = u"""SELECT * FROM (
    """ + sql + sql_from + sql_where + u"""
    )
    WHERE 1=1
    """

    if conditions.get("FREE"):
        main_query += u"AND (" + conditions.get("FREE") + u")"

    #
    # Построи запрос верхнего уровня, который имеет вид
    #   select * from (...) main_query
    #   where 1=1
    #     and main_query.pk in (...)
    #     and main_query.pk in (...)
    #     and rownum <= ...
    # где вложенные запросы определяются дополнительными условиями фильтрации.

    toplevel_sql = u"""SELECT main_query.* FROM (
    """ + main_query + u""") main_query
    WHERE 1=1"""

    if conditions.get("confirmed"):
        toplevel_sql += u" and main_query.pk in (select f_article_id from confirmed_articles) "

    if conditions.get("department"):
        val =  conditions.get("department")
         # запрос, который выбирает статьи нужных подразделений
        dep_subquery = u""" select f_article_id
                            from v_department_articles da
                            where da.f_department_id"""  # + условие = или in
        try:
            department_id = int(val)
            dep_subquery += u" = %s" % department_id
        except ValueError:
            # val - это строка запроса
            dep_subquery += u" in (" + val + u")"

        toplevel_sql += u"""
        and main_query.pk in (""" + dep_subquery + u")"

    if conditions.get("man"):
        val =  conditions.get("man")

        # запрос, который выбирает статьи нужных авторов
        man_subquery = u""" select f_article_id
                            from authora aa
                            where aa.f_man_id"""  # + условие = или in
        try:
            author_id = int(val)
            man_subquery += u" = %s" % author_id
        except ValueError:
            # val - это строка запроса
            man_subquery += u" in (" + val + u")"

        toplevel_sql += u"""
        and main_query.pk in (""" + man_subquery + u")"

    if conditions.get("topic"):
        try:
            val = conditions.get("topic")
            topic_id = int(val)
            topic_subquery = u"""
            select a.f_article_id
            from article a, (
              select f_journalrub_id
              from journalrub
              connect by prior f_journalrub_id = jou_f_journalrub_id
              start with f_journalrub_id = %s
            ) jr,
            journalrublink jrl
            where a.f_journal_id = jrl.f_journal_id
              and jrl.f_journalrub_id = jr.f_journalrub_id""" % topic_id
            #print topic_subquery
            toplevel_sql += u"""
            and main_query.pk in (""" + topic_subquery + u")"
        except ValueError:
            pass
    if conditions.get("ROWNUM"):
        try:
            rn = int(conditions.get("ROWNUM", ""))
            toplevel_sql += (u" and ROWNUM <= %s" % rn)
        except ValueError:
            pass

    order_by_clause = u" ORDER BY " + ", ".join(order_by)
    toplevel_sql += order_by_clause

    return toplevel_sql

def generate_department_condition(author_table, object_table, condition,
                                  year_expr = None,
                                  select_field = None):
    """ Возвращает текст запроса, который выбирает SELECT_FIELD из произведения
    author_table, object_table и all_man_department, которые:
    1. удовлетворяют условиям соединения по равенству f_man_id
    2. условию соединения author_table и object_table
    3. условиям включения года публикации (year_expr) в интервал дат,
       определенный amd.b и amd.e
    4. удовлетворяют условиям CONDITION на amd.f_department_id
    По умолчанию предполагается, что все ключи имеют вид f_<table>_id.
    Если значение SELECT_FIELD не задано, то выбирается author_table.f_<object_table>_id.

    Пример: author_table=authorb, object_table book
       Output:
        (select author_table.F_BOOK_ID
         from AUTHORB author_table, all_man_department amd, book object_table
         where author_table.f_book_id = object_table.f_book_id
         and amd.f_man_id = author_table.f_man_id
         and ( amd.e is null
               or (extract(year from amd.e) >= object_table.f_book_year
                   and extract(year from amd.b) <= object_table.f_book_year)
             )
         and amd.department_id = CONDITION ИЛИ in (CONDITION)
        )
    """

    object_pk_name = "object_table.f_" + object_table + "_id"
    if not select_field:
        select_field = "f_" + object_table + "_id"
    author_fk_name = "author_table." + select_field
    if not year_expr:
        year_expr =  "object_table.f_" + object_table + "_year"
    sql = u"""
    (select author_table.""" + select_field + u"""
       from """ + author_table + u""" author_table, all_man_department amd, """ + object_table + """ object_table
       where """ + object_pk_name + " = " + author_fk_name + """
         and amd.f_man_id = author_table.f_man_id
         and ( amd.e is null
               or (extract(year from amd.e) >= """ + year_expr +"""
                   and extract(year from amd.b) <= """ + year_expr +""")
             )
         and amd.department_id """
    try:
        sql += u" = %d" % int(condition)
    except ValueError:
        # condition - это строка запроса
        sql += u" in (" + condition + u")"
    sql += u")"
    return sql


def generate_author_condition(table_name, select_field, condition):
    """Example: table_name=authorb select_field=f_book_id.
       Output:
        (select F_BOOK_ID
        from AUTHORB x
         where x.f_man_id = CONDITION ИЛИ in (CONDITION)
        )
    """

    sql = u"""
           (select x.""" + select_field + u"""
            from """ + table_name + u""" x
            where x.f_man_id """
    try:
        author_id = int(condition)
        sql += u" = %d" % author_id
    except ValueError:
        # condition - это строка запроса
        sql += u" in (" + condition + u")"
    sql += u")"
    return sql



def get_department_generate_nonarticles_pubs_query(addon_fields={},
                                                   conditions={},
                                                   order_by=[u'year', u'name']):
    """Возвращает текст sql запроса, который выбирает список всех
    публикаций с полями category, pk, name, user_doi, year, doi, source, npages, authors, isbn.
    """

    sql = u"""
    SELECT
          case when bg.f_bookgrouptype_id = %s or bg.f_bookgrouptype_id = %s then 'Учебное пособие'
               else 'Монография'
          end AS category,""" % (BOOKTYPE_TEXTBOOK, BOOKTYPE_TEXTBOOK_NOGRIF)
    sql += u"""
           b.f_book_id as pk,
           b.f_book_name AS name,
           null as user_doi,
           b.f_book_year AS year,
           null as doi,
           p.f_publishing_name || ' ' || b.f_book_place AS source,
           b.f_book_pages as npages,
           ba.authors AS authors,
           b.f_book_isbn as isbn,
           b.f_book_redpage as redpages,
           b.f_book_ncopies as ncopies
    FROM book b, publishing p,
         (SELECT f_book_id, LISTAGG(f_authorb_name, ', ') WITHIN GROUP (ORDER BY f_authorb_ord) as authors
          FROM   authorb
          GROUP BY f_book_id
         ) ba,
         bookgroup bg
    WHERE ba.f_book_id = b.f_book_id
    and b.f_book_id = bg.f_book_id (+)
    and b.f_publishing_id = p.f_publishing_id (+)"""

    if conditions.get("department"):
        sql += u"""
        and b.f_book_id in """ + generate_department_condition("authorb", "book", conditions.get("department"))

    if conditions.get("man"):
        sql += u"""
        and b.f_book_id in """ + generate_author_condition("authorb", "f_book_id", conditions.get("man"))

    # Patents
    sql += u"UNION ALL "
    sql += u"""
    select 'Патенты' AS category,
           p.f_patent_id as pk,
           p.f_patent_name AS name,
           null as user_doi,
           extract(year from p.f_patent_date) as year,
           null as doi,
           to_char(p.f_patent_num) AS source,
           null as npages,
           pa.authors AS authors,
           null as isbn,
           null as redpages,
           null as ncopies
    from patent p,
         (SELECT f_patent_id, LISTAGG(f_authorp_name, ', ') WITHIN GROUP (ORDER BY f_authorp_ord) as authors
          FROM   authorp
          GROUP BY f_patent_id
         ) pa
    where pa.f_patent_id = p.f_patent_id"""

    if conditions.get("department"):
        sql += u"""
        and p.f_patent_id in """ + generate_department_condition("authorp", "patent", conditions.get("department"),
                                                                 year_expr="extract(year from object_table.f_patent_date)")

    if conditions.get("man"):
        sql += u"""
        and p.f_patent_id in """ + generate_author_condition("authorp", "f_patent_id", conditions.get("man"))

    toplevel_sql = u"SELECT * from (" + sql + u") WHERE 1=1"
    if conditions.get("FREE"):
        toplevel_sql += u"AND (" + conditions.get("FREE") + u")"

    if order_by:
        toplevel_sql += u"""
        ORDER BY """ + ", ".join(order_by)
    return toplevel_sql



def get_department_generate_confs_query(addon_fields={},
                                        conditions={},
                                        order_by=[u'year', u'title']):
    sql = u"""
    SELECT 'Конференция' as category,
           p.f_presentation_id as pk,
           p.f_presentation_name as title,
           talkauthors.authors as authors,
           c.name as conf_name,
           c.conf_place as place,
           cc.country_name as country,
           c.year as year,
           s.f_confscope_name scope_name
    FROM t_confs c, presentation p, confscope s,
         (select c.conf_id, cc.id, NVL(cc.country_name_ru, u'Россия') AS country_name -- u'Россия') country_name
          from t_confs c,
          (
          select conf_id, country_.id as id, country_name_ru
          from t_confs cf, country_
          where nls_upper(conf_place) like '%%' || nls_upper(country_name_en) || '%%'
          or nls_upper(conf_place) like '%%' || nls_upper(country_name_ru) || '%%'
          ) cc
          where
          c.conf_id = cc.conf_id (+)
         ) cc,
         (SELECT f_presentation_id, LISTAGG(f_authord_name, ', ') WITHIN GROUP (ORDER BY f_authord_ord) as authors
          FROM   authord
          GROUP BY f_presentation_id
         ) talkauthors
    WHERE c.conf_id = p.conf_id
      and c.conf_id = cc.conf_id
      and talkauthors.f_presentation_id = p.f_presentation_id
    """

    if conditions.get("department"):
        sql += u"""
        and p.f_presentation_id in """ + generate_department_condition("authord", "f_presentation_id", conditions.get("department"))

    if conditions.get("man"):
        sql += u"""
        and p.f_presentation_id in """ + generate_author_condition("authord", "f_presentation_id", conditions.get("man"))

    toplevel_sql = u"SELECT * from (" + sql + u") WHERE 1=1"
    if conditions.get("FREE"):
        toplevel_sql += u"AND (" + conditions.get("FREE") + u")"

    if order_by:
        toplevel_sql += u"""
        ORDER BY """ + ", ".join(order_by)
    return toplevel_sql



def get_department_jboards(dep_id, year=None):
    sql = """
    select j.f_journal_name, count(distinct r.f_man_id)
    from journal j, redjournal r, all_man_department amd
    where r.f_journal_id = j.f_journal_id
      and amd.f_man_id = r.f_man_id
      and (amd.b <= NVL(r.f_redjournal_end,sysdate) and NVL(amd.e, sysdate) >= r.f_redjournal_begin)
      and amd.department_id = %s
    """
    if year:
        sql += """
        and trunc(r.f_redjournal_begin, 'yyyy') <= to_date('%s', 'yyyy') """ % int(year)
        sql += """
        and trunc(nvl(r.f_redjournal_end, sysdate), 'yyyy') >= trunc(to_date('%s', 'yyyy'), 'yyyy')
        """ % int(year)
    sql += """
    group by f_journal_name
    order by 2 desc
    """

    cursor = connections['reports_server'].cursor()
    cursor.execute(sql, [int(dep_id)])
    return list(cursor)


def get_department_dissovets(dep_id, year=None):
    sql = """
    select b.f_board_num || ' ' || b.f_board_name, count(distinct m.f_man_id)
    from board b, inboard m, all_man_department amd
    where b.f_board_id = m.f_board_id
      and amd.f_man_id = m.f_man_id
      and (amd.b <= NVL(m.f_inboard_end,sysdate) and NVL(amd.e, sysdate) >= m.f_inboard_begin)
      and amd.department_id = %s
    """
    if year:
        sql += """
        and trunc(m.f_inboard_begin, 'yyyy') <= to_date('%s', 'yyyy') """ % int(year)
        sql += """
        and trunc(nvl(m.f_inboard_end, sysdate), 'yyyy') >= trunc(to_date('%s', 'yyyy'), 'yyyy')
        """ % int(year)
    sql += """
    group by b.f_board_num || ' ' || b.f_board_name
    order by 2 desc
    """

    cursor = connections['reports_server'].cursor()
    cursor.execute(sql, [int(dep_id)])
    return list(cursor)



def get_anketa_data(man_id):
    sql = "select f_man_id, papers_in_scopus_wos, papers_in_vak, ncitations_wos, hirsh_index from v_anketa_sovet where f_man_id=%s and rownum<=1"
    cursor = connections['reports_server'].cursor()
    cursor.execute(sql, [int(man_id)])
    return list(cursor)[0]


def is_number(x):
  try:
    x + 1
    return True
  except TypeError:
    return False


def get_aggregate_info_searchresults(innerSql,  type, sqlparam_list=[]):
    
    years = []    
    deps = []

    sql = "select year, count(pk) from (%s) GROUP BY year ORDER BY year DESC" % innerSql
    #print sql
    cursor = connections['reports_server'].cursor()
    try:
      cursor.execute(sql, sqlparam_list)
      _years = cursor.fetchall()
    except:
      _years = []
    i = 0
    maxLength = 6
    for year in _years:
      if i >= maxLength:
        break
      if is_number(year[0]):
        years.append((year[0], year[1]))
        i += 1  # This is the same as count = count + 1

    
    
    if (type == "articles") or (type == "theses"):
      sql = "select f_department_id, count(*) as count from v_department_articles where f_article_id in (select pk from (%s) ) group by f_department_id order by count  desc" % innerSql
      #print sql
      cursor = connections['reports_server'].cursor()
      try:
        cursor.execute(sql, sqlparam_list)
        _deps = cursor.fetchall()
      except:
        _deps = []
      i = 0
      maxLength = 6
      for dep in _deps:
        if i >= maxLength:
          break
        deps.append((dep[0], Department.objects.get(pk=dep[0]).name, dep[1]))
        i += 1  
    



    return {'years' : years , 'departments' : deps}

