// -*- coding: utf-8; -*-

var dynamic = dynamic || {};

$(function() {
    var visualization;
    var data;
    var dataView;
    var options = {'showRowNumber': true};

    var visualization_kaf;
    var data_kaf;

    var visualization_pub;
    var data_pub;

    var visualization_pubrub; // distribution of articles by rubrics pie
    var data_pubrub;
    var dataView_pubrub;

    var dep_hist={};
    var n_dep_hist=0;

    var rub_hist={};
    var n_rub_hist=0;
    rub_hist[0]='<a href="javascript:void(0)" onClick="dynamic.setRubpieId(0, ' + "''" + ')">Все рубрики</a>';


    var pie_options = {title:"Данные по подразделениям", width:800, sliceVisibilityThreshold:1/30, pieResidueSliceLabel:"Другие"};
    var rubpie_options = {title:"", width:800, sliceVisibilityThreshold:1/30, pieResidueSliceLabel:"Другие"};

    var n_running_queries=0;
    var query_params={};

    dep_hist[0]='<a href="javascript:void(0)" onClick="dynamic.setDepartment(0, '+"''"+')">Все подразд.</a>';

    function initVisualization() {

      // Set paging configuration options
      // Note: these options are changed by the UI controls in the example.
      options['page'] = 'enable';
      options['pageSize'] = 10;
      options['pageSize'] = null; // disable paging
      options['page'] = null;
      options['height'] = 420;

      // Create the visualizations.
      visualization = new google.visualization.Table(document.getElementById('table'));
      visualization_kaf = new google.visualization.Table(document.getElementById('table_kaf'));
      visualization_pub = new google.visualization.Table(document.getElementById('table_pub'));
      visualization_pubrub = new google.visualization.PieChart(document.getElementById('pie_pubrub'));

      // Add our selection handler.
      google.visualization.events.addListener(visualization, 'select', selectHandler);
      google.visualization.events.addListener(visualization_pubrub, 'select', rubSelectHandler);

      google.visualization.events.addListener(visualization, 'ready', hide_dep_loading);
      //google.visualization.events.addListener(visualization_kaf, 'ready', hide_loading);
      google.visualization.events.addListener(visualization_pubrub, 'ready', hide_rubpie_loading);

      // setup default query params
      query_params["metric"] = $('#performance_metric_type').val();
      query_params["year"] = $('#publication_year').val();
      query_params["confirmed"] = $('#show_confirmed:checked').length;
      query_params["pub_type"] = $('#publication_type').val(); // journal, proc, etc
      query_params["id"] = ""; // department id
      query_params["rubid"] = ""; // rubric id
      query_params["rubtype"] = $('#rubtype').val(); // scopus, GRNTI, ...
      query_params["filter_deps_by_rub"] = $('#filter_deps_by_rub:checked').length;
      query_params["hide_unknown_rub"] = $('#hide_unknown_rub:checked').length;

      reload_all_data();
    }

    function draw() {
      //visualization.draw(data, options);
      visualization.draw(dataView, options);
      visualization.setSelection();
      $(".selected_dep h3").html(dep_hist[n_dep_hist]);
    }


    // sets the number of pages according to the user selection.
    function setNumberOfPages(value) {
      if (value) {
        options['pageSize'] = parseInt(value, 10);
        options['page'] = 'enable';
      } else {
        options['pageSize'] = null;
        options['page'] = null;
      }
      draw();
    }


    function update_pie(dataAsJson) {
        var piedata = [];
        piedata.push(["Department", "Value"]);
        $.each(dataAsJson["rows"], function(i, v){
                piedata.push([v["c"][1]["v"], v["c"][2]["v"]]);
          });
        piedata = google.visualization.arrayToDataTable(piedata);
        new google.visualization.PieChart(document.getElementById('pie_dep')).
        draw(piedata, pie_options);
    }

    function update_pubrub_pie(non_recursive) {
      show_rubpie_loading();
      query_params["group_by"]="rub";
      $.get("/statistics/xhr_depactivity_articles/",
            query_params,
            show_rubrics_data);
      query_params["group_by"]="department";
      if(query_params["filter_deps_by_rub"]==1 && non_recursive==1 ) {
        reload_all_data();
      }
    }

    function show_data(dataAsJsonStr) {
        var dataAsJson=jQuery.parseJSON(dataAsJsonStr);
        data = new google.visualization.DataTable(dataAsJson);
        dataView = new google.visualization.DataView(data);
        dataView.setColumns([1,2]);
        //hide_loading();
        visualization_kaf.clearChart();
        draw();
        update_pie(dataAsJson);
    }


    function rubSelectHandler() {
      var selection_rubpie = visualization_pubrub.getSelection();
      var rub_ids = selection_to_str(selection_rubpie, dataView_pubrub, data_pubrub, 0);
      if(rub_ids == "0" || rub_ids == "") {
        return;
      }
      query_params["rubid"] = rub_ids;

      n_rub_hist += 1;
      var url='<a href="javascript:void(0)" onClick="dynamic.setRubpieId('
           + n_rub_hist + ","
           + selection_to_str(selection_rubpie, dataView_pubrub, data_pubrub, 0) + ')">'
           + selection_to_str(selection_rubpie, dataView_pubrub, data_pubrub, 1)
           + "</a>";
      rub_hist[n_rub_hist] = rub_hist[n_rub_hist-1] + ": " + url;

      //rubpie_options["title"] = rub_hist[n_rub_hist];
      $(".selected_rub h3").html(rub_hist[n_rub_hist]);
      update_pubrub_pie(1);
    }


    dynamic.setRubpieTreshold = function (value) {
      rubpie_options["sliceVisibilityThreshold"]=parseFloat(value);
      draw_rubpie();
    }

    dynamic.setRubpieUnknown = function (value) {
      alert(value);
      query_params["hide_unknown_rub"] = value;
      if(query_params["hide_unknown_rub"]==1) {
           dataView_pubrub.hideRows( data_pubrub.getFilteredRows([{column: 0, value: 0}]) );
      }
      else {
        dataView_pubrub.setRows( 1, data_pubrub.getNumberOfRows() );
      }
      draw_rubpie();
    }


    dynamic.setRubpieType = function (value) {
      query_params["rubtype"] = value;
      dynamic.setRubpieId(0, "");
      update_pubrub_pie(1);
    }

    dynamic.setDeprubLink = function (value) {
      query_params["filter_deps_by_rub"]=value;
    }  

    dynamic.setRubpieId = function (hist_id, rub_ids) {
      n_rub_hist = hist_id;
      query_params["rubid"] = rub_ids;
      $(".selected_rub h3").html(rub_hist[n_rub_hist]);
      update_pubrub_pie(1);
    }

    dynamic.setRubpieMetric = function (value) {
      query_params["rubmetric"]=value;
      update_pubrub_pie();
    }


    function draw_rubpie() {
        visualization_pubrub.draw(dataView_pubrub.toDataTable(), rubpie_options);
        visualization_pubrub.setSelection();
    }

    function show_rubrics_data(dataAsJsonStr) {
        var dataAsJson=jQuery.parseJSON(dataAsJsonStr);
        data_pubrub = new google.visualization.DataTable(dataAsJson);
        dataView_pubrub = new google.visualization.DataView(data_pubrub);
        dataView_pubrub.setColumns([1,2]);
        
        // Remove unknown rubrics (condition: id==0)
        if(query_params["hide_unknown_rub"]==1) {
           dataView_pubrub.hideRows( data_pubrub.getFilteredRows([{column: 0, value: 0}]) );
        }

        draw_rubpie();
    }

    function show_data_kaf(dataAsJsonStr) {
        var dataAsJson=jQuery.parseJSON(dataAsJsonStr);
        data_kaf = new google.visualization.DataTable(dataAsJson);
        var dataView_kaf = new google.visualization.DataView(data_kaf);
        dataView_kaf.setColumns([1,2]);
        visualization_kaf.draw(dataView_kaf);

        new google.visualization.PieChart(document.getElementById('pie_dep')).draw(dataView_kaf.toDataTable(), pie_options);

        //update_pie(dataAsJson);
        //update_pubrub_pie();
    }


    function show_rubpie_loading() {
      $('#rubpie_loading').show();
      show_loading();
    }

    function hide_rubpie_loading() {
      $('#rubpie_loading').hide();
      hide_loading();
    }

    function show_dep_loading() {
       $('#vse_loading').show();
       show_loading();
    }

    function hide_dep_loading() {
      $('#vse_loading').hide();
      hide_loading();
    }


    function show_loading() {
       if( ++n_running_queries == 1 ) {
         $.blockUI({message: '<h3 class="loading-ui-anim-indicator-on-the-left" style="margin-bottom: 0px; padding: 10px; border: 1px solid gray; background-color: buttonface;">Обработка запроса</h3>'});
       }
    }

    function hide_loading() {
      if( --n_running_queries == 0 ) {
         $.unblockUI();
      }
    }


    function selection_to_str(selection, dataView, data, colidx) {
      var message = [];
      var str="";
      for (var i = 0; i < selection.length; i++) {
         var item = selection[i];
         if (item.row != null) {
           var  tableRowIndex = dataView.getTableRowIndex(item.row);
           var id = data.getFormattedValue(tableRowIndex, colidx);
           message.push(id);
         }
      }
      if(selection.length>0) str = message.join();
      return str;
    }

    dynamic.reload_all_data = function () {
      var request_params;
      show_dep_loading();
      $.get("/statistics/xhr_depactivity_articles/",
            query_params,
            show_data);
      update_pubrub_pie();
    }

    function reload_all_data() {
      dynamic.reload_all_data();
    }

    dynamic.list_matching_articles = function() {
        form=document.getElementById('filterParamsForm');
        old_action = form.action;
        form.target='_blank';
        document.getElementById('department_id').value = query_params["id"];
        document.getElementById('rub_id').value = query_params["rubid"];
        $("#filterParamsForm").attr("method", "get");
        form.action='/publications/search/articles/';
        form.submit();
        form.action=old_action;
        form.target='';
        return false;
    }

    dynamic.setParam = function(key, value) {
      query_params[key]=value;
    }

    dynamic.setDepartment = function (hist_id, dep_ids) {
      n_dep_hist = hist_id;
      query_params["id"] = dep_ids;
      $(".selected_dep h3").html(dep_hist[n_dep_hist]);
      reload_all_data();
    }

    dynamic.showDepartmentStats = function (dep_id) {
      var stats_url;
      stats_url = '/statistics/department_stats/' + dep_id + '/';
      if( !isNaN(parseInt(query_params["year"])) ) {
        stats_url += query_params["year"] + '/';
      }
      window.open(stats_url);
    }


    // The selection handler.
    // Loop through all items in the selection and concatenate
    // a single message from all of them.
    function selectHandler() {
       var selection = visualization.getSelection();
       if (selection.length>0) {
         var selection = visualization.getSelection();
         dep_ids = selection_to_str(selection, dataView, data, 0);
         query_params["id"] = dep_ids;

         if(n_dep_hist==0) {
           show_dep_loading();
           $.get("/statistics/xhr_depactivity_articles/",
              query_params,
              show_data); //show_data_kaf);
         }
         if(n_dep_hist<=1) { n_dep_hist += 1; }
         var url='<a href="javascript:void(0)" onClick="dynamic.setDepartment('
           + n_dep_hist + ","
           + selection_to_str(selection, dataView, data, 0) + ')">'
           + selection_to_str(selection, dataView, data, 1)
           + "</a>";
         var stats_link;
         stats_link ='(<a href="javascript:void(0)" onClick="dynamic.showDepartmentStats(' + dep_ids + ')">';
         stats_link += 'stats)</a>';
         dep_hist[n_dep_hist] = dep_hist[n_dep_hist-1] + ": " + url + " " + stats_link;

         $(".selected_dep h3").html(dep_hist[n_dep_hist]);

         update_pubrub_pie();
       }
     }

    google.setOnLoadCallback(initVisualization);
    //google.setOnLoadCallback(reload_all_data);

    //$(document).ready(reload_all_data);

});
