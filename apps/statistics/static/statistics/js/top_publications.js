$(function() {

    var worker_ids = $("#workers_registered").text().split(",");
    $.each(worker_ids, function(index, value) {
        $("ul.activity a[href^='/workers/" + value + "/']").addClass("emph")
    });

    $("a.article_confirm").click(function(event) {
        event.preventDefault()
        var article_id = $(this).parent().find("span.article_id").text()
        var mode;
        if ($(this).hasClass("yes")) {
            mode = "confirm";
            var deconfirm_all = 0;
        }
        else {
            mode = "deconfirm";
            var deconfirm_all = parseInt($("#deconfirm_all").text());
        }
        var url = "/statistics/articles/top/" + article_id + "/" + mode + "/"
        if (deconfirm_all) {
            url = url + "all/";
        }
        $(this).parent().find(".ui-autocomplete-loading-padded").toggle()
        $(this).parent().find("a.article_confirm").hide()
        $.ajax({
            url: url,
            dataType: "json",
            context: $(this),
            success: function() {
                $(this).parent().find(".ui-autocomplete-loading-padded").hide()
                if (mode == "confirm") {
                    $(this).parent().find("a.no").show()
                }
                else {
                    $(this).parent().find("a.yes").show()
                }
            },
            error: function() {
                alert("Произошла ошибка. Пожалуйста, обратитесь к администраторам.")
                $(this).parent().find(".ui-autocomplete-loading-padded").hide()
                if (mode == "confirm") {
                    $(this).parent().find("a.yes").show()
                }
                else {
                    $(this).parent().find("a.no").show()
                }
            }
        });
    });

});
