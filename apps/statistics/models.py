# -*- coding: utf-8; -*-
from django.db import models
from django.contrib.auth.models import User
from statistics.managers import LinkedToArticleManager, RankedArticleManager, TopArticleManager
import re
from common.utils.number import quantize
from common.models import MyModel, Country
from collections import namedtuple
from publications.utils import split_authors
from publications.models import Article, Book
from patents.models import Patent
from journals.models import Journal
from icollections.models import Collection
from conferences.models import Conference, ConferencePresentation
from random import randint


class LinkedToArticle(MyModel):

    objects = LinkedToArticleManager()

    class Meta:
        abstract = True


class SeenArticle(LinkedToArticle):
    id = models.AutoField(primary_key=True, db_column="f_selected_article_id")
    user = models.ForeignKey(to=User, related_name="seen_articles", db_column="f_user_id", verbose_name=u"Пользователь")
    article = models.ForeignKey(to="publications.Article", related_name="seen_articles", db_column="f_article_id", verbose_name=u"Статья")
    timestamp = models.DateTimeField(null=True, blank=True, db_column="f_date", auto_now_add=True)

    class Meta:
        db_table = "rep_selected_articles"
        verbose_name = u"просмотренная статья"
        verbose_name_plural = u"просмотренные статьи"

    def __unicode__(self):
        return "%s %s %s" % (self.timestamp.strftime("%d.%m %H:%M:%S"), self.user.username, self.article.title if hasattr(self, "article") else "")


class ConfirmedArticle(LinkedToArticle):
    id = models.AutoField(primary_key=True, db_column="f_confirmed_article_id")
    article = models.ForeignKey(to="publications.Article", related_name="confirmed_articles", db_column="f_article_id", verbose_name=u"Статья", on_delete=models.PROTECT)
    user = models.ForeignKey(to=User, related_name="confirmed_articles", db_column="f_user_id", verbose_name=u"Пользователь", blank=True, null=True)
    updated_at = models.DateTimeField(null=True, blank=True, db_column="f_confirmation_date", auto_now=True)

    class Meta:
        db_table = "confirmed_articles"
        verbose_name = u"подтвержденная статья"
        verbose_name_plural = u"подтвержденные статьи"
        permissions = (
            ("confirm_any_article", "Can confirm any article"),
            ("confirm_department_article", "Can confirm article from his department"),
        )

    def __unicode__(self):
        return "%s %s %s" % (self.updated_at.strftime("%d.%m %H:%M:%S"), self.user.username if self.user else "", self.article.title if hasattr(self, "article") else "")


class RankingProject(MyModel):
    name = models.CharField(u"Название проекта", max_length=255)
    startdate = models.DateField(u"Дата начала проекта")
    enddate = models.DateField(u"Дата окончания проекта")
    comment = models.TextField(u"Комментарий", db_column="txtcomment", blank=True)

    def __unicode__(self):
        return self.name

    def clear_results(self):
        self.rewarded_articles.all().delete()
        self.users.all().delete()

    def save_results(self):
        from publications.models import Article
        if self.rewarded_articles.exists():
            return
        all_top_articles_ids = TopArticle.objects.filter(article__year=2012).values_list('article', flat=True)
              
        # include only confirmed articles
        confirmed_articles_ids = ConfirmedArticle.objects.values_list('article', flat=True)
        articles_ids = set(all_top_articles_ids) & set(confirmed_articles_ids)
        articles = Article.objects.filter(id__in=articles_ids)
        for article in articles:
            if not is_linked_to_user(article): # there are no linked users, skip the article
                continue
            # get all previously rewarded users for the article
            users_previously_rewarded = []
            if article.rewarded_articles.exists():
                # somebody has been already rewarded for the article
                users_previously_rewarded = article.rewarded_articles.values_list('rewarded_authorships__user__user', flat=True)
            
            in_collaboration = is_article_in_collaboration(article)
            article_rating = calculate_article_rating(article, in_collaboration)
            article_kwargs = {
                'project': self,
                'article': article,
                'articleid': article.id,
                'title': article.title,
                'authors_count': article.authorships.count(),
                'authors_string': article.authors_string,
                'in_collaboration': in_collaboration,
                'journal': article.journal,
                'journal_name': article.journal.name,
                'year': article.year,
                'volume': article.volume,
                'number': article.number,
                'firstpage': article.firstpage,
                'lastpage': article.lastpage,
                'doi': article.doi,
                'rating': article_rating
            }
            rewarded_article = None
            # create authorships and users
            # get or create ranked user
            authorships = article.authorships.filter(author__profile__isnull=False)
            for authorship in authorships:
                worker = authorship.author
                user = worker.user
                if not user:
                    raise Exception("user must not be None")
                if user.id not in users_previously_rewarded: # 'reward' user only if he has not been previously rewarded for this article
                    if not rewarded_article:
                        rewarded_article, created = RankedArticle.objects.get_or_create(**article_kwargs)
                    user_kwargs = {
                        'project': self,
                        'user': user,
                        'worker': worker,
                        'name': worker.fullname,
                    }
                    try:
                        rewarded_user = RankedUser.objects.get(**user_kwargs)
                    except:
                        rewarded_user = RankedUser.objects.create(rating=0, **user_kwargs)
                    RankedArticleAuthorship.objects.get_or_create(user=rewarded_user, article=rewarded_article)
        # update user ratings
        for rewarded_user in self.users.all():
            articles_ids = rewarded_user.rewarded_authorships.values_list('article', flat=True)
            rating = sum(RankedArticle.objects.filter(id__in=articles_ids).values_list('rating', flat=True))
            rewarded_user.rating = quantize(rating)
            rewarded_user.save()


class RankedUser(MyModel):
    project = models.ForeignKey(to="RankingProject", related_name="users", verbose_name=u"Проект", on_delete=models.PROTECT)
    user = models.ForeignKey(to="auth.User", related_name="rewarded_users", verbose_name=u"Пользователь", on_delete=models.PROTECT)
    worker = models.ForeignKey(to="workers.Worker", related_name="rewarded_users", verbose_name=u"Сотрудник", on_delete=models.PROTECT)
    name = models.CharField(u"Имя сотрудника", max_length=255)
    rating = models.DecimalField(u"Рейтинг", max_digits=6, decimal_places=2)


class RankedArticle(LinkedToArticle):
    project = models.ForeignKey(to="RankingProject", related_name="rewarded_articles", verbose_name=u"Проект", on_delete=models.PROTECT)
    article = models.ForeignKey(to="publications.Article", related_name="rewarded_articles", verbose_name=u"Статья", on_delete=models.PROTECT)
    # redundant info for backup
    articleid = models.IntegerField(u"ID статьи")
    title = models.CharField(u"Название статьи", max_length=255)
    authors_count = models.IntegerField(u"Количество авторов")
    authors_string = models.CharField(u"Строка авторов", max_length=2000)
    in_collaboration = models.BooleanField(u"В коллаборации")
    journal = models.ForeignKey(to="journals.Journal", related_name="rewarded_articles", verbose_name=u"Журнал", on_delete=models.PROTECT)
    journal_name = models.CharField(u"Название журнала", max_length=255)
    year = models.IntegerField(u"Год издания статьи")
    volume = models.IntegerField(u"Том журнала", null=True, blank=True)
    number = models.CharField(u"Номер журнала", max_length=255, blank=True)
    firstpage = models.CharField(u"Первая страница статьи", max_length=32, blank=True)
    lastpage = models.CharField(u"Последняя страница статьи", max_length=32, blank=True)
    doi = models.CharField(u"DOI", max_length=255, blank=True)
    rating = models.FloatField(u"Рейтинг")

    objects = RankedArticleManager()


class RankedArticleAuthorship(MyModel):
    user = models.ForeignKey(to="RankedUser", related_name="rewarded_authorships", verbose_name=u"Пользователь", on_delete=models.PROTECT)
    article = models.ForeignKey(to="RankedArticle", related_name="rewarded_authorships", verbose_name=u"Статья")


class TopArticle(LinkedToArticle):
    article = models.OneToOneField("publications.Article", primary_key=True, related_name="top_article", verbose_name=u"Статья", db_column="F_ARTICLE_ID", on_delete=models.DO_NOTHING)
    objects = TopArticleManager()

    class Meta:
        db_table = "V_WOS_TOP25_ARTICLES"
        managed = False
        verbose_name = u"статья из топ-25%"
        verbose_name_plural = u"статьи из топ-25%"       


class ExternalSystem(models.Model):
    """
    External systems from which we take data. E.g. WoS, Scopus, DOI.
    """
    short_name = models.CharField(u"Краткое название", primary_key=True, max_length=64, db_column="f_bibsource_id")
    full_name = models.CharField(u"Полное название", max_length=512, db_column="f_bibsource_descr")
    has_citation_index = models.BooleanField(u"Содержит ли индекс цитирований (WoS - да, DOI - нет)", db_column="f_bibsource_iscitindex")

    class Meta:
        db_table = "BIBSOURCE"
        verbose_name = u"внешняя система"
        verbose_name_plural = u"внешние системы"       


class ExternalArticle(LinkedToArticle): # TODO: delete this record on edit/delete article
    """
    This table contains facts that we've found an external identifier for an article in external system.
    Note that we can learn e.g. article's DOI in Web of Science. In this case: system=WoS, category=DOI, external_id=DOI value.
    When an external id is found for the article, this date is set to found_at (and stays there virtually forever). 
    """
    id = models.AutoField(primary_key=True, db_column="f_externalid_id")
    article = models.ForeignKey(to="publications.Article", related_name="external_articles", db_column="f_activity_id", verbose_name=u"Статья")
    category = models.ForeignKey(to="ExternalSystem", verbose_name=u"Тип идентификатора (схема)", related_name="identifiers", max_length=64, db_column="f_bibsource_id")
    system = models.ForeignKey(to="ExternalSystem", verbose_name=u"Внешняя система, в которой нашли статью", related_name="identifiers_found_in", max_length=64, db_column="f_externalid_from")
    external_id = models.CharField(u"Идентификатор статьи в системе 'category', найденный в 'system'", max_length=512, db_column="f_externalid_externalid")   
    url = models.CharField(u"Ссылка на объект во внешней системе", max_length=2000, db_column="f_externalid_url")    
    user = models.ForeignKey(to=User, related_name="article_external_ids_added", db_column="f_user_id", verbose_name=u"Пользователь", blank=True, null=True)
    found_at = models.DateField(u"Дата нахождения", db_column="f_externalid_date")
    comment = models.CharField(u"Комментарий", max_length=512, db_column="f_externalid_comment", blank=True)

    class Meta:
        db_table = "EXTERNALID"
        verbose_name = u"идентификатор статьи во внешней системе"
        verbose_name_plural = u"идентификаторы статей во внешних системах"


class ExternalArticleSearchLog(LinkedToArticle):
    """
    This class represents facts that we search for an article in external system.
    The search is activated iff there is no ExternalArticle for the article in system and the last search is older than 1 month.
    """
    id = models.AutoField(primary_key=True, db_column="f_externalidlog_id")
    article = models.ForeignKey(to="publications.Article", related_name="external_search_logs", db_column="f_activity_id", verbose_name=u"Статья")
    system = models.ForeignKey(to="ExternalSystem", verbose_name=u"Внешняя система, в которой искали статью", related_name="search_logs", max_length=64, db_column="f_externalidlog_from")
    date = models.DateField(u"Дата поиска", db_column="f_externalidlog_date")

    class Meta:
        db_table = "EXTERNALIDLOG"
        verbose_name = u"поисковый запрос статьи во внешней системе"
        verbose_name_plural = u"поисковые запросы статей во внешних системах"


class ExternalArticleSearchRequest(LinkedToArticle):
    id = models.AutoField(primary_key=True, db_column="f_externalidrq_id")
    article = models.ForeignKey(to="publications.Article", related_name="pending_searches", db_column="f_activity_id", verbose_name=u"Статья")
    user = models.ForeignKey(to=User, related_name="requested_searches", db_column="f_externalidrq_user", verbose_name=u"Пользователь")
    date = models.DateField(u"Дата заказа", db_column="f_externalidrq_date", auto_now_add=True, null=True, blank=True)

    class Meta:
        db_table = "EXTERNALIDRQ"


class ArticleCitationCount(LinkedToArticle):
    """
    This is a database view that combines tables above to a compact view that is used to retrieve
    individial article's citations. And to check that for the article there is an external entity found.
    Here the last date of retrieval is stored, and the maximum citation count among all retrievals.
    """
    article = models.ForeignKey(to="publications.Article", related_name="citation_counts", verbose_name=u"Статья", db_column="F_ARTICLE_ID", on_delete=models.DO_NOTHING)
    system = models.ForeignKey(to="ExternalSystem", related_name="current_citation_counts", verbose_name=u"Внешняя система, из которой получено число цитирований статьи", max_length=64, db_column="F_CITCOUNT_TYPE")    
    external_id = models.CharField(u"Идентификатор статьи во внешней системе", primary_key=True, max_length=512, db_column="F_EXTCITCOUNT_EXTERNALID")
    value = models.IntegerField(u"Количество цитирований статьи", db_column="F_CITCOUNT_VAL")
    date = models.DateField(u"Дата последнего получения данных", db_column="F_CITCOUNT_DATE")

    class Meta:
        db_table = "V_CITCOUNT"
        managed = False
        verbose_name = u"актуальное число цитирований статьи по данным внешней системы"
        verbose_name_plural = u"актуальные показатели цитирования статей по данным внешних систем"


class ArticleCitationCountLog(MyModel):
    """
    This class represents articles' citations counts in external systems, with fixed date of retrieval.
    So there can be many objects for the same (system, external_id) but for different dates.
    """
    id = models.AutoField(primary_key=True, db_column="f_extcitcount_id")
    system = models.ForeignKey(to="ExternalSystem", related_name="citation_counts", verbose_name=u"Внешняя система, из которой получено число цитирований статьи", max_length=64, db_column="f_bibsource_id")
    external_id = models.CharField(u"Идентификатор статьи во внешней системе", max_length=512, db_column="f_extcitcount_externalid")
    value = models.IntegerField(u"Количество цитирований статьи", db_column="f_extcitcount_val")
    date = models.DateField(u"Дата получения данных", db_column="f_extcitcount_date")
    comment = models.CharField(u"Комментарий", max_length=512, db_column="f_extcitcount_comment", blank=True)    

    class Meta:
        db_table = "EXTCITCOUNT"
        verbose_name = u"число цитирований статьи по данным внешней системы"
        verbose_name_plural = u"число цитирований статей по данным внешних систем"


class WorkerCitationCount(MyModel):
    # this should not be a primary_key, but this view does not have one, and it is not managed, so it's ok
    # see http://stackoverflow.com/a/11594959/304209
    worker = models.ForeignKey(to="workers.Worker", primary_key=True, related_name="citation_counts", verbose_name=u"Сотрудник", db_column="F_MAN_ID", on_delete=models.PROTECT)
    system = models.ForeignKey(to="ExternalSystem", related_name="worker_citation_counts", verbose_name=u"Внешняя система, из которой получены цитирования сотрудника", max_length=255, db_column="F_CITCOUNT_TYPE")
    value = models.IntegerField(u"Количество ссылок", db_column="TOTAL_CITES")

    class Meta:
        db_table = "V_CITCOUNT_TOTAL_ALL"
        managed = False
        verbose_name = u"число цитирований сотрудника"
        verbose_name_plural = u"число цитирований сотрудников"


class DepartmentStatistics(MyModel):
    department = models.ForeignKey(to="organizations.Department", primary_key=True, related_name="statistics", db_column="f_department_id", verbose_name=u"Подразделение")
    articles_scopus = models.IntegerField(u"Количество статей в журналах из Scopus", db_column="nscopus")
    articles_wos_if3 = models.IntegerField(u"Количество статей в журналах с импакт-фактором > 3", db_column="nwos3")
    citations_scopus = models.IntegerField(u"Количество цитирований статей из Scopus в Web of Science", db_column="citscopus")
    articles_vak = models.IntegerField(u"Количество статей в русскоязычных ВАКовских журналах", db_column="nrussian")
    citations_vak = models.IntegerField(u"Количество цитирований русскоязычных статей из ВАК", db_column="citrussian")
    citations_per_article = models.FloatField(u"Среднее число цитирований на статью в Scopus", db_column="citations_per_article")
    workers_hirsh5 = models.IntegerField(u"Количество ученых с h>=5 на данный момент", db_column="hirsh5")
    workers_hirsh5_05_12 = models.IntegerField(u"Количество ученых с h>=5, работавших в 2005-2012 годах", db_column="hirsh5_05_12")

    class Meta:
        db_table = "DEPARTMENT_STAT_RANK"
        managed = False # this is a database view, not a table
        verbose_name = u"библиографические показатели подразделения"
        verbose_name_plural = u"библиографические показатели подразделений"


class WorkTuple(namedtuple('WorkTuple',
                            """category, pk, title, authors, source, year,
                            volume, number, firstpage, lastpage, npages, isbn,
                            conf_name, place, country, conf_start, conf_end,
                            doi, user_doi,
                            impact_wos, impact_scopus, impact_rinc,
                            in_wos, in_scopus, in_vak,
                            wos_id, scopus_id,
                            citcount_wos, citcount_scopus,redpages,ncopies""")):
    u"""
    Class used for returning stats data via sql about a single work (e.g. publication).
    Fields:
        category: type of work, in russian words (e.g. Статья в журнале),
        pk: id of work,
        title: title of work,
        authors: comma-separated list of authors in proper order, with initials,
        source: source of work, e.g. name of the journal, title of the proceedings, etc.,
        year: year of publication,
        volume: volume of the journal,
        number: number of the journal,
        firstpage: first page number,
        lastpage: last page number,
        npages: number of pages (for books),
        isbn: isbn (for books),
        conf_name: conference name,
        place: conference place,
        country: conference country,
        doi: DOI found by our algorithms,
        user_doi: DOI supplied by user,
        impact_wos: journal impact factor in Web of Science,
        impact_scopus: journal impact factor in Scopus,
        impact_rinc: journal impact factor in RINC (eLibrary.ru),
        in_wos: journal is found in Web of Science, 1/0,
        in_scopus: journal is found in Scopus, 1/0,
        in_vak: journal is explicitly enlisted in russian VAK list (so it is russian and is not in Wos/Scopus), 1/0,
        wos_id: work ID in Web of Science if it is found there,
        scopus_id: work ID in Scopus if it is found there,
        citcount_wos: number of citations for work in Web of Science,
        citcount_scopus: number of citations for work in Scopus.
    """

    def __new__(cls, *args, **kwargs):
        # initialize missing kwargs with None
        full_kwargs = {key: kwargs.get(key, None) for key in cls._fields}
        return super(WorkTuple, cls).__new__(cls, *args, **full_kwargs)

    def print_fields(self):
        print "\n".join(["%s: %s" % (field, unicode(getattr(self, field))) for field in self._fields])

    @property 
    def is_article(self):
        return self.category.startswith(u'Статьи') or self.category == u'Тезисы'

    @property 
    def is_book(self):
        return self.category in [u'Монография', u'Учебное пособие']

    @property 
    def is_patent(self):
        return self.category == u'Патенты'

    @property 
    def is_conference(self):
        return self.category == u'Конференция'

    @property 
    def conf_date_place(self):
        return "%d, %s" % (self.year, self.place)

    @property
    def external_system(self):
        "A name of the external system where information about the work's journal was found."
        # first priority is where we managed to find the actual work, not just the journal
        if self.wos_id:
            return 'Web of Science'
        elif self.scopus_id: 
            return 'Scopus'
        # second, if we haven't found the work, we take the system in which we've found the journal
        elif self.in_wos:
            return 'Web of Science'
        elif self.in_scopus:
            return 'Scopus'
        # third, if for some reason, in_wos/in_scopus is also None (this can be the case, see SVA for example)
        # then take impact factor source
        elif self.impact_wos:
            return 'Web of Science'
        elif self.impact_scopus:
            return 'Scopus'
        elif self.in_vak:
            # this is not absolutely correct, because if the article is in vak,
            # it does not mean it is found in RINC.
            # and actually we don't find any works in RINC (only journals).
            # but for the report purposes we assume that all works from vak
            # can be found in RINC.
            return u'РИНЦ'
        return ''

    @property 
    def citation_count(self):
        # first priority is where we managed to find the actual work, not just the journal
        if self.wos_id:
            return self.citcount_wos
        elif self.scopus_id: 
            return self.citcount_scopus
        # second, if we haven't found the work, we take the system in which we've found the journal
        elif self.in_wos:
            return self.citcount_wos
        elif self.in_scopus:
            return self.citcount_scopus
        return ''

    @property
    def in_full_vak(self):
        u"""
        Returns True if the item is in full vak list, i.e. in the russian vak list OR in wos/scopus databases.
        Open Question: what should be returned, if the work information is not found in wos/scopus, 
        but the journal is found, i.e. it has wos/scopus impact factor?
        """
        return self.in_vak or self.in_wos or self.in_scopus

    @property 
    def in_full_vak_str(self):
        return u'да' if self.in_full_vak else u'нет'

    @property 
    def impact_factor(self):
        # first priority is where we managed to find the actual work, not just the journal
        if self.wos_id and self.impact_wos:
            return self.impact_wos
        elif self.scopus_id and self.impact_scopus: 
            return self.impact_scopus
        # second, if we haven't found the work, we take the system in which we've found the journal
        elif self.in_wos and self.impact_wos:
            return self.impact_wos
        elif self.in_scopus and self.impact_scopus:
            return self.impact_scopus
        # finally, just return what we've got
        return self.impact_wos or self.impact_scopus or self.impact_rinc or ""

    @property 
    def instance(self):
        """Returns an instance of a corresponding python class for the work, e.g. Article, Book, Patent etc."""
        try:
            return self._instance
        except AttributeError:
            if self.is_article:
                self._instance = Article()
                if self.journal:
                    self._instance.journal = Journal(name=self.journal)
                elif self.collection:
                    self._instance.collection = Collection(title=self.collection)
            elif self.is_book:
                self._instance = Book()
            elif self.is_patent:
                self._instance = Patent()
            elif self.is_conference:
                self._instance = ConferencePresentation()
                self._instance.conference = Conference(name=self.conf_name, startdate=self.conf_start, enddate=self.conf_end, year=self.year, location=self.place, country=Country(name_ru=self.country))
            else:
                raise Exception('Unknown work type', self.category)
            self._instance.id = self.pk
            return self._instance
        
    @property 
    def url(self):
        if self.scopus_id:
            return self.instance.get_url_scopus(self.scopus_id)
        else:
            return self.instance.get_full_url()

    @property 
    def authors_string_bibtex(self):
        # note that self.authors can be None (and article can have authors, but some ancient articles,
        # didn't have authorship (AUTHORA), and for them self.authors will be None)
        # in this case we just call the function with None, and it takes authors from database by hit,
        # so there will be no problem (because id is set for instance)        
        return self.instance.get_authors_string_bibtex(self.authors)

    @property 
    def journal(self):
        return self.source if self.category == u'Статьи в журналах' else ''

    @property
    def collection(self):
        return self.source if self.category in [u'Статьи в сборниках', u'Тезисы'] else ''

    @property 
    def language(self):
        return self.instance.get_language_func(self.title)

    @property 
    def get_doi(self):
        return self.user_doi or self.doi or ''

    @property 
    def get_doi_str(self):
        return self.get_doi or u"нет"

    @property 
    def bib_pages(self):
        if self.is_article:
            return self.instance.get_pages(double_dash=True, firstpage=self.firstpage, lastpage=self.lastpage)
        elif self.is_book:
            return self.npages
        return ''

    @property 
    def publisher(self):
        if self.is_book:
            return self.source
        else:
            return ''

    @property 
    def source2number(self):
        return self.source.strip(u". №")

    @property 
    def get_title(self):
        if self.is_book and self.isbn:
            return self.title + u". — ISBN %s" % unicode(self.isbn)
        else:
            return self.title


    @property 
    def bibtex_fields(self):
        fields = [
            ('author', self.authors_string_bibtex),
            ('title', self.get_title),
            ('journal', self.journal),
            ('booktitle', self.collection),
            ('year', self.year),
            ('volume', self.volume),
            ('number', self.source2number if self.is_patent else self.number),
            ('isbn', self.isbn),
            ('pages', self.bib_pages),
            ('publisher', self.publisher),
            ('language', self.language)
        ]
        return filter(lambda x: bool(x[1]), fields)

    @property 
    def bibtex_id(self):
        # note that authors can be None (and article can have authors, but some ancient articles,
        # didn't have authorship (AUTHORA), and for them self.authors will be None)
        # in this case we just call the function, and it takes authors from database by hit,
        # so there will be no problem (because id is set for instance)
        first_author_lastname = split_authors(self.authors)[0][0] if self.authors else None
        # we add a random salt to bibtex_id, so that the different records
        # with the same metadata won't get merged by bibtex, screwing everything
        number = randint(0, 1000000)
        return self.instance.get_bibtex_id(self.title, self.year, first_author_lastname) + str(number)

    @property 
    def bib_type(self):
        return self.instance.bib_type

    @property 
    def bibtex(self):
        return self.instance.get_bibtex(self)

       

def is_linked_to_user(article):
    return article.authorships.filter(author__profile__isnull=False).exists()

def is_article_in_collaboration(article):
    patterns = [r'^et al$', r'^et al\..*$', r'^et\..*al.*$', r'^others$', r'^.*collaboration.*$', r'^alice c\.$']
    flags = re.UNICODE|re.DOTALL|re.IGNORECASE
    for name in article.authorships.values_list('original_name', flat=True):
        if any(re.match(pattern, name, flags) for pattern in patterns):
            return True
    return False

def get_collaboration_capacity(article):
    names = ", ".join(article.authorships.values_list('original_name', flat=True))
    names = names.lower()
    count = 0
    if 'atlas' in names:
        count += 2900
    if 'clas' in names:
        count += 150
    if 'cms' in names:
        count += 2200
    if 'd0' in names:
        count += 400
    if 'opera' in names:
        count += 190
    if 'zeus' in names:
        count += 300
    if 'dirac' in names:
        count += 80
    if 'lhcb' in names:
        count += 600
    if 'alice' in names:
        count += 1000
    if 'others' in names:
        count += 20
    if 'et al' in names or 'et. al' in names:
        count += 20
    if not count:
        count = 199
    return count

def calculate_article_rating(article, in_collaboration):
    authors_count = article.authorships.count()
    if in_collaboration:
        authors_count += (get_collaboration_capacity(article) - 1) # -1 not to count collaboration twice
        # the only difference with serg's sql calculation is that here only 1 author is subtracted for collaboration, and serg subtracts 1 for each collaboration author (so here number of authors may be greater by 1, if e.g. there are authors 'et al' and 'CMS collaboration'). But this is insignificant in virtually all cases.
    return 1./authors_count

def print_workers_coeffs(projects):
    from collections import defaultdict
    # get coefficients using goldan's stored information
    workers = defaultdict(int)
    for project in projects:
        ratings = project.users.values('worker', 'rating')
        for rating in ratings:
            workers[rating['worker']] += rating['rating']
#    print len(workers)
#    for worker_id, rating in workers.items():
#        worker = Worker.objects.get(id=worker_id)
#        print worker.fullname, rating
    # get coefficients using serg's analytics
    from statistics.sqlutils import get_top_workers
    workers_serg_raw = get_top_workers(year=2012) # add ', showall="all" ' to compare pairs of projects
    workers_serg = defaultdict(int)
    for worker in workers_serg_raw:
        workers_serg[worker.id] += worker.rating
        
    # compare two dicts
    matches = []
    rating_changed = []
    missing_goldan = []
    missing_serg = []
    all_keys = set(workers.keys() + workers_serg.keys())
    for worker_id in all_keys:
        if worker_id in workers.keys() and worker_id in workers_serg.keys():
            # matches or rating changed
            if workers[worker_id] == workers_serg[worker_id]:
                matches.append(worker_id)
            else:
                rating_changed.append(worker_id)
        elif worker_id in workers.keys():
            missing_serg.append(worker_id)
        elif worker_id in workers_serg.keys():
            missing_goldan.append(worker_id)
    print u"Рейтинг совпадает: ", len(matches)
    print u"Рейтинг отличается: ", len(rating_changed)
    for worker_id in rating_changed:
        print worker_id, workers[worker_id], workers_serg[worker_id]
    print u"Есть у Дениса, нет у Сергея: ", len(missing_serg)
    for worker_id in missing_serg:
        print worker_id
    print u"Есть у Сергея, нет у Дениса: ", len(missing_goldan)
    for worker_id in missing_goldan:
        print worker_id


from statistics.signals import *
from statistics.admin import *
