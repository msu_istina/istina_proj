# -*- coding: utf-8 -*-
import re
import pdb
from os import path
from collections import namedtuple, Counter
from math import sqrt
from django.db import connection
from django.db.models import Q, Count
from models import RankingProject, RankedUser, RankedArticle, RankedArticleAuthorship, ConfirmedArticle
from sqlutils import get_top_articles_ids, get_top_workers
from publications.models import Article, ArticleAuthorship
from workers.models import Worker
from decimal import Decimal
from common.utils.cache import invalidate_activity_type
from common.utils.list import list_powerset
from common.utils.number import quantize
from settings import ENV_DIR
from xlwt import Workbook
import datetime
from itertools import chain
from actstream.models import Action
from operator import itemgetter

def get_top_publications(year=None):
    sql = """
    select a.f_article_id
    from v_wos_top25_articles ta, article a
    where a.f_article_id=ta.f_article_id"""
    if year:
        sql += " and a.f_article_year=%d" % year
    cursor = connection.cursor()
    cursor.execute(sql)
    article_ids = [item[0] for item in list(cursor)]
    return article_ids

def is_article_in_collaboration(article):
    patterns = [r'^et al$', r'^et al\..*$', r'^et\..*al.*$', r'^others$', r'^.*collaboration.*$']
    flags = re.UNICODE|re.DOTALL|re.IGNORECASE
    for name in article.authorships.values_list('original_name', flat=True):
        if any(re.match(pattern, name, flags) for pattern in patterns):
            return True
    return False

def calculate_article_rating(article, in_collaboration):
    authors_count = article.authorships.count() + (0 if not in_collaboration else 199) # fixme bad
    return 1./authors_count

def save_reward_project_results():
    excluded, included, rating_changed, rating_remained = check_rewarded_workers()
    project = RankingProject.objects.all()[0]
    for worker in rating_remained+rating_changed:
        rating_changed = worker.old_rating != worker.current_rating
        real_worker = Worker.objects.get(id=worker.id)
        user = real_worker.user
        if not user:
            raise Exception("user must not be None")
        kwargs = {
            'project': project,
            'user': user,
            'worker': real_worker,
            'name': worker.name,
            'rating': Decimal(str(worker.old_rating))
        }
        rewarded_user, created = RankedUser.objects.get_or_create(**kwargs)
        articles_ids = get_top_articles_ids(project.startdate.year, worker_id=worker.id)
        articles = Article.objects.filter(id__in=articles_ids)
        articles = [article for article in articles if article.is_confirmed]
        user_rating = 0
        articles_to_filter = [] # store articles for future filtering for workers with changed rating
        for article in articles:
            in_collaboration = is_article_in_collaboration(article)
            article_rating = calculate_article_rating(article, in_collaboration)
            kwargs = {
                'project': project,
                'article': article,
                'articleid': article.id,
                'title': article.title,
                'authors_count': article.authorships.count(),
                'authors_string': article.authors_string,
                'in_collaboration': in_collaboration,
                'journal': article.journal,
                'journal_name': article.journal.name,
                'year': article.year,
                'volume': article.volume,
                'number': article.number,
                'firstpage': article.firstpage,
                'lastpage': article.lastpage,
                'doi': article.doi,
                'rating': article_rating
            }
            user_rating += article_rating
            if not rating_changed: # this is worker with rating not changed, thus all articles are rated
                rewarded_article, created = RankedArticle.objects.get_or_create(**kwargs)
                RankedArticleAuthorship.objects.get_or_create(user=rewarded_user, article=rewarded_article)
            else: # some of the selected articles are not rewarded (i.e. not included in final official prikaz)
                articles_to_filter.append(kwargs)
        if rating_changed: # we need to select articles that were rated
           flag = False
           # compute power set of sets of articles
           powerset = list_powerset(articles_to_filter)
           for subset in powerset:
               set_rating = sum(article['rating'] for article in subset)
               set_rating = float(quantize(set_rating))
               if worker.old_rating == set_rating: # combined rating for articles in the subset is equal to user official rating
                   for article in subset:
                       rewarded_article, created = RankedArticle.objects.get_or_create(**article)
                       RankedArticleAuthorship.objects.get_or_create(user=rewarded_user, article=rewarded_article)
                   flag = True
                   break
           if not flag:
               pdb.set_trace()
           if not flag:
               raise Exception("flag must not be None")
        user_rating = quantize(user_rating) # round down to two decimal places
        if worker.old_rating != float(user_rating) and (worker.current_rating == worker.old_rating or worker.current_rating != float(user_rating)):
            pdb.set_trace()
        if not (worker.old_rating == float(user_rating) or (worker.current_rating != worker.old_rating and worker.current_rating == float(user_rating))):
            raise Exception("ratings do not match")

def check_saved_reward_project_results():
    '''Check that stored data reflects data in official prikaz.'''
    # recalculate users' ratings
    for user in RankedUser.objects.all():
        user_rating = sum(authorship.article.rating for authorship in user.rewarded_authorships.all())
        if quantize(user_rating) != user.rating:
            pdb.set_trace()
        if not quantize(user_rating) == user.rating:
            raise Exception("ratings do not match")
    # check that the ratings are equal to static data from official prikaz
    workers = load_coeffs()
    print u"Количество сотрудников в приказе:", len(workers)
    print u"Количество сотрудников в базе данных:", RankedUser.objects.count()
    if not len(workers) >= RankedUser.objects.count(): 
        raise Exception("some workers from prikaz could be excluded, but not vice versa")
    for worker in workers:
        try:
            if worker.id: # if there are several workers in prikaz with the same name
                user = RankedUser.objects.get(worker__id=worker.id)
            else:
                user = RankedUser.objects.get(name=worker.name)
        except RankedUser.DoesNotExist:
            print u"Сотрудник %s из приказа не найден в базе данных" % worker.name
        else:
            if worker.old_rating != float(user.rating):
                print "Worker's rating in prikaz: %f" % worker.old_rating
                print "Worker's rating in database: %f" % user.rating
                pdb.set_trace()
    print u"Все рейтинги совпадают"


def load_coeffs():
    f = open(path.join(ENV_DIR, "tmp", "coeffs.txt"))
    WorkerTuple = namedtuple('WorkerTuple', 'name, id, department, was_selected, now_selected, old_rating, current_rating')
    workers = []
    for line in f:
        if not line.strip():
            continue
        try:
            rating, name, id = re.match(r'^(\d\,\d\d)\s+([\w\-\ ]+?)\s*(\d*)\s*$', line.decode("utf8"), re.UNICODE|re.DOTALL).groups()
        except AttributeError:
            import pdb; pdb.set_trace()
        worker = WorkerTuple(name=name, id=int(id) if id else None, department=None, was_selected=True, now_selected=None, old_rating=float(rating.replace(",", ".")), current_rating=None)
        workers.append(worker)
    f.close()
    return workers

def get_main_employments(worker):
    employments = worker.employments.exclude(enddate__lt=datetime.date.today()).exclude(parttime=True)
    parttime = False
    if not employments.exists(): # take part time employments
        employments = worker.employments.exclude(enddate__lt=datetime.date.today())
        parttime = True
    roots = set(chain.from_iterable(employment.department.roots for employment in employments))
    return roots, parttime

def export_reward_project_results():
    excel = Workbook(encoding='utf8')
    def encode(value):
        return unicode(value).encode('utf8')

    # i - row index, j - cell index (both 0-based)
    sheet = excel.add_sheet(u'Премированные сотрудники')
    headers = map(encode, [u"Номер", u"Имя сотрудника", u"Коэффициент", u"Премированных статей", u"Места работы", u"Несколько мест работы", u"Работает только по совместительству", u"Имя пользователя", u"ID сотрудника"])
    for j, value in enumerate(headers):
        sheet.write(0, j, value)
    for i, user in enumerate(RankedUser.objects.all().order_by('name'), start=1):
        employments, parttime = get_main_employments(user.worker)
        several_employments = u"Да" if len(employments) > 1 else u""
        parttime_str = u"Да" if parttime else u""
        employments_str = ", ".join(employment.name for employment in employments)
        values = map(encode, [i, user.name, user.rating, user.rewarded_authorships.count(), employments_str, several_employments, parttime_str, user.user.username, user.worker.id])
        for j, value in enumerate(values):
            sheet.write(i, j, value)
    note = u"В поле 'места работы' указываются все текущие места работы, кроме отмеченных как 'по совместительству'. Если таких нет, т.е. все места работы указаны как 'по совместительству', то в этом поле указаны все такие места работы, и в поле 'Работает только по совместительству' указывается 'Да'. Поле 'несколько мест работы' означает, что в поле 'места работы' указано несколько значений."
    sheet.write(i+1, 4, encode(note))

    sheet = excel.add_sheet(u"Премированные статьи")
    headers = map(encode, [u"Номер", u"ID статьи", u"Название статьи", u"Премированных авторов", u"Всего авторов", u"Премированные авторы", u"Все авторы", u"В коллаборации", u"ID журнала", u"Название журнала", u"Год", u"Том", u"Номер", u"Первая страница", u"Последняя страница", u"DOI", u"Коэффициент авторства"])
    for j, value in enumerate(headers):
        sheet.write(0, j, value)
    for i, article in enumerate(RankedArticle.objects.all().order_by('title'), start=1):
        rewarded_authors = article.rewarded_authorships.order_by('user__name').values_list('user', flat=True)
        rewarded_authors_string = ", ".join(RankedUser.objects.get(id=user).worker.fullname_short for user in rewarded_authors)
        in_collaboration_str = u"Да" if article.in_collaboration else ""
        volume = article.volume if article.volume is not None else ""
        values = map(encode, [i, article.articleid, article.title, len(rewarded_authors), article.authors_count, rewarded_authors_string, article.authors_string, in_collaboration_str, article.journal.id, article.journal_name, article.year, volume, article.number, article.firstpage, article.lastpage, article.doi, quantize(article.rating)])
        for j, value in enumerate(values):
            sheet.write(i, j, value)

    sheet = excel.add_sheet(u"Премированные авторства")
    headers = map(encode, [u"Номер", u"Номер сотрудника", u"Имя сотрудника", u"Имя пользователя", u"ID статьи", u"Название статьи", u"Премированных авторов", u"Всего авторов", u"В коллаборации", u"Коэффициент"])
    for j, value in enumerate(headers):
        sheet.write(0, j, value)
    last_worker_id = None
    worker_num = 0
    for i, authorship in enumerate(RankedArticleAuthorship.objects.all().order_by('user__name', 'user__id'), start=1):
        if last_worker_id != authorship.user.id:
            worker_num += 1
            last_worker_id = authorship.user.id
        article = authorship.article
        rewarded_authors_count = article.rewarded_authorships.count()
        in_collaboration_str = u"Да" if article.in_collaboration else ""
        values = map(encode, [i, worker_num, authorship.user.name, authorship.user.user.username, article.articleid, article.title, rewarded_authors_count, article.authors_count, in_collaboration_str, quantize(article.rating)])
        for j, value in enumerate(values):
            sheet.write(i, j, value)
    excel.save(open(path.join(ENV_DIR, 'tmp', 'reward_project_results.xls'), 'wb'))


def check_rewarded_workers():

    old_workers = load_coeffs()
    current_workers = get_top_workers()
    found_workers = []
    workers = []

    WorkerTuple = namedtuple('WorkerTuple', 'name, id, department, was_selected, now_selected, old_rating, current_rating')

    for worker in old_workers:
        for new_worker in current_workers:
            if new_worker in found_workers:
                continue
            if re.sub("\ +", " ", new_worker.name) == worker.name:
                if worker.id and worker.id != new_worker.id:
                    continue
                worker = worker._replace(id=new_worker.id, department=new_worker.department, now_selected=True, current_rating=float(new_worker.rating))
                found_workers.append(new_worker)
                break
        workers.append(worker)

    for worker in set(current_workers) - set(found_workers):
        workers.append(WorkerTuple(worker.name, worker.id,
                        worker.department, False, True, None, float(worker.rating)))


    excluded = [w for w in workers if w.was_selected and not w.now_selected]
    included = [w for w in workers if not w.was_selected and w.now_selected]
    rating_changed = [w for w in workers if w.was_selected and w.now_selected and w.current_rating != w.old_rating]
    rating_remained = [w for w in workers if w.was_selected and w.now_selected and w.current_rating == w.old_rating]
    print "Previously selected, now excluded:", len(excluded)
    print "Previously not selected, now included:", len(included)
    print "Same rating:", len(rating_remained)
    print "Changed rating:", len(rating_changed)
    return excluded, included, rating_changed, rating_remained

def is_linked_to_user(article):
    return article.authorships.filter(author__profile__isnull=False).exists()

def get_foreign_coauthors(mode):
    '''
    Return foreign coauthors of rewarded publications and related information.
    Two modes: 'top' and 'all'
    'top' mode: top publications coauthors in 1950-2012
    'all' mode: all coauthors
    '''
    def print_coauthors(articles):
        # filter out in-collaboration articles, articles not linked to users and articles with many authors
        articles = exclude_in_collaboration(articles)
        articles = filter_linked_to_users(articles)
        articles = filter_less_authors(articles, 10)
        ids = articles.values_list('id', flat=True)



        # get ids of authors of articles that are not linked to profiles
        coauthors = Article.objects.filter(id__in=ids, authorships__author__isnull=False, authorships__author__profile__isnull=True).values_list('authorships__author__id', 'authorships__author__lastname', 'authorships__author__firstname', 'authorships__author__middlename')
        coauthors = [pk for (pk, lastname, firstname, middlename) in list(set(coauthors)) if is_foreign((lastname, firstname, middlename))]
        #cnt = Counter(coauthors)
        #print "\n".join("%d %s %d" % (count, name, id) for ((id, name), count) in cnt.most_common(30))
        return coauthors

    if mode == 'top':
        # first part, top publications coauthors
        # get top articles for years from 1950 to 2012
        articles_ids = []
        for year in range(1950, 2013):
            articles_ids.extend(get_top_articles_ids(year))
        articles = Article.objects.filter(id__in=articles_ids)
    elif mode == 'all': # examine all articles
        articles = Article.objects.all()
    return print_coauthors(articles)


def export_coauthors_info():
    '''
    Return foreign coauthors of rewarded publications and related information.
    Format: excel file ENV_DIR/tmp/coauthors.xsl.
    Columns: #, coauthor name, all publications count (with users-coauthors), top publications count (with users-coauthors), profile url, users-coauthors
    '''
    # ids are selected by hand from 'get_foreign_coauthors' function output
    #ids = [544254,387476,445033,476404,428368,436885,427907,484243,405233,237799,401004,442572,836335,406292,471382,411529,477646]
    ids = get_foreign_coauthors('all')
    excel = Workbook(encoding='utf8')
    sheet = excel.add_sheet(u'Иностранные соавторы')
    def encode(value):
        return unicode(value).encode('utf8')
    top_articles_ids = []
    for year in range(1950, 2013):
        top_articles_ids.extend(get_top_articles_ids(year))

    rows = []
    for i, worker_id in enumerate(ids):
        worker = Worker.objects.get(id=worker_id)
        print "%d of %d" % (i, len(ids))
        worker_all_articles_ids = ArticleAuthorship.objects.filter(author=worker).values_list('article__id', flat=True)
        worker_all_articles = Article.objects.filter(id__in=worker_all_articles_ids)
        worker_all_articles = [article for article in worker_all_articles if is_linked_to_user(article)]
        worker_all_articles_count = len(worker_all_articles)
        if worker_all_articles_count < 2:
            continue
        # get top publications count with users coauthors
        worker_top_articles_ids = ArticleAuthorship.objects.filter(author=worker).filter(article__in=top_articles_ids).values_list('article__id', flat=True)
        worker_top_articles = Article.objects.filter(id__in=worker_top_articles_ids)
        worker_top_articles = [article for article in worker_top_articles if is_linked_to_user(article)]
        worker_top_articles_count = len(worker_top_articles)
        coauthors_str = ", ".join(coauthor.fullname_short for coauthor in worker.coauthors if coauthor.user) # coauthors linked to users
        rows.append(map(encode, [worker.fullname, worker_all_articles_count, worker_top_articles_count, worker.get_absolute_url(), coauthors_str]))
        #alues.append(map(encode, [i+1, worker.fullname]))
    rows_sorted = sorted(rows, key=lambda row: int(row[1]), reverse=True)
    headers = map(encode, [u"Номер", u"Имя автора", u"Общее количество совместных статей с сотрудниками МГУ", u"Количество совместных статей с сотрудниками МГУ в топ-журналах", u"Адрес страницы в ИСТИНЕ (добавьте в начале http://istina.msu.ru)", u"Соавторы из МГУ"])
    for i, value in enumerate(headers):
        sheet.write(0, i, value)
    for i, row in enumerate(rows_sorted):
        print "printing", i, "line"
        sheet.write(i+1, 0, str(i+1))
        for j, value in enumerate(row):
            sheet.write(i+1, j+1, value)
    excel.save(open(path.join(ENV_DIR, 'tmp', 'foreign_coauthors.xls'), 'wb'))

def exclude_in_collaboration(article_qs):
    ids = article_qs.values_list('id', flat=True)
    return Article.objects.filter(id__in=ids).exclude(
        Q(authorships__original_name__iexact="et al") | \
        Q(authorships__original_name__istartswith="et al.") | \
        Q(authorships__original_name__istartswith="et.", authorships__original_name__icontains="al") | \
        Q(authorships__original_name__icontains="collaboration")
    ).distinct()

def filter_less_authors(article_qs, n_authors):
    ids = article_qs.values_list('id', flat=True)
    return Article.objects.values('id').annotate(num_authors=Count('authorships')).filter(id__in=ids, num_authors__lte=n_authors).distinct().order_by()

def filter_linked_to_users(article_qs):
    ids = article_qs.values_list('id', flat=True)
    return Article.objects.filter(id__in=ids, authorships__author__profile__isnull=False).distinct()

def exclude_russian():
    suffixes = ["ov", u"ов", "ev", u"ев", "va", u"ва", "in", u"ин", "na", u"на", "ko", u"ко", "ii", u"ий", "aya", u"ая", "ky", u"ич", "ki"]
    query = Q(lastname__iendswith=suffixes[0])
    for suffix in suffixes[1:]:
        query = query | Q(lastname__iendswith=suffix)
    return Worker.objects.exclude(query)

def is_russian(name):
    suffixes = ("ov", u"ов", "ev", u"ев", "va", u"ва", "in", u"ин", "na", u"на", "ko", u"ко", "ii", u"ий", "aya", u"ая", "ky", u"ич", "ki", "ich", "yak", "tsyn", "yants", "chuk", "dze", "yk", "yuk", "yan", "shvili", "aia", "kiy", "kikh", "ok", "nyi", "kij", "ak", "ach")
    return name.lower().endswith(suffixes)

def not_author(name):
    blocklist = ("et al.", "ATLAS")
    return name.endswith(blocklist)

def is_foreign(names):
    for name in names:
        if name and (not re.match(r'[a-zA-Z]', name) or is_russian(name) or not_author(name)):
            return False
    return True

def test():
 articles = Article.objects.filter(authorships__author__id=580214)
 articles = exclude_in_collaboration(articles)
 articles = filter_less_authors(articles, 10)
 articles = filter_linked_to_users(articles)
 print articles.count()

def cmp_authorships(auth1, auth2):
    if auth1.author.fullname < auth2.author.fullname:
        return -1
    elif auth1.author.fullname > auth2.author.fullname:
        return 1
    if auth1.author.id < auth2.author.id:
        return -1
    elif auth1.author.id > auth2.author.id:
        return 1
    return auth1.article.title <= auth2.article.title

def get_not_rewarded_authorships(export_to_excel=True):
    '''Get authorships of confirmed articles that have not yet been rewarded.'''
    authorships = [] # authorships to be notified (confirmed but not rewarded).
    for confirmed_article in ConfirmedArticle.objects.all():
        article = confirmed_article.article
        rewarded_article = RankedArticle.objects.filter(article=article)
        # this article should be in RankedArticles AND all authorships should be rewarded.
        if not rewarded_article.exists():
            authorships.extend(list(ArticleAuthorship.objects.filter(article=article,author__profile__isnull=False)))
        else: # the article is rewarded, but check that all authorships are rewarded
            rewarded_article = rewarded_article[0]
            for authorship in ArticleAuthorship.objects.filter(article=article,author__profile__isnull=False):
                rewarded_user = RankedUser.objects.filter(worker=authorship.author)
                if not rewarded_user.exists():
                    authorships.append(authorship)
                else:
                    rewarded_authorship = RankedArticleAuthorship.objects.filter(user=rewarded_user,article=rewarded_article)
                    if not rewarded_authorship.exists():
                        authorships.append(authorship)
    if export_to_excel:
        excel = Workbook(encoding='utf8')
        sheet = excel.add_sheet(u'Не премированные авторства')
        def encode(value):
            return unicode(value).encode('utf8')
        headers = map(encode, [u"Номер", u"Номер сотрудника", u"Имя сотрудника", u"Имя пользователя/адрес страницы в ИСТИНЕ (префикс http://istina.msu.ru/accounts/profile/)", u"ID статьи/адрес страницы в ИСТИНЕ (префикс http://istina.msu.ru/publications/article/)", u"Название статьи"])
        for i, value in enumerate(headers):
            sheet.write(0, i, value)
        # sort authorships by worker surname
        authorships_sorted = sorted(authorships, cmp=cmp_authorships)
        worker_num = 0
        last_worker_id = None
        for i, authorship in enumerate(authorships_sorted):
            if last_worker_id != authorship.author.id:
                worker_num += 1
                last_worker_id = authorship.author.id
            worker = authorship.author
            article = authorship.article
            values = map(encode, [i+1, worker_num, worker.fullname_short, worker.user.username, article.id, article.title])
            for j, value in enumerate(values):
                sheet.write(i+1, j, value)
        excel.save(open(path.join(ENV_DIR, 'tmp', 'unrewarded_workers.xls'), 'wb'))
    return authorships

def article_creation_date(article):
    actions = Action.objects.filter(verb=u"добавил статью", target_object_id=article.id).values_list('timestamp', flat=True)
    date = min(actions).strftime("%d.%m %H:%M:%S") if actions else ""
    return date

def get_possible_unrewarded_authorships():
    '''Get articles that were confirmed, and at least one of authors was included in prikaz, but there also were other authors that were linked to this article but were not included in prikaz. See mail http://goo.gl/v5pQl for details.'''
    excel = Workbook(encoding='utf8')
    sheet1 = excel.add_sheet(u"Не учтенные статьи")
    def encode(value):
        return unicode(value).encode('utf8')
    headers = map(encode, [u"Номер", u"ID статьи", u"Название статьи", u"Дата добавления", u"Дата подтверждения", u"Кто подтвердил"])
    for i, value in enumerate(headers):
        sheet1.write(0, i, value)
    sheet1_cnt = 1
    sheet2 = excel.add_sheet(u'Авторства вовремя')
    headers = map(encode, [u"Номер", u"Номер сотрудника", u"Имя сотрудника", u"Имя пользователя", u"Дата связи пользователя с сотрудником", u"Дата последнего действия", u"ID статьи", u"Название статьи"])
    for i, value in enumerate(headers):
        sheet2.write(0, i, value)
    sheet2_cnt = 1
    excluded_articles = []
    excluded_authorships_ontime = []
    excluded_authorships_late = []
    # get confirmed articles
    confirmed_articles = ConfirmedArticle.objects.all()
    for confirmed_article in confirmed_articles:
        article = confirmed_article.article
        rewarded_article = RankedArticle.objects.filter(article=article)
        if not rewarded_article.exists(): # article was not included in prikaz
            excluded_articles.append(article)
            confirmation_date = confirmed_article.updated_at.strftime("%d.%m %H:%M:%S")
            username = confirmed_article.user.username if confirmed_article.user else ""
            values = map(encode, [sheet1_cnt, article.id, article.title, article_creation_date(article), confirmation_date, username])
            for i, value in enumerate(values):
                sheet1.write(sheet1_cnt, i, value)
            sheet1_cnt += 1
        else:
            rewarded_article = rewarded_article[0]
            # check that all authorships were included in prikaz
            if not rewarded_article.rewarded_authorships.count() == article.authorships.filter(author__profile__isnull=False).count():
                for authorship in article.authorships.filter(author__profile__isnull=False):
                    if not rewarded_article.rewarded_authorships.filter(user__worker=authorship.author):
                        name = authorship.author.fullname_short
                        username = authorship.author.user
                        link_actions = Action.objects.filter(actor_object_id=authorship.author.user.id, verb=u"связал свой профиль с сотрудником", target_object_id=authorship.author.id).values_list('timestamp', flat=True)
                        link_date = min(link_actions)
                        link_date_str = link_date.strftime("%d.%m %H:%M:%S")
                        last_action_date = max(Action.objects.filter(actor_object_id=authorship.author.user.id).values_list('timestamp', flat=True)).strftime("%d.%m %H:%M:%S")
                        values = [name, username, link_date_str, last_action_date, article.id, article.title]
                        if link_date <= datetime.datetime(2012, 06, 04, 23, 59, 59):
                            excluded_authorships_ontime.append(values)
                        else:
                            excluded_authorships_late.append(values)
    #import pdb; pdb.set_trace()
    excluded_authorships_ontime.sort(key=itemgetter(0)) # sort by worker name
    last_worker_id = None
    worker_num = 0
    for i, row in enumerate(excluded_authorships_ontime, start=1):
        sheet2.write(i, 0, str(i))
        if row[1] != last_worker_id:
            worker_num += 1
            last_worker_id = row[1]
        sheet2.write(i, 1, str(worker_num))
        for j, value in enumerate(row, start=2):
            sheet2.write(i, j, encode(value))

    excluded_authorships_late.sort(key=itemgetter(0))
    last_worker_id = None
    worker_num = 0
    sheet3 = excel.add_sheet(u"Поздние авторства")
    for i, value in enumerate(headers):
        sheet3.write(0, i, value)
    for i, row in enumerate(excluded_authorships_late, start=1):
        sheet3.write(i, 0, str(i))
        if row[1] != last_worker_id:
            worker_num += 1
            last_worker_id = row[1]
        sheet3.write(i, 1, str(worker_num))
        for j, value in enumerate(row, start=2):
            sheet3.write(i, j, encode(value))
    excel.save(open(path.join(ENV_DIR, 'tmp', 'unrewarded_authorships.xls'), 'wb'))

def invalidate_rewarded_articles_cache(reward_project):
    '''Invalidate article list cache for all authors of rewarded articles. Needed for correct reward icons display. Run after finalizing reward project results. See #1021.'''
    workers = []
    for article in reward_project.rewarded_articles.all():
        if article.article:
            workers.extend(article.article.authors.all())
    for worker in set(workers):
        invalidate_activity_type(worker.id, "article") # invalidates full articles list and latest articles list caches

def invalidate_all_workers_articles_cache():
    '''Invalidate article list cache for all workers. Needed when top journals list changes, and reward icons for all articles should be recalculated. See #1021.'''
    for worker in Worker.objects.all():
        invalidate_activity_type(worker.id, "article") # invalidates full articles list and latest articles list caches

