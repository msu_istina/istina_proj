# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from statistics.views.stats import TopWorkersBySpecialtyIndexView, TopWorkersBySpecialtyView
from dissertations.models import Specialty

urlpatterns = patterns('statistics.views',

    # public pages
    url(r'^$', 'index', name='statistics_index'),

    url(r'^journals/top/$', 'top_journals', name="statistics_top_journals"),
    url(r'^journals/top/all/$', 'top_journals', {'all': True}, name="statistics_top_journals_all"),
    url(r'^journals/top/(?P<rubric_name>[\w\-]+)/$', 'top_journals', name="statistics_top_journals_rubric"),

    url(r'^workers/top/$', 'top_workers', name="statistics_top_workers"),
    url(r'^workers/top/(?P<year>\d+)/(?P<showall>\w+)/$', 'top_workers', name="statistics_top_workers_extra"),
    url(r'^organization/(?P<organization_id>\d+)/workers/top/by_specialty/$', TopWorkersBySpecialtyIndexView.as_view(), name="statistics_top_workers_by_specialty_index"),
    url(r'^organization/(?P<organization_id>\d+)/workers/top/by_specialty/(?P<specialty_number>[\d\.]+)/$', TopWorkersBySpecialtyView.as_view(), name="statistics_top_workers_by_specialty"),

    url(r'^articles/top/(?:(?P<year>\d+)/)?(?:(?P<by_department>by_department)/)?(?:(?P<edit>edit)/)?$',
        'top_articles', name="statistics_top_articles"),

    url(r'^projects/(?P<project_id>\d+)/articles/rewarded/$', 'rewarded_articles', name="statistics_rewarded_articles"),
    url(r'^worker/top_articles/$', "top_articles", {'my': True}, name="statistics_my_top_articles"),
    url(r'^worker/top_publications/$', "top_articles", {'my': True}, name="statistics_my_top_articles_duplicate"),
    url(r'^worker/(?P<worker_id>\d+)/top_articles/$', "top_articles", name="statistics_top_articles_by_worker"),
    url(r'^worker/(?P<worker_id>\d+)/top_articles/edit/$', "top_articles", {'edit': True}, name="statistics_top_articles_by_worker_edit"),

    # Hirsh index and other citation metrics
    url(r'^department/(?P<dep_id>\d+)/hirsh/$', 'department_hirsh', name='statistics_department_hirsh'),
    url(r'^department/(?P<dep_id>\d+)/ccby/$', 'department_citcount_by_year', name='statistics_department_citcount_by_year'),

    url(r'^top_departments/$', "top_departments", name="statistics_top_departments"),

    url(r'^last_month_activities/$', 'last_month_activities', name="last_month_activities"),

    url(r'^xhr_depactivity_articles/$', 'xhr_depactivity_articles', name='statistics_xhr_depactivity_articles'),
    url(r'^dynamic/$', 'dynamic', name='statistics_dynamic'),

    # admin pages
    url(r'^journals/rubrics/$', 'top_journals_rubrics_stats', name="statistics_top_journals_rubrics_stats"),
    url(r'^articles/rubrics/$', 'top_articles_by_rubrics', name="statistics_top_articles_by_rubrics"),

    url(r'^department_stats/(?P<dep_id>\d+)/((?P<year>\d+)/)?$', 'department_stats', name='statistics_department_stats'),

    url(r'^departments/achievements/$', 'departments_achievements', name='statistics_departments_achievements'),

    url(r'^articles/top/(?P<article_id>\d+)/confirm/$', "top_article_confirm", {"confirm": True}, name="statistics_article_confirm"),
    url(r'^articles/top/(?P<article_id>\d+)/deconfirm/(?:(?P<deconfirm_all>all)/)?$', "top_article_confirm", {"confirm": False}, name="statistics_article_deconfirm"),

    # generic SQL reports
    url(r'^get_org_tree/$', "get_org_tree", name="statistics_get_org_tree"),

    url(r'^activity/$', 'activity_stats', name='statistics_activity'),

)
