# -*- coding: utf-8; -*-
from django import forms

# stats top rated publications
class STRPForm(forms.Form):
    r = forms.FloatField()
    year = forms.IntegerField()
    impact_factor_type = forms.ChoiceField(label=u"Тип импакт-фактора", choices= (("5-Year Impact Factor 2010", u"5-летний"), ("Impact Factor 2010", u"2010 года")))

    #def __init__(self, *args, **kwargs):
        #super(STRPForm, self).__init__(*args, **kwargs)
        #self.fields['impact_factor_type'] = forms.ChoiceField(choices= (("5-Year Impact Factor 2010", u"5-летний"), ("Impact Factor 2010", u"2010 года")))


