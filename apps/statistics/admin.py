from django.contrib import admin
from models import SeenArticle, ConfirmedArticle, RankingProject, RankedUser, RankedArticle, RankedArticleAuthorship, WorkerCitationCount, TopArticle, ArticleCitationCount

admin.site.register(SeenArticle)
admin.site.register(ConfirmedArticle)
admin.site.register(RankingProject)
admin.site.register(RankedUser)
admin.site.register(RankedArticle)
admin.site.register(RankedArticleAuthorship)
admin.site.register(WorkerCitationCount)
admin.site.register(TopArticle)
admin.site.register(ArticleCitationCount)
