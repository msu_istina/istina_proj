# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from common.utils.user import get_fullname
from common.utils.email import get_sent_user_ids
from django.core.mail import send_mail
from settings import ENV_DIR, DEFAULT_FROM_EMAIL
from os.path import join
from time import sleep

class Command(BaseCommand):
    def handle(self, *args, **options):

        subject = u"Информация о компенсации личных расходов сотрудников МГУ на оплату публикаций в высокорейтинговых научных журналах"
        template_name = "statistics/user_refund_email.txt"
        log_filename = "refund_email.log"
        users = "no" # no, goldan, admins, active, all
        really_send = False

        usernames_excluded = ['Barmin', 'Buchin', 'Falunin', 'Glagolev', 'Kuznetsov', 'Panob', 'Pavelyev', 'Savinov', 'Stepanov', 'Yakimov', 'Nekrasov', 'Cherny']
        users_excluded = User.objects.filter(email="gala@imec.msu.ru", username__in=usernames_excluded)

        if users == "no":
            users = [] # no users, default
        elif users == "goldan":
            users = User.objects.filter(email__in=["denis.golomazov@gmail.com"]) # just me
        elif users == "admins":
            users = User.objects.filter(email__in=["denis.golomazov@gmail.com", "serg@msu.ru"]) # admins to test
        elif users == "active":
            users = User.objects.filter(is_active=True) # all active users, use with caution!
        elif users == "all":
            users = User.objects.all() # all users, use with caution!
        else:
            raise

        # send only to users to which the email has been not sent yet
        #sent_ids = get_sent_user_ids(["../../var/log/rewarding_email_1.log", "../../var/log/rewarding_email_2.log", "../../var/log/rewarding_email_3.log"])
        #users = User.objects.exclude(id__in=sent_ids)

        from_email = u"ИСТИНА <info@istina.msu.ru>"
        logfile = open(join(ENV_DIR, 'var', 'log', log_filename), 'w')
        for user in users:
            name = get_fullname(user)
            log_message = u"%d %s %s %s\n" % (user.id, user.username, user.email, name)
            if user in users_excluded or not user.email:
                log_message = "SKIP: %s" % log_message
            else:
                message = render_to_string(template_name, {'name': name, 'username': user.username})
                if really_send:
                    send_mail(subject, message, from_email, [user.email])
            logfile.write(log_message.encode('utf8'))
            print log_message,
            sleep(1)
        logfile.close()
