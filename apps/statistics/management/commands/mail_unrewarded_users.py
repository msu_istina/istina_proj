# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from common.utils.user import get_fullname, get_worker
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from settings import ENV_DIR, DEFAULT_FROM_EMAIL
from os.path import join
from time import sleep
from statistics.utils import get_not_rewarded_authorships
from collections import defaultdict

def cmp_users(user1, user2):
    if get_worker(user1[0]).fullname < get_worker(user2[0]).fullname:
        return -1
    if get_worker(user1[0]).fullname > get_worker(user2[0]).fullname:
        return 1
    return user1[0].id <= user2[0].id


class Command(BaseCommand):
    def handle(self, *args, **options):
        authorships = get_not_rewarded_authorships()
        users = defaultdict(list)
        for authorship in authorships:
            users[authorship.author.user].append(authorship.article)
        users = sorted([(user, articles) for user, articles in users.items()], cmp=cmp_users)
        subject = u"Информация о Ваших отобранных публикациях в системе Наука-МГУ"
        from_email = u"ИСТИНА <info@istina.msu.ru>"
        logfile = open(join(ENV_DIR, 'var', 'log', 'email_unrewarded.log'), 'w')
        for user, articles in users:
            name = get_fullname(user)
            top_articles_link = reverse("statistics_top_articles_by_worker", kwargs={'worker_id': get_worker(user).id})
            message = render_to_string("statistics/unrewarded_user_email.txt",
                {'name': name,
                 'top_articles_link': top_articles_link,
                 'unrewarded_articles': articles,
                 'articles_count': len(articles)})
            #send_mail(subject, message, from_email, [user.email])
            log_message = u"кому: %s\nтема: %s\n\n%s\n\n" % (user.email, subject, message)
            logfile.write(log_message.encode('utf8'))
            print_message = u"%d %s %s %s" % (user.id, user.username, user.email, name)
            print print_message
            sleep(1)
        logfile.close()


