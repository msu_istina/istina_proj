# -*- coding: utf-8; -*-
from actstream import action
from django.core import serializers
from django.db.models.signals import post_save, pre_delete, post_delete
from django.dispatch import receiver

from common.utils.cache import invalidate_activity_type
from statistics.models import ConfirmedArticle

@receiver(post_save, sender=ConfirmedArticle)
@receiver(post_delete, sender=ConfirmedArticle)
def update_authors_cache(sender, **kwargs):
    '''Update corresponding authors' cache after (de)confirmation of an article.'''
    confirmed_article = kwargs.get("instance")
    for worker in confirmed_article.article.authors.all():
        invalidate_activity_type(worker.id, "article") # invalidate article list (on profile page and on latest activities page) cache for authors of the article. See #1021.

@receiver(post_save, sender=ConfirmedArticle)
def log_confirm_article(sender, **kwargs):
    '''Logs article confirmation event.'''
    confirmed_article = kwargs.get("instance")
    description = u"Confirmed article. \nConfirmed article id: %d, \narticle id: %d, \narticle title: '%s'" % (confirmed_article.id, confirmed_article.article.id, confirmed_article.article.title)
    action.send(confirmed_article.user, verb=u'подтвердил статью', action_object=confirmed_article, target=confirmed_article.article, description=description)

@receiver(pre_delete, sender=ConfirmedArticle)
def log_unconfirm_article(sender, **kwargs):
    '''Logs article unconfirmation event.'''
    confirmed_article = kwargs.get("instance")
    user = getattr(confirmed_article, "deleting_user", None)
    if user: # can only log if user is specified
        confirmed_article_data = serializers.serialize("xml", ConfirmedArticle.objects.filter(id=confirmed_article.id)) # if use post_delete, object would not exist at this moment
        description = u"Unconfirmed article. \nConfirmed article id: %d, \narticle id: %d, \narticle title: '%s'. \nConfirmed article data: \n%s" % (confirmed_article.id, confirmed_article.article.id, confirmed_article.article.title, confirmed_article_data)
        action.send(user, verb=u'отменил подтверждение статьи', action_object=confirmed_article, target=confirmed_article.article, description=description)
