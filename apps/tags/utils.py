# -*- coding: utf-8 -*-


def get_all_list_tag_values(instance, tag_type=None):
    try:
        tags = instance.tags.all()
        if tag_type:
            tags = tags.filter(category=tag_type)
    except AttributeError:
        return []
    else:
        return  [tag.list_value for tag in tags if tag.list_value]