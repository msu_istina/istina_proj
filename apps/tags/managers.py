# -*- coding: utf-8 -*-

from common.managers import MyManager


class TagTypeManager(MyManager):

    def get_or_create_by_name(self, name):
        return self.get_or_create(name=name)[0]

    def article_type(self):
        return self.get_or_create_by_name(u'Тип статьи')

    @property
    def confirmation(self):
        return self.get_or_create_by_name(u'Статус проверки')


class ListTagValueManager(MyManager):

    def get_tags_by_category(self, tag_category):
        return self.filter(category=tag_category)

    def get_article_category_tags(self):
        from .models import TagCategory
        article_type_category = TagCategory.objects.article_type()
        return self.get_tags_by_category(article_type_category)

    def get_by_tagcategory_name(self, tagcategory_name):
        from .models import TagCategory
        tagtype = TagCategory.objects.get(tagcategory_name)
        return self.filter(category=tagtype)