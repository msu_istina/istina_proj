# -*- coding: utf-8 -*-

'''
This module implements tagging of some objects in database. At the moment it's possible only to add  tags for articles,
this module should be rewritten using django content type framework to implement possibility to add tags to any object.
Example of using tags can found at  publications.forms:ArticleAdditionalForm, how it looks can be seen at
http://istina.msu.ru/publications/article/4791347/additional/tags/
'''

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db import models
from publications.models import Article
from .managers import ListTagValueManager, TagTypeManager


class TagCategory(models.Model):
    '''
    Category of tag, that unites several possible tags, for example category - 'article tag', possible tags -
    'translation', review'.
    '''
    id = models.AutoField(primary_key=True, db_column="F_TAGTYPEWORK_ID")
    name = models.CharField(max_length=255, db_column="F_TAGTYPEWORK_NAME", verbose_name=u"Название", blank=True)
    category = models.CharField(max_length=30, db_column="F_TAGTYPEWORK_TYPE", verbose_name=u"Тип",  blank=True)

    objects = TagTypeManager()

    class Meta:
        db_table = u'TAGTYPEWORK'
        verbose_name = u"Тип тега"
        verbose_name_plural = u"Типы тегов"

    def __unicode__(self):
        return self.name


class ListTagValue(models.Model):
    '''
    TagCategory (for example 'article tag') can have several tag values, for example 'translation', 'review', etc. Each
    instance of this class contains one value.
    '''
    id = models.AutoField(primary_key=True, db_column="F_TAGTYPELIST_ID")
    category = models.ForeignKey(TagCategory, related_name='list_tag_values', verbose_name=u"Тип", db_column="F_TAGTYPEWORK_ID")
    value = models.CharField(max_length=255, verbose_name=u"Значение", db_column="F_TAGTYPELIST_VALUE")

    objects = ListTagValueManager()

    class Meta:
        db_table = u'TAGTYPELIST'
        verbose_name = u"Значение тега из списка"
        verbose_name_plural = u"Значения тегов из списков"

    def __unicode__(self):
        return self.value


class TagTypeDomain(models.Model):
    '''
    Describes the domain where TagCategory instance can be applied. Is not used anywhere at the moment.
    '''
    id = models.AutoField(primary_key=True, db_column="F_TAGOJBJECT_ID")
    category = models.ForeignKey(TagCategory, related_name='domains', verbose_name=u"Тип", db_column="F_TAGTYPEWORK_ID")
    domain = models.CharField(max_length=255, verbose_name=u"Область применения", db_column="F_TAGOJBJECT_CODE")

    class Meta:
        db_table = u'TAGOJBJECT'
        verbose_name = u"Область применения типа тега"
        verbose_name_plural = u"Области применения типов тегов"

    def __unicode__(self):
        return u"%s : %s" % (self.category, self.domain)


class Tag(models.Model):
    '''
    Class, that connects an object of any class and it's tag. It can have a link to ListTagValue, in that case it will be considered as
    it's value, otherwise it must have non-empty string_value field, that will contain value of the tag.
    '''

    id = models.AutoField(primary_key=True, db_column="F_TAG_ID")
    category = models.ForeignKey(TagCategory, verbose_name=u"Тип тега", related_name='tags', db_column="F_TAGTYPEWORK_ID")
    list_value = models.ForeignKey(ListTagValue, verbose_name=u"Тег из списка", related_name='tags', db_column="F_TAGTYPELIST_ID", blank=True, null=True)
    string_value = models.CharField(max_length=1000, verbose_name=u"Тег-строка", db_column="F_TAG_VALUE", blank=True)
    content_type = models.ForeignKey(ContentType, db_column="f_tag_contenttype_id")
    object_id = models.PositiveIntegerField(db_column="f_tag_object_id")
    content_object = generic.GenericForeignKey()  
    user = models.ForeignKey('auth.User', verbose_name=u"Пользователь", db_column="F_TAGARTICLE_USER", blank=True, null=True)

    class Meta:
        db_table = u'TAG'
        verbose_name = u"Тег объекта"
        verbose_name_plural = u"Теги объектов"

    def __unicode__(self):
        if self.list_value:
            value = self.list_value
        else:
            value = self.string_value
        return u"%s, объект %s" % (value, self.content_object)


class ArticleTag(models.Model):
    '''
    Class, that connects article and it's tag. It can have a link to ListTagValue, in that case it will be considered as
    it's value, otherwise it must have non-empty string_value field, that will contain value of the tag.
    '''

    id = models.AutoField(primary_key=True, db_column="F_TAGARTICLE_ID")
    category = models.ForeignKey(TagCategory, related_name='article_tags', verbose_name=u"Тип", db_column="F_TAGTYPEWORK_ID")
    list_value = models.ForeignKey(ListTagValue, verbose_name=u"Тег", related_name='article_tags', null=True, blank=True, db_column="F_TAGTYPELIST_ID")
    article = models.ForeignKey(Article, related_name='tags', verbose_name=u"Статья", db_column="F_ARTICLE_ID")
    string_value = models.CharField(max_length=1000, verbose_name=u"Значение", blank=True, db_column="F_TAGARTICLE_VALUE")
    user = models.ForeignKey('auth.User', null=True, blank=True, verbose_name=u"Пользователь", db_column="F_TAGARTICLE_USER")

    class Meta:
        db_table = u'TAGARTICLE'
        verbose_name = u"Тег статьи"
        verbose_name_plural = u"Теги статей"

    def __unicode__(self):
        if self.list_value:
            value = self.list_value
        else:
            value = self.string_value
        return u"%s, статья %s" % (value, self.article)


from tags.admin import *