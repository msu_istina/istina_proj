from common.utils.admin import register_with_versions
from models import ListTagValue, TagCategory, TagTypeDomain, Tag, ArticleTag

register_with_versions(ListTagValue)
register_with_versions(TagCategory)
register_with_versions(TagTypeDomain)
register_with_versions(Tag)
register_with_versions(ArticleTag)
