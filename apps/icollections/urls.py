from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_detail

from common.views import autocomplete_search
from icollections.models import Collection, CollectionEditorship, Series
from icollections.views import CollectionEditorshipWizardView as Wizard
from icollections.views import edit_collection, edit_marks
from publications.views import object_publications_in_style

# collections
urlpatterns = patterns('icollections.views',
    url(r'^(?P<object_id>\d+)/$', object_detail, {'queryset': Collection.objects.all(), 'template_object_name': 'collection'}, name='icollections_collection_detail'),
    url(r'^(?P<object_id>\d+)/edit/$', edit_collection, name='icollections_edit_collection'),
    url(r'^(?P<object_id>\d+)/marks/$', edit_marks, name='icollections_edit_marks'),
    url(r'^(?P<object_id>\d+)/style/(?P<style>.+)/$', object_publications_in_style, {'model': Collection, 'template_name': 'icollections/collection_detail.html', 'template_object_name': 'collection'}, name='icollections_collection_detail_in_style'),
    url(r'^(?P<object_id>\d+)/articles.bib$', object_detail, {'queryset': Collection.objects.all(), 'template_name': 'publications/publication_list.bib', 'mimetype': 'text/plain'}, name='icollections_collection_articles_bibtex'),
    url(r'^search/$', "autocomplete_search_collections", name='icollections_collection_search'),
    url(r'^search/simple/$', autocomplete_search, {'model': Collection}, name='icollections_collection_search_simple'),
    url(r'^series/(?P<object_id>\d+)/$', object_detail, {'queryset': Series.objects.all(), 'template_object_name': 'series'}, name='icollections_series_detail'),
    url(r'^series/search/$', autocomplete_search, {'model': Series}, name='icollections_series_search'),
)

# editorships of collections
editorship_options_base = {'model': CollectionEditorship}
editorship_options_detail = dict(editorship_options_base.items() + [('template_name', 'icollections/editorship_detail.html')])

urlpatterns += patterns('common.views',
    url(r'^editorships/add/$', Wizard.as_view(), name='icollections_editorship_add'),
    url(r'^editorships/(?P<object_id>\d+)/$', 'detail', editorship_options_detail, name='icollections_editorship_detail'),
    url(r'^editorships/(?P<object_id>\d+)/edit/$', Wizard.as_view(), name='icollections_editorship_edit'),
    url(r'^editorships/(?P<object_id>\d+)/delete/$', 'delete', editorship_options_base, name='icollections_editorship_delete'),
)
