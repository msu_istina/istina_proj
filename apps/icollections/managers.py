# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from actstream import action
from common.managers import SimilarFieldManager
from common.utils.dict import filter_true
from common.utils.idifflib import get_close_matches
from common.models import merge_two_objects


class CollectionManager(SimilarFieldManager):
    '''Model manager for Collection model.'''

    def get_similar(self, title, **kwargs):
        qs = self
        # filter out kwargs with False value
        kwargs = filter_true(kwargs)
        if kwargs:
            qs = qs.filter(**kwargs)
        candidates = self.get_similar_by_field('title', title, cutoff=.5, n=10, qs=qs)

        stoplist = frozenset((u"proceedings", u"of", u"the", u"international", u"conference", u"on"))

        def prepare(value):
            return " ".join(filter(lambda word: word not in stoplist, value.lower().split())).strip()

        def get_value(obj):
            return prepare(obj.title)

        candidates = get_close_matches(prepare(title), candidates, n=10, cutoff=.7, key=get_value)
        candidates = self.filter(pk__in=map(lambda x: x[0].id, candidates))
        return candidates

    def merge(self, username, from_ids, to_id):
        '''Merge collection with from_ids to collection with to_id.

        from_ids can be an integer, or a list of integers.'''

        # user exists
        user = User.objects.get(username=username)
        if not user.is_superuser:
            raise Exception("user must be superuser")

        # to collection must exist
        c_to = self.get(id=to_id)

        if isinstance(from_ids, int):
            from_ids = (from_ids,)

        for from_id in from_ids:
            c_from = None
            try:
                # from collection may not exist
                c_from = self.get(id=from_id)
            except models.ObjectDoesNotExist:
                # skip missing collections
                continue


            # move related objects
            c_from.editorships.all().update(collection=c_to) # collection editorships
            c_from.articles.all().update(collection=c_to) # articles in collection
            c_from.category_memberships.all().update(collection=c_to)

            # Collection fields
            # if some fields of c_to are empty, move them from c_from
            for field in ["year", "series", "volume", "publisher", "location", "theses", "abstract", "xml"]:
                if getattr(c_from, field) and not getattr(c_to, field):
                    setattr(c_to, field, getattr(c_from, field))
            c_to.save()

            # Copy all other fields, if any
            merge_two_objects(object_from=c_from, object_to=c_to)
 
            # log action
            description = u"Удаленный сборник: %s (id %d). Его данные: \n%s" % (c_from.title, c_from.id, c_from.get_xml().decode('utf8'))
            action.send(user, verb=u'объединил сборники', action_object=c_from, target=c_to, description=description)

            # delete c_from
            c_from.delete()
        return None

class SeriesManager(SimilarFieldManager):
    '''Model manager for Series model.'''

    def get_similar(self, title):
        '''Returns a queryset.'''
        return self.get_similar_by_field('title', title, cutoff=0.5, n=10)
