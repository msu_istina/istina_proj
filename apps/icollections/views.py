# -*- coding: utf-8; -*-
from django.http import HttpResponse
from django.utils import simplejson
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages

import logging

from common.utils.autocomplete import autocomplete_helper
from common.utils.uniqify import uniqify
from common.views import LinkedToWorkersModelWizardView
from icollections.forms import CollectionEditorshipForm, CollectionEditorshipSimilarCollectionsForm, CollectionEditorshipNewCollectionForm, CollectionEditorshipSimilarSeriesPublisherForm
from icollections.forms import CollectionForm, CollectionStaffForm
from icollections.models import Collection, CollectionEditorship, Series, CollectionMark
from icollections.models import CollectionCategoryMembership, CollectionCategory
from publishers.models import Publisher
from workers.forms import WorkerFormSet
from workers.models import Worker

from unified_permissions import has_permission

logger = logging.getLogger("icollections.views")


class CollectionEditorshipWizardView(LinkedToWorkersModelWizardView):
    form_list = [
        (LinkedToWorkersModelWizardView.main_step, CollectionEditorshipForm),
        ('similar_collections', CollectionEditorshipSimilarCollectionsForm),
        ('new_collection', CollectionEditorshipNewCollectionForm),
        ('similar_series_publishers', CollectionEditorshipSimilarSeriesPublisherForm),
        (LinkedToWorkersModelWizardView.workers_steps[0], WorkerFormSet)
    ]
    condition_dict = LinkedToWorkersModelWizardView.make_condition_dict(
        'similar_collections', 'new_collection', 'similar_series_publishers')
    model = CollectionEditorship  # used in templates

    def get_form_initial(self, step):
        """
        Returns a dictionary which will be passed to the form for `step`
        as `initial`. If no initial data was provied while initializing the
        form wizard, a empty dictionary will be returned.
        """
        initial = super(CollectionEditorshipWizardView, self).get_form_initial(step)
        if step == 'new_collection':
            initial['title'], initial['volume'], initial['year'] = self.get_extra_data('collection_str', 'volume', 'year')
        return initial

    def get_form(self, step=None, data=None, files=None):
        form = super(CollectionEditorshipWizardView, self).get_form(step, data, files)
        if not step:
            step = self.steps.current
        if step == 'similar_collections':
            # the real step can be 5, but this function can be called to render previous forms
            # get the similar collections basing on collection_str entered at 'main' step.
            # if this is really step 2, and it's not skipped, similar collections should be calculated (again)
            similar_collections, collection_str, volume, year = self.get_extra_data('similar_collections', 'collection_str', 'volume', 'year')
            if not similar_collections:
                if self.steps.current == step: # this is really step 2
                    similar_collections = Collection.objects.get_similar(collection_str, volume=volume, year=year)
                    # store similar collections not to calculate them twice (on step 2 and on step 3).
                    self.set_extra_data('similar_collections', similar_collections)
                else: # this is just a previous form for step 5
                    similar_collections = Collection.objects.none()
            form.fields['collection'].queryset = similar_collections
            form.fields['collection'].empty_label = (collection_str or "") + u" (добавить новый сборник)"
        elif step == 'similar_series_publishers':
            # the real step can be 5, but this function can be called to render previous forms
            # get the similar series basing on series_str entered at 'main' step.
            # if this is really step 4, and it's not skipped, similar series should be calculated (again)
            similar_series, series_str = self.get_extra_data('similar_series', 'series_str')
            if not similar_series:
                if self.steps.current == step: # this is really step 4
                    similar_series = Series.objects.get_similar(series_str)
                    # store similar series not to calculate them twice (on step 4 and on step 5).
                    self.set_extra_data('similar_series', similar_series)
                else: # this is just a previous form for step 5
                    similar_series = Series.objects.none()
            form.fields['series'].queryset = similar_series
            form.fields['series'].empty_label = (series_str or "") + u" (добавить новую серию)"

            similar_publishers, publisher_str, location = self.get_extra_data('similar_publishers', 'publisher_str', 'location')
            if not similar_publishers:
                if self.steps.current == step: # this is really step 4
                    similar_publishers = Publisher.objects.get_similar(publisher_str, location, return_queryset=True)
                    # store similar publishers not to calculate them twice (on step 4 and on step 5).
                    self.set_extra_data('similar_publishers', similar_publishers)
                else: # this is just a previous form for step 5
                    similar_publishers = Publisher.objects.none()
            form.fields['publisher'].queryset = similar_publishers
            form.fields['publisher'].empty_label = publisher_str + ", " + location + u" (найденные издательства не подходят)"
        return form

    def get_context_data(self, form, **kwargs):
        context = super(CollectionEditorshipWizardView, self).get_context_data(form, **kwargs)
        forms = context['previous_forms']
        self.hide_form_fields(forms, 'main', 'collection_str', 'similar_collections')
        self.hide_form_fields(forms, 'main', ('collection_str', 'volume', 'year'), ('new_collection', 'similar_series_publishers'))
        self.hide_form_fields(forms, 'similar_collections', 'collection', ('new_collection', 'similar_series_publishers'))
        self.hide_form_fields(forms, 'new_collection', ('series_str', 'publisher_str'), 'similar_series_publishers')
        if 'similar_collections' in self.steps.all or 'new_collection' in self.steps.all:
            self.hide_form_fields(forms, 'main', ('collection_str', 'volume', 'year'), self.workers_steps[0])
        if 'new_collection' in self.steps.all:
            self.hide_form_fields(forms, 'similar_collections', 'collection', self.workers_steps[0])
        if 'similar_series_publishers' in self.steps.all:
            self.hide_form_fields(forms, 'new_collection', ('series_str', 'publisher_str'), self.workers_steps[0])
        return context

    def process_step(self, form):
        collection = None
        if self.steps.current == self.main_step:
            collection_str = form.cleaned_data['collection_str']
            volume = form.cleaned_data['volume']
            year = form.cleaned_data['year']
            old_collection_str, old_volume, old_year = self.get_extra_data('collection_str', 'volume', 'year')
            if (collection_str, volume, year) != (old_collection_str, old_volume, old_year) : # some parameters have changed
                self.set_extra_data(('collection_str', collection_str), ('volume', volume), ('year', year)) # for later use in creating similar collections form
                self.delete_extra_data('similar_collections') # now they should be calculated again
                try:
                    # try to select collection by name
                    collection = Collection.objects.get(title=collection_str, year=year, volume=volume)
                except:
                    # calculate similar collections
                    similar_collections = Collection.objects.get_similar(collection_str, year=year, volume=volume)
                    # store similar collections not to calculate them twice (on step 2 and on step 3).
                    self.set_extra_data('similar_collections', similar_collections)
                    self.skip_or_include_steps('similar_collections', not similar_collections)
                    if not similar_collections:
                        self.include_steps('new_collection')
                else: # collection selected by name, go to the last step
                    self.skip_steps('similar_collections', 'new_collection', 'similar_series_publishers')
        elif self.steps.current == 'similar_collections':
            collection = form.cleaned_data['collection']
            self.skip_or_include_steps('new_collection', isinstance(collection, Collection))
        if isinstance(collection, Collection): # collection found by name or similar collection selected
            self.set_extra_data('collection_instance', collection)
            self.skip_steps('similar_series_publishers')

        series = None
        publisher = None
        similar_series = None
        similar_publishers = None
        if self.steps.current == 'new_collection':
            old_series_str, similar_series, similar_publishers = self.get_extra_data('series_str', 'similar_series', 'similar_publishers')
            series_str = form.cleaned_data['series_str']
            if series_str != old_series_str: # series_str has changed
                self.set_extra_data('series_str', series_str) # for later use in creating similar series form
                self.delete_extra_data('similar_series') # now they should be calculated again
                try:
                    # try to select series by name
                    series = Series.objects.get(title=series_str)
                except:
                    # calculate similar series
                    similar_series = Series.objects.get_similar(series_str)
                    # store similar series not to calculate them twice (on step 2 and on step 3).
                    self.set_extra_data('similar_series', similar_series)
            publisher_str = form.cleaned_data['publisher_str']
            location = form.cleaned_data['location']
            old_publisher_str, old_location = self.get_extra_data('publisher_str', 'location')
            if (publisher_str, location) != (old_publisher_str, old_location) : # publisher_str or location has changed
                self.set_extra_data(('publisher_str', publisher_str), ('location', location)) # for later use in creating similar publishers form
                self.delete_extra_data('similar_publishers') # now they should be calculated again
                try:
                    # try to select publishers by name
                    publisher = Publisher.objects.get(name=publisher_str)
                except:
                    try:
                        publisher = Publisher.objects.get(name=publisher_str, city=location)
                    except:
                        # calculate similar publishers
                        similar_publishers = Publisher.objects.get_similar(publisher_str, location, return_queryset=True)
                        # store similar publishers not to calculate them twice (on step 2 and on step 3).
                        self.set_extra_data('similar_publishers', similar_publishers)
            if (series_str, publisher_str, location) != (old_series_str, old_publisher_str, old_location):
                # update step skipping
                self.skip_or_include_steps('similar_series_publishers', (series or not similar_series) and (publisher or not similar_publishers))

        elif self.steps.current == 'similar_series_publishers':
            series = form.cleaned_data['series']
            publisher = form.cleaned_data['publisher']
        if isinstance(series, Series): # series found by title or similar series selected
            self.set_extra_data('series_instance', series)
        if isinstance(publisher, Publisher): # publisher found by name or similar publisher selected
            self.set_extra_data('publisher_instance', publisher)

        return super(CollectionEditorshipWizardView, self).process_step(form)

    def save_object(self, form_list):
        '''This function is executed during done() method, and used to save main form object.
        Workers objects has been already saved.
        '''
        if 'new_collection' in self.steps.all: # new collection has been added
            series, publisher = self.get_extra_data(('series_instance', Series), ('publisher_instance', Publisher)) # this is a series found by title or a selected similar series, this is a publisher found by name or a selected similar publisher
            new_collection_form = form_list[self.get_step_index('new_collection')]
            collection = new_collection_form.save(self.request, series, publisher) # save a new collection
        else: # collection could be found by title or selected among similar collections
            collection = self.get_extra_data('collection_instance', Collection) # this is a collection found by title or a selected similar collection
        return super(CollectionEditorshipWizardView, self).save_object(form_list, collection)


def autocomplete_search_collections(request):
    '''Server side for jquery autocomplete
    '''
    term = request.GET.get('term', '')
    fields = []
    limit = 10
    if len(term) >= 2:
        items = autocomplete_helper(Collection, "title", term, limit)
        def extract_info(obj):
            base_fields = [i.attname for i in obj._meta.fields]
            info = dict((i, getattr(obj, i)) for i in base_fields)
            if obj.publisher:
                info["publisher_name"] = obj.publisher.name
                info["publisher_city"] = obj.publisher.city
            if obj.series:
                info["series_title"] = obj.series.title
            info["label"] = unicode(obj)
            return info
        fields = map(extract_info, items)
        fields = uniqify(fields, lambda i: i['label'])
    return HttpResponse(simplejson.dumps(fields), content_type='application/javascript')


def autocomplete_search_editors(request):
    '''Server side for jquery autocomplete
    '''
    term = request.GET.get('term', '')
    fields = []
    limit = 10
    if len(term) >= 2:
        items = autocomplete_helper(CollectionEditorship, "worker__lastname", term, limit)
        fields = [{'label': item.worker.fullname, 'value': item.worker.fullname_short} for item in items]
        fields = uniqify(fields, lambda i: i['label'])
    return HttpResponse(simplejson.dumps(fields), content_type='application/javascript')


@login_required
def edit_collection(request, object_id):    
    collection = Collection.objects.get(pk=object_id)

    staff_form = None
    main_data_form = None

    if not has_permission(request.user, 'is_representative_for'):
        messages.error(request, u'Извините, в настоящее время только ответственные могут редактировать сборники.')
        return redirect('icollections_collection_detail', object_id)
    
    if request.method == 'POST':
        if 'save_collection' in request.POST:
            main_data_form = CollectionForm(request.POST, instance=collection)
            if main_data_form.is_valid():
                col = main_data_form.save()
                CollectionCategoryMembership.objects.filter(collection=collection).delete()
                for cat_id in request.POST.getlist('cats', []):
                    cat = CollectionCategory.objects.get(pk=cat_id)
                    CollectionCategoryMembership.objects.create(
                        collection=collection,
                        category=cat,
                        creator=request.user)
                messages.success(request, "Сохранено")
        if 'save_staff' in request.POST:
            staff_form = CollectionStaffForm(request.POST)
            if staff_form.is_valid():
                editorship = staff_form.save(commit=False)
                editorship.collection = collection
                editorship.creator = request.user
                editorship.save()
                messages.success(request, "Сохранено")
        if 'delete_staff' in request.POST:
            record_id = int(request.POST.get('record_id', 0))
            CollectionEditorship.objects.filter(collection=collection, pk=record_id).delete()
            messages.success(request, "Сохранено")
    staff_form = staff_form or CollectionStaffForm()
    main_data_form = main_data_form or CollectionForm(instance=collection)
    return render(request, "icollections/edit_collection.html",
                  {"collection": collection,
                   "main_data_form": main_data_form,
                   "staff_form": staff_form,                   
                   })

@login_required
def edit_marks(request, object_id):    
    collection = Collection.objects.get(pk=object_id)
    if not collection.marks.exists():
        mark = CollectionMark.objects.create(
            collection=collection,
            marktype='selected',
            creator=request.user)
    else:
        CollectionMark.objects.filter(collection=collection, creator=request.user).delete()
    return redirect('icollections_edit_collection', object_id)
