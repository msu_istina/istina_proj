# -*- coding: utf-8 -*-
from django.db import models
import logging

from common.models import LinkedToWorkersModel, WorkershipModel, MyModel, Activity
from managers import *

logger = logging.getLogger("icollections.models")


class Collection(LinkedToWorkersModel):
    id = models.AutoField(primary_key=True, db_column="F_COLLECTION_ID")
    title = models.CharField(u"Название", max_length=255, db_column="F_COLLECTION_NAME")
    editors = models.ManyToManyField("workers.Worker", related_name="collections_edited", through="CollectionEditorship")
    year = models.IntegerField(u"Год издания", db_column="F_COLLECTION_YEAR", null=True, blank=True)
    series = models.ForeignKey(to="Series", related_name="collections", db_column="F_SERIA_ID", null=True, blank=True)
    volume = models.IntegerField(u"Том", db_column="F_COLLECTION_VAL", null=True, blank=True)
    publisher = models.ForeignKey(to="publishers.Publisher", related_name="collections", db_column="F_PUBLISHING_ID", null=True, blank=True)
    location = models.CharField(u"Место издания", max_length=255, db_column="F_COLLECTION_PLACE", null=True, blank=True)
    theses = models.NullBooleanField(u"Тезисы", db_column="F_COLLECTION_TESIS", blank=True)
    categories = models.ManyToManyField("CollectionCategory", related_name="collections", through="CollectionCategoryMembership")
    abstract = models.TextField(u"Аннотация сборника", db_column="F_COLLECTION_ABSTRACT", blank=True)
    xml = models.TextField(u"XML", db_column="F_COLLECTION_XML", null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="collections_added", db_column="F_COLLECTION_USER", null=True, blank=True)

    objects = CollectionManager()

    workers_attr = 'editors'
    workerships_attr = 'editorships'
    workers_verbose_name_single = u"Редактор"
    workers_verbose_name_plural = u"Редакторы"
    workers_required_error_msg = u"Укажите хотя бы одного редактора"

    nominative_en = "collection"
    instrumental = u"сборником"

    class Meta:
        db_table = "COLLECTION"
        verbose_name = u"сборник"
        verbose_name_plural = u"сборники"
        unique_together = (("title", "year", "series", "volume", "location", "theses",))
        ordering = ('-year', '-volume', 'title')

    def __unicode__(self):
        extra = (
            self.year and (u"%s год" % self.year),
            self.series and (u"серия %s" % self.series.title),
            self.volume and (u"том %s" % self.volume),
            self.location,
            self.theses and u"тезисы"
            )
        extra = u", ".join(filter(bool, extra))
        return u"%s (%s)" % (self.title, extra)

    @models.permalink
    def get_absolute_url(self):
        return ('icollections_collection_detail', (), {'object_id': self.id})

    @property
    def published_in(self):
        return getattr(self.publisher, "short_info", None) or self.location

    @property
    def bib_editors(self):
        return " and ".join(editor.fullname_bib for editor in self.editors.all())

    def update_categories(self, categories, user=None):
        '''Adds collection in specified categories. Categories is a list of tuples (category, include).
           if include is True, add category, otherwise remove.
           @user is the user adding/removing categories.'''
        for category, include in categories:
            if include:
                membership, created = CollectionCategoryMembership.objects.get_or_create(collection=self, category=category)
                if user and created:
                    membership.creator = user
                    membership.save()
            else:
                CollectionCategoryMembership.objects.filter(collection=self, category=category).delete()

    # function that suppost merging wizard
    def datatable_merge_json(self):
        data = dict(enumerate([self.get_absolute_url_html(),
                               self.title,
                               self.year,
                               self.publisher,
                               self.articles.count(),
                               self.editorships.count()]))
        data['DT_RowId'] = self.id
        return data

    @staticmethod
    def get_datatable_merge_column_names():
        return ("ID",
                u"Название",
                u"Год",
                u"Издательство",
                u"Кол-во статей",
                u"Кол-во редакторов")




COLLECTION_EDITORSHIP_ROLES = (
        ("member", u"член редколлегии"),
        ("chief", u"главный редактор"),
        ("science_editor", u"научный редактор"),
        ("compiler", u"составитель"),
        ("commentator", u"комментарии"),
        ("forewords", u"автор предисловия"),
        ("corrector", u"корректор"),
    )

class CollectionEditorship(WorkershipModel, Activity):
    id = models.AutoField(primary_key=True, db_column="F_CORRECTOR_ID")
    worker = models.ForeignKey(to="workers.Worker", related_name="collection_editorships", db_column="F_MAN_ID", null=True, blank=True)
    collection = models.ForeignKey(to="Collection", related_name="editorships", db_column="F_COLLECTION_ID")
    role = models.CharField(u"Тип участия", max_length=255, choices=COLLECTION_EDITORSHIP_ROLES, default="member", db_column="F_CORRECTOR_ROLE")
    original_name = models.CharField(max_length=255, db_column="F_CORRECTOR_NAME", blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="collection_editorships_added", db_column="F_CORRECTOR_USER", null=True, blank=True)

    worker_attr = "worker"
    workers_verbose_name_single = u"ФИО члена редколлегии"
    workers_required_error_msg = u"Укажите члена редколлегии сборника"

    nominative_en = "collection_editorship"
    nominative_short = u"членство в редколлегии"
    nominative_plural = u"членство в редколлегиях сборников"
    genitive = u"членства в редколлегии сборника"
    genitive_short = u"членства в редколлегии"
    genitive_plural = u"членства в редколлегиях сборников"
    genitive_plural_full = u"членств в редколлегиях сборников"
    accusative_short = u"членство в редколлегии"
    instrumental = u"членством в редколлегии сборника"
    locative = u"членстве в редколлегии сборника"
    gender = "neuter"

    class Meta:
        db_table = "CORRECTOR"
        verbose_name = u"членство в редколлегии сборника"
        verbose_name_plural = u"членства в редколлегиях сборников"
        ordering = ('-collection__year', '-collection__volume', 'collection__title')

    def __unicode__(self):
        return u"Членство в редколлегии сборника {0}, {1}".format(
            unicode(self.collection), self.workers_string)

    @models.permalink
    def get_absolute_url(self):
        return ('icollections_editorship_detail', (), {'object_id': self.id})

    @property
    def title(self):
        return u"%s в редколлегии сборника %s" % (self.workers_string, self.collection)

    @property
    def role_name(self):
        for k, v in COLLECTION_EDITORSHIP_ROLES:
            if k == self.role:
                return v
        return None

class CollectionMark(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_COLLECTIONMARK_ID")
    collection = models.ForeignKey(to="Collection", related_name="marks", db_column="F_COLLECTION_ID")
    department = models.ForeignKey(to="organizations.Department", related_name="collection_marks", db_column="F_DEPARTMENT_ID")
    marktype = models.CharField(u"Тип метки", max_length=255, db_column="F_COLLECTIONMARK_TYPE")
    creator = models.ForeignKey(to="auth.User", related_name="collection_marks_added", db_column="F_COLLECTIONMARK_USER", null=True, blank=True)

    class Meta:
        db_table = "COLLECTIONMARK"
        verbose_name = u"метка сборников"
        verbose_name_plural = u"метки сборников"

    def __unicode__(self):
        return u"Метка '%s' для сборника '%s'" % (self.marktype, self.collection.title)


class Series(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SERIA_ID")
    title = models.CharField(u"Название", max_length=255, unique=True, db_column="F_SERIA_NAME")
    abstract = models.TextField(u"Аннотация серии", db_column="F_SERIA_ABSTRACT", blank=True)
    xml = models.TextField(u"XML журнала", db_column="F_SERIA_XML", null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="series_added", db_column="F_SERIA_USER", null=True, blank=True)

    objects = SeriesManager()

    class Meta:
        db_table = "SERIA"
        verbose_name = u"серия сборников"
        verbose_name_plural = u"серии сборников"
        ordering = ('title',)

    def __unicode__(self):
        return u"%s" % self.title

    @models.permalink
    def get_absolute_url(self):
        return ('icollections_series_detail', (), {'object_id': self.id})


class CollectionCategory(MyModel):
    '''A category of collections having common properties.'''
    id = models.AutoField(primary_key=True, db_column="F_COLLECTIONGROUPTYPE_ID")
    name = models.CharField(u"название", max_length=255, db_column="F_COLLECTIONGROUPTYPE_NAME")
    code = models.CharField(u"кодовое название", max_length=255, db_column="F_COLLECTIONGROUPTYPE_CODE", blank=True)
    article_name = models.CharField(u"название типа статьи", max_length=255, db_column="F_COLLECTIONGROUPTYPE_ARTICLEN")

    class Meta:
        db_table = "COLLECTIONGROUPTYPE"
        verbose_name = u"Категория сборников"
        verbose_name_plural = u"Категории сборников"

    def __unicode__(self):
        return self.name


class CollectionCategoryMembership(MyModel):
    '''A membership of a collection in a category.'''
    id = models.AutoField(primary_key=True, db_column="F_COLLECTIONGROUP_ID")
    collection = models.ForeignKey(to=Collection, verbose_name=u"сборник", related_name="category_memberships", db_column="F_COLLECTION_ID")
    category = models.ForeignKey(to=CollectionCategory, verbose_name=u"категория", related_name="memberships", db_column="F_COLLECTIONGROUPTYPE_ID")
    creator = models.ForeignKey(to=User, verbose_name=u"создатель", related_name="collection_category_memberships_added", db_column="F_COLLECTIONGROUP_USER", null=True, blank=True)

    class Meta:
        db_table = "COLLECTIONGROUP"
        verbose_name = u"Членство в категории сборника"
        verbose_name_plural = u"Членства в категориях сборников"

    def __unicode__(self):
        return u"сборник '%s' в категории '%s'" % (self.collection.title, self.category.name)


from icollections.admin import *
