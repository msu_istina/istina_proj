# -*- coding: utf-8; -*-
from actstream import action
from django import forms
from django.core import serializers

from common.forms import WorkershipModelForm, MyForm, MyModelForm, YearField
from icollections.models import Collection, CollectionEditorship, Series, CollectionCategory
from icollections.models import CollectionCategoryMembership
from publishers.models import Publisher

from common.forms import Select2ModelField, Select2SingleWorkerField
from common.widgets import MyFilteredSelectMultiple
from workers.models import Worker

class SeriaSelect2Field(Select2ModelField):
    def __init__(self, *args, **kwargs):
        super(SeriaSelect2Field, self).__init__(self, *args, add_option=False, model=Series, **kwargs)
        self.widget.options['placeholder'] = u'Введите название серии'
    def get_description(self, obj):
        return u''

class CollectionForm(forms.ModelForm):            
    series = SeriaSelect2Field(label='Серия', queryset=Series.objects.all(), required=False)
    cats = forms.ModelMultipleChoiceField(label=u"Тип сборника",
                                          widget=MyFilteredSelectMultiple(
                                              u"типы сборников", is_stacked=False),
                                          queryset=CollectionCategory.objects.none(),
                                          required=False)

    css_classes = [('title', 'wide'), ('series', 'wide')]

    class Meta:
        fields = ('title', 'volume', 'year', 'series')
        model = Collection

    def __init__(self, *args, **kwargs):
        super(CollectionForm, self).__init__(*args, **kwargs)
        cats_queryset = CollectionCategory.objects.all()
        instance_cats = [x.category for x in CollectionCategoryMembership.objects.filter(collection=self.instance)]
        self.fields['cats'].queryset=cats_queryset
        self.fields['cats'].initial = [x for x in cats_queryset if x in instance_cats]


class StaffSelect2Field(Select2SingleWorkerField):
    def __init__(self, *args, **kwargs):
        super(StaffSelect2Field, self).__init__(self, *args, add_option=False, model=Worker, **kwargs)
        self.widget.options['placeholder'] = u'Введите фамилию'

class CollectionStaffForm(forms.ModelForm):
    class Meta:
        model = CollectionEditorship
        fields = ('worker', 'role',)

    worker = StaffSelect2Field(label=u'Поиск сотрудников',
                               queryset=Worker.objects.all(),
                               required=True)


class CollectionEditorshipForm(WorkershipModelForm):
    '''A simple form for collection editorial board membership.'''
    collection_str = forms.CharField(label=u"Сборник", max_length=255)
    volume = forms.IntegerField(label=u"Том", required=False)
    year = YearField(label=u"Год издания", required=False)
    fields_order = ["worker_str", "collection_str", "role", "volume", "year"]
    css_classes = [('collection_str', 'wide autocomplete_collection'), ('volume', 'narrow')]

    class Meta:
        model = CollectionEditorship
        exclude = ('worker', 'collection', 'creator')

    def __init__(self, *args, **kwargs):
        super(CollectionEditorshipForm, self).__init__(*args, **kwargs)
        if self.instance_linked(): # initialize collection_str field
            self.fields['collection_str'].initial = self.instance.collection.title
            self.fields['volume'].initial = self.instance.collection.volume
            self.fields['year'].initial = self.instance.collection.year
        self.fields['role'].choices = CollectionEditorship.get_choices("collection_editorship_roles")

    def clean_worker_str(self):
        # only one worker is allowed
        value = self.cleaned_data['worker_str'].strip(", ")
        if "," in value:
            raise forms.ValidationError(u"Пожалуйста, укажите ФИО только одного члена редколлегии. В строке не должно быть запятых.")
        return value

    def save(self, request, workers, collection, commit=True):
        # collection must exist now
        editorship = super(CollectionEditorshipForm, self).save(request, commit=False)
        editorship.collection = collection
        if commit:
            self.commit_save(request, editorship, workers)
        return editorship


class CollectionEditorshipSimilarCollectionsForm(MyForm):
    collection = forms.ModelChoiceField(label=u"Сборник", queryset=Collection.objects.all(), empty_label="добавить новый сборник", required=False)


class CollectionEditorshipNewCollectionForm(MyModelForm):
    series_str = forms.CharField(label=u"Серия", max_length=255, required=False)
    publisher_str = forms.CharField(label=u"Издательство", max_length=255)
    categories = forms.ModelMultipleChoiceField(label=u"Категории", queryset=CollectionCategory.objects.all(), widget=forms.CheckboxSelectMultiple, required=False)
    fields_order = ['title', 'volume', 'year', 'series_str', 'publisher_str', 'location', 'theses', 'abstract']
    css_classes = [('volume', 'narrow'), ('series_str', 'wide autocomplete_series'), ('publisher_str', 'wide autocomplete_publisher'), ('location', 'wide')]

    class Meta:
        model = Collection
        exclude = ('editors', 'series', 'publisher', 'xml', 'creator')

    def save(self, request, series, publisher, commit=True):
        collection = super(CollectionEditorshipNewCollectionForm, self).save(request, commit=False)
        series_created = False
        # link to series
        if not series and self.cleaned_data['series_str']: # create a new series if series name is specified
            properties = {
                'title': self.cleaned_data['series_str'],
                'creator': request.user
            }
            series = Series(**properties)
            series.xml = series.get_xml()
            series.save()
            series_created = True
        collection.series = series

        # link to publisher or location
        if publisher:
            collection.publisher = publisher
        else: # fill location
            delimiter = ', ' if self.cleaned_data['publisher_str'] and self.cleaned_data['location'] else ''
            collection.location = "%s%s%s" % (self.cleaned_data['publisher_str'], delimiter, self.cleaned_data['location'])

        if commit:
            self.commit_save(request, collection)
            if series_created:
                action.send(request.user, verb=u'добавил серию', action_object=series, target=collection)
            selected_categories = self.cleaned_data.get('categories', [])
            categories = [(category, bool(category in selected_categories)) for category in CollectionCategory.objects.all()]
            collection.update_categories(categories, request.user)
        return collection


class CollectionEditorshipSimilarSeriesPublisherForm(MyForm):
    series = forms.ModelChoiceField(label=u"Серия", queryset=Series.objects.all(), empty_label="добавить новую серию", required=False)
    publisher = forms.ModelChoiceField(label=u"Издательство", queryset=Publisher.objects.all(), required=False)
