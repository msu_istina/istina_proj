from common.utils.admin import register_with_versions
from models import Publisher

register_with_versions(Publisher)
