# -*- coding: utf-8 -*-
from django.db import models
import logging

from common.models import MyModel
from managers import PublisherManager

logger = logging.getLogger("publishers.models")


class Publisher(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_PUBLISHING_ID")
    name = models.CharField(u"Имя", max_length=255, db_column="F_PUBLISHING_NAME")
    city = models.CharField(u"Город", max_length=255, db_column="F_PUBLISHING_TOWN")

    objects = PublisherManager()

    search_attr = 'name'

    class Meta:
        db_table = "PUBLISHING"
        verbose_name = u"Издательство"
        verbose_name_plural = u"Издательства"
        unique_together = (("name", "city",))
        ordering = ('name', 'city')

    def __unicode__(self):
        return "'%s' (%s)" % (self.name, self.city)

    @models.permalink
    def get_absolute_url(self):
        return ('publishers_detail', (), {'object_id': self.id})

    @property
    def activities(self):
        ''' Returns full list of works published by the publisher.
        '''
        data = {
            'journals': self.journals.all(),
            'collections': self.collections.all(),
            'books': self.books.all()
        }
        # return non-empty keys
        return dict((k, v) for k, v in data.items() if v)

    def short_info(self):
        '''Returns short info about the publisher.'''
        s = unicode(self.name) if self.name else ""
        s += ", " + unicode(self.city) if self.city else ""
        return s

from publishers.admin import *