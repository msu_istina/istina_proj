# -*- coding: utf-8 -*-
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.core.cache import get_cache
import logging
import operator

from common.managers import MyManager, get_similar_canidates
from common.utils import idifflib
from common.utils.uniqify import uniqify

logger = logging.getLogger("publishers.managers")


class PublisherManager(MyManager):
    '''Model manager for Publisher model.'''

    # cache settings for PublisherManager.all_id_and_short_info
    CACHE_EXPIRE = 1728000  # 60 * 60 * 24 * 20 = 20 days
    CACHE_KEY = "publishers_short_info"

    def get_similar(self, name, city=None, return_queryset=False):
        '''Returns list or queryset of similar publishers '''
        if not name and not city:
            return self.none() if return_queryset else []

        # now construct a list of strings representing publishers (name + ' ' + city)
        similar = self.all_id_and_short_info()
        namecity = (lambda n, c: "%s%s%s" % (n, ', ' if n and c else '', c if c else ''))(name, city)
        similar = get_similar_canidates(namecity, similar, key=operator.itemgetter(1))
        similar = idifflib.get_close_matches(namecity, similar, cutoff=0.6, n=10, key=operator.itemgetter(1))
        similar_ids = uniqify(i[0][0] for i in similar)
        similar_publishers = self.filter(id__in=similar_ids)
        if not return_queryset:
            similar_publishers = list(similar_publishers)
        return similar_publishers

    def all_id_and_short_info(self, force=False):
        """Get list of tuples(publisher id, publisher short_info) using cache

        force
            if True then cache be recalculated if exists
        """
        cache = get_cache("bigdata")
        data = cache.get(self.CACHE_KEY)
        if data is None or force:
            data = tuple((publisher.id, publisher.short_info()) for publisher in self.all())
            try:
                cache.set(self.CACHE_KEY, data, timeout=self.CACHE_EXPIRE)
            except:
                logger.error("Failed to setup publisher cache")
        return data


@receiver(post_save)
@receiver(post_delete)
def recalculate_cache_publishers(sender, **kwargs):
    from publishers.models import Publisher
    if sender is Publisher:
        Publisher.objects.all_id_and_short_info(force=True)
