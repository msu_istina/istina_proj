# -*- coding: utf-8; -*-
from django.http import HttpResponse
from django.utils import simplejson
import logging

from publishers.models import Publisher
from common.utils.autocomplete import autocomplete_helper

logger = logging.getLogger("publishers.views")


def autocomplete_search_publishers(request):
    '''Server side for jquery autocomplete

    Returns objects of specified type matching query term.
    '''
    term = request.GET.get('term', '')
    fields = []
    limit = 10
    if len(term) >= 2:
        items = autocomplete_helper(Publisher, "name", term, limit)
        fields = [{"name": i.name, "city": i.city} for i in items]
    return HttpResponse(simplejson.dumps(fields), content_type='application/javascript')


