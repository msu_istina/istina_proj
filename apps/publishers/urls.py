from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_detail

from common.views import autocomplete_search
from publications.views import object_publications_in_style
from publishers.models import Publisher


urlpatterns = patterns('publishers.views',
    url(r'^(?P<object_id>\d+)/$', object_detail, {
        'queryset': Publisher.objects.all(),
        'template_name': 'publishers/detail.html',
        'template_object_name': 'publisher'},
        name='publishers_detail'),
    url(r'^(?P<object_id>\d+)/books.bib$', object_detail, {
        'queryset': Publisher.objects.all(),
        'template_name': 'publications/publication_list.bib',
        'mimetype': 'text/plain'},
        name='publishers_publisher_books_bibtex'),
    url(r'^(?P<object_id>\d+)/style/(?P<style>.+)/$', object_publications_in_style, {
        'model': Publisher,
        'template_name': 'publishers/detail.html',
        'template_object_name': 'publisher'},
        name='publishers_publisher_detail_in_style'),
    url(r'^search/$', "autocomplete_search_publishers"),
    url(r'^search/simple/$', autocomplete_search, {'model': Publisher}),
)
