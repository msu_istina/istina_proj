# -*- coding: utf-8; -*-
from django import forms
import logging

from myforms.forms import MyBaseForm
from common.forms import MyModelForm
from publishers.models import Publisher

logger = logging.getLogger("publishers.forms")


class PublisherForm(MyBaseForm):
    id = forms.IntegerField(widget=forms.HiddenInput, required=False)
    publisher_name = forms.CharField(label=u"Название издательства", max_length=255, required=False)
    publisher_city = forms.CharField(label=u"Место издания", max_length=255, required=False)

    def get_namecity(self):
        return self.get_cleaned_list("publisher_name", "publisher_city")

    def fullname(self):
        name, city = self.get_namecity()
        if city:
            return (u"%s (%s)" % (name, city)).strip()
        else:
            return ("%s" % name).strip()

    def get_similar(self):
        """Get similar publisher (if both name and city are present)

        Returns list of Publusher's
        """
        name, city = self.get_namecity()
        return Publisher.objects.get_similar(name, city)

    def find_publisher(self):
        """Try to find corresponding publisher

        creates
            self.publisher_instance (=None if didn't find publisher)
            self.location = (not publisher_instance) and (name + city)
        returns
            (self.publisher_instance, self.location)
        """
        if not hasattr(self, "publisher_found"):
            id = self.cleaned_data["id"]
            name, city = self.get_namecity()
            self.publisher_instance = None
            self.location = ""

            kwargs = None
            if id:
                kwargs = dict(id=id)
            elif name and city:
                kwargs = dict(name=name, city=city)

            if kwargs:
                try:
                    self.publisher_instance = Publisher.objects.get(name=name, city=city)
                except Publisher.DoesNotExist:
                    pass

            if self.publisher_instance is None:
                f = lambda s: s.strip()
                self.location = f("%s %s" % tuple(map(f, (name, city))))
            else:
                self.location = ""
        self.publisher_found = True
        return self.publisher_instance, self.location

class PublisherModelForm(MyModelForm):
    class Meta:
        model = Publisher

