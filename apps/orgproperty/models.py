# -*- coding: utf-8 -*-
from django.db import models
from django.core.cache import cache
from object_permissions import register as register_permissions
import logging
import operator
import datetime

from common.models import MyModel
from common.utils.activities import get_workers_activities_summary
from common.utils.strings import uncapitalize_1st_letter
from common.utils.uniqify import uniqify
from common.utils.user import get_profile
from workers.models import Worker, Profile, Employment
from organizations.managers import RepresentativeManager, OrganizationManager
from organizations.utils import is_representative_for

logger = logging.getLogger("orgproperty.models")


class OrganizationPropertyCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_TYPEDESIGN_ID")
    name = models.CharField(u"Property name", max_length=255, unique=False, db_column="F_TYPEDESIGN_NAME")

    class Meta:
        db_table = "TYPEDESIGN"
        verbose_name = u"OrganizationPropertyCategory"
        verbose_name_plural = u"OrganizationPropertyCategory"

    def __unicode__(self):
        return "%s: %s" % (self.id, self.name)


class OrganizationProperty(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DESIGN_ID")
    organization = models.ForeignKey(to="organizations.Organization", related_name="properties", 
        db_column="F_ORGANIZATION_ID")
    category = models.ForeignKey(OrganizationPropertyCategory, db_column="F_TYPEDESIGN_ID")
    value = models.CharField(u"Property value", max_length=255, unique=False, db_column="F_DESIGN_TEXT")

    class Meta:
        db_table = "DESIGN"
        verbose_name = u"OrganizationProperty"
        verbose_name_plural = u"OrganizationProperty"
        unique_together = ('organization', 'category')

    def __unicode__(self):
        return "%s: %s" % (self.organization.name, self.category.name)

   