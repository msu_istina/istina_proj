# -*- coding: utf-8 -*-
from django.db.models import Q
from common.managers import MyManager
import datetime

class DissertationManager(MyManager):
    """Model manager for Dissertation model."""

    def date_in_future(self):
		return self.filter(
        	Q(date__isnull=False, date__gte=datetime.date.today()) | \
        	Q(date__isnull=True, year__isnull=False, year__gte=datetime.date.today().year)
    	)
    def date_in_past(self):
		return self.filter(
        	Q(date__isnull=False, date__lte=datetime.date.today()) | \
        	Q(date__isnull=True, year__isnull=False, year__lte=datetime.date.today().year)
    	)
    def date_in_future_or_recent_past(self):
        "2 weeks before today or in future."
        return self.filter(
            Q(date__isnull=False, date__gte=datetime.date.today() - datetime.timedelta(14)) | \
            Q(date__isnull=True, year__isnull=False, year__gte=datetime.date.today().year)
        )

    def defended(self):
        return self.date_in_past().filter(status='defended')

    def to_defend(self):
        return self.date_in_future_or_recent_past().filter(status='to_defend').order_by('year', 'date', 'title')

    def to_consider(self):
        return self.date_in_future_or_recent_past().filter(status='to_consider').order_by('year', 'date', 'title')

    def defence_failed(self):
        return self.date_in_past().filter(status='defence_failed').order_by('year', 'date', 'title')

    def unknown_status(self):
        return self.date_in_past().filter(Q(status='to_defend') | Q(status='to_consider'))

    def move_specialties(self):
        """Use only in shell."""
        from django.db.models import get_model
        Specialty = get_model("dissertations", "Specialty")
        count = 0
        for dissertation in self.filter(specialty__isnull=True, specialty_text__isnull=False):
            specialty = Specialty.objects.getdefault(number=dissertation.specialty_text.strip().strip('.'))
            if specialty:
                count += 1
                print count, dissertation.id, specialty
                dissertation.specialty = specialty
                dissertation.specialty_text = ""
                dissertation.save()
