# -*- coding: utf-8; -*-
import authority
from common.permissions import LinkedToWorkersModelPermission
from dissertations.models import Dissertation
from workers.utils import get_worker
from publications.utils import split_authors
from unified_permissions import has_permission


class DissertationPermission(LinkedToWorkersModelPermission):
    label = 'dissertation_permission'
    operations_over_superuser = tuple(
        LinkedToWorkersModelPermission.operations_over_superuser +
        ('link_to_author', 'link_to_advisers', 'link_to_opponents')
    )
    operations = (
        ("link_to_author", u"может привязаться к автору диссертации", False),
        ("link_to_advisers", u"может привязаться к руководителю диссертации", False),
        ("link_to_opponents", u"может привязаться к оппоненту диссертации", False),
        ("unlink_from_council", u"может открепить диссертацию от совета", False),
    )

    def check_link_to(self, dissertation):
        return False

    def check_link_to_author(self, dissertation):
        worker = get_worker(self.user)
        if worker and getattr(dissertation.author, 'user', None) is None:
            if dissertation.author:
                return dissertation.author.is_similar_to(worker)
            else:
                name = split_authors(dissertation.author_original_name)[0]
                fullname = dict(zip(('lastname', 'firstname', 'middlename'), name))
                return worker.is_similar_to_fullnames([fullname])
        return False

    def check_link_to_advisers(self, dissertation):
        worker = get_worker(self.user)
        if dissertation.is_linked_to(self.user, dissertation.advisers):
            return False
        return bool(dissertation.get_similar_workership_to_worker(worker))

    def check_link_to_opponents(self, dissertation):
        worker = get_worker(self.user)
        if dissertation.is_linked_to(self.user, dissertation.opponents):
            return False
        return bool(dissertation.get_similar_workership_to_worker(
            worker, workerships = dissertation.opponentships))

    def check_unlink_from_council(self, dissertation):
        return has_permission(self.user, "edit", dissertation.council) if dissertation.council else False

    def check_view_object_history(self, dissertation):
        '''Returns True if the user has access to read the object's action history.'''
        return (dissertation.author and hasattr(dissertation.author, "profile") and dissertation.author.profile.user == self.user) \
            or has_permission(self.user, "edit", dissertation.council)

    def check_edit(self, dissertation):
        '''Returns True if the user has access to edit the dissertation. Allow for the author and editors of the dissertation council.'''
        return super(DissertationPermission, self).check_edit(dissertation) \
            or (dissertation.author and hasattr(dissertation.author, "profile") and dissertation.author.profile.user == self.user) \
            or has_permission(self.user, "edit", dissertation.council)

authority.register(Dissertation, DissertationPermission)
