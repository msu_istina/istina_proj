# -*- coding: utf-8; -*-
from actstream import action
from django import forms
from django.core import serializers
import datetime
import re

from common.forms import LinkedToWorkersModelForm, Select2ModelField
from dissertation_councils.models import DissertationCouncil
from dissertations.models import Dissertation, DissertationOpponentship, Specialty
from organizations.models import Organization


class SpecialtyChoiceField(Select2ModelField):
    search_fields = ['name__icontains', 'number__icontains']

    def __init__(self, *args, **kwargs):
        super(SpecialtyChoiceField, self).__init__(model=Specialty, *args, **kwargs)

    def get_title(self, specialty):
        return specialty.name

    def get_description(self, specialty):
        return specialty.number


def init_council_number_non_standard_numeration(form):
    if not DissertationForm.council_number_message in form.non_field_errors() and not getattr(form, "cleaned_data", {}).get('non_standard_numeration', False):
        form.fields['non_standard_numeration'].label = ""
        form.fields['non_standard_numeration'].widget.attrs['class'] = 'hide'

def clean_council_number(form):
    if 'council_number' in form.cleaned_data:
        number = form.cleaned_data['council_number'].strip()
        regexp = ur'^(Д|К)М?\ \d{3}\.\d{3}\.\d{2}$'
        if not re.match(regexp, number) and not form.cleaned_data['non_standard_numeration']:
            raise forms.ValidationError(DissertationForm.council_number_message)


class DissertationForm(LinkedToWorkersModelForm):
    '''A simple form for dissertations.'''
    council_number = forms.CharField(label=u"Шифр совета", max_length=255)
    specialty = SpecialtyChoiceField()
    specialty2 = SpecialtyChoiceField(required=False)
    non_standard_numeration = forms.BooleanField(label=u"Нестандартная нумерация совета", required=False)
    workers_fields = ['advisers_str', 'author_str', 'opponents_str']
    workers_labels = [u"Научные руководители / консультанты", u"Автор", u"Оппоненты"]
    fields_order = ["advisers_str", "author_str", "title", "date", "time", "phd",
        "domain", "specialty", "specialty2", "council_number", "non_standard_numeration",
        "opponents_str",
        "leading_organization_str", "organization_where_done_str",
        "status", "abstract"]
    css_classes = [
        ('council_number', 'wide autocomplete_dissertation_council'),
        ('specialty', 'wide'),
        ('specialty2', 'wide'),
        ('time', 'narrow'),
        ('leading_organization_str', 'wide autocomplete_organization'),
        ('organization_where_done_str', 'wide autocomplete_organization')
    ]
    council_number_message = u"Пожалуйста, исправьте шифр совета. Шифр указывается в следующих форматах: Д XXX.XXX.XX, К XXX.XXX.XX, ДМ XXX.XXX.XX, КМ XXX.XXX.XX. Информация об организации в шифре не указывается. Если же вы вводите информацию о ранее существовавшем совете с другой нумерацией, отметьте галочку 'Нестандартная нумерация совета'."

    class Meta:
        model = Dissertation
        exclude = ('advisers', 'author', 'opponents', 'council', 'year', 'specialty_text',
            'leading_organization', 'organization_where_done', 'creator', 'xml')

    def __init__(self, *args, **kwargs):
        super(DissertationForm, self).__init__(*args, **kwargs)
        if self.instance_linked(): # initialize council_number field
            self.initial['council_number'] = self.instance.get_council_number
            self.initial['author_str'] = self.instance.author_fullname_short
            self.initial['leading_organization_str'] = self.instance.leading_organization_name
            self.initial['organization_where_done_str'] = self.instance.organization_where_done_name
            self.initial['opponents_str'] = self.instance.opponents_string
        if self.data and self.data.get('main-phd', None) == u'N': # doctoral dissertation may not have adviser, see #1036
            self.fields['advisers_str'].required = False
        self.fields['opponents_str'].required = False
        self.fields['opponents_str'].help_text = "необязательно"
        init_council_number_non_standard_numeration(self)

    def clean_author_str(self):
        # only one author is allowed
        value = self.cleaned_data['author_str'].strip(", ")
        if "," in value:
            raise forms.ValidationError(u"Пожалуйста, укажите ФИО только одного автора. В строке не должно быть запятых.")
        return value

    def clean(self):
        cleaned_data = super(DissertationForm, self).clean()
        status = cleaned_data.get("status")
        date = cleaned_data.get("date")
        if status and date:
            if status in ["to_defend", "to_consider", "consideration_failed"] and date < datetime.date.today():
                raise forms.ValidationError(u"Дата предстоящей защиты не может быть в прошлом.")
            if status in ["defended", "defence_failed"] and date > datetime.date.today():
                raise forms.ValidationError(u"Прошедшая защита не может иметь дату в будущем.")
        clean_council_number(self)
        return cleaned_data

    def save(self, request, workers, council, organization_str, department, commit=True):
        dissertation = super(DissertationForm, self).save(request, workers, commit=False)
        dissertation.author_original_name = self.cleaned_data['author_str']

        # try to retrieve the organizations with this name
        try:
            dissertation.leading_organization = Organization.objects.get(name=dissertation.leading_organization_str)
            dissertation.leading_organization_str = ""
        except Organization.DoesNotExist:
            dissertation.leading_organization = None
        try:
            dissertation.organization_where_done = Organization.objects.get(name=dissertation.organization_where_done_str)
            dissertation.organization_where_done_str = ""
        except Organization.DoesNotExist:
            dissertation.organization_where_done = None
        council_created = False
        # link to council
        if not council: # create a new council
            properties = {
                'number': self.cleaned_data['council_number'],
                'department': department,
                'creator': request.user
            }
            # try to retrieve the organization with this name
            try:
                properties['organization'] = Organization.objects.get(name=organization_str)
            except:
                properties['organization_str'] = organization_str
            council = DissertationCouncil(**properties)
            council.xml = council.get_xml()
            council.save()
            council_created = True
        dissertation.council = council
        if commit:
            self.commit_save(request, dissertation, workers)
            if council_created:
                action.send(request.user, verb=u'добавил диссертационный совет', action_object=council, target=dissertation)
        return dissertation

    def commit_save(self, request, object, workers):
        '''Method that is executed just before final commiting the object into a database.'''
        if len(workers) == 3: # advisers, author and opponents are specified (all as lists)
            advisers = workers[0]
            author = workers[1][0]
            opponents = workers[2]
        elif len(workers) == 2: # author is specified and (advisers or opponents are specified)
            if self.cleaned_data['advisers_str'].strip() and not self.cleaned_data['opponents_str'].strip():
                advisers = workers[0]
                author = workers[1][0]
                opponents = []
            elif not self.cleaned_data['advisers_str'].strip() and self.cleaned_data['opponents_str'].strip():
                advisers = []
                author = workers[0][0]
                opponents = workers[1]
            else:
                raise
        elif len(workers) == 1: # only author is specified (in a list)
            advisers = []
            author = workers[0][0]
            opponents = []
        else:
            raise
        try:
            old_author = object.author
        except:
            old_author = None
        object.author = author
        super(DissertationForm, self).commit_save(request, object, advisers)
        # save opponents separately
        # copied the code from LinkedToWorkersModelForm.commit_save and modified
        # if edit, first delete old object-worker intermediate objects(e.g. authorships), then save the new ones
        object.notify_new_workers(opponents, request.user, old_workerships=object.opponentships.all())
        object.notify_new_workers([author], request.user, old_workers=[old_author])
        if self.edit:
            object.opponentships.all().delete()
        if opponents:
            # save intermediate objects
            object.add_workers(opponents, request.user, DissertationOpponentship)
