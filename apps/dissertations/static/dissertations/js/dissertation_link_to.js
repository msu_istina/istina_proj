$(function() {
    // show confirmation and link to object
    $("a.link_to_object_author_initial").click(function(event) {
        event.preventDefault()
        $(this).hide()
        $(".link_to_object_author_question").show()
    });
    $("a.link_to_object_author_question").click(function(event) { // FIXME add event.preventDefault() outside 'if'?
        if ($(this).hasClass("cancel")) {
            event.preventDefault()
            $(".link_to_object_author_question").hide()
            $(".link_to_object_author_initial").show()
        }
    });
});

$(function() {
    // show confirmation and link to object
    $("a.link_to_object_advisers_initial").click(function(event) {
        event.preventDefault()
        $(this).hide()
        $(".link_to_object_advisers_question").show()
    });
    $("a.link_to_object_advisers_question").click(function(event) { // FIXME add event.preventDefault() outside 'if'?
        if ($(this).hasClass("cancel")) {
            event.preventDefault()
            $(".link_to_object_advisers_question").hide()
            $(".link_to_object_advisers_initial").show()
        }
    });
});

$(function() {
    // show confirmation and link to object
    $("a.link_to_object_opponents_initial").click(function(event) {
        event.preventDefault()
        $(this).hide()
        $(".link_to_object_opponents_question").show()
    });
    $("a.link_to_object_opponents_question").click(function(event) { // FIXME add event.preventDefault() outside 'if'?
        if ($(this).hasClass("cancel")) {
            event.preventDefault()
            $(".link_to_object_opponents_question").hide()
            $(".link_to_object_opponents_initial").show()
        }
    });
});
