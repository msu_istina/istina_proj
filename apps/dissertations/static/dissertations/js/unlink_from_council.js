$(function() {
    // show confirmation and unlink from council
    $("a.unlink_from_council_initial").click(function(event) {
        event.preventDefault()
        $(this).hide()
        $(".unlink_from_council_question").show()
    });
    $("a.unlink_from_council_question").click(function(event) { // FIXME add event.preventDefault() outside 'if'?
        if ($(this).hasClass("cancel")) {
            event.preventDefault()
            $(".unlink_from_council_question").hide()
            $(".unlink_from_council_initial").show()
        }
    });
});