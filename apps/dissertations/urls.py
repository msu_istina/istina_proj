# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *

from dissertations.models import Dissertation
from dissertations.views import DissertationWizardView as Wizard, DissertationAttestationCase
from django.views.generic.list_detail import object_detail
from common.models import AuthorRole
from common.views import link_to

options_base = {'model': Dissertation }
options_detail = dict(options_base.items() + [('extra_context', {'roles': AuthorRole.objects.filter(authorroleobj__code="disser")})])
options_detail['template_name'] = 'dissertations/detail.html'

urlpatterns = patterns(
    'common.views',
    url(r'^add/$', Wizard.as_view(), name='dissertations_add'),
    url(r'^(?P<object_id>\d+)/edit/$', Wizard.as_view(), name='dissertations_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', options_base, name='dissertations_delete'),
    url(r'^(?P<object_id>\d+)/announcement/$', object_detail,
        {
            'queryset': Dissertation.objects.all(),
            'template_name': 'dissertations/defence_announcement.html',
            'template_object_name': 'dissertation'
        },
        name='dissertations_defence_announcement'),
    url(r'^(?P<object_id>\d+)/print/$', object_detail,
        {
            'queryset': Dissertation.objects.all(),
            'template_name': 'dissertations/detail_print.html',
            'template_object_name': 'dissertation'
        },
        name='dissertations_detail_print'),
) + patterns(
    'dissertations.views',
    url(r'^(?P<object_id>\d+)/$', 'detail', options_detail, name='dissertations_detail'),
    url(r'^(?P<object_id>\d+)/announcement/update/$',
        'update_announcement', name='dissertations_update_defence_announcement'),
    url(r'^link_to_author/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$',
        link_to, {'permission': 'link_to_author', 'link_to_method': 'link_to_author'},
        name='dissertations_link_to_author'),
    url(r'^link_to_advisers/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$',
        link_to, {'permission': 'link_to_advisers', 'link_to_method': 'link_to_advisers'},
        name='dissertations_link_to_advisers'),
    url(r'^link_to_opponents/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$',
        link_to, {'permission': 'link_to_opponents', 'link_to_method': 'link_to_opponents'},
        name='dissertations_link_to_opponents'),
    url(r'^(?P<pk>\d+)/attestation_case/$', DissertationAttestationCase.as_view(), name='dissertations_attestation_case'),
    url(r'^(?P<object_id>\d+)/unlink_from_council/$', 'unlink_from_council', name='dissertations_unlink_from_council'),
)
