# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
import datetime
import logging

from common.models import LinkedToWorkersModel, WorkershipModel, MyModel, Activity, AuthorScience
from workers.models import Worker
from common.utils.validators import validate_year
from workers.utils import get_worker, get_all_named_representations
from publications.utils import split_authors
from dissertations.managers import DissertationManager
from common.utils.cache import invalidate_all_authors, invalidate_worker_coauthors_summary

logger = logging.getLogger("dissertations.models")


class Dissertation(LinkedToWorkersModel, Activity):
    '''A dissertation that has been defended under a supervision of a worker.'''
    id = models.AutoField(primary_key=True, db_column="F_DISSER_ID")
    title = models.CharField(u"Название диссертации", max_length=255, db_column="F_DISSER_NAME")
    author = models.ForeignKey(to="workers.Worker", related_name="dissertations_defended", db_column="F_MAN_ID", verbose_name=u"Автор", blank=True, null=True)
    author_original_name = models.CharField(max_length=255, db_column="F_DISSER_AUTHORNAME", blank=True)
    advisers = models.ManyToManyField("workers.Worker", related_name="dissertations_advised", through="DissertationAdvisement")
    opponents = models.ManyToManyField("workers.Worker", related_name="dissertations_opposed", through="DissertationOpponentship", blank=True, null=True)
    council = models.ForeignKey(to="dissertation_councils.DissertationCouncil", related_name="dissertations", db_column="F_BOARD_ID", verbose_name=u"Диссертационный совет", blank=True, null=True)
    council_number = models.CharField(u"Шифр совета", max_length=255, db_column="F_DISSER_BOARDNUM", blank=True)
    council_organization = models.ForeignKey(to="organizations.Organization", related_name="dissertation", db_column="ORG2_F_ORGANIZATION_ID", verbose_name=u"Организация", blank=True, null=True)
    council_organization_str = models.CharField(u"Организация совета", max_length=255, db_column="F_DISSER_BOARDORG", blank=True)
    domain = models.ForeignKey(to="dissertations.Domain", related_name="dissertations", db_column="F_BRANCH_ID", verbose_name=u"Область знаний")
    specialty = models.ForeignKey("dissertations.Specialty", related_name="dissertations",
                                  verbose_name=u"Специальность", db_column="F_JOURNALRUB_ID", null=True)
    specialty2 = models.ForeignKey("dissertations.Specialty", related_name="dissertations_as_second",
                                  verbose_name=u"Вторая специальность", db_column="JOU_F_JOURNALRUB_ID", blank=True, null=True)
    specialty_text = models.CharField(u"Номер специальности", max_length=255, db_column="F_DISSER_BRANCHNUM", blank=True)
    phd = models.CharField(u"Тип диссертации", max_length=1, choices=(("Y", u"Кандидатская"), ("N", u"Докторская")), db_column="F_DISER_PRIOR")
    year = models.IntegerField(u"Год защиты", db_column="F_DISSER_YEAR", validators=[validate_year], null=True, blank=True)
    date = models.DateField(u"Дата защиты", db_column="F_DISSER_DATE", help_text=u"Если точная дата неизвестна, укажите примерную")
    time = models.TimeField(u"Время защиты", db_column="F_DISSER_TIME", null=True, blank=True,
                            help_text=u"необязательно; используется для публикации объявления о защите на странице диссертационного совета; пример: 16:30")
    leading_organization_str = models.CharField(u"Ведущая организация", help_text=u"необязательно",
                                                max_length=1000, db_column="F_DISSER_LEADORG", blank=True)
    leading_organization = models.ForeignKey(to="organizations.Organization", verbose_name=u"Ведущая организация",
                                             related_name="dissertations_lead", db_column="F_ORGANIZATION_ID", blank=True, null=True)
    organization_where_done_str = models.CharField(u"Организация, в которой выполнялась работа", help_text=u"необязательно",
                                                   max_length=1000, db_column="F_DISSER_WHEREORGSTR", blank=True)
    organization_where_done = models.ForeignKey(to="organizations.Organization", verbose_name=u"Организация, в которой выполнялась работа",
                                                related_name="dissertations_done", db_column="ORG_F_ORGANIZATION_ID", blank=True, null=True)
    status = models.CharField(u"Статус", max_length=50, choices = (
        ("defended", u"Защищена"),
        ("to_defend", u"Принята к защите"),
        ("to_consider", u"Принята к рассмотрению"),
        ("consideration_failed", u"Отказ в приеме к защите"),
        ("defence_failed", u"Отрицательная защита"),
    ), default="defended", db_column="F_DISSER_STATUS")
    abstract = models.TextField(u"Аннотация", db_column="F_DISER_ABSTRACT", blank=True)
    announcement_date = models.DateField(u"Дата публикации объявления о защите", db_column="F_DISSER_DATEANNOUN", null=True, blank=True)
    xml = models.TextField(u"XML диссертации", db_column="F_DISSER_XML", blank=True)
    creator = models.ForeignKey(to=User, related_name="dissertations_added", db_column="F_DISER_USER", null=True, blank=True)

    objects = DissertationManager()

    workers_attr = 'advisers'
    workerships_attr = 'advisements'
    workers_verbose_name_single = u"Научный руководитель"
    workers_verbose_name_plural = u"Научные руководители"
    workers_required_error_msg = u"Укажите научного руководителя или консультанта"

    nominative_en = "dissertation"
    genitive = u"диссертации"
    genitive_plural_full = u"диссертаций"
    accusative = u"диссертацию"
    accusative_short = u"диссертацию"
    instrumental = u"диссертацией"
    locative = u"диссертации"
    gender = "feminine"

    class Meta:
        db_table = "DISSER"
        verbose_name = u"диссертация"
        verbose_name_plural = u"диссертации"
        get_latest_by = "date"
        ordering = ('-year', '-date', 'title')

    def __unicode__(self):
        return u"%s" % self.title

    @models.permalink
    def get_absolute_url(self):
        return ('dissertations_detail', (), {'object_id': self.id})

    def save(self, *args, **kwargs):
        # store the year because it is used in various SQL reports
        if self.date:
            self.year = self.date.year
        super(Dissertation, self).save(*args, **kwargs)
        # manually reset all authors cache
        # this is required when no Workership model is created when adding a dissertation
        # i.e. it has an author, but no advisers or opponents
        # in this case all invalidation signals do not do anything
        if self.author and not self.advisers.exists() and not self.opponents.exists():
            invalidate_all_authors(self, invalidate_summary=True)

    def delete(self, *args, **kwargs):
        if self.author and not self.advisers.exists() and not self.opponents.exists():
            author = self.author
        else:
            author = None
        super(Dissertation, self).delete(*args, **kwargs)
        # manually reset all authors cache
        # this is required when no Workership model is linked to dissertation
        # i.e. it has an author, but no advisers or opponents
        # in this case all invalidation signals do not do anything
        if author:
            invalidate_worker_coauthors_summary(author)

    @property
    def linked_users(self):
        '''Override linked_users property from LinkedToWorkersModel
        as linked users for dissertations are not only advisers,
        but also opponents and author.'''
        users = super(Dissertation, self).linked_users

        users_ids = self.opponents.values_list('profile__user', flat=True)
        users.extend(User.objects.filter(id__in=users_ids))

        if getattr(self.author, 'user', None) is not None:
            users.append(self.author.user)

        return users

    def is_linked_to(self, user, workers=None):
        '''Override is_linked_to() method from LinkedToWorkersModel
        as dissertations need additional optional @workers argument.
        If @workers argument is specified, then it is used as a QuerySet
        to obtain a list of users. Otherwise, a list of users returned by
        linked_users property is used.'''
        if workers is None:
            users = self.linked_users
        else:
            users_ids = workers.values_list('profile__user', flat=True)
            users = User.objects.filter(id__in=users_ids)
        return self.id and user.is_authenticated() and user in users

    def link_to_author(self, user):
        worker = get_worker(user)
        if worker and getattr(self.author, 'user', None) is None:
            name = split_authors(self.author_fullname)[0]
            fullname = dict(zip(('lastname', 'firstname', 'middlename'), name))
            worker_representations = get_all_named_representations(worker)
            matching_worker = Worker.objects.get_very_similar(
                fullname, worker_representations)[0][0]
            if matching_worker:
                old_worker = self.author
                alias, created = None, False
                aliases = worker.aliases.filter(**fullname)
                if aliases:
                    alias = aliases[0]
                else:
                    alias = worker.aliases.create(validated=0, **fullname)
                    created = True
                self.author = matching_worker
                self.save()
                return matching_worker, old_worker, alias, created
        return None, None, None, False

    def link_to_advisers(self, user):
        return self.link_to(user, self.advisements)

    def link_to_opponents(self, user):
        return self.link_to(user, self.opponentships)

    def unauthor(self, user):
        '''Override unauthor() method from LinkedToWorkersModel
        to unauthor user from advisements, opponentships and authorship.'''
        super(Dissertation, self).unauthor(user)

        worker_attr = self.opponentships.model.worker_attr
        filter_dict = {'{0}__profile__user'.format(worker_attr): user}
        workerships = self.opponentships.filter(**filter_dict)

        super(Dissertation, self).unauthor(user, workerships)

        if self.author and self.author.user == user:
            if not self.author_original_name:
                self.author_original_name = self.author.fullname_short
            self.author = None
            self.save()

    def change_worker(self, man_from, man_to, limit=None):
        '''Relink the object from man_from to man_to.
        Man_{from,to} can be either an id of a worker or
        a worker instance or a username or a user instance.
        If a limit is set, change max limit links.
        '''
        super(Dissertation, self).change_worker(man_from, man_to, limit)
        # now switch the author, since the parent function only switched advisers (due to workers_attr)
        worker_from = get_worker(man_from)
        worker_to = get_worker(man_to)
        if not worker_from or not worker_to:
            return
        if self.author == worker_from:
            self.author = worker_to
            self.save()
            logger.debug("Args: %s (id %d), %s, %s%s", self.__class__, self.id, man_from, man_to, ', limit=%s' % limit if limit else '')
            logger.debug("Successfully relinked author from worker %s (id %d) to worker %s (id %d)", worker_from, worker_from.id, worker_to, worker_to.id)
        # now switch the reviewers
        super(Dissertation, self).change_worker(man_from, man_to, limit, workerships_manager=self.reviewerships)

    @property
    def workers_verbose_name(self):
        if self.phd == 'Y':  # кандидатская, а не докторская
            return super(Dissertation, self).workers_verbose_name
        else:
            return u"Научные консультанты" if self.workers.count() > 1 else u"Научный консультант"

    @property
    def all_workers_string(self):
        advisers_str = super(Dissertation, self).all_workers_string
        author_str = self.author.fullname_short if self.author else self.author_original_name
        return "%s%s%s" % (author_str, ", " if author_str and advisers_str else "", advisers_str)

    @property
    def get_year(self):
        return self.date.year if self.date else self.year

    @property
    def date_in_future(self):
        if self.date:
            return self.date >= datetime.date.today()
        else:
            return self.year >= datetime.date.today().year

    @property
    def date_in_future_or_recent_past(self):
        if self.date:
            return self.date >= datetime.date.today() - datetime.timedelta(14)
        else:
            return self.year >= datetime.date.today().year

    @property
    def date_in_past(self):
        '''
        Note that if self.year == current year,
        then a dissertation will be in future and in past
        simultaneously.
        We can't do anything with that, but all new
        dissertations should have exact date.
        Also see how these properties are used, there should be no problems.
        '''
        if self.date:
            return self.date <= datetime.date.today()
        else:
            return self.year <= datetime.date.today().year

    @property
    def defended(self):
        return self.status == 'defended' and self.date_in_past

    @property
    def to_defend(self):
        return self.status == 'to_defend' and self.date_in_future_or_recent_past

    @property
    def to_consider(self):
        return self.status == 'to_consider' and self.date_in_future

    @property
    def defence_failed(self):
        return self.status == 'defence_failed' and self.date_in_past

    @property
    def show_defence_date_or_year(self):
        return self.defended or self.to_defend

    @property
    def show_opponents_leading_org(self):
        return self.defended or self.to_defend

    @property
    def show_attachments_in_snippet(self):
        return self.to_defend or self.to_consider

    @property
    def show_year_in_snippet(self):
        return self.defended or self.to_defend or self.defence_failed

    @property
    def show_announcement(self):
        return self.announcement_date and (self.to_defend or (self.defended and self.date >= datetime.date.today() - datetime.timedelta(30*12)))

    @property
    def show_update_announcement_link(self):
        return self.to_defend

    @property
    def get_specialty(self):
        text = unicode(self.specialty or self.specialty_text)
        if self.specialty2:
            text += u", %s" % self.specialty2
        return text

    @property
    def leading_organization_name(self):
        return self.leading_organization and self.leading_organization.name or self.leading_organization_str

    @property
    def organization_where_done_name(self):
        return self.organization_where_done and self.organization_where_done.name or self.organization_where_done_str

    @property
    def opponents_list(self):
        return self._workers_list(workerships=self.opponentships.all(), initials=False)

    @property
    def opponents_string(self):
        return ", ".join(worker['name'] for worker in self.opponents_list)

    @property
    def author_status(self):
        return u'Автор' if self.status == 'defended' else u'Соискатель'

    @property
    def author_fullname(self):
        return self.author_original_name or self.author.fullname

    @property
    def author_fullname_short(self):
        return self.author_original_name or self.author.fullname_short

    @property
    def opponentships(self):
        return self.reviewerships.filter(category__name=DissertationOpponentship.category_name)

    @property
    def fulltext_upload_date(self):
        fulltexts = self.attachments.filter(category__name="Полный текст диссертации").order_by('date_uploaded')
        date = fulltexts[0].date_uploaded if fulltexts else ""
        return date

    @property
    def autoreferat_upload_date(self):
        autoreferats = self.attachments.filter(category__name="Автореферат").order_by('date_uploaded')
        date = autoreferats[0].date_uploaded if autoreferats else ""
        return date

    @property
    def autoreferat_url(self):
        autoreferats = self.attachments.filter(category__name="Автореферат").order_by('date_uploaded')
        url = autoreferats[0].get_full_url() if autoreferats else ""
        return url

    @property
    def authorship(self):
        '''Fake workership object, used e.g. in WorkerFormSet to preserve workership for author.'''
        return DissertationAuthorship(author=self.author, dissertation=self, original_name=self.author_original_name)

    @property
    def is_done_in_council_organization(self):
        if not self.council:
            return (self.organization_where_done and self.organization_where_done.name == self.council_organization_name) \
                or (self.organization_where_done_str and self.organization_where_done_str == self.council_organization_name)
        else:
            return (self.organization_where_done and self.organization_where_done == self.council.organization) \
                or (self.organization_where_done_str and self.organization_where_done_str == self.council.organization_str)

    @property
    def is_candidate(self):
        return self.phd == 'Y'

    @property
    def is_doctor(self):
        return self.phd == 'N'

    @property
    def get_council_number(self):
        return self.council.number if self.council else self.council_number

    @property
    def council_organization_name(self):
        if self.council:
            name = self.council.organization_name
        elif self.council_organization:
            name = self.council_organization.name
        else:
            name = self.council_organization_str
        return name

    @property
    def authorscience(self):
        authors = AuthorScience.objects.filter(Q(sciencework__id=self.id, flag_validate=1)|Q(sciencework__id=self.id,flag_validate__isnull=True))
        authorsdict = {}
        for author in authors:
            if author.authorrole.name not in authorsdict.keys():
                authorsdict[author.authorrole.name] = []
            authorsdict[author.authorrole.name].append(author)
        return authorsdict



class DissertationAuthorship(WorkershipModel):
    '''Fake workership object, used e.g. in WorkerFormSet to preserve workership for author.'''
    id = models.AutoField(primary_key=True)
    author = models.ForeignKey(to="workers.Worker", related_name="dissertation_authorships", null=True, blank=True)
    dissertation = models.ForeignKey(to="Dissertation", related_name="authorships")
    original_name = models.CharField(max_length=255, blank=True)

    worker_attr = 'author'

    class Meta:
        abstract = True


class DissertationAdvisement(WorkershipModel):
    id = models.AutoField(primary_key=True, db_column="F_DISERHEAD_ID")
    adviser = models.ForeignKey(to="workers.Worker", related_name="dissertation_advisements", db_column="F_MAN_ID", null=True, blank=True)
    dissertation = models.ForeignKey(to="Dissertation", related_name="advisements", db_column="F_DISSER_ID")
    original_name = models.CharField(max_length=255, db_column="F_DISERHEAD_NAME", blank=True)

    worker_attr = 'adviser'

    class Meta:
        db_table = "DISERHEAD"
        verbose_name = u"руководство диссертацией"
        verbose_name_plural = u"руководства диссертациями"


class DissertationReviewship(WorkershipModel):
    id = models.AutoField(primary_key=True, db_column="f_disjudge_id")
    reviewer = models.ForeignKey(to="workers.Worker", related_name="dissertations_reviewerships", db_column="f_man_id", null=True, blank=True)
    dissertation = models.ForeignKey(to="Dissertation", related_name="reviewerships", db_column="f_disser_id")
    original_name = models.CharField(max_length=255, db_column="f_disjudge_name", blank=True)
    position = models.IntegerField(db_column="f_disjudge_ord", default=0, blank=True, null=True)
    category = models.ForeignKey(to="DissertationReviewshipCategory",
                                 related_name="reviewerships", db_column="f_disjudgetype_id")

    worker_attr = 'reviewer'

    class Meta:
        db_table = "DISJUDGE"
        verbose_name = u"оценивание диссертации"
        verbose_name_plural = u"оценивание диссертаций"


class DissertationOpponentship(DissertationReviewship):
    worker_attr = 'opponent'
    category_name = u'Оппонент'

    class Meta:
        verbose_name = u"оппонирование диссертации"
        verbose_name_plural = u"оппонирование диссертаций"
        proxy = True

    @property
    def opponent(self):
        return self.reviewer

    @opponent.setter
    def opponent(self, value):
        self.reviewer = value

    @opponent.deleter
    def opponent(self):
        del self.reviewer

    def save(self, *args, **kwargs):
        self.category, _ = DissertationReviewshipCategory.objects.get_or_create(name=self.category_name)
        super(DissertationOpponentship, self).save(*args, **kwargs)


class DissertationReviewshipCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="f_disjudgetype_id")
    name = models.CharField(u"Название", max_length=255, db_column="f_disjudgetype_name")

    class Meta:
        db_table = "disjudgetype"
        verbose_name = u"вид оценивания диссертации"
        verbose_name_plural = u"виды оценивания диссертаций"

    def __unicode__(self):
        return u"%s" % self.name


class Domain(MyModel):
    '''A domain of science.'''
    id = models.AutoField(primary_key=True, db_column="F_BRANCH_ID")
    name = models.CharField(u"Название", max_length=255, db_column="F_BRANCH_NAME")
    shortname = models.CharField(u"Краткое название", max_length=255, db_column="F_BRANCH_ABBR")

    class Meta:
        db_table = "BRANCH"
        verbose_name = u"область знаний"
        verbose_name_plural = u"области знаний"

    def __unicode__(self):
        return u"%s" % self.name


class Specialty(MyModel):
    '''A specialty of a dissertation. It's a view. The main table is "Рубрика журнала" (JOURNALRUB).'''
    id = models.AutoField(primary_key=True, db_column="F_SPECIALTY_ID")
    number = models.CharField(u"Номер", max_length=10, db_column="F_SPECIALTY_NUMBER")
    name = models.CharField(u"Название", max_length=255, db_column="F_SPECIALTY_NAME")

    class Meta:
        db_table = "SPECIALTY"
        managed = False  # it's a database view, not a model
        verbose_name = u"специальность диссертации"
        verbose_name_plural = u"специальности диссертаций"
        ordering = ('number',)

    def __unicode__(self):
        return u"%s - %s" % (self.number, self.name)

    @property
    def level(self):
        if self.number.endswith("00.00"):
            return 1
        if self.number.endswith("00"):
            return 2
        return 3

from dissertations.admin import *
