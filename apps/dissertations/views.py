# -*- coding: utf-8; -*-
import logging
import datetime
from actstream import action
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.detail import DetailView
from common.forms import make_attachment_formset
from dissertations.models import Dissertation
from dissertations.forms import DissertationForm
from dissertation_councils.models import DissertationCouncil
from dissertation_councils.forms import DissertationCouncilOrganizationForm
from workers.forms import WorkerFormSet, UnrelatedWorkerFormSet
from common.models import Attachment
from common.views import LinkedToWorkersModelWizardView
from common.views import detail as common_detail_view
from unified_permissions import has_permission
from unified_permissions.decorators import permission_required

logger = logging.getLogger("dissertations.views")


class DissertationWizardView(LinkedToWorkersModelWizardView):
    workers_steps = [
        'advisers_disambiguation',
        'author_disambiguation',
        'opponents_disambiguation'
    ]  # names of the workers disambiguation steps
    form_list = [
        (LinkedToWorkersModelWizardView.main_step, DissertationForm),
        ('organization', DissertationCouncilOrganizationForm),
        ('advisers_disambiguation', WorkerFormSet),
        ('author_disambiguation', WorkerFormSet),
        ('opponents_disambiguation', UnrelatedWorkerFormSet)
    ]
    condition_dict = LinkedToWorkersModelWizardView.make_condition_dict(
        'organization', 'advisers_disambiguation', 'opponents_disambiguation')
    model = Dissertation  # used in templates

    def get_form_kwargs(self, step=None):
        """
        Returns the keyword arguments for instantiating the form
        (or formset) on given step.
        """
        kwargs = super(DissertationWizardView, self).get_form_kwargs(step)
        if step == 'author_disambiguation':
            kwargs.update({'required_error_msg': u"Укажите автора"})
            if kwargs.get('object_instance', None):
                kwargs.update({'workerships': [kwargs['object_instance'].authorship]})
        elif step == 'opponents_disambiguation' and kwargs.get('object_instance', None):
            kwargs.update({'workerships': kwargs['object_instance'].opponentships.all()})
        return kwargs

    def get_context_data(self, form, **kwargs):
        context = super(DissertationWizardView, self).get_context_data(form, **kwargs)
        if self.steps.current == 'author_disambiguation':
            context.update({
                'workers_verbose_name_single': u"Автор",
                'workers_verbose_name_plural': u"Автор",
            })
        elif self.steps.current == 'opponents_disambiguation':
            context.update({
                'workers_verbose_name_single': u"Оппонент",
                'workers_verbose_name_plural': u"Оппоненты",
            })
        return context

    def process_step(self, form):
        if self.steps.current == self.main_step:
            council_number = form.cleaned_data['council_number']
            old_council_number = self.get_extra_data('council_number')
            if council_number != old_council_number:  # council_number has changed
                self.set_extra_data('council_number', council_number)  # store it to detect changes in number after pressing 'back' button and not make db query if the number has not changed
                try:
                    council = DissertationCouncil.objects.get(number=council_number)
                except:
                    self.include_steps('organization')
                else:
                    self.set_extra_data('council_instance', council)
                    self.skip_steps('organization')
            # if adviser is not specified, the disambiguation step for advisers should be skipped
            if not form.fields['advisers_str'].required and not form.cleaned_data['advisers_str'].strip():
                self.skip_steps('advisers_disambiguation')
            if not form.fields['opponents_str'].required and not form.cleaned_data['opponents_str'].strip():
                self.skip_steps('opponents_disambiguation')
        return super(DissertationWizardView, self).process_step(form)

    def save_object(self, form_list):
        '''This function is executed during done() method, and used to save main form object.
        Workers objects has been already saved.
        '''
        council = self.get_extra_data('council_instance', DissertationCouncil)
        organization_str = form_list[1].cleaned_data['organization_str'] if not council else ''
        department = form_list[1].cleaned_data['department'] if not council else None
        return super(DissertationWizardView, self).save_object(form_list, council, organization_str, department)


def detail(request, object_id, model, template_name, extra_context=None):
    dissertation = get_object_or_404(model, pk=object_id)
    operations = ('link_to_author', 'link_to_advisers', 'link_to_opponents')
    show_object_menu = any((has_permission(request.user, operation, dissertation) for operation in operations))
    show_object_menu_context = {'show_object_menu': show_object_menu}
    if extra_context is not None:
        extra_context.update(show_object_menu_context)
    else:
        extra_context = show_object_menu_context
    content_type = ContentType.objects.get_for_model(model)
    extra_context['attachment_formset'] = add_attachment_categories_optgroups(make_attachment_formset(content_type, queryset=Attachment.objects.none()))
    return common_detail_view(request, object_id, model, template_name,
                              extra_context = extra_context)

def add_attachment_categories_optgroups(attachment_formset):
    main_categories_names = [
        u"Полный текст диссертации",
        u"Автореферат",
        u"Отзыв научного руководителя/консультанта",
        u"Отзыв ведущей организации",
        u"Отзыв официального оппонента",
        u"Сведения о ведущей организации, включая публикации",
        u"Сведения об официальных оппонентах, включая публикации",
        u"Заявление в дисс. совет",
        u"Решение дисс.совета о приеме/отказе к защите",
        u"Сведения о результатах защиты",
        u"Информационная справка",
    ]
    for form in attachment_formset.forms:
        categories = form.fields['category'].choices
        new_categories = []
        main_optgroup = []
        other_options = []
        for id, name in categories:
            if not id: # empty choice
                new_categories.append((id, name))
            elif name in main_categories_names:
                main_optgroup.append((id, name))
            else:
                other_options.append((id, name))
        new_categories.append([u'Обязательные файлы', main_optgroup])
        new_categories.append([u'Остальные файлы', other_options])
        form.fields['category'].choices = new_categories
    return attachment_formset

def update_announcement(request, object_id):
    dissertation = get_object_or_404(Dissertation, pk=object_id)
    if dissertation.show_update_announcement_link and has_permission(request.user, "edit", dissertation):
        message = u"Дата публикации объявления о защите успешно обновлена." if dissertation.announcement_date else u"Объявление о защите успешно опубликовано."
        verb = u"обновил дату объявления о защите" if dissertation.announcement_date else u"опубликовал объявление о защите"
        dissertation.announcement_date = datetime.date.today()
        dissertation.save()
        action.send(request.user, verb=verb, target=dissertation)
        messages.success(request, message)
    return redirect("dissertations_defence_announcement", object_id=object_id)


class DissertationAttestationCase(DetailView):
    model = Dissertation
    context_object_name = "dissertation"
    template_name = 'dissertations/attestation_case.html'

    def get_context_data(self, **kwargs):
        kwargs = super(DissertationAttestationCase, self).get_context_data(**kwargs)
        year = self.object.year
        recent_years = range(year-4, year+1) # 5 recent including year of defence
        if self.object.author:
            publications = self.object.author.publications_for_dissertation_council(recent_years)
        else:
            publications = []
        kwargs.update({
            'year': year,
            'council': self.object.council,
            'publications': publications
        })
        return kwargs


@permission_required("unlink_from_council", Dissertation)
def unlink_from_council(request, object_id):
    dissertation = get_object_or_404(Dissertation, pk=object_id)
    if dissertation.council:
        council = dissertation.council
        dissertation.council_number = council.number
        if council.organization:
            dissertation.council_organization = council.organization
        else:
            dissertation.council_organization_str = council.organization_str
        dissertation.council = None
        dissertation.save()
        action.send(request.user, verb=u"открепил диссертацию от совета", target=dissertation, action_object=council)
        messages.success(request, u"Диссертация успешно откреплена от совета.")
    return redirect(dissertation)
