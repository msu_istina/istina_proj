# -*- coding: utf-8; -*-
from common.forms import LinkedToWorkersModelForm
from certificates.models import RegistrationCertificate

class RegistrationCertificateForm(LinkedToWorkersModelForm):
    '''A simple form for certificates of registration.'''
    fields_order = ["authors_str", "title", "number", "date", "description"]

    class Meta:
        model = RegistrationCertificate
        exclude = ('authors', 'creator', 'xml')

