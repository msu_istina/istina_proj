# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from common.views import LinkedToWorkersModelWizardView as Wizard
from certificates.models import RegistrationCertificate
from certificates.forms import RegistrationCertificateForm

RegistrationCertificateFormWizard = Wizard.create_wizard_from_form(RegistrationCertificateForm)

options_base = {'model': RegistrationCertificate}
options_detail = dict(options_base.items() + [('template_name', 'certificates/detail.html')])

urlpatterns = patterns('common.views',
    url(r'^add/$', RegistrationCertificateFormWizard.as_view(), name='certificates_add'),
    url(r'^(?P<object_id>\d+)/$', 'detail', options_detail, name='certificates_detail'),
    url(r'^(?P<object_id>\d+)/edit/$', RegistrationCertificateFormWizard.as_view(), name='certificates_edit'),
    url(r'^(?P<object_id>\d+)/delete/$', 'delete', options_base, name='certificates_delete'),
)
