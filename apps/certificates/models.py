# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from common.models import AuthoredModel, AuthorshipPositionedModel, Activity

class RegistrationCertificate(AuthoredModel, Activity):
    '''A certificate of registration for a computer program rights.'''
    id = models.AutoField(primary_key=True, db_column="F_PATPROGRAM_ID")
    title = models.CharField(u"Название объекта", max_length=255, db_column="F_PATPROGRAM_NAME")
    authors = models.ManyToManyField("workers.Worker", related_name="certificates", through="RegistrationCertificateAuthorship")
    number = models.CharField(u"Номер", max_length=255, db_column="F_PATPROGRAM_NUMBER")
    date = models.DateField(u"Дата получения", db_column="F_PATPROGRAM_DATE")
    description = models.TextField(u"Описание", db_column="F_PATPROGRAM_DESCRIPTION", blank=True)
    xml = models.TextField(u"XML свидетельства", db_column="F_PATPROGRAM_XML", blank=True)
    creator = models.ForeignKey(to=User, related_name="certificates_added", db_column="F_PATPROGRAM_USER", null=True, blank=True)
    rights_owner = models.ForeignKey(to="organizations.Organization", related_name="registration_certificates", db_column="F_ORGANIZATION_ID", verbose_name=u"Организация-правообладатель", blank=True, null=True)

    nominative_en = "certificate"
    nominative_short = u"свидетельство"
    genitive = u"свидетельства о регистрации прав на ПО"
    genitive_short = u"свидетельства"
    genitive_plural_full = u"свидетельств о регистрации прав на ПО"
    accusative_short = u"свидетельство"
    instrumental = u"свидетельством о регистрации прав на ПО"
    locative = u"свидетельстве о регистрации прав на ПО"
    gender = 'neuter'

    class Meta:
        db_table = "PATPROGRAM"
        verbose_name = u"свидетельство о регистрации прав на ПО"
        verbose_name_plural = u"свидетельства о регистрации прав на ПО"
        get_latest_by = "date"
        ordering = ('-date', '-number')

    def __unicode__(self):
        return "%s (# %s)" % (self.title, self.number)

    @models.permalink
    def get_absolute_url(self):
        return ('certificates_detail', (), {'object_id': self.id})


class RegistrationCertificateAuthorship(AuthorshipPositionedModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORPOG_ID")
    author = models.ForeignKey(to="workers.Worker", related_name="certificate_authorships", db_column="F_MAN_ID", null=True, blank=True)
    certificate = models.ForeignKey(to="RegistrationCertificate", related_name="authorships", db_column="F_PATPROGRAM_ID")
    original_name = models.CharField(max_length=255, db_column="F_AUTHORPOG_NAME", blank=True)
    position = models.IntegerField(db_column="F_AUTHORPOG_ORD", default=0, null=True, blank=True)
    creator = models.ForeignKey(to=User, related_name="certificate_authorships_added", db_column="F_AUTHORPOG_USER", null=True, blank=True)

    class Meta(AuthorshipPositionedModel.Meta):
        db_table = "AUTHORPOG"
        verbose_name = u"авторство свидетельства о регистрации"
        verbose_name_plural = u"авторства свидетельств о регистрации"

    def __unicode__(self):
        return u"Авторство сертификата '%s', %s" % (self.certificate.title, self.workers_string_full)

from certificates.admin import *
