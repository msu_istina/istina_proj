# -*- coding: utf-8 -*-
from django.db import models
from common.models import MyModel
from common.models import Country
from django.core.validators import MaxLengthValidator

SMI_CATEGORY_CHOICES = (
    (None, u"--- Выберите тип ---"),
    ("radio", u"Радиостанция"),
    ("tv", u"Телеканал"),
    ("newspaper", u"Газета"),
    ("magazine", u"Журнал"),
    ("internet", u"Интернет издание"),
    )

SMI_META_CATEGORIES = {
    'audiovisual': ['radio', 'tv'],
    'printed': ['newspaper', 'magazine'],
    'internet': ['internet'],
    }

SMI_AUTHOR_ROLE_CHOICES = (
    ("author", u"Автор"),
    ("program_manager", u"Ведущий"),
    ("talkshow_guest", u"Участник дискуссии"),
    )

SMI_EVENT_CATEGORY_CHOICES = (
    (None, u"--- Выберите тип ---"),
    ("article", u"Статья"),
    ("internet_article", u"Статья в Интернет издании"),
    ("interview", u"Интервью в печатных СМИ"),
    ("interview_radio", u"Интервью в медийных СМИ"),
    ("public_lecture", u"Лекция"),
    ("talkshow", u"Участие в дискуссии"),
    ("comment", u"Комментарий в печатных СМИ"),
    ("comment_radio", u"Комментарий в медийных"),
    ("program_manager", u"Ведущий постоянной программы"),
    ("other", u"Прочие выступления"),
    )


class SMI(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SMI_ID")
    category = models.CharField(u"Тип СМИ", max_length=64, choices=SMI_CATEGORY_CHOICES, default=None, db_column="F_SMI_TYPE")
    name = models.CharField(u"Название СМИ", max_length=255, db_column="F_SMI_NAME")
    country = models.ForeignKey(Country, related_name="massmedias", db_column="F_COUNTRY_ID", null=False, blank=False)
    url = models.CharField(u"Адрес в Интернет", max_length=1000, db_column="F_SMI_URL", blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="smi_added", db_column="F_SMI_USER", null=True, blank=True)

    class Meta:
        db_table = "SMI"
        verbose_name = u"Средство массовой информации"
        verbose_name_plural = u"Средства массовой информации"
        ordering = ("name",)

    def __unicode__(self):
        for (k, v) in SMI_CATEGORY_CHOICES:
            if k == self.category:
                return u'%s "%s"' % (v, self.name)
        return u'%s "%s"' % (self.category, self.name)

class SMIEvent(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SMIEVENT_ID")
    name = models.CharField(u"Название статьи", max_length=255, blank=False, null=False, db_column="F_SMIEVENT_NAME")
    category = models.CharField(u"Тип выступления в СМИ", max_length=64, choices=SMI_EVENT_CATEGORY_CHOICES, default=None, db_column="F_SMIEVENT_TYPE")
    creator = models.ForeignKey(to="auth.User", related_name="smievents_added", db_column="F_SMIEVENT_USER", null=True, blank=True)
    smi = models.ForeignKey(to=SMI, verbose_name=u"СМИ", related_name="events", db_column="F_SMI_ID", null=False, blank=False)
    date = models.DateField(u"Дата выхода", db_column="F_SMIEVENT_DATE", blank=False, null=False)
    issue = models.CharField(u"Номер, выпуск", max_length=6, db_column="F_SMIEVENT_ISSUE", blank=True)
    time = models.CharField(u"Время выхода в эфир", max_length=6, db_column="F_SMIEVENT_TIME", blank=True)
    duration = models.FloatField(u"Продолжительность (мин)", help_text=u'', db_column="F_SMIEVENT_DURATION", blank=True)
    redpages = models.FloatField(u"Примерный объём (авт. лист.)", help_text=u'Один авторский лист равен 40 000 знаков. Разделитель - точка. Например, 0.2', db_column="F_SMIEVENT_REDPAGES", blank=True)
    keywords = models.CharField(u"Ключевые слова", max_length=1000, db_column="F_SMIEVENT_KEYWORDS", blank=True)
    description = models.TextField(u"Краткое описание", validators=[MaxLengthValidator(1900)], db_column="F_SMIEVENT_DESCRIPTION", blank=True)
    url = models.URLField(u"Адрес в Интернет", help_text=u"Если текст или медиа файл Вашего выступления есть в Интернет, Вы можете указать адрес соответствующей страницы.", max_length=1000, db_column="F_SMIEVENT_URL", blank=True)

    gender = "neuter"
    
    class Meta:
        db_table = "SMIEVENT"
        verbose_name = u"Выступление в СМИ"
        verbose_name_plural = u"Выступления в СМИ"

class AuthorSMI(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORSMI_ID")
    event = models.ForeignKey(to=SMIEvent, related_name="authors", db_column="F_SMIEVENT_ID")
    role = models.CharField(u"Роль", max_length=255, db_column="F_AUTHORSMI_ROLE", blank=True)
    name = models.CharField(u"ФИО", max_length=2000, db_column="F_AUTHORSMI_NAME", blank=True)
    titles = models.CharField(u"Роль", max_length=2000, db_column="F_AUTHORSMI_TITLES", blank=True)
    author = models.ForeignKey(to='workers.Worker', related_name="smievents_authorships", db_column="F_MAN_ID", null=True, blank=True)
    sequence_number = models.IntegerField(u"Порядковый номер", db_column="F_AUTHORSMI_ORD", blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="smiauthors_added", db_column="F_AUTHORSMI_USER", null=True, blank=True)

    class Meta:
        db_table = "AUTHORSMI"
        verbose_name = u"Авторство выступления в СМИ"
        verbose_name_plural = u"Авторства выступлений в СМИ"
        ordering = ("sequence_number",)
