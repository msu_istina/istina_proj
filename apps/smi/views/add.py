# -*- coding: utf-8; -*-
import logging
from django.contrib import messages
from django.shortcuts import redirect, render, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from common.views import LinkedToWorkersModelWizardView, MyModelWizardView, detail as common_detail_view
from unified_permissions.decorators import permission_required
from unified_permissions import has_permission

from workers.models import Worker

from smi.models import SMI, SMIEvent, AuthorSMI
from smi.models import SMI_AUTHOR_ROLE_CHOICES, SMI_META_CATEGORIES
from smi.forms import SMIEventEditForm, SMIForm, SMISearchForm, AuthorSMIForm

import sys
import logging
logger = logging.getLogger('sentry_debug')


def find_display_name(choices, key, default=None):
    for (k, v) in choices:
        if k == key:
            return v
    return default

def get_meta_category(event_category):
    if event_category in ['article', 'comment', 'interview', 'interview_press']:
        return 'printed'
    elif event_category in ['internet_article']:
        return 'internet'
    elif event_category in ['interview_radio', 'public_lecture', 'talkshow', 'comment_radio', 'program_manager']:
        return 'audiovisual'
    return None


@login_required
def choose_smi(request, event_category):
    meta_category = get_meta_category(event_category)
    my_smi = SMI.objects.filter(Q(creator=request.user), category__in=SMI_META_CATEGORIES[meta_category])
    url_name = "smi_add_event"
    smi_form = None
    if request.method == 'POST' and 'create_smi' in request.POST:
        smi_form = SMIForm(request.POST)
        if smi_form.is_valid():
            theSMI = smi_form.save(request, commit=False)
            theSMI.creator = request.user
            theSMI.save()
            return redirect(reverse(url_name, kwargs = {'smi_id': theSMI.id, 'event_category': event_category}))
        else:
            messages.error(request, u"Пожалуйста, введите полную информацию о СМИ, которое Вы хотите добаить.")
    if request.method == 'POST' and 'select_smi' in request.POST:
        smi_search_form = SMISearchForm(request.POST)
        if smi_search_form.is_valid():
            theSMI = smi_search_form.cleaned_data['smi']
            if theSMI:
                return redirect(reverse(url_name, kwargs = {'smi_id': theSMI.id, 'event_category': event_category}))
            messages.error(request, u"Название СМИ не выбрано. Пожалуйста, выберите название СМИ из списка или введите информацию о новом СМИ.")
    smi_form = smi_form or SMIForm(meta_category=get_meta_category(event_category))
    smi_search_form = SMISearchForm(meta_category=get_meta_category(event_category))
    context = {
        'search_form': smi_search_form,
        'search_tab_available': smi_search_form.queryset.exists(),
        'form': smi_form,
        'my_smi': my_smi,
        'event_category': event_category,
        }
    return render(request, "smi/add_choose_smi.html",
                  context)


def extract_authors_info(request):
    """Extract information about all smievent authors from request.POST. Returns dict for save_authors."""
    authors = []
    for seq in request.POST.getlist('au_sequence_numbers', []):
        author_info = {}
        for field_name in ['au_id', 'au_name', 'au_role', 'au_role_name', 'au_comment']:
            field_name_seq = field_name + "_" + seq
            value = request.POST.get(field_name_seq, '')
            author_info[field_name[3:]] = value # drop au_ prefix from field_name
        authors.append(author_info)
    return authors

def save_authors(event, authors, creator=None):
    event.authors.all().delete()
    for (author_seq, author_info) in enumerate(authors):
        worker_id = author_info['id']
        author_worker = None
        try:
            author_worker = Worker.objects.get(pk=int(worker_id))
        except (TypeError, ValueError, Worker.DoesNotExist):
            pass
            #author_worker = Worker.objects.create_from_fullname(author_info['name'])
            #print "New worker with id=%s created" % author_worker.id
        author = AuthorSMI.objects.create(event=event,
                                          role=author_info['role'],
                                          name=author_info['name'] if not author_worker else None,
                                          titles=author_info['comment'],
                                          author=author_worker,
                                          sequence_number=author_seq,
                                          creator=creator)
    return True


@login_required
def add_event(request, smi_id, event_category=None):
    theSMI = get_object_or_404(SMI, pk=smi_id)
    worker = request.user.get_profile().worker
    meta_category = get_meta_category(event_category)
    authors = []
    if request.method == 'POST':
        authors = extract_authors_info(request)
        event_form = SMIEventEditForm(request.POST, smi=theSMI, meta_category=meta_category)
        if event_form.is_valid():
            event = event_form.save(request, commit=False)
            event.category=event_category
            event.save()
            save_authors(event, authors, creator=request.user)
            messages.success(request, u"Информация о выступлении сохранена.")
            return redirect("smi_home")
        else:
            messages.error(request, u"Некорректные данные")
    else:
        event_form = SMIEventEditForm(smi=theSMI, meta_category=meta_category)
        # This is a new event, suggest current user as an author
        authors.append({'id': worker.id, 'name': worker,
                        'role': u'author', 'role_name': find_display_name(SMI_AUTHOR_ROLE_CHOICES, 'author')})

    author_form = AuthorSMIForm()
    context = {'form': event_form, 'smi': theSMI, 'author_form': author_form, 'authors': authors}
    return render(request, "smi/add_article.html",
                  context)


@login_required
def edit_event(request, smievent_id):
    theEvent = get_object_or_404(SMIEvent, pk=smievent_id)
    theSMI = theEvent.smi
    meta_category = get_meta_category(theEvent.category)

    event_form = SMIEventEditForm(request.POST or None, smi=theSMI, meta_category=meta_category, instance=theEvent)
    author_form = AuthorSMIForm()

    authors = []
    if request.method == 'POST':
        authors = extract_authors_info(request)
        if event_form.is_valid():
            event_form.save(request)
            #authors = save(request, event=theEvent, smi=theSMI, meta_category=meta_category, event_type=theEvent.category)
            save_authors(theEvent, authors, creator=request.user)
            return redirect("smi_home")
    else:
        # Load authors from DB
        for a in theEvent.authors.all():
            authors.append({'id': a.author.id if a.author else None, 'name': a.name or a.author.fullname,
                            'role': a.role, 'role_name': find_display_name(SMI_AUTHOR_ROLE_CHOICES, a.role, default='')})

    context = {'form': event_form, 'smi': theSMI,
               'author_form': author_form, 'authors': authors}
    return render(request, "smi/add_article.html",
                  context)


@login_required
def delete_event(request, smievent_id):
    theEvent = get_object_or_404(SMIEvent, pk=smievent_id)
    if request.user == theEvent.creator:
        event_name = theEvent.name
        theEvent.delete()
        messages.success(request, u"Выступление <b>%s</b> удалено." % event_name)
    else:
        messages.error(request, u"Выступление было добавлено другим пользователем, Вы не можете его удалить.")
    return redirect("smi_home")



