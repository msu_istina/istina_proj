# -*- coding: utf-8; -*-
import logging
from django.contrib import messages
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required

from smi.models import SMIEvent

@login_required
def browse_events(request):
    context = {
        'events': SMIEvent.objects.filter(creator=request.user),
        }
    return render(request, "smi/browse_events.html",
                  context)
