# -*- coding: utf-8; -*-
from django.views.generic.simple import direct_to_template
from django.conf.urls.defaults import *
from common.views import LinkedToWorkersModelWizardView as Wizard

urlpatterns = patterns('smi.views',
    url(r'^$', 'browse_events', name='smi_home'),
    # add
    url(r'^add/$', direct_to_template, {'template': 'smi/add_choice.html'}, name='smi_add'),
    url(r'^add/(?P<event_category>[A-Za-z_]+)/$', 'choose_smi', name='smi_add_event'),
    url(r'^add/(?P<event_category>[A-Za-z_]+)/(?P<smi_id>\d+)/$', 'add_event',  name='smi_add_event'),
    url(r'^edit/(?P<smievent_id>\d+)/$', 'edit_event', name='smi_edit_event'),
    url(r'^delete/(?P<smievent_id>\d+)/$', 'delete_event', name='smi_delete_event'),
    # XHR tools
    url(r'^worker_lookup/$', 'xhr_get_worker_name', name='smi_get_worker_name'),
)
