# -*- coding: utf-8; -*-

from django import forms
from common.forms import MyModelForm, MyForm
from common.forms import Select2SingleWorkerField
from common.forms import CountryChoiceField
from common.widgets import MyFilteredSelectMultiple
from common.widgets import FilteredSelect
from common.widgets import Select2ChoiceWidget, LightSelect2ChoiceWidget
from common.models import Country
from .models import SMI, SMIEvent
from .models import SMI_CATEGORY_CHOICES, SMI_AUTHOR_ROLE_CHOICES, SMI_META_CATEGORIES

class SMISearchForm(MyForm):
    queryset = SMI.objects.none()
    smi = forms.ModelChoiceField(label=u'Название СМИ',
                                 widget=LightSelect2ChoiceWidget(width=600),
                                 queryset=SMI.objects.none(),
                                 required=False)
    alias = forms.CharField(label=u"Уточненное название", required=False)

    def __init__(self, *args, **kwargs):
        meta_category = None
        try:
            meta_category = kwargs.pop('meta_category')
        except KeyError:
            pass
        super(SMISearchForm, self).__init__(*args, **kwargs)
        self.queryset=SMI.objects.none()
        if meta_category in SMI_META_CATEGORIES.keys():
            self.queryset=SMI.objects.filter(category__in=SMI_META_CATEGORIES[meta_category])
        else:
            self.queryset=SMI.objects.all()
        self.fields['smi'].queryset = self.queryset


class SMIForm(MyModelForm):
    country = CountryChoiceField(label=u'Страна')
    class Meta:
        model = SMI
        exclude = ('creator',)

    def __init__(self, *args, **kwargs):
        meta_category = None
        try:
            meta_category = kwargs.pop('meta_category')
        except KeyError:
            pass
        super(SMIForm, self).__init__(*args, **kwargs)

        try:
            self.fields['country'].initial = Country.objects.get(name='Russia')
        except (KeyError, Country.DoesNotExist):
            pass

        choices = SMI_CATEGORY_CHOICES
        if meta_category:
            choices = [t for t in SMI_CATEGORY_CHOICES if (t[0] in SMI_META_CATEGORIES[meta_category] or t[0] is None)]
        self.fields['category'].choices = choices

        self.fields['url'].help_text = u'Пожалуйста, укажите адрес издания, а не конкретно Вашей статьи. Например, http://elpais.com/.'


class AuthorSMIForm(forms.Form):
    worker = Select2SingleWorkerField(label=u'Руководители этапа НИР', initial='', input_string_option=True)
    fio = forms.CharField(label=u"ФИО")
    comment = forms.CharField(label=u"Место работы")
    role = forms.ChoiceField(label=u"Роль", choices=SMI_AUTHOR_ROLE_CHOICES)

class SMIEventEditForm(MyModelForm):
    '''A simple form for paper article edit.'''
    css_classes = [
        ('keywords', 'wide'),
        ('description', 'wide'),
        ('url', 'wide'),
        ]
    required_css_class = 'required'

    # Maps meta_category names to list of inrelevant fields
    inrelevant_fields = {
        'printed': ['duration', 'time'],
        'audiovisual': ['redpages', 'issue'],
        'internet': ['duration', 'time', 'issue'],
        }

    class Meta:
        model = SMIEvent
        exclude = ('creator', 'category')

    def __init__(self, *args, **kwargs):
        def get_kwarg(kwargs, name, default=None):
            try:
                return kwargs.pop(name)
            except KeyError:
                return default
        selected_smi = get_kwarg(kwargs, 'smi')
        meta_category = get_kwarg(kwargs, 'meta_category')
        super(SMIEventEditForm, self).__init__(*args, **kwargs)

        self.fields['date'].widget.attrs['placeholder']=u'23.04.2011'
        self.fields['description'].widget.attrs['rows'] = 4
        self.fields['description'].widget.attrs['style'] = 'height: 4em;'

        if selected_smi:
            self.fields['smi'].choices = ((selected_smi.id, selected_smi),)
            self.fields['smi'].widget.attrs['disabled'] = True
            self.fields['smi'].widget = forms.HiddenInput()
            self.fields['smi'].initial = selected_smi
        if meta_category:
            for field_name in self.inrelevant_fields.get(meta_category, []):
                self.fields.pop(field_name)
            if meta_category == 'printed':
                self.fields['date'].label = u'Дата публикации'
            elif meta_category == 'audiovisual':
                self.fields['time'].widget.attrs['placeholder']=u'13:35'
            elif meta_category == 'internet':
                for field_name in ['url', 'date']:
                    self.fields[field_name].required = True
                self.fields['url'].help_text = u'Пожалуйста, укажите адрес страницы с Вашей публикацией.'

    def clean(self):
        return super(SMIEventEditForm, self).clean()
