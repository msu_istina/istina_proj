# -*- coding: utf-8; -*-
__author__ = 'Vladimir'
import authority
from django.db.models import Q
from common.permissions import MyModelPermission
from organizations.models import Organization, Department, DepartmentRelation, Representative


class OrganizationPermission(MyModelPermission):
    label = 'organization_permission'
    checks = ('preemptive_check', 'unified_check',)
    operations = (
        ("view_councils_stats", u"может просматривать статистику дисс. советов", False),
        ("view_stats", u"может просматривать статистику организации", False),
        ("view_top_workers_by_specialty", u"может просматривать списки топ-сотрудников по специальностям", False),
    )

class DepartmentPermission(MyModelPermission):
    label = 'department_permission'
    checks = ('preemptive_check', 'unified_check', )
    operations = (
        ("confirm_projects", u"может подтверждать проекты", False),
        ("confirm_achievements", u"может подтверждать достижения", False),
        ("department_parameters_view", u"может просматривать параметры подразделения", False),
        ("department_parameters_edit", u"может редактировать параметры подразделения", False),
        ("view_stats", u"может просматривать статистику подразделения", False),
    )

    def unified_check(self, operation, department):
        super_unified_check = super(DepartmentPermission, self).unified_check
        return super_unified_check(operation, department) \
            or any([super_unified_check(operation, ancestor) for ancestor in department.ancestors]) \
            or OrganizationPermission(self.user).unified_check(operation, department.organization)


authority.register(Organization, OrganizationPermission)
authority.register(Department, DepartmentPermission)