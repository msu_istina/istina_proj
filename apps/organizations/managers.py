# -*- coding: utf-8 -*-

from common.managers import MyManager
from common.utils.user import get_worker
from django.contrib.auth.models import User, Group
from django.db.models import Q
from common.models import get_related_fields
from common.utils.user import mail_users

class OrganizationManager(MyManager):
    '''Model manager for Organization model.'''

    def get_msu(self):
        msu, created = self.get_or_create(name=u"МГУ имени М.В. Ломоносова")
        return msu


class DepartmentManager(MyManager):
    '''Model manager for Department model.'''

    def merge(self, from_id, to_id):
        dep_from = self.get(id=from_id)
        dep_to = self.get(id=to_id)
        # move all related objects
        # this is done not to accidentally delete related objects
        # that we forgot to move manually
        # due to cascade delete by default in django
        for related_name, field_name in get_related_fields(dep_from):
            print related_name, field_name,
            try:
                # e.g. worker_from.reports.all().update(author=worker_to)
                getattr(dep_from, related_name).all().update(**{field_name: dep_to})
                print "OK"
            except:
                # this shouldn't happen, but it happens for manually created tables
                print "FAIL"


class RepresentativeManager(MyManager):
    '''Model manager for Representative model.'''

    def get_for_worker(self, worker, organization_level=False):
        ''' Returns representatives that are relevant to the worker and active.
            If there are no employments, return all active representatives linked to user.
        '''
        employments = worker.employments.all()
        if not employments:
            if not organization_level: # linked to departments
                return self.filter(department__isnull=False, is_active=True)
            else:
                return self.filter(organization__isnull=False, department__isnull=True, is_active=True)
        departments, departments_ids = zip(*[(emp.department, emp.department.id) for emp in employments])
        ancestors = []
        for department in departments:
            ancestors.extend(department.get_ancestors_flat())
        ancestors_ids = [ancestor.id for ancestor in ancestors]
        all_departments_ids = set(list(departments_ids) + ancestors_ids)
        if not organization_level: # linked to departments
            return self.filter(department__in=all_departments_ids, is_active=True)
        else: # linked to the whole organizations
            organizations_ids = set([department.organization.id for department in list(departments) + ancestors])
            return self.filter(organization__in=organizations_ids, department__isnull=True, is_active=True)

    def get_for_user(self, user, organization_level=False):
        ''' Returns representatives that are relevant to the worker and active.
            If there are no employments, return all active representatives.
        '''
        worker = get_worker(user)
        if worker:
            return self.get_for_worker(worker, organization_level)
        else:
            return self.none()

    def get_for_all(self, organization_level=False):
        '''Returns all representatives, ordered by department.'''
        if not organization_level: # linked to departments
            return self.filter(department__isnull=False, is_active=True)
        else:
            return self.filter(organization__isnull=False, department__isnull=True).exclude(is_active=False)

    def get_active_users(self):
        '''Returns a list of User objects, which are active representatives (representative.is_active=True for them).'''
        user_ids = self.filter(is_active=True, user__isnull=False).values_list('user', flat=True)
        return User.objects.filter(id__in=user_ids)

    def update_representatives_group(self):
        '''Updates a special representatives Group with all current active representatives (and excludes others).'''
        group, _ = Group.objects.get_or_create(name=u"Ответственные")
        users_in_group = set(group.user_set.all())
        users_current = set(self.get_active_users())
        # remove old users
        for user in users_in_group - users_current:
            user.groups.remove(group)
        # add new users
        for user in users_current - users_in_group:
            user.groups.add(group)
        print "%d users removed, %d users added. Now contains %d users." % (len(users_in_group - users_current),
            len(users_current - users_in_group), group.user_set.count())

    def create_sql_statement_for_departments_dynatree(self, user):
        #214524 - id of MSU
        departments_sql = """
                select d.f_department_id as f_department_id,
                       d.f_department_name as f_department_name,
                       dl.dep_f_department_id as parent,
                       level,
                       SYS_CONNECT_BY_PATH(d.f_department_id, '/') as url
                from
                    (select f_department_id, dep_f_department_id from departmentlink
                        union all
                    select f_department_id, NULL from department d2 where not exists
                        (select 1 from departmentlink dl2 where dl2.f_department_id=d2.f_department_id)
                    ) dl, department d
                where  d.f_department_id = dl.f_department_id
                    and d.f_organization_id in (select NVL(representative.f_organization_id, department.f_organization_id)
                                                from representative
                                                     left join department on (department.f_department_id = representative.f_department_id)
                                                where f_representative_user = %(user_id)s
                                                  and (representative.f_organization_id IS NOT NULL OR
                                                       department.f_organization_id IS NOT NULL))
                """ % {'user_id': user.id }
        if None not in self.all().values_list('department', flat=True):
            department_descendants_string_list = "("
            for representative in self.all():
                for department_descendant_id in representative.department.descendants_ids(include_self=True):
                    department_descendants_string_list += str(department_descendant_id) + ", "
            if department_descendants_string_list != "(":
                department_descendants_string_list = department_descendants_string_list[:-2] + ")"
                departments_sql += """
                    and (dl.dep_f_department_id in """ + department_descendants_string_list + \
                    """ or d.f_department_id in """ + department_descendants_string_list + """)"""
        departments_sql += """
                start with dl.dep_f_department_id is null
                connect by PRIOR dl.f_department_id = dl.dep_f_department_id
                order siblings by d.f_department_name"""
        return departments_sql

    def organization_level(self):
        '''This function returns True if user is representative on the Organization level.'''
        return self.filter(department__isnull=True).exists()

    def notify_about_missing_info(self, really_send=False):
        '''This function sends emails to all representatives, who has missing information.'''
        def analyze_for_missing_parts(local_representative):
            missing_parts = []
            for field in ["scope", "post", "email", "phone_number", "address"]:
                if not getattr(local_representative, field):
                    missing_parts.append(local_representative._meta.get_field_by_name(field)[0].verbose_name)
            return missing_parts

        subject = u"Заполнение данных ответственных сотрудников в системе ИСТИНА"
        user_representatives = self.filter(user__isnull=False).values_list("user", flat=True)
        for user in User.objects.filter(id__in=user_representatives):
            missing_data = []
            sending_enabled = False
            for representative in user.representatives.all():
                missing_fields = analyze_for_missing_parts(representative)
                if missing_fields:
                    sending_enabled = True
                    missing_data.append({"representative": representative, "empty_fields": missing_fields})
            if sending_enabled:
                extra_context = {"missing_data_list": missing_data}
                mail_users([user], subject,
                                  template_filename="organizations/emails/representatives_notify_emails.txt",
                                  extra_context=extra_context,
                                  really_send=really_send)


class DepartmentParameterCategoryManager(MyManager):
    '''Model manager for DepartmentParameterCategory model.'''

    HIDDEN_CATEGORIES = (u"Направление",)

    def get_for_organization(self, organization):
        return self.filter(Q(organization=organization) | Q(organization__isnull=True)).exclude(name__in=self.HIDDEN_CATEGORIES)
