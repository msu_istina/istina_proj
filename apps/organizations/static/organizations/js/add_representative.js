/**
 * Created by Vladimir on 9/22/2014.
 */
$(function() {

    // remove submit on enter for worker autocomplete field
    $(".autocomplete_workers").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            return false
        }
        return true
    })
    // add autocomplete for workers and other fields
    $(".autocomplete_workers").autocomplete(generate_workers_autocomplete_options("/workers/search/"))

})