$(function() {
    $("li[class^='level']").each(function() {
        var level = $(this).attr("class").split("-")[1]
        $(this).attr("style", "margin-left: " + (level-1)*50 + "px")
    });
});
