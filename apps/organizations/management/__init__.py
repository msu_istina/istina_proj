from django.db.models.signals import post_syncdb
import organizations.models

def get_msu_callback(sender, **kwargs):
    organizations.models.Organization.objects.get_msu()

post_syncdb.connect(get_msu_callback, sender=organizations.models)