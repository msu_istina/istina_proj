# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from common.utils.user import get_fullname
from workers.models import Worker
from organizations.models import Department, Representative
from os import path
from settings import ENV_DIR
from time import sleep


class Command(BaseCommand):
    def handle(self, *args, **options):
        '''Send a mail to all representatives that are not registered yet.'''
        subject = u"Информация о назначении ответственным по системе Наука-МГУ (ИСТИНА)"
        from_email = u"ИСТИНА <info@istina.msu.ru>"
        logfile = open(path.join(ENV_DIR, 'var', 'log', 'email_representatives_unregistered.log'), 'w')
        template = 'organizations/emails/representatives_unregistered.txt'
        reps = Representative.objects.filter(user__isnull=True)
        for rep in reps:
            name = " ".join(rep.comment.split()[1:])
            message = render_to_string(template, {'name': name, 'department': rep.department_str})
            # send_mail(subject, message, from_email, [rep.email])
            log_message = u"%s %s %s\n" % (name, rep.email, rep.department_str)
            logfile.write(log_message.encode('utf8'))
            print log_message,
            print message
            sleep(1)
        logfile.close()
