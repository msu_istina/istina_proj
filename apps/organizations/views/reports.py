# -*- coding: utf-8; -*-
from django.utils.log import getLogger
from django.shortcuts import get_object_or_404
from statistics.views.stats import get_report, format_report
from organizations.models import Department
from workers.models import ManRank, ManDegree, Worker
from dissertations.models import Dissertation
from copy import copy
from unified_permissions.decorators import permission_to_department_required

logger = getLogger("organizations.views.reports")

def get_degrees(workers_ids):
    workers = copy(workers_ids) # not to alter original workers_ids
    # academics
    academics = set(ManRank.objects.filter(worker__in=workers, academy__isnull=False).values_list('worker', flat=True))
    workers -= academics
    # doctors: via degree + via dissertation
    # should we count academics as doctors, or the sets should not overlap?
    doctors = set(ManDegree.objects.filter(worker__in=workers, degree__is_degree_prior='N').values_list('worker', flat=True))
    workers -= doctors
    # add doctors via dissertation
    doctors |= set(Dissertation.objects.filter(author__in=workers, phd='N').values_list('author', flat=True))
    workers -= doctors
    # candidates: via degree + via dissertation
    candidates = set(ManDegree.objects.filter(worker__in=workers, degree__is_degree_prior='Y').values_list('worker', flat=True))
    workers -= candidates
    candidates |= set(Dissertation.objects.filter(author__in=workers, phd='Y').values_list('author', flat=True))
    return academics, doctors, candidates

@permission_to_department_required("view_stats")
def monitoring_degrees(request, department_id, export_excel=False):
    title = u"Сведения о научных и научно-педагогических работниках (Приложение 1)"
    headers = [u'№ п\п', u'Показатель', u'Значение']
    keys = [u'Всего', u'Академики и член-корреспонденты государственных академий',
        u'Доктора наук', u'Кандидаты наук']
    department = get_object_or_404(Department, pk=department_id)
    workers = set(department.get_workers(return_ids=True))
    workers_count = len(workers)
    academics, doctors, candidates = get_degrees(workers)
    values = [workers_count, len(academics), len(doctors), len(candidates)]
    rows = zip(range(1, len(keys)+1), keys, values)
    context = {'department': department}
    excel_args = {'export_excel': True, 'excel_sheetname': u'Приложение_1',
        'excel_filename': 'monitoring-2013-degrees.%s.xls' % department_id} if export_excel else {}
    return format_report(request, title, headers, rows, context, **excel_args)

@permission_to_department_required("view_stats")
def degrees(request, department_id, all_workers=False, export_excel=False):
    if all_workers:
        title = u"Список сотрудников с указанием степеней и званий"
    else:
        title = u"Список академиков, докторов и кандидатов наук"
    headers = [u'№ п\п', u'ФИО', u'Статус', u'Адрес страницы']
    department = get_object_or_404(Department, pk=department_id)
    workers_ids = set(department.get_workers(return_ids=True))
    academics, doctors, candidates = get_degrees(workers_ids)
    rows = []
    i = 1
    if all_workers:
        workers = Worker.objects.filter(id__in=workers_ids)
    else:
        workers = Worker.objects.filter(id__in=academics|doctors|candidates)
    for worker in workers:
        if worker.id in academics:
            status = u"академик"
        elif worker.id in doctors:
            status = u"доктор"
        elif worker.id in candidates:
            status = u"кандидат"
        else:
            status = ""
        rows.append((i, worker.fullname, status, worker.get_full_url()))
        i += 1
    context = {'department': department}
    excel_sheetname = u"Список_сотрудников" if all_workers else u'Список_академиков_докторов'
    excel_filename = ('report-workers-degrees.%s.xls' if all_workers else 'report-degrees.%s.xls') % department_id
    excel_args = {'export_excel': True, 'excel_sheetname': excel_sheetname,
        'excel_filename': excel_filename} if export_excel else {}
    return format_report(request, title, headers, rows, context, **excel_args)

@permission_to_department_required("view_stats")
def monitoring_books(request, department_id, startyear, endyear, export_excel=False):
    startyear = int(startyear)
    endyear = int(endyear)
    title = u"Монографии, изданные за %d-%d годы (Приложение 4)" % (startyear, endyear)
    headers = [u'№ п\п', u'Полное библиографическое описание', u'Объем (п.л.)', u'Тираж (экз.)', u'Электронный адрес размещения']
    make_row = lambda i, work: [i, 'bibtex', work.redpages or '', work.ncopies or '', work.url]
    filters = u"((category = 'Монография' or category = 'Учебное пособие') and year>%d and year<%d)" % (startyear-1, endyear+1)
    excel_args = {'export_excel': True, 'excel_sheetname': u'Приложение_4',
        'excel_filename': 'monitoring-2013-books.%s.xls' % department_id} if export_excel else {}
    return get_report(request, title, headers, make_row,
        department_id=department_id, bibtex_column=1, filters=filters, books=True, **excel_args)

monitoring_publications_headers = [
        u'№ п\п',
        u'Полное библиографическое описание статьи',
        u'Библиографическая база, в которой индексируется журнал',
        u'Входит в Перечень ведущих рецензируемых научных журналов и изданий, рекомендованный ВАК',
        u'Импакт-фактор журнала',
        u'Количество цитирований',
        u'Электронный адрес размещения',
        u'Цифровой идентификатор объекта DOI']

@permission_to_department_required("view_stats")
def monitoring_publications_indexed(request, department_id, startyear, endyear, export_excel=False):
    startyear = int(startyear)
    endyear = int(endyear)
    title = u'Список публикаций %d-%d годов в журналах, индексируемых в международных системах цитирования (Приложение 5)' % (startyear, endyear)
    headers = monitoring_publications_headers
    make_row = lambda i, work: [i, '', work.external_system, u"да", work.impact_factor, work.citation_count, work.url, work.get_doi_str]
    # search by journal in systems, so no russian articles at all
    filters = """(
        (in_scopus = 1 or in_wos = 1
        or wos_id is not null or scopus_id is not null
        or impact_wos is not null or impact_scopus is not null)
        and year>%d and year<%d)
    """ % (startyear-1, endyear+1)
    excel_args = {'export_excel': True, 'excel_sheetname': u'Приложение_5',
        'excel_filename': 'monitoring-2013-publications-indexed.%s.xls' % department_id} if export_excel else {}
    return get_report(request, title, headers, make_row,
        department_id=department_id, bibtex_column=1, filters=filters, **excel_args)

@permission_to_department_required("view_stats")
def monitoring_publications_vak(request, department_id, startyear, endyear, export_excel=False):
    startyear = int(startyear)
    endyear = int(endyear)
    title = u'Список публикаций %d-%d годов в научных журналах, входящих в Перечень ВАК (Приложение 6)' % (startyear, endyear)
    headers = monitoring_publications_headers
    make_row = lambda i, work: [i, '', u"РИНЦ", u"да", work.impact_rinc, "", work.url, work.get_doi_str]
    filters = 'in_vak = 1 and year>%d and year<%d' % (startyear-1, endyear+1) # here no english publications can be, because in_vak is orthogonal to in_scopus/wos
    excel_args = {'export_excel': True, 'excel_sheetname': u'Приложение_6',
        'excel_filename': 'monitoring-2013-publications-vak.%s.xls' % department_id} if export_excel else {}
    return get_report(request, title, headers, make_row,
        department_id=department_id, bibtex_column=1, filters=filters, **excel_args)

@permission_to_department_required("view_stats")
def monitoring_patents(request, department_id, startyear, endyear, export_excel=False):
    startyear = int(startyear)
    endyear = int(endyear)
    title = u"Патенты на изобретения, полученные в %d-%d годах (Приложение 7)" % (startyear, endyear)
    headers = [u'№ п\п', u'Полное библиографическое описание', u'Электронный адрес размещения']
    make_row = lambda i, work: [i, "bibtex", work.url]
    filters = u"(category = 'Патенты' and year>%d and year<%d)" % (startyear-1, endyear+1)
    post_process = lambda row: [row[0], row[1].replace("Tech. Rep", u"пат"), row[2]]
    excel_args = {'export_excel': True, 'excel_sheetname': u'Приложение_7',
        'excel_filename': 'monitoring-2013-patents.%s.xls' % department_id} if export_excel else {}
    return get_report(request, title, headers, make_row, department_id=department_id,
        bibtex_column=1, filters=filters, books=True, post_process=post_process,
        **excel_args)
