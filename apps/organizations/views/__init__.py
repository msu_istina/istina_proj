# -*- coding: utf-8; -*-
import datetime

from django.contrib.auth.decorators import login_required
from django.contrib import messages

from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404, render_to_response, render, redirect
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.utils import simplejson
from django.utils.log import getLogger
from django.utils.safestring import mark_safe
from django.views.generic import FormView, DetailView
from django.db.models import Q

from actstream import action
from paging.helpers import paginate
from common.utils.activities import get_activities_last_added, fetch_activities_compact
from common.utils.latex import latex_to_pdf

from organizations.models import Department, Organization, DepartmentParameterCategory, Representative
from organizations.forms import DepartmentParametersForm, RepresentativeCreateEditForm, RepresentativeEditYourselfForm

from django.template import RequestContext
from unified_permissions import has_permission
from unified_permissions.decorators import permission_to_department_required

logger = getLogger("organizations.views")


def department_get_tree(request, department_id):
    department = get_object_or_404(Department, pk=department_id)
    subtree = department.descendants()[1:] # exclude the department
    def transform_level (level):
        s = "--" * level
        if s:
            s += " "
        return s
    tree = [(subdepartment.id, transform_level(level - 1) + subdepartment.name) for (subdepartment, level) in subtree] # prepare data for putting into select
    data = {'tree': tree, 'empty': not bool(tree)}  # empty is needed for checking in javascript
    return HttpResponse(simplejson.dumps(data), content_type='application/javascript')


def department_publications_xml(request, department_id):
    '''Returns an xml file with all publications of the department.'''
    department = get_object_or_404(Department, pk=department_id)
    publications = department.publications
    return render_to_response('publications/list.xml', {'articles': publications['articles'], 'books': publications['books']}, mimetype="application/xhtml+xml")


def department_activities_2weeks(request, department_id, mode=None):
    '''Show a page with list of activities added by department's workers in last two weeks.'''
    department = get_object_or_404(Department, pk=department_id)
    two_weeks_ago = datetime.datetime.now() - datetime.timedelta(14)
    activities = get_activities_last_added(department.current_workers, from_date=two_weeks_ago, raw=True)

    if mode == 'pdf':
        date_from = two_weeks_ago
        date_to = datetime.datetime.today()
        same_year = date_to.year == date_from.year
        activities = fetch_activities_compact(activities)
        # fix activities nominative_short for proper latex hyphenation
        for activity, timestamp in activities:
            if activity["nominative_short"] == u"диссертация":
                activity["nominative_short"] = u"диссер\-тация"
            elif activity["nominative_short"] == u"преподавание курса":
                activity["nominative_short"] = u"препода\-вание курса"
            elif activity["nominative_short"] == u"свидетельство":
                activity["nominative_short"] = u"свидете\-льство"
            elif activity["nominative_short"] == u"инвестиция":
                activity["nominative_short"] = u"инвести\-ция"
        latex = render_to_string("organizations/activities_2weeks.tex",
                                {'activities': activities, 'department': department, 'date_from': date_from, 'date_to': date_to, 'same_year': same_year})
        pdf = latex_to_pdf(latex)
        response = HttpResponse(pdf, mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=department_2weeks_%d.pdf' % department.id
        return response

    context = paginate(request, activities, per_page=30, endless=False)
    context_p = context["paginator"]
    if "objects" in context_p:
        context_p["objects"] = fetch_activities_compact(context_p["objects"])
    context["paging"] = mark_safe(render_to_string('paging/pager.html', context))
    context["department"] = department
    return render(request, "organizations/activities_2weeks.html", context)


class DepartmentParametersFormView(FormView):
    form_class = DepartmentParametersForm
    template_name = 'organizations/department_parameters_edit.html'

    @permission_to_department_required("department_parameters_edit")
    def dispatch(self, request, *args, **kwargs):
        self.department = get_object_or_404(Department, pk=kwargs.get('department_id', None))
        self.year = kwargs.get('year', None)
        self.categories = DepartmentParameterCategory.objects.get_for_organization(self.department.organization)
        return super(DepartmentParametersFormView, self).dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = super(DepartmentParametersFormView, self).get_initial()
        for category in self.categories:
            parameter_value = category.parameters.filter(department=self.department, year=self.year).get_first_or_none()
            if parameter_value:
                initial[category.to_field_name] = parameter_value.value
        return initial

    def get_form_kwargs(self):
        kwargs = super(DepartmentParametersFormView, self).get_form_kwargs()
        kwargs.update({
            'department': self.department,
            'year': self.year,
            'categories': self.categories,
        })
        return kwargs

    def get_context_data(self, **kwargs):
        kwargs = super(DepartmentParametersFormView, self).get_context_data(**kwargs)
        kwargs['department'] = self.department
        kwargs['year'] = self.year
        return kwargs

    def form_valid(self, form):
        form.save()
        messages.success(self.request, u"Параметры подразделения успешно сохранены.")
        action.send(self.request.user, verb=u"отредактировал параметры подразделения", target=self.department, description=self.year)
        return super(DepartmentParametersFormView, self).form_valid(form)

    def get_success_url(self):
        return reverse('department_parameters', kwargs={'department_id': self.department.id})


class DepartmentParametersView(DetailView):
    model = Department
    context_object_name = 'department'
    template_name = 'organizations/department_parameters.html'
    pk_url_kwarg = 'department_id'

    @permission_to_department_required("department_parameters_view")
    def dispatch(self, request, *args, **kwargs):
        return super(DepartmentParametersView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs = super(DepartmentParametersView, self).get_context_data(**kwargs)
        year = datetime.date.today().year
        if datetime.date.today() < datetime.date(year, 7, 1):
            year -= 1
        years = range(year, year-5, -1)
        categories = DepartmentParameterCategory.objects.get_for_organization(self.object.organization)
        parameters = []
        for category in categories:
            parameters.append([category])
            for year in years:
                parameter = self.object.parameters.getdefault(category=category, year=year)
                value = parameter.value if parameter else ""
                parameters[-1].append(value)
        kwargs['parameters'] = parameters
        kwargs['years'] = years
        return kwargs

@login_required
def representative_add(request, level):
    '''This function allows you to add new representatives in the system'''
    if level == 'organization':
        if not request.user.representatives.organization_level():
            messages.warning(request, u'Эта страница доступна только для ответственных по организации. '
                                      u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.')
            return redirect("/home/")
        org_level = True
    elif level == 'department':
        if not has_permission(request.user, "edit_permissions_and_roles"):
            messages.warning(request, u'Эта страница доступна только для ответственных по подразделению. '
                                      u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.'
                                      u'Если Вы ответственный по организации, Вам требуется специальное разрешение на получение доступа.')
            return redirect("/home/")
        org_level = False
    else:
        raise Http404
    if request.method == "POST":
        redirect_url = request.GET.get("redirect_url")
        if request.POST.get("cancel_button"):
            return redirect(redirect_url)
        form = RepresentativeCreateEditForm(request.user, False, org_level, request.POST)
        if form.is_valid():
            form.save(request)
            return redirect("/unified_permissions/user/department/list/")
    else:
        redirect_url = request.META.get('HTTP_REFERER')
        form = RepresentativeCreateEditForm(request_user=request.user, org_level=org_level)
    return render(request, "organizations/representative_add_edit.html",
                  {'form': form, 'redirect_url': redirect_url,
                   'edit_mode': False, 'org_level': org_level, },
                  context_instance=RequestContext(request))

@login_required
def representative_edit(request, representative_id, level):
    '''This function allows you to edit Representative objects in the system'''
    instance = get_object_or_404(Representative, id=representative_id)
    if level == 'organization':
        if not request.user.representatives.organization_level():
            messages.warning(request, u'Эта страница доступна только для ответственных по организации. '
                                      u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.')
            return redirect("/home/")
        if not request.user.representatives.filter(department__isnull=True, organization=instance.organization):
                messages.warning(request, u'Эта страница доступна только для ответственных по организации "' +
                                  unicode(instance.organization.name) + u'". ')
                return redirect("/home/")
        org_level = True
    elif level == 'department':
        if not has_permission(request.user, "edit_permissions_and_roles"):
            messages.warning(request, u'Эта страница доступна только для ответственных по подразделению. '
                                      u'Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.'
                                      u'Если Вы ответственный по организации, Вам требуется специальное разрешение на получение доступа.')
            return redirect("/home/")
        if not request.user.representatives.filter(Q(department=instance.department) |
                                                   Q(department__in=instance.department.get_ancestors_flat()) |
                                                  (Q(department__isnull=True) &
                                                  (Q(organization=instance.organization) |
                                                   Q(organization=instance.department.organization)))):
            messages.warning(request, u'Эта страница доступна только для ответственных по подразделению "' + unicode(instance.department.name) + u'" '
                                      u'и ответственных по вышестоящим подразделениям. '
                                      u'Если Вы ответственный по организации, Вам требуется специальное разрешение на получение доступа.')
            return redirect("/home/")
        org_level = False
    else:
        raise Http404
    if request.method == "POST":
        redirect_url = request.GET.get("redirect_url")
        if request.POST.get("cancel_button"):
            return redirect(redirect_url)
        if request.user == instance.user:
            form = RepresentativeEditYourselfForm(request.user, request.POST or None, instance=instance)
        else:
            form = RepresentativeCreateEditForm(request.user, True, org_level, request.POST or None, instance=instance)
        if form.is_valid():
            form.save(request)
            if "/home/" in redirect_url:
                return redirect(redirect_url)
            else:
                return redirect("/unified_permissions/user/department/list/")
    else:
        redirect_url = request.META.get('HTTP_REFERER')
        if request.user == instance.user:
            form = RepresentativeEditYourselfForm(request.user, instance=instance)
        else:
            form = RepresentativeCreateEditForm(request.user, True, org_level, instance=instance)
    self_editing = request.user == instance.user
    return render(request, "organizations/representative_add_edit.html",
                  {'form': form, 'representative_id': representative_id, 'redirect_url': redirect_url,
                   'edit_mode': True, 'self_editing': self_editing, 'org_level': org_level, },
                   context_instance=RequestContext(request))
