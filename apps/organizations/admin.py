# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db import models
from common.widgets import FilteredSelect

from common.utils.admin import MyVersionAdmin, register_with_versions, MyModelAdmin
from models import Organization, Department, DepartmentRelation, Representative, OrganizationDomain, DepartmentParameterCategory, DepartmentParameterValue

register_with_versions(Organization)
class DepartmentAdmin(MyModelAdmin):
    list_display = ('name', 'id', 'level', 'organization')
    search_fields = ('name',)
register_with_versions(Department, DepartmentAdmin)


register_with_versions(DepartmentRelation)
register_with_versions(OrganizationDomain)
register_with_versions(DepartmentParameterCategory)
register_with_versions(DepartmentParameterValue)


class RepresentativeAdmin(MyVersionAdmin):
    '''Admin class representing Representative class.'''
    list_display = ('fullname_short', 'department', 'is_active', 'post', 'email', 'phone_number', 'address', 'comment', 'startdate', 'enddate')
    list_editable = ('post', 'phone_number', 'email', 'address', 'comment', 'startdate', 'enddate')
    list_display_links = ('fullname_short',)
    search_fields = ['user__profile__lastname', 'user__username', 'department__name', 'organization__name', 'post', 'email', 'phone_number', 'address', 'comment']
    formfield_overrides = {models.ForeignKey: {'widget': FilteredSelect}}

register_with_versions(Representative, RepresentativeAdmin)


