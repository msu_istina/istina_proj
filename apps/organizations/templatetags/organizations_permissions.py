from django.template import Library

register = Library()


@register.filter
def permissions(user, obj):
    """
    Returns the list of permissions a user has on an object
    """
    if user:
        permissions = user.get_perms(obj, False)
        if hasattr(obj, 'ancestors'): # include department ancestors permissions
            for ancestor in obj.ancestors:
                permissions.extend(user.get_perms(ancestor, False))
        if hasattr(obj, 'organization'): # include parent organization permissions
            permissions.extend(user.get_perms(obj.organization, False))
        return set(permissions)
    return []
