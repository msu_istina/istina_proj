# -*- coding: utf-8 -*-
from django import template
from organizations.models import Representative
from common.utils.user import get_worker

register = template.Library()

@register.inclusion_tag('organizations/representatives.html')
def show_representatives(user, full=None, organization_level=False):
    '''Displays a table of departments' representatives.
        full
        if True, show the full list of representatives
        otherwise show only relevant ones.
    '''
    if full:
        representatives = Representative.objects.get_for_all(organization_level)
    else: # show only relevant representatives
        representatives = Representative.objects.get_for_user(user, organization_level)
    return {'representatives': representatives, 'full': full, 'organization_level': organization_level}
