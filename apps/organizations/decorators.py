# -*- coding: utf-8 -*-
import logging
from functools import wraps
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import available_attrs
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from organizations.utils import is_representative_for

logger = logging.getLogger("organizations.decorators")

def representative_required(function):
    """
    Decorator for views that checks that current user is a representative
    for the department he asks information for.
    """
    def decorator(view_func):

        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            from organizations.models import Department
            department_id = kwargs.get("department_id", None) or kwargs.get("object_id", None)
            if department_id:
                department = get_object_or_404(Department, pk=department_id)
                if is_representative_for(request.user, department):
                    return view_func(request, *args, **kwargs)
                else:
                    if not request.user.is_authenticated():
                        return login_required(view_func)(request, *args, **kwargs)
                    else:
                        messages.warning(request,
                            u'Эта страница доступна только для ответственных по подразделению. Чтобы стать ответственным, следуйте инструкциям на странице <a href="/help/feedback/">обратной связи</a>.')
                        return redirect(department)
            else:
                return redirect('/')
        return _wrapped_view

    return decorator(function)