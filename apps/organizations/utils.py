# -*- coding: utf-8 -*-

from itertools import chain
from django.db.models import get_model
from django.db.models import Q
from common.utils.user import get_worker


def is_representative_for_user(user_representative, user):
    '''Returns True if user_representative is a matching representative for the user.
    Both user_representative and user are User instances.
    FIXME: does not include organization_level representatives
    FIXME: select only active representatives
    '''
    worker = get_worker(user)
    return is_representative_for_worker(user_representative, worker)

def is_representative_for_worker(user_representative, worker):
    '''Returns True if user_representative is a matching representative for the worker.
    FIXME: select only active representatives
    '''
    if not user_representative.is_authenticated() or not worker:
        return False

    # get all representatives linked to user_representative

    linked_representatives = set(user_representative.representatives.filter(is_active=True))
    if linked_representatives:
        # get all representatives appropriate for user
        Representative = get_model("organizations", "Representative")
        appropriate_representatives = set(Representative.objects.get_for_worker(worker))
        # return True if these two sets of representatives intersect
        if bool(linked_representatives & appropriate_representatives):
            return True
    deps_qs = user_representative.representatives.filter(is_active=True).values('department')
    orgs_qs = user_representative.representatives.filter(department__isnull=True, is_active=True).values('organization')
    qs = worker.current_employments.filter(Q(department__in=deps_qs) | Q(department__organization__in=orgs_qs))
    return qs.exists()

def is_representative_for_any_user(user_representative, users):
    '''Returns True if user_representative is a matching representative for *any* of the users.
    All user_representative and users are User instances.
    FIXME: select only active representatives
    '''

    # get all representatives linked to user_representative
    linked_representatives = set(user_representative.representatives.filter(is_active=True))
    if linked_representatives:
        # get all representatives appropriate for users
        Representative = get_model("organizations", "Representative")
        appropriate_representatives = set(chain.from_iterable(Representative.objects.get_for_user(user) for user in users))
        # return True if these two sets of representatives intersect
        if bool(linked_representatives & appropriate_representatives):
            return True
    
    for user in users:
        ok = is_representative_for_user(user_representative, user)
        if ok:
            return True
    return False

def is_representative_for(user, dep, excluded_representative_id=None):
    '''Returns True if USER is a an active representative for department DEP
    '''
    Department = get_model("organizations", "Department")
    Organization = get_model("organizations", "Organization")
    if not isinstance(dep, Department):
        if isinstance(dep, Organization):
            return is_representative_for_whole_organization(user, dep, excluded_representative_id)
        return False
    if excluded_representative_id is not None:
        representatives_qs = user.representatives.exclude(id=excluded_representative_id)
    else:
        representatives_qs = user.representatives
    if representatives_qs.filter(organization=dep.organization, department__isnull=True, is_active=True).exists():
        return True
    if representatives_qs.exists():
        depsids = representatives_qs.filter(is_active=True).values_list('department', flat=True)
        deps = Department.objects.filter(id__in=depsids)
        subtree = chain.from_iterable(dep.descendants(show_level=False) for dep in deps)
        if dep in subtree:
            return True
    return False


def is_representative_for_whole_organization(user, org, excluded_representative_id=None):
    '''Returns True if USER is an active representative for the whole organization ORG
    '''
    repr_qs = user.representatives.filter(organization=org, department__isnull=True, is_active=True)
    if excluded_representative_id is not None:
        return repr_qs.exclude(id=excluded_representative_id).exists()
    return repr_qs.exists()


def get_representatives_departments(user, recursive=True):
    '''Returns all departments managed by user as a representative.

    If recursive flag is False, returns only these daprtments that was
    explicitly granted to user. Otherwise returns all descendant
    departments as well.

    Usage example:
    subtree = get_representatives_departments(user)
    emps = Employment.objects.filter(department__in=subtree)
    '''
    from organizations.models import Department

    if user.representatives.exists():
        depsids = user.representatives.filter(is_active=True).values_list('department', flat=True)
        deps = Department.objects.filter(id__in=depsids)
        if recursive:
            subtree = chain.from_iterable(dep.descendants(show_level=False) for dep in deps)
            return subtree
        return deps
    return None
