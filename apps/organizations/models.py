# -*- coding: utf-8 -*-
from django.db import models
from django.core.cache import cache
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import logging
import operator
import datetime

from common.models import MyModel
from common.utils.activities import get_workers_activities_summary
from common.utils.strings import uncapitalize_1st_letter
from common.utils.uniqify import uniqify
from common.utils.user import get_profile
from common.utils.choices import DATATYPES
from workers.models import Worker, Profile, Employment
from organizations.managers import RepresentativeManager, OrganizationManager, DepartmentManager, DepartmentParameterCategoryManager
from organizations.utils import is_representative_for
from orgproperty.models import OrganizationProperty, OrganizationPropertyCategory

logger = logging.getLogger("organizations.models")


class Organization(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_ORGANIZATION_ID")
    name = models.CharField(u"Название организации", max_length=255, unique=True, db_column="F_ORGANIZATION_NAME")
    creator = models.ForeignKey(to="auth.User", related_name="organizations_added", db_column="F_ORGANIZATION_USER", null=True, blank=True)

    search_attr = 'name'

    objects = OrganizationManager()

    class Meta:
        db_table = "ORGANIZATION"
        verbose_name = u"Организация"
        verbose_name_plural = u"Организации"
        ordering = ("name",)

    def __unicode__(self):
        return "%s" % self.name

    @models.permalink
    def get_absolute_url(self):
        return ('organization_detail', (), {'object_id': self.id})

    @property
    def root_departments(self):
        # get ids of all departments in the organization that currently don't have parent departments
        #have_parents = DepartmentRelation.objects.filter(parent__organization=self, child__organization=self).exclude(enddate__lt=datetime.date.today()).values_list('child', flat=True)
        #
        # FIXME: Do not respect relationship dates. Otherwise there is
        # no way to mark department as 'old': old departments become
        # root departments.
        have_parents = DepartmentRelation.objects.filter(parent__organization=self, child__organization=self).values_list('child', flat=True)
        return self.departments.exclude(id__in=have_parents)

    @property
    def workers_count(self):
        '''Returns number of workers in all departments of the organization.
          Note that the number includes past workers.'''
        return len(set(Employment.objects.filter(department__organization=self).values_list("worker", flat=True)))

    def get_workers(self, state='current'):
        '''Returns current workers in all departments of the organization.
        @state can be 'current', 'past', 'all' or 'all_divided', meaning to return only current workers, only past ones or all. all_divided means that current and past workers are returned separately.'''
        if state not in ['current', 'past', 'all', 'all_divided']:
            raise
        workers = {'current': [], 'past': []}
        worker_ids = {'current': [], 'past': []}
        if state in ['current', 'all_divided']:
            worker_ids['current'] = Employment.objects.filter(department__organization=self).exclude(enddate__lt=datetime.date.today()).values_list("worker", flat=True).distinct()
            workers['current'] = Worker.objects_ordered_by_articles.filter(id__in=worker_ids['current'])
        if state in ['past', 'all_divided']:
            worker_ids['past'] = Employment.objects.filter(enddate__lt=datetime.date.today(), department__organization=self).values_list("worker", flat=True).distinct()
            workers['past'] = Worker.objects_ordered_by_articles.filter(id__in=set(worker_ids['past']) - set(worker_ids['current']))
        if state == 'all':
            worker_ids = Employment.objects.filter(department__organization=self).values_list("worker", flat=True).distinct()
            workers = Worker.objects_ordered_by_articles.filter(id__in=worker_ids)
        if state in ['current', 'past']:
            workers = workers[state]
        return workers

    current_workers = property(get_workers)

    @property
    def past_workers(self):
        '''Returns past workers in all departments of the organization.'''
        return self.get_workers(state='past')

    @property
    def all_workers(self):
        '''Returns workers in all departments of the organization, current and past.'''
        return self.get_workers(state='all')

    @property
    def all_workers_divided(self):
        '''Returns workers in all departments of the organization, current and past separately.'''
        return self.get_workers(state='all_divided')

    @property
    def users_count(self):
        '''Returns number of registered users that work or worked in all departments of the organization.'''
        return len(set(Employment.objects.filter(department__organization=self, worker__profile__isnull=False).values_list("worker", flat=True)))

    @property
    def profiles(self):
        '''Returns profiles of all current users in all departments of the organization.'''
        profile_ids = Employment.objects.filter(department__organization=self).exclude(enddate__lt=datetime.date.today()).values_list("worker__profile", flat=True).distinct()
        profiles = Profile.objects.filter(id__in=profile_ids)
        return profiles

    @property
    def users(self):
        '''Returns current active users in all departments of the organization.'''
        users_ids = Employment.objects.filter(
            department__organization=self).exclude(
            enddate__lt=datetime.date.today()).exclude(
            worker__profile__user__is_active=False).values_list(
            "worker__profile__user", flat=True)
        users = User.objects.filter(id__in=users_ids)
        return users

    @property
    def activities_summary(self):
        '''Returns the summary of activities of all workers of the organization.'''
        return get_workers_activities_summary(self.all_workers)

    def get_headertext(self):
        '''Returns text string which we need to paste in header in the base.html.'''
        try:
            return self.properties.get(category_id=10000701).value
        except DoesNotExist:
            return ""

    def get_header_imageurl(self):
        '''Returns image url which we need to paste in header in the base.html.'''
        try:
            return self.properties.get(category_id=10000700).value
        except DoesNotExist:
            return ""


class Department(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEPARTMENT_ID")
    name = models.CharField(u"Название подразделения", max_length=255, db_column="F_DEPARTMENT_NAME")
    organization = models.ForeignKey(to="Organization", related_name="departments", verbose_name=u"организация", db_column="F_ORGANIZATION_ID")
    creator = models.ForeignKey(to="auth.User", related_name="departments_added", db_column="F_DEPARTMENT_USER", null=True, blank=True)
    search_attr = 'name'

    objects = DepartmentManager()

    class Meta:
        db_table = "DEPARTMENT"
        verbose_name = u"Подразделение"
        verbose_name_plural = u"Подразделения"
        unique_together = ("name", "organization")
        ordering = ("organization__name", "name")

    def __unicode__(self):
        return "%s" % self.name

    @models.permalink
    def get_absolute_url(self):
        return ('department_detail', (), {'object_id': self.id})

    def get_workers(self, subdepartments=True, state='current', return_ids=False):
        '''Returns list of workers that are working for the department at the current time.

        subdepartments
            If it is true, also return all workers of descendants.
        state
            can be 'current', 'past', 'all' or 'all_divided', meaning to return only current workers, only past ones or all. all_divided means that current and past workers are returned separately.
        return_ids
           If it is true, return not the actual Worker objects, but a set of their ids.
        '''
        if not state in ['current', 'past', 'all', 'all_divided']:
            raise Exception("state is unknown")

        if not subdepartments:
            department_ids = [self.id]
        else:
            department_ids = self.current_children_ids_recursive

        worker_ids = {'current': [], 'past': [], 'all': []}
        base_queryset = Employment.objects.filter(department__in=department_ids)
        if state == 'current':
            employments = base_queryset.exclude(enddate__lt=datetime.date.today())
            worker_ids['current'] = list(employments.values_list('worker', flat=True).distinct())
        elif state == 'past':
            employments = base_queryset.filter(enddate__lt=datetime.date.today())
            worker_ids['past'] = list(employments.values_list('worker', flat=True).distinct())
        else:
            # all or all_divided
            all_workers = list(Employment.objects.filter(department__in=department_ids).values_list('worker', "enddate").distinct())
            if state == 'all':
                worker_ids['all'] = map(operator.itemgetter(0), all_workers)
            else:
                worker_ids['past'] = [x[0] for x in all_workers if x[1] is not None and x[1] < datetime.date.today()]
                worker_ids['current'] = [x[0] for x in all_workers if x[1] is None or x[1] >= datetime.date.today()]

        if state in ['current', 'all']:
            worker_ids = set(worker_ids[state])
        elif state == 'past':
            worker_ids = set(worker_ids['past']) - set(worker_ids['current']) # current worker should not be counted as past workers
        elif state == 'all_divided':
            worker_ids['current'] = set(worker_ids['current'])
            worker_ids['past'] = set(worker_ids['past']) - set(worker_ids['current'])

        if return_ids:
            return worker_ids
        else:
            fetch_workers = lambda ids: Worker.objects_ordered_by_articles.filter(id__in=ids)
            if state == 'all_divided':
                workers = {'current': [], 'past': []}
                workers['current'] = fetch_workers(worker_ids['current'])
                workers['past'] = fetch_workers(worker_ids['past'])
            else:
                workers = fetch_workers(worker_ids)
            return workers

    current_workers = property(get_workers)

    @property
    def past_workers(self):
        '''Returns list of workers that were working for the department but not working at the current time.'''
        return self.get_workers(state='past')

    @property
    def all_workers(self):
        '''Returns list of workers that are or were working for the department or subdepartments.'''
        return self.get_workers(state='all')

    @property
    def all_workers_divided(self):
        '''Returns list of workers that are or were working for the department or subdepartments, divided into two groups: current and past workers.'''
        return self.get_workers(state='all_divided')

    @property
    def current_parents(self):
        '''Returns list of departments that are parents of the department at the current time.'''
        parents_ids = self.parent_relations.exclude(enddate__lt=datetime.date.today()).values_list('parent', flat=True).distinct()
        parents = Department.objects.filter(id__in=parents_ids)
        return parents

    @property
    def roots(self):
        '''Return current roots of the department.'''
        all_roots = self.organization.root_departments
        return list(set(all_roots) & set(self.ancestors+[self]))

    @property
    def is_root(self):
        '''Return True if the department is a root one.'''
        return self in self.organization.root_departments

    @property
    def current_children(self):
        '''Returns list of departments that are direct children of the department at the current time.'''
        children_ids = self.children_relations.exclude(enddate__lt=datetime.date.today()).values_list('child', flat=True).distinct()
        children = Department.objects.filter(id__in=children_ids)
        return children

    @property
    def current_children_ids_recursive(self):
        '''Returns list of departments' ids that are direct or indirect children of the department at the current time.'''
        all_ids = set()
        current_level_ids = set([self.id])
        while current_level_ids:
            all_ids.update(current_level_ids)
            current_level_ids = DepartmentRelation.objects.filter(parent__in=current_level_ids).exclude(enddate__lt=datetime.date.today()).values_list('child', flat=True).distinct()
            current_level_ids = set(current_level_ids).difference(all_ids)
        return list(all_ids)

    def descendants(self, level=0, show_level=True):
        '''Returns a list of all descendants in the format [(descendant, level)]. A flattened list of descendants follows each node.
           Level indicates the level of the current node.
           if show_level = False, then return a flat list without levels.
        '''
        if show_level:
            children_list = [(self, level)]
        else:
            children_list = [self]
        for child in self.current_children:
            children_list.extend(child.descendants(level=level + 1, show_level=show_level))
        return children_list

    def descendants_ids(self, include_self=False):
        '''Returns a list containing all descendants' ids.
        If include_self=True, than the department's id also included, else - excluded.'''
        children_ids = list(self.children_relations.exclude(
                            enddate__lt=datetime.date.today()).values_list('child', flat=True).distinct())
        for child in Department.objects.filter(id__in=children_ids):
                children_ids.extend(child.descendants_ids(include_self=False))
        if include_self:
            children_ids.append(self.id)
        return children_ids

    def get_descendants_queryset(self, include_self=False):
        '''Returns a queryset containing all descendants.
        If include_self=True, than the department also included, else - excluded.'''
        descendants_ids = self.descendants_ids(include_self)
        descendants = Department.objects.filter(id__in=descendants_ids)
        return descendants

    descendants_queryset = property(get_descendants_queryset)

    @property
    def descendants_count(self):
        '''Returns the number of descendants of the department.'''
        count = self.children_relations.exclude(enddate__lt=datetime.date.today()).count()
        for child in self.current_children:
            count += child.descendants_count
        return count

    def get_level(self, parent=None):
        '''Returns a level of the department in parents' descendants.
            If self is not a descendant of the parent, raise KeyError. If self=parent, return 0. If self is a direct child of the parent, return 1, etc.
            If self is a multiple descendant (i.e. several times with different intermediate nodes), return the last level, just as dict() works.
            If @parent is not specified, calculate level of the department as a distance to nearest root department. If self is root, return 0. If a root department is a direct parent, return 1.
        '''
        if parent:
            return dict(parent.descendants())[self]
        else:
            ancestors = self.get_ancestors(show_level=True)
            level = min([level for (ancestor, level) in ancestors+[(self, 0)] if ancestor in self.roots])
            return level

    level = property(get_level)

    def get_workers_count(self, subdepartments=True):
        '''Returns number of (current and past) workers in the department and, optionally, all its subdepartments.'''
        return len(self.get_workers(state='all', subdepartments=subdepartments, return_ids=True))

    workers_count = property(get_workers_count)

    def get_users_count(self, subdepartments=True):
        '''Returns number of users in the department and, optionally, all its subdepartments.'''
        return len(self.get_profiles(subdepartments=subdepartments, return_ids=True))

    def get_own_users_count(self):
        '''Returns number of users in the department only (excluding all its subdepartments).'''
        return self.get_users_count(subdepartments=False)

    users_count = property(get_users_count)
    own_users_count = property(get_own_users_count)

    def get_profiles(self, subdepartments=False, return_ids=False):
        '''Returns list of profiles that are working for the department at the current time. If subdepartments is true, also return all profiles of descendants.
        If return_ids is true, return not the actual Profile objects, but their ids.'''
        def get_profile_ids(department, subdepartments):
            profile_ids = department.employments.filter(worker__profile__isnull=False).exclude(enddate__lt=datetime.date.today()).values_list('worker__profile', flat=True).distinct()
            if subdepartments:
                profile_ids = list(profile_ids)
                for child in department.current_children:
                    profile_ids.extend(get_profile_ids(child, subdepartments))
            return profile_ids
        profile_ids = get_profile_ids(self, subdepartments)
        if return_ids:
            return set(profile_ids)
        else:
            profiles = Profile.objects.filter(id__in=profile_ids)
            return profiles

    profiles = property(get_profiles)

    @property
    def full_profiles(self):
        '''Returns current profiles of workers of the department and its descendants.'''
        return self.get_profiles(subdepartments=True)

    def get_ancestors(self, level=1, show_level=False):
        '''Returns a list of all ancestors in the format [(ancestor, level)]. A flattened list of ancestors follows each node.
           Level indicates the level of the current node.
           if show_level = False, then return a flat list without levels.
        '''
        if not show_level:
            return self.get_ancestors_flat()
        parents_list = [(parent, level) for parent in self.current_parents]
        for parent in self.current_parents:
            parents_list.extend(parent.get_ancestors(level=level + 1, show_level=True))
        return parents_list

    def get_ancestors_flat(self):
        '''Returns a list of all ancestors. The department itself is not included.'''
        key = "ancestors_flat.%s" % self.id
        ancestors = cache.get(key)
        if ancestors is None:
            ancestors = []
            for parent in self.current_parents:
                ancestors += parent.get_ancestors_flat()
            ancestors += self.current_parents
            cache.set(key, ancestors, 7200) # two hours
        return ancestors

    ancestors = property(get_ancestors_flat)

    @property
    def publications(self):
        '''Returns a dictionary of all publications (articles, books) of the workers of the department.'''
        articles = []
        books = []
        for worker in self.all_workers:
            articles.extend(list(worker.articles.all()))
            books.extend(list(worker.books.all()))
        return {'articles': uniqify(articles), 'books': uniqify(books)}

    @property
    def activities_summary(self):
        '''Returns the summary of activities of workers of the department and its subdepartments.'''
        return get_workers_activities_summary(self.all_workers)

    def merge(self, department_from):
        '''Move all linked objects from department_from to self.
        NB: after merge, department_from should be deleted manually from admin panel
        (to avoid accidental deletions of linked objects).
        '''
        department_from.schools.all().update(maindep=self)
        department_from.schoolmembers.all().update(maindep=self)
        department_from.reports.all().update(department=self)
        department_from.courses_teachings.all().update(department=self)
        department_from.representatives.all().update(department=self)
        department_from.employments.all().update(department=self)
        department_from.man_ranks.all().update(department=self)


class DepartmentRelation(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEPARTMENTLINK_ID")
    parent = models.ForeignKey(to="Department", related_name="children_relations", verbose_name=u"родительское подразделение", db_column="DEP_F_DEPARTMENT_ID")
    child = models.ForeignKey(to="Department", related_name="parent_relations", verbose_name=u"дочернее подразделение", db_column="F_DEPARTMENT_ID")
    startdate = models.DateField(u"Начало действия связи", db_column="F_DEPARTMENTLINK_BEGIN")
    enddate = models.DateField(u"Окончание действия связи", db_column="F_DEPARTMENTLINK_END", null=True, blank=True)
    creator = models.ForeignKey(to="auth.User", related_name="department_relations_added", db_column="F_DEPARTMENTLINK_USER", null=True, blank=True)

    class Meta:
        db_table = "DEPARTMENTLINK"
        verbose_name = u"Связь между подразделениями"
        verbose_name_plural = u"Связи между подразделениями"
        unique_together = ("parent", "child", "startdate")
        ordering = ("-startdate", "-enddate")

    def __unicode__(self):
        return "%s is a parent of %s" % (unicode(self.parent), unicode(self.child))

class NestedParentDepartment(models.Model):
    # department is not a primary key, required by Django
    department = models.ForeignKey(to="Department", related_name="nested_parent_relations", verbose_name=u"подразделение", db_column="F_DEPARTMENT_ID", primary_key=True, on_delete=models.DO_NOTHING)
    parent = models.ForeignKey(to="Department", related_name="nested_children_relations", verbose_name=u"родительское подразделение", db_column="PARENT_F_DEPARTMENT_ID", on_delete=models.DO_NOTHING)

    def save(self, **kwargs):
        raise NotImplementedError() # Disable insertion/deletion

    class Meta:
        db_table = "ALL_PARENT_DEPARTMENT"
        managed = False



class Representative(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_REPRESENTATIVE_ID")
    organization = models.ForeignKey(to="Organization", related_name="representatives", verbose_name=u"организация", db_column="F_ORGANIZATION_ID", blank=True, null=True)
    department = models.ForeignKey(to="Department", related_name="representatives", verbose_name=u"подразделение", db_column="F_DEPARTMENT_ID", blank=True, null=True)
    user = models.ForeignKey(to="auth.User", related_name="representatives", verbose_name=u"пользователь", db_column="F_REPRESENTATIVE_USER", blank=True, null=True)
    scope = models.CharField(u"Сфера компетенции", max_length=1000, help_text=u'укажите, какие вопросы находятся в компетенции пользователя в системе, например, "Общие вопросы"', db_column="F_REPRESENTATIVE_SCOPE", blank=True)
    post = models.CharField(u"Должность", max_length=255, db_column="F_REPRESENTATIVE_POST", blank=True)
    email = models.CharField(u"Адрес электронной почты", max_length=255, db_column="F_REPRESENTATIVE_EMAIL", blank=True)
    phone_number = models.CharField(u"Номер телефона", max_length=255, db_column="F_REPRESENTATIVE_PHONE", blank=True)
    address = models.CharField(u"Рабочий адрес", max_length=255, db_column="F_REPRESENTATIVE_ADDRESS", blank=True)
    comment = models.CharField(u"Примечание", max_length=1000, db_column="F_REPRESENTATIVE_COMMENT", blank=True)
    is_active = models.NullBooleanField(u"Активен", db_column="F_REPRESENTATIVE_ISACTIVE", blank=True, default=True)
    startdate = models.DateField(u"Дата назначения ответственным", db_column="F_REPRESENTATIVE_BEGIN", blank=True)
    enddate = models.DateField(u"Дата окончания срока действия полномочий", db_column="F_REPRESENTATIVE_END", blank=True)

    objects = RepresentativeManager()

    nominative_en = "representative"
    genitive = u"ответственного сотрудника"
    genitive_plural_full = u"ответственным сотрудникам"
    dative = u"ответственному сотруднику"
    accusative = u"ответственного сотрудника"
    accusative_short = u"ответственного сотрудника"
    instrumental = u"ответственным сотрудником"
    locative = u"ответственном сотруднике"

    class Meta:
        db_table = "REPRESENTATIVE"
        verbose_name = u"Ответственный сотрудник"
        verbose_name_plural = u"Ответственные сотрудники"
        ordering = ('department__name',)

    def __unicode__(self):
        return u"%s, %s" % (self.fullname_short, unicode(self.unit))

    @property
    def unit(self):
        return self.department or self.organization

    def get_organization(self):
        return self.organization or self.department.organization

    @property
    def root_department(self):
        '''Returns root department of self.department. Returns self.department if this is a root department.'''
        return self.department and ", ".join(unicode(root) for root in self.department.roots)

    @property
    def child_department(self):
        '''Returns self.department if it is not root. Otherwise returns None.'''
        return self.department and not self.department.is_root and self.department # if self.department is not None and not is_root, then return self.department, otherwise None or False

    @property
    def department_str(self):
        value = unicode(self.root_department)
        child_dep = self.child_department
        if child_dep:
            value += ", %s" % uncapitalize_1st_letter(unicode(child_dep))
        return value

    @property
    def unit_str(self):
        return self.department_str if self.department else unicode(self.organization)

    @property
    def fullname(self):
        profile = get_profile(self.user)
        return profile.fullname if profile else u"<не зарегистрирован в системе>"

    @property
    def fullname_short(self):
        profile = get_profile(self.user)
        return profile.fullname_short if profile else u"<не зарегистрирован в системе>"

    @models.permalink
    def get_absolute_url(self):
        return ('user_profile_public', (), {'username': self.user.username})
        #FIXME: representative's details page

class OrganizationDomain(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_ORGDOMEN_ID")
    organization = models.ForeignKey(to="organizations.Organization", db_column="F_ORGANIZATION_ID",
        related_name="domain")
    domain = models.CharField(u"Domain value", max_length=255, unique=False, db_column="F_ORGDOMEN_CODE")

    class Meta:
        db_table = "ORGDOMEN"
        verbose_name = u"OgranizationDomain"
        verbose_name_plural = u"OgranizationDomain"
        ordering = ("id",)

    def __unicode__(self):
        return "%s" % self.domain

class DepartmentParameterCategory(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEPDATATYPE_ID")
    name = models.CharField(u"название", max_length=255, db_column="F_DEPDATATYPE_NAME")
    code = models.CharField(u"кодовое название", max_length=255, db_column="F_DEPDATATYPE_CODE", blank=True)
    organization = models.ForeignKey("Organization", related_name="department_parameter_categories", verbose_name=u"организация", db_column="F_ORGANIZATION_ID", blank=True, null=True)
    ordinal = models.PositiveSmallIntegerField(u"порядковый номер", db_column="F_DEPDATATYPE_ORD", blank=True, null=True)
    value_type = models.CharField(u"тип значений", max_length=30, db_column="F_DEPDATATYPE_TYPE", choices=DATATYPES)

    objects = DepartmentParameterCategoryManager()

    class Meta:
        db_table = "DEPDATATYPE"
        verbose_name = u"тип параметра подразделения"
        verbose_name_plural = u"типы параметров подразделений"
        ordering = ('ordinal', 'name')
        unique_together = ('code', 'organization')

    def __unicode__(self):
        return self.name

    @property
    def to_field_name(self):
        return "field_" + str(self.id)

    @property
    def is_integer(self):
        return self.value_type == u"Числовое целое"


class DepartmentParameterValue(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_DEPDATA_ID")
    department = models.ForeignKey(Department, related_name="parameters", verbose_name=u"подразделение", db_column="F_DEPARTMENT_ID")
    category = models.ForeignKey(DepartmentParameterCategory, related_name="parameters", verbose_name=u"тип параметра", db_column="F_DEPDATATYPE_ID")
    year = models.IntegerField(u"год", null=True, blank=True, db_column="F_DEPDATA_YEAR")
    value_integer = models.IntegerField(u"значение (число)", null=True, blank=True, db_column="F_DEPDATA_VALUEINT")
    value_string = models.CharField(u"значение (строка)", max_length=255, blank=True, db_column="F_DEPDATA_VALUE")

    class Meta:
        db_table = "DEPDATA"
        verbose_name = u"значение параметра подразделения"
        verbose_name_plural = u"значения параметров подразделений"
        ordering = ('category', 'department', '-year')

    def __unicode__(self):
        return u"%s%s: %s, %s" % (self.category, (", %d" % self.year) if self.year else "", self.value_to_string, self.department)

    @property
    def value_to_string(self):
        return unicode(self.value_integer or self.value_string)

    @property
    def value(self):
        return self.value_integer or self.value_string
    @value.setter
    def value(self, value):
        if self.category.is_integer:
            self.value_integer = value
        else:
            self.value_string = value


from organizations.admin import *
