from django.conf.urls.defaults import *
from organizations.models import Department
from django.views.generic.list_detail import object_detail

excel_dict = {'export_excel': True}

urlpatterns = patterns('organizations.views.reports',

    url(r'^(?P<year>\d{4})/$', lambda request, department_id, year: object_detail(request, object_id=department_id,
        queryset=Department.objects.all(),
        template_name='organizations/reports/monitoring.html', template_object_name='department',
        extra_context={'year': int(year)}),
        name="organizations_department_reports_monitoring"),

    url(r'^degrees/$', 'monitoring_degrees', name="organizations_department_reports_monitoring_degrees"),
    url(r'^degrees/excel/$', 'monitoring_degrees', excel_dict, name="organizations_department_reports_monitoring_degrees_excel"),

    url(r'^books/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$',
        'monitoring_books', name='organizations_department_reports_monitoring_books'),
    url(r'^books/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$',
        'monitoring_books',
        excel_dict, name='organizations_department_reports_monitoring_books_excel'),

    url(r'^publications/indexed/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$',
        'monitoring_publications_indexed',
        name='organizations_department_reports_monitoring_publications_indexed'),
    url(r'^publications/indexed/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$',
        'monitoring_publications_indexed',
        excel_dict, name='organizations_department_reports_monitoring_publications_indexed_excel'),

    url(r'^publications/vak/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$',
        'monitoring_publications_vak',
        name='organizations_department_reports_monitoring_publications_vak'),
    url(r'^publications/vak/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$',
        'monitoring_publications_vak',
        excel_dict, name='organizations_department_reports_monitoring_publications_vak_excel'),

    url(r'^patents/(?P<startyear>\d{4})-(?P<endyear>\d{4})/$',
        'monitoring_patents', name='organizations_department_reports_monitoring_patents'),
    url(r'^patents/(?P<startyear>\d{4})-(?P<endyear>\d{4})/excel/$',
        'monitoring_patents',
        excel_dict, name='organizations_department_reports_monitoring_patents_excel')
)
