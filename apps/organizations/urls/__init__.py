from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_detail, object_list

from common.views import autocomplete_search
from organizations.models import *
from organizations.views import DepartmentParametersFormView, DepartmentParametersView, representative_add, representative_edit

urlpatterns = patterns('organizations.views',
    url(r'^$', object_list, {'queryset': Organization.objects.all(), 'template_object_name': 'organization'}, name='organization_list'),
    url(r'^(?P<object_id>\d+)/$', object_detail, {'queryset': Organization.objects.all(), 'template_object_name': 'organization'}, name='organization_detail'),
    url(r'^(?P<object_id>\d+)/stats/$', object_detail, {'queryset': Organization.objects.all(), 'template_object_name': 'organization', 'extra_context': {'stats': True}}, name='organization_detail_stats'),
    url(r'^(?P<object_id>\d+)/workers/$', object_detail, {'queryset': Organization.objects.all(), 'template_name': 'workers/worker_list.html', 'template_object_name': 'organization'}, name='organization_worker_list'),
    url(r'^(?P<object_id>\d+)/workers/xml/$', object_detail, {'queryset': Organization.objects.all(), 'template_name': 'workers/worker_list_organization.xml', 'template_object_name': 'organization'}, name='organization_worker_list_xml'),
    url(r'^(?P<object_id>\d+)/users/$', object_detail, {'queryset': Organization.objects.all(), 'template_name': 'userprofile/profile/list.html', 'template_object_name': 'organization'}, name='organization_profile_list'),
    url(r'^search/$', autocomplete_search, {'model': Organization}, name="organizations_search"),

    url(r'^department/(?P<object_id>\d+)/$', object_detail, {'queryset': Department.objects.all(), 'template_object_name': 'department'}, name='department_detail'),
    url(r'^department/(?P<object_id>\d+)/workers/$', object_detail, {'queryset': Department.objects.all(), 'template_name': 'workers/worker_list.html', 'template_object_name': 'department'}, name="department_worker_list"),
    url(r'^department/(?P<object_id>\d+)/workers/xml/$', object_detail, {'queryset': Department.objects.all(), 'template_name': 'workers/worker_list_department.xml', 'template_object_name': 'department'}, name="department_worker_list_xml"),
    url(r'^department/(?P<object_id>\d+)/users/$', object_detail, {'queryset': Department.objects.all(), 'template_name': 'userprofile/profile/list.html', 'template_object_name': 'department'}, name="department_profile_list"),
    url(r'^department/(?P<department_id>\d+)/tree/$', "department_get_tree", name="department_get_tree"),
    url(r'^department/(?P<department_id>\d+)/publications/xml/$', "department_publications_xml", name="department_publications_xml"),
    url(r'^department/(?P<department_id>\d+)/activities/2weeks/((?P<mode>.+)/)?$', "department_activities_2weeks", name="department_activities_2weeks"),

    url(r'^department/(?P<department_id>\d+)/reports/', include('organizations.urls.reports')),

    url(r'^department/(?P<department_id>\d+)/parameters/$', DepartmentParametersView.as_view(), name="department_parameters"),
    url(r'^department/(?P<department_id>\d+)/parameters/(?P<year>\d{4})/edit/$', DepartmentParametersFormView.as_view(), name="department_parameters_edit"),

    url(r'^representative/(?P<level>\w+)/add/$', representative_add, name='representative_add'),
    url(r'^representative/(?P<level>\w+)/(?P<representative_id>\d+)/edit/$', representative_edit, name='representative_edit')
    #FIXME: representative's details page

) + patterns('',
    url(r'^department/(?P<dep_id>\d+)/stats/(?:(?P<year>\d+)/)?$', 'statistics.views.department_stats', name='department_detail_stats')

)
