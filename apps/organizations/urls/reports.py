from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_detail
from organizations.models import Department

excel_dict = {'export_excel': True}

urlpatterns = patterns('organizations.views.reports',
	url(r'^$', lambda request, department_id: object_detail(request, object_id=department_id,
		queryset=Department.objects.all(),
		template_name='organizations/reports/index.html', template_object_name='department'),
		name="organizations_department_reports_index"),

    url(r'^monitoring/', include('organizations.urls.monitoring')),
    url(r'^degrees/$', 'degrees', name="organizations_department_reports_degrees"),
	url(r'^degrees/excel/$', 'degrees', excel_dict, name="organizations_department_reports_degrees_excel"),
	url(r'^workers/degrees/$', 'degrees', {"all_workers": True}, name="organizations_department_reports_workers_degrees"),
	url(r'^workers/degrees/excel/$', 'degrees', dict(excel_dict.items() + {"all_workers": True}.items()), name="organizations_department_reports_workers_degrees_excel"),

)
