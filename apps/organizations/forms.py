# -*- coding: utf-8; -*-

from django import forms
from django.forms.models import ModelChoiceIterator, ModelForm
from django.forms.fields import TypedChoiceField
from itertools import chain
from common.forms import Select2ModelField, Select2ModelFormMixin, LightSelect2ChoiceWidget, MyForm, Select2ModelMultipleField
from common.utils.strings import string_to_bool_or_null

from workers.forms import ProfileChoiceField

from models import *



import datetime

from organizations.utils import is_representative_for, is_representative_for_whole_organization
from unified_permissions import has_permission

from common.dynatree_widgets import DynatreeWidget


logger = logging.getLogger("organizations.forms")


class OrganizationDepartmentField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        '''Replacing default __unicode__ method for generating labels for select options.'''
        label = obj.name
        try:
            if self.indent_children and self.parent_department:  # indent children in the tree
                prefix = u"--" * (obj.get_level(self.parent_department) - 1)
                label = prefix + " " + label
        except AttributeError:
            pass
        return label


class OrganizationDepartmentFieldAddOption(OrganizationDepartmentField):
    def set_add_choice(self, add_choice=u"Добавить новое подразделение"):
        self.add_choice = add_choice

    @property
    def _choices(self):
        add_custom_choice = (("add_option", self.add_choice),)
        return chain(ModelChoiceIterator(self), add_custom_choice)

    def to_python(self, value):
        # import pdb; pdb.set_trace()
        if (value == "add_option"):
            return None
        else:
            return super(OrganizationDepartmentFieldAddOption, self).to_python(value)


class OrganizationForm(forms.Form):
    organization = OrganizationDepartmentField(label=u"Организация", empty_label=u"Выберите организацию", queryset=Organization.objects.all())
    new_organization_name = forms.CharField(label=u"Название организации", required=False)

    def clean(self):
        cleaned_data = self.cleaned_data
        organization = cleaned_data.get("organization")
        new_organization_name = cleaned_data.get("new_organization_name")
        if not organization and not new_organization_name:
            raise forms.ValidationError(u"Вы должны выбрать организацию из списка или добавить свой вариант.")
        return cleaned_data


class ParentDepartmentForm(forms.Form):
    parent_department = OrganizationDepartmentField(label=u"Подразделение", empty_label=u"Выберите подразделение", queryset=Department.objects.all())

    new_department_name = forms.CharField(label=u"Название подразделения", required=False)

    def __init__(self, *args, **kwargs):
        # organization is used for dynamically create a queryset of root departments.
        try:
            self.organization = kwargs.pop('organization')
        except KeyError:
            self.organization = None
        super(ParentDepartmentForm, self).__init__(*args, **kwargs)
        if self.organization:
            self.fields['parent_department'].queryset = self.organization.root_departments
        else:
            # empty queryset for initial form
            self.fields['parent_department'].queryset = Department.objects.none()

    def clean(self):
        logger.debug("Validating Parent Department form. Cleaned data: %s", self.cleaned_data)
        parent = self.cleaned_data.get("parent_department")
        new_department_name = self.cleaned_data.get("new_department_name")
        if parent and parent.organization != self.organization:
            logger.debug("Form validation failed! Parent department belongs to different organization.")
            raise forms.ValidationError(u"Подразделение не принадлежит к данной организации.")
        if not parent and not new_department_name:
            raise forms.ValidationError(u"Вы должны выбрать подразделение из списка или добавить свой вариант.")
        return self.cleaned_data


def ChildDepartmentForm(POST=None, **kwargs):
    # child_department = OrganizationDepartmentFieldAddOption(label=u"Дочернее подразделение", empty_label=u"Выберите дочернее подразделение", queryset=Department.objects.all(), required=False)
    # child_department.set_add_choice(u"Добавить новое подразделение") //Disable add child department choice. To enable uncomment, remove next line and see employment_edit.js in revision before 2015
    child_department = OrganizationDepartmentField(label=u"Дочернее подразделение", empty_label=u"Выберите дочернее подразделение", queryset=Department.objects.all(), required=False)
    new_child_department_name = forms.CharField(label=u"Название дочернего подразделения", required=False)

    fields = {'child_department': child_department, 'new_child_department_name': new_child_department_name}
    i = 1
    try:
        extra_field = POST['new_child_department_name%s' % (i)]
    except:
        extra_field = None
    while extra_field:
        fields['new_child_department_name%s' % i ] = forms.CharField(label=u"Название дочернего подразделения", required=False)
        i += 1
        try:
            extra_field = POST['new_child_department_name%s' % (i)]
        except:
            extra_field = None
    fields["extra_field_num"] = i - 1
    ChildDepartmentFormExpanded = type('ChildDepartmentFormExpanded', (forms.Form,), fields)
    temp = ChildDepartmentFormExpanded.base_fields

    def __init__(self, *args, **kwargs):
        try:
            self.parent_department = kwargs.pop('parent_department')
        except KeyError:
            self.parent_department = None
        try:
            self.new_department_name = kwargs.pop('new_department_name')
        except KeyError:
            self.new_department_name = None
        super(ChildDepartmentFormExpanded, self).__init__(*args, **kwargs)
        if self.parent_department:
            self.fields['child_department'].parent_department = self.parent_department
            # indent labels for children in the option tree
            self.fields['child_department'].indent_children = True
            self.fields['child_department'].queryset = self.parent_department.descendants_queryset
        else:
            self.fields['child_department'].queryset = Department.objects.none()

    def clean(self):
        logger.debug("Validating Child Department form. Cleaned data: %s", self.cleaned_data)
        parent = self.parent_department
        child = self.cleaned_data.get("child_department")
        new_department_name = self.new_department_name
        if not parent and not new_department_name and not child:
            logger.debug("Form validation failed! No department specified.")
            raise forms.ValidationError(u"Укажите организацию и подразделение.")
        if child and child.organization != parent.organization:
            logger.debug("Form validation failed! Child department belongs to different organization.")
            raise forms.ValidationError(u"Дочернее подразделение не принадлежит к данной организации.")
        if parent and child and child not in parent.descendants(show_level=False):
            logger.debug("Form validation failed! Child department is not a descendant of the parent department.")
            raise forms.ValidationError(u"Дочернее подразделение не является дочерним для данного подразделения.")
        return self.cleaned_data

    ChildDepartmentFormExpanded.__init__ = __init__
    ChildDepartmentFormExpanded.clean = clean
    return ChildDepartmentFormExpanded(POST, **kwargs)


class DepartmentChoiceField(Select2ModelField):

    def __init__(self, *args, **kwargs):
        super(DepartmentChoiceField, self).__init__(model=Department, *args, **kwargs)

    def get_title(self, department):
        return department.name

    def get_description(self, department):
        ancestors = department.ancestors
        description = ""
        for parent in ancestors:
            description += u'%s, ' % parent
        description += department.organization.name
        return description
    
class SubFacultyDepartmentChoiceField(Select2ModelField):
    
    def __init__(self, *args, **kwargs):
        super(SubFacultyDepartmentChoiceField, self).__init__(queryset=Department.objects.filter(name__istartswith="Факультет"), *args, **kwargs)
        
        

    def get_title(self, department):
        return department.name

    def get_description(self, department):
        ancestors = department.ancestors
        description = ""
        for parent in ancestors:
            description += u'%s, ' % parent
        description += department.organization.name
        return description   


class DepartmentMultipleChoiceField(Select2ModelMultipleField, DepartmentChoiceField):
    pass


class OranizationSelect2FieldNoCreation(Select2ModelField):
   def __init__(self, *args, **kwargs):
        super(OranizationSelect2FieldNoCreation, self).__init__(self, *args, input_string_option=True, model=Organization, **kwargs)

   def get_title(self, obj):
        return obj.name

   def to_python(self, value):
        try:
            int(value)
        except (ValueError, UnicodeEncodeError):
            return value
        else:
            return super(OranizationSelect2FieldNoCreation, self).to_python(value)


class DepartmentParametersForm(MyForm):

    def __init__(self, *args, **kwargs):
        self.department = kwargs.pop("department", None)
        self.year = int(kwargs.pop("year", None))
        self.categories = kwargs.pop("categories", None)
        super(DepartmentParametersForm, self).__init__(*args, **kwargs)
        for category in self.categories:
            field_name = category.to_field_name
            if category.is_integer:
                self.fields[field_name] = forms.IntegerField(label=category.name, required=False)
            else:  # TODO support more types
                self.fields[field_name] = forms.CharField(label=category.name, max_length=255, required=False)

    def save(self):
        for category in self.categories:
            field_name = category.to_field_name
            value = self.cleaned_data[field_name]
            if value not in [None, ""]:
                parameter_value, _ = category.parameters.get_or_create(department=self.department, year=self.year)
                parameter_value.value = value
                parameter_value.save()
            else:
                category.parameters.filter(department=self.department, year=self.year).delete()


class RepresentativeCreateEditForm(Select2ModelFormMixin):
    profile = ProfileChoiceField(label=u"Пользователь", empty_label=u"Выберите пользователя", required=True)
    is_active = TypedChoiceField(label=u"Активен",
                                 choices=((True, u"Да"), (False, u"Нет"), (None, u"Неизвестно")),
                                 widget=LightSelect2ChoiceWidget(width=308),
                                 coerce=string_to_bool_or_null)
    organization_field = forms.ModelChoiceField(label=u"Организация", queryset=Organization.objects.all(), empty_label=None)

    css_classes = [('scope', 'wide')]

    class Meta:
        model = Representative
        fields = ['profile', 'organization', 'organization_field', 'department', 'scope', 'post',
                  'email', 'phone_number', 'address', 'comment', 'is_active', 'startdate', 'enddate', ]

    def __init__(self,
                 request_user=None,
                 edit=False,
                 org_level=False,
                 *args, **kwargs):
        super(RepresentativeCreateEditForm, self).__init__(*args, **kwargs)
        self.edit = edit
        self.add = not edit
        if self.edit:
            self.fields['profile'].initial = get_profile(self.instance.user)
        self.request_user = request_user
        self.organization_field_data = None
        if org_level:
            repr_qs = request_user.representatives.filter(department__isnull=True)
            orgs_qs = Organization.objects.filter(id__in=repr_qs.values_list('organization', flat=True))
            if self.request_user and self.request_user.is_superuser:
                orgs_qs = Organization.objects.all()
            self.fields['organization'].queryset = orgs_qs

            if len(orgs_qs) == 1:
                self.fields['organization_field'] = forms.CharField()
                self.fields['organization_field'].label = u"Организация"
                self.fields['organization_field'].initial = Organization.objects.get(id=repr_qs.values_list('organization', flat=True)[0])
                self.fields['organization_field'].widget.attrs['readonly'] = 'readonly'
                self.organization_field_data = Organization.objects.get(id=repr_qs.values_list('organization', flat=True)[0])
                del self.fields['organization']
            else:
                self.fields['organization'].empty_label = None
                self.fields['organization'].required = True
                self.fields['organization'].queryset = orgs_qs
                del self.fields['organization_field']
            del self.fields['department']
        else:
            departments_sql = request_user.representatives.create_sql_statement_for_departments_dynatree(request_user)
            self.fields['department'].widget = DynatreeWidget(select_mode=1, initial_sql=departments_sql)
            self.fields['department'].required = True
            del self.fields['organization_field']
            del self.fields['organization']

    def clean_department(self):
        department = self.cleaned_data.get('department')
        if department is not None:
            if not (is_representative_for(self.request_user, department) or self.request_user.is_superuser):
                raise forms.ValidationError(u"У Вас не хватает полномочий, чтобы добавить ответственного сотрудника для данного подразделения.")
        return department

    def clean_organization(self):
        organization = self.cleaned_data.get('organization') or self.organization_field_data
        if organization is not None:
            if not (is_representative_for_whole_organization(self.request_user, organization)
                    or self.request_user.is_superuser):
                raise forms.ValidationError(u"У Вас не хватает полномочий, чтобы добавить ответственного сотрудника по данной организации")
        return organization

    def clean(self):
        cleaned_data = super(RepresentativeCreateEditForm, self).clean()
        department = cleaned_data.get('department')
        organization = cleaned_data.get('organization') or self.organization_field_data
        profile = cleaned_data.get('profile')
        if profile is not None:
            user = profile.user
            if user is not None and department is not None:
                if not self.edit:
                    if has_permission(user, "is_representative_for", department):
                        raise forms.ValidationError(u"Этот сотрудник уже является ответственным для данного подразделения.")
                else:
                    if is_representative_for(user, department, self.instance.id):
                        raise forms.ValidationError(u"Этот сотрудник уже является ответственным для данного подразделения.")
            if user is not None and organization is not None:
                if not self.edit:
                    if has_permission(user, "is_representative_for", organization):
                        raise forms.ValidationError(u"Этот сотрудник уже является ответственным для данной организации.")
                else:
                    if is_representative_for_whole_organization(user, organization, self.instance.id):
                        raise forms.ValidationError(u"Этот сотрудник уже является ответственным для данной организации.")
        return cleaned_data

    def save(self, request, commit=True):
        representative = super(RepresentativeCreateEditForm, self).save(commit=False, request=request)
        if not self.cleaned_data.get('startdate'):
            representative.startdate = datetime.date.today()
        representative.user = self.cleaned_data['profile'].user
        department = self.cleaned_data.get('department', None)
        if department:
            representative.organization = department.organization
        else:
            organization = self.cleaned_data.get('organization') or self.organization_field_data
            representative.organization = organization
        if commit:
            self.commit_save(request, representative)
        return representative


class RepresentativeEditYourselfForm(Select2ModelFormMixin):

    css_classes = [('scope', 'wide')]

    class Meta:
        model = Representative
        fields = ['scope', 'post', 'email', 'phone_number', 'address', 'startdate', 'comment']

    def __init__(self, request_user=None, *args, **kwargs):
        representative_instance = kwargs.pop('instance')
        if not representative_instance.email:
            representative_instance.email = representative_instance.user.email
        if not representative_instance.post \
                and request_user is not None \
                and request_user.get_profile() is not None \
                and request_user.get_profile().worker is not None \
                and request_user.get_profile().worker.current_employments.filter(department=representative_instance.department):
            representative_instance.post = request_user.get_profile().worker.current_employments.get(department=representative_instance.department).position
        kwargs['instance'] = representative_instance
        super(RepresentativeEditYourselfForm, self).__init__(*args, **kwargs)
        self.fields["scope"].help_text = u'укажите, какие вопросы находятся в Вашей компетенции в системе, например, "Общие вопросы"'
