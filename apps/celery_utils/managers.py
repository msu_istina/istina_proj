from djcelery.managers import TaskManager, transaction_retry


class MyTaskManager(TaskManager):
    @transaction_retry(max_retries=2)
    def update_task(self, task_id, defaults):
        return self.update_or_create(task_id=task_id, defaults=defaults)
