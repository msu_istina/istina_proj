from django.contrib import admin
from celery_utils.models import MyTaskState, MyTaskMeta
from djcelery.admin import TaskMonitor
from common.utils.admin import ForeignKeySmartSelectAdmin

class MyTaskMonitor(TaskMonitor, ForeignKeySmartSelectAdmin):

    fieldsets = (
        (None, {
            'fields': ('state', 'task_id', 'name', 'args', 'kwargs',
                       'eta', 'runtime', 'worker', 'tstamp', 'user'),
            'classes': ('extrapretty', ),
        }),
        ('Details', {
            'classes': ('collapse', 'extrapretty'),
            'fields': ('result', 'traceback', 'expires'),
        }),
    )
admin.site.register(MyTaskState, MyTaskMonitor)
admin.site.register(MyTaskMeta, ForeignKeySmartSelectAdmin)