# -*- coding: utf-8; -*-
from django.db import models
from djcelery.models import TaskState, TaskMeta, PickledObjectField
from celery_utils.managers import MyTaskManager
from celery_utils.celery_app import app
from celery.states import PENDING, FAILURE, SUCCESS

class MyTaskState(TaskState):
    user = models.ForeignKey(to="auth.User", related_name="celery_task_states", null=True, blank=True)


class MyTaskMeta(TaskMeta):
    user = models.ForeignKey(to="auth.User", related_name="celery_tasks", null=True, blank=True)
    task_info = PickledObjectField(null=True, default=None, editable=False)
    name = models.CharField(max_length=200, null=True,  blank=True)
    startdate = models.DateTimeField(auto_now_add=True)
    enddate = models.DateTimeField(null=True)
    objects = MyTaskManager()

    verbose_statuses = {
        PENDING : u'В процессе',
        FAILURE: u'Ошибка',
        SUCCESS: u'Выполнено'
    }
    class Meta:
        db_table = 'celery_mytaskmeta'
        ordering = ('-date_done', )

    def __init__(self, *args, **kwargs):
        super(MyTaskMeta, self).__init__(*args, **kwargs)

    @property
    def task_class(self):
        return app.tasks[self.name]

    @property
    def verbose_name(self):
        return self.task_class.verbose_name

    @property
    def result_url(self):
        return self.task_class.result_url(self)

    @property
    def verbose_status(self):
        return self.verbose_statuses[self.status]

