from celery import  Task
from datetime import datetime
from django.contrib.auth.models import User

class MyTask(Task):
    abstract = True
    task_verbose_name = None

    def run(self, user_id,  *args, **kwargs):
        task_info = kwargs.get('task_info')
        fields_to_update = {
            'name': self.task_verbose_name or self.name,
            'user': User.objects.get(pk=user_id),
            'task_info': task_info
        }
        self.update_task(**fields_to_update)

    def on_success(self, retval, task_id, args, kwargs):
        self.update_task(enddate=datetime.now())


    def update_task(self, **defaults):
        """
        Update task and store it's info in MyTaskMeta object. This method can be used to store in task_info field
        of MyTask any object, that can be pickled. If you don't need to store smth in custom fields of MyTaskMeta,
        it's better to use update_state method.
        """
        task_id = self.request.id
        self.backend.update_task(task_id, defaults)

    @classmethod
    def delay_shortcut(cls, *args, **kwargs):
        """
        Can be used to call delay function without explicit task isntantiating. Will not work in complex cases, when
        constructor must accept arguments, in this case use delay function.
        """
        return cls().delay(*args, **kwargs)

