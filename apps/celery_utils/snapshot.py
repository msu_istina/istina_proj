from djcelery.snapshot import Camera, maybe_make_aware, aware_tstamp, maybe_iso8601, NOT_SAVED_ATTRIBUTES
from celery_utils.models import MyTaskState, MyTaskMeta


class MyCamera(Camera):
    """Override native Camera class to save creator or other info to task snapshot """
    TaskState = MyTaskState

    def update_task(self, state, **kwargs):
        task_id = kwargs.get('task_id')
        try:
            task_meta = MyTaskMeta.objects.get(task_id=task_id)
        except MyTaskMeta.DoesNotExist:
            pass
        else:
            defaults = kwargs.get('defaults', {})
            defaults['user'] = task_meta.user
            kwargs['defaults'] = defaults
        return super(MyCamera, self).update_task(state, **kwargs)