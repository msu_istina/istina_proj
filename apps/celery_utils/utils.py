# -*- coding: utf-8; -*-
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy

def task_added_message(request):
    tasks_list_url =  u'<a href=%s> здесь </a>' % reverse_lazy('common_celery_tasks_list')
    messages.success(request, u'Заявка передана на обработку. Статус выполнения заявок доступен %s.' % tasks_list_url)
