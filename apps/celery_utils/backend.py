from djcelery.backends.database import DatabaseBackend
from celery_utils.models import MyTaskMeta


class MyDatabaseBackend(DatabaseBackend):
    TaskModel = MyTaskMeta

    def update_task(self, task_id, defaults):
        self.TaskModel._default_manager.update_task(task_id, defaults)