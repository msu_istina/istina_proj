from djcelery.management.commands.celerycam import Command as CeleryCamCommand, ev

class Command(CeleryCamCommand):
    def handle(self, *args, **options):
        """Handle the management command."""
        options['camera'] = 'celery_utils.snapshot.MyCamera'
        ev.run(*args, **options)