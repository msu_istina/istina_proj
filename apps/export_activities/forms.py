# -*- coding: utf-8; -*-
from django import forms

from common.forms import MyForm
from organizations.models import Department
from db_procedures import export_activities_type_choices


class ExportForm(MyForm):
    export_type = forms.ChoiceField(label=u'Тип экспорта')
    department = forms.ModelChoiceField(Department.objects.none(), label=u'Подразделение')

    def __init__(self, worker, *args, **kwargs):
        super(ExportForm, self).__init__(*args, **kwargs)
        worker_departments_ids = worker.current_departments_ids
        self.fields['department'].queryset = Department.objects.filter(pk__in=worker_departments_ids)
        self.fields['export_type'].choices = export_activities_type_choices().items()
