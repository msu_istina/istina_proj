# -*- coding: utf-8; -*-
from common.utils.context import render_to_response
from export_activities.forms import ExportForm
from common.utils.user import get_worker
from export_activities.tasks import  ExportActivitiesTask
from celery_utils.utils import task_added_message

def export_activities(request, *args, **kwargs):
    worker = get_worker(request.user)
    export_form = None
    if worker:
        export_form = ExportForm(worker, request.POST or None)
        if export_form.is_valid():
            export_type = export_form.cleaned_data['export_type']
            department = export_form.cleaned_data['department']
            ExportActivitiesTask.delay_shortcut(request.user.id, export_type, department.id)
            task_added_message(request)
            export_form = ExportForm(worker, None)
    return render_to_response(request, "export_activities/export_activities.html", {'export_form': export_form})

