# -*- coding: utf-8; -*-
from django.db import connection
from django.core.files.base import ContentFile
from common.models import Attachment, AttachmentCategory



def select_from_db_function_sql(func_name, args=None):
    args = args or ()
    sql = "SELECT {func_name}({format_substring})  FROM dual"
    format_substring = ','.join(['%s'] * len(args))
    sql = sql.format(format_substring=format_substring, func_name=func_name)
    return sql, args


def export_activities_type_choices():
    c = connection.cursor()
    try:
        sql_result = c.execute(*select_from_db_function_sql('IMP_ORG.get_enable_list'))
        choices_string = sql_result.fetchone()[0]
    finally:
        c.close
    choices_list = filter(None, choices_string.split(';'))
    choices = {}
    for choice_string in choices_list:
        choice, verbose = choice_string.split(',')
        choices[choice] = verbose.decode('utf-8')
    return choices

def export_organization_activities(user, report_type, department, report_folder, worker_id=None):
    organization = department.organization
    args = report_type, str(user.id), str(organization.id), worker_id, str(department.id)

    c = connection.cursor()
    try:
        sql_result = c.execute(*select_from_db_function_sql("IMP_ORG.GET_IMPORT",
                                                        args))
        report_lob =sql_result.fetchone()[0]
    finally:
        c.close()
    attachment_category, _ = AttachmentCategory.objects.get_or_create(name=u"Другое")
    attachment = Attachment(category=attachment_category, creator=user)
    report_content_file = ContentFile(report_lob.read())
    full_report_path = 'export_activities/' + report_folder + '/report'
    attachment.link.save(full_report_path, report_content_file, save=False)
    return attachment

