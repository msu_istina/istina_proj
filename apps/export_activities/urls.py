# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *

from export_activities.views import export_activities


urlpatterns = patterns('',
    url(r'^export_activities/$',  export_activities, name='export_activities'),
)
