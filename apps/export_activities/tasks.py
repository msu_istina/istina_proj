# -*- coding: utf-8; -*-
from __future__ import absolute_import
from celery_utils.task import MyTask
from export_activities.db_procedures import export_organization_activities
from common.models import Attachment

from django.contrib.contenttypes.models import ContentType

class ExportActivitiesTask(MyTask):
    name = 'export_activities'
    verbose_name = u'Генерация отчета по деятельности организации'

    def run(self, user_id, report_type, department_id,  worker_id=None, *args, **kwargs):
        from organizations.models import Department
        from django.contrib.auth.models import User
        from celery_utils.models import MyTaskMeta

        super(ExportActivitiesTask, self).run(user_id, report_type, department_id,  worker_id,
                                              *args, **kwargs)
        department = Department.objects.get(pk=department_id)
        user = User.objects.get(pk=user_id)
        attachment = export_organization_activities(user, report_type, department, str(self.request.id),
                                                     worker_id)

        task_meta = MyTaskMeta.objects.get(task_id=self.request.id)
        attachment.content_object = task_meta
        attachment.save()

    @classmethod
    def result_url(cls, task_meta):
        from celery_utils.models import MyTaskMeta
        content_type = ContentType.objects.get_for_model(MyTaskMeta)
        try:
            report = list(Attachment.objects.filter(content_type=content_type,
                                                object_id=task_meta.id))[0]
        except IndexError:
            return ''

        return report.link.url
