# -*- coding: utf-8; -*-
from django.contrib import messages
from django.contrib.auth.views import logout
from django.contrib.flatpages.views import flatpage
from django.contrib.staticfiles.views import serve
from django.core.urlresolvers import reverse
from django.views.generic.simple import direct_to_template
from django.shortcuts import redirect
from pytils.templatetags.pytils_numeral import get_plural
from django.conf import settings
import logging

from common.views import LinkedToWorkersModelWizardView, MyModelWizardView
from conferences.views import ConferenceCommitteeMembershipWizardView, ConferencePresentationWizardView
from dissertation_councils.views import DissertationCouncilMembershipWizardView
from dissertations.views import DissertationWizardView
from icollections.views import CollectionEditorshipWizardView
from investments.views import InvestmentWizardView
from journals.views import JournalEditorialBoardMembershipWizardView
from publications.models import ParsingEvent
from publications.views import parse_data, add_work, clear_adding_queue
from workers.models import Profile
from workers.views import profile_edit, employment_edit
from reversion.revisions import revision_context_manager
logger = logging.getLogger("common.middleware")


class MessageMiddleware(object):
    """
    Middleware that adds some annoying messages for users who has not completed something.
    """
    def process_view(self, request, view_func, view_args, view_kwargs):
        skip_views = [serve, logout]
        # uncomment to activate upcoming maintenance message
        # (or any other message on every page on the site)
        # if not view_func in skip_views and not self.message_exists(request, 'upcoming_maintenance'):
            # messages.info(request, u'Сегодня с 22.00 до 1.00 система будет отключена в связи с переездом на новое оборудование в целях повышения производительности.', extra_tags='upcoming_maintenance')        
        if request.user.is_authenticated() and not request.is_ajax() and not request.method == "POST" and not view_func in skip_views and not request.path.startswith('/workers/registration/'):
            class_based_views = [LinkedToWorkersModelWizardView, MyModelWizardView, DissertationCouncilMembershipWizardView, JournalEditorialBoardMembershipWizardView, DissertationWizardView, CollectionEditorshipWizardView, InvestmentWizardView, ConferenceCommitteeMembershipWizardView, ConferencePresentationWizardView]
            add_view = view_func in [parse_data, add_work] or self.is_class_based_view(view_func, class_based_views)
            try:
                worker = request.user.get_profile().worker
            except Profile.DoesNotExist:
                Profile.objects.create(user=request.user) # create missing profile
                worker = None
            if not request.user.get_profile().registration_completed() and (not hasattr(settings, 'USER_REGISTRATION_WIZARD_ACTIVE') or settings.USER_REGISTRATION_WIZARD_ACTIVE):
                return redirect("workers_registration")
            if not worker and view_func != profile_edit and not self.message_exists(request, 'link_worker'):
                #logger.debug("Adding linking to profile message, view_func: %s (module %s), args: %s, kwargs: %s", view_func, view_func.__module__, view_args, view_kwargs)
                messages.info(request, u'Пожалуйста, <a href="%s" title="Найти себя в базе сотрудников">найдите себя</a> в базе сотрудников.' % reverse("workers_profile_edit"), extra_tags='link_worker')
            if worker:
                view_employment = view_func in [employment_edit]
                view_works = add_view or (view_func == direct_to_template and view_kwargs.get('template', None) in ['publications/add_citation.html', 'add_work.html'])
                if not worker.employed and not view_employment and (worker.has_works or (not worker.has_works and not view_works)) and not self.message_exists(request, 'add_employment'):
                    #logger.debug("Adding employment message, view_func: %s (module %s), args: %s, kwargs: %s", view_func, view_func.__module__, view_args, view_kwargs)
                    messages.info(request, u'Пожалуйста, <a href="%s" title="Перейти на страницу выбора места работы">укажите место работы.</a>' % reverse("workers_employment_edit"), extra_tags='add_employment')
                if not worker.has_works and not view_works and (worker.employed or (not worker.employed and not view_employment)) and not self.message_exists(request, 'add_works'):
                    #logger.debug("Adding add works message, view_func: %s (module %s), args: %s, kwargs: %s", view_func, view_func.__module__, view_args, view_kwargs)
                    messages.info(request, u'Пожалуйста, <a href="%s" title="Перейти на страницу добавления научных работ">добавьте свои работы.</a>' % reverse("add_work"), extra_tags='add_works')
            queue_size = ParsingEvent.objects.get_queue_size(request.user)
            clear_queue_view = (view_func == clear_adding_queue)
            if queue_size and not add_view and not clear_queue_view and not self.message_exists(request, 'add_work_from_queue'):
                #logger.debug("Adding parsing next from queue welcome message, view_func: %s (module %s), args: %s, kwargs: %s", view_func, view_func.__module__, view_args, view_kwargs)
                messages.info(request, u'В вашей очереди добавления находится %s. <a href="%s" title="Перейти на страницу добавления следующей публикации из очереди">Добавить следующую публикацию</a><a class="margin-left" href="%s" title="Удалить все публикации из очереди добавления">очистить очередь</a>' % (get_plural(queue_size, [u"публикация", u"публикации", u"публикаций"]), reverse('publications_add_next_from_queue'), reverse('publications_add_queue_clear')), extra_tags='add_work_from_queue')

    def process_response(self, request, response):
        '''This function is needed in the case when a flatpage has been requested. In this case process_view has not been called, since no view has been found.'''
        if response.status_code == 404:
            import settings
            if 'django.contrib.flatpages' in settings.INSTALLED_APPS:
                try:
                    fpage = flatpage(request, request.path_info) # it can be a flatpage response or it can be anything else, we should check
                    if dict(fpage.items())['X-Object-Type'] == 'flatpages.flatpage':
                        # only if we actually returning a flatpage, execute process_view, because it has not been executed yet.
                        self.process_view(request, None, [], {})
                except:
                    pass
        return response

    def message_exists(self, request, tag):
        '''Returns True if request contains a message with specified extra tag. Works only if there is only one extra tag.'''
        try: # get already loaded messages to avoid duplicates
                loaded_messages = messages.get_messages(request)._loaded_messages
        except:
            return False
        for message in loaded_messages:
            if message.extra_tags == tag: # message with this tag exists
                return True
        return False

    def is_class_based_view(self, view_func, class_based_views):
        for view in class_based_views:
            if view_func.__name__ == view.__name__:
                return True
        return False


class DebugToolbarToogleMiddleware(object):
    """
    Middleware that handles DISABLE_DEBUG_TOOLBAR session flag
    """
    def process_view(self, request, view_func, view_args, view_kwargs):
        from django.conf import settings
        if not settings.DEBUG or not hasattr(settings, "DEBUG_TOOLBAR_SESSION_KEY"):
            return
        if settings.DEBUG_TOOLBAR_DISABLE_REQUEST in request.GET:
            request.session[settings.DEBUG_TOOLBAR_SESSION_KEY] = 1
        elif settings.DEBUG_TOOLBAR_ENABLE_REQUEST in request.GET:
            request.session[settings.DEBUG_TOOLBAR_SESSION_KEY] = 0


class RevisionUserMiddleware(object):

    def process_request(self, request):
        if hasattr(request, "user") and request.user.is_authenticated():
            revision_context_manager._user = request.user

    def process_response(self, request, response):
        revision_context_manager._user = None
        return response

