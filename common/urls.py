# -*- coding: utf-8; -*-
from django.conf.urls.defaults import *
from common.views import (
    datatables_json_by_id, MergeWizardView, merge_request_view,
    datatable_merge_requests_json, merge_request_json_by_id,
    celery_tasks_list, link_to, unauthor, add_redmine_ticket
)

urlpatterns = patterns(
    'common.views',
    url(r'^link_to/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$',
        link_to, name='common_link_to_object'),
    url(r'^unauthor/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$',
        unauthor, name='common_unauthor_object'),
    url(r'^attachments/add/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$',
        'add_attachments', name='common_attachments_add'),
    url(r'^attachments/delete/(?P<content_type_id>\d+)/(?P<object_id>\d+)/(?P<attachment_id>\d+)/$',
        'delete_attachments', name='common_attachments_delete'),
    url(r'^attachments/delete/(?P<content_type_id>\d+)/(?P<object_id>\d+)/all/$',
        'delete_attachments', {'delete_all': True}, name='common_attachments_delete_all'),
    url(r'^json_by_id/(?P<app_label>\w+)/(?P<model_name>\w+)/$',
        datatables_json_by_id, name="datatables_json_by_id"),
    url(r'^merge_request_json/(?P<merge_request_id>\d+)$',
        merge_request_json_by_id, name="common_merge_request_json_by_id"),
    url(r'^(?P<app_label>\w+)/(?P<model_name>\w+)/merge_wizard/$',
        MergeWizardView.as_view(), {'merge_step': 'candidates_list'}, name="conferences_merge"),
    url(r'^(?P<app_label>\w+)/(?P<model_name>\w+)/merge_wizard/final$',
        MergeWizardView.as_view(), {'merge_step': 'final'}, name="conferences_merge_final"),
    url(r'^merge_requests/$', merge_request_view, name="common_merge_requests"),
    url(r'^merge_requests/datatable_json/$',
        datatable_merge_requests_json, name="common_datatable_json_merge_requests"),
    url(r'^merge_requests/detail/(?P<merge_request_id>\d+)$',
        MergeWizardView.as_view(), {'merge_step': 'final'}, name="common_merge_requests_detail"),
    url(r'^tasks_list/$', celery_tasks_list, name="common_celery_tasks_list"),
    url(r'^delete_if_no_related_objects/(?P<content_type_id>\d+)/(?P<object_id>\d+)/$',
        'delete_if_no_related_objects', name='common_delete_if_no_related_objects'),
    url(r'^add_ticket/$', add_redmine_ticket, name="common_add_redmine_ticket"),
    url(r'^confirm_authorscience/$', "confirm_authorscience", name="confirm_authorscience"),
    url(r'^recall_authorscience/$', "recall_authorscience", name="recall_authorscience"),
    url(r'^add_authorscience/(?P<object_id>\d+)/$', "add_authorscience", name="add_authorscience")


)
