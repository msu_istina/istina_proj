# -*- coding: utf-8; -*-
from actstream import action
from django import forms
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.forms.models import modelformset_factory, BaseInlineFormSet
from django_select2 import AutoModelSelect2Field, ModelSelect2Field, AutoModelSelect2MultipleField, \
    AutoModelSelect2TagField, NO_ERR_RESP
import re
import copy

from common.models import Attachment, Country
from common.utils.list import is_tuple_list, flatten
from common.utils.strings import capitalize_1st_letter
from common.utils.validators import validate_year
from common.widgets import Select2ChoiceWidget, LightSelect2ChoiceWidget, Select2MultipleChoiceWidget, Select2WorkersWidget, Select2TagWidget


class MyBaseForm(forms.BaseForm):

    def __init__(self, *args, **kwargs):
        super(MyBaseForm, self).__init__(*args, **kwargs)
        if hasattr(self, 'fields_order') and set(self.fields_order) == set(self.fields.keys()):
            self.fields.keyOrder = self.fields_order # do it only if no fields will be added dynamically
        # common css classes
        for field in ['title', 'name']:
            self.add_css_class(field, 'wide')
        for field in ['number', 'year']:
            self.add_css_class(field, 'narrow')
        for field in ['date', 'startdate', 'enddate','actual_date']:
            self.add_css_class(field, 'narrow datepicker')
        if hasattr(self, 'css_classes'):
            for field, css_class in self.css_classes:
                self.add_css_class(field, css_class)
        if hasattr(self, 'css_container_classes'):
            for field, css_class in self.css_container_classes:
                self.add_css_class(field, css_class, to_container=True)
        # set default organization, if applicable
        if 'organization' in self.fields and not getattr(self, "disable_organization_preselection", False):
            from organizations.models import Organization # to avoid circular import
            index = min(getattr(self, 'organization_index', 0), Organization.objects.count() - 1) # index can be 0 (first organization, or, e.g., 1 (second organization, MSU). Not a perfect assumption, but effective and simple (and not critical, it's just an initial value). Should be fixed later.
            if index >= 0:
                self.fields['organization'].initial = Organization.objects.all()[index]

    def init_field(self, field_name, field_class):
        if field_name in self.fields:
            self.fields[field_name] = field_class(label=self.fields[field_name].label)

    def add_css_class(self, field_name, css_class, to_container=False):
        '''Adds a specified css class to a field or its div container.'''
        try:
            field = self.fields[field_name]
        except KeyError:
            return 1
        old_css_classes = getattr(field, 'css_container_classes', '') if to_container else field.widget.attrs.get('class', '')
        space = " " if old_css_classes else ""
        new_css_classes = "%s%s%s" % (old_css_classes, space, css_class)
        if to_container:
            field.css_container_classes = new_css_classes
        else:
            field.widget.attrs['class'] = new_css_classes

    def remove_css_class(self, field_name, css_class):
        '''Removes a specified css class from a field.'''
        try:
            field = self.fields[field_name]
        except KeyError:
            return 1
        css_classes = field.widget.attrs.get('class', '').split(" ")
        try:
            css_classes.remove(css_class)
        except ValueError:
            pass
        else:
            field.widget.attrs['class'] = " ".join(css_classes)

    def hide_field(self, field_name):
        '''Hides a form field, adding a 'hide' css class.'''
        self.add_css_class(field_name, 'hide')

    def hide_fields(self, fields_names):
        '''Hides form fields, adding a 'hide' css class.'''
        if is_tuple_list(fields_names):
            for field_name in fields_names:
                self.hide_field(field_name)
        else:
            self.hide_field(fields_names)


class MyForm(MyBaseForm, forms.Form):
    '''Just for inheritance simplicity.'''


class MyModelForm(MyBaseForm, forms.ModelForm):
    '''A simple model form for objects.'''

    add = True # add mode
    edit = False # edit mode

    def __init__(self, *args, **kwargs):
        super(MyModelForm, self).__init__(*args, **kwargs)
        # for fields with no labels, get labels from model
        for name, field in self.fields.items():
            if field.label is None and getattr(self._meta.model, name, None):
                field.label = getattr(self._meta.model, name).field.verbose_name

    def commit_save(self, request, object):
        '''Method that is executed just before final commiting the object into a database.'''
        description = ''
        if hasattr(object, "xml"):
            if self.edit: # if edit, store old xml in event description
                description = u'старый XML %s: %s' % (object.genitive, object.xml) #FIXME temporarily disable xml logging because of projects
                object.xml = "" # clear old xml, not to store it twice
            object.xml = object.get_xml()
        object.save() # save all data including xml
        action_str = (u"добавил", u"добавлен" + object.gender_suffix) if self.add else (u"отредактировал", u"отредактирован" + object.gender_suffix)
        action.send(request.user, verb=u'%s %s' % (action_str[0], object.accusative), target=object, description=description)
        messages.success(request, u"%s успешно %s." % (capitalize_1st_letter(object.nominative), action_str[1]))

    def save(self, request, commit=True):
        self.add = not self.instance_linked()
        self.edit = not self.add
        obj = super(MyModelForm, self).save(commit=False)
        if self.add and hasattr(obj.__class__, "creator"):
            obj.creator = request.user
        if commit:
            self.commit_save(request, obj)
        return obj

    def instance_linked(self):
        try:
            id = self.instance.id
            return bool(id)
        except:
            return False


class Select2ModelFormMixin(MyModelForm):
        def _post_clean(self):
            select2_choice_fields = []
            for field_name in self.fields:
                if (isinstance(self.fields[field_name], Select2ModelField)
                    and self.fields[field_name].add_option
                    and self.cleaned_data[field_name] is None):
                        select2_choice_fields.append(field_name)
                        self._meta.exclude.append(field_name)
            super(Select2ModelFormMixin, self)._post_clean()
            for field_name in select2_choice_fields:
                self._meta.exclude.pop()



class LinkedToWorkersModelForm(Select2ModelFormMixin):
    '''A simple model form for objects linked to workers through an intermediate model. Used for LinkedToWorkersModel instances.'''

    def __init__(self, *args, **kwargs):
        super(LinkedToWorkersModelForm, self).__init__(*args, **kwargs)
        for field, label in zip(self.workers_fields, self.workers_labels):
            self.fields[field] = forms.CharField(label=label)
        if hasattr(self, 'fields_order'): # do it here, since we've added a new field, and in parent class this has not been done
            self.fields.keyOrder = self.fields_order
        if self.instance_linked(): # initialize workers field
            self.fields[self.workers_fields[0]].initial = self.instance.workers_string
        for field in self.workers_fields:
            self.add_css_class(field, 'wide autocomplete_workers')

    @property
    def workers_fields(self):
        '''Returns the names of the fields that contain names of the linked workers.
        NB: the name of the field is expected to be equal to model's workers_attr + '_str', e.g. 'advisers_str'.
        If several names are expected, the function should be overriden.
        '''
        return [self.Meta.model.workers_attr + '_str']

    @property
    def workers_labels(self):
        return [self.Meta.model.workers_verbose_name_plural]

    def commit_save(self, request, object, workers):
        '''Method that is executed just before final commiting the object into a database.'''
        super(LinkedToWorkersModelForm, self).commit_save(request, object)
        # if edit, first delete old object-worker intermediate objects(e.g. authorships), then save the new ones
        if workers and is_tuple_list(workers[0]):
            # e.g. from projects workers is not a list, but a list of lists
            # we need to make a flat list
            workers_flattened = flatten(workers)
        else:
            workers_flattened = workers
        object.notify_new_workers(workers_flattened, request.user)
        if self.edit:
            object.workerships.all().delete()
        if workers:
            # save intermediate objects
            object.add_workers(workers, request.user)

    def save(self, request, workers=None, commit=True):
        object = super(LinkedToWorkersModelForm, self).save(request, commit=False)
        if commit:
            self.commit_save(request, object, workers)
        return object


class WorkershipModelForm(LinkedToWorkersModelForm):
    '''A simple model form for objects linked to workers directly. Used for WorkershipModel instances.
    This class subclasses LinkedToWorkersModelForm, unlike WorkershipModel, which does not subclass the corresponding class. This is done because these form classes are very similar.
    '''
    @property
    def workers_fields(self):
        '''Returns the names of the fields that contain names of the linked workers.
        NB: the name of the field is expected to be equal to model's workers_attr + '_str', e.g. 'advisers_str'.
        If several names are expected, the function should be overriden.
        '''
        return [self.Meta.model.worker_attr + '_str']

    @property
    def workers_labels(self):
        return [self.Meta.model.workers_verbose_name_single]

    def commit_save(self, request, object, workers):
        '''Method that is executed just before final commiting the object into a database.'''
        if workers:
            try:
                old_workers = [getattr(object, object.worker_attr, None)]
            except:
                old_workers = []
            # take the first worker and link the object to it
            setattr(object, object.worker_attr, workers[0])
        # note that the method of the parent class is not called
        super(LinkedToWorkersModelForm, self).commit_save(request, object)
        if workers:
            object.notify_new_workers(workers, request.user, old_workers=old_workers)


class YearField(forms.IntegerField):
    """Integer field with enforced validate_year validator"""
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("validators", []).append(validate_year)
        super(YearField, self).__init__(*args, **kwargs)


class MyBaseSelect2ModelField(object):
    '''
    This is mixin, used to add some functionality to all select2 fields used in project. We use django-select2
    implementation of select2 fields in python.
    Links:
        * Select2 - it's not necessary to read, only to get an idea. http://ivaynberg.github.io/select2/
        * Django-select2 - implementation of select2 python wrapper. http://django-select2.readthedocs.org/en/latest/
        * Example of usage of select2 fields at website(bottom of the page, add article field). http://istina.msu.ru/projects/5335272/
        * Examples of usage in code: projects.forms:ProjectArticleForm, projects.forms:ProjectMembersForm,
          organizations.forms:OranizationSelect2FieldNoCreation
    Select2 fields can be used as a replacement for default ChoiceField, ModelChoiceField, and ModelMultipleChoiceField.
    Select2 fields allow to select one or multiple options from dropdown menu. The options can be a list of strings or
    a list of instances of one model(e.g. choose one or several articles from the database).The main features - autocompletion and
    support out of the box for ajax requests when there are too many options to select from. It also can be customized in
    several ways by passing some arguments. By default  you should use Select2ModelField, but there re several other variants (
    see LightSelect2ModelField, Select2ModelMultipleField, Select2TagField below).
    This mixin allows constructor to accept following arguments:
        * model - model class, that will be used to generate options
        * queryset - instead of model you can pass specific queryset, that will be used as select options. If you don't pass
        queryset or model, get_queryset method will be called. If it is not implemented, AttributeError will be raised.

        * search_field - model field, that is used, when user inputs the beginning of the string. By default uses
          model.search_attr.
        * prefix - str, users as prefix for generating id of the form. If several instances of the select2field (with same
         model and same class) are rendered on the same page, each one must have it's own unique prefix, otherwise,
         they will have the same options even if they mustn't.
        * required - bool, specifies if the field is required. If add_option=True, then required will be set to False.
        * widget_kwargs - dict, specifies kwargs, that will be passed to field widget. There are some options
        * add_option - bool, if True , additional option will be generated, that will be cleaned as None with text u'Добавить новый'.
        This argument is passed to field widget.
        * input_string_option - bool, if True, user can input it's own option from the keyboard. You should manually
         control cleaning of this field, for example see organizations.forms:OranizationSelect2FieldNoCreation.
    This mixin has following attributes:
        * max_results - max number of options to render.
        * empty_values - shoulnd't be overrided
    Following methods can be overriden:
        * get_title(self, obj) - returns unicode representation of object, that is used to generate title of option
        * get_description(self, obj) - each object can have a short description, that will be rendered under the title.
        This method generates it. Description can contain HTML.
    Widget mixin, that is used to add some options to default django_select2 widget, can be found at common.widgets:MySelect2BaseWidget.
    It should be edited in case you want to customize select2 javascript rendering. For the instructions look
    at http://django-select2.readthedocs.org/en/latest/ref_widgets.html#module-django_select2.widgets
    '''
    empty_values = [u'']
    max_results = 100

    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('model', None)
        queryset = kwargs.get('queryset', None)

        if queryset is not None:
            kwargs['queryset'] = queryset
            self.model = None
        else:
            try:
                kwargs['queryset'] = self.model.objects
            except AttributeError:
                kwargs['queryset'] = self.get_queryset()
        self.add_option = kwargs.pop('add_option', False)
        self.input_string_option = kwargs.pop('input_string_option', False)
        widget_kwargs = kwargs.pop('widget_kwargs', {})
        kwargs['widget'] = self.widget(add_option=self.add_option,
                                       input_string_option=self.input_string_option,
                                       **widget_kwargs)
        model = self.model or kwargs['queryset'].model
        search_field = kwargs.pop('search_field', model.search_attr)
        try:
            self.search_fields
        except AttributeError:
             self.search_fields = [search_field + '__icontains']
        prefix = str(kwargs.pop('prefix', ''))
        kwargs['auto_id'] = prefix + self.__class__.__name__ + str(model)
        super(MyBaseSelect2ModelField, self).__init__(*args, **kwargs)
        self.required = kwargs.pop('required', True)
        if self.add_option:
            self.required = False

    def label_from_instance(self, obj):
        title = self.get_title(obj)
        description = self.get_description(obj)
        delimiter = '@title_end@'
        return escape(title + delimiter + description)

    def get_title(self, obj):
        return unicode(obj)

    def get_description(self, obj):
        return u''


class Select2ModelField(MyBaseSelect2ModelField, AutoModelSelect2Field):
    '''
    Select2ModelField, that uses ajax to get autocomplete options. Should be used by default.
    '''
    widget = Select2ChoiceWidget


class LightSelect2ModelField(MyBaseSelect2ModelField, ModelSelect2Field):
    '''
    Select2ModelField, that renders all options during page rendering.
    '''
    widget = LightSelect2ChoiceWidget


class Select2ModelMultipleField(MyBaseSelect2ModelField, AutoModelSelect2MultipleField):
    '''
    Select2ModelField, similar to Select2ModelField, allows to select multiple options
    '''
    widget = Select2MultipleChoiceWidget


class Select2TagField(MyBaseSelect2ModelField, AutoModelSelect2TagField):
    '''
    Similar to Select2ModelMultipleField, but uses tag interface.
    '''
    widget = Select2TagWidget


class Select2WorkerMultipleField(Select2TagField):
    widget = Select2WorkersWidget


def get_Select2WorkerField(select_single=False, select_multiple=False):
    '''
    Select2 realization of worker selection. An example can be seen on the second step of project adding.
    '''
    if select_single and not select_multiple:
        base_class = Select2ModelField
    elif select_multiple and not select_single:
        base_class = Select2WorkerMultipleField
    else:
        # raise an error ?
        base_class = Select2WorkerMultipleField
    class Select2WorkerField(base_class):
        search_fields = ('lastname__istartswith',)

        def get_queryset(self):
            from workers.models import Worker # to avoid circular import
            return Worker.objects.prefetch_related('departments')

        def get_description(self, obj):
            return u', '.join(unicode(department) for department in obj.departments.all())

        def save(self, *args, **kwargs):
            return super(Select2WorkerField, self).save(*args, **kwargs)

        def get_model_field_values(self, worker_string):
            ''' Worker construction method is partialy taken from workers.forms.WorkerForm '''
            worker_string_parts = re.split('[. ]',worker_string)
            worker_string_parts = [part.strip(".,:-") for part in worker_string_parts]
#            if len(worker_string_parts) != 3:
#                raise ValidationError(u'Для нового сотрудника %s нужно указать фамилию, имя и отчество' % worker_string)
            if len(worker_string_parts) == 3:
                lastname, firstname, middlename = worker_string_parts
            elif len(worker_string_parts) == 2:
                lastname, firstname  = worker_string_parts
                middlename = None
            else:
                raise ValidationError(u'Для нового сотрудника %s нужно указать фамилию, имя и отчество' % worker_string)
            return {'lastname': lastname, 'middlename': middlename, 'firstname': firstname}

        def sort_results(self, res):
            def worker_key(worker):
                if not worker.departments.exists():
                    return True, None
                else:
                    return False, worker.departments.all()[0].name
            return sorted(res, key=worker_key)

        def prepare_qs_params(self, request, search_term, search_fields):
            search_term_parts = search_term.split()
            filter_conditions = {}
            if len(search_term_parts) == 1:
                filter_conditions = {'lastname__istartswith': search_term_parts[0]}
            if len(search_term_parts) == 2:
                filter_conditions = {'lastname__iexact': search_term_parts[0],
                                     'firstname__istartswith': search_term_parts[1]}
            if len(search_term_parts) >= 3:
                filter_conditions = {'lastname__iexact': search_term_parts[0],
                                     'firstname__iexact': search_term_parts[1],
                                     'middlename__icontains': search_term_parts[2]}
            return {'or': [], 'and': filter_conditions}

        def get_results(self, request, term, page, context):
            """
            Copied  and modified from django_select2.fields.ModelResulJsonMix  because it's impossible to get what we want by
            inheriting(sort results)
            """
            if not hasattr(self, 'search_fields') or not self.search_fields:
                raise ValueError('search_fields is required.')
            if not term.strip():
                return (NO_ERR_RESP, False, [], )
            qs = copy.deepcopy(self.get_queryset())
            params = self.prepare_qs_params(request, term, self.search_fields)

            if self.max_results:
                min_ = (page - 1) * self.max_results
                max_ = min_ + self.max_results + 1  # fetching one extra row to check if it has more rows.
                res = list(qs.filter(*params['or'], **params['and'])[min_:max_])
                has_more = len(res) == (max_ - min_)
                if has_more:
                    res = res[:-1]
            else:
                res = list(qs.filter(*params['or'], **params['and']))
                has_more = False
            res = self.sort_results(res) #only modified line from original implementation
            res = [(getattr(obj, self.to_field_name), self.label_from_instance(obj), self.extra_data_from_instance(obj))
                    for obj in res]
            return (NO_ERR_RESP, has_more, res, )
    return Select2WorkerField

Select2WorkerField = get_Select2WorkerField(select_multiple = True)
Select2SingleWorkerField = get_Select2WorkerField(select_single = True)

class Select2LinkedToWorkersModelForm(LinkedToWorkersModelForm):

    def __init__(self, *args, **kwargs):
        super(LinkedToWorkersModelForm, self).__init__(*args, **kwargs)
        '''Disable worker fields constructing in init method because it doesn't work with Select2 - it must register fields
        during compilation or will be problems with multiple threads'''
        #for field, label in zip(self.workers_fields, self.workers_labels):
        #    self.fields[field]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          = Select2WorkerField(label=label)
        if hasattr(self, 'fields_order'): # do it here, since we've added a new field, and in parent class this has not been done
            self.fields.keyOrder = self.fields_order
        if self.instance_linked(): # initialize workers field
            for worker_field in self.workers_fields:
                workers = getattr(self.instance, worker_field)
                try:
                    workers = workers.all() # in case it's ManyRelaterManger, not iterable queryset
                except AttributeError:
                    pass
                self.fields[worker_field].initial = workers

    @property
    def workers_fields(self):
        return [self.Meta.model.workers_attr]

    def save(self, request, commit=True):
        object_instance = super(LinkedToWorkersModelForm, self).save(request, commit=False)
        if commit:
            workers = [self.cleaned_data[worker_field] for worker_field in self.workers_fields]
            if len(workers) == 1:
                workers = workers[0]
            self.commit_save(request, object_instance, workers)
        return object_instance

def make_attachment_formset(obj_content_type, *args, **kwargs):
    """
    Dynamically make attachment formset class for attaching files to object of obj_content_type.
    """
    return modelformset_factory(Attachment, fields=('link', 'category', 'description'),
        formfield_callback=ModelFormCustomCallback('category', 'memberships__content_type', obj_content_type).callback)(*args, **kwargs)


class ModelFormCustomCallback(object):
    """
    Use in formsets or modelformsets for dynamic filtering for querysets.
    field_name: name of the Choice (ForeignKey) field in the form (or model).
    filter_attribute: attribute to filter related objects by.
    filter_value: value to filter related objects by.
    """
    def __init__(self, field_name, filter_attribute, filter_value):
        self._field_name = field_name
        self._filter_attribute = filter_attribute
        self._filter_value = filter_value

    def callback(self, field, **kwargs):
        new_field = field.formfield(**kwargs)
        if field.name == self._field_name:
            new_field.queryset = field.related.parent_model.objects.filter(**{self._filter_attribute: self._filter_value})
        return new_field


class CountryChoiceField(Select2ModelField):
    search_fields = ['name__icontains', 'name_ru__icontains']

    def __init__(self, *args, **kwargs):
        super(CountryChoiceField, self).__init__(model=Country, *args, **kwargs)

    def get_title(self, country):
        return country.name_ru or country.name

    def get_description(self, country):
        return country.name if country.name_ru else ""


class MyBaseModelFormSet(BaseInlineFormSet):
    def save(self, request, obj):
        self.instance = obj
        for form in self.forms:
            form.request = request
        return super(MyBaseModelFormSet, self).save()


class ModelChoiceLinkField(forms.ModelChoiceField):
    def __init__(self, link_text=lambda a: u'(открыть в новой вкладке)', *args, **kwargs):
        super(ModelChoiceLinkField, self).__init__(*args, **kwargs)
        self.link_text = link_text

    def label_from_instance(self, obj):
        label = escape(unicode(obj))
        if hasattr(obj, 'get_absolute_url'):
            label += u' <a href="%s" target="_blank">%s</a>' % (obj.get_absolute_url(), self.link_text(obj))
        return mark_safe(label)


class AddAuthorScienceForm(forms.Form):
    def __init__(self, roles, *args, **kwargs):
        super(AddAuthorScienceForm, self).__init__(*args,**kwargs)
        self.fields['role'] = forms.ModelChoiceField(label=u"Выберите роль авторства", queryset=roles, required=True)