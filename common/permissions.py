# -*- coding: utf-8 -*-
__author__ = 'Vladimir'
import authority
from authority import permissions
from common.models import MyModel, LinkedToWorkersModel, WorkershipModel
from django.conf import settings
from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from unified_permissions.models import PermissionLabel, PermissionWithLabel, AllGrantedPermissions
from organizations.utils import is_representative_for, is_representative_for_user, is_representative_for_any_user
from unified_permissions import has_permission
from journals.models import Journal
from conferences.models import Conference
from workers.models import Worker
from icollections.models import Collection
from workers.utils import get_worker


class MyBasePermission(permissions.BasePermission):
    operations_over_superuser = ()

    def preemptive_check(self, operation, obj=None):
        if self.user.is_superuser and operation not in self.operations_over_superuser:
            return True
        if not self.user.is_authenticated():
            return False
        return None

    def get_queryset_kwargs(self, label, obj, **kwargs):
        return {
            "user": self.user,
            "label": label,
            "approved": True
        }

    def unified_check(self, operation, obj=None, **kwargs):
        permission_label = PermissionLabel.objects.get(name=operation)
        queryset_kwargs = self.get_queryset_kwargs(permission_label, obj, **kwargs)
        permission_exists = PermissionWithLabel.objects.filter(**queryset_kwargs).exists()
        if permission_exists:
            return True
        parents = permission_label.get_parents()
        for parent in parents:
            if has_permission(self.user, parent.name, obj): # this runs not only unified_check, but specific checks too
                return True
            if parent.is_role and has_permission(self.user, parent.name):
                # for role parent permissions, check abstract permissions too
                # this can be possibly dangerous, so it's better to check explicit 'abstract' attribute.
                return True
        return False


class AbstractPermission(MyBasePermission):
    operations = (  # (name, verbose_name, is_role), needed only for automatic creation on syncdb; that's why they be overriden in subclasses
        ("edit_permissions_and_roles", u"может редактировать роли и разрешения", False),
        ("is_representative_for", u"ответственный", False),
        ("review_depreports", u"может просматривать годовой отчет", False),
        ("analyze_top_articles", u"может просматривать дополнительные списки топ-статей и сотрудников", False),
        ("is_editor", u"редактор сайта", True),
        ("is_staff", u"персонал сайта", True),
        ("is_librarian", u"сотрудник библиотеки", True),
        ("edit_journals", u"может редактировать журналы", False),
        ("view_all_estimates", u"может просматривать сметы всех проектов", False),
    )

    def get_queryset_kwargs(self, label, obj, **kwargs):
        """
        We allow access via unified_check (specific permissions granted via admin) only for *roles*,
        as an extra security check, because otherwise, if a user has some abstract permission to *any* object,
        he would get this permission to *every* object, because unified_check checks that permission exists for any object.
        Non-role abstract permissions still exist and can be checked, but only via custom function-checks
        (or via skip_check_is_role parameter).
        To overcome this, there are several options:
            - rethink the logic and replace abstract operation with a non-abstract one applied to e.g. Organization
            (it's not a good idea to make abstract operations which are really limited to organization scope)
            - add abstract operation with role=True ('operations' class attribute);
            - write custom function-check which calls unified_check(skip_check_is_role=True), e.g. see self.check_confirm_projects.
        """
        queryset_kwargs = super(AbstractPermission, self).get_queryset_kwargs(label, obj, **kwargs)
        if not kwargs.get('skip_check_is_role', False):
            queryset_kwargs['label__is_role'] = True
        return queryset_kwargs

    # It is important that these methods don't take obj as an argument, so that they must be called without an argument
    def check_edit_permissions_and_roles(self):
        representatives = self.user.representatives.filter(is_active=True)
        return representatives.filter(department__isnull=False).exists() \
            or any(has_permission(self.user, 'edit_permissions_and_roles', rep.organization)
                   for rep in representatives.filter(department__isnull=True))

    def check_is_representative_for(self):
        return self.user.representatives.filter(is_active=True).exists()

    def check_confirm_projects(self):
        """This operation is a special case (I don't know why). Is_role is not verified. See usage in depadmin/views/menu.py."""
        return self.unified_check("confirm_projects", skip_check_is_role=True)

    def check_is_staff(self):
        return self.user.is_staff

    def check_view_all_estimates(self):
        return AllGrantedPermissions.objects.current.filter(user=self.user, role_name='edit_all_estimates').exists() \
            or has_permission(self.user, 'is_representative_for')



class MyModelPermission(MyBasePermission):
    label = 'common_permission'
    # do not include in @checks checks that can return False for superuser
    # (e.g. preemptive check); authority always returns True for him
    # so I decided to drop checks attribute altogether
    operations_over_superuser = (
        'view_locked_message', 'delete_if_no_related_objects', 'link_to', 'unauthor'
    )
    operations = (  # (name, verbose_name, is_role)
        ("view", u"может просматривать", False),
        ("edit", u"может редактировать", False),
        ("view_locked_message", u"может просматривать сообщение о блокировке", False),
        ("delete", u"может удалять", False),
        ("delete_if_no_related_objects", u"может удалять объекты без связей", False),
        ("link_to", u"может привязаться к объекту", False),
        ("unauthor", u"может отказаться от объекта", False),
        ("sign", u"может подписать объект", False),
        ("view_pdf", u"может просматривать pdf-версию", False),
        ("edit_status_tags", u"может редактировать статусы проверки", False), # Status tags mean not inner tags, but tags set by managers (like confirmation status).
        ("manages_creator", u"ответственный за создателя объекта", False),
        ("manages_linked_user", u"ответственный за сотрудника, привязанного к объекту", False),
        ("view_object_history", u"может просматривать историю действий с объектом", False),
        ("is_creator", u"создатель объекта", False),
        ("is_linked", u"привязан к объекту", False),
    )

    def get_queryset_kwargs(self, label, obj, **kwargs):
        queryset_kwargs = super(MyModelPermission, self).get_queryset_kwargs(label, obj, **kwargs)
        queryset_kwargs['content_type'] = ContentType.objects.get_for_model(obj)
        queryset_kwargs['object_id'] = obj.id
        return queryset_kwargs

    def check_is_representative_for(self, obj):
        return is_representative_for(self.user, obj)

    def check_edit(self, obj):
        """Checks additional info required to edit the object."""
        return obj.id and (
            has_permission(self.user, "is_editor")
            or obj.is_creator_managed_by(self.user)
            or obj.is_linked_managed_by(self.user)
            or ((obj.is_created_by(self.user) or obj.is_linked_to(self.user)) and not obj.is_locked)
        )

    def check_view_locked_message(self, obj):
        """Returns True if the object is locked for user, and otherwise he could have edited it."""
        return obj.id and ((obj.is_created_by(self.user) or obj.is_linked_to(self.user)) and obj.is_locked)

    def check_delete(self, obj):
        return obj.id and (
            has_permission(self.user, "is_editor")
            or (
                (
                    obj.is_created_by(self.user)
                    or obj.is_creator_managed_by(self.user)
                    or obj.is_probably_created_by(self.user)
                    or obj.is_probably_creator_managed_by(self.user)
                )
                and not obj.is_locked
            )
        )

    def check_delete_if_no_related_objects(self, obj):
        return obj.__class__ in [Journal, Conference, Collection, Worker] \
            and not obj.has_related_objects

    def check_link_to(self, obj):
        '''Returns True if the object is not linked to the user,
        but it can be linked to the user,
        i.e. there is a workership that is not linked to any user,
        and the workership is "similar" to the user's worker.'''
        worker = get_worker(self.user)
        if obj.is_linked_to(self.user):
            return False
        return bool(obj.get_similar_workership_to_worker(worker))

    def check_unauthor(self, obj):
        '''MyModel instances cannot be unauthored. Proper unauthor checks
        are implemented for LinkedToWorkersModel and WorkershipModel objects.'''
        return False

    def check_sign(self, obj):
        return not getattr(obj, "signed", False) and has_permission(self.user, "edit", obj)

    def check_view_pdf(self, obj):
        return getattr(obj, "signed", True) and has_permission(self.user, "view", obj)

    def check_manages_creator(self, obj):
        '''Returns True if the user is a manager (e.g. moderator, representative)
        of the creator of the object.'''
        return hasattr(obj, 'creator') and is_representative_for_user(self.user, obj.creator)

    def check_manages_linked_user(self, obj):
        '''Returns True if the user is a manager (e.g. moderator, representative)
        of one of the linked users to the object.'''
        return is_representative_for_any_user(self.user, obj.linked_users)

    def check_is_creator(self, obj):
        '''Returns True if the user is the creator of the object.'''
        return hasattr(obj, 'creator') and self.user == obj.creator

    def check_is_linked(self, obj):
        '''Returns True if the user is linked to the object,
        i.e. user is linked to the worker linked to the object.
        User is one of the linked workers or user is *the* linked worker.'''
        return self.user in obj.linked_users


class LinkedToWorkersModelPermission(MyModelPermission):
    label = 'linked_to_workers_model_permission'

    def check_unauthor(self, obj):
        if not obj.is_linked_to(self.user):
            return False

        try:
            obj.get_workership_model()._meta.get_field_by_name('original_name')
        except:
            return False

        return True


class WorkershipModelPermission(MyModelPermission):
    label = 'workership_model_permission'

    def check_unauthor(self, obj):
        return obj.is_linked_to(self.user) and hasattr(obj, 'original_name')


authority.register(MyModel, MyModelPermission)
authority.register(LinkedToWorkersModel, LinkedToWorkersModelPermission)
authority.register(WorkershipModel, WorkershipModelPermission)
