# -*- coding: utf-8; -*-

#
# Generic editor of tree-organized models
# Usage:
#
#  Define three URLs all pointing to tree_data_editor:
#    1. url with no template parameters (used as a start page)
#    2. url with one integer argument (current intem)
#    3. url with two params: item_id and operation
#  All urls should be defined ad follows:
#
#  url(r'^some_url/(?P<item_id>\d+)/(?P<operation>\w+)/$',
#    tree_data_editor,
#    {
#      'model': EquipmentType,          # tree model instance (what we are editing)
#      'field_name_to_set': 'name',     # what field is 'the name'; for records editing and creation
#      'view_name': 'equipment_types',  # name of the url which this function is attached to; used to generate edit/delete links on html page
#      'page_title': u'any text',       # title of editor's page
#      'go_back_url': '/equipment/'     # if present, this link is presented on editor's page
#      'go_back_label': 'some text'     # text of the link
#      'check_permitions': function     # user->bool, if returns false, no edit/delete are posiible
#    },
#    name='equipment_types'),
#
#

from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.db import connection
from django.db import models

# http://stackoverflow.com/questions/2608067/how-to-find-out-whether-a-models-column-is-a-foreign-key
def get_fk_model(model, fieldname):
    '''returns None if if fieldname is not a foreign key, otherswise the relevant model.'''
    field_object, model, direct, m2m = model._meta.get_field_by_name(fieldname)
    if not m2m and direct and isinstance(field_object, models.ForeignKey):
        return field_object.rel.to
    return None

def find_self_reference(model, return_field_name=False):
    '''Returns database column name of the first found self-reerence field of the model `model`, or None.
    If return_field_name=True, the field name is returned.
    '''
    for field in model._meta.fields:
        column_name = field.get_attname_column()[1]
        if model == get_fk_model(model, field.name):
            if return_field_name:
                return field.name
            else:
                return column_name
    return None

def find_primary_key(model):
    '''Returns database column name of the primary key column.'''
    return model._meta.pk.get_attname_column()[1]


@login_required
def tree_data_editor(request,
                     view_name=None,
                     model=None, field_name_to_set=None,
                     page_title=None, go_back_url=None, go_back_label=u'Закончить редактирование',
                     item_id=None, operation=None,
                     check_permitions=None):
    item = None

    if check_permitions and not check_permitions(request.user):
        messages.warning(request, u'У Вас нет прав для редактирования этих данных.')

    show_delete_confirmation = False
    if operation == 'edit' or operation == 'delete' or operation == 'delete_confirmed':
        item = get_object_or_404(model, pk=item_id)
        if operation == 'delete_confirmed':
            item.delete()
            messages.info(request, u'Запись удалена.')
            item = None
            item_id = None
            return redirect(view_name)
        elif operation == 'delete':
            show_delete_confirmation = True

    # process form data (create new or save changes to existing record)
    if request.method == 'POST' and len(request.POST.get('new_item_value', ''))>0:
        item_to_edit = None
        parent_item_id = request.POST.get('parent_item_id', None)
        can_commit = True
        if request.POST.get('item_id', None):
            item_to_edit_id = int(request.POST.get('item_id'))
            item_to_edit = get_object_or_404(model, pk=item_to_edit_id)
        else:
            item_to_edit = model()
            if parent_item_id:
                try:
                    selfreference_fieldname = find_self_reference(model, return_field_name=True)
                    parent_item = get_object_or_404(model, pk=int(parent_item_id))
                    setattr(item_to_edit, selfreference_fieldname, parent_item)
                except ValueError:
                    messages.error(request, u'Не удалось установить ссылку на вышестоящую запись.')
                    can_commit = False
        setattr(item_to_edit, field_name_to_set, request.POST.get('new_item_value'))
        if can_commit:
            item_to_edit.save()
        if parent_item_id:
            return redirect(view_name, parent_item_id)
        return redirect(view_name)

    query_inline_params = {
        'pk_column': find_primary_key(model),
        'parent_column': find_self_reference(model),
        'table_name': model._meta.db_table,
        'percent_s': '%s'
        }

    toplevel_sql = '''
    select %(pk_column)s
    from %(table_name)s
    where %(parent_column)s is null
    ''' % query_inline_params

    parent_items_sql = '''
    select %(pk_column)s, %(parent_column)s
    from %(table_name)s
    connect by prior %(parent_column)s = %(pk_column)s
    start with %(pk_column)s = %(percent_s)s
    ''' % query_inline_params

    children_items_sql = '''
    select %(pk_column)s, %(parent_column)s
    from %(table_name)s
    where %(parent_column)s = %(percent_s)s
    ''' % query_inline_params

    split_at_sql = '''
    select %(pk_column)s
    from (
        select %(pk_column)s, %(parent_column)s
        from %(table_name)s
        connect by prior %(parent_column)s = %(pk_column)s
        start with %(pk_column)s = %(percent_s)s
    )
    where %(parent_column)s is null
    '''  % query_inline_params

    toplevel = model.objects.raw(toplevel_sql)

    parent_items = None
    split_toplevel_at = None
    children_items = None
    children_offset = ''
    if item_id:
        parent_items = model.objects.raw(parent_items_sql, [item_id])
        children_items = model.objects.raw(children_items_sql, [item_id])

        cursor = connection.cursor()
        cursor.execute(split_at_sql, [item_id])
        split_toplevel_at = cursor.fetchone()[0]
        cursor.close()

        # FIXME: find a better way to layout tree structure in HTML template
        offsets = ['']
        level_offset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
        for x in parent_items:
            offsets.insert(0, offsets[0] + level_offset)
        offsets = offsets[1:]
        children_offset = offsets[0] + level_offset
        parent_items = [{'object': x[0], 'offset': x[1]} for x in zip(parent_items, offsets)]

    return render(request, "common/tree_data_editor.html",
                  {'item': item,
                   'highlighted_item_id': item_id,
                   'toplevelitems': toplevel,
                   'split_toplevel_at': split_toplevel_at,
                   'children': children_items,
                   'parents': parent_items,
                   'children_offset': children_offset,
                   'view_name': view_name,
                   'page_title': page_title,
                   'go_back_url': go_back_url,
                   'go_back_label': go_back_label,
                   'show_delete_confirmation': show_delete_confirmation,
                   })
