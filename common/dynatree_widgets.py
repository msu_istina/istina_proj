# -*- coding: utf-8; -*-
##
## Originally taken from https://github.com/zen4ever/django-dynatree
## Supports static (DynatreeWidget) or ajax-enabled lazy (DynatreeLazyWidget) trees.
## Static tree may be initialized with queryset parameter, or by a ajax call. Lazy
## mode assumes that ajax call is made each time a user opens new layer in the tree.
##
## Widget constructor accepts following arguments:
## choices: queryset that used by Django for user's input validation
## queryset: used for tree initialization
## select_mode: 1-single choice, 2-multiple choices, 3-hierarhical selection
## ajax_url: the url to be called by ajax-code (see below)
## ajax_extra_data: dictionary of additional params passed with each ajax call
##
## If choices is not givien and queryset is, possible choices are
## inherited from the queryset. In case of any ajax communication, valid choices
## should be explicitly provided to widget constructor.
##
## DynatreeWidget usage:
##
## in form's __init__:
##     from common.dynatree_widgets import DynatreeWidget
##     field.widget = DynatreeWidget(queryset=Model.objects.all(), select_mode=1)
##
## and put in html template
##     {% block extrahead %}
##         {{ form.media }}
##     {% endblock %}
##
## DynatreeLazyWidget or DynatreeWidget with ajax-initialization usage:
##   urls.py
##     from common.dynatree_widgets import dynatree_widget_helper
##     url(r'^path/$', dynatree_widget_helper, {'model': model_instance}, name='some_name_for_helper_url'),
##  forms.py
##     from django.core.urlresolvers import reverse
##     field.widget = DynatreeLazyWidget(ajax_url=reverse('some_name_for_helper_url'),
##                                       ajax_extra_data={"key": "val"}, select_mode=1,
##                                       choices=model.objects.all())
##  Ajax url should accept GET requests with two parameters (extra_data is passed as is):
##  1) "operation": possible values are "get_tree", "get_roots", "get_children" and "get_path_to_root"
##  2) "node_id": current record's key for "get_children" and "get_path_to_root"
##  "get_roots" should return a string of the form '/12/17/23' (12 is the pk of top-level item, 23==node_id is the leaf).
##  All other operations generate Dynatree-compatible json. For example:
##     [{"title": "A title", "key": "123", "isFolder": True, "isLazy": False,
##       "children": [{"title": "Child", "key": "124", "isFolder": False, "isLazy": False}]
##      },
##      {"title": "Another toplevel", "key": "188", "isFolder": False, "isLazy": False}]
##
##

from itertools import chain

from django import forms
from django.conf import settings
from django.forms.widgets import SelectMultiple, Select
from django.utils.encoding import force_unicode
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.utils import simplejson

from django.http import HttpResponse
from django.db import connection

from tree_data_editor import get_fk_model, find_primary_key, find_self_reference

DT_SELECT_ONE=1
DT_SELECT_MULTI=2
DT_SELECT_HIER=3

def get_doc(node, values):
    if hasattr(node, "get_doc"):
        return node.get_doc(values)
    if hasattr(node, "id"):
        pk = node.id
    else:
        pk = node.pk
    if hasattr(node, "name"):
        name = node.name
    else:
        name = unicode(node)
    url = ' '
    if hasattr(node, "url"):
        url = node.url
    doc = {"title": name, "key": pk, "url": url}
    if str(pk) in values:
        doc['select'] = True
        doc['expand'] = True
    return doc

def get_tree(nodes, values):
    parent = {}
    parent_level = 0
    stack = []
    results = []

    def find_parent(child, results):
        for node in reversed(results):
            if not hasattr(node, 'url') or child.url.startswith(node["url"]):
                if child.parent_id != node["key"]:
                    return find_parent(child, node["children"])
                else:
                    return node

    def add_doc(node):
        if node.level == 1:
            results.append(get_doc(node, values))
        elif node.level >= 2:
            parent = find_parent(node, results)
            children = parent.get("children", [])
            child = get_doc(node, values)
            if child.get('select', False):
                parent['expand'] = True
            children.append(child)
            parent["children"] = children

    if nodes:
        for node in nodes:
            add_doc(node)

    return results


class DynatreeWidgetBase(SelectMultiple):
    class Media:
        css = {
            'all': ('base/css/dynatree/ui.dynatree.css',)
        }
        js = ('base/js/dynatree/jquery/jquery.js',
              'base/js/dynatree/jquery/jquery-ui.custom.js',
              'base/js/dynatree/jquery/jquery.cookie.js',
              'base/js/dynatree/jquery.dynatree.js',
              'base/js/dynatree_widgets.js',
              )

    def __init__(self, attrs=None, choices=(), queryset=None, initial_sql=None,
                 select_mode=DT_SELECT_MULTI,
                 ajax_url=None, ajax_extra_data=None,
                 collapsable_tree=None):
        if not choices and queryset:
            choices = tuple([tuple([x.id, x]) for x in queryset])
        super(DynatreeWidgetBase, self).__init__(attrs, choices)
        self.queryset = queryset
        self.initial_sql = initial_sql
        self.use_xdr_init = False if (queryset or initial_sql or not ajax_url) else True
        self.select_mode = select_mode
        self.selection_field = None
        self.ajax_url = ajax_url
        self.ajax_extra_data = ajax_extra_data
        self.collapsable_tree = collapsable_tree

    def value_from_datadict(self, data, files, name):
        # Decodes js-encoded comma-separeted list of selected keys
        result_multi = super(DynatreeWidgetBase, self).value_from_datadict(data, files, name)
        js_encoded = None
        if result_multi:
            js_encoded = result_multi[0]
        results = js_encoded.split(',') if js_encoded else []
        if self.select_mode == DT_SELECT_ONE and len(results)>=1:
            return results[0]
        return results

    def _generate_dynatree_js_init_code(self, name, value, attrs=None):
        return u''

    def _generate_show_collapse_buttons_init(self, name, value, attrs=None):
        return [u'''
        $(function() {
            $("#btnShowChecked_%(id)s").click(function(){
               return dynatree_widgets.showChecked("#%(id)s");
            });

            $("#btnCollapseAll_%(id)s").click(function(){
                return dynatree_widgets.collapseAll("#%(id)s");
            });

            $("#dynatree_change_button_%(id)s").click(function(){
               document.getElementById('divDynatreeContainer_%(id)s').style.display = "block";
            });

            $("#btnHideTree_%(id)s").click(function(){
               document.getElementById('divDynatreeContainer_%(id)s').style.display = "none";
            });
        });
        ''' % attrs]

    def render(self, name, value, attrs=None, choices=()):
        if value is None:
            value = []
        elif isinstance(value, list):
            pass
        else:
            value = [value]
        has_id = attrs and 'id' in attrs
        output = []
        output.append(u'''<span id="dynatree_widget_%(id)s">''' % attrs)
        if self.collapsable_tree:
            output.append(u'''        
            <span id="dynatree_selection_message_%(id)s">Подождите, пожалуйста...</span>
            <a id="dynatree_change_button_%(id)s"> Изменить</a>
            ''' % attrs)
        output.append(u'''
        <div id="divDynatreeContainer_%(id)s" style="display:%(display_mode)s;">
        ''' % {'id': attrs['id'],
               'display_mode': "none" if self.collapsable_tree else "block"})
        output.append(u'''
            <a id="btnCollapseAll_%(id)s">Свернуть все</a>
            &nbsp;&nbsp;
            <a id="btnShowChecked_%(id)s">Показать выбранные</a>
        ''' % {'id': attrs['id']})
        if self.collapsable_tree:
            output.append(u'''
            &nbsp;&nbsp;
            <a id="btnHideTree_%(id)s">Скрыть дерево</a>            
            ''' % {'id': attrs['id']})
        if has_id:
            output.append(u'<div id="dynatree_container_%(id)s" class="dynatree_container">' % attrs)
            output.append(u'<div id="%(id)s"></div>' % attrs)
            output.append(u'</div>')
        self.selection_field = '%(id)s_checkboxes' % attrs

        # close dynatree container div and widget's span
        output.append(u'''
        </div>
        </span>
        ''')

        # Output marked checkboxes holder field; keys are stored as a comma separeated list
        encoded_value = ",".join(map(str, value))
        output.append(u'<input type="hidden" name="%s" id="%s_checkboxes" value="%s"/>' % (name, attrs['id'], encoded_value))

        output.append(u'<script type="text/javascript">')
        output.extend(self._generate_dynatree_js_init_code(name, value, attrs))
        output.extend(self._generate_show_collapse_buttons_init(name, value, attrs))
        output.append(u'</script>')
        return mark_safe(u'\n'.join(output))


class DynatreeWidget(DynatreeWidgetBase):
    def _generate_dynatree_js_init_code(self, name, value, attrs):
        str_values = set([force_unicode(v) for v in [value,]])
        output = []
        if not self.use_xdr_init:
            if self.initial_sql:
                cursor = connection.cursor()
                cursor.execute(self.initial_sql)
                tree_data = construct_tree_from_cursor(cursor)
                cursor.close()
            else:
                tree_data = get_tree(self.queryset, str_values)
            output.append(u'var dynatree_data_%s = %s;' % (
                attrs['id'],
                simplejson.dumps(tree_data)
                ))
        output.append(
        u"""
        $(function() {
            var collapsable_tree = %(collapsable_tree)s;
            function get_selected_titles(selNodes) {
                var selTitles = $.map(selNodes, function(node){
                    return node.data.title
                });
                var titles = selTitles.join("; ");
                if(titles.length == 0) {
                    titles = 'Значение не выбрано';
                }
                return titles;
            }

            function update_encoded_choices(tree) {
                // Compose a comma separeted list of all selected keys
                var selNodes = tree.getSelectedNodes();
                var selKeys = $.map(selNodes, function(node){
                    return node.data.key
                });
                var keys=selKeys.join(",");
                $("#%(selection_field)s").val(keys);
            }
            
            $("#%(id)s").dynatree({
                checkbox: true,
                selectMode: %(select_mode)d,
                children: %(children_var)s,
                initAjax: {
                    url: "%(ajax_url)s",
                    data: $.extend({"operation": "get_tree"},
                                    %(ajax_extra_data)s)
                    },
                debugLevel: %(debug)d,
                onSelect: function(select, node) {
                    // Compose a comma separeted list of all selected keys
                    update_encoded_choices(node.tree);

                    // Hide collapsable dynatree
                    if(collapsable_tree) {
                        document.getElementById('divDynatreeContainer_%(id)s').style.display = "none";
                        
                        // Show selected values in visible field
                        titles = get_selected_titles(node.tree.getSelectedNodes());
                        $("#dynatree_selection_message_%(id)s").text(titles);
                    }
                },
                onClick: function(node, event) {
                    if( node.getEventTargetType(event) == "title" )
                        node.toggleSelect();
                },
                onKeydown: function(node, event) {
                    if( event.which == 32 ) {
                        node.toggleSelect();
                        return false;
                    }
                },
                onPostInit: function(isReloading, isError) {
                    var t = $("#%(id)s").dynatree("getTree");
                    var ids = $("#%(selection_field)s").val().split(",");
                    for(var i=0; i < ids.length; i++) if(ids[i].length > 0) {
                         var node = t.getNodeByKey(ids[i]);
                         if(node) {
                             node.select();
                             node.makeVisible();
                         }
                     }
                     titles = get_selected_titles(t.getSelectedNodes());
                     $("#dynatree_selection_message_%(id)s").text(titles);
                     update_encoded_choices(t);
                },
                // The following options are only required, if we have more than one tree on one page:
                cookieId: "dynatree-%(id)s",
                idPrefix: "dynatree-%(id)s-"
            });
        });
        """ % {
            'id': attrs['id'],
            'children_var': 'null' if self.use_xdr_init else ('dynatree_data_' + attrs['id']),
            'debug': 1 if settings.DEBUG else 0,
            'collapsable_tree': 'true' if self.collapsable_tree else 'false',
            'select_mode': self.select_mode,
            'selection_field': self.selection_field,
            'ajax_url': self.ajax_url,
            'ajax_extra_data': simplejson.dumps(self.ajax_extra_data) if self.ajax_extra_data else '{}'
            }
        );
        return output


class DynatreeLazyWidget(DynatreeWidgetBase):
    def _generate_dynatree_js_init_code(self, name, value, attrs):
        output = []
        output.append(u"""
            $(function() {
                $("#%(id)s").dynatree({
                    checkbox: true,
                    selectMode: %(select_mode)d,
                    debugLevel: %(debug)d,
                    fx: { height: "toggle", duration: 200 },
                    initAjax: {
                        url: "%(ajax_url)s",
                        data: $.extend({"operation": "get_roots"},
                                       %(ajax_extra_data)s) // data passed to the query
                    },
                    onSelect: function(select, node) {
                        // Compose a comma separeted list of all selected keys
                        var selNodes = node.tree.getSelectedNodes();
                        var selKeys = $.map(selNodes, function(node){
                            return node.data.key
                        });
                        var keys=selKeys.join(",");
                        $("#%(selection_field)s").val(keys);
                    },
                    onClick: function(node, event) {
                        if( node.getEventTargetType(event) == "title" )
                            node.toggleSelect();
                    },
                    onKeydown: function(node, event) {
                        if( event.which == 32 ) {
                            node.toggleSelect();
                            return false;
                        }
                    },
                    onLazyRead: function(node) {
                        // Load next level via jquery call
                        node.appendAjax({
                            url: "%(ajax_url)s",
                            data: $.extend({"node_id": node.data.key,
                                            "operation": "get_children"},
                                           %(ajax_extra_data)s)
                        });
                    },
                    onPostInit: function(isReloading, isError) {
                        // In lazy mode we should load required branches
                        var t = $("#%(id)s").dynatree("getTree");
                        var ids = $("#%(selection_field)s").val().split(",");
                        for(var i=0; i < ids.length; i++) if(ids[i].length > 0) {
                           $.get("%(ajax_url)s",
                                 $.extend(
                                     {
                                         "operation": "get_path_to_root",
                                         "node_id": ids[i]
                                     },
                                     %(ajax_extra_data)s),
                                 function(data) {
                                     var dataAsJson=jQuery.parseJSON(data);
                                     t.loadKeyPath(dataAsJson, function(node, status) {
                                         if(status == "loaded") {
                                             // 'node' is a parent that was just traversed.
                                             // If we call expand() here, then all nodes will be expanded
                                             // as we go
                                             node.expand();
                                         } else if(status == "ok") {
                                             // 'node' is the end node of our path.
                                             // If we call activate() or makeVisible() here, then the
                                             // whole branch will be exoanded now
                                             node.activate();
                                             node.toggleSelect();
                                         } else if(status == "notfound") {
                                             var seg = arguments[2],
                                                 isEndNode = arguments[3];
                                         }
                                     });
                                 }
                             );
                             var node = t.getNodeByKey(ids[i]);
                             if(node) {
                                 node.select();
                                 node.makeVisible();
                             }
                         }
                    },
                    // The following options are only required, if we have more than one tree on one page:
                    cookieId: "dynatree-%(id)s",
                    idPrefix: "dynatree-%(id)s-"
                });
            });
            """ % {
                'id': attrs['id'],
                'debug': 1 if settings.DEBUG else 0,
                'select_mode': self.select_mode,
                'selection_field': self.selection_field,
                'ajax_url': self.ajax_url,
                'ajax_extra_data': simplejson.dumps(self.ajax_extra_data) if self.ajax_extra_data else '{}'
                })
        return output


def construct_tree_from_cursor(cursor):
    '''Accepts open cursor that each time returns five items: id, title, parent_key, level, and path from root.
    path is a / separeted list of keys.
    Returns json representation accepted by jquery.dynatree.'''
    prev = None
    stack = [] # stack contains pairs (m, i), where m is a list of completed descriptions
               # of all nodes at the same level and i is the last item that should be
               # appended to m when its children field will be constructed.
    message_data = list()
    # add one dummy top-level record to force popping data from the stack at the end
    for key, title, parent, level, url in chain(cursor, [(0, 'dummy', 0, 1, '/')]):
        if prev:
            if level == prev["level"]:
                message_data.append({"title": prev['title'], "key": prev['key'], "isFolder": False, "isLazy": False})
            elif level > prev["level"]:
                item = {"title": prev['title'], "key": prev['key'], "isFolder": True, "unselectable": False, "isLazy": False}
                stack.append((message_data, item))
                message_data = []
            elif level < prev["level"]:
                message_data.append({"title": prev['title'], "key": prev['key'], "isFolder": False, "isLazy": False})
                while level < prev["level"] and stack:
                    last_list, last_item = stack.pop()
                    last_item["children"] = message_data
                    message_data = last_list
                    message_data.append(last_item)
                    prev["level"] = prev["level"] - 1
        prev = {"key": str(key), "title": title, "level": level}
    return message_data

def dynatree_widget_helper(request, model, pk_column=None, parent_column=None,
                           title_fieldname=None,
                           toplevel_qs=None, toplevel_sqlgen=None):
    operation = request.GET.get('operation', '')
    node_id = request.GET.get('node_id', None)
    try:
        node_id = int(node_id)
    except:
        node_id = None

    if title_fieldname:
        title_field, field_model, direct, m2m = model._meta.get_field_by_name(title_fieldname)
        title_column = title_field.get_attname_column()[1]
    else:
        title_column = 'f_' + model._meta.db_table + '_name'

    parent_column = parent_column or find_self_reference(model)
    pk_column = pk_column or find_primary_key(model)

    toplevel_qs_sql = ' (select %s from %s where %s is null) ' % (pk_column, model._meta.db_table, parent_column)
    if toplevel_sqlgen:
        toplevel_qs_sql = ' (' + toplevel_sqlgen(request) + ') '
    elif toplevel_qs:
        toplevel_qs_sql = ' (select %s from (%s))' % (pk_column, toplevel_qs.query)

    query_inline_params = {
        'pk_column': pk_column or find_primary_key(model),
        'parent_column': parent_column or find_self_reference(model),
        'table_name': model._meta.db_table,
        'title_column': title_column,
        'percent_s': '%s',
        'node_id': '%s',
        'toplevel_qs_sql': toplevel_qs_sql,
        }

    if operation == 'get_path_to_root':
        sql = '''
        select SYS_CONNECT_BY_PATH(%(pk_column)s, '/') as path
        from %(table_name)s
        where %(pk_column)s in %(toplevel_qs_sql)s
        start with %(pk_column)s = %(node_id)s
        connect by %(pk_column)s = prior %(parent_column)s
        UNION ALL
        select '/' from dual
        ''' % query_inline_params
        cursor = connection.cursor()
        cursor.execute(sql, [node_id])
        message_data = cursor.fetchone()[0]
        cursor.close()
        # reverse the order of selected keys from top-level to leaf
        message_data = '/' + '/'.join(reversed(message_data[1:].split('/')))
        return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')
    elif operation in ['get_children', 'get_roots']:
        if operation == 'get_roots':
            node_id = None
        sql = '''
        select %(pk_column)s, %(title_column)s
        from %(table_name)s
        where %(parent_column)s = %(node_id)s OR (%(node_id)s is null and %(parent_column)s is null)
        '''  % query_inline_params
        cursor = connection.cursor()
        cursor.execute(sql, [node_id, node_id])
        message_data = []
        for pk, title in cursor:
            message_data.append( {"title": title, "key": str(pk), "isFolder": True, "isLazy": True} )
        cursor.close()
        return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')
    elif operation == 'get_tree':
        sql = '''
        select %(pk_column)s, %(title_column)s, %(parent_column)s, level, SYS_CONNECT_BY_PATH(%(pk_column)s, '/') as url
        from %(table_name)s
        start with %(pk_column)s in %(toplevel_qs_sql)s
        connect by prior %(pk_column)s = %(parent_column)s
        '''  % query_inline_params
        cursor = connection.cursor()
        cursor.execute(sql)
        message_data = construct_tree_from_cursor(cursor)
        cursor.close()
        return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')

    return HttpResponse(simplejson.dumps(''), content_type='application/javascript')
