# -*- coding: utf-8 -*-
from django import template
from django.core.cache import cache

from common.utils.activities import get_activities_last_added

register = template.Library()

CACHE_KEY = "startpage_last_activities"
CACHE_EXPIRE = 120


def recalculate_startpage_last_activities():
    t = template.loader.get_template("common/activities_last_added.html")
    context = dict(activities=get_activities_last_added(limit=10), compact=1)
    html = t.render(template.context.Context(context))
    cache.set(CACHE_KEY, html, timeout=CACHE_EXPIRE)
    return html


@register.simple_tag
def startpage_last_activities():
    return cache.get(CACHE_KEY) or recalculate_startpage_last_activities()
