import logging
import hashlib

from django.templatetags.cache import CacheNode
from django.template import Library, Node, TemplateSyntaxError, Variable, VariableDoesNotExist
from django.template import resolve_variable
from django.core.cache import get_cache
from django.utils.http import urlquote

register = Library()

logger = logging.getLogger("my_cache")

default_cache = get_cache("default")
bigdata_cache = get_cache("bigdata")


class MyCacheNode(CacheNode):
    def __init__(self, nodelist, cache_disabled, expire_time_var, fragment_name, vary_on):
        self.cache_disabled = Variable(cache_disabled)
        CacheNode.__init__(self, nodelist, expire_time_var, fragment_name, vary_on)

    def render(self, context):
        try:
            self.cache_disabled.resolve(context)
        except VariableDoesNotExist:
            try:
                return self.render_choose_cache(context)
            except TemplateSyntaxError:
                raise
            except:
                logger.warning("Cache FAIL: %s" % self.vary_on, exc_info=True)
                return self.nodelist.render(context)
        else:
            return self.nodelist.render(context)

    def render_choose_cache(self, context):
        '''The same function as parent render, but added cache argument for specifying non-default cache.'''
        try:
            expire_time = self.expire_time_var.resolve(context)
        except VariableDoesNotExist:
            raise TemplateSyntaxError('"cache" tag got an unknown variable: %r' % self.expire_time_var.var)
        try:
            expire_time = int(expire_time)
        except (ValueError, TypeError):
            raise TemplateSyntaxError('"cache" tag got a non-integer timeout value: %r' % expire_time)
        # Build a unicode key for this fragment and all vary-on's.
        args = hashlib.md5(u':'.join([urlquote(resolve_variable(var, context)) for var in self.vary_on]))
        cache_key = 'template.cache.%s.%s' % (self.fragment_name, args.hexdigest())
        value = default_cache.get(cache_key)
        if value is None:
            logger.warning("MISS (default): %s" % self.vary_on)
            value = bigdata_cache.get(cache_key)
            if value is None:
                logger.warning("MISS (bigdata): %s" % self.vary_on)
                value = self.nodelist.render(context)
                try:
                    default_cache.set(cache_key, value, expire_time)
                except:
                    bigdata_cache.set(cache_key, value, expire_time)
            else:
                logger.warning("HIT (bigdata): %s" % self.vary_on)
        else:
            logger.warning("HIT (default): %s" % self.vary_on)
        return value



@register.tag('my_cache')
def do_cache(parser, token):
    nodelist = parser.parse(('endcache',))
    parser.delete_first_token()
    tokens = token.contents.split()
    if len(tokens) < 4:
        raise TemplateSyntaxError(u"'%r' tag requires at least 3 arguments." % tokens[0])
    return MyCacheNode(nodelist, tokens[1], tokens[2], tokens[3], tokens[4:])
