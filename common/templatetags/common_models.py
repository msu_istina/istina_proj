# -*- coding: utf-8 -*-
from django import template
register = template.Library()

@register.simple_tag
def get_field_verbose_name(instance, field_name):
    """
    Returns verbose_name for a field.
    Source: http://stackoverflow.com/a/14498938/304209.
    """
    return instance._meta.get_field(field_name).verbose_name.capitalize()