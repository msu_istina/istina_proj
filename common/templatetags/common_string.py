# -*- coding: utf-8 -*-
from django import template
from django.template.defaultfilters import stringfilter

from common.utils.strings import line_length, replace_by_list

register = template.Library()


@register.filter
@stringfilter
def replace(value, arg):
    "Replaces all occurences of first symbol in arg to the second symbol."
    if len(arg) != 2:
        return value
    return value.replace(arg[0], arg[1])

@register.filter
@stringfilter
def replace_comma_sep(value, arg):
    "arg: 'str1,str2'. Replaces str1 with str2."
    old, new = arg.split(',')
    return value.replace(old, new)


@register.filter
@stringfilter
def nbsp_space(value):
    "Replaces all occurences spaces to &nbsp;"
    return value.replace(" ", "&nbsp;")


@register.filter(name='line_length')
@stringfilter
def line_length_filter(text, delimiter='\n'):
    "Return number of lines in text."
    return  line_length(text, delimiter)


@register.filter
@stringfilter
def to_string(value):
    "Return the value coerced to string."
    # it has been already coerced due to @stringfilter
    return value


@register.filter
@stringfilter
def startswith(value, prefix):
    "Returns True if the value coerced to string starts with prefix."
    return value.startswith(prefix)


@register.filter
@stringfilter
def escapelatex(value):
    conversion_list = [ # see http://stackoverflow.com/questions/2627135/how-do-i-sanitize-latex-input and http://stackoverflow.com/a/5422751/304209
        ('\\', "\\textbackslash"),
        ('{', "\\{"),
        ('}', "\\}"),
        ('\\textbackslash', "\\textbackslash{}"), # fix braces in #1 (or they will be badly replaced)
        ('$', "\\$"),
        ('&', "\\&"),
        ('#', "\\#"),
        ('^', "\\textasciicircum{}"), # (requires the textcomp package)
        ('_', "\\_"),
        ('~', "\\textasciitilde{}"),
        ('%', "\\%"),
        ('<', "\\textless{}"),
        ('>', "\\textgreater{}"),
        ('|', "\\textbar{}"),
        ('"', "\\textquotedbl{}"),
        ("'", "\\textquotesingle{}"),
        ('`', "\\textasciigrave{}")
    ]
    return replace_by_list(value, conversion_list)

@register.filter
@stringfilter
def latexnewlines(value):
    return value.replace("\n", "\\\\")
