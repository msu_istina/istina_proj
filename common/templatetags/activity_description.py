# -*- coding: utf-8 -*-
"""Activity description is a small snippet that serves as a standard represenatation
for activity (work). For example, articles are represented by
"publications/article_short.html" template. As such snippets are used very often
and are changed really rarely, pre-caching of them is a crucial task
and so this file were created.

"Interface" for snippets generation is the following templatag:

-->  {% show_activity_description object no_links %}

where object is the activity to show (consult KNOWN_ACTIVITIES below
to discover the supported ones) and no_links is a boolean flag that
determines whether entities (such as authors, journals and so on)
should be wrapped with <a> tags or not

The output of the templatetag is cached by some random time (currently for 20-27 days),
but you shouldn't worry about this.

For cache invalidation take a look at common/cache.py
For cache pregeneration - recache_activity_descriptions command

Also templates/common/activities_list.html make it easy to list activities
"""
from django import template
from django.core.cache import cache
from django.db.models import get_model

import random

register = template.Library()

KNOWN_ACTIVITIES = {
        "publications.Article": ("publications/article_short.html", "article"),
        "publications.Book": ("publications/book_short.html", "book"),
        "conferences.ConferencePresentation": ("conferences/presentation_short.html", "object"),
        "projects.Project": ("projects/project_short.html", "object"),
        "patents.Patent": ("patents/short.html", "object"),
        "certificates.RegistrationCertificate": ("certificates/short.html", "object"),
        "reports.Report": ("reports/short.html", "object"),
        "awards.Award": ("awards/short.html", "object"),
        "intrelations.SocietyMembership": ("intrelations/society_membership_short.html", "object"),
        "intrelations.HonoraryMembership": ("intrelations/honorary_membership_short.html", "object"),
        "intrelations.Traineeship": ("intrelations/traineeship_short.html", "object"),
        "investments.Investment": ("investments/short.html", "object"),
        "journals.JournalEditorialBoardMembership": ("journals/journal_editorial_board_membership_short.html", "object"),
        "icollections.CollectionEditorship": ("icollections/editorship_short.html", "object"),
        "conferences.ConferenceCommitteeMembership": ("conferences/committee_membership_short.html", "object"),
        "dissertation_councils.DissertationCouncilMembership": ("dissertation_councils/membership_short.html", "object"),
        "dissertations.Dissertation": ("dissertations/short.html", "object"),
        "dissertations.Dissertation": ("dissertations/short.html", "object"),
        "diplomas.Diploma": ("diplomas/short.html", "object"),
        "courses.Course": ("courses/short.html", "object"),
        "courses.CourseTeaching": ("courses/teaching_short.html", "object"),
}

KNOWNN_MODELS = dict((get_model(*model_name.split(".")), model_name) for model_name in KNOWN_ACTIVITIES)


class UnknownWorkTypeError(Exception):
    pass


class ProxyActivity(object):
    """Lazy evaluated activity.

    Use this class if you somehow known model and its id, but don't have fetched object
    """
    def __init__(self, activity_model, activity_id):
        if not activity_model in KNOWNN_MODELS:
            raise Exception("Unknown model: %s" % activity_model)
        self.activity_model = activity_model
        self.id = activity_id

    @property
    def activity(self):
        if not hasattr(self, "_activity"):
            self._activity = self.activity_model.objects.get(pk=self.id)
        return self._activity

    @property
    def activity_type(self):
        return KNOWNN_MODELS[self.activity_model]


def get_cache_key(activity_type, activity_obj, no_links):
    return "activity_description_%s_%d_%s" % (activity_type, activity_obj.id, int(no_links))


def get_activity_type(activity_obj):
    if isinstance(activity_obj, ProxyActivity):
        return activity_obj.activity_type
    try:
        return KNOWNN_MODELS[activity_obj.__class__]
    except KeyError:
        raise UnknownWorkTypeError("Unknown activity: %s" % activity_obj.__class__)


def invalidate_activity_description(activity_obj, silient=False):
    try:
        cache.delete(get_cache_key(get_activity_type(activity_obj), activity_obj, True))
        cache.delete(get_cache_key(get_activity_type(activity_obj), activity_obj, False))
    except UnknownWorkTypeError:
        if not silient:
            raise


def recalculate_activity_description(activity_obj, no_links, force=False, no_fetch=False):
    """Read from cache or calculate activity description for activity_obj

    activity_obj
        either one of KNOWN_ACTIVITIES or ProxyActivity
    no_links
        is going to be passed to the template
    force
        if True, then cache is ignored
    no_fetch
        if True then
            if cache exists then its content won't be fetch
            if one doesn't exist then it will be calculated
            used by management command

    returns tuple
        (html, is_recalculated)
    """
    activity_type = get_activity_type(activity_obj)
    cache_key = get_cache_key(activity_type, activity_obj, no_links)
    if not force and cache_key in cache:
        if no_fetch:
            return (None, False)
        return (cache.get(cache_key).decode("utf8"), False)

    if isinstance(activity_obj, ProxyActivity):
        activity_obj = activity_obj.activity

    template_name, context_var = KNOWN_ACTIVITIES[activity_type]
    t = template.loader.get_template(template_name)
    html = t.render(template.context.Context({context_var: activity_obj, "no_links": no_links}))

    # 20 days + 0-7 days
    cache_interval = 1728000 + int(random.random() * 604800)
    cache.set(cache_key, html.encode("utf8"), cache_interval)
    return (html, True)


@register.simple_tag
def show_activity_description(activity_obj, no_links=False):
    """Show standard description for current activity

    See module level docstring for more info
    """
    return recalculate_activity_description(activity_obj, bool(no_links))[0]
