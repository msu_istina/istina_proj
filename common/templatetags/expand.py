﻿# -*- coding: utf-8 -*-
from django import template
from django.template.defaultfilters import stringfilter, safe, escape
from common.utils.strings import split_lines_take_left, split_lines_take_right, line_length
from common.utils.templates import get_args

register = template.Library()


@register.filter(name='expand')
@stringfilter
def expand_wrap(text, args):
    """Split text in two parts, one of which is hidden unless user clicks link.
       Has three optional args(enter as string,use coma as delimiter). First arg 
       is number of lines in first part,second is number of lines to indent,third is
       delimiter."""

    defaults = (10, 0, '\n', 0)
    (num, br_num, delimiter, safe_data) = get_args(args, defaults)
    return expand(text, num, br_num, delimiter, safe_data)


@register.filter(name='expand_safe_data')
@stringfilter
def expand_safe_data_wrap(text, args):
    """Split text in two parts, one of which is hidden unless user clicks link.
       Has three optional args(enter as string,use coma as delimiter). First arg 
       is number of lines in first part,second is number of lines to indent,third is
       delimiter. Doesn't escapes text from HTML in opposite to filter "expand" """

    defaults = (10, 0, '\n')
    (num, br_num, delimiter) = get_args(args, defaults)
    return expand(text, num, br_num, delimiter, safe_data=1)


def expand(text, num, br_num, delimiter, safe_data):
    lines = split_lines_take_left(text, num, delimiter)
    output = lines if safe_data else escape(lines)
    if line_length(text, delimiter) > num:
        output += delimiter
        output += "<br>" * br_num
        if br_num == 0:
            output += "<span class=\"expand\" > </span>"
        output += u"<a class=\"expand\">показать полностью...</a><span class=\"hidden_text hide\">"
        lines = split_lines_take_right(text, num, delimiter)
        output += lines if safe_data else escape(lines)
        output += "</span>"
    return safe(output)
