# -*- coding: utf-8 -*-
from django import template

register = template.Library()


@register.filter
def logic_or(value1, value2):
    return value1 or value2
