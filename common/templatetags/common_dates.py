# -*- coding: utf-8 -*-
from django import template
from common.utils.dates import get_nearest_year as _get_nearest_year

register = template.Library()

@register.assignment_tag
def get_nearest_year():
	return _get_nearest_year()
