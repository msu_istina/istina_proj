# -*- coding: utf-8 -*-
from django import template

register = template.Library()

@register.filter
def get_dict_value(dct, key):
    if isinstance(dct, dict) and key in dct:
        return dct[key]
    else:
        return None
