# -*- coding: utf-8; -*-
from datetime import datetime
from actstream import action
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.contrib.formtools.wizard.views import SessionWizardView
from django.core import serializers
from django.db import models
from django.http import HttpResponse, Http404, HttpResponseForbidden, HttpResponseServerError
from django.shortcuts import get_object_or_404, redirect
from django.utils import simplejson
from django.utils.decorators import classonlymethod, method_decorator
from django.db.models.loading import get_model
from django.forms.formsets import BaseFormSet
from django.views.generic import TemplateView
from django import forms
from reversion.models import Version

from common.forms import make_attachment_formset, AddAuthorScienceForm
from common.models import Attachment, MergeRequest, MergeRequestItem, AuthorScience, AuthorRoleObject, AuthorRole, ScienceWork
from common.utils.autocomplete import autocomplete_helper
from common.utils.context import render_to_response
from common.utils.files import process_attachments
from common.utils.list import flatten, is_tuple_list, in_or_equal
from common.utils.strings import capitalize_1st_letter
from common.utils.uniqify import uniqify
from common.utils.reversion_utils import get_version_comment, get_admin_url
from common.utils.user import get_profile_url
from common.utils.user import get_worker
from publications.utils import split_authors
from sqlreports.models import CheckboxesProcessor
from sqlreports.views import createreport
from celery_utils.models import MyTaskMeta
from common.permissions import MyModelPermission
from unified_permissions import has_permission, get_permission_class
from unified_permissions.decorators import permission_required
import requests, json
from organizations.models import Representative
from django.conf import settings
from workers.models import Employment, Worker
import re




class MyModelWizardView(SessionWizardView):
    '''Class containing some extra common methods and attributes for wizards.'''
    add = True  # add mode
    edit = False  # edit mode
    model = None  # should be overriden
    main_step = 'main'  # name of the main step
    steps_info = {}

    @classonlymethod
    def make_condition_dict(cls, *steps):
        condition_dict = {}
        for step in steps:
            # see http://stackoverflow.com/a/2295372
            condition_dict[step] = lambda wizard, step = step: wizard.step_included(step)

        return condition_dict

    @classonlymethod
    def create_wizard_from_form(cls, form_class):
        class MyModelWizardViewWrapper(cls):
            form_list = [(MyModelWizardView.main_step, form_class)]
            model = form_class.Meta.model

        return MyModelWizardViewWrapper

    @classonlymethod
    def as_view(cls, *args, **kwargs):
        '''Main function that is executed to get the dispatch function,
        which then processes get and post requests.'''
        return super(MyModelWizardView, cls).as_view(
            form_list = cls.form_list,
            initial_dict = cls.initial_dict,
            instance_dict = cls.instance_dict,
            condition_dict = cls.condition_dict,
            *args, **kwargs)

    def has_edit_permission(self, user, instance):
        return has_permission(user, "edit", instance)

    def dispatch(self, request, *args, **kwargs):
        object_id = kwargs.get('object_id', None)
        if object_id and self.model:
            try:
                instance = self.model.objects.get(id=object_id)
            except:
                return redirect("home")
            else:
                if not self.has_edit_permission(request.user, instance):
                    return redirect(instance)
                self.instance_dict[self.main_step] = instance
                self.edit = True
                self.add = False
        kwargs['session_object_num'] = self.get_current_wizard_num(request)
        return super(MyModelWizardView, self).dispatch(request, *args, **kwargs)

    def get_template_names(self):
        return 'common/add_edit_object_wizard.html'

    def get_context_data(self, form, **kwargs):
        context = super(MyModelWizardView, self).get_context_data(form, **kwargs)
        # show all previous forms (read-only)
        previous_forms = []
        try:
            self.no_previous_forms
        except AttributeError:
            for form_key in self.get_form_list():
                if form_key == self.steps.current:  # show only previous forms
                    break
                form_obj = self.get_form(
                    step=form_key,
                    data=self.storage.get_step_data(form_key),
                    files=self.storage.get_step_files(form_key))
                previous_forms.append(form_obj)
        context.update({
            'model': self.model,
            'add': self.add,
            'edit': self.edit,
            'previous_forms': previous_forms,
            'formset': isinstance(form, BaseFormSet),
            'current_step_info': self.steps_info.get(self.steps.current, None)
        })
        return context

    def set_extra_data(self, *args):
        '''Arguments can be two-tuples (key/value) or two args (key and value).'''
        if not is_tuple_list(args[0]):  # the args are two: key and value.
            data = {args[0]: args[1]}
        else:  # args are two-tuples
            data = {}
            for key, value in args:
                data.update({key: value})
        extra_data = self.storage._get_extra_data()
        extra_data.update(data)
        self.storage._set_extra_data(extra_data)

    def get_extra_data(self, *args):
        '''Get the stored extra data by keys.
        If cls is specified, check if the value is an instance of the class.
        If not, return None.
        @args can be just keys, or two-tuples (key, cls), or two args: key and cls.
        '''
        if len(args) == 2 and type(args[1]) == models.base.ModelBase:  # two args: key and cls
            args = (args,)
        else:
            args = [(arg, None) if not is_tuple_list(arg) else arg for arg in args]

        def check_class(value, cls):
            return value if ((not cls) or isinstance(value, cls)) else None
        values = [check_class(self.storage._get_extra_data().get(key, None), cls) for key, cls in args]
        return tuple(values) if len(values) > 1 else values[0]

    def delete_extra_data(self, key):
        '''Deletes the given key.'''
        extra_data = self.storage._get_extra_data()
        if key in extra_data:
            extra_data.pop(key)
        self.storage._set_extra_data(extra_data)

    def done(self, form_list, **kwargs):
        # all forms are submitted and validated, now process the data
        # save main object form
        object = form_list[0].save(self.request)
        return redirect(object)

    def hide_form_fields(self, forms, form_key, fields, current_step_in=None):
        '''This function hides @fields from the form with @form_key in @forms
        if the current step is (optionally) @current_step_in.
        @current_step_in can be a tuple or list or a single string.
        @fields can be a tuple/list or a single field (a string).
        '''
        if not in_or_equal(self.steps.current, current_step_in) or form_key not in self.steps.all:
            return
        forms[self.get_step_index(form_key)].hide_fields(fields)

    def step_included(self, step):
        # if step is not skipped, it is included
        return not self.get_extra_data('skip_%s_step' % step)

    def include_steps(self, *steps):
        for step in steps:
            self.set_extra_data('skip_%s_step' % step, False)

    def skip_steps(self, *steps):
        for step in steps:
            self.set_extra_data('skip_%s_step' % step, True)

    def skip_or_include_steps(self, *args):
        '''args are two: step and condition,
        or two-tuples (step, condition).'''
        if not is_tuple_list(args[0]):  # two args
            if args[1]:  # condition is true
                self.skip_steps(args[0])
            else:
                self.include_steps(args[0])
        else:  # args are two-tuples (step, condition)
            for step, condition in args:
                if condition:
                    self.skip_steps(step)
                else:
                    self.include_steps(step)

    def get_current_wizard_num(self, request):
        '''
        Determine number of wizard. If this is new step of existing wizard, than we get it's number from management
        form. Otherwise, generate new number based on request session.
        '''
        POST = request.POST
        prefix = self.get_prefix_base()
        management_form_field_name = None
        for field in POST:
            if field.startswith(prefix):
                management_form_field_name = field
        if not management_form_field_name:
            return str(self.get_new_wizard_num(request))
        object_num = ''
        for c in management_form_field_name[len(prefix) + 1:]:
            try:
                int(c)
            except ValueError:
                break
            else:
                object_num += c
        return object_num

    def get_new_wizard_num(self, request):
        '''
        Each wizard has it's own record in request session. We count records of this wizard class in session and
        return number to be assigned to new wizard. "wizard_" is a prefix, automatically added by django to every
        record of wizards in request session.
        '''
        session = request.session
        prefix = 'wizard_' + self.get_prefix_base()
        count_num = len([x for x in session.keys() if x.startswith(prefix)])
        return count_num + 1

    def get_prefix_base(self,  *args, **kwargs):
        return super(MyModelWizardView, self).get_prefix(*args, **kwargs)

    def get_prefix(self,  *args, **kwargs):
        base_prefix = self.get_prefix_base(*args, **kwargs)
        session_objects_num = kwargs.get('session_object_num', None)
        if not session_objects_num:
            return super(MyModelWizardView, self).get_prefix(*args, **kwargs)
        return base_prefix + '_' + session_objects_num


class LinkedToWorkersModelWizardView(MyModelWizardView):
    '''Common class for wizards working with objects that are linked to workers.

    Note that this class is used not only for LinkedToWorkersModel subclasses,
    but also for Workership subclasses (e.g. DissertationCouncilMembership),
    since such classes also linked to a worker
    (directly, not through an intermediate model).

    The wizard can contain several forms linking to workers,
    e.g. for advisors of a dissertation and for the author.
    '''
    # names of the workers disambiguation steps
    workers_steps = ['workers_disambiguation']

    @classonlymethod
    def create_wizard_from_form(cls, form_class, *args, **kwargs):
        from workers.forms import WorkerFormSet # to avoid circular import
        class LinkedToWorkersModelWizardViewWrapper(cls):
            form_list = [(cls.main_step, form_class)]
            for step in cls.workers_steps:
                form_list.append((step, WorkerFormSet))
            model = form_class.Meta.model

        return LinkedToWorkersModelWizardViewWrapper

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LinkedToWorkersModelWizardView, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self, step=None):
        """
        Returns the keyword arguments for instantiating the form
        (or formset) on given step.
        """
        kwargs = super(LinkedToWorkersModelWizardView, self).get_form_kwargs(step)
        if step in self.workers_steps:
            kwargs.update({
                'required': True,
                'required_error_msg': self.model.workers_required_error_msg,
                'user': self.request.user,
                'object_instance': self.instance_dict.get(self.main_step, None)
            })
        return kwargs

    def get_form_initial(self, step):
        """
        Returns a dictionary which will be passed to the form for `step`
        as `initial`. If no initial data was provied while initializing the
        form wizard, a empty dictionary will be returned.
        """
        initial = super(LinkedToWorkersModelWizardView, self).get_form_initial(step)
        if step in self.workers_steps:
            # prepare workers entered
            workers_str = self.get_extra_data(step + '-workers_str')
            if workers_str:
                workers = split_authors(workers_str)
                tuple2dict = lambda worker_tuple: dict(
                    zip(('lastname', 'firstname', 'middlename',), worker_tuple))
                initial = map(tuple2dict, workers)
        return initial

    def get_context_data(self, form, **kwargs):
        context = super(LinkedToWorkersModelWizardView, self).get_context_data(form, **kwargs)
        context.update({
            'workers_step': self.steps.current in self.workers_steps
        })
        return context

    def process_step(self, form):
        if self.steps.current == self.main_step:
            # save workers_str for workers disambiguation form initial data
            # NB! workers_steps should go in the same order as form.workers_fields
            for step, field in zip(self.workers_steps, form.workers_fields):
                # form.workers_fields should be defined
                self.set_extra_data(step + '-workers_str', form.cleaned_data[field])
        return super(LinkedToWorkersModelWizardView, self).process_step(form)

    def save_object(self, form_list, *args, **kwargs):
        '''This function is executed during done() method,
        and used to save main form object.

        Workers objects has been already saved.
        '''
        return form_list[0].save(self.request, self.workers, *args, **kwargs)

    def done(self, form_list, **kwargs):
        # all forms are submitted and validated, now process the data
        # get the index of the workers step, to get the form
        self.workers = []
        workers_forms = []
        for step in self.workers_steps:
            if not self.step_included(step):
                continue
            index = self.get_step_index(step)
            workers_form = form_list[index]
            workers_form.save()
            self.workers.append(workers_form.worker_instances)
            workers_forms.append(workers_form)
        if len(self.workers_steps) == 1:  # only one workers step, flatten workers ([[1,2]] -> [1,2])
            self.workers = flatten(self.workers)
        # save main object form
        object = self.save_object(form_list)
        # log events
        for workers_form in workers_forms:
            for worker in workers_form.workers_created:  # log new worker creations
                action.send(self.request.user, verb=u'добавил сотрудника', action_object=worker, target=object)
            for worker_alias in workers_form.worker_aliases_created:  # log new worker aliases creations
                action.send(self.request.user, verb=u'добавил псевдоним сотрудника', action_object=worker_alias, target=object)
        return redirect(object)


def detail(request, object_id, model, template_name, extra_context=None):
    '''Shows the object page.'''
    object = get_object_or_404(model, pk=object_id)
    can_edit_object = has_permission(request.user, "edit", object)
    can_delete_object = has_permission(request.user, "delete", object)
    can_view_object_history = has_permission(request.user, "view_object_history", object)
    content_type = ContentType.objects.get_for_model(model)
    attachment_formset = make_attachment_formset(content_type, queryset=Attachment.objects.none())
    context = {
        '%s' % object.nominative_en: object,
        'can_edit_object': can_edit_object,
        'can_delete_object': can_delete_object,
        'can_view_object_history': can_view_object_history,
        'attachment_formset': attachment_formset
    }
    if extra_context is not None:
        context.update(extra_context)
    return render_to_response(request, template_name, context)


@login_required
def delete(request, object_id, model):
    '''Deletes the object.'''
    object = get_object_or_404(model, pk=object_id)
    if not has_permission(request.user, "delete", object):
        return redirect(object)
    if hasattr(object, 'xml'):
        object.xml = ""  # not to store it twice in the event logs
    description = serializers.serialize("xml", [object])
    action.send(request.user, verb=u'удалил %s' % object.accusative, target=object, description=description)
    message = u"%s '%s' успешно удален%s." % (capitalize_1st_letter(object.nominative), object.title, object.gender_suffix)
    object.delete()
    messages.success(request, message)
    return redirect("home")

@permission_required("delete_if_no_related_objects")
def delete_if_no_related_objects(request, content_type_id, object_id):
    '''Deletes the object if it has no related objects.'''
    try:
        content_type = ContentType.objects.get_for_id(content_type_id)
        obj = content_type.get_object_for_this_type(pk=object_id)
    except models.ObjectDoesNotExist:
        raise Http404
    # another check to be sure that superuser will not delete the object too
    if obj.has_related_objects:
        messages.error(request, u"%s нельзя удалить, потому что к н%s привязаны другие объекты" % (obj.nominative, obj.gender_possessive_dative))
        return redirect("home")
    if hasattr(obj, 'xml'):
        obj.xml = ""  # not to store it twice in the event logs
    description = serializers.serialize("xml", [obj])
    action.send(request.user, verb=u'удалил %s' % obj.accusative, target=obj, description=description)
    message = u"%s '%s' успешно удален%s." % (capitalize_1st_letter(obj.nominative), obj.title, obj.gender_suffix)
    obj.delete()
    messages.success(request, message)
    return redirect("home")


def autocomplete_search(request, model):
    '''Returns instances info of a given model for jquery autocomplete for a given term.'''
    term = request.GET.get('term', '')
    limit = 10
    fields = []
    if len(term) >= 2:
        objects = autocomplete_helper(model, model.search_attr, term, limit)
        fields = [getattr(obj, obj.search_attr) for obj in objects]
        fields = uniqify(fields)
    return HttpResponse(simplejson.dumps(fields), content_type='application/javascript')


@user_passes_test(lambda u: u.is_superuser)
def history_view(request, username=None):
    versions_num_limit = 100
    if username:
        user = get_object_or_404(User, username=username)
        versions = Version.objects.filter(revision__user=user)
    else:
        versions = Version.objects.all()
    action_list = [
        {
            "revision": version.revision,
            "object": version.object or version.object_repr,
            "comment": get_version_comment(version),
            'model': getattr(version.object, 'nominative', None) or version.content_type,
            "profile_url": get_profile_url(version.revision.user) if version.revision.user else None,
            'admin_url': get_admin_url(version)
        }
        for version
        in versions.order_by('-revision__date_created').select_related("revision__user")[:versions_num_limit]
    ]
    context = {"action_list": action_list}
    return render_to_response(request, 'history.html', context)


@login_required
def link_to(request, content_type_id, object_id,
            permission = 'link_to', link_to_method = 'link_to'):
    try:
        content_type = ContentType.objects.get_for_id(content_type_id)
        obj = content_type.get_object_for_this_type(pk=object_id)
    except models.ObjectDoesNotExist:
        raise Http404
    if not has_permission(request.user, permission, obj):
        return redirect(obj)

    workership, old_worker, alias, alias_created = getattr(
        obj, link_to_method)(request.user)
    if not workership:
        return redirect(obj)

    # cache should be invalidated manually,
    # since after deleting the link to worker,
    # we don't know any more to which worker the object used to be linked to
    if old_worker:
        from common.utils.cache import invalidate_worker_coauthors_summary, invalidate_activity_type
        invalidate_worker_coauthors_summary(old_worker)
        invalidate_activity_type(old_worker.id, obj.nominative_en)

    action.send(request.user,
                verb=u'привязал к себе {0}'.format(obj.accusative_short),
                action_object=old_worker,
                target=obj)

    message = u'Вы успешно привязали к себе {0}. ' \
        u'Информация на Вашей личной странице была обновлена.'.format(obj.accusative_short)
    messages.success(request, message)

    if alias_created:
        description = u'lastname: {0}, firstname: {1}, middlename: {2}, ' \
            u'validated: {3}, создан автоматически при связывании {4} с собой'.format(
                alias.lastname, alias.firstname, alias.middlename,
                alias.validated, obj.accusative_short)
        action.send(request.user,
                    verb=u'добавил псевдоним сотрудника',
                    action_object=alias,
                    target=get_worker(request.user),
                    description=description)

    return redirect('home_profile')


@permission_required("unauthor")
def unauthor(request, content_type_id, object_id):
    try:
        content_type = ContentType.objects.get_for_id(content_type_id)
        obj = content_type.get_object_for_this_type(pk=object_id)
    except models.ObjectDoesNotExist:
        raise Http404

    obj.unauthor(request.user)

    # cache should be invalidated manually,
    # since after deleting the link to worker,
    # we don't know any more to which worker the object used to be linked to
    worker = get_worker(request.user)
    if worker:
        from common.utils.cache import invalidate_worker_coauthors_summary, invalidate_activity_type
        invalidate_worker_coauthors_summary(worker)
        invalidate_activity_type(worker.id, obj.nominative_en)

    action.send(request.user,
                verb = u'отказался от {0}'.format(obj.genitive_short),
                target = obj)

    message = u'Вы успешно отказались от {0}.'.format(obj.genitive_short)
    messages.success(request, message)

    return redirect('home')


def datatables_json_by_id(request, app_label, model_name):
    model = get_model(app_label, model_name)
    ids = request.GET.get('ids', "")
    instances = model.objects.get_objects_by_id_string(ids)
    message_data = model.objects.get_datatable_json(instances)
    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')


def merge_request_json_by_id(request, merge_request_id):
    merge_request = get_object_or_404(MergeRequest, pk=merge_request_id)
    message_data = merge_request.objects_to_merge_datatables_json()
    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')


@permission_required("edit")
def add_attachments(request, content_type_id, object_id):
    try:
        content_type = ContentType.objects.get_for_id(content_type_id)
        obj = content_type.get_object_for_this_type(pk=object_id)
    except models.ObjectDoesNotExist:
        raise Http404
    attachment_formset = make_attachment_formset(content_type, request.POST, request.FILES)
    process_attachments(request, attachment_formset, obj, request.user)
    return redirect(obj)


@permission_required("edit")
def delete_attachments(request, content_type_id, object_id, attachment_id=None, delete_all=False):
    try:
        content_type = ContentType.objects.get_for_id(content_type_id)
        obj = content_type.get_object_for_this_type(pk=object_id)
    except models.ObjectDoesNotExist:
        raise Http404

    if not delete_all:
        attachments = obj.attachments.filter(id=attachment_id)
    else:
        attachments = obj.attachments.all()
    for attachment in attachments:
        description = u"filename: %s%s%s" % (
            attachment.filename,
            u', category: %s' % attachment.category if attachment.category else '',
            u', description: %s' % attachment.description if attachment.description else '')
        action.send(
            request.user,
            verb=u'удалил прикрепленный файл к %s' % obj.dative,
            action_object=attachment,
            target=obj,
            description=description)
        attachment.delete()
    from common.templatetags.activity_description import invalidate_activity_description
    invalidate_activity_description(obj, silient=True)
    return HttpResponse(simplejson.dumps({'status': "ok"}), content_type='application/javascript')


class MergeWizardView(TemplateView):
    u'''
    View, отвечающий за реализацию интерфейса по подаче заявок на слияние  объектов и дальнейшее их сливание.
    Для того, чтобы была возможность слить объекты модели Example из приложения test необходимо:
        1. Создать SQL отчет по ссылке /sqlreports/edit/ с названием example_test. В качестве примера можно посмотреть
          отчет conferences_conference
        2. Для того, чтобы  объекты  класса Example можно было представить в виде таблицы datatables,
           у класса должны быть реализованы методы:
          -  get_datatable_merge_column_names, который возвращает список названий столбцов
          - datatable_merge_json, который сериализует специальным образом объекты класса. В качестве примера можно использовать
          соответствующий метод класса Conference
        3. У менеджера , соответствующего классу Example, должен быть метод merge с сигнатурой
        merge(self, username, from_ids, to_id), который выполняет объединение объектов с первичными ключами из from_ids,
        перекидывая информацию из них в объект с ключом to_id, после чего удаляет объекты c ключами из from_ids. Также
        функция должна проверять, что у пользователя username, который выполняет объединение(обычно администратор), есть
        все необходимые права. В идеале, необходима одна единая функция сливания для всех классов, пока что
         для каждого класса  пишется отдельная.
    Схема работы пользователя с интерфейсом( на примере модели Conference из приложения conferences)
        1. Пройти по ссылке /common/conferences/conference/merge_wizard/. Генерируется отчет из sqlreports при помощи
        метода merge_candidates_list.
        2. Выбрать объекты для объединения, расставив галочки, и нажать "Передать". За генерацию этой страницы и
        и отправку данных на следующий шаг, отвечает модуль sqlreports и класс MergeProcessor.
        3. На следующий странице отредактировать свой выбор, выбрать главный объект( в который будут сливаться все остальные) и
        оставить заявку. Эта страница использует js datables для генерации таблицы.
    Далее администратор заходит на страницу /common/merge_requests/, просматривает добавленные заявки и подтверждает их
    (в этом случае происходит объединение объектов) или отклоняет(в этом случае заявка помечается как отклоненная, объединения
    не происходит). За генерацию списка добавленных заявок отвечает функция merge_request_view.
    Заявки на слияние хранятся в объектах моделей MergeRequest(сама заявка и главный сливаемый объект) и  MergeRequestItem( второстепенные объекты).
    Заявки, оставляемые администраторами, автоматически подтверждаются и выполняются.
    '''
    template_name = "common/merge_confirm.html"

    def dispatch(self, request, *args, **kwargs):
        if 'merge_request_id' in kwargs:
            # так как просмотр заявок недоступен простым пользователям, нужен второй dispatch с декоратором
            # user_passes_test
            return self.dispatch_detail(request, *args, **kwargs)
        return super(MergeWizardView, self).dispatch(request, *args, **kwargs)

    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch_detail(self, request, *args, **kwargs):
        return super(MergeWizardView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MergeWizardView, self).get_context_data(**kwargs)
        merge_request_id = kwargs.get('merge_request_id', None)
        if merge_request_id: # если просмтариваем заявку, то данные берем из объекта заявки
            merge_request = get_object_or_404(MergeRequest, pk=merge_request_id)
            context['candidate_ids'] = merge_request.objects_to_merge_ids() # первичные ключи сливаемых объектов
            context['app_label'] = merge_request.content_type.app_label # название приложения
            context['model_name'] = merge_request.content_type.model # название модели
            context['main_candidate_id'] = merge_request.main_object_id #главный сливаемый объект
            context['merge_request'] = merge_request # объект заявки на слияние
        else:
            context['candidate_ids'] = kwargs['ids']
            context['app_label'] = kwargs['app_label']
            context['model_name'] = kwargs['model_name']

        model = get_model(context['app_label'], context['model_name'])
        context['can_merge'] = self.request.user.is_superuser # пользователь имеет право проверять заявку
        context['column_names'] = model.get_datatable_merge_column_names()
        return context

    def get(self, *args, **kwargs):
        merge_step = kwargs.get('merge_step', 'candidates_list')
        if merge_step == 'candidates_list':
            app_label = kwargs['app_label']
            model_name = kwargs['model_name']
            return self.merge_candidates_list(app_label, model_name)
        return super(MergeWizardView, self).get(self, *args, **kwargs)

    def post(self, *args, **kwargs):
        merge_step = kwargs.get('merge_step', 'candidates_list')
        if merge_step == 'final': # пользователь засабмитил данные на последнем шаге
            if 'return' in self.request.POST: # Нажата кнопка 'Отмена'
                return redirect('merge_wizard_done', kwargs['app_label'], kwargs['model_name'], 'cancel')
            main_candidate_id = int(self.request.POST['main_candidate'])
            secondary_candidates_id = []
            for field_name in self.request.POST:
                if field_name.startswith('candidate_'):
                    candidate_id = int(self.request.POST[field_name])
                    if candidate_id != main_candidate_id:
                        secondary_candidates_id.append(candidate_id)
            model = get_model(kwargs['app_label'], kwargs['model_name'])
            if 'submit_request' in self.request.POST: # сохраняем заявку для обработки администратором
                self.create_merge_request(model, main_candidate_id, secondary_candidates_id)
            else: # администратор проверил заявку
                self.review_merge_request(model, main_candidate_id, secondary_candidates_id)
            return redirect('merge_wizard_done', kwargs['app_label'], kwargs['model_name'], 'submit')
        return super(MergeWizardView, self).get(self, *args, **kwargs)

    def review_merge_request(self, model, main_candidate_id, secondary_candidates_id, *args, **kwargs):
        accept_merge_request = True if 'accept_request' in self.request.POST else False
        merge_request_id = self.request.POST['merge_request_id']
        try:
            merge_request_id = int(merge_request_id)
        except ValueError:
            merge_request = None
        else:
            merge_request = MergeRequest.objects.get(pk=merge_request_id)
        reviewer = self.request.user
        if accept_merge_request:
            if not merge_request:
                merge_request = self.create_merge_request(model, main_candidate_id, secondary_candidates_id)
                error_message = merge_request.approve_request(reviewer)
            else:
                if merge_request.same_merge_candidates(main_candidate_id, secondary_candidates_id):
                    error_message = merge_request.approve_request(reviewer)
                else:
                    new_merge_request = self.create_merge_request(model, main_candidate_id, secondary_candidates_id)
                    error_message = new_merge_request.approve_request(reviewer)
                    if not error_message:
                        merge_request.decline_request(reviewer)

            if error_message is None:
                messages.success(self.request, u'Заявка подтверждена')
            else:
                messages.error(self.request, u'Во время объединения объектов произошла ошибка: %s' % error_message)

        else:
            if merge_request:
                merge_request.decline_request(reviewer)
                messages.success(self.request, u'Заявка отклонена')

    def create_merge_request(self, model, main_candidate_id, secondary_candidates_ids, *args, **kwargs):
        try:
            request_description = unicode(model.objects.get(pk=main_candidate_id))
        except model.DoesNotExist:
            messages.error(self.request, u'Объекта, в который должны быть перенесены данные, не существует')
            return
        merge_request = MergeRequest(content_type=ContentType.objects.get_for_model(model), main_object_id=main_candidate_id,
                                     description=request_description, creator=self.request.user)
        merge_request.save()
        secondary_candidates = model.objects.filter(pk__in=secondary_candidates_ids)
        for candidate in secondary_candidates:
            MergeRequestItem(merge_request=merge_request, object_id=candidate.id,
                             description=unicode(candidate)).save()
        messages.success(self.request, u'Заявка на объединение сохранена')
        return merge_request

    def merge_candidates_list(self, app_label, model_name):
        GET = self.request.GET.copy()
        GET.update({
            '__checkbox_fn': 'merge_processor',
            '__checkbox_model_name': model_name,
            '__checkbox_app_label': app_label,
        })
        self.request.GET = GET
        repid = '%s_%s' % (app_label, model_name)
        return createreport(self.request, repid, check_signatures=False)


class MergeProcessor(CheckboxesProcessor):
    codename = 'merge_processor'

    def get_form_fields(cls, report_params, signer):
        fields = {}
        fields["app_label"] = forms.CharField(widget=forms.HiddenInput(),
                                              initial=report_params['__checkbox_app_label'],
                                              label="")
        fields["model_name"] = forms.CharField(widget=forms.HiddenInput(),
                                               initial=report_params['__checkbox_model_name'],
                                               label="")
        return fields

    def do(cls, request, checkboxes_marked, checkboxes_shown):
        app_label = request.POST['app_label']
        model_name = request.POST['model_name']
        return MergeWizardView.as_view()(
            request,
            merge_step='confirm',
            ids=checkboxes_marked,
            model='model',
            app_label=app_label,
            model_name=model_name)


@user_passes_test(lambda u: u.is_superuser)
def merge_request_view(request):
    return render_to_response(request, 'merge_requests.html')


def datatable_merge_requests_json(request):
    max_search_result_limit = 300
    object_class = MergeRequest
    fields_to_prefetch = ['creator', 'reviewer', 'secondary_items']
    order_by_fields = ['-request_date']
    merge_requests = object_class.objects.all().prefetch_related(*fields_to_prefetch).order_by(*order_by_fields)
    merge_requests = list(merge_requests[:max_search_result_limit])
    message_data = {}
    message_data["aaData"] = [merge_request.datatable_data_json() for merge_request in merge_requests]
    return HttpResponse(simplejson.dumps(message_data), content_type='application/javascript')

@user_passes_test(lambda u:u.is_superuser)
def celery_tasks_list(request):
    tasks = MyTaskMeta.objects.filter(user=request.user)
    time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return render_to_response(request, 'celery_tasks_list.html', {'tasks': tasks,
                                                                  'time': time})

def redirect_with_message(request, status, message, redirect_to="startpage"):
    getattr(messages, status)(request, message)
    return redirect(redirect_to)

def redirect_with_error(request, message, redirect_to="startpage"):
    return redirect_with_message(request, "error", message, redirect_to)





def add_redmine_ticket(request):

    if request.method != 'POST':
        return HttpResponseServerError("Request method is not applied. ")


    user = request.user
    url = settings.REDMINE_FEEDBACK['root_url'] + settings.REDMINE_FEEDBACK['issues_url'] + "?key=" + settings.REDMINE_FEEDBACK['redmine_api_key']
    description = request.POST.get("description", "")
    subject = request.POST.get("subject", description[:50])
    category = request.POST.get("category", "empty category")

    # here we should get id of author and put it as watcher_user_id value
    #

    representatives_fullname = u"" if not user.is_authenticated() else u"\n".join([rep.fullname_short + u"[" + str(rep.user_id) + u"]" + "  " + rep.get_full_url()
    for rep in Representative.objects.get_for_user(user)[:10] if rep.user_id])

    departments_fulllist = u"" if not user.is_authenticated() else u"\n".join([_dep.department.name +  "  " + _dep.get_full_url()
    for _dep in user.get_profile().worker.employments.current()])


    watcher_user_ids = redmine_get_userid(user, request.POST.get("user_email", ""))
    assigned_to_id = 19 # we have to assign all tickets to our common user in redmine
    description += u"\n----------------------------------------------------------------------------"
    description += u"\nНиже представлена техническая информация, необходимая для обработки запроса:"
    description += u"\nАдрес профиля: " + ( user.get_profile().get_full_url() if (user.id) else "" )
    description += u"\nИмя пользователя: " + user.username
    description += u"\nEmail: " + ( user.email if (user.id) else request.POST.get("user_email", "") )
    description += u"\nФИО: " + ( user.get_profile().fullname if (user.id) else "" )
    description += u"\nСписок ответственных: " + ( representatives_fullname if (user.id) else "" )
    description += u"\nНомер подразделения: " + ( departments_fulllist if (user.id) else "" )
    description += u"\nUrl: " + request.META.get('HTTP_REFERER')

    tracker_id = settings.REDMINE_FEEDBACK['tracker_id']
    if ((user.id) and user.representatives.exists()):
        tracker_id = settings.REDMINE_FEEDBACK['representatives_tracker_id']
    if (user.id == None):
        tracker_id = settings.REDMINE_FEEDBACK['anonymous_tracker_id']


    data = {'issue':
                {'project_id' : settings.REDMINE_FEEDBACK['project_id'],
                 'tracker_id': tracker_id  ,
                 'description': description,
                      'status_id': settings.REDMINE_FEEDBACK['status_id'], 'subject': subject, 'assigned_to_id': assigned_to_id, 'watcher_user_ids': watcher_user_ids,
                      'custom_fields' : [{'value' : category, 'id' : settings.REDMINE_FEEDBACK['custom_field_id']}]
                }
    }
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    #print json.dumps(data)
    r = requests.post(url, data=json.dumps(data), headers=headers)

    if r.status_code == 201:
        return HttpResponse(simplejson.dumps({'new_ticket_id': json.loads(r.text)['issue']['id']}), content_type='application/javascript')
    else:
        return HttpResponseServerError("Cannot create ticket for some reason")

    return HttpResponseServerError("Cannot create ticket for some reason")


def redmine_get_userid(istina_user_object, email = ""):
    #Check if user exist in redmine.
    #If yes -- update his\\her email address and return user id.
    #If no -- create new user and return user id
    url = settings.REDMINE_FEEDBACK['root_url'] + settings.REDMINE_FEEDBACK['users_url'] + "?key=" + settings.REDMINE_FEEDBACK['redmine_api_key']
    add_to_group_url = settings.REDMINE_FEEDBACK['root_url'] + settings.REDMINE_FEEDBACK['user_to_group_url'] + "?key=" + settings.REDMINE_FEEDBACK['redmine_api_key'] # hardcode 14 very very bad
    search_url = url + "&name=" + ( istina_user_object.email if (istina_user_object.id) else email)

    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.get(search_url,  headers=headers)


    for x in range(0, int(json.loads(r.text)['total_count'])):
        if json.loads(r.text)['users'][x]['mail'] == ( istina_user_object.email if (istina_user_object.id) else email):
            return json.loads(r.text)['users'][x]['id']

    # если не нашли такую почту в редмайне, то пробуем найти такой ид



    if (istina_user_object.id) :
        search_url = url + "&name=" + str(istina_user_object.id)
        r = requests.get(search_url,  headers=headers)
        # если находим такой ид, то апдейтим почту и возвращаем этот номер
        for x in range(0, int(json.loads(r.text)['total_count'])):
            if json.loads(r.text)['users'][x]['login'] == istina_user_object.id:
                redmine_update_user_email(json.loads(r.text)['users'][x]['id'], istina_user_object.email)
                return json.loads(r.text)['users'][x]['id']

    # если мы не нашли ид и почту, то нам надо создать юзера
    data = {'user': {
        'login': ( istina_user_object.id if (istina_user_object.id) else re.sub('[^A-Za-z0-9]+', '', email)),
        'firstname': (istina_user_object.get_profile().firstname if (istina_user_object.id) else "firstname"),
        'lastname': ( istina_user_object.get_profile().lastname if (istina_user_object.id) else "lastname"),
        'mail': ( istina_user_object.email if (istina_user_object.id) else email)
    }}
    r = requests.post(search_url, data=json.dumps(data), headers=headers)
    new_user_id = -1
    if r.status_code == 201:
        #in case if new user have been created we need to add this user to 'istina_users' role
        new_user_id = int(json.loads(r.text)['user']['id'])
        data = {'user_id': new_user_id}
        r = requests.post(add_to_group_url, data=json.dumps(data), headers=headers)



    #    return json.loads(r.text)['user']['id']

    return new_user_id


def redmine_update_user_email(redmine_user_id, email):

    url = settings.REDMINE_FEEDBACK['root_url'] + "/users/" + str(redmine_user_id) + ".json?key=" + settings.REDMINE_FEEDBACK['redmine_api_key']
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    data = {'user' : {'mail': email}}
    r = requests.put(url,  data=json.dumps(data), headers=headers)


@login_required
def add_authorscience(request, object_id):
    sciencework = get_object_or_404(ScienceWork,pk=object_id)
    objectdict = {'article':sciencework.article,
                  'course':sciencework.course,
                  'disser':sciencework.disser,
                  'book':sciencework.book,
                  'report':sciencework.report,
                  'diploma':sciencework.diploma,
                  'patent':sciencework.patent}

    type, object = next((key,objectdict[key]) for key,obj in objectdict.items() if obj is not None)
    roles = AuthorRole.objects.filter(authorroleobj__code=type)
    form = AddAuthorScienceForm(roles, request.POST or None)
    worker = get_worker(request.user)
    template = "add_authorscience.html"
    if request.POST:
        if form.is_valid():
            role = form.cleaned_data['role']
            if AuthorScience.objects.filter(sciencework=sciencework, authorrole=role, worker=worker).exists():
                messages.info(request, u"Заявка на участие в создании данной научной продукции уже была подана ранее")
                return redirect(object.get_absolute_url())
            AuthorScience.objects.create(sciencework=sciencework,worker=worker,authorrole=role,user=request.user,name=worker.fullname)
            messages.success(request, u"Заявка на участие в создании данной научной продукции подана. Когда она будет одобрена, Ваше имя появится в описании научной продукции.")
            return redirect(object.get_absolute_url())

    return render_to_response(request,template,{'form':form,'object':object,'worker':worker})


def add_authorscience_old(worker_id, object_id,role_id):
    sciencework = get_object_or_404(ScienceWork,pk=object_id)
    if not AuthorScience.objects.filter(sciencework=sciencework,worker__id=worker_id).exists():
        worker = get_object_or_404(Worker, pk=worker_id)
        role = get_object_or_404(AuthorRole, pk=role_id)
        AuthorScience.objects.create(sciencework=sciencework, worker=worker, authorrole=role)

@login_required
def confirm_authorscience(request):
    try:
        authorscience_id = request.POST.get('authorscience_id', None)
        confirm_flag = int(request.POST.get('confirmflag', None))
        authorscience = get_object_or_404(AuthorScience,pk=authorscience_id)
        authorscience.date_validated = datetime.now()
        authorscience.user_validate = request.user
        authorscience.flag_validate = confirm_flag
        authorscience.save()
    except Exception, e:
        return HttpResponseForbidden(e)
    if confirm_flag == 1:
        return HttpResponse('CONFIRMED')
    else:
        return HttpResponse('DECLINED')

@login_required
def recall_authorscience(request):
    try:
        authorscience_id = request.POST.get('authorscience_id', None)
        authorscience = get_object_or_404(AuthorScience,pk=authorscience_id)
        authorscience.delete()
    except Exception, e:
        return HttpResponseForbidden(e)
    return HttpResponse('RECALLED')
