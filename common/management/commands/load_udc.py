# -*- coding: utf-8 -*-
import cookielib
import logging
import lxml.etree
import socket
import sys
import urllib2
import os.path

from collections import namedtuple
from django.core.management.base import BaseCommand
from urlparse import urljoin
from journals.models import JournalCategorization, JournalRubric

logger = logging.getLogger("common.management.commands.load_udc")


class Command(BaseCommand):

    def handle(self, *args, **options):
        parser = UDCRubricParser(args[0])
        parser.parse()
        parser.save()


class URLLoadingError(Exception):
    pass


class UDCRubric(namedtuple("UDCRubric", "name, code, url, description, parent")):
    def __unicode__(self):
        return "%s %s" % (self.code, self.name)


class UDCRubricParser(object):

    def __init__(self, base_folder):
        self.cookiejar = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cookiejar))
        self.rubrics = []
        self.urls_cached = []
        self.urls_skipped = []
        self.base_folder = base_folder
        self.start_url = "http://teacode.com/online/udc/index.html"

    def get_text(self, node):
        return ''.join(node.itertext())

    def get_href(self, node, page_url):
        return urljoin(page_url, dict(node.items())['href'])

    def get_filename_by_key(self, key):
        return os.path.join(self.base_folder, key.replace("/", "_"))

    def get_cache(self, key):
        try:
            f = open(self.get_filename_by_key(key))
        except IOError:
            return None
        else:
            return f.read()
            f.close()

    def set_cache(self, key, value):
        f = open(self.get_filename_by_key(key), "w")
        f.write(value)
        f.close()

    def parse(self, page_url=None, parent=None):
        if not page_url:
            page_url = self.start_url

        # parse url and load xml tree
        try:
            tree = self.load_url(page_url)
        except URLLoadingError:
            self.urls_skipped.append(page_url)
            return

        rows = tree.xpath("//tr[@bgcolor='#eaeaea']")
        for row in rows:
            code_node, name_node, desc_node = row.xpath("td")
            code = self.get_text(code_node)
            if not code: # empty code, skip
                continue
            url_node = code_node.xpath("font/a")
            url = self.get_href(url_node[0], page_url) if url_node else ""
            name = self.get_text(name_node).replace("\r\n", " ")
            description = self.get_text(desc_node)
            rubric = UDCRubric(name, code, url, description, parent)
            self.rubrics.append(rubric)
            if url:
                self.parse(rubric.url, rubric)

    def load_url(self, url):
        '''Loads url and transforms it into an ElementTree, returning the tree.'''
        print len(self.rubrics), "Loading", url,
        tree_string = self.get_cache(url)
        if tree_string:
            print "CACHED"
            tree = lxml.etree.fromstring(tree_string)
        else:
            try:
                webpage = self.opener.open(url, timeout=10)
                tree = lxml.etree.parse(webpage, lxml.etree.HTMLParser(encoding='utf-8'))
            except (urllib2.URLError, urllib2.HTTPError, socket.timeout):
                logger.warning("Error when loading url %s: %s", url, sys.exc_info())
                print "ERROR"
                raise URLLoadingError
            except:
                logger.warning("Unexpected error when loading url %s: %s", url, sys.exc_info())
                print "ERROR"
                raise URLLoadingError
            self.set_cache(url, lxml.etree.tostring(tree))
            self.urls_cached.append(url)
            print "OK"
        return tree

    def save(self):
        categorization, _ = JournalCategorization.objects.get_or_create(name=u"УДК", code="UDC")
        created_count = 0
        updated_count = 0
        big_descriptions = []
        for i, rubric in enumerate(self.rubrics):
            rubric.journal_rubric, created = JournalRubric.objects.get_or_create(
                categorization=categorization,
                parent=getattr(rubric.parent, "journal_rubric", None),
                name=rubric.name,
                code=rubric.code
            )
            if created:
                created_count += 1
            updated = False
            if rubric.description and not rubric.journal_rubric.description:
                if len(rubric.description) > 2000: # 4000 produces Oracle error ORA-01461: can bind a LONG value only for insert into a LONG column, limit is 2273
                    big_descriptions.append(rubric)
                    rubric.journal_rubric.description = rubric.description[:2000]
                else:
                    rubric.journal_rubric.description = rubric.description
                updated = True
            if rubric.url and not rubric.journal_rubric.url:
                rubric.journal_rubric.url = rubric.url
                updated = True
            if updated:
                rubric.journal_rubric.save()
                updated_count += 1
            print i, rubric.code, rubric.name,
            if created:
                print "CREATED",
            if updated:
                print "UPDATED",
            print ""
        print created_count, u"рубрик создано"
        print updated_count, u"рубрик обновлено"
        print "Big descriptions:", u"\n".join([r.code for r in big_descriptions])
