# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from optparse import make_option
from django.contrib.auth.models import User
from django.template.loader import render_to_string

class Command(BaseCommand):

    def handle(self, *args, **options):
        username = args[0]
        print get_user_email_template(username)


def get_user_email_template(username):
    user = User.objects.get(username=username)
    feedbacks = user.feedback_messages.filter(seen=False).order_by('date')
    profile = user.get_profile()
    name = "%s %s" % (profile.firstname.strip(), profile.middlename.strip())
    return render_to_string("user_email.txt", {'email': user.email, 'name': name, 'feedbacks': feedbacks})