#!/usr/bin/env python
"""Update personal rating cache, stored in RATINGCACHE table.
"""

from datetime import datetime
from json import loads
from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db import connections
from django.utils import translation

afonin = User.objects.get(username='safonin')

from apps.pmodel.models import PointsFormula, PointsFormulaDepartments, RatingCache
from apps.pmodel.ruleprocessor import process_rules
from apps.workers.models import Worker

verbosity = None

def log(string, my_verbosity):
    if verbosity >= my_verbosity:
        print datetime.now(), string

def create_cache_item(formula, man_id, value):
    log("  Worker %d: setting rating to %r" % (man_id, value), 2)

    cache_item = RatingCache.objects.create(
        formula=formula,
        worker=Worker.objects.get(pk=man_id),
        value=value,
        from_year=formula.begin,
        to_year=formula.end)
    cache_item.save()

def update_cache_for_formula(formula):
    log("Processing formula %d" % formula.id, 1)

    RatingCache.objects.filter(formula=formula).delete()

    pfdeps = PointsFormulaDepartments.objects.filter(formula=formula)
    department_ids = [pfdep.department.id for pfdep in pfdeps]

    if not department_ids:
        log("  Formula is not published, skipping", 2)
        return

    rules = loads(formula.jsondata)

    cursor = connections['reports_server'].cursor()

    sql_clear = """DECLARE
     j varchar2(255);
    BEGIN
     j:=pack_temp_points.%(tablename)s.FIRST;
     WHILE j IS NOT NULL LOOP
      pack_temp_points.%(tablename)s(j).DELETE;
      j:=pack_temp_points.%(tablename)s.NEXT(j);
     END LOOP;
    END;/"""
    cursor.execute(sql_clear % {'tablename': 'tempTs'})
    cursor.execute(sql_clear % {'tablename': 'tempTs_men'})

    data, _, _ = process_rules(rules,
                   start_year=formula.begin or 2010,
                   end_year=formula.end or 2014,
                   department_ids=department_ids,
                   user=afonin)

    for row in data:
        create_cache_item(formula, row[0], row[4])

    log("  Added %d records" % len(data), 1)

class Command(BaseCommand):
    help = __doc__

    def handle(self, *args, **options):
        translation.activate(settings.LANGUAGE_CODE)
        global verbosity
        verbosity = int(options["verbosity"])
        settings.DEBUG = (verbosity >= 3)

        for formula in PointsFormula.objects.all():
            update_cache_for_formula(formula)
