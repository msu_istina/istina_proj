# -*- coding: utf-8 -*-
"""Update activitities descriptions (snippets) cache.

By default, existing snippets will not be recalculated.
Use --force options to change this.
"""

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import translation
from optparse import make_option

from common.utils.cutoff import CutoffTimer
from common.templatetags.activity_description import KNOWNN_MODELS, recalculate_activity_description


class Command(BaseCommand):
    help = __doc__
    option_list = BaseCommand.option_list + (
        make_option('--force',
            action='store_true',
            dest='force',
            default=False,
            help='Update existing cache entries as well'),
        )

    def handle(self, *args, **options):
        # by default, django sets current lanugage to en-us so we have to redefine it
        translation.activate(settings.LANGUAGE_CODE)

        options["verbosity"] = int(options["verbosity"])
        if options["verbosity"] >= 1:
            timer = CutoffTimer(None)

        recalc_count = found_count = 0

        for model in sorted(KNOWNN_MODELS, key=lambda x: x.__name__):
            if options["verbosity"] >= 1:
                count = model.objects.count()
                print "Working on %s" % model.__name__
                print " Objects count: %d" % count
            for i, obj in enumerate(model.objects.all()):
                is_recalculated = False
                for no_links in (True, False):
                    is_recalculated_local = recalculate_activity_description(obj, no_links, force=options["force"], no_fetch=True)[1]
                    is_recalculated = is_recalculated_local or is_recalculated

                if options["verbosity"] >= 1:
                    if is_recalculated:
                        timer.cutoff(model.__name__)
                        recalc_count += 1
                    else:
                        timer.cutoff("cache_hit")
                        found_count += 1

                if options["verbosity"] >= 2:
                    status_str = "Recalc" if is_recalculated else "Found"
                    print "   %3d/%d %10s %s" % (i + 1, count, status_str, obj)

        if options["verbosity"] >= 1:
            fmt = "{:<40}: {}"
            print
            print "TIMES:"
            for action, (time, count) in sorted(timer):
                print fmt.format(action, time)
            print "----------"
            total_time = sum(i[1][0] for i in timer)
            found_time = timer["cache_hit"][0]
            recalc_time = total_time - found_time
            print fmt.format("Total", total_time)
            print fmt.format("Objects recalc", recalc_count)
            print fmt.format("Objects found", found_count)

            def zero_check(exp):
                try:
                    return exp()
                except ZeroDivisionError:
                    return "???"

            print fmt.format("Time, sec/100 recalc objects",
                            zero_check(lambda: recalc_time * 100 / recalc_count))
            print fmt.format("Time, sec/100 found objects",
                            zero_check(lambda: found_time * 100 / found_count))
