# -*- coding: utf-8 -*-
"""Update last activities list on the startpage cache"""
from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils import translation

from common.templatetags import startpage_last_activities


class Command(BaseCommand):
    help = __doc__

    def handle(self, *args, **options):
        # by default, django sets current lanugage to en-us so we have to redefine it
        translation.activate(settings.LANGUAGE_CODE)
        startpage_last_activities.recalculate_startpage_last_activities()
