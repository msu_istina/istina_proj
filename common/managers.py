# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import logging

from django.db import models
from django.db.models.loading import get_model
from collections import Counter
from common.utils.cutoff import CutoffTimer
from common.utils.idifflib import get_close_matches
from reversion import default_revision_manager
from common.utils.models_related import get_related_objects, get_linked_to_instance_objects
import re

class MyQuerySet(models.query.QuerySet):
    def update(self, **kwargs):
        objects_to_update = list(self)
        super(MyQuerySet, self).update(**kwargs)
        default_revision_manager.save_revision(objects_to_update, comment=u'Результат функциии update')

    def get_first_or_none(self):
        if not self:
            return None
        else:
            return self[0]



class MyManager(models.Manager):
    '''Manager containing some common methods.'''

    def get_query_set(self):
        return MyQuerySet(self.model, using=self._db)

    def update(self, *args, **kwargs):
        objects_to_update = list(self.get_query_set())
        super(MyManager, self).update(self, *args, **kwargs)
        default_revision_manager.save_revision(objects_to_update, comment=u'Результат функциии update')

    def non_empty_dated(self):
        return self._non_empty_dated(self.get_query_set(), self.model.ordering_field())

    @staticmethod
    def _non_empty_dated(queryset, ordering_field, order_by=False):
        '''Returns all objects excluding those having empty date attribute.
        The date attribute is determined as the first attribute in the Meta 'ordering' option.'''
        if ordering_field:
            no_dashed_ordering_field = ordering_field if ordering_field[0] != '-' else ordering_field[1:] # without leading dash
            params = {no_dashed_ordering_field + '__isnull': True}
            qs = queryset.exclude(**params)
            if order_by: # extra ordering by the ordering_field
                qs = qs.order_by(ordering_field)
            return qs
        else:
            return queryset.all()

    def delete_unlinked_objects(self, delete_condition=None):
        if not delete_condition:
            delete_condition = lambda x: not get_related_objects(x) and not get_linked_to_instance_objects(x)
        queryset = self.get_query_set()
        deleted_num = 0
        for obj in queryset:
            if delete_condition(obj):
                obj.delete()
                deleted_num += 1
        return deleted_num

    def recent(self, limit=3, recent_type="date"):
        '''Returns @limit recent objects with non-empty date.'''
        if recent_type == "date":
            return self.non_empty_dated()[:limit]
        else:
            return self.recent_by_id()

    @classmethod
    def recent_qs(cls, queryset, ordering_field, order_by=True, limit=3):
        return cls._non_empty_dated(queryset, ordering_field, order_by)[:limit]

    def recent_by_id(self, limit=5):
        return self.order_by('-id')[:limit]

    @classmethod
    def recent_by_id_qs(cls, queryset, limit=5):
        return queryset.order_by('-id')[:limit]

    def get_objects_by_id_string(self, id_string):
        ids = id_string.split(",")
        ids = filter(bool, ids)
        return self.filter(pk__in=ids)

    def get_datatable_json(self, instances):
        data_list = [instance.datatable_merge_json() for instance in instances]
        return {'aaData': data_list}

    def getdefault(self, default=None, *args, **kwargs):
        try:
            return self.get(*args, **kwargs)
        except self.model.DoesNotExist:
            return default

    def strip_fields(self, fields, queryset=None):
        if queryset is None:
            queryset = self.all()
        updated_count = 0
        for obj in queryset:
            to_update = False
            for field in fields:
                if getattr(obj, field).strip() != getattr(obj, field):
                    setattr(obj, field, getattr(obj, field).strip())
                    to_update = True
            if to_update:
                obj.save()
                updated_count += 1
        print updated_count, "objects updated"

def strip_stopwords(word, stopwords):
    if not stopwords: return word
    stop_regex = '(' + '|'.join(stopwords) + ')\S*\s*'
    return re.sub(stop_regex, '', word, flags=re.IGNORECASE)

def get_similar_canidates_from_qset(qset, field_name, field_value, N=3, stopwords=[]):
    """Fetch items from qset which field_name is similar to field_value

    Similarity is understood in terms if get_similar_canidates

    This function is a generator

    The key argument plays the same role as in sort()

    returns a list of tuples(obj_id, field_value)

    NB: it omits words with length < 4 chars. So e.g. u"Курс про всё" will not find any similar, even if there is a course with exactly the same name.
    """
    field_value = strip_stopwords(field_value, stopwords)
    test_words = filter(lambda x: len(x) > 3, field_value.lower().split())[:N]
    test_q = None
    if not test_words:
        return
    for word in test_words:
        word_q = models.Q(**{"{0}__icontains".format(field_name): word})
        if test_q is None:
            test_q = word_q
        else:
            test_q = test_q | word_q
    if test_q is not None:
        qset = qset.filter(test_q)
    qset = qset.values_list("pk", field_name)
    similar_values = list(qset)
    for i in get_similar_canidates(field_value, similar_values, N=N, key=lambda x: x[1], stopwords=stopwords):
        yield i


def get_similar_canidates(field_value, similar_values, key=lambda x: x, N=3, stopwords=[]):
    """Get only these similar values that contains any of first N words from field_value

    This function is a generator

    The key argument plays the same role as in sort()
    """
    field_value = strip_stopwords(field_value, stopwords)
    test_words = frozenset(filter(lambda x: len(x) > 3, field_value.lower().split())[:N])
    if not test_words:
        return
    for value in similar_values:
        for word in key(value).lower().split():
            if word in test_words:
                yield value
                break


class SimilarFieldManager(MyManager):

    def get_similar_by_field(self, field_name, field_value, cutoff=0.6, n=10, qs=None, sort=False, stopwords=[]):
        '''Returns queryset for objects with similar field from queryset

        sort
            if True target objects will be sorted by similarrity

        returns
            queryset if sort=False
            list if sort=True
        '''
        timer = CutoffTimer(None)
        stopwords = map(lambda x: x.lower(), stopwords)
        if qs is None:
            qs = self.all()

        similar_values = get_similar_canidates_from_qset(qs, field_name, field_value, stopwords=stopwords)
        similar_values = list(similar_values)
        logging.info("similar candidates (%s): %d", self.__class__.__name__, len(similar_values))

        similar_values = get_close_matches(field_value.lower(),
                                similar_values,
                                cutoff=cutoff,
                                n=n,
                                key=lambda x: x[1].lower(), stopwords=stopwords)
        similar_values = map(lambda x: x[0], similar_values)
        logging.info("similar values (%s): %d", self.__class__.__name__, len(similar_values))
        timer.cutoff("get_close_matches")

        similar_values_ids = [x[0] for x in similar_values]
        similar_objects = qs.filter(pk__in=similar_values_ids)
        logging.info("similar objects (%s): %d", self.__class__.__name__, len(similar_objects))
        list(similar_objects)
        timer.cutoff("fetch_similar")

        if sort:
            def weight(obj):
                field_value = getattr(obj, field_name)
                try:
                    return similar_values.index(field_value)
                except ValueError:
                    return len(similar_values)
            similar_objects = sorted(similar_objects, key=weight)

        logging.getLogger("benchmark.get_similar_by_field").info("Timer: %s", timer)
        return similar_objects


class ChoiceListOptionManager(MyManager):

    def get_for_list(self, list_code):
        return self.filter(list_code=list_code).values_list('code', 'name')


class ObjectListManager(MyManager):

    def merge_duplicates(self):
        values = list(self.all().values_list('id', 'name', 'category', 'code', 'user', 'department'))
        for list1 in values:
            for list2 in values:
                if list1[0] < list2[0] and list1[1:] == list2[1:]:
                    self.merge(list2[0], list1[0])

    def merge(self, from_id, to_id):
        list_from = self.get(id=from_id)
        list_to = self.get(id=to_id)
        if (list_from.status == "CONFIRM" and list_to.status != "CONFIRM") or (list_from.status == "SIGN" and list_to.status in ["EDIT", "REEDIT"]):
            list_tmp = list_to
            list_to = list_from
            list_from = list_tmp
        if (list_from.name, list_from.category, list_from.code, list_from.user, list_from.department) != (list_to.name, list_to.category, list_to.code, list_to.user, list_to.department):
            raise
        print "Merge list %d to %d" %(list_from.id, list_to.id)
        for event in list_from.statuslog_events.all():
            event.object_list = list_to
            event.save()
        list_from_items = set(list_from.items.values_list('content_type', 'object_id'))
        list_to_items = set(list_to.items.values_list('content_type', 'object_id'))
        for new_item in list_from_items - list_to_items:
            for item in list_from.items.filter(object_id=new_item[1]):
                item.object_list = list_to
                item.save()
        list_from.delete()

    def delete_obsolete_by_department(self, really_delete=False):
        Employment = get_model("workers", "Employment")
        statuses = []
        lists_to_delete = []
        for lst in self.filter(user__profile__worker__isnull=False, department__isnull=False).values('id', 'user__profile__worker', 'department', 'status'):
            worker_id = lst['user__profile__worker']
            department_id = lst['department']
            status = lst['status']
            if not Employment.objects.filter(worker=worker_id, department=department_id).exists():
                # it's the object list for department, but the worker does not have any employments in that department (current or past)
                print worker_id
                statuses.append(status)
                lists_to_delete.append(lst['id'])
        cnt = Counter(statuses)
        print cnt.items()
        print "Number of lists to delete: ", len(lists_to_delete)
        if really_delete:
            ObjectListChangeStatusEvent = get_model("common", "ObjectListChangeStatusEvent")
            ObjectListItem = get_model("common", "ObjectListItem")
            ObjectListChangeStatusEvent.objects.filter(object_list__in=lists_to_delete).delete()
            ObjectListItem.objects.filter(object_list__in=lists_to_delete).delete()
            self.filter(id__in=lists_to_delete).delete()

    def signed_reports(self, year=None):
        qs = self.filter(status__in=["SIGN", "CONFIRMED"], category=u"Отчеты", name=u"Годовой отчет")
        if year:
            qs = qs.filter(code=year)
        return qs
