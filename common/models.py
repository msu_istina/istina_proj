# -*- coding: utf-8 -*-
import logging
import sys
import re
from functools import partial

from django.db import models
from django.core import serializers
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.template.defaultfilters import truncatechars
from django.template.loader import render_to_string
from django.contrib.sites.models import Site
from actstream.models import Action
from datetime import datetime

from common.managers import MyManager, ChoiceListOptionManager, ObjectListManager
from common.utils.user import get_fullname, mail_users
from common.utils.cache import workership_changed
from common.utils.compare import convert_to_datetime
from common.utils.files import get_attachment_file_path, validate_uploaded_file
from workers.utils import get_worker
from publications.utils import split_authors
from organizations.utils import is_representative_for_user, is_representative_for_any_user
from common.utils.reversion_utils import my_create_revision
from common.utils.datatables import data_list_to_datatables
from common.utils.models_related import has_related_objects as _has_related_objects, get_related_objects

logger = logging.getLogger("common.models")


class MyModel(models.Model):
    '''A common class for all models.'''

    objects = MyManager()  # inherited by all subclasses

    search_attr = 'title'  # base attribute for searching, used in autocomplete search
    attachments = generic.GenericRelation('common.Attachment')

    class Meta:
        abstract = True

    @classmethod
    def ordering_field(cls):
        '''Returns the first attribute in the Meta 'ordering' option.'''
        ordering = cls._meta.ordering
        if len(ordering) == 0:  # ordering option is not set
            return None
        field = ordering[0]
        if field.startswith('-'):
            field = field[1:]
        return field

    @classmethod
    def related_fields(cls):
        '''Returns list of tuples (linking_related_name, linking_field_name).
        For example, if JournalAlias has a field 'journal' that is a ForeignKey
        to Journal model with related_name = 'aliases', then this method returns
        >>> ('aliases', 'journal') in Journal.related_fields()
        >>> True
        '''
        return get_related_fields(cls)

    def get_full_url(self):
        return 'http://%s%s' % (Site.objects.get_current().domain, self.get_absolute_url())

    @property
    def get_year(self):
        # it is not named 'year' because in Article class .get_year yields
        # correct year of an article, and .year yields only year of the article
        # in journal, not in a collection.
        '''Return year of the ordering_field, if applicable.'''
        date_field = self.ordering_field()
        if date_field:
            obj = self
            for attr in date_field.split("__"):
                try:
                    obj = getattr(obj, attr)
                except AttributeError:
                    return None
            if obj:
                date = convert_to_datetime(obj)
                if date:
                    return date.year
        return None

    def check_year_range(self, years):
        '''Returns True if the object's year is among a list of given years.
        If object has startdate and enddate, check if the ranges intersect.
        @years are supposed to be a sorted list of years, e.g. [2010,2011,2015].'''
        if hasattr(self, 'startdate') and hasattr(self, 'enddate'):
            if not self.startdate and not self.enddate:
                return False
            elif (not self.startdate or self.startdate.year <= years[-1]) and (not self.enddate or self.enddate.year >= years[0]):
                return True
            else:
                return False
        if hasattr(self, 'startyear'):
            endyear = getattr(self, 'endyear', None)
            if self.startyear <= years[-1] and (not endyear or endyear >= years[0]):
                return True
            else:
                return False
        year = self.get_year
        return bool(year and year in years)

    def get_default_case(self):
        return getattr(self._meta, 'verbose_name', None)

    @property
    def nominative(self):
        return self.get_default_case()

    @property
    def nominative_short(self):
        return self.get_default_case()

    @property
    def nominative_en(self):
        # must be overriden e.g. for correct adding workers (add_workers method)
        # can not rely on default_case (verbose name), since it is usually in russian
        return None

    @property
    def nominative_plural(self):
        u'''Used with count=21,
        e.g. "21 публикация, членство в редколлегиях журналов".'''
        return self.nominative

    @property
    def genitive(self):
        return self.get_default_case()

    @property
    def genitive_short(self):
        return self.genitive

    @property
    def genitive_plural(self):
        u'''Used with count=2.
        The is essentially a single genitive, e.g. "(2) публикации, патента",
        but if the name is composite ("членство в редколлегии"),
        then genitive_plural combines single genitive of the main word ((2)"членства")
        and plural of the remainder ("в редколлегиях").'''
        return self.genitive

    @property
    def genitive_plural_full(self):
        u'''Used with count=5.
        The is a pure genitive case of the plural form, e.g. "(5) публикаций, патентов".'''
        return getattr(self._meta, 'verbose_name_plural', None) or self.get_default_case()

    @property
    def dative(self):
        return self.get_default_case()

    @property
    def accusative(self):
        return self.get_default_case()

    @property
    def accusative_short(self):
        return self.get_default_case()

    @property
    def instrumental(self):
        return self.get_default_case()

    @property
    def locative(self):
        return self.get_default_case()

    @property
    def gender_suffix(self):
        gender = getattr(self, 'gender', None)
        if gender == 'feminine':
            return u"а"  # добавлен(а)
        if gender == 'neuter':
            return u"о"
        if gender == 'plural':
            return u"ы"
        return ""  # masculine or not set

    @property
    def gender_adjective_end(self):
        gender = getattr(self, 'gender', None)
        if gender == 'feminine':
            return u"ая"  # новая
        if gender == 'neuter':
            return u"ое"  # новое
        if gender == 'plural':
            return u"ые"  # новые
        return u"ый"  # masculine or not set

    @property
    def gender_possessive_genitive(self):
        gender = getattr(self, 'gender', None)
        if gender == 'feminine':
            return u"её"
        if gender == 'plural':
            return u"их"
        return u"его"  # masculine, neuter or not set

    @property
    def gender_possessive_dative(self):
        gender = getattr(self, 'gender', None)
        if gender == 'feminine':
            return u"ей"
        if gender == 'plural':
            return u"им"
        return u"ему"  # masculine, neuter or not set

    @property
    def gender_possessive_end(self):
        gender = getattr(self, 'gender', None)
        if gender == 'feminine':
            return u"а"  # ваша
        if gender == 'neuter':
            return u"е"  # ваше
        if gender == 'plural':
            return u"и"  # ваши
        return u""  # masculine or not set

    def is_created_by(self, user):
        '''Returns True if the user is the creator of the object.'''
        return hasattr(self, 'creator') and user == self.creator

    def is_creator_managed_by(self, user):
        '''Returns True if the user is a manager (e.g. moderator, representative)
        of the creator of the object.'''
        return hasattr(self, 'creator') and is_representative_for_user(user, self.creator)

    def is_probably_created_by(self, user):
        '''Returns True if the object does not have the creator field AND the user
        is the only user linked to the object. In this case he is considered creator.'''
        return not hasattr(self, 'creator') and [user] == self.linked_users

    def is_probably_creator_managed_by(self, user):
        '''Returns True if the user is a manager (e.g. moderator, representative)
        of the probable creator of the object.'''
        if not hasattr(self, 'creator'):
            linked_users = self.linked_users
            return len(linked_users) == 1 and is_representative_for_user(user, linked_users[0])
        else:
            return False

    @property
    def linked_users(self):
        '''Returns a list of linked users to the object,
        i.e. linked to worker(s) that are linked to the object.'''
        users = []
        if hasattr(self, 'workers'):
            users_ids = self.workers.values_list('profile__user', flat=True)
            users.extend(User.objects.filter(id__in=users_ids))
        if hasattr(self, 'worker') and hasattr(self.worker, 'user'):
            users.append(self.worker.user)
        return users

    def is_linked_to(self, user):
        '''Returns True if the user is linked to the object,
        i.e. user is linked to the worker linked to the object.
        User is one of the linked workers or user is *the* linked worker.'''
        return self.id and user.is_authenticated() and user in self.linked_users

    def is_linked_managed_by(self, user):
        '''Returns True if the user is a manager (e.g. moderator, representative)
        of one of the linked users to the object.'''
        return is_representative_for_any_user(user, self.linked_users)

    @property
    def is_locked(self):
        return False

    def get_similar_workership_to_worker(self, worker, workerships=None):
        return None

    def link_to(self, user, workerships = None):
        '''Link the object to user, if it is allowed.'''
        worker = get_worker(user)
        workership = self.get_similar_workership_to_worker(worker, workerships)
        if workership:
            old_worker = workership.worker
            alias, alias_created = workership.make_alias(worker)
            setattr(workership, workership.worker_attr, worker)
            workership.save()
            return workership, old_worker, alias, alias_created
        return None, None, None, False

    def get_xml(self):
        '''Return xml representation of the object.'''
        xml = serializers.serialize("xml", [self, ])
        if hasattr(self, 'xml'):
            # strip self.xml from serialized xml, uses lookbehind assertion
            xml = re.sub(r'(?<=<field type="TextField" name="xml">)[^<]*', '', xml, flags=re.DOTALL)
        return xml

    @my_create_revision
    def save(self, force_insert=False, force_update=False, using=None):
        super(MyModel, self).save(force_insert, force_update, using)

    @my_create_revision
    def delete(self, using=None):
        super(MyModel, self).delete(using)

    @property
    def content_type(self):
        '''Get ContentType objects corresponding to the current (derived) model.'''
        return ContentType.objects.get_for_model(self.__class__)

    @property
    def actions(self):
        '''Return queryset of actions (via actstream) performed on this object.
        The objects must be a target or action_object of an action.
        '''
        content_type = self.content_type
        actions = Action.objects.filter(
            Q(target_object_id=self.id, target_content_type=content_type) |
            Q(action_object_object_id=self.id, action_object_content_type=content_type)).order_by('-timestamp')

        # fix actions' verbs to make the gender-independent
        verbs = [u"добавил", u"изменил", u"объединил",
                 u"отменил", u"отредактировал", u"очистил",
                 u"подтвердил", u"поставил", u"привязал",
                 u"разобрал", u"связал", u"удалил"]
        complex_verbs = [(u"доволен", u"доволен(-ьна)"),
                         (u"отказался", u"отказался(-ась)")]
        for action in actions:
            for verb in verbs:
                if verb in action.verb:
                    action.verb = action.verb.replace(verb, verb + u"(а)")
            for male_verb, neutral_verb in complex_verbs:
                if male_verb in action.verb:
                    action.verb = action.verb.replace(male_verb, neutral_verb)
        return actions

    def get_attachments_by_filename(self, filename):
        '''Returns attachments with a specified filename.'''
        return [attachment for attachment in self.attachments.all() if attachment.filename == filename]

    def get_absolute_url_html(self):
        return "<a href=" + self.get_absolute_url() + " target='_blank' >" + str(self.id) + "</a>"

    def notify_new_workers(self, workers, user, old_workers=None, old_workerships=None):
        """
        Determine new workers that are to be linked to the object.
        And send them notification emails, so that they can easily unauthor the object.
        This function is called when an object is edited, before existing memberships are deleted,
        or when an object is added.
        This method is added to MyModel, because it is used both for LinkedToWorkersModel and for WorkershipModel.
        Probably it should be in a new middle class, a common ancestor of those two.
        @workers:
            list of workers objects to be added.
        @user:
            the user adding the object.
        @old_workers:
            If specified, old workers are taken from it.
            This parameter is useful, when this function is called for Workership class.
        @old_workerships:
            If specified, old workers are taken from it.
            This parameter is useful, when you want to add workers
            through another relation, like Opponentship for dissertations.
        """
        if old_workers is not None:
            existing_users_ids = [worker.user.id for worker in old_workers if worker and worker.user]
        elif old_workerships is not None:
            existing_users_ids = [workership.worker.user.id for workership in old_workerships if workership and workership.worker and workership.worker.user]
        else:
            existing_users_ids = self.workers.values_list('profile__user', flat=True)
        new_users_ids = [worker.user.id for worker in workers if worker and worker.user]
        users_ids_to_notify = set(new_users_ids) - set(existing_users_ids) - set([user.id])
        users = list(User.objects.filter(id__in=users_ids_to_notify))
        if users:
            subject = u'В ваш профиль добавлен%s нов%s %s "%s" | ИСТИНА' % (
                self.gender_suffix,
                self.gender_adjective_end,
                self.nominative,
                truncatechars(self.title, 50)
            )
            creator_fullname = get_fullname(user)
            if creator_fullname:
                creator_name = creator_fullname + " (%s)" % user.username
            else:
                creator_name = user.username
            extra_context = {
                'work': self,
                'creator_name': creator_name
            }
            mail_users(users, subject,
                       template_filename="workers/emails/new_work.txt",
                       extra_context=extra_context,
                       really_send=True, log_enabled=False, sleep_enabled=False,
                       action_target=self, action_object=user)

    @property
    def has_related_objects(self):
        return _has_related_objects(self)

    @property
    def related_objects(self):
        return get_related_objects(self)

    @classmethod
    def get_choices(cls, list_code):
        return ChoiceListOption.objects.get_for_list(list_code)


class LinkedToWorkersModel(MyModel):
    '''An object linked to workers through an intermediate model.
    Used instead of AuthoredModel when workers field is not 'authors' (e.g. 'advisers').
    '''

    workers_attr = 'workers'
    workerships_attr = 'workerships'  # attribute pointing to intermediate model instances
    workers_verbose_name_single = u"Сотрудник"
    workers_verbose_name_plural = u"Сотрудники"
    workers_required_error_msg = u"Укажите хотя бы одного сотрудника"
    is_LinkedToWorkersModel = True

    class Meta:
        abstract = True

    @property
    def workers(self):
        return getattr(self, self.workers_attr)

    @property
    def workerships(self):
        return getattr(self, self.workerships_attr)

    def _workers_list(self, workerships=None, initials=True):
        '''Returns a list of workers linked to the object
        together with their originally entered names, ordered by the original position.'''
        workers = []
        if workerships is None:
            workerships = self.workerships.all()
        workers_cache = {obj.id: obj for obj in self.workers.all()}
        if workerships:
            position_exists = hasattr(workerships[0], "position")
            for workership in workerships:
                try:
                    worker = workers_cache[workership.get_worker_id()]
                except KeyError:
                    # In most cases this means that the author isn't linked to any worker.
                    worker = workership.worker
                worker = {'worker': worker}
                if hasattr(workership, "original_name") and workership.original_name:
                    worker['name'] = workership.original_name
                elif worker['worker']:
                    worker['name'] = worker['worker'].get_fullname(initials=initials)
                else:
                    worker['name'] = ""
                if position_exists:
                    worker['position'] = workership.position
                workers.append(worker)
        return workers

    workers_list = property(_workers_list)

    def get_workers_string(self, exclude_workers=None):
        if exclude_workers is None: # ugly? but it's the only right way, see http://stackoverflow.com/a/366430/304209
            exclude_workers = []
        return ", ".join(worker['name'] for worker in self.workers_list if not worker['worker'] in exclude_workers)

    workers_string = property(get_workers_string)

    @property
    def workers_string_short(self):
        limit = 10
        workers = self.workers_list
        et_al = ", et al" if len(workers) > limit else ""
        return ", ".join(worker['name'] for worker in workers[:limit]) + et_al

    @property
    def all_workers_string(self):
        '''
        This property is used for presentational purposes to display a string of all workers linked to the object.
        Mainly, in activities_last_added template.

        This property is essentially the same as self.workers_string, but it can be reinitialized in subclasses.
        The goal is to show all workers linked to the object, even through multiple attributes.
        An example is dissertations, which are linked to workers through advisers and author.
        self.workers_string would display only advisers, and this property would display all of them together with author.

        Why not just modify workers_string property?
        Because it is used in *editing* objects when initializing a workers field in form
        (see common.forms). In the case of dissertations we want this field to be initialized only with advisers,
        and not author (author is shown in separate field).

        all_workers_string is used for presentational purposes only.
        '''
        return self.workers_string

    @property
    def workers_names(self):
        '''Return a list of dicts with all linked workers's names (including their aliases).
        If a linked workership is not linked to a worker, make name dict from its original name and add it to the list.'''
        names = []
        for workership in self.workerships.all():
            names.extend(workership.workers_names)
        return names

    def add_workers(self, workers, user=None, workership_class=None, **kwargs):
        """
        Create workerships with given workers.
        Set model_instance.field (m2m field) to workers by means of through class.
        This base function is called when every LinkedToWorkersModel is saved from forms.
        If any workerships already exist, they should be deleted before calling this function.
        @workers:
            list of workers objects to be added.
        @user:
            the user adding the object.
        @workership_class:
            If not specified, the default workership class is taken,
            which is derived from self.workers_attr (e.g. advisers).
            This parameter is useful, when you want to add workers
            through another relation, like Opponentship for dissertations.
        @kwargs:
            used to instantiate workership objects.
        """
        if not workership_class:
            workership_class = self.workers.through
        for worker in workers:
            workership_kwargs = kwargs.copy()
            workership_kwargs[self.nominative_en] = self
            if worker.id is not None:  # if not UnknownWorker
                workership_kwargs[workership_class.worker_attr] = worker
            if user and hasattr(workership_class, 'creator'):
                workership_kwargs['creator'] = user
            workership = workership_class(**workership_kwargs)
            if hasattr(workership, 'position'):
                workership.position = worker.position
            if hasattr(workership, 'original_name'):
                workership.original_name = getattr(worker, "fullname_orig", worker.fullname_short)
            workership.save()

    @property
    def workers_verbose_name(self):
        return self.workers_verbose_name_plural if self.workers.count() > 1 else self.workers_verbose_name_single

    def change_worker(self, man_from, man_to, limit=None, workerships_manager=None):
        '''Relink the object from man_from to man_to.
        Man_{from,to} can be either an id of a worker or a worker instance or a username or a user instance.
        If a limit is set, change max limit links.
        '''
        worker_from = get_worker(man_from)
        worker_to = get_worker(man_to)
        if not worker_from or not worker_to:
            return
        if not workerships_manager:
            workerships_manager = self.workerships
        workerships_model = workerships_manager.model
        worker_attr = workerships_model.worker_attr
        workerships = workerships_manager.filter(**{worker_attr: worker_from})
        if not workerships:
            logger.debug("Worker %s is not linked to the object.", worker_from)
            return
        if limit:
            workerships = workerships[:limit]
        for workership in workerships:
            setattr(workership, worker_attr, worker_to)
            workership.save()
        logger.debug("Args: %s (id %d), %s, %s%s", self.__class__, self.id, man_from, man_to, ', limit=%s' % limit if limit else '')
        logger.debug("Successfully relinked %d links from worker %s (id %d) to worker %s (id %d)",
                     len(workerships), worker_from, worker_from.id, worker_to, worker_to.id)

    @classmethod
    def get_workership_model(cls):
        '''Returns workership model object.'''
        # do not try to switch to cls.workerships, it won't work on class objects
        return getattr(cls, cls.workerships_attr).related.model

    def get_similar_workership_to_worker(self, worker, workerships = None):
        if workerships is None:
            worker_attr = self.get_workership_model().worker_attr
            workerships = self.workerships
        else:
            worker_attr = workerships.model.worker_attr

        no_profile_option = {'{0}__profile__isnull'.format(worker_attr): True}
        return worker.get_similar_workership(workerships.filter(**no_profile_option)) \
            if worker else None

    def unauthor(self, user, workerships = None):
        if workerships is None:
            worker_attr = self.get_workership_model().worker_attr
            filter_dict = {'{0}__profile__user'.format(worker_attr): user}
            workerships = self.workerships.filter(**filter_dict)
        else:
            worker_attr = workerships.model.worker_attr

        for wrkrshp in workerships:
            if not wrkrshp.original_name:
                wrkrshp.original_name = getattr(wrkrshp, worker_attr).fullname_short
            setattr(wrkrshp, worker_attr, None)
            wrkrshp.save()


class AuthoredModel(LinkedToWorkersModel):
    '''An object having authors.'''

    workers_attr = 'authors'
    workerships_attr = 'authorships'  # attribute pointing to intermediate model instances
    workers_verbose_name_single = u"Автор"
    workers_verbose_name_plural = u"Авторы"
    workers_required_error_msg = u"Укажите хотя бы одного автора"

    class Meta:
        abstract = True

    @property
    def authors_list(self):
        return self.workers_list

    @property
    def authors_string(self):
        return self.workers_string

    @property
    def authors_string_short(self):
        return self.workers_string_short

    def add_authors(self, workers, user=None):
        '''Create authorships with given workers. @user is the user adding the object.'''
        self.add_workers(workers, user)

    @property
    def authorscience(self):
        authors = AuthorScience.objects.filter(Q(sciencework__id=self.id, flag_validate=1)|Q(sciencework__id=self.id,flag_validate__isnull=True))
        authorsdict = {}
        for author in authors:
            if author.authorrole.name not in authorsdict.keys():
                authorsdict[author.authorrole.name] = []
            authorsdict[author.authorrole.name].append(author)
        return authorsdict



class WorkershipModel(MyModel):
    '''Intermediate model connecting some object model and Worker model.
    Note that this model can be used directly for adding objects,
    e.g. DissertationCouncilMembership.
    '''

    worker_attr = 'worker'
    workers_verbose_name_single = u"Сотрудник"
    workers_required_error_msg = u"Укажите сотрудника"
    is_workership = True  # FIXME temporary attribute

    class Meta:
        abstract = True

    @property
    def worker(self):
        return getattr(self, self.worker_attr)

    def get_worker_id(self):
        return getattr(self, "{0}_id".format(self.worker_attr))

    def _workers_string(self, initials=True):
        if self.worker:
            return self.worker.get_fullname(initials=initials)
        elif hasattr(self, 'original_name'):
            return self.original_name
        else:
            return ''

    workers_string = property(_workers_string)
    workers_string_full = property(partial(_workers_string, initials=False))

    @property
    def all_workers_string(self):
        '''See description of LinkedToWorkersModel.all_workers_string.'''
        return self.workers_string

    @property
    def workers_names(self):
        '''Return a list of dicts with the linked worker's names (including his aliases).
        If it is not linked to a worker, make name dict from its original name and add it to the list.
        Also add link to self to each name.'''
        names = []
        if self.worker:
            names = self.worker.names
            for name in names:
                name['workership'] = self
            return names
        elif hasattr(self, "original_name"):
            names = split_authors(self.original_name)
            if names:
                return [{'workership': self, 'lastname': names[0][0], 'firstname': names[0][1], 'middlename': names[0][2]}]
        return []

    @property
    def workers_verbose_name(self):
        return self.workers_verbose_name_single

    def change_worker(self, man_to):
        '''Relink the object to man_to.
        Man_to can be either an id of a worker or a worker instance or a username or a user instance.
        '''
        worker_from = unicode(self.worker)
        worker_from_id = self.worker.id
        worker_to = get_worker(man_to)
        if not worker_to:
            return
        setattr(self, self.worker_attr, worker_to)
        self.save()
        logger.debug("Args: %s (id %d), %s", self.__class__, self.id, man_to)
        logger.debug("Successfully relinked the link from worker %s (id %d) to worker %s (id %d)",
                     worker_from, worker_from_id, worker_to, worker_to.id)

    def __setattr__(self, name, value):
        if name == self.worker_attr:
            if self.id is None:
                pass
            else:
                workership_changed.send(sender=self.__class__, instance=self, signal_name="workership_changed")
        super(WorkershipModel, self).__setattr__(name, value)

    def make_alias(self, worker):
        '''Make worker alias from workership. Uses original name or linked worker's full name'''
        kwargs = {}
        if getattr(self, 'original_name', ''):
            try:
                name = split_authors(self.original_name)[0]
                kwargs = dict(zip(('lastname', 'firstname', 'middlename'), name))
            except:
                return None, False
        else:
            worker = self.worker
            kwargs = {
                'lastname': worker.lastname,
                'firstname': worker.firstname,
                'middlename': worker.middlename
            }
        created = False
        if kwargs:
            aliases = worker.aliases.filter(**kwargs)
            if aliases:
                alias = aliases[0]
            else:
                alias = worker.aliases.create(validated=0, **kwargs)
                created = True
            return alias, created
        return None, False

    def get_similar_workership_to_worker(self, worker, workerships=None):
        if worker and not getattr(self.worker, "user", None):
            return worker.get_similar_workership([self])
        return None

    def unauthor(self, user):
        if not self.original_name:
            self.original_name = self.worker.fullname_short

        setattr(self, self.worker_attr, None)
        self.save()


class AuthorshipModel(WorkershipModel):

    worker_attr = 'author'

    class Meta:
        abstract = True


class AuthorshipPositionedModel(AuthorshipModel):

    class Meta:
        abstract = True
        ordering = ['position']


class Activity(object):
    """
    This model represents an activity of a worker. It is an independent model,
    subclassed directly from MyModel.
    Activity is something that worker has done. It is a result of his actions.
    Activity is the class of objects that we show on worker's profile page,
    on add work page, and in overall site statistics.
    Activity instance can be either an instance of LinkedToWorkersModel
    or instance of WorkershipModel.
    But not all instances of LinkedToWorkersModel/WorkershipModel are instances of Activity.
    Examples:
        Article. Article is Activity, and ArticleAuthorship is not an activity,
        because on worker's page we show articles, not authorships (and also on add work page
        and in site statistics).

        JournalEditorialBoardMembership is Activity, and Journal is not.

        Conference is LinkedToWorkersModel and not an Activity.

        ArticleAuthorship is WorkershipModel and not an Activity.
    """
    pass


def get_related_fields(cls):
    '''Returns list of tuples (linking_related_name, linking_field_name).
    E.g. if JournalAlias has a field 'journal' that is a ForeignKey to Journal model
    with related_name = 'aliases', then this method returns
    Implemented as a regular function to enable using with Django classes, e.g. User
    >>> ('aliases', 'journal') in get_related_fields(Journal)
    >>> True
    '''
    return [(rel.get_accessor_name(), rel.field.name) for rel in cls._meta.get_all_related_objects()]


def merge_two_objects(object_from, object_to):
    # move all related objects
    # this is done not to accidentally delete related objects
    # that we forgot to move manually
    # due to cascade delete by default in django
    for related_name, field_name in get_related_fields(object_from):
        try:
            # e.g. worker_from.reports.all().update(author=worker_to)
            getattr(object_from, related_name).all().update(**{field_name: object_to})
        except:
            # this shouldn't happen, but it happens for manually created tables
            # print related_name, field_name
            pass


class Country(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_COUNTRY_ID')
    iso2 = models.CharField(u'Код ISO2', max_length=255, db_column='F_COUNTRY_ISO2')
    iso3 = models.CharField(u'Код ISO3', max_length=255, db_column='F_COUNTRY_ISO3')
    name = models.CharField(u'Название', max_length=255, db_column='F_COUNTRY_NAME')
    name_ru = models.CharField(u'Название по-русски', max_length=255, db_column='F_COUNTRY_RUSNAME', blank=True)

    search_attr = 'name'

    class Meta:
        db_table = u'COUNTRY'
        verbose_name = u'Страна'
        verbose_name_plural = u'Страны'
        ordering = ['name']

    def __unicode__(self):
        return self.name_ru or self.name


def get_activity_models():
    all_models = models.get_models()
    submodels = lambda model: [m for m in all_models if issubclass(m, model)]
    return submodels(Activity)


def print_models():
    all_models = models.get_models()
    submodels = lambda model: [m.__name__ for m in all_models if issubclass(m, model)]

    linkedtoworkers = submodels(LinkedToWorkersModel)
    workerships = submodels(WorkershipModel)
    assert not set(linkedtoworkers) & set(workerships)
    mymodels = set(submodels(MyModel)) - set(linkedtoworkers) - set(workerships)
    othermodels = set(submodels(object)) - set(submodels(MyModel))

    sort_models = lambda s: sorted(list(s))

    print "\nLinkedToWorkersModel models:"
    print " ".join(sort_models(linkedtoworkers))

    print "\nWorkershipModel models:"
    print " ".join(sort_models(workerships))

    print "\nMyModel models:"
    print " ".join(sort_models(mymodels))

    print "\nOther models:"
    print " ".join(sort_models(othermodels))

STATUS_CHOICES = (
    ("EDIT", u"Редактируется"),
    ("SIGN", u"Подписан"),
    ("CONFIRM", u"Утвержден"),
    ("REEDIT", u"Отправлен на доработку"))


class ObjectList(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_MARKEDLIST_ID')
    name = models.CharField(u'Название', max_length=255, db_column='F_MARKEDLIST_NAME')
    category = models.CharField(u'Тип', max_length=255, db_column='F_MARKEDLIST_TYPE')
    code = models.CharField(u'Код (напр., год)', max_length=255, db_column='F_MARKEDLIST_VALUE', blank=True)
    user = models.ForeignKey(User, verbose_name=u"Пользователь", related_name="object_lists", db_column='F_MARKEDLIST_USER')
    department = models.ForeignKey(to='organizations.Department', verbose_name=u"Подразделение", db_column='F_DEPARTMENT_ID', null=True)
    status = models.CharField(u"Статус", max_length=255, choices=STATUS_CHOICES, db_column="F_MARKEDLIST_SIGNTYPE", blank=True)
    date_created = models.DateField(u'Дата создания', auto_now_add=True, db_column='F_MARKEDLIST_CREATE', blank=True, null=True)
    date_updated = models.DateField(u'Дата последней правки', auto_now=True, db_column='F_MARKEDLIST_DATE', blank=True, null=True)

    objects = ObjectListManager()

    class Meta:
        db_table = u'MARKEDLIST'
        verbose_name = u'Список объектов'
        verbose_name_plural = u'Списки объектов'
        ordering = ('-id',)

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('workers_worker_report_year', (), {'worker_id': self.worker.id, 'years_str': self.code, 'department_id': self.department.id})

    @property
    def worker(self):
        return get_worker(self.user)

    def set_status(self, user, status, comment="", pdf_url=None):
        old_status = self.status
        self.status = status
        self.save()
        self.statuslog_events.create(status=status, comment=comment, user=user)
        status_text = self.get_status_display().lower()
        if status in ("CONFIRM", "REEDIT"):
            subject = u"Ваш %s %s в системе ИСТИНА" % (self.name.lower(), status_text)
            body = render_to_string('workers/emails/report_status_changed.txt', {
                'status_text': status_text,
                'name': get_fullname(self.user),
                'editor_name': get_fullname(user, initials=True),
                'editor_user': user,
                'url': self.get_full_url(),
                'department': self.department.name,
                'comment': comment
            })
            self.user.email_user(subject, body)

        elif old_status == "REEDIT" and status == "SIGN":
            # determine who set reedit status
            editor = self.statuslog_events.filter(status="REEDIT")[0].user
            fullname = get_fullname(self.user, initials=True)
            subject = u"%s сотрудника %s %s в системе ИСТИНА" % (self.name, fullname, status_text)
            body = render_to_string('workers/emails/report_resigned.txt', {
                'name': get_fullname(editor),
                'worker_name': fullname,
                'url': self.get_full_url(),
                'department': self.department.name,
                'pdf_url': pdf_url
            })
            editor.email_user(subject, body)

    @property
    def can_be_edited(self):
        return not self.status or self.status in ("EDIT", "REEDIT")

    @property
    def can_be_approved(self):
        return self.status == "SIGN"

    @property
    def can_be_disapproved(self):
        return self.status in ["SIGN", "CONFIRM"]


class ObjectListItem(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_MARKEDLISTID_ID')
    object_list = models.ForeignKey(ObjectList, related_name="items", db_column='F_MARKEDLIST_ID')
    object_id = models.PositiveIntegerField(db_column='F_MARKEDLISTID_VAL')
    class_name = models.CharField(u"Название типа объекта", max_length=255, db_column='F_MARKEDLISTID_TYPE')
    content_type = models.ForeignKey(ContentType, verbose_name=u"Тип объекта", db_column='F_MARKEDLISTID_DJANGOTYPE', blank=True, null=True)
    title = models.CharField(u"Название объекта", max_length=1000, db_column='F_MARKEDLISTID_VALSTR', blank=True)
    date_created = models.DateField(u'Дата создания', auto_now_add=True, db_column='F_MARKEDLISTID_CREATE')
    group = models.IntegerField(u"Группа объектов", db_column='F_MARKEDLISTID_GROUP', blank=True, null=True)

    class Meta:
        db_table = u'MARKEDLISTID'
        verbose_name = u'Элемент списка объектов'
        verbose_name_plural = u'Элементы списков объектов'

    def __unicode__(self):
        return self.title


class ObjectListChangeStatusEvent(MyModel):
    id = models.AutoField(primary_key=True, db_column='F_MRLSTSIGN_ID')
    object_list = models.ForeignKey(ObjectList, related_name="statuslog_events", db_column='F_MARKEDLIST_ID')
    status = models.CharField(u"Статус", max_length=255, choices=STATUS_CHOICES, db_column="F_MRLSTSIGN_SIGNTYPE")
    comment = models.CharField(u"Комментарий", max_length=1000, db_column="F_MRLSTSIGN_COMMENT", blank=True)
    user = models.ForeignKey(User, verbose_name=u"Пользователь", db_column='F_MRLSTSIGN_USER', blank=True)
    date = models.DateField(u'Дата изменения статуса', auto_now_add=True, db_column='F_MRLSTSIGN_DATE')

    class Meta:
        db_table = u'MRLSTSIGN'
        verbose_name = u'Статус списка объектов'
        verbose_name_plural = u'Статусы списков объектов'
        ordering = ('-date',)

    def __unicode__(self):
        return u"%s: %s" % (self.object_list, self.get_status_display().lower())


class Attachment(MyModel):
    '''An uploaded file linked to a publication, e.g. full text, image etc.'''
    id = models.AutoField(primary_key=True, db_column="F_FILES_ID")
    link = models.FileField(u"Файл", upload_to=get_attachment_file_path,
                            max_length=255, db_column="F_FILES_LINK", validators=[validate_uploaded_file])
    category = models.ForeignKey('AttachmentCategory', verbose_name=u"Категория файла",
                                 related_name="category_attachments", db_column="F_FILETYPE_ID",
                                 null=True, blank=True, on_delete=models.SET_NULL)
    description = models.CharField(u"Описание файла", max_length=1000,
                                   db_column="F_FILES_DESCIPTION", blank=True)
    article_old = models.ForeignKey(to="publications.Article", related_name="attachments_old",
                                    db_column="F_ARTICLE_ID", verbose_name=u"связанная статья",
                                    null=True, blank=True)
    book_old = models.ForeignKey(to="publications.Book", related_name="attachments_old",
                                 db_column="F_BOOK_ID", verbose_name=u"связанная книга",
                                 null=True, blank=True)
    content_type = models.ForeignKey(ContentType, db_column="f_contenttype_id")
    object_id = models.PositiveIntegerField(db_column="f_object_id")
    content_object = generic.GenericForeignKey()
    date_uploaded = models.DateField(u"Дата загрузки", auto_now_add=True, db_column="F_FILES_CREATE")
    creator = models.ForeignKey(to="auth.User", related_name="attachments_added",
                                db_column="F_FILES_USER", null=True, blank=True)

    class Meta:
        db_table = "FILES"
        verbose_name = u"прикрепленный файл"
        verbose_name_plural = u"прикрепленные файлы"

    def __unicode__(self):
        name = self.filename or unicode(self.category) or self.description or "<no name attachment>"
        return u"%s (%s)" % (name, unicode(self.content_object))

    def get_absolute_url(self):
        return self.link.url

    @property
    def filename(self):
        '''Returns the name of the attachment file.'''
        if self.link.name:
            return self.link.name.split("/")[-1]
        elif hasattr(self, "_filename") and self._filename:
            return self._filename
        else:
            return ''

    @property
    def size(self):
        '''Returns the size of the attached file.'''
        return self.link.size

    @property
    def exists(self):
        '''Returns True if the file exists, False if it has been deleted.'''
        try:
            size = self.size
        except OSError:
            return False
        else:
            return True

    @property
    def short_description(self):
        if self.category and self.category.name != u"Другое":
            return self.category
        if self.description:
            return self.description
        return truncatechars(self.filename, 60)

    @property
    def long_description(self):
        if self.description and self.category and self.category.name != u"Другое":
            return self.description
        return ''

    def delete(self, *args, **kwargs):
        '''Deletes an attachment, but first removes a linked file from a filesystem.'''
        # store filename before deletion to be used in django-reversion
        self._filename = self.filename
        self.link.delete()
        super(Attachment, self).delete(*args, **kwargs)


class AttachmentCategory(MyModel):
    '''A text category which attachments can belong to by N-N relationship.'''
    id = models.AutoField(primary_key=True, db_column="F_FILETYPE_ID")
    name = models.CharField(u"Имя категории", max_length=255, db_column="F_FILETYPE_NAME", unique=True)

    class Meta:
        db_table = "FILETYPE"
        verbose_name = u"категория прикрепленного файла"
        verbose_name_plural = u"категории прикрепленных файлов"
        ordering = ('name', )

    def __unicode__(self):
        return self.name


class AttachmentCategoryMembership(MyModel):
    '''An intermediate model to link attachments and categories.'''
    id = models.AutoField(primary_key=True, db_column="F_FILETYPEDJANG_ID")
    content_type = models.ForeignKey(ContentType, verbose_name=u"Тип объекта", db_column="F_FILETYPEDJANG_CONTYPEID")
    category = models.ForeignKey(AttachmentCategory, verbose_name=u"Категория файла", related_name="memberships", db_column="F_FILETYPE_ID")

    class Meta:
        db_table = "FILETYPEDJANG"
        verbose_name = u"категория файла по типу объекта"
        verbose_name_plural = u"категории файлов по типам объектов"
        unique_together = ('content_type', 'category')
        ordering = ('content_type', 'category')

    def __unicode__(self):
        return u"%s: %s" % (self.content_type, self.category)


MERGE_REQUEST_STATUSES = {
    'approved': 'Y',
    'declined': 'N'
}


class MergeRequest(MyModel):

    '''Request to merge some objects, submitted by user and approved by admin'''
    id = models.AutoField(primary_key=True, db_column="F_MERGEREQUEST_ID")
    content_type = models.ForeignKey(ContentType, verbose_name=u'Тип объекта',
                                     db_column="F_MERGEREQUEST_CONTENTTYPE")
    main_object_id = models.PositiveIntegerField(u'Главный объединяемый', db_column='F_MERGEREQUEST_MAINOBJECTID')
    creator = models.ForeignKey(to="auth.User", related_name="merge_requests_created",
                                db_column="F_MERGEREQUEST_CREATOR", null=True, blank=True)
    reviewer = models.ForeignKey(to="auth.User", related_name="merge_requests_reviewed",
                                 db_column="F_MERGEREQUEST_REVIEWER", null=True, blank=True)
    status = models.CharField(u'Статус проверки', choices=MERGE_REQUEST_STATUSES.items(), max_length=1020,
                              null=True, blank=True, db_column='F_MERGEREQUEST_STATUS')
    description = models.CharField(u'Описание', max_length=2048, null=True, blank=True, db_column='F_MERGEREQUEST_MAINOBJECTDESCR')
    request_date = models.DateField(u'Дата создания заявки', auto_now_add=True, db_column='F_MERGEREQUEST_REQUESTDATE',)
    review_date = models.DateField(u'Дата проверки заявки', null=True, blank=True, db_column='F_MERGEREQUEST_REVIEWDATE',)

    class Meta:
        db_table = "MERGEREQUEST"
        verbose_name = u"Заявка на объединение"
        verbose_name_plural = u"Заявки на объединение"

    def objects_to_merge_ids(self):
        return list(self.secondary_objects_to_merge_ids()) + [self.main_object_id]

    def secondary_objects_to_merge_ids(self):
        return list(self.secondary_items.values_list('object_id', flat=True))

    def datatable_data_json(self):
        def user_to_url_column(user):
            if not user:
                return ''
            profile = user.get_profile()
            return '<a href="' + profile.get_absolute_url() + '" target="_blank">' + user.username + '</a>'

        data_list = [
            self.get_absolute_url_html(),
            self.content_type.name,
            user_to_url_column(self.creator),
            self.description,
            len(self.objects_to_merge_ids()),
            self.request_date.strftime('%d.%m.%Y'),
            self.status,
            user_to_url_column(self.reviewer),
            self.review_date and self.review_date.strftime('%d.%m.%Y'),
        ]
        data = dict(enumerate(data_list))
        data["DT_RowId"] = self.id
        return data

    @models.permalink
    def get_absolute_url(self):
        return ('common_merge_requests_detail', (), {'merge_request_id': self.id})

    def same_merge_candidates(self, main_candidate_id, secondary_canidates_ids):
        return (set(self.secondary_objects_to_merge_ids()) == set(secondary_canidates_ids) and
                main_candidate_id == self.main_object_id)

    def review_request(self, reviewer, status):
        self.status = status
        self.reviewer = reviewer
        self.review_date = datetime.now()
        self.save()

    def decline_request(self, reviewer):
        self.review_request(reviewer, MERGE_REQUEST_STATUSES['declined'])

    def approve_request(self, reviewer):
        self.review_request(reviewer, MERGE_REQUEST_STATUSES['approved'])
        model = self.content_type.model_class()
        try:
            model.objects.merge(reviewer.username, self.secondary_objects_to_merge_ids(), self.main_object_id)
        except Exception as err:
            sentry_logger = logging.getLogger('sentry_debug')
            sentry_logger.error(u'MergeRequest: error while merging something', exc_info=sys.exc_info())
            return unicode(err)
        else:
            return None

    @property
    def model(self):
        return self.content_type.model_class()

    def objects_to_merge_datatables_json(self):
        data_list = []
        try:
            main_object = self.model.objects.get(pk=self.main_object_id)
        except self.model.DoesNotExist:
            instance_json = data_list_to_datatables([
                '<span class=instance_missing> %d </span' % self.main_object_id,
                self.description
            ], self.main_object_id)
        else:
            instance_json = main_object.datatable_merge_json()
        data_list.append(instance_json)

        for secondary_item in self.secondary_items.all():
            item_id = secondary_item.object_id
            try:
                instance = self.model.objects.get(pk=item_id)
            except self.model.DoesNotExist:
                instance_json = secondary_item.missing_item_datatables()
            else:
                instance_json = instance.datatable_merge_json()
            data_list.append(instance_json)

        return {'aaData': data_list}

    @property
    def approved(self):
        return self.status == MERGE_REQUEST_STATUSES['approved']


class MergeRequestItem(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_MERGEREQUESTITEM_ID")
    merge_request = models.ForeignKey(MergeRequest, related_name='secondary_items', db_column="F_MERGEREQUEST_ID")
    object_id = models.PositiveIntegerField(u'Объединяемый объект', db_column='F_MERGEREQUESTITEM_OBJECTID')
    description = models.CharField(u'Описание', max_length=2048, null=True, blank=True, db_column='F_MERGEREQUESTITEM_OBJECTDESCR')

    class Meta:
        db_table = 'MERGEREQUESTITEM'
        verbose_name = u"Объединяемый объект"
        verbose_name_plural = u"Объединяемый объект"

    def missing_item_datatables(self):
        return data_list_to_datatables(
            [
                '<span class=instance_missing>%d </span' % self.id,
                self.description
            ], self.id
        )


class ProxyContentType(ContentType):
    search_attr = "name"

    class Meta:
        proxy = True


class ChoiceListOption(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_CODENAME_ID")
    list_code = models.CharField(u"Код списка выбора", max_length=255, db_column='F_CODENAME_TYPE')
    code = models.CharField(u"Код элемента списка", max_length=255, db_column='F_CODENAME_CODE')
    name = models.CharField(u"Название элемента списка", max_length=255, db_column='F_CODENAME_NAME')

    objects = ChoiceListOptionManager()

    class Meta:
        db_table = 'CODENAME'
        verbose_name = u"Элемент списка выбора"
        verbose_name_plural = u"Элементы списков выбора"

    def __unicode__(self):
        return self.name


class ScienceWork(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_SCEINCEWORK_ID")
    article = models.ForeignKey(to="publications.Article", verbose_name=u"Статья", related_name="sciencework",db_column="F_ARTICLE_ID", blank=True, null=True)
    course = models.ForeignKey(to="courses.Course", verbose_name=u"Курс", related_name="sciencework",db_column="F_COURSE_ID", blank=True, null=True)
    disser = models.ForeignKey(to="dissertations.Dissertation", verbose_name=u"Диссертация", related_name="sciencework",db_column="F_DISSER_ID", blank=True, null=True)
    book = models.ForeignKey(to="publications.Book", verbose_name=u"Книга", related_name="sciencework",db_column="F_BOOK_ID", blank=True, null=True)
    report = models.ForeignKey(to="reports.Report", verbose_name=u"Отчёт", related_name="sciencework",db_column="F_REPORT_ID", blank=True, null=True)
    diploma = models.ForeignKey(to="diplomas.Diploma", verbose_name=u"Диплом", related_name="sciencework",db_column="F_DIPLOM_ID", blank=True, null=True)
    patent = models.ForeignKey(to="patents.Patent", verbose_name=u"Патент", related_name="sciencework",db_column="F_PATENT_ID", blank=True, null=True)

    class Meta:
        db_table = 'SCEINCEWORK'
        verbose_name = u"Научная продукция"
        verbose_name_plural = u"Научная продукция"

    def __unicode__(self):
        if self.article:
            return u"статья %s" % self.article.title
        elif self.book:
            return u"книга %s" % self.book.title
        elif self.course:
            return u"учебный курс %s" % self.course.title
        elif self.diploma:
            return u"дипломная работа %s" % self.diploma.title
        elif self.disser:
            return u"диссертация %s" % self.disser.title
        elif self.patent:
            return u"патент %s" % self.patent.title
        elif self.report:
            return u"отчёт %s" % self.report.title


class AuthorRole(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORROLE_ID")
    name = models.CharField(u"Название роли авторства", max_length=255, db_column="F_AUTHORROLE_NAME")
    code = models.CharField(u"Код роли", max_length=255, db_column="F_AUTHORROLE_CODE", blank=True, null=True)

    class Meta:
        db_table = 'AUTHORROLE'
        verbose_name = u"Роль авторства"
        verbose_name_plural = u"Роли авторства"

    def __unicode__(self):
        return self.name


class AuthorRoleObject(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORROLEOBJ_ID")
    role = models.ForeignKey(to=AuthorRole, verbose_name=u"Роль авторства", related_name="authorroleobj", db_column="F_AUTHORROLE_ID")
    code = models.CharField(u"Код объекта", max_length=255, db_column="F_AUTHORROLEOBJ_OBJECTCODE")

    class Meta:
        db_table = 'AUTHORROLEOBJ'
        verbose_name = u"Применимость роли к объекту"
        verbose_name_plural = u"Применимости роли к объекту"

    def __unicode__(self):
        return u"%s для %s" % (self.role, self.code)


class AuthorScience(MyModel):
    id = models.AutoField(primary_key=True, db_column="F_AUTHORSCIENCE_ID")
    sciencework = models.ForeignKey(to=ScienceWork, verbose_name=u"Научная продукция", related_name="authorscience", db_column="F_SCEINCEWORK_ID")
    authorrole = models.ForeignKey(to=AuthorRole, verbose_name=u"Роль авторства", related_name="authorscience", db_column="F_AUTHORROLE_ID")
    worker = models.ForeignKey(to="workers.Worker", verbose_name=u"Сотрудник", related_name="authorscience", db_column="F_MAN_ID")
    date_created = models.DateField(u'Дата создания', auto_now_add=True, db_column='F_AUTHORSCIENCE_CREATE', blank=True, null=True)
    user = models.ForeignKey(User, verbose_name=u"Создатель", related_name="authorscience", db_column='F_AUTHORSCIENCE_USER', blank=True, null=True)
    name = models.CharField(u"ФИО", max_length=255, db_column="F_AUTHORSCIENCE_NAME", blank=True, null=True)
    date_validated = models.DateField(u'Дата потверждения', db_column='F_AUTHORSCIENCE_DATEVALIDATE', blank=True, null=True)
    user_validate = models.ForeignKey(User, verbose_name=u"Потверждающий пользователь", related_name="authorscience_validated", db_column='F_AUTHORSCIENCE_USERVALIDATE', blank=True, null=True)
    flag_validate = models.IntegerField(u"Флаг потверждения", db_column="F_AUTHORSCIENCE_FLAGVALIDATE", blank=True, null=True)
    description = models.CharField(u"Комментарии", max_length=2000, db_column="F_AUTHORSCIENCE_DESCRIPTION", blank=True, null=True)

    class Meta:
        db_table = 'AUTHORSCIENCE'
        verbose_name = u"Участие в создании научной продукции"
        verbose_name_plural = u"Участия в создании научной продукции"
        ordering = ['flag_validate']

    # def __unicode__(self):
    #     return self.name
