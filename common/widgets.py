# -*- coding: utf-8 -*-
from django import forms
from django.utils.safestring import mark_safe
from django.contrib.admin.widgets import FilteredSelectMultiple
from django_select2 import AutoHeavySelect2Widget,  Select2Widget, AutoHeavySelect2MultipleWidget, AutoHeavySelect2TagWidget
from django_select2.util import JSFunction, JSFunctionInContext
import re

class FilteredSelect(forms.widgets.Select):
    """
        Displays as an ordinary selectbox with an additional text-input for filtering the options.
        Requires: http://www.barelyfitz.com/projects/filterlist/filterlist.js

        Author: Konrad Giæver Beiske
        Company: Linpro
        For usage, see http://stackoverflow.com/q/4176613/304209 and http://stackoverflow.com/q/8562788/304209
    """
    class Media:
        js = ("base/js/filterlist.js",)

    case_sensitive_enabled = False
    pre_html = u"""
                    фильтр: <input class="filterlist" name=regexp onKeyUp="%s_filter.set(this.value)"/>
                    """
    post_html = u"""
                    <span%s><input type=checkbox name="toLowerCase" onClick="%s_filter.set_ignore_case(!this.checked)" />с учетом регистра</span>
                    <script type="text/javascript">
                    <!--
                    var %s_filter = new filterlist(document.getElementById("id_%s"));
                    //-->
                    </script>
                    """
    hide_html = ' style = "display: none"'

    def __init__(self, case_sensitive_enabled=None, *args, **kwargs):
        super(FilteredSelect, self).__init__(*args, **kwargs)
        if case_sensitive_enabled is not None:
            self.case_sensitive_enabled = case_sensitive_enabled

    def render(self, name, value, attrs=None, choices=()):
        super_res = super(FilteredSelect, self).render(name, value, attrs=attrs, choices=choices)
        name_safe = name.replace("-", "_")
        return mark_safe(self.pre_html % name_safe) + super_res + mark_safe(self.post_html % (self.hide_html if not self.case_sensitive_enabled else "", name_safe, name_safe, name))


class MyFilteredSelectMultiple(FilteredSelectMultiple):
    @property
    def media(self):
        media = super(MyFilteredSelectMultiple, self).media
        media.add_css({'all': ['admin/css/widgets.css',
                               'base/css/widgets.css'
                               ]})
        js = ['base/js/jsi18n.js',
              'admin/js/jquery.min.js',
              'admin/js/jquery.init.js',
        ]
        media._js = js + media._js
        return media


class MySelect2BaseWidget(object):
    class Media:
        js = ('base/js/select2.js', 'base/js/select2_locale_ru.js')
        css = {'all': ("base/css/widgets.css",)}

    def __init__(self, *args, **kwargs):
        self.add_option = kwargs.pop('add_option', False)
        self.input_string_option = kwargs.pop('input_string_option', False)
        self.width = kwargs.pop('width', 'default')
        super(MySelect2BaseWidget, self).__init__(*args, **kwargs)

    def init_options(self):
        super(MySelect2BaseWidget, self).init_options()
        self.options['formatResult'] = JSFunction('format_result')
        self.options['formatSelection'] = JSFunction('format_selected')
        self.options['escapeMarkup'] = JSFunction('escape')
        if 'ajax' in self.options:
            self.options['ajax']['quietMillis'] = 1000
        if self.width == 'wide':
            width = '770'
        elif self.width == 'narrow':
            width = '80'
        else:
            width = 'element'
        try:
            width = int(self.width)
        except ValueError:
            pass
        self.options['width'] = width
        if self.add_option:
            self.options['createSearchChoice'] = JSFunction('createSearchChoice')
            self.options['placeholder'] = u'Добавить новый'
        elif self.input_string_option:
            self.options['createSearchChoice'] = JSFunctionInContext('django_select2.createSearchChoice')


class Select2ChoiceWidget(MySelect2BaseWidget, AutoHeavySelect2Widget):
    pass


class LightSelect2ChoiceWidget(MySelect2BaseWidget, Select2Widget):
    pass


class Select2MultipleChoiceWidget(MySelect2BaseWidget, AutoHeavySelect2MultipleWidget):

    #this seems as a solution to Prod/Dev issue: https://github.com/applegrew/django-select2/issues/103
    def render_inner_js_code(self, id_, *args):
        field_id = self.field_id if hasattr(self, 'field_id') else id_
        fieldset_id = re.sub(r'-\d+-', '_', id_).replace('-', '_')
        if '__prefix__' in id_:
            return ''
        else:
            js = '''
                  window.django_select2.%s = function (selector, fieldID) {
                    var hashedSelector = "#" + selector;
                    $(hashedSelector).data("field_id", fieldID);
                  ''' % (fieldset_id)
            js += super(Select2MultipleChoiceWidget, self).render_inner_js_code(id_, *args)
            js += '};'
            js += 'django_select2.%s("%s", "%s");' % (fieldset_id, id_, field_id)
            return js




class Select2TagWidget(MySelect2BaseWidget, AutoHeavySelect2TagWidget):
    pass

class Select2WorkersWidget(Select2TagWidget):
    def init_options(self):
        super(Select2WorkersWidget, self).init_options()
        self.options['tokenSeparators'] = [",", ";" ]
        self.options['placeholder'] = u'Наберите ФИО сотрудника целиком или только начало'
        self.options['minimumInputLength'] = 3
