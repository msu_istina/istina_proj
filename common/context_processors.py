# -*- coding: utf-8 -*-
from django.conf import settings
import os
import shlex
import subprocess
from organizations.models import Department, Organization, OrganizationDomain


def current_database(request):
    '''Adds information about current database to any page. '''
    db = settings.DATABASES['default']
    engine = db['ENGINE'].split(".")[-1]
    if db['HOST']:
        db_string = "%s, %s@%s" % (engine, db['USER'], db['HOST'])
    else:
        db_string = "%s, %s" % (engine, db['NAME'])
    return {'settings_current_database': db_string}

def get_orgfields(request):
    '''Adding custom header text and header image. 
       We use organization property for that. 
    '''
    header_text = ""
    image_url = ""
    try: 
        organization = OrganizationDomain.objects.filter(domain = request.META['HTTP_HOST'])[0].organization
        if organization:
            header_text = organization.get_headertext()
            image_url = organization.get_header_imageurl()
    except: 
        pass
        
    if header_text == "":
        header_text = "Интеллектуальная Система Тематического Исследования НАучно-технической информации"
    if image_url == "":
        image_url = "//istina.msu.ru/static/base/img/msulogo.gif"

    return {"custom_headertext" : header_text, "custom_imageurl" : image_url}
    

def get_hg_info():
    """Get dict(current_commit, last_commit, branch, is_clean). If error, None is returned"""
    repo_args = ["-R", os.path.join(settings.SETTINGS_DIR, '..')]
    try:
        hg_current_commit = subprocess.check_output(["hg", "id", "-i"] + repo_args)
        hg_branch = subprocess.check_output(["hg", "id", "-b"] + repo_args)
        hg_last_commit = subprocess.check_output(shlex.split('hg log -b %s -l 1 --template "{node|short}"' % hg_branch.strip()) + repo_args)
    except subprocess.CalledProcessError:
        return None
    info = {
        "is_clean": not hg_current_commit.strip().endswith("+"),
        "branch": hg_branch.strip(),
        "commit": hg_current_commit.strip().rstrip("+"),
        "last_commit": hg_last_commit.strip()
    }
    return info


def hg_info(request):
    '''Adds information about current branch and commit to any page if DEBUG.

    Populates
        hg_commit - commit on which the server is started
        hg_branch - corresponding branch
        hg_is_clean - if repo is clean at the moment of request
        hg_last_commit - last commit in hg_branch at the moment of request
    If error populates hg_error only
    '''
    if not settings.DEBUG:
        return {}
    # we use cache to show hg info for the moment when server was started
    # rather then info for the moment when request came
    if not hasattr(hg_info, "info"):
        hg_info.info = get_hg_info()
    if hg_info.info is None:
        return {"hg_error": "Failed to detect mercurial revision"}

    # update info about repo is_clean and last_commit
    info = hg_info.info.copy()
    new_info = get_hg_info()
    for f in "is_clean", "last_commit":
        info[f] = new_info[f]

    context = dict(("hg_" + k, v) for (k, v) in info.iteritems())
    return context


def debug_toolbar_toogle_info(request):
    '''Adds information about current debug_toolbar status'''
    if not settings.DEBUG or not hasattr(settings, "DEBUG_TOOLBAR_SESSION_KEY"):
        return {}
    return {"DEBUG_TOOLBAR_SESSION_KEY": settings.DEBUG_TOOLBAR_SESSION_KEY,
            "DEBUG_TOOLBAR_ENABLE_REQUEST": settings.DEBUG_TOOLBAR_ENABLE_REQUEST,
            "DEBUG_TOOLBAR_DISABLE_REQUEST": settings.DEBUG_TOOLBAR_DISABLE_REQUEST,
            "debug_toolbar_enabled": settings.DEBUG_TOOLBAR_CONFIG['SHOW_TOOLBAR_CALLBACK'](request),
            }


def dummy_debug(request):
    '''Populate context with debug=settings.DEBUG regardless of INTERNAL_IPS'''
    return {'debug': settings.DEBUG}


def cache_time(request):
    "Time of cache life in seconds"
    return {'CACHE_TIME': settings.CACHE_TIME, 'CACHE_TIME_ARTICLES':settings.CACHE_TIME_ARTICLES }
