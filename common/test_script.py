def test():
    from django.contrib.auth.models import User
    user = User.objects.get(username='negrega')
    from workers.models import Worker
    list(Worker.objects.get_merge_candidates(user))

import os
from settings import *
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import timeit
t1 = timeit.Timer('test()', "from __main__ import test")
print t1.repeat(10, 1)

