import csv
from journals.models import JournalRubricMembership

journals_ids1 = JournalRubricMembership.objects.filter(
    rubric__parent__name__startswith="Computer Science").values_list('journal', flat=True)
journals_ids2 = JournalRubricMembership.objects.filter(
    rubric__name__startswith="Computer Science").values_list('journal', flat=True)
journals_ids = list(journals_ids1) + list(journals_ids2)

articles = Article.objects.filter(
    journal__in=journals_ids).values_list(
        'id', 'title', 'year', 'journal__name', 'abstract')
data = []
for article in articles:
    art = Article.objects.get(id=article[0])
    rubrics = ";".join(art.journal.rubric_memberships.filter(rubric__name__istartswith='Computer Science').values_list('rubric__name', flat=True))
    data.append([
        article[0], article[1], article[2],
        article[3], rubrics, article[4]
    ])

fout = open("/tmp/articles.csv", 'w')
csvwriter = csv.writer(fout)
rows = map(lambda a: map(lambda x: unicode(x).encode('utf8'), a), data)
csvwriter.writerows(rows)
fout.close()