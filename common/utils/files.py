# -*- coding: utf-8 -*-
from pytils.translit import translify
import hashlib
import re
from django.contrib import messages
from django.core.exceptions import ValidationError
import logging
from actstream import action

logger = logging.getLogger("common.utils.files")

def transform_filename(filename):
    '''Makes filename safe for 'any' operating system.
    Cyrillic characters are translified to ascii,
    spaces are replaced with underscores,
    after that all characters except
    alphanumeric, underscores, dashes and dots,
    are stripped.
    '''
    filename = translify(filename) # cyrillic characters -> latin 
    filename = re.sub(r' ', '_', filename) # spaces -> underscores
    filename = re.sub(r'[^a-zA-Z0-9_\-\.]+', '', filename) # strip all characters except alphanumeric, underscores, dashes and dots
    return filename


def number2path(number):
    '''Takes a number (e.g. id of an object) and returns a string, part of a path in a filesystem.
    Reason: avoid collecting too many subfolders in a folder.
    The result is repeatable (i.e. always the same for the same input.
    Example: number2path(100000) = '14e/e22/1000000'.
    '''
    hash = hashlib.md5(str(number)).hexdigest()
    return '%s/%s/%s' % (hash[:3], hash[3:6], str(number))


def get_attachment_file_path(instance, filename):
    '''Returns a path for storing attachments. 'instance' is an Attachment instance being saved.'''
    # e.g. "publications/article/14e/e22/1000000/file.jpg"
    if instance.content_object:
        return "%s/%s/%s/%s" % (
            instance.content_object._meta.app_label, # application name
            instance.content_object._meta.object_name.lower(), # instance class, lowercased
            number2path(instance.content_object.id), # make id safe
            transform_filename(filename)) # make filename safe
    else:
        # if there is no content object attached, you should specify full path
        return filename


def validate_uploaded_file(fileobj):
    if fileobj.size > 50 * 1024 * 1024: # 50 mb
        raise ValidationError(u"Слишком большой файл (> 50 мб).")


def process_attachments(request, formset, obj, user):
    if formset.is_valid():
        logger.debug("Attachment formset data: %s", getattr(formset, "cleaned_data", formset.data))
        attachments = formset.save(commit=False)
        for attachment in attachments:
            attachment.content_object = obj
            attachment.creator = user
            attachment.save()
            description = u"filename: %s%s%s" % (
                attachment.filename, 
                u', category: %s' % attachment.category if attachment.category else '', 
                u', description: %s' % attachment.description if attachment.description else '')
            action.send(user, 
                verb=u'добавил файл к %s' % obj.dative, 
                action_object=attachment, 
                target=obj, 
                description=description)
            messages.success(request, u"Файл %s успешно прикреплен." % attachment.filename)
        from common.templatetags.activity_description import invalidate_activity_description
        invalidate_activity_description(obj, silient=True)
