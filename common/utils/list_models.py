# -*- coding: utf-8 -*-
from database_choose import database
import django.db.models # should go after database_choose, since in the database_choose database is choosed
from django.db.utils import DatabaseError
from colors import error, okgreen

for model in django.db.models.get_models():
    app = model._meta.app_label
    name = model.__name__
    try:
        count = model.objects.count()
        print "%s.%s %s" % (app, name, okgreen(str(count)) if count else '')
    except DatabaseError:
        print "%s.%s %s" % (app, name, error("error"))
 
