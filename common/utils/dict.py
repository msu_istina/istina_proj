# -*- coding: utf-8 -*-

def filter_true(dct):
    return dict((key, value) for key, value in dct.items() if value)

def flatten_list_dict_values(dict_list):
    '''Return a flattened list of all values in a nested dict or list, see http://goo.gl/r0M1G. Also extracts values from nested lists.'''
    values = []
    for value in (dict_list.itervalues() if isinstance(dict_list, dict) else dict_list):
        if isinstance(value, dict) or isinstance(value, list):
            values.extend(flatten_list_dict_values(value))
        else:
            values.append(value)
    return values
