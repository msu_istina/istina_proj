# -*- coding: utf-8 -*-
from optparse import OptionParser

from colors import header, error
from python_init import settings


usage = "Usage: python %prog database_name (e.g. science)"
parser = OptionParser(usage)
(options, args) = parser.parse_args()
if len(args) != 1:
    parser.error("incorrect number of arguments. Specify database name.")

database = args[0]
try:
    settings.DATABASES['default'] = settings.DATABASES[database]
except KeyError:
    print error("Incorrect database name")
    print "Possible names: %s" % header(", ".join(settings.DATABASES.keys()))
    exit()

print header("Database '%s':" % database)
