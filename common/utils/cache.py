# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.core.cache import get_cache
from django.db.models.signals import pre_delete, post_save
from django.dispatch import receiver, Signal
from django.utils.hashcompat import md5_constructor
from django.utils.http import urlquote
from itertools import chain, product

from common.utils.activities import _ACTIVITIES_TYPES_SINGLE, get_work_from_workership
from common.utils.user import get_user, get_worker
default_cache = get_cache("default")
bigdata_cache = get_cache("bigdata")

def is_thesis(work):
    try:
        work.is_publication  # FIXME temporary check, https://bitbucket.org/goldan/istina/issue/634/cachepy
    except AttributeError:
        return False
    else:
        return work.theses

def get_deleted_versions_cache_key(model_class):
    return 'reversion_deleted_ids_%s' % model_class.__name__

def invalidate_template_cache(fragment_name, *variables):
    args = md5_constructor(u':'.join([urlquote(var) for var in variables]))
    cache_key = 'template.cache.%s.%s' % (fragment_name, args.hexdigest())
    default_cache.delete(cache_key)
    bigdata_cache.delete(cache_key)


def invalidate_worker_short(worker):
    ''' Invalidates cache of show_worker_short tag for worker'''
    no_links = (1, 0)
    for no_links_value in no_links:
        cache_key = "show_worker_short.%s.%s" % (worker.id, no_links_value)
        default_cache.delete(cache_key)
        bigdata_cache.delete(cache_key)


def invalidate_worker_coauthors_summary(worker):
    '''Invalidates worker's coauthors and summary.'''
    invalidate_template_cache("worker_full_info", worker.id, "coauthors")
    invalidate_worker_short(worker)


def invalidate_all_authors(work, invalidate_summary=True):
    '''For each work author invalidates his corresponding activity. If invalidate_summary is True, invalidates their's summary and coathours '''
    try:
        workers = work.workers.all()
    except AttributeError:
        workers = filter(bool, [work.worker])
    work_type = work.nominative_en
    if is_thesis(work):
        work_type = 'thesis'
    for worker in workers:
        if invalidate_summary:
            invalidate_worker_coauthors_summary(worker)
        invalidate_activity_type(worker.id, work_type)
        invalidate_all_activities(worker, only_latest=True)
    if work_type == "dissertation" and work.author is not None:
        if invalidate_summary:
            invalidate_worker_coauthors_summary(work.author)
        invalidate_activity_type(work.author.id, work_type)
        invalidate_all_activities(work.author, only_latest=True)


def invalidate_activity_type(worker_id, activity, only_latest=False):
    '''Invalidates  author's activity on profile page.'''
    no_links = (1, 0)
    if only_latest:
        activities_list_type = ("latest", "latest_added")
    else:
        activities_list_type = ("latest", "latest_added", "all")
    for (no_links_value, activities_list_type_value) in product(no_links, activities_list_type):
        if activity == "article":
            invalidate_template_cache("activities", worker_id, "article_journals", no_links_value, activities_list_type_value)
            invalidate_template_cache("activities", worker_id, "article_collections", no_links_value, activities_list_type_value)
        else:
            invalidate_template_cache("activities", worker_id, activity, no_links_value, activities_list_type_value)


def invalidate_all_activities(worker, only_latest=False):
    '''Invalidates all author's activities on profile page.'''
    for activity in _ACTIVITIES_TYPES_SINGLE:
        invalidate_activity_type(worker.id, activity, only_latest)


def invalidate_worker(worker):
    invalidate_worker_coauthors_summary(worker)
    invalidate_all_activities(worker)


def invalidate_activities_for_worker_and_coauthors(worker):
    '''Invalidates all worker's actvities and activities of  his coauthors where worker participates, on their profile pages.'''
    activities = list(chain(*worker.activities.values()))
    for work in activities:
        try:
            work.is_workership
        except AttributeError:
            pass
        else:
            work_from_workership = get_work_from_workership(work)
            if work_from_workership: # if work_from_workership is None, leave work as it is
                work = work_from_workership
        invalidate_all_authors(work, invalidate_summary=False)


def invalidate_user(username):
    '''Aimed only for usage in shell.'''
    from common.templatetags.activity_description import invalidate_activity_description # this must not be done in the beginning of the file, since otherwise the file activity_description.py will be imported too early, and KNOWNN_MODELS will not contain all needed models, since get_model function will not be ready yet.
    user = get_user(username)
    worker = get_worker(user)
    if worker:
        invalidate_all_activities(worker)
        for key, activities in worker.activities.items():
            for activity in activities:
                invalidate_activity_description(activity, silient=False)
        invalidate_worker(worker)


def invalidate_all_users():
    '''Invalidates all active users cache. Use with caution!'''
    for username in User.objects.filter(is_active=True).values_list('username', flat=True):
        invalidate_user(username)

def invalidate_all_users_articles():
    '''Invalidates all active users outer articles cache. !'''
    activity = "article"
    for user in User.objects.filter(is_active=True):
         worker = get_worker(user)
         if worker:
            invalidate_activity_type(worker_id=worker.id, activity=activity)


workership_changed = Signal(providing_args=["instance", "signal_name"])


@receiver(workership_changed)
@receiver(post_save)
@receiver(pre_delete)
def invalidate_cache_on_save_delete_edit_WorkershipModel(sender, **kwargs):
    from common.templatetags.activity_description import invalidate_activity_description
    workership = kwargs.get("instance")
    try:
        workership.is_workership  # FIXME temporary check, https://bitbucket.org/goldan/istina/issue/634/cachepy
        worker = workership.worker  # FIXME must be in else, here because of "Error in formatting: object has no attribute  *_id "
    except AttributeError:
        pass
    else:
        work = get_work_from_workership(workership)
        invalidate_activity_description(work, silient=True)
        invalidate_activity_description(workership, silient=True)
        if not kwargs.get("signal_name") == "workership_changed":
            # post_save and per_delete handler
            invalidate_all_authors(work or workership) # work can be None if workership is not linked to LinkedToWorkersModel
        if worker:
            invalidate_worker_coauthors_summary(worker)
            if work:
                work_type = work.nominative_en
                if is_thesis(work):
                    work_type = "thesis"
            else:
                work_type = workership.nominative_en
            invalidate_activity_type(worker.id, work_type)
            invalidate_all_activities(worker, only_latest=True)



@receiver(post_save)
def invalidate_cache_on_worker_name_change(sender, **kwargs):
    worker = kwargs.get("instance")
    created = kwargs.get("created")
    if worker.__class__.__name__ == "Worker" and  not created:
        for coauthor in worker.get_coauthors():
            invalidate_worker_coauthors_summary(coauthor)
        invalidate_worker_coauthors_summary(worker)
        invalidate_activities_for_worker_and_coauthors(worker)


@receiver(post_save)
@receiver(pre_delete)
def invalidate_cache_on__save_delete_edit_LinkedToWorkersModel(sender, **kwargs):
    '''Invalidates workers coathors and acitivity information when some of their work is changed.'''
    from common.templatetags.activity_description import invalidate_activity_description
    created = kwargs.get("created")
    work = kwargs.get("instance")
    if not created:
        try:
            # FIXME temporary check, must be fixed, https://bitbucket.org/goldan/istina/issue/634/cachepy
            work.is_LinkedToWorkersModel
        except AttributeError:
            pass
        else:
            invalidate_all_authors(work, invalidate_summary=False)
            work_type = work.nominative_en
            invalidate_activity_description(work, silient=True)
            if work_type == 'journal' or work_type == 'collection':
                # If journal or collection name changes, we must invalidate articles from it
                for article in  work.articles.all():
                    if is_thesis(article):
                        work_type = "thesis"
                    else:
                        work_type = "article"
                    invalidate_activity_description(article)
                    for worker in article.workers.all():
                        invalidate_activity_type(worker.id, work_type)
                        invalidate_all_activities(worker, only_latest=True)
            if work_type == 'conference':
                work_type = 'conference_presentation'
                for conference_presentation in work.presentations.all():
                    invalidate_activity_description(conference_presentation)
                    for worker in conference_presentation.workers.all():
                        invalidate_activity_type(worker.id, work_type)
                        invalidate_all_activities(worker, only_latest=True)

instance_recovered = Signal(providing_args=["instance"])

@receiver(instance_recovered)
@receiver(pre_delete)
def invalidate_recovery_list(sender, **kwargs):
    instance = kwargs.get("instance")
    from common.models import MyModel
    if isinstance(instance, MyModel):
        model = instance.__class__
        cache_key = get_deleted_versions_cache_key(model)
        default_cache.delete(cache_key)
