# -*- coding: utf-8 -*-
import datetime


def convert_to_datetime(date):
    if not isinstance(date, datetime.datetime):
        if isinstance(date, int): # year
            date = datetime.datetime(date, 7, 1) # 1st of July, middle of the year
        elif isinstance(date, datetime.date):
            date = datetime.datetime(date.year, date.month, date.day, 0, 0)
        else:
            date = None
    return date


def compare(item1, item2):
    '''Compares two instances of MyModel.'''
    ordering_field1 = getattr(item1, 'ordering_field', None)()
    ordering_field2 = getattr(item2, 'ordering_field', None)()
    date1 = getattr(item1, ordering_field1, None) if ordering_field1 else None
    date2 = getattr(item2, ordering_field2, None) if ordering_field2 else None
    # try to convert into datetime
    date1 = convert_to_datetime(date1)
    date2 = convert_to_datetime(date2)
    # if failed converting
    if not date1 and date2:
        return 1 # correct ascending order
    elif date1 and not date2:
        return -1
    elif not date1 and not date2:
        return 0
    if date1 < date2:
        return 1 # correct ascending order
    elif date1 > date2:
        return -1
    return 0
