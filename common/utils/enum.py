class Enum(dict):
    """Simple dict-based enumeration

    >>> enum = Enum("first", "second", "third")
    >>> enum.first, enum.second
    (0, 1)
    >>> enum.unexisted
    Traceback (most recent call last):
    ...
    AttributeError
    >>> sorted(list(enum))
    [0, 1, 2]
    """
    def __init__(self, *names):
        self._dict = dict((name, i) for i, name in enumerate(names))

    def __getattr__(self, name):
        if name in self._dict:
            return self._dict[name]
        raise AttributeError

    def __iter__(self):
        return self._dict.itervalues()
