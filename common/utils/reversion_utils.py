# -*- coding: utf-8; -*-
from reversion.models import VERSION_ADD, VERSION_CHANGE, VERSION_DELETE
from reversion import create_revision, get_user, revision_context_manager, default_revision_manager
from django.core.urlresolvers import reverse
VERSION_TYPE_CHOICES = {
    VERSION_ADD: u"Создан",
    VERSION_CHANGE: u"Отредактирован",
    VERSION_DELETE: u"Удален"
}


def get_version_comment(version):
    revision = version.revision
    if revision.comment:
        return revision.comment
    else:
        try:
            return VERSION_TYPE_CHOICES[version.type]
        except KeyError:
            return ""

def get_admin_url(version):
    if version.type != VERSION_DELETE:
        return reverse('admin:%s_%s_history' % (version.content_type.app_label, version.content_type.model),
                       args=(version.object_id,))
    else:
        return reverse('admin:%s_%s_changelist' % (version.content_type.app_label, version.content_type.model))

def my_create_revision(f):
    def wrapped(*args, **kwargs):
        with create_revision():
            f(*args, **kwargs)
            user = get_user()
        revision_context_manager._user = user
    return wrapped


def get_deleted_objects_versions(model_class):
    versions = default_revision_manager.get_deleted(model_class)
    return [[versions, reverse('admin:%s_%s_recover' % (version.content_type.app_label, version.content_type.model),
                               args=(version.id,))]
            for version in versions]


def get_object_versions(model, object_id):
    return default_revision_manager.get_for_object_reference(model, object_id)
