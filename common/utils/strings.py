def capitalize_1st_letter(str):
    return str[0].capitalize() + str[1:]

def uncapitalize_1st_letter(s):
   if len(s) == 0:
      return s
   else:
      return s[0].lower() + s[1:]

def split_lines_take_left(text, num=10, delimiter='\n'):
    "Split first num lines of lines from text,return them as text."
    return split_lines(text, num, delimiter, left=1)


def split_lines_take_right(text, num=10, delimiter='\n'):
    "Split first num lines of lines from text,return remaining text."
    return split_lines(text, num, delimiter, left=0)


def split_lines(text, num=10, delimiter='\n', left=1):
    lines = text.split(delimiter, num)
    if len(lines) > num:
        if left:
            lines = lines[:-1]
        else:
            return lines[-1]
    return  delimiter.join(lines)


def line_length(text, delimiter='\n'):
    "Return number of lines in text."
    lines = text.split(delimiter)
    if lines[-1].isspace():
        return len(lines) - 1
    return len(lines)


def replace_by_list(text, conversion_list):
    for k, v in conversion_list:
        text = text.replace(k, v)
    return text


def string_to_bool_or_null(value):
        """
        Explicitly checks for the string 'True', 'False'. If here is smth other, return None
        """
        if value.lower() in ('false', '0'):
            return False
        else:
            if value.lower() in ('true', '1'):
                return True
        return None