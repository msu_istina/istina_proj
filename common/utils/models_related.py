# -*- coding: utf-8 -*-
from django.db.models import ManyToManyField, ForeignKey, OneToOneField, ObjectDoesNotExist


def get_related_objects(instance):
    related_fields_names = set()
    related_fields_names.update([related.get_accessor_name() for related in instance._meta.get_all_related_objects()])
    related_fields_names.update([related.get_accessor_name() for related in instance._meta.get_all_related_many_to_many_objects()])
    related_objects = []
    for field_name in related_fields_names:
        try:
            current_related_objects = getattr(instance, field_name).all()
        except ObjectDoesNotExist:
            pass
        except AttributeError:
            current_related_objects = [getattr(instance, field_name)]
        related_objects.extend(current_related_objects)
    return related_objects


def has_related_objects(instance, exceptions=[]):
    related_fields_names = set()
    related_fields_names.update([related.get_accessor_name() for related in instance._meta.get_all_related_objects()])
    related_fields_names.update([related.get_accessor_name() for related in instance._meta.get_all_related_many_to_many_objects()])
    for field_name in related_fields_names:
        if field_name in exceptions:
            continue
        try:
            related_exists = getattr(instance, field_name).exists()
        except ObjectDoesNotExist:
            related_exists = False
        except AttributeError:
            related_exists = bool(getattr(instance, field_name))
        if related_exists:
            return True
    return False


def test_get_related_objects():
    from workers.models import  Profile
    profile = Profile.objects.get(user__username='safonin')
    worker = profile.worker
    related = get_related_objects(worker)
    assert profile in related #Test OneToOne
    articles = worker.articles.all()
    for article in articles: #Test ManyToMany
        assert article in related
    article_authorships = worker.article_authorships.all()
    for article_authorship in article_authorships: #Test ForeignKey
        assert article_authorship in related


def get_linked_to_instance_objects(instance):
    exclude = ['creator']
    linked_fields = [ManyToManyField, ForeignKey, OneToOneField]
    model = instance.__class__
    fields = model._meta.fields
    linked_to = []
    for field in fields:
        if field.__class__ in linked_fields:
            if field.name not in exclude:
                objs = getattr(instance, field.name)
                try:
                    linked_to.extend(objs)
                except TypeError:
                    if objs:
                        linked_to.append(objs)
    return linked_to


def test_get_linked_to_instance_objects():
    from workers.models import Profile, WorkerAlias
    profile = Profile.objects.get(user__username='safonin')
    worker = profile.worker
    linked_to_profile = get_linked_to_instance_objects(profile)
    assert worker in linked_to_profile #Test OneToOne
    linked_to_worker = get_linked_to_instance_objects(worker)
    departments = worker.departments.all()
    for department in departments: #Test ManyToMany
        assert department in linked_to_worker

    worker_alias = WorkerAlias.objects.all()[0]
    linked_to_alias = get_linked_to_instance_objects(worker_alias)
    worker = worker_alias.worker
    assert worker in linked_to_alias
