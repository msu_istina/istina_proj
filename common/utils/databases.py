from settings import DATABASES

oracle_engine_string = 'django.db.backends.oracle'

def current_database_engine():
    '''Returns current database engine string'''
    current_db = DATABASES['default']
    return current_db['ENGINE']
