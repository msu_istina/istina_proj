# -*- coding: utf-8 -*-
import os
import sys
from os.path import join
from commands import getoutput

# init settings for running python scripts, e.g. 'python common/utils/dump_models.py'
sys.path.insert(0, join(os.environ['VIRTUAL_ENV'], 'django-istina', 'istina'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import settings
