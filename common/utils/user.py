# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from common.utils.list import is_tuple_list
from pytils.translit import translify
from settings import DEFAULT_FROM_EMAIL, ENV_DIR
from django.core.mail import send_mail
from os.path import join
from time import sleep
from actstream import action


def get_user(username_or_email):
    '''Returns a user instance by username or email. First check username, then email. If no or multiple users are found, raise exception. If there is a user A with username=username_or_email and there is a user B with email=username_or_email, user A will be returned without exception.
    Returns only a single user or raises an exception.
    '''
    user = None
    try:
        user = User.objects.get(username=username_or_email)
    except User.DoesNotExist:
        user = User.objects.get(email=username_or_email)
    return user


def get_profile(user):
    '''Returns user's profile if the user is a User instance (not anonymous) and profile exists.'''
    try:
        profile = user.get_profile()
    except (AttributeError, ObjectDoesNotExist):
        profile = None
    return profile


def get_profile_url(user):
    '''Returns user's profile absolute_url'''
    return reverse('user_profile_public', kwargs={'username': user.username})


def get_worker(user):
    '''Returns user's worker if it is linked to him.'''
    profile = get_profile(user)
    return profile.worker if profile else None

def get_fullname(user, initials=False):
    fullname = ""
    profile = get_profile(user)
    if profile:
        if profile.fullname:
            fullname = profile.fullname if not initials else profile.fullname_short
        else:
            worker = get_worker(user)
            if worker:
                fullname = (worker.fullname if not initials else worker.fullname_short) or ""
    return fullname

def set_active(user):
    if not user.is_active:
        user.is_active = True
        user.save()

def mail_users(users, subject, body="", template_filename="base_email.txt", extra_context=None, really_send=False, log_enabled=True, sleep_enabled=True, action_target=None, action_object=None):
    usernames_excluded = ['Barmin', 'Buchin', 'Falunin', 'Glagolev', 'Kuznetsov', 'Panob', 'Pavelyev', 'Savinov', 'Stepanov', 'Yakimov', 'Nekrasov', 'Cherny']
    users_excluded = User.objects.filter(email="gala@imec.msu.ru", username__in=usernames_excluded)

    from organizations.models import Organization

    if users == "no":
        users = [] # no users, default
    elif users == "goldan":
        users = User.objects.filter(email__in=["denis.golomazov@gmail.com"]) # just me
    elif users == "admins":
        users = User.objects.filter(email__in=["denis.golomazov@gmail.com", "serg@msu.ru"]) # admins to test
    elif users == "msu":
        msu = Organization.objects.get_msu()
        users = msu.users # all active employees in msu, use with caution!
    elif users == "active":
        users = User.objects.filter(is_active=True) # all active users, use with caution!
    elif users == "all":
        users = User.objects.all() # all users, use with caution!
    elif is_tuple_list(users):
        pass # just a list of users
    else:
        raise

    if log_enabled:
        log_filename = u"%s.log" % translify(subject)
        logfile = open(join(ENV_DIR, 'var', 'log', log_filename), 'w')

    from_email = DEFAULT_FROM_EMAIL

    for user in users:
        name = get_fullname(user)
        if log_enabled:
            log_message = u"%d %s %s %s\n" % (user.id, user.username, user.email, name)
        if user in users_excluded or not user.email:
            if log_enabled:
                log_message = "SKIP: %s" % log_message
        else:
            context = {'name': name, 'username': user.username, 'body': body}
            if extra_context is not None:
                context.update(extra_context)
            message = render_to_string(template_filename, context)
            if really_send:
                send_mail(subject, message, from_email, [user.email])
        if log_enabled:
            logfile.write(log_message.encode('utf8'))
            print log_message,
        if really_send:
            action.send(user, verb=u"получил письмо от системы", action_object=action_object, target=action_target, description=subject)
        if sleep_enabled:
            sleep(1)
    if log_enabled:
        logfile.close()
