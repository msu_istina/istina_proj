import copy
from cStringIO import StringIO
from decimal import Decimal
from itertools import dropwhile
import os.path

import HTMLParser
import openpyxl
from openpyxl.writer.excel import save_virtual_workbook
import xlrd
import xlutils.copy
from xlwt import Workbook

from django.http import HttpResponse
from django.template.loaders.app_directories import Loader
from django.utils.html import strip_tags


encode = lambda cell: cell.encode('utf8')
html_parser = HTMLParser.HTMLParser()

def writerow(sheet, index, cells):
    for j, value in enumerate(cells):
        sheet.write(index, j, value)

def prepare_cell(cell_to_prepare):
    if cell_to_prepare is None:
        return ''
    cell = unicode(cell_to_prepare)
    if isinstance(cell_to_prepare, (float, Decimal)):
        # if cell is a float/Decimal number, replace dot with comma, since Excel likes comma more
        cell = cell.replace('.', ',')
    # remove html tags, unescape html sequences like &amp; and encode to encoding
    return encode(html_parser.unescape(strip_tags(cell)))

def export_to_xlsx_response(data, template, filename):
    """
    Exports a list of sheets to excel (xlsx).

    data: dictionary of sheet name -> dictionary (rows, first_row?, first_column?)
    first_row and first_column are 1-based
    template: name of .xlsx template file
    filename: name of the output file

    Returns an HttpResponse with file
    """
    template_path = next(dropwhile(lambda p: not os.path.exists(p), Loader().get_template_sources(template)))
    workbook = openpyxl.load_workbook(template_path)
    for sheet_name, sheet_data in data.iteritems():
        sheet = workbook[sheet_name]
        rows = sheet_data['rows']
        for i, row in enumerate(rows, start=sheet_data.get('first_row', 1)):
            for j, value in enumerate(row, start=sheet_data.get('first_column', 1)):
                sheet.cell(row=i, column=j).value = prepare_cell(value) if not isinstance(value, (int, float, Decimal)) else value

    response = HttpResponse(save_virtual_workbook(workbook), content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response

def export_to_excel_response(rows, sheetname, filename):
    """
    Exports a list of rows in excel (xls).
    Also strips html tags and converts html entities to strings.
    Returns a HttpResponse with a file.
    """    
    excelfile = StringIO()
    excel = Workbook(encoding='utf8')
    # i - row index, j - cell index (both 0-based)
    sheet = excel.add_sheet(sheetname)
        
    for i, row in enumerate(rows, start=0):
        writerow(sheet, i, map(prepare_cell, row))
    
    excel.save(excelfile)
    response = HttpResponse(excelfile.getvalue(), content_type='application/octet-stream')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response

#
# Function that modify exisitng excel file
#

def update_cell_value(ws, row, col, value):
    '''Change value of cell at (ROW, COL) preserving formatting.'''
    if ws.rows.get(row, None) and ws.rows[row]._Row__cells[col]:
        old_xf_idx = ws.rows[row]._Row__cells[col].xf_idx
        ws.write(row, col, value)
        ws.rows[row]._Row__cells[col].xf_idx = old_xf_idx
    else:
        ws.write(row, col, value)


def insert_rows(worksheet, insert_at, rows_to_add=1):
    '''Insert ROWS_TO_ADD rows to worksheet WORKSHEET starting at
    position INSERT_AT using row at that position as a template.'''
    nrows = max(worksheet.get_rows().keys())
    # move all affeted rows down
    for rownum in xrange(nrows-1, insert_at-1, -1):
        rownum2 = rownum + rows_to_add
        row = worksheet.rows[rownum]
        row._Row__idx = rownum2
        # update cells
        for cell_at in row._Row__cells.keys():
            cell = row._Row__cells[cell_at]
            if cell:
                cell.rowx = row.get_index()
        # update merged ranges
        def update_mr(mr, rn, rn2):
            if mr and len(mr) == 4 and mr[0] == rn and mr[1] == rn:
                return (rn2, rn2, mr[2], mr[3])
            else:
                return mr
        if worksheet._Worksheet__merged_ranges is not None:
            worksheet._Worksheet__merged_ranges = map(lambda mr: update_mr(mr, rownum, rownum2),\
                                                      worksheet._Worksheet__merged_ranges)
        # remove old row
        worksheet.rows[rownum2] = row
        worksheet.rows[rownum] = None
        del worksheet.rows[rownum]
    # insert empty rows using the first moved row as a template
    row = worksheet.rows[insert_at + rows_to_add]
    for rownum in xrange(insert_at, insert_at + rows_to_add):
        newrow = worksheet.row(rownum)
        # copy cells from the template
        for cell_at in row._Row__cells.keys():
            newcell = None
            cell = row._Row__cells[cell_at]
            if cell:
                newcell = copy.copy(cell)
                newcell.rowx = rownum
            newrow._Row__cells[cell_at] = newcell
        worksheet.rows[rownum] = newrow
        # copy merged ranges data, if any
        for mr in filter(lambda r: r and r[0] == r[1] == row.get_index(),
                         worksheet._Worksheet__merged_ranges):
            worksheet._Worksheet__merged_ranges.append(
                (row.get_index(), row.get_index(), mr[2], mr[3]))


def populate_template(input_filename, output_filename, data,
                      row = 0, worksheet_index = 0):
    '''Put DATA into WORKSHEET starting from row number ROW. DATA
    is a dictionary with keys `data` (list of lists) and `positions`
    (list of zero-based columns` indeces).'''
    from xlutils.copy import copy
    wb = copy(xlrd.open_workbook(input_filename, formatting_info = True))
    worksheet = wb.get_sheet(worksheet_index)

    number_to_add = len(data['data'])
    pos = data['positions']
    insert_after = row

    insert_rows(worksheet, insert_after, number_to_add)
    for rownum, datarow in enumerate(data['data'], insert_after):
        for colnum, item in enumerate(datarow):
            update_cell_value(worksheet, rownum, pos[colnum], item)

    wb.save(output_filename)


def populate_template_to_response(template_filename, data,
                                  row = 0, worksheet_index = 0,
                                  filename = 'report.xls'):
    """
    DATA is a dictionary with keys 'data' (list of lists) and 'positions'
    (list of zero-based columns' indeces).
    Returns a HttpResponse with a file.
    """    
    output_file = StringIO()
    
    populate_template(template_filename, output_file, data,
                      row = row, worksheet_index = worksheet_index)
    response = HttpResponse(output_file.getvalue(), content_type='application/octet-stream')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response

