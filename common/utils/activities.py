# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import defaultdict
from itertools import chain
from django.contrib.auth.models import User
from django.db import models
import logging
from django.core.exceptions import ObjectDoesNotExist

from actstream.models import Action, actors_stream
from common.utils.compare import compare
from common.utils.list import get_plural

logger = logging.getLogger("common.utils.activities")

# ordering of the list matters in activities summary (17 items)
# this should be the way it is called in worker., like worker.articles
_ACTIVITIES_TYPES = [
    'articles', 'books', 'conference_presentations',
    'theses', 'projects', 'patents',
    'certificates', 'reports', 'investments',
    'awards',
    'society_memberships', 'honorary_memberships', 'traineeships',
    'journal_editorial_board_memberships', 'collection_editorships', 'conference_committee_memberships',
    'dissertation_councils_memberships', 'dissertations_advised', 'dissertations_defended',
    'diplomas', 'courses', 'courses_teachings',
    'articles_journal', 'articles_collection',
]
_ACTIVITIES_TYPES_SINGLE = [
    'article', 'book', 'certificate',
    'conference', 'conference_presentation', 'collection',
    'course', 'dissertation', 'dissertation_council',
    'diploma', 'investment', 'journal',
    'patent', 'project', 'report',
    'thesis', 'award',
]
_WORKERSHIP_ACTIVITIES_TYPES = [
    'project_memberships', 'society_memberships', 'honorary_memberships',
    'traineeships', 'journal_editorial_board_memberships', 'collection_editorships',
    'conference_committee_memberships', 'dissertation_councils_memberships', 'courses_teachings'
]
_ACTIVITY_STREAM_PUBLICATIONS_VERBS = frozenset("добавил " + subj for subj in(
    'статью', 'книгу', 'доклад на конференции',
    'тезисы доклада', 'НИР', 'патент',
    'свидетельство о регистрации прав на ПО', 'отчет', 'инвестицию',
    'членство в редколлегии журнала', 'членство в редколлегии сборника', 'членство в программном комитете',
    'членство в диссертационном совете', 'диссертацию', 'дипломную работу',
    'учебный курс', 'преподавание учебного курса'
))


def get_activities_managers(worker, prefetched=False):
    """Get a list of tuples(activity type, object manager for current activity for given worker)"""

    def get_activities_by_type(key):
        if key == 'articles':  # means pure articles, not theses
            return worker.articles.pure()
        elif key == 'articles_journal':
            return worker.articles.pure_journal()
        elif key == 'articles_collection':
            return worker.articles.pure_collection()
        elif key == 'theses':
            return worker.articles.theses()
        elif key == 'projects':
            return worker.projects.public()
        else:
            return getattr(worker, key)

    def get_activities_by_type_prefetched(key):
        if key in ['articles', 'articles_journal', 'articles_collection', 'theses']:
            if key == 'articles': # means pure articles, not theses
                activity = worker.articles.pure()
            elif key == 'articles_journal':
                activity = worker.articles.pure_journal()
            elif key == 'articles_collection':
                activity = worker.articles.pure_collection()
            else:
                activity = worker.articles.theses()
            return activity.prefetch_related('authors__profile',
                                             'rewarded_articles',
                                             'confirmed_articles',
                                             'workerships',
                                             'collection__editors',
                                             'journal__publisher',
                                             'collection__series'
                                             )
        else:
            activity = getattr(worker, key)
            if key == 'conference_committee_memberships':
                return activity.prefetch_related('conference')
            elif key in _WORKERSHIP_ACTIVITIES_TYPES:
                return activity
            activity = activity.prefetch_related('workers', 'workerships')
            if key == 'conference_presentations':
                return activity.prefetch_related('conference')
            elif key == 'dissertations_defended' or key =='dissertations_advised':
                return activity.prefetch_related('author', 'domain')
            elif key == 'books':
                return activity.prefetch_related('publisher')
            elif key == 'projects':
                return worker.projects.public()
            else:
                return activity
    if not prefetched:
        activities = [(key, get_activities_by_type(key)) for key in _ACTIVITIES_TYPES]
    else:
        activities = [(key, get_activities_by_type_prefetched(key)) for key in _ACTIVITIES_TYPES]
    return activities


def get_activities(worker, limit=False, summary=False, clickable_summary=False, years=None, prefetched=False, bool_only=False, recent_type="date", no_duplicates=False):
    ''' Returns full list of works and activities of the worker.
        if @limit is specified, return this number of latest activities.
        if @summary is specified, do not return the activities themselves, but only the summary string.
        if @clickable_summary is specified, emit html references into summay.
        if @years are specified, return all activities for the given years (a list).
        if @no_duplicates = True, don't include duplicating keys (like articles_journal, articles_collection)
    '''
    activities = get_activities_managers(worker, prefetched)
    if not limit:
        if summary or clickable_summary:
            # dissertations are a special case, defended and advised dissertations should be counted together
            dissertations_defended_count = dict(activities)['dissertations_defended'].count()
            # courses are a special case, courses (authored and teached) should be counted together, as a number of different courses
            courses_authored_ids = list(dict(activities)['courses'].values_list('id', flat=True))
            courses_teachings_ids = list(dict(activities)['courses_teachings'].values_list('course', flat=True))
            courses_count = len(set(courses_authored_ids + courses_teachings_ids))
            summary_string_list = []
            for key, subactivities in activities:
                if key in ['articles_journal', 'articles_collection']: # do not put articles' subtypes into summary
                    continue
                obj = None
                if key in ['dissertations_defended', 'courses_teachings']: # plain 'courses' means courses authored
                    count = 0
                elif key == "courses":
                    count = courses_count
                else:
                    count = subactivities.count()
                    if key == 'dissertations_advised' and dissertations_defended_count:
                        # if there are no dissertations_advised, but some dissertations defended, sample object should be created
                        if not count:
                            obj = worker.dissertations_defended.all()[0]
                        count += dissertations_defended_count
                if count:
                    if not obj:
                        if key == 'theses': # special case, model() will return (pure) Article, see #920
                            subactivities.model.pure_bak = subactivities.model.pure
                            subactivities.model.pure = False
                        obj = subactivities.model()
                    activity_label = ''
                    if clickable_summary:
                        activity_label = '<a href="#' + key + '">' + get_plural(obj, count) + '</a>'
                    else:
                        activity_label = get_plural(obj, count)
                    summary_string_list.append(activity_label)
                    if key == 'theses':
                        subactivities.model.pure = subactivities.model.pure_bak
            return ", ".join(summary_string_list)
        elif years:
            activities = [(key, [subactivity for subactivity in subactivities.all() if subactivity.check_year_range(years)]) for key, subactivities in activities]
        else:
            if not bool_only:
                activities = [(key, subactivities.all()) for key, subactivities in activities]
            else:
                activities = [(key, subactivities.exists()) for key, subactivities in activities]
        activities_dict = dict((key, value) for key, value in activities)
        if no_duplicates:
            for key in ["articles_journal", "articles_collection"]:
                activities_dict.pop(key, None)
        return activities_dict
    else:
        # get annotated recent activities
        def get_recent_subactivities(key, subactivities):
            if key not in ['articles', 'articles_journal', 'articles_collection', 'theses', 'projects']:
                return subactivities.recent(limit=limit, recent_type=recent_type)
            elif key in ['articles', 'articles_journal', 'articles_collection'] : # means pure articles, not theses
                return worker.articles.recent_pure(limit=limit, recent_type=recent_type, pure_type=key)
            elif key == 'theses':
                return worker.articles.recent_theses(limit=limit, recent_type=recent_type)
            elif key == 'projects':
                return worker.projects.recent_public(limit=limit, recent_type=recent_type)
        activities_annotated = dict([(key, get_recent_subactivities(key, subactivities)) for key, subactivities in activities])
        # flatten the list and sort it
        sort_attr = {"cmp": compare} if recent_type == "date" else {"key": lambda x: x.id, "reverse": True}
        selected_activities = sorted(chain(*activities_annotated.values()), **sort_attr)
        selected_activities = selected_activities[:limit]
        # now exclude not selected activities from the annotated dict
        activities = [(key, [activity for activity in subactivities if activity in selected_activities]) for key, subactivities in activities_annotated.items()]
    # remove empty keys
    return dict((key, value) for key, value in activities)


def get_activities_last_added(workers=None, limit=None, from_date=None, to_date=None, raw=False):
    """Get last added publications

    workers
        instances of Worker
        None means 'added by anyone'
    limit
        maximum count of activities to return
        None for unlimited
        note, that it's not guranteed than exactle limit will be returned
        (even if there are enough data in database)
        because actstream journal can contain broken links
    from_date, to_date
        time range of interest
    raw
        if True doesn't fetch full details from db and return queryset to actstream

    returns
        if raw = False
            list of tuples (activity dictionary, timestamp)
        if raw = True
            quryset of tuples(timestamp, app_label, model_name, pk)
    """
    if workers is None:
        activities_qset = Action.objects.all().order_by("-timestamp")
    else:
        activities_qset = actors_stream(User, [worker.user.id for worker in workers if worker.user]).order_by("-timestamp")
    activities_qset = activities_qset.filter(
            verb__in=_ACTIVITY_STREAM_PUBLICATIONS_VERBS,
            target_object_id__isnull=False,
        ).values_list(
            "timestamp", "target_content_type__app_label",
            "target_content_type__model", "target_object_id"
        ) # this is correct only because all 'adding' actions contain target_object_id of the target (added) object. See #1104.
    if from_date:
        activities_qset = activities_qset.filter(timestamp__gte=from_date)
    if to_date:
        activities_qset = activities_qset.filter(timestamp__lte=to_date)
    if limit:
        activities_qset = activities_qset[:limit]
    if raw:
        return activities_qset
    activities = fetch_activities_compact(activities_qset)
    return activities


def fetch_activities_compact(activities):
    """Fetch all required data for given activities to pass to activities_last_added template

    activities
        quryset of tuples(timestamp, app_label, model_name, pk)

    returns list of tuples (timestamp, info_dict)
    note, that result list could be shorted if some of given activities were deleted
    """
    activities = [((models.get_model(app_label, model_name), pk), timestamp)
                    for timestamp, app_label, model_name, pk in activities
                 ]
    if not activities:
        return []
    activities, timestamps = zip(*activities)
    activities_grouped = defaultdict(list)
    for model, pk in activities:
        activities_grouped[model].append(pk)
    activities_cached = {}
    for model, pks in activities_grouped.iteritems():
        objs = model.objects.filter(pk__in=pks)

        related = ("workers", "workerships", "conference", "journal", "collection")
        related = filter(lambda x: hasattr(model, x), related)
        objs = objs.prefetch_related(*related)

        fields = set(i.name for i in model._meta.fields)
        fields.add("nominative_short")
        fields.add("title")
        fields.add("all_workers_string")
        fields.add("get_year")
        fields.discard("creator")
        fields.discard("xml")
        for obj in objs:
            data = {f: getattr(obj, f, None) for f in fields}
            data["get_absolute_url"] = obj.get_absolute_url()
            activities_cached[model, obj.pk] = data
    activities = [activities_cached.get((model, pk)) for model, pk in activities]
    activities = zip(activities, timestamps)
    activities = filter(lambda x: x[0] is not None, activities)
    return activities


def activities_dict2string(activities_dict):
    '''Takes a dict of activities of form {'activity_type': (count, sample_object)} and returns a summary string.'''
    return ", ".join(get_plural(activities_dict[key][1], activities_dict[key][0]) for key in _ACTIVITIES_TYPES if key in activities_dict)


def get_workers_activities_summary(workers):
    '''Returns a summary of activities of a list of workers.'''
    total_activities = defaultdict(list)
    activities_samples = {}  # for get_plural function
    for worker in workers:
        for key, activities in get_activities(worker).items():
            if key not in activities_samples:
                activities_samples[key] = activities[0]
            total_activities[key].extend(activities.values_list('id', flat=True))

    # count defended dissertations and advised dissertations together
    if 'dissertations_advised' not in activities_samples and 'dissertations_defended' in activities_samples:
        activities_samples['dissertations_advised'] = activities_samples['dissertations_defended']
    if 'dissertations_defended' in total_activities:
        total_activities['dissertations_advised'].extend(total_activities['dissertations_defended'])
        del total_activities['dissertations_defended']

    # count authored courses and courses teachings together
    if 'courses' not in activities_samples and 'courses_teachings' in activities_samples:
        activities_samples['courses'] = activities_samples['courses_teachings']
    if 'courses_teachings' in total_activities:
        total_activities['courses'].extend(total_activities['courses_teachings'])
        del total_activities['courses_teachings']

    for key, activities_ids in total_activities.items():
        total_activities[key] = (len(set(activities_ids)), activities_samples[key])

    return activities_dict2string(total_activities)


def get_work_from_workership(workership):
    '''Takes a workership, returns LinkedToWorkersModel instance that is linked through given workership.'''
    for activity in _ACTIVITIES_TYPES_SINGLE:
        try:
            return getattr(workership, activity)
        except (AttributeError, ObjectDoesNotExist):
            pass
    return None  # emergency exit, means something wrong
    # actually not, there are workership activities
    # that do not have any LinkedToWorkersModel linked, e.g. HonoraryMembership
    # and it can be called right before the workership is deleted, when the work has been already deleted

def check_activities_models():
    '''This function checks that all activities models (which are linked to from actstream 'adding' events) have required attributes (e.g. for activities_last_added templates). Prints errors, so use in shell only.'''
    objects = []
    for verb in _ACTIVITY_STREAM_PUBLICATIONS_VERBS: # get one instance of every object from actions' targets
        if Action.objects.filter(verb=verb, target_object_id__isnull=False).exists():
            obj = None
            i = 0
            while not obj:
                obj = Action.objects.filter(verb=verb, target_object_id__isnull=False)[i].target
                i += 1
            objects.append(obj)
    # check that all these instances have required attributes and they are not None
    for obj in objects:
        for attr in ("get_absolute_url", "title", "get_year", "nominative_short", "all_workers_string"):
            if not getattr(obj, attr, None):
                print "Object's attribute does not exist or is None: %s (id %d), %s" % (repr(obj), obj.id, attr)
    return objects
