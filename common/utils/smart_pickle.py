# -*- coding: utf-8 -*-
try:
    import cPickle as pickle
except ImportError:
    import pickle
import base64


def smart_pickle(obj, logger=None):
    '''Returns the pickled object. If an error is raised, logs the event, fails silently, returning the object converted in unicode.'''
    try:
        return base64.b64encode(pickle.dumps(obj))
    except:
        if logger:
            logger.warning("Error pickling object %s, storing unicode", obj)
        return unicode(obj)


def smart_unpickle(obj, logger=None):
    '''Returns the unpickled object. If an error is raised, logs the event, fails silently, returning the object converted in unicode.'''
    try:
        return pickle.loads(base64.b64decode(obj))
    except:
        if logger:
            logger.warning("Error unpickling object %s, loading unicode", obj)
        return unicode(obj)
