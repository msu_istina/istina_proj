# -*- coding:utf-8 -*-
def getDefaultContext(request, context_dict={}):
    from django.template import RequestContext
    return RequestContext(request, context_dict,)

def render_to_response(request, template_name, context_dict={}):
    from django.shortcuts import render_to_response as _render_to_response
    context = getDefaultContext(request, context_dict)
    return _render_to_response(template_name, context_instance=context)
