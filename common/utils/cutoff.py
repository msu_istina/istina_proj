from collections import defaultdict
import datetime


class CutoffTimer(object):
    """Simple tool to measure piece of code (actions) times and call count

    You can pass of actions for more rogue design
    >>> import time
    >>> timer = CutoffTimer(["cat1", "cat2"])
    >>> time.sleep(.2)
    >>> dt = timer.cutoff("cat1")
    >>> assert timer["cat1"][0] >= .1, timer["cat1"][0]
    >>> assert timer["cat2"][0] >= .0
    >>> assert dict(timer)["cat1"][0] == timer["cat1"][0]
    >>> timer["cat1"][1]
    1
    >>> timer["cat2"][1]
    0

    You can use actions=None in order to autogenerate actions
    >>> timer = CutoffTimer(None)
    >>> time.sleep(.2)
    >>> dt = timer.cutoff("cat1")
    >>> assert timer["cat1"][0] >= .1, timer["cat1"][0]
    >>> assert timer["cat2"][0] >= .0
    >>> assert dict(timer)["cat1"][0] == timer["cat1"][0]
    >>> timer["cat1"][1]
    1
    >>> timer["cat2"][1]
    0
    """
    def __init__(self, actions):
        """You can pass actions=None - than actions will be created on the fly"""
        self.actions = actions
        self.reset()

    def reset(self):
        self._last_cutoff = datetime.datetime.now()
        if self.actions is None:
            self._calls = defaultdict(int)
            self._times = defaultdict(int)
        else:
            self._calls = dict((i, 0) for i in self.actions)
            self._times = dict((i, 0) for i in self.actions)

    def cutoff(self, action, last_time=None):
        """Add time past from last call or last_time to the action times

        returns
            current time
        """
        t = datetime.datetime.now()
        self._calls[action] += 1
        if last_time is None:
            delta = t - self._last_cutoff
        else:
            delta = t - last_time
        self._times[action] += delta.seconds * 100000 + delta.microseconds / 10
        self._last_cutoff = t
        return self._last_cutoff

    def __getitem__(self, key):
        return self._times[key] / 100000., self._calls[key]

    def __iter__(self):
        if self.actions is not None:
            return ((action, self[action]) for action in self.actions)
        else:
            return ((action, self[action]) for action in self._times.keys())

    def sorted_stats(self, specific=True):
        if specific:
            denom = lambda c: c
        else:
            denom = lambda c: 1
        tms = [(t / denom(c), name) for name, (t, c) in  dict(self).iteritems()]
        return sorted(tms, reverse=True)

    def sorted_stats_fmt(self, specific=True):
        tms = self.sorted_stats(specific)
        tms = [("%0.5f" % t, name) for t, name in tms]
        return tms

    def __str__(self):
        return ", ".join(" ".join(x) for x in self.sorted_stats_fmt())
