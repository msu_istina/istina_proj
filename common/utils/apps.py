# -*- coding: utf-8 -*-

def get_app_name(instance):
    '''Returns the name of the application, in which the class of the instance is defined.'''
    return instance.__module__.split('.')[0]
