from reversion import VersionAdmin
from django.contrib import admin
from django.db import models
from reversion import default_revision_manager
from reversion.models import Version
from common.utils.cache import default_cache, get_deleted_versions_cache_key, instance_recovered
import types

def _get_deleted_cached(self, model_class, db=None, model_db=None):
        cache_key = get_deleted_versions_cache_key(model_class)
        deleted_version_pks = default_cache.get(cache_key)
        if deleted_version_pks is not None:
            return Version.objects.filter(pk__in=deleted_version_pks).order_by("-pk")
        else:
            deleted_versions = default_revision_manager.get_deleted_uncached(model_class, db, model_db)
            deleted_version_pks = deleted_versions.values_list('pk', flat=True)
            default_cache.set(cache_key, deleted_version_pks)
            return deleted_versions
default_revision_manager.get_deleted_uncached = default_revision_manager.get_deleted
default_revision_manager.get_deleted = types.MethodType(_get_deleted_cached, default_revision_manager)


class MyVersionAdmin(VersionAdmin):
    revision_manager = default_revision_manager

    def render_revision_form(self, request, obj, version, context, revert=False, recover=False):
        response = super(MyVersionAdmin, self).render_revision_form(request, obj, version, context, revert, recover)
        if request.method == "POST" and recover:
            ModelForm = self.get_form(request, obj)
            form = ModelForm(request.POST, request.FILES, instance=obj, initial=self.get_revision_form_data(request, obj, version))
            if form.is_valid():
                instance_recovered.send(sender=self, instance=obj)
        return response

class ForeignKeySmartSelectAdmin(admin.ModelAdmin):
    MODELS_EXCLUDED = ["django.contrib.contenttypes.models.ContentType"]
    def __init__(self, *args, **kwargs):
        super(ForeignKeySmartSelectAdmin, self).__init__(*args, **kwargs)
        raw_id_fields = self.raw_id_fields
        new_raw_id_fields = []
        for field in self.model._meta.fields:
            if isinstance(field, models.ForeignKey) and not(hasattr(field, 'related') and get_full_modelname(field.related.parent_model) in self.MODELS_EXCLUDED):
                new_raw_id_fields.append(field.name)
        self.raw_id_fields = list(raw_id_fields) + new_raw_id_fields


def get_full_modelname(model):
    return "%s.%s" % (model.__module__, model.__name__)


class MyModelAdmin(MyVersionAdmin, ForeignKeySmartSelectAdmin):
    def __init__(self, *args, **kwargs):
        super(MyModelAdmin, self).__init__(*args, **kwargs)


def register_with_versions(model, modeladmin=None):
    if not modeladmin:
        modeladmin = MyModelAdmin
    try:
        if not model._meta.managed or model._meta.proxy:
            admin.site.register(model)
        else:
            admin.site.register(model, modeladmin)
    except AttributeError:
        admin.site.register(model, modeladmin)


def make_RelatedOnlyFieldListFilter(attr_name, filter_title):

    class RelatedOnlyFieldListFilter(admin.SimpleListFilter):
        """Filter that shows only referenced options, i.e. options having at least a single object."""
        title = filter_title
        parameter_name = attr_name

        def lookups(self, request, model_admin):
            related_objects = set(filter(bool, [getattr(obj, attr_name) for obj in model_admin.model.objects.all()]))
            return [(related_obj.id, unicode(related_obj)) for related_obj in related_objects]

        def queryset(self, request, queryset):
            if self.value():
                return queryset.filter(**{'%s__id__exact' % attr_name: self.value()})
            else:
                return queryset

    return RelatedOnlyFieldListFilter

