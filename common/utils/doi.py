# -*- coding: utf-8 -*-
from urllib import urlencode
import urllib2
import httplib
import re
from socket import timeout


class DoiServiceError(Exception):
    pass


class TryDoiError(Exception):
    pass


def doi_to_bibtex(doi):

    try:
        opener = urllib2.build_opener()
        opener.addheaders = [
            ("Accept",'text/bibliography; style=bibtex'),
        ]
        doi_url = "http://dx.doi.org/"

        #truncate all before 10. e.g. http://dx.doi.org/... or DOI:...
        res = re.search('(10\.[0-9A-Za-z\.]*/\S*)', doi)
        likeDoi = False
        if res:
            prefix = doi[:res.span()[0]]
            if re.search("doi", prefix.lower()) and len(prefix) <= 20:
                likeDoi = True
            doi = doi[res.span()[0]:]

            if len(prefix) > 0 and not re.match("http:\/\/dx\.doi\.org\/|doi:", prefix.lower()):
                if likeDoi:
                    raise TryDoiError(u"Скорее всего, Вы пытаетесь добавить публикацию по идентификатору DOI. Попробуйте ввести DOI-идетнификатор в формате 10.1234/123456, http://dx.doi.org/10.1234/123456 или DOI: 10.1234/123456")
                else:
                    raise

        else:
            raise # not a DOI

        result = opener.open(doi_url,urlencode({'hdl':doi}), timeout=30).read()

        if not re.match("^@",result.strip()):
            raise DoiServiceError(u'Статья не найдена')
        return result.decode('utf-8')
    except urllib2.HTTPError, e:
        if e.code == 404:
            raise DoiServiceError(u'Статья не найдена')
        else:
            raise DoiServiceError(u'Сервис недоступен')
            # wrong DOI
    except timeout:
        raise DoiServiceError(u'Сервис недоступен')

    except (urllib2.URLError, httplib.BadStatusLine):
        raise DoiServiceError