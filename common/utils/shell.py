# -*- coding: utf-8 -*-
import re

from django.contrib.auth.models import User, Permission
from django.core.exceptions import MultipleObjectsReturned

from common.utils.email import print_user_email_template as ue, disclaimer_email as de
from common.utils.cache import invalidate_user as uuc, invalidate_all_users as uuca
from common.utils.reversion_utils import get_deleted_objects_versions, get_object_versions
from common.utils.user import get_user, get_profile, get_worker, set_active
from workers.models import Worker
from workers.utils import merge_workers, merge_users as mu, update_profile_name
from organizations.models import Representative
from journals.models import Journal
from publishers.models import Publisher
from dissertation_councils.models import DissertationCouncil


def print_user_info(username_or_email):
    '''Print user short info'''
    user = shell_get_user(username_or_email)
    print user.email, user.get_profile().fullname

def shell_get_user(username_or_email):
    '''Return user and print additional information if failed to return a single one. This function is for shell_plus use only, since it contains prints.'''
    try:
        user = get_user(username_or_email)
    except MultipleObjectsReturned:
        print "There are many users with this username:"
        users = User.objects.filter(email=username_or_email)
        print "\n".join("%s %s %s" % (item[1], item[0], item[2]) for item in users.values_list('username', 'email', 'is_active'))
        raise
    except User.DoesNotExist:
        print "There are no users with this username or email"
        raise
    else:
        return user

def shell_get_worker(username_or_email):
    return get_worker(shell_get_user(username_or_email))

def shell_set_active(username_or_email):
    user = shell_get_user(username_or_email)
    if not user.is_active:
        set_active(user)
        print "User is not active, made active"
    else:
        print "User is already active"

def print_names(username_or_email):
    user = shell_get_user(username_or_email)
    profile = get_profile(user)
    worker = get_worker(user)
    print "Profile name: %s;%s;%s" % (profile.lastname, profile.firstname, profile.middlename)
    print "Worker name: %s;%s;%s" % (worker.lastname, worker.firstname, worker.middlename)

def shell_update_profile_name(username, lastname, firstname, middlename):
    user = shell_get_user(username)
    profile = get_profile(user)
    print "Updating profile's name with new name:", "%s;%s;%s -> %s;%s;%s" % (profile.lastname, profile.firstname, profile.middlename, lastname, firstname, middlename)
    update_profile_name(user, lastname, firstname, middlename)

def shell_copy_name_from_profile(username):
    user = shell_get_user(username)
    profile = get_profile(user)
    worker = get_worker(user)
    print "Updating worker's name with profile's name:", "%s;%s;%s -> %s;%s;%s" % (worker.lastname, worker.firstname, worker.middlename, profile.lastname, profile.firstname, profile.middlename)
    worker.copy_name_from_profile()

def shell_merge_workers(*args, **kwargs):
    success, loglist = merge_workers(*args, **kwargs)
    print "\n".join(loglist)

def shell_representatives_emails():
    emails = list(Representative.objects.exclude(is_active=False).values_list('email', flat=True))
    emails.append("reznikova@rector.msu.ru") # editor
    emails.append("reznikova.am@gmail.com")
    while emails:
        print "{%s}" % " ".join(emails[:50]).replace(",", " ")
        emails = emails[50:]

def journals_info(*journal_names):
    journals = Journal.objects
    for journal_name in journal_names:
        journals = journals.filter(name__icontains=journal_name)
    journals = journals.order_by('-id').values_list('name', 'id', 'publisher__name', 'publisher__id')
    for journal in journals:
        print u" ".join(unicode(item) for item in journal)
    if journals:
        return Journal.objects.get(id=list(journals)[-1][1]) # return last journal (with min id) 
    else:
        return None
        
def publishers_info(publisher_name):
    publishers = Publisher.objects.filter(name__icontains=publisher_name)
    publishers = sorted(publishers, key=lambda pub: pub.journals.count())
    for publisher in publishers:
        print publisher.journals.count(), publisher.name, publisher.id, publisher.city
    if publishers:
        return list(publishers)[-1]
    else:
        return None

def users_with_permission(codename):
    perm = Permission.objects.get(codename=codename)
    # users that have permission through groups 
    group_users = User.objects.filter(groups__permissions=perm)
    print "%d users have %s permission through groups: \n%s" % (len(group_users),
        codename, ", ".join(user.username for user in group_users))
    # users that have permission set directly  
    direct_users = User.objects.filter(user_permissions=perm)
    print "\n%d users have %s permission set directly: \n%s" % (len(direct_users),
        codename, ", ".join(user.username for user in direct_users))

def dissertation_councils_info(*council_number_parts):
    councils = DissertationCouncil.objects.find(council_number_parts)
    good_council = None
    for council in councils:
        print council.id, council.dissertations.count(), council.number, \
            (council.organization or council.organization_str)
        if not good_council and re.match(ur"^(К|Д)\ \d{3}\.\d{3}\.\d{2}$", council.number) and council.organization:
            good_council = council
    if not good_council:
        good_council = councils[0]
    print "DissertationCouncil.objects.merge('goldan', (%s), %d)" % (
        ", ".join(str(council.id) for council in councils if council != good_council),
        good_council.id)
    if councils:
        return list(councils)[-1]
    else:
        return None

def number_of_workers_with_typos_fixed():
    count = 0
    i = 0
    worker_count = Worker.objects.count()
    for w in Worker.objects.all(): 
        i += 1
        print "%d/%d" % (i, worker_count)
        # find an alias with different lastname than worker's
        # but the first letter of the lastnames is the same (to exclude aliases in another language)
        # if such alias exists, count += 1
        count += int(w.aliases.exclude(lastname=w.lastname).filter(lastname__startswith=w.lastname[0]).exists())
        
    # print percent of workers for which there is at least one alias with a typo in lastname,
    # that shows how important is our typo-fixing similar workers retrieval function;
    # first test showed result 6.2% which means it is not important at all;
    # and surely does not give benefit that justifies its super long execution time.
    print (float(count) / worker_count)*100    

def deactivate_user(username):
    user = shell_get_user(username)
    if user:
        profile = user.get_profile()
        if profile.worker:
            profile.worker = None
            profile.save()
        user.is_active = False
        user.save()


# shortcuts
ui = print_user_info
gu = shell_get_user
gw = shell_get_worker
sa = shell_set_active
pn = print_names
upn = shell_update_profile_name
cnp = shell_copy_name_from_profile
mw = shell_merge_workers
rep = shell_representatives_emails
ji = journals_info
pi = publishers_info
uwp = users_with_permission
dci = dissertation_councils_info
du = deactivate_user