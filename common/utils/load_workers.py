# -*- coding: utf-8 -*-
"""
Read workers data from specified fixture file and loads into specified database.
For workers in the database that have the same lastname, 
their firstname and middlename is updated according to fixture (BE CAREFUL!)
Workers which lastname is not found in db, are inserted into db.
The script is intended to be used on a almost clean database when it is 
not possible to delete all workers, and it is needed to update their data.
"""

import os
import sys
from os.path import join

sys.path.insert(0, join(os.environ['VIRTUAL_ENV'], 'django-istina', 'istina'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'


from optparse import OptionParser
usage = "Usage: python common/utils/load_workers.py database_name fixture (e.g. science)"
parser = OptionParser(usage)
(options, args) = parser.parse_args()
if len(args) != 2:
    parser.error("incorrect number of arguments. Specify database name and fixture file.")

database = args[0]
filename = args[1]

import settings
try:
    settings.DATABASES['default'] = settings.DATABASES[database]
except KeyError:
    print "ERROR: Incorrect database name"
    exit()

print "Will load data to database '%s' from %s" % (database, filename)

import django.db.models # it should be imported after database setting is set
from workers.models import Worker
from django.utils.simplejson import load

f = open(filename)
data = load(f)
f.close()

updated = 0
created = 0
read = len(data)

for worker in data:
    # find worker with the same lastname in the database and update his name
    same = Worker.objects.filter(lastname=worker['fields']['lastname'])
    if same:
        for w in same:
            w.firstname = worker['fields']['firstname']
            w.middlename = worker['fields']['middlename']
            w.save()
            updated += 1
    if not same:
        Worker.objects.create(firstname=worker['fields']['firstname'], middlename=worker['fields']['middlename'], lastname=worker['fields']['lastname'])
        created += 1
print "Read data about %d workers" % read
print "Updated data about %d workers" % updated
print "Created %d workers" % created

