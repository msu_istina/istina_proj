import operator

from common.utils.uniqify import uniqify


def autocomplete_helper(model, field_name, term, limit):
    """Select objects which field_name starts with (first) or contains (then) term

    Return list of no more then limit model's instances
    """
    items = model.objects.filter(**{("%s__istartswith" % field_name):term}).order_by(field_name)[:limit]
    items = list(items)
    if len(items) < limit:
        items_more = model.objects.filter(**{("%s__icontains" % field_name):term}).order_by(field_name)[:(limit - len(items))]
        items += list(items_more)
        items = uniqify(items, idfun=operator.attrgetter("id"))
    return items
