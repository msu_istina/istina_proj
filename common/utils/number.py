# -*- coding: utf-8 -*-

from decimal import Decimal, ROUND_DOWN

def quantize(value):
    '''Round down to two decimal places'''
    return Decimal(str(value)).quantize(Decimal('.01'), rounding=ROUND_DOWN)

def quantize_int(value):
    '''Round down to integer'''
    return Decimal(str(value)).quantize(Decimal('1'), rounding=ROUND_DOWN)
