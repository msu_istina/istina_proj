_COLORS = {
    'HEADER':  '\033[95m', # purple
    'OKBLUE':  '\033[94m', # blue
    'OKGREEN': '\033[92m', # green
    'WARNING': '\033[93m', # yellow
    'ERROR':   '\033[91m', # red
    'END':     '\033[0m'   # end color
}

def colored(s, color):
    try:
        return _COLORS[color] + s + _COLORS['END']
    except KeyError:
        return s
    
def header(s):  return colored(s, 'HEADER')  
def okblue(s):  return colored(s, 'OKBLUE')
def okgreen(s): return colored(s, 'OKGREEN')
def warning(s): return colored(s, 'WARNING')    
def error(s):   return colored(s, 'ERROR')        
