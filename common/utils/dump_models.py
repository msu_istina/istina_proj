# -*- coding: utf-8 -*-
from database_choose import database
import django.db.models # should go after database_choose, since in the database_choose database is choosed
from django.db.utils import DatabaseError
from commands import getoutput
from colors import error, okgreen


print "Will dump data from database '%s' to fixtures/%s" % (database, database)
getoutput("mkdir -p fixtures/%s" % database)
for model in django.db.models.get_models():
    app = model._meta.app_label
    name = model.__name__
    try:
        count = model.objects.count()
        print "%s.%s %s" % (app, name, okgreen(str(count)) if count else '')
        if count and app not in ['sessions', 'contenttypes'] and not (app == 'auth' and name == 'Permission'):
            getoutput("./manage.py dumpdata --indent=2 --natural --database='%(database)s' %(app)s.%(name)s > fixtures/%(database)s/%(app)s.%(name)s.json" % {'database': database, 'app': app, 'name': name})
    except DatabaseError:
        print "%s.%s %s" % (app, name, error("error"))

# make timestamp
import datetime
timestamp = datetime.datetime.now().strftime("%d.%m.%Y.%H:%M:%S")
getoutput("echo %s > fixtures/%s/timestamp.txt" % (timestamp, database))
