from django.templatetags.static import static

def get_args(string, defaults):
    "Used to get arguments of template filter from string"
    arg_list = [arg.strip() for arg in string.split(',')]
    args = []
    for arg in arg_list:
        if arg.isdigit():
            args.append(int(arg))
        else:
            args.append(arg)
    for i in range(len(arg_list), len(defaults)):
        args.append(defaults[i])
    return args

def get_icon_by_bool(boool, only_true=False):
    icon = {
        True: "accept-32.png",
        False: "decline-32.png"
    }[boool]
    if not boool and only_true:
        icon = None
    return (u'<img src="%s" width="16px" />' % static(u"base/img/%s" % icon)) if icon else ""

def get_icon_by_level(level):
    icon = {
        1: "accept-32.png",
        0: "decline-32.png"
    }.get(level, "star_half.png")
    return u'<img src="%s" width="16px" />' % static(u"base/img/%s" % icon)
