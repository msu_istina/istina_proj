# -*- coding: utf-8 -*-
from common.management.commands.show_user_email_template import get_user_email_template

def print_user_email_template(username):
    print get_user_email_template(username)

def disclaimer_email():
    message = """От лица разработчиков системы ИСТИНА хочу сообщить,
что определение критериев для поощрения сотрудников МГУ
находится за рамками нашей ответственности.
Пожалуйста, задайте свой вопрос сотруднику ректората
Александру Викторовичу Чертовичу.
Контакты: chertov@polly.phys.msu.ru, тел. 939-1013."""
    print message
    email_signature()

def email_signature():
    message = """--
С уважением,
Денис Голомазов, группа разработчиков системы ИСТИНА,
http://istina.msu.ru"""
    print message

def get_sent_user_ids(logfilenames):
    all_user_ids = []
    for filename in logfilenames:
        f = open(filename)
        data = f.read()
        f.close()
        user_ids = []
        try:
            for line in data.split("\n"):
                if not line or line.startswith('SKIP'):
                    continue
                user_ids.append(int(line.split()[0]))
        except:
            import pdb; pdb.set_trace()
        print "Count: %d, from %d to %d" % (len(user_ids), user_ids[0], user_ids[-1])
        all_user_ids.extend(user_ids)
    return all_user_ids
