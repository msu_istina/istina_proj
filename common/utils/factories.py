# -*- coding: utf-8 -*-

import factory
from factory.django import DjangoModelFactory
from django.contrib.auth.models import User
from workers.models import Worker, WorkerAlias, Profile
from userprofile.signals import create_profile
from django.db.models.signals import post_save


class WorkerFactory(DjangoModelFactory):
    FACTORY_FOR = Worker

    lastname = factory.Sequence(lambda n: u"Фамилия_%s" % n)
    middlename = u'Отчество'
    firstname = u'Имя'


class SimilarWorkerFactory(WorkerFactory):
    lastname = u'Фамилия'
    firstname = factory.Sequence(lambda n: u"Имя%s" % n)


class WorkerAliasFactory(DjangoModelFactory):
    FACTORY_FOR = WorkerAlias

    worker = factory.SubFactory(WorkerFactory)
    lastname = factory.LazyAttribute(lambda a: a.worker.lastname + '_Alias')
    middlename = factory.LazyAttribute(lambda a: a.worker.middlename)
    firstname = factory.LazyAttribute(lambda a: a.worker.firstname)


class ProfileFactory(DjangoModelFactory):
    FACTORY_FOR = Profile

    worker = factory.SubFactory(WorkerFactory)
    lastname = factory.SelfAttribute('worker.lastname')
    middlename = factory.SelfAttribute('worker.middlename')
    firstname = factory.SelfAttribute('worker.firstname')


class UserFactory(DjangoModelFactory):
    FACTORY_FOR = User

    username = factory.Sequence(lambda n: u"Username_%s" % n)
    email = factory.LazyAttribute(lambda obj: '%s@example.com' % obj.username)
    profile = factory.RelatedFactory(ProfileFactory, 'user')

    @classmethod
    def _generate(cls, create, attrs):
        """Override the default _generate() to disable the post-save signal.
        Taken from factory_boy documentation (see common recipes from project docs).
        """

        post_save.disconnect(
            sender = User,
            dispatch_uid = "django-profile.userprofile.signals.create_profile")

        user = super(UserFactory, cls)._generate(create, attrs)

        post_save.connect(
            create_profile,
            sender = User,
            dispatch_uid = "django-profile.userprofile.signals.create_profile")

        return user
