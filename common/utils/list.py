# -*- coding: utf-8 -*-
import pytils
from django.db import models

def flatten(lst):
    new_list = []
    for sublist in lst:
        new_list.extend(sublist)
    return new_list


def is_tuple_list(obj):
    return isinstance(obj, (list, tuple, models.query.QuerySet))


def in_or_equal(obj1, obj2):
    if is_tuple_list(obj2) and obj1 in obj2:
        return True
    if not is_tuple_list(obj2) and obj1 == obj2:
        return True
    return False


def get_plural(obj, count):
    '''Returns correct plural form for specified number of objects of the same class as obj.'''
    if not obj:
        return
    if count == 1:
        return str(count) + " " + obj.nominative
    return pytils.numeral.get_plural(count, ", ".join((obj.nominative_plural, obj.genitive_plural, obj.genitive_plural_full)))

def list_powerset(lst):
    '''Computes the power set of a list of distinct elements.'''
    # the power set of the empty set has one element, the empty set
    result = [[]]
    for x in lst:
        # for every additional element in our set
        # the power set consists of the subsets that don't
        # contain this element (just take the previous power set)
        # plus the subsets that do contain the element (use list
        # comprehension to add [x] onto everything in the
        # previous power set)
        result.extend([subset + [x] for subset in result])
    return result
 
def powerset(s):
    '''Simply converts the input and output from lists to sets'''
    return frozenset(map(frozenset, list_powerset(list(s))))    
