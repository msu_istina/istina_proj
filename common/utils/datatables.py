from collections import defaultdict
def data_list_to_datatables(data_list, id):
    data = defaultdict(str)
    for i, x in enumerate(data_list):
        data[i] = x
    data['DT_RowId'] = id
    return data