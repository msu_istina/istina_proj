# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
import datetime


def validate_year(year):
    if not datetime.MINYEAR <= year <= datetime.MAXYEAR:
        raise ValidationError(u"Введите правильное значение года.")


def validate_non_negative_number(number):
    if number < 0:
        raise ValidationError(u"Введите неотрицательное число.")
