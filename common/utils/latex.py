from django.conf import settings

import tempfile, subprocess, os, shutil
import re

BAD_CHARS = u'\x02\x03\x04\x05\x06\x07\x08\x0b\x0c\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e'
BAD_CHARS_RE = re.compile(u"(%s)" % (u"|".join(BAD_CHARS)))


def latex_to_pdf(latex_content, fname='report', pdflatex=None):
    "Convert latex file represented by string latex_content into pdf"
    supported_pdflatex = ['pdflatex', 'xelatex']

    latex_content = BAD_CHARS_RE.sub("", latex_content)

    d = tempfile.mkdtemp(suffix='', prefix='tmp_istina_latex_')
    with open(os.path.join(d, fname + '.tex'), 'wb') as f:
        f.write(latex_content.encode("utf8"))
    origWD = os.getcwd() # remember our original working directory
    os.chdir(d)

    if settings.DEBUG:
        proc = subprocess.Popen(['cp', fname + '.tex', '/tmp/last.processed.tex'])
        proc.communicate()

    commands_to_try = supported_pdflatex
    if pdflatex in supported_pdflatex:
        commands_to_try = [pdflatex] + supported_pdflatex

    for command in commands_to_try:
        proc = subprocess.Popen([command, '-interaction', 'batchmode', fname + '.tex'])
        proc.communicate()
        proc = subprocess.Popen([command, '-interaction', 'batchmode', fname + '.tex'])
        proc.communicate()

        if settings.DEBUG:
            proc = subprocess.Popen(['cp', fname + '.pdf', '/tmp/last.processed.pdf'])
            proc.communicate()

        try:
            f = open(os.path.join(d, fname + '.pdf'), 'rb')
        except IOError:
            f = None
        else:
            break

    if f:
        pdf = f.read()
        f.close()
    else:
        pdf = ""

    os.chdir(origWD) # restore working directory
    shutil.rmtree(d)

    return pdf
