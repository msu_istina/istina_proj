def uniqify(seq, idfun=None):
    """Return elements from seq with uniq idfun preserving order

    seq is iterable
    idfun is a callable or none
    returns list
    """
    if idfun is None:
        idfun = lambda x: x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        if marker in seen:
            continue
        seen[marker] = 1
        result.append(item)
    return result
