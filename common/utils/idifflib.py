import difflib
import heapq
import re


def get_close_matches(word, possibilities, n=3, cutoff=0.6, key=lambda x: x, stopwords=[]):
    """Same as difflib.get_close_matches but also retuns scores and supports key function

    return list of tuples (posibility, score) sorted by score desc
    >>> m = get_close_matches("appel", ["ape", "apple", "peach", "puppy"])
    >>> for k, v in m: print "%s %.2f" % (k, v)
    apple 0.80
    ape 0.75

    The key argument plays the same role as in sort()
    >>> m = get_close_matches("appel", [("ape", 1), ("apple", 2), ("peach", 3)], \
                         key=lambda x: x[0])
    >>> for k, v in m: print "%s %.2f" % (k, v)
    ('apple', 2) 0.80
    ('ape', 1) 0.75
    """

    if not n > 0:
        raise ValueError("n must be > 0: %r" % (n,))
    if not 0.0 <= cutoff <= 1.0:
        raise ValueError("cutoff must be in [0.0, 1.0]: %r" % (cutoff,))

    if stopwords:
        stop_regex = '(' + '|'.join(stopwords) + ')\S*\s*'
        word = re.sub(stop_regex, '', word.lower())
        old_key = key
        key = lambda x: re.sub(stop_regex, '', old_key(x).lower())
        if not word or len(word) < 5: return []

    result = []
    s = difflib.SequenceMatcher()
    s.set_seq2(word)
    for x in possibilities:
        s.set_seq1(key(x))
        if s.real_quick_ratio() >= cutoff and \
           s.quick_ratio() >= cutoff and \
           s.ratio() >= cutoff:
            result.append((s.ratio(), x))

    # Move the best scorers to head of list and sort by score
    result = ((i[1], i[0]) for i in heapq.nlargest(n, result))
    result = sorted(result, key=lambda x: x[1], reverse=True)
    return result
