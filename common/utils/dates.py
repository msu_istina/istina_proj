# -*- coding: utf-8 -*-
import datetime
from django.template.defaultfilters import date as date_filter

def get_nearest_year():
    today = datetime.datetime.today()
    year = today.year
    if today < datetime.datetime(year, 7, 1):
        year -= 1
    return year

def extract_years_list(years_str):
    if '-' in years_str: # e.g. 2005-2010
        start = int(years_str.split('-')[0])
        end = int(years_str.split('-')[1])
        years = [start+i for i in range(end-start+1)]
    else:
        years = [int(years_str)]
    return years

def dates_text(startdate, enddate, fallback_year=None):
    format_date = lambda date, frmt="j E Y": date_filter(date, frmt)
    if not startdate and not enddate:
        return str(fallback_year or "")
    if startdate and not enddate:
        return u"с %s" % format_date(startdate)
    if enddate and not startdate:
        return u"до %s" % format_date(enddate)
    if startdate == enddate:
        return format_date(startdate)
    if startdate.year == enddate.year:
        if startdate.month == enddate.month:
            return u"%s-%s" % (format_date(startdate, "j"), format_date(enddate))
        return u"%s - %s" % (format_date(startdate, "j E"), format_date(enddate))
    return u"%s - %s" % tuple(map(format_date, (startdate, enddate)))

def get_academic_range(n_years):
    """Return range of years to contain n_years of academic years.
    If it is 2015 now, then get_academic_range(5) = [2010, 2011, 2012, 2013, 2014, 2015]
    (2010-2011, 2011-2012, 2012-2013, 2013-2014, 2014-2015).
    It works together with CourseTeaching.total_hours_in_period method.
    """
    return [datetime.date.today().year-n_years+i for i in range(n_years+1)]
