import csv
from cStringIO import StringIO
from django.http import HttpResponse
from django.utils.html import strip_tags
import HTMLParser
import codecs

def export_to_csv_response(headers, rows, filename, bomb=False):
    """
    Exports a list of rows in csv.
    Also strips html tags and converts html entities to strings.
    If bomb is True, BOM_UTF8 byte inserted into output stream; if bomb
    is not False (or None) and not equals to True, then it should be a bomb value.
    Returns a HttpResponse with a file.
    """
    csvfile = StringIO()
    if bomb:
        if bomb == True:
            csvfile.write(codecs.BOM_UTF8)
        else:
            csvfile.write(bomb)
    writer = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_ALL)
    encode = lambda cell: unicode(cell).encode('utf8')
    parser = HTMLParser.HTMLParser()
    process = lambda cell: '' if cell is None else encode(parser.unescape(strip_tags(cell)))
    writer.writerow(map(encode, headers))
    for row in rows:
        writer.writerow(map(process, row))
    response = HttpResponse(csvfile.getvalue(), content_type='application/octet-stream')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response
