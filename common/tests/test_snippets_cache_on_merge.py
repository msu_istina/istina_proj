# -*- coding: utf-8; -*-
from __future__ import unicode_literals

from django.core.cache import cache
from django.utils import unittest

from common.templatetags.activity_description import get_cache_key, get_activity_type, invalidate_activity_description, show_activity_description
from publications.models import Article
from workers.utils import merge_workers
from workers.models import Worker


class SnippetsCacheOnWorkersMergeTest(unittest.TestCase):
    def setUp(self):
        workers = [Worker.objects.create(lastname="Doe{}".format(i), firstname="John", middlename="") for i in range(2)]
        articles = [Article.objects.create(title=title) for title in ("Lorem Ipsum", "Duis aute irure")]
        for worker, article in zip(workers, articles):
            worker.position = 0
            worker.fullname_orig = worker.fullname
            article.add_authors([worker])
            article.save()
            invalidate_activity_description(article)

        # reread models to drop worker.position or any other temp atributes
        self.workers = [Worker.objects.get(pk=obj.id) for obj in workers]
        self.articles = [Article.objects.get(pk=obj.id) for obj in articles]

    def test_main(self):
        """Let two identical workers has some article. Then after merge snippet for deleted should be recalculated"""
        my_get_cache = lambda article: cache.get(get_cache_key(get_activity_type(article), article, True))
        my_recalculate_cache = lambda article: show_activity_description(article, True)

        target_article = self.articles[1]  # snipppet for this article should be changed
        show_activity_description(target_article, True)
        self.assertTrue(my_get_cache(target_article) is not None, "Cache wasn't created")
        merge_workers([self.workers[1]], self.workers[0], delete=True)
        print "Merge"
        self.assertTrue(my_get_cache(target_article) is None, "Cache wasn't invalidated!")
        self.assertEqual(list(self.articles[0].authors.all()), list(self.articles[1].authors.all()), "Auhtors failed to merge")
