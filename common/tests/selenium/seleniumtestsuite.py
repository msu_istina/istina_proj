# -*- coding: utf-8; -*-
"""This module provides various classes used by selenium tests.

The main class is SeleniumTestCase, which provides basic methods
specific for selenium tests, for example proper setUp/tearDown.
"""

from django.test import LiveServerTestCase

from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException

import types
import re

from .settings import SELENIUM_DRIVER, SELENIUM_TIMEOUT


class SeleniumTestCase(LiveServerTestCase):

    """This class is intended to be base class of all our selenium tests.

    It provides required setUp/tearDown methods and other useful methods/attributes.
    """

    @staticmethod
    def webdriver_setUp():
        driver = getattr(webdriver, SELENIUM_DRIVER)()
        driver.implicitly_wait(SELENIUM_TIMEOUT)

        return driver

    def setUp(self):
        super(SeleniumTestCase, self).setUp()

        self.driver = self.webdriver_setUp()
        self.base_url = self.live_server_url
        self.verificationErrors = []
        self.accept_next_alert = True

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException:
            return False
        return True

    def css_is_element_present(self, element_id):
        try:
            self.driver.find_element_by_css_selector(element_id)
        except NoSuchElementException:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)
        super(SeleniumTestCase, self).tearDown()

    @staticmethod
    def add_get_window_method(driver):
        create_new_window_link = """var d=document,a=d.createElement('a');
        a.target='_blank';a.href='{0}';a.innerHTML='.';d.body.appendChild(a);
        return a;"""
        remove_new_window_link = """var a=arguments[0];a.parentNode.removeChild(a);"""

        def get_window(self, url):
            parent_handle = self.current_window_handle
            prev_handles = set(self.window_handles)

            a = self.execute_script(create_new_window_link.format(url))
            a.click()
            self.execute_script(remove_new_window_link, a)

            child_handle = (set(self.window_handles) - prev_handles).pop()

            return parent_handle, child_handle

        driver.get_window = types.MethodType(get_window, driver)


class SeleniumToolbox(SeleniumTestCase):

    """This class provides methods for low-level element testing.

    These methods rely on CSS selectors as they are faster than XPath selectors
    and also uniform across browsers.
    """

    def css_list_children(self, element_id, child_type):
        """Return a list of all children of a given type for a given element."""
        return self.driver.find_elements_by_css_selector(
            "{element_id} > {child_type}".format(
                element_id = element_id, child_type = child_type))

    def _css_list_single_element_children(
            self, element_id, child_type, expected_children_num):
        """Return a list of all children of a given type for a given element.

        This method is not intended to be used directly. Its purpose is
        to be a placeholder of common code for css_test_single_* methods below.
        """
        children = self.css_list_children(element_id, child_type)

        self.assertEqual(len(children), expected_children_num,
                         "The number of discovered {child_type} children "
                         "of {element_id} element is {children_num}. "
                         "Expected {expected_children_num}.".format(
                             child_type = child_type,
                             element_id = element_id,
                             children_num = len(children),
                             expected_children_num = expected_children_num)
                         )

        return children

    def _css_list_multiple_elements_children(
            self, parent_id, element_id, child_type, list_of_lists,
            full_inspect = False):
        """Return a list of lists of all children of a given type
        of all elements of a given type for a given parent.

        This method is not intended to be used directly. Its purpose is
        to be a placeholder of common code for css_test_multiple_* methods below.

        Fifth argument is a list of lists. If a sublist is empty and
            full_inspect == False, then we will not try to discover
            the corresponding child.
        Sixth argument specifies whether we should try to discover all children.
            Defaults to False. It reduces test runtime as we don't have to wait
            till selenium fails to find non-existing elements.
            This is useful when we know in advance that certain elements
            should be ignored or have no children of a given type.
        """
        elements_num = len(self.css_list_children(parent_id, element_id))
        expected_elements_num = len(list_of_lists)

        self.assertEqual(elements_num, expected_elements_num,
                         "The number of discovered {element_id} elements "
                         "is {elements_num}. Expected {expected_elements_num}.".format(
                             element_id = element_id,
                             elements_num = elements_num,
                             expected_elements_num = expected_elements_num)
                         )

        if full_inspect:
            # if full_inspect == True, then
            # we search for all child_type children of each element
            elements_children = [self.css_list_children(
                "{parent_id} > {element_id}:nth-of-type({element_num})".format(
                    parent_id = parent_id,
                    element_id = element_id,
                    element_num = 1 + element_num),
                child_type) for element_num in xrange(elements_num)]
        else:
            # if full_inspect == False, then
            # we search only for child_type children
            # that correspond to non-empty lists of texts
            elements_children = [[]] * elements_num
            for element_num in xrange(elements_num):
                if list_of_lists[element_num]:
                    elements_children[element_num] = self.css_list_children(
                        "{parent_id} > "
                        "{element_id}:nth-of-type({element_num})".format(
                            parent_id = parent_id,
                            element_id = element_id,
                            element_num = 1 + element_num),
                        child_type)

        for i in xrange(elements_num):
            children_num = len(elements_children[i])
            expected_children_num = len(list_of_lists[i])
            self.assertEqual(children_num, expected_children_num,
                             "The number of discovered {child_type} children "
                             "of {i}-th {element_id} element is {children_num}. "
                             "Expected {expected_children_num}.".format(
                                 child_type = child_type,
                                 i = i,
                                 element_id = element_id,
                                 children_num = children_num,
                                 expected_children_num = expected_children_num)
                             )

        return elements_children

    def css_test_multiple_elements_children_text(
            self, parent_id, element_id, child_type, texts,
            full_inspect = False, strip_spaces = False):
        """Test that all children of a given type of all given elements
           for a given parent have given texts.

        Fifth argument is a list of lists of strings that store texts to compare with.
            The first sublist of this list holds texts that children
            of the first element should have (in the given order) and so on.
        Sixth argument specifies whether we should try to discover all children.
            Defaults to False. This way it doesn't bother with children that
            have empty text lists. It reduces test runtime as we don't have to
            wait till selenium fails to find non-existing elements.
            This is useful when we know in advance that certain elements
            should be ignored or have no children of a given type.
        Seventh argument specifies whether space characters are stripped from
            the beggining and the end of text prior to comparing.

        Example: consider a fieldset with four paragraphs.
                 The first and the last paragraphs have some text under the span tag.
                 In order to compare that span children of these paragraphs
                 have proper texts, one should call this method as follows:
                 self.css_test_multiple_elements_children_text(
                         "fieldset", "p", "span",
                         [
                             ["Span text in the first paragraph."],
                             [],
                             [],
                             ["Span text in the last paragraph."]
                         ]
                     )
        """
        elements_num = len(texts)
        elements_children = self._css_list_multiple_elements_children(
            parent_id, element_id, child_type, texts, full_inspect)

        for i in xrange(elements_num):
            for j in xrange(len(elements_children[i])):
                text = elements_children[i][j].text

                # strip spaces if needed
                if strip_spaces:
                    text = text.strip()

                self.assertEqual(texts[i][j], text)

    def css_test_multiple_elements_children_attribute(
            self, parent_id, element_id, child_type, attribute, values,
            full_inspect = False, strip_spaces = False):
        """Test that all children of a given type of all given elements
        for a given parent have given values of a given attribute.

        Sixth argument is a list of lists of strings that store values to compare with.
            The first sublist of this list holds values that children
            of the first element should have (in the given order) and so on.
        Seventh argument specifies whether we should try to discover all children.
            Defaults to False. This way it doesn't bother with children that
            have empty value lists. It reduces test runtime as we don't have to
            wait till selenium fails to find non-existing elements.
            This is useful when we know in advance that certain elements
            should be ignored or have no children of a given type.
        Eighth argument specifies whether space characters are stripped from
            the beggining and the end of value prior to comparing.
        """
        elements_num = len(values)
        elements_children = self._css_list_multiple_elements_children(
            parent_id, element_id, child_type, values, full_inspect)

        for i in xrange(elements_num):
            for j in xrange(len(elements_children[i])):
                value = elements_children[i][j].get_attribute(attribute)

                # strip spaces if needed
                if strip_spaces:
                    value = value.strip()

                self.assertEqual(values[i][j], value)

    def css_test_single_element_children_text(
            self, element_id, child_type, texts,
            strip_spaces = False):
        """Test that all children of a given type for a given element have given texts.

        Fourth argument is a list of strings that store texts to compare with.
            The first string holds text that the first child should have and so on.
        Fifth argument specifies whether space characters are stripped from
            the beggining and the end of text prior to comparing.
        """
        children_num = len(texts)
        children = self._css_list_single_element_children(
            element_id, child_type, children_num)

        for i in xrange(children_num):
            text = children[i].text

            # strip spaces if needed
            if strip_spaces:
                text = text.strip()

            self.assertEqual(texts[i], text)

    def css_test_single_element_children_attribute(
            self, element_id, child_type, attribute, values,
            strip_spaces = False):
        """Test that all children of a given type for a given element
        have given values of a given attribute.

        Fifth argument is a list of strings that store values to compare with.
            The first string holds value that the first child should have and so on.
        Sixth argument specifies whether space characters are stripped from
            the beggining and the end of value prior to comparing.
        """
        children_num = len(values)
        children = self._css_list_single_element_children(
            element_id, child_type, children_num)

        for i in xrange(children_num):
            value = children[i].get_attribute(attribute)

            # strip spaces if needed
            if strip_spaces:
                value = value.strip()

            self.assertEqual(values[i], value)

    def css_test_single_element_text(
            self, element_id, text, strip_spaces = False):
        """Test that a given element has a given text."""
        elem_text = self.driver.find_element_by_css_selector(element_id).text

        # strip spaces if needed
        if strip_spaces:
            elem_text = elem_text.strip()

        self.assertEqual(text, elem_text)

    def css_test_single_element_attribute(
            self, element_id, attribute, value, strip_spaces = False):
        """Test that a given element has a given value of a given attribute."""
        elem_value = self.driver.find_element_by_css_selector(
            element_id).get_attribute(attribute)

        # strip spaces if needed
        if strip_spaces:
            elem_value = elem_value.strip()

        self.assertEqual(value, elem_value)

    def css_set_multiple_elements(self, element_id, values):
        """Set (append) elements to given values."""
        elements = self.driver.find_elements_by_css_selector(element_id)
        for element_num in xrange(len(elements)):
            elements[element_num].send_keys(values[element_num])

    def css_set_single_element(self, element_id, value):
        """Set (append) element to given value."""
        self.driver.find_element_by_css_selector(element_id).send_keys(value)

    def stb_test_istina_page_title(self, title):
        self.assertEqual(
            u"{title} | ИСТИНА - Интеллектуальная Система "
            u"Тематического Исследования "
            u"НАучно-технической информации".format(title = title),
            self.driver.title)

    def stb_test_messages_text(
            self, texts, message_class = "message success",
            strip_spaces = False):
        messages_num = len(texts)
        messages = self._css_list_single_element_children(
            'div[class^="messages"]',
            'div[class^="{klass}"]'.format(klass = message_class),
            messages_num)

        for i in xrange(messages_num):
            # we assume that text to check is under the first div.span* tag
            text = messages[i].find_element_by_css_selector(
                'div[class^="span"]').text

            # strip spaces if needed
            if strip_spaces:
                text = text.strip()

            self.assertEqual(texts[i], text)


class SeleniumSelect(SeleniumTestCase):

    def select_option_select_by_value(self, select_id, value):
        """Select option of a select element by a given value."""
        Select(self.driver.find_element_by_css_selector(
            select_id)).select_by_value(value)

    def select_option_select_by_visible_text(self, select_id, text):
        """Select option of a select element by a given visible text.

        Note: we rely on select_by_visible_text, which uses XPATH internally.
        """
        Select(self.driver.find_element_by_css_selector(
            select_id)).select_by_visible_text(text)


class SeleniumRadioButton(SeleniumTestCase):
    # radiobutton requires name attribute per w3 standart

    def radiobutton_check_by_id(self, name, choice_id, selector = None):
        if selector is None:
            selector = 'input[type="radio"]'
        selector += u'[name="{name}"][id="{choice_id}"]'.format(
            name = name, choice_id = choice_id)
        self.driver.find_element_by_css_selector(selector).click()

    def radiobutton_check_by_value(self, name, value, selector = None):
        if selector is None:
            selector = 'input[type="radio"]'
        selector += u'[name="{name}"][value="{value}"]'.format(
            name = name, value = value)
        self.driver.find_element_by_css_selector(selector).click()


class SeleniumCheckbox(SeleniumTestCase):

    def checkbox_check(self, checkbox_id):
        self.driver.find_element_by_css_selector(checkbox_id).click()


class TestSingleInputFormByCSS(SeleniumTestCase):

    """This class provides routines for testing single input forms."""

    def single_input_form_test_value(self, form_id, value):
        """Test that text field has a given value."""
        try:
            self.assertEqual(
                self.driver.find_element_by_css_selector(
                    '{form_id} > input[type="text"]'.format(
                        form_id = form_id)).get_attribute("value"),
                value)
        except AssertionError as e:
            self.verificationErrors.append(str(e))

    def single_input_form_is_present(self, form_id, value = ""):
        """Test that input form is present,
        i.e. has a submit button and a text field with a given value.
        """
        self.assertTrue(self.css_is_element_present(form_id))
        self.assertTrue(self.css_is_element_present(
            '{form_id} > input[type="text"]'.format(form_id = form_id)))
        self.assertTrue(self.css_is_element_present(
            '{form_id} > input[type="submit"]'.format(form_id = form_id)))

        self.single_input_form_test_value(form_id, value)

        return True

    def single_input_form_clear(self, form_id):
        """Clear text field."""
        self.driver.find_element_by_css_selector(
            '{form_id} > input[type="text"]'.format(
                form_id = form_id)).clear()

    def single_input_form_set(self, form_id, value):
        """Set (append) text field to a given value."""
        self.driver.find_element_by_css_selector(
            '{form_id} > input[type="text"]'.format(
                form_id = form_id)).send_keys(value)

    def single_input_form_submit_empty(self, form_id):
        """Submit a given form with an empty query."""
        self.single_input_form_clear(form_id)
        self.driver.find_element_by_css_selector(
            '{form_id} > input[type="submit"]'.format(
                form_id = form_id)).submit()

    def single_input_form_submit(self, form_id, value):
        """Submit a given form with a given query."""
        self.single_input_form_set(form_id, value)
        self.driver.find_element_by_css_selector(
            '{form_id} > input[type="submit"]'.format
            (form_id = form_id)).submit()


class TestFieldsetByCSS(SeleniumTestCase):

    """This class provides routines for testing fieldsets."""

    def fieldset_test_values(self, fieldset_id, **kwargs):
        """Test that given fields have given values."""
        for field_id in kwargs:
            try:
                self.assertEqual(
                    self.driver.find_element_by_css_selector(
                        "{fieldset_id} {field_id}".format(
                            fieldset_id = fieldset_id,
                            field_id = field_id)).get_attribute("value"),
                    kwargs[field_id])
            except AssertionError as e:
                self.verificationErrors.append(str(e))

    def fieldset_is_present(self, fieldset_id, **kwargs):
        """Test that fieldset is present and has given fields with given values."""
        self.assertTrue(self.css_is_element_present(fieldset_id))
        for field_id in kwargs:
            self.assertTrue(self.css_is_element_present(
                "{fieldset_id} {field_id}".format(
                    fieldset_id = fieldset_id, field_id = field_id)))

        self.fieldset_test_values(fieldset_id, **kwargs)

        return True

    def fieldset_set(self, fieldset_id, **kwargs):
        """Set (append) given fields to given values."""
        for field_id in kwargs:
            self.driver.find_element_by_css_selector(
                "{fieldset_id} {field_id}".format(
                    fieldset_id = fieldset_id, field_id = field_id)).send_keys(
                kwargs[field_id])

    def fieldset_clear(self, fieldset_id, **kwargs):
        """Clear given fields."""
        for field_id in kwargs:
            self.driver.find_element_by_css_selector(
                "{fieldset_id} {field_id}".format(
                    fieldset_id = fieldset_id, field_id = field_id)).clear()


class TestMultipleInputFormByCSS(TestFieldsetByCSS):

    """This class provides routines for testing multiple input forms with fieldset."""

    def multiple_input_form_fieldset_is_present(self, form_id, prefix = u"", **kwargs):
        """Test that fieldset in multiple input form is present."""
        self.assertTrue(self.css_is_element_present(form_id))

        input_ids = {'input[id="{prefix}{field_id}"]'.format(
            prefix = prefix, field_id = field_id): kwargs[field_id]
            for field_id in kwargs}
        self.fieldset_is_present("{form_id} > fieldset".format(
            form_id = form_id), **input_ids)

        return True

    def multiple_input_form_is_present(self, form_id, **kwargs):
        """Test that multiple input form is present,
        i.e. fieldset is present and submit button is present.
        """
        self.multiple_input_form_fieldset_is_present(form_id, **kwargs)

        self.assertTrue(self.css_is_element_present(
            '{form_id} input[type="submit"]'.format(form_id = form_id)))

        return True

    def multiple_input_form_fieldset_test_values(self, form_id, prefix = u"", **kwargs):
        input_ids = {'input[id="{prefix}{field_id}"]'.format(
            prefix = prefix, field_id = field_id): kwargs[field_id]
            for field_id in kwargs}
        self.fieldset_test_values("{form_id} > fieldset".format(
            form_id = form_id), **input_ids)

    def multiple_input_form_fieldset_set(self, form_id, prefix = u"", **kwargs):
        input_ids = {'input[id="{prefix}{field_id}"]'.format(
            prefix = prefix, field_id = field_id): kwargs[field_id]
            for field_id in kwargs}
        self.fieldset_set("{form_id} > fieldset".format(
            form_id = form_id), **input_ids)

    def multiple_input_form_fieldset_clear(self, form_id, prefix = u"", **kwargs):
        input_ids = {'input[id="{prefix}{field_id}"]'.format(
            prefix = prefix, field_id = field_id): kwargs[field_id]
            for field_id in kwargs}
        self.fieldset_clear("{form_id} > fieldset".format(
            form_id = form_id), **input_ids)

    def multiple_input_form_submit_empty(self, form_id, prefix = u"", **kwargs):
        self.multiple_input_form_fieldset_clear(form_id, prefix, **kwargs)
        self.driver.find_element_by_css_selector(
            '{form_id} input[type="submit"]'.format(form_id = form_id)).submit()

    def multiple_input_form_submit(self, form_id, prefix = u"", **kwargs):
        self.multiple_input_form_fieldset_set(form_id, prefix, **kwargs)
        self.driver.find_element_by_css_selector(
            '{form_id} input[type="submit"]'.format(form_id = form_id)).submit()

    def multiple_input_form_fieldset_test_maxlength_field(
            self, form_id, max_length, prefix = u"", **kwargs):
        """Test that field with a given maximal length of value properly truncates input."""
        self.multiple_input_form_fieldset_set(form_id, prefix, **kwargs)
        shortened_kwargs = {field_id: kwargs[field_id][:max_length]
                            for field_id in kwargs}
        self.multiple_input_form_fieldset_test_values(
            form_id, prefix, **shortened_kwargs)


class TestEmailFormByCSS(TestMultipleInputFormByCSS):

    """This class provides routines for testing input forms with email fields.

    RFC 2822 forbids the usage of certain characters inside an email address.
    Address is constructed as follows:
        addr-spec   =  local-part "@" domain
        local-part  =  atom *("." atom) / quoted string
        domain      =  atom *("." atom) / domain-literal
    An atom is a sequence of printable ASCII characters except space or any of the following:
        ()<>@,;:\".[]
        Note: a typical russian keyboard adds '№' char to this list,
        but it can be legally used in IDN domain.
        Indeed, u"№".encode('idna').decode('ascii') == u"no".
    Positively speaking, this means that the valid constituents of an atom are the following:
        !#$%&'*+-/=?^_`{|}~
        0123456789
        ABCDEFGHIJKLMNOPQRSTUVWXYZ
        abcdefghijklmnopqrstuvwxyz
    Also, domain part is covered by other RFCs, thus not all chars
    that are allowed for atom are allowed for domain.
    """

    # needed for mail.outbox
    from django.core import mail

    # do not actually send any emails, but store them in memory
    EMAIL_BACKEND = "mail.backends.locmem.EmailBackend"

    email_invalid_chars = u'()<>@,;:\".[] №'
    email_valid_atoms = {
        "lowercase": "abcdefghijklmnopqrstuvwxyz",
        "uppercase": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "digits": "0123456789",
        "chars": "!#$%&'*+-/=?^_`{|}~"
    }

    # common mistakes made by human
    invalid_emails = [
        u"mailbox.example.com", u"mailbox@example@com",
        u"@mailboxexample.com", u"mailboxexample.com@",
        u"mailbox@.com",
        u"mailbox@127.0.0.1",
    ]
    # invalid local-part
    invalid_emails += [c.join((u"mailbox", u"@example.com"))
                       for c in email_invalid_chars]
    # invalid domain
    # '№' can be used in IDN domain, so we exclude it here
    invalid_emails += [c.join((u"mailbox@example", u".com"))
                       for c in email_invalid_chars[:-1]]
    # domain cannot begin or end with hyphens
    invalid_emails += [u"mailbox@-example.com"]
    invalid_emails += [u"mailbox@example-.com"]

    # common mistakes made by human
    extra_invalid_emails = [u"mailbox@example.com."]
    # some examples from Django test suite
    extra_invalid_emails += [u'"\\\012"@example.com',
                             u'test@example.com\n\n<script src="x.js">']

    valid_emails = []
    for mailbox in sorted(email_valid_atoms.values()):
        valid_emails += [mailbox + u"@example.com",
                         mailbox + u"@[127.0.0.1]",
                         mailbox.join((u"", u".", u"@example.com"))]

    for domain in sorted(email_valid_atoms.values())[1:]:
        valid_emails += [domain.join((u"mailbox@", u".com"))]
        valid_emails += [domain.join((u"mailbox@", u"---", u".com"))]

    # some examples from Django test suite
    extra_valid_emails = [u'"test@test"@example.com', u'"\\\011"@example.com']

    # set this to True to make email_form_test methods
    # also process extra emails defined above,
    # which are known to cause fails with Django 1.4
    with_extra_emails = False

    @staticmethod
    def validate_email(ustring, use_default = True):
        """Test if a given unicode string is a valid email address.

        What is a valid email address is a pretty complex question.
        Django 1.4 has EmailValidator class for this purpose.
        Unfortunately, it is not bug-free. Therefore we also provide
        a solution from django trunk as of 02-11-2013, which is used
        when use_default == False.
        """
        if use_default:
            from django.core.exceptions import ValidationError
            from django.core.validators import validate_email
            try:
                validate_email(ustring)
            except ValidationError:
                return False
            else:
                return True
        else:
            # implementation of EmailValidator class from trunk as a function
            user_regex = re.compile(
                # dot-atom
                r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*$"
                # quoteded-string
                r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"$)',
                re.IGNORECASE)
            domain_regex = re.compile(
                # domain
                r'(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}|[A-Z0-9-]{2,})$'
                # literal form, ipv4 address (SMTP 4.1.3)
                r'|^\[(25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}\]$',
                re.IGNORECASE)

            if not ustring or '@' not in ustring:
                return False

            user_part, domain_part = ustring.rsplit('@', 1)

            if not user_regex.match(user_part):
                return False

            if not domain_regex.match(domain_part):
                # try for possible IDN domain-part
                try:
                    domain_part = domain_part.encode('idna').decode('ascii')
                    if not domain_regex.match(domain_part):
                        return False
                    else:
                        return True
                except UnicodeError:
                    return False

            return True

    @staticmethod
    def _construct_callback(func, *args, **kwargs):
        def callback():
            return func(*args, **kwargs)
        return callback

    def email_form_test_invalid(
            self, form_id, callback_function, email_field_id, **kwargs):
        """Test email field with invalid input.

        Third argument is a callback function that accepts no arguments.
            Is is called each time the form with invalid email is submitted.
            Use it to perform the needed error checks.
            Use _construct_callback() method to create such function.
        """
        invalid_emails = self.invalid_emails + (
            self.extra_invalid_emails if self.with_extra_emails else [])

        for email in invalid_emails:
            self.assertFalse(self.validate_email(email, use_default = False))
            kwargs.update({email_field_id: email})
            self.multiple_input_form_submit(form_id, **kwargs)
            callback_function()
            self.multiple_input_form_fieldset_clear(form_id, **kwargs)

    def email_form_test_valid(
            self, form_id, callback_function, email_field_id, **kwargs):
        """Test email field with valid input.

        Third argument is a callback function that accepts no arguments.
            Is is called each time the form with invalid email is submitted.
            Use it to perform the needed error checks.
            Use _construct_callback() method to create such function.
        """
        valid_emails = self.valid_emails + (
            self.extra_valid_emails if self.with_extra_emails else [])

        for email in valid_emails:
            self.assertTrue(self.validate_email(email, use_default = False))
            kwargs.update({email_field_id: email})
            self.multiple_input_form_submit(form_id, **kwargs)
            callback_function()
            self.multiple_input_form_fieldset_clear(form_id, **kwargs)


class TestTableByCSS(SeleniumToolbox):

    """This class provides routines for testing tables.

    According to specs, permitted table content is as follows (in this order):
        * an optional <caption> element,
        * zero or more <colgroup> elements,
        * an optional <thead> element,
        * one of the two alternatives:
            * one <tfoot> element, followed by:
                either zero or more <tbody> elements
                or one or more <tr> elements
            * elements specified below followed by an optional <tfoot> element:
                either zero or more <tbody> elements
                or one or more <tr> elements
    """

    def table_list_rows(self, table_id,
                        with_thead = False, with_tfoot = False, tbody = True):
        rows = []
        tfoot_rows = []

        if with_thead:
            rows += self.css_list_children(table_id, "thead > tr")

        if with_tfoot:
            tfoot_rows += self.css_list_children(table_id, "tfoot > tr")

        if tbody:
            table_id += " > tbody"

        rows += self.css_list_children(table_id, "tr")
        return rows + tfoot_rows

    def table_row_list_cells(self, row, with_th = False):
        # no need to worry about non-child selector here
        # as specs allow only td and th under tr
        # if your table does not follow specs, then it is your fault
        selector = "td"
        if with_th:
            selector += ", th"

        return row.find_elements_by_css_selector(selector)

    def table_list_cells(self, table_id,
                         with_thead = False, with_tfoot = False,
                         with_th = False, tbody = True):

        cells = []
        for row in self.table_list_rows(table_id, with_thead, with_tfoot, tbody):
            cells.append(self.table_row_list_cells(row, with_th))

        return cells

    def table_get_row_by_num(self, table_id, row_num,
                             with_thead = False, with_tfoot = False, tbody = True):
        rows = self.table_list_rows(table_id, with_thead, with_tfoot, tbody)

        try:
            return rows[row_num]
        except IndexError:
            raise IndexError(
                "Specified row number {row_num} "
                "is out of range 0..{rows_num}".format(
                    row_num = row_num, row_nums = len(rows) - 1))

    def table_get_cell_by_num(self, table_id, row_num, cell_num,
                              with_thead = False, with_tfoot = False,
                              with_th = False, tbody = True):

        row = self.table_get_row_by_num(
            table_id, row_num,
            with_thead, with_tfoot, tbody)
        cells = self.table_row_list_cells(row, with_th)

        try:
            return cells[cell_num]
        except IndexError:
            raise IndexError(
                "Specified cell number {cell_num} "
                "is out of range 0..{cells_num}".format(
                    cell_num = cell_num, cells_num = len(cells) - 1))

    def _table_row_list_cells(self, table_id, row_num, expected_cells_num,
                              with_thead = False, with_tfoot = False,
                              with_th = False, tbody = True):

        cells = self.table_row_list_cells(
            self.table_get_row_by_num(
                table_id, row_num, with_thead, with_tfoot, tbody),
            with_th)
        cells_num = len(cells)

        self.assertEqual(cells_num, expected_cells_num,
                         "The number of discovered cells of {row_num}-th row "
                         "is {cells_num}. Expected {expected_cells_num}.".format(
                             row_num = row_num, cells_num = cells_num,
                             expected_cells_num = expected_cells_num)
                         )

        return cells

    def table_row_test_texts(self, table_id, row_num, texts,
                             with_thead = False, with_tfoot = False,
                             with_th = False, tbody = True, strip_spaces = False):
        cells_num = len(texts)
        cells = self._table_row_list_cells(
            table_id, row_num, cells_num,
            with_thead, with_tfoot, with_th, tbody)

        for i in xrange(cells_num):
            text = cells[i].text

            # strip spaces if needed
            if strip_spaces:
                text = text.strip()

            self.assertEqual(texts[i], text)

    def table_row_test_values(self, table_id, row_num, attribute, values,
                              with_thead = False, with_tfoot = False,
                              with_th = False, tbody = True, strip_spaces = False):
        cells_num = len(values)
        cells = self._table_row_list_cells(
            table_id, row_num, cells_num,
            with_thead, with_tfoot, with_th, tbody)

        for i in xrange(cells_num):
            value = cells[i].get_attribute(attribute)

            # strip spaces if needed
            if strip_spaces:
                value = value.strip()

            self.assertEqual(values[i], value)

    def _table_list_cells(self, table_id, values,
                          with_thead = False, with_tfoot = False,
                          with_th = False, tbody = True):

        rows = self.table_list_rows(table_id, with_thead, with_tfoot, tbody)
        rows_num = len(rows)
        expected_rows_num = len(values)

        self.assertEqual(rows_num, expected_rows_num,
                         "The number of discovered rows is {rows_num}. "
                         "Expected {expected_rows_num}.".format(
                             rows_num = rows_num,
                             expected_rows_num = expected_rows_num)
                         )

        cells = [self.table_row_list_cells(row, with_th)
                 for row in self.table_list_rows(
                     table_id, with_thead, with_tfoot, tbody)]

        for i in xrange(rows_num):
            cells_num = len(cells[i])
            expected_cells_num = len(values[i])

            self.assertEqual(cells_num, expected_cells_num,
                             "The number of discovered cells in {i}-th row "
                             "is {cells_num}. Expected {expected_cells_num}.".format(
                                 i = i, cells_num = cells_num,
                                 expected_cells_num = expected_cells_num)
                             )

        return cells

    def table_test_texts(self, table_id, texts,
                         with_thead = False, with_tfoot = False,
                         with_th = False, tbody = True, strip_spaces = False):

        rows_num = len(texts)
        cells = self._table_list_cells(
            table_id, texts,
            with_thead, with_tfoot, with_th, tbody)

        for i in xrange(rows_num):
            for j in xrange(len(cells[i])):
                text = cells[i][j].text

                # strip spaces if needed
                if strip_spaces:
                    text = text.strip()

                self.assertEqual(texts[i][j], text)

    def table_test_values(self, table_id, attribute, values,
                          with_thead = False, with_tfoot = False,
                          with_th = False, tbody = True, strip_spaces = False):

        rows_num = len(values)
        cells = self._table_list_cells(
            table_id, values,
            with_thead, with_tfoot, with_th, tbody)

        for i in xrange(rows_num):
            for j in xrange(len(cells[i])):
                value = cells[i][j].get_attribute(attribute)

                # strip spaces if needed
                if strip_spaces:
                    value = value.strip()

                self.assertEqual(values[i][j], value)
