# -*- coding: utf-8; -*-
"""This module provides various classes to ease form processing using selenium."""

from django.forms.widgets import (
    TextInput, PasswordInput, HiddenInput,
    DateInput, DateTimeInput, TimeInput,
    Textarea)
from django.forms.widgets import (
    CheckboxInput, CheckboxSelectMultiple,
    Select, SelectMultiple, NullBooleanSelect,
    RadioSelect)
from django.forms.widgets import boolean_check
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support.select import Select as SeleniumSelect
from seleniumtestsuite import SeleniumTestCase

import functools


TEXTWIDGETS = (TextInput, PasswordInput, HiddenInput,
               DateInput, DateTimeInput, TimeInput)

TEXTAREAWIDGETS = (Textarea,)

CHECKBOXWIDGETS = (CheckboxInput,)

SELECTWIDGETS = (Select, SelectMultiple, NullBooleanSelect)

MULTIPLECHECKWIDGETS = (RadioSelect, CheckboxSelectMultiple)


class SeleniumCachedObject(object):

    def __init__(self, update_cache, *args, **kwargs):
        self._update_cache = functools.partial(update_cache, *args, **kwargs)
        self._cache = self._update_cache()

    @property
    def webobj(self):
        try:
            self._cache.tag_name
        except StaleElementReferenceException:
            self._cache = self._update_cache()
        return self._cache

    def __getattr__(self, name):
        return self.webobj.get_attribute(name)

    def click(self):
        self.webobj.click()

    def clear(self):
        self.webobj.clear()


class SeleniumTextInputField(SeleniumCachedObject):

    def __init__(self, id, form):
        self.id = id
        self.form = form
        super(SeleniumTextInputField, self).__init__(
            form.driver.find_element_by_css_selector,
            "input#{id}".format(id = id))

    def set(self, value):
        self.webobj.clear()
        self.webobj.send_keys(value)


class SeleniumTextAreaField(SeleniumCachedObject):

    def __init__(self, id, form):
        self.id = id
        self.form = form
        super(SeleniumTextAreaField, self).__init__(
            form.driver.find_element_by_css_selector,
            "textarea#{id}".format(id = id))

    def set(self, value):
        self.webobj.clear()
        self.webobj.send_keys(value)


class SeleniumSelectField(SeleniumCachedObject):

    def __init__(self, id, form):
        self.id = id
        self.form = form
        super(SeleniumSelectField, self).__init__(
            form.driver.find_element_by_css_selector,
            "select#{id}".format(id = id))

    def set(self, value):
        SeleniumSelect(self.webobj).select_by_visible_text(value)


class SeleniumCheckboxField(SeleniumCachedObject):

    def __init__(self, id, form):
        self.id = id
        self.form = form
        super(SeleniumCheckboxField, self).__init__(
            form.driver.find_element_by_css_selector,
            'input[type="checkbox"][id="{id}"]'.format(id = id))

    def set(self, value):
        if not boolean_check(value) == self.webobj.is_checked():
            self.webobj.check()


class SeleniumForm(object):

    def create_cached_field(self, field_id, field_type):
        if field_type in TEXTWIDGETS:
            return SeleniumTextInputField(field_id, self)
        elif field_type in TEXTAREAWIDGETS:
            return SeleniumTextAreaField(field_id, self)
        elif field_type in SELECTWIDGETS:
            return SeleniumSelectField(field_id, self)
        elif field_type in CHECKBOXWIDGETS:
            return SeleniumCheckboxField(field_id, self)
        else:
            raise ValueError("Unsupported field type: {0}".format(field_type))

    def __init__(self, driver, form, fields):
        self.driver = driver
        self.form = form
        self.fields = {}

        for field in fields:
            boundfield_obj = form[field]
            field_id = boundfield_obj.id_for_label
            field_type = type(boundfield_obj.field.widget)
            self.fields[field] = self.create_cached_field(field_id, field_type)

    def __getitem__(self, key):
        return self.fields[key]

    def __setitem__(self, key, value):
        if key in self.fields:
            self.fields[key].set(value)
        else:
            boundfield_obj = self.form[key]
            field_id = boundfield_obj.id_for_label
            field_type = type(boundfield_obj.field.widget)
            cached_field = self.create_cached_field(field_id, field_type)
            cached_field.set(value)
            self.fields[key] = cached_field

    def set(self, data):
        for field in data:
            self.fields[field].set(data[field])

    def clear(self, fields = None):
        fields = self.fields.keys() if fields is None else fields
        for field in fields:
            self.fields[field].clear()

    def validate(self, data):
        for field in data:
            if not self.fields[field].value == data[field]:
                raise AssertionError(
                    "Field {field} has {value} value. "
                    "Expected {expected_value}.".format(
                        field = field, value = self.fields[field].value,
                        expected_value = data[field]))

    def submit(self):
        self.fields.values()[0].webobj.submit()


class SeleniumFormAutomaton(SeleniumTestCase):

    def process_form(self, form_obj, fields = None, initial_data = None):
        form_fields = fields.keys() if fields is not None else []
        if initial_data is not None:
            form_fields = set(form_fields).union(initial_data.keys())

        form = SeleniumForm(self.driver, form_obj, form_fields)

        if initial_data is not None:
            form.validate(initial_data)
        if fields is not None:
            form.set(fields)

        return form
