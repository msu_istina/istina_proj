<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <title>Test Suite</title>
</head>
<body>
<table id="suiteTable" cellpadding="1" cellspacing="1" border="1" class="selenium"><tbody>
<tr><td><b>Test Suite</b></td></tr>
<tr><td><a href="startpage.tc">Главная страница</a></td></tr>
<tr><td><a href="register.tc">Регистрация</a></td></tr>
<tr><td><a href="activate_user.tc">Активация пользователя</a></td></tr>
<tr><td><a href="login.tc">Вход</a></td></tr>
<tr><td><a href="link_to_worker.tc">Привязка к сотруднику</a></td></tr>
<tr><td><a href="employment.tc">Добавление места работы</a></td></tr>
<tr><td><a href="add_article.tc">Добавление статьи</a></td></tr>
</tbody></table>
</body>
</html>
