# -*- coding: utf-8; -*-
"""This module provides framework for Django objects verification."""

from django.db.models.fields import AutoField
from django.db.models.fields.related import ForeignKey, OneToOneField
from django.db.models.fields.related import ManyToManyField
from collections import namedtuple, defaultdict

from organizations.models import Organization

IGNORED_FIELDS = {AutoField}  # practically you should never verify AutoField
DIRECT_FIELDS = {ForeignKey, ManyToManyField, OneToOneField}
SPECIAL_FIELDS = DIRECT_FIELDS.union(IGNORED_FIELDS)


# DjangoFieldInfo(field, model, direct, m2m),
# where field is the Field instance,
# model is the model containing this field (None for local fields),
# direct is True if the field exists on this model,
# and m2m is True for many-to-many relations.
#
# When 'direct' is False, 'field' is the corresponding RelatedObject
# for this field (since the field doesn't have an instance associated with it).
#
# This is needed for crystal clear processing of tuples returned by Django's
# _meta.get_field_by_name method, which supports indirect fields
# as opposed to _meta.get_field
DjangoFieldInfo = namedtuple(
    'DjangoFieldInfo', ('field', 'model', 'direct', 'm2m'))


# Basically this is a mutable namedtuple
class FieldInfo(object):
    __slots__ = ('type', 'value')

    def __init__(self, *args, **kwargs):
        for key, value in zip(self.__slots__, args):
            setattr(self, key, value)
        for key, value in kwargs.items():
            setattr(self, key, value)

    def as_dict(self):
        return dict((attr, getattr(self, attr)) for attr in self.__slots__)

    def as_tuple(self):
        return tuple(getattr(self, attr) for attr in self.__slots__)

    def __iter__(self):
        for attr in self.__slots__:
            yield getattr(self, attr)


class SeleniumModelNode(object):

    """This class defines node that represents a model instance object.

    Node requires a name that can be used to ease tree construction.
    Node also requires a model of the instance it represents.
    Finally node requires dict of fields or existing instance of model object.

    Node stores three dicts: one for regular fields and two separate ones
    for direct and related relationship fields.
    Each dict has fields' names as keys and FieldInfo tuples as values.
    FieldInfo stores field's type and field's value.
    For regular fields this is straightforward.
    For relationship fields allowed values are SeleniumModelNode, None or string
    for ForeignKey and OneToOneField (including backwards OneToOneField)
    and lists of the said types for ManyToManyField (including backwards)
    and backwards ForeignKey fields.
    For backwards fields a related direct field type is stored.

    Node can be constructed by providing either dict of fields with their values
    or existing instance of the appropriate model. In the latter case all direct
    regular fields with their values are copied to regular fields dict.
    """

    def __add_field(self, field_name, field_value):
        model_field = DjangoFieldInfo(
            *self.model._meta.get_field_by_name(field_name))
        field_type = type(model_field.field)

        if model_field.direct:
            if field_type not in SPECIAL_FIELDS:
                self.regular_fields[field_name] = FieldInfo(
                    field_type, field_value)
            elif field_type in DIRECT_FIELDS:
                self.direct_fields[field_name] = FieldInfo(
                    field_type, field_value)
        else:
            self.related_fields[field_name] = FieldInfo(
                type(DjangoFieldInfo(
                    *self.model._meta.get_field_by_name(field_name)).field.field),
                field_value)

    def __init__(self, name, model, dict_or_instance):
        self.name = name
        self.model = model
        self.regular_fields = {}
        self.direct_fields = {}
        self.related_fields = {}

        if not isinstance(dict_or_instance, model):
            # if model instance is not supplied,
            # then we assume it is a dict of fields
            for field_name, field_value in dict_or_instance.items():
                self.__add_field(field_name, field_value)
        else:
            # if model instance is supplied,
            # then we copy all regular direct fields
            # this is the best guessing logic available,
            # but should be still enough for simple cases

            # store instance
            self.obj = dict_or_instance

            # store pk field so we can reliably verify this object
            pk = dict_or_instance._meta.pk
            # only primary keys that AutoField are allowed
            if not isinstance(pk, AutoField):
                raise TypeError("Unsupported primary key type: {type}".format(
                    type = type(pk)))

            self.regular_fields[pk.name] = FieldInfo(
                AutoField, pk.value_from_object(dict_or_instance))

            # process the rest of the fields
            for field in dict_or_instance._meta.fields:
                field_type = type(field)
                if field_type not in SPECIAL_FIELDS:
                    self.regular_fields[field.name] = FieldInfo(
                        field_type, field.value_from_object(dict_or_instance))

    @property
    def fields(self):
        return dict(
            self.regular_fields.items() +
            self.direct_fields.items() +
            self.related_fields.items()
        )

    @property
    def lookup(self):
        return {field_name: self.regular_fields[field_name].value
                for field_name in self.regular_fields}

    def verify_node(self):
        return self.model.objects.get(**self.lookup)

    def __getitem__(self, key):
        if key in self.regular_fields:
            return self.regular_fields[key].value
        elif key in self.direct_fields:
            return self.direct_fields[key].value
        elif key in self.related_fields:
            return self.related_fields[key].value
        else:
            raise KeyError("{name} node has no {key} field.".format(
                name = self.name, key = key))

    def __setitem__(self, key, new_value):
        if key in self.regular_fields:
            self.regular_fields[key].value = new_value
        elif key in self.direct_fields:
            self.direct_fields[key].value = new_value
        elif key in self.related_fields:
            self.related_fields[key].value = new_value
        else:
            self.__add_field(key, new_value)


class SeleniumModelTree(object):

    """This class represents tree of SeleniumModelNode nodes and provides
    methods to verify all nodes in the given tree and relations between them.
    """
    @classmethod
    def model_tree_lookup_field_node(cls, field_name, field_value, node, nodes):
        """Return appropriate node for the given relationship field
        or None if field's value is None
        """
        if isinstance(field_value, SeleniumModelNode) or field_value is None:
            # if field_value is node or None, then return it
            return field_value
        elif isinstance(field_value, str):
            # if field_value is a string,
            # then search for a node with appropriate name
            if not field_value:
                # if an empty string is given, then try to guess the node's name
                # from current node's name and field_name
                field_value = node.name + "__" + field_name

            try:
                rnode, = filter(lambda n: n.name == field_value, nodes)
                return rnode
            except ValueError:
                raise LookupError(
                    "Unable to find unique node for {node} node's field {field}".format(
                        node = node.name, field = field_name))

        raise TypeError(
            "Unsupported type of {node} node's {field} field value: {type}".format(
                node = node.name, field = field_name, type = type(field_value)))

    @classmethod
    def model_tree_lookup_fk_node(cls, field_name, node, nodes):
        field_value = node.direct_fields[field_name].value
        node.direct_fields[field_name].value = cls.model_tree_lookup_field_node(
            field_name, field_value, node, nodes)

    @classmethod
    def model_tree_lookup_o2o_node(cls, field_name, node, nodes):
        field_value = node.direct_fields[field_name].value
        node.direct_fields[field_name].value = cls.model_tree_lookup_field_node(
            field_name, field_value, node, nodes)

    @classmethod
    def model_tree_lookup_m2m_node(cls, field_name, node, nodes):
        field_values = node.direct_fields[field_name].value
        if field_values:
            node.direct_fields[field_name].value = [
                cls.model_tree_lookup_field_node(
                    field_name, field_value, node, nodes)
                for field_value in field_values]

    @classmethod
    def model_tree_lookup_manyrelated_node(cls, field_name, node, nodes):
        field_values = node.related_fields[field_name].value
        if field_values:
            node.related_fields[field_name].value = [
                cls.model_tree_lookup_field_node(
                    field_name, field_value, node, nodes)
                for field_value in field_values]

    @classmethod
    def model_tree_lookup_singlerelated_node(cls, field_name, node, nodes):
        field_value = node.related_fields[field_name].value
        node.related_fields[field_name].value = cls.model_tree_lookup_field_node(
            field_name, field_value, node, nodes)

    @classmethod
    def model_tree_contruct_tree(cls, root, nodes, instance_index):
        instance_index[root.model] += 1

        for dfield in root.direct_fields:
            field_type = root.direct_fields[dfield].type
            {
                ForeignKey: cls.model_tree_lookup_fk_node,
                OneToOneField: cls.model_tree_lookup_o2o_node,
                ManyToManyField: cls.model_tree_lookup_m2m_node
            }[field_type](dfield, root, nodes)

            field_type, field_value = root.direct_fields[dfield]
            if field_value:
                if not field_type == ManyToManyField:
                    cls.model_tree_contruct_tree(
                        field_value, nodes, instance_index)
                else:
                    for node in field_value:
                        cls.model_tree_contruct_tree(
                            node, nodes, instance_index)
            else:
                instance_index[root.model._meta.get_field(dfield).rel.to]

        for rfield in root.related_fields:
            field_type = root.related_fields[rfield].type
            {
                ForeignKey: cls.model_tree_lookup_manyrelated_node,
                OneToOneField: cls.model_tree_lookup_singlerelated_node,
                ManyToManyField: cls.model_tree_lookup_manyrelated_node
            }[field_type](rfield, root, nodes)

            field_type, field_value = root.related_fields[rfield]
            if field_value:
                if field_type == OneToOneField:
                    cls.model_tree_contruct_tree(
                        field_value, nodes, instance_index)
                else:
                    for node in field_value:
                        cls.model_tree_contruct_tree(
                            node, nodes, instance_index)

    def __init__(self, root, *nodes):
        self.root = root
        self.instance_index = defaultdict(int)
        self.model_tree_contruct_tree(root, nodes, self.instance_index)
        self.__getitem_cache = {self.root.name: self.root}
        self.instance_index[Organization] += 1

    @classmethod
    def model_tree_verify_tree(cls, root):
        obj = root.verify_node()

        for dfield in root.direct_fields:
            field_type, field_value = root.direct_fields[dfield]
            if field_value:
                related_name = root.model._meta.get_field(
                    dfield).related.get_accessor_name()
                if field_type == ForeignKey:
                    rel_obj = cls.model_tree_verify_tree(field_value)
                    # verify that fk relation works properly
                    if not rel_obj == getattr(obj, dfield):
                        raise AssertionError(
                            "FK relation does not work properly"
                            " for {field} dfield of {model}".format(
                                field = dfield, model = root.model))
                    if obj not in getattr(rel_obj, related_name).all():
                        raise AssertionError(
                            "Backwards FK relation does not work properly"
                            " for {field} dfield of {model}".format(
                                field = dfield, model = root.model))
                elif field_type == ManyToManyField:
                    rel_objs = []
                    for node in field_value:
                        rel_obj = cls.model_tree_verify_tree(node)
                        rel_objs.append(rel_obj)
                        # verify that m2m relation work backwards properly
                        if related_name:
                            # if this is a symmetrical m2m relation on self,
                            # then there is no reverse accessor
                            if obj not in getattr(rel_obj, related_name).all():
                                raise AssertionError(
                                    "Backwards M2M relation does not work properly"
                                    " for {field} dfield of {model}".format(
                                        field = dfield, model = root.model))
                    # verify that m2m relation work forwards properly
                    if set(getattr(obj, dfield).all()).difference(rel_objs):
                        raise AssertionError(
                            "M2M relation does not work properly"
                            " for {field} dfield of {model}".format(
                                field = dfield, model = root.model))
                elif field_type == OneToOneField:
                    rel_obj = cls.model_tree_verify_tree(field_value)
                    # verify that o2o relation works properly
                    if not rel_obj == getattr(obj, dfield):
                        raise AssertionError(
                            "O2O relation does not work properly"
                            " for {field} dfield of {model}".format(
                                field = dfield, model = root.model))
                    if not obj == getattr(rel_obj, related_name):
                        raise AssertionError(
                            "Backwards O2O relation does not work properly"
                            " for {field} dfield of {model}".format(
                                field = dfield, model = root.model))
            else:
                if not field_type == ManyToManyField:
                    if not field_value == getattr(obj, dfield):
                        raise AssertionError(
                            "{node} node's object has {obj_value} value"
                            " of {field} dfield. Expected {node_value}.".format(
                                node = root.name,
                                obj_value = getattr(obj, dfield),
                                field = dfield,
                                node_value = field_value))
                else:
                    if not field_value == list(getattr(obj, dfield).all()):
                        raise AssertionError(
                            "{node} node's object has {obj_value} value"
                            " of {field} dfield. Expected {node_value}.".format(
                                node = root.name,
                                obj_value = list(getattr(obj, dfield).all()),
                                field = dfield,
                                node_value = field_value))

        for rfield in root.related_fields:
            field_type, field_value = root.related_fields[rfield]
            if field_value:
                related_name = DjangoFieldInfo(
                    *root.model._meta.get_field_by_name(rfield)).field.field.name
                if field_type == ForeignKey:
                    for node in field_value:
                        rel_obj = cls.model_tree_verify_tree(node)
                        if rel_obj not in getattr(obj, rfield).all():
                            raise AssertionError(
                                "Backwards FK relation does not work properly"
                                " for {field} rfield of {model}".format(
                                    field = rfield, model = root.model))
                        if not obj == getattr(rel_obj, related_name):
                            raise AssertionError(
                                "FK relation does not work properly"
                                " for {field} rfield of {model}".format(
                                    field = rfield, model = root.model))
                if field_type == ManyToManyField:
                    for node in field_value:
                        rel_obj = cls.model_tree_verify_tree(node)
                        if rel_obj not in getattr(obj, rfield).all():
                            raise AssertionError(
                                "Backwards M2M relation does not work properly"
                                " for {field} rfield of {model}".format(
                                    field = rfield, model = root.model))
                        if obj not in getattr(rel_obj, related_name).all():
                            raise AssertionError(
                                "M2M relation does not work properly"
                                " for {field} rfield of {model}".format(
                                    field = rfield, model = root.model))
                if field_type == OneToOneField:
                    rel_obj = cls.model_tree_verify_tree(field_value)
                    if not rel_obj == getattr(obj, rfield):
                        raise AssertionError(
                            "Backwards O2O relation does not work properly"
                            " for {field} rfield of {model}".format(
                                field = rfield, model = root.model))
                    if not obj == getattr(rel_obj, related_name):
                        raise AssertionError(
                            "O2O relation does not work properly"
                            " for {field} rfield of {model}".format(
                                field = rfield, model = root.model))
            else:
                raise RuntimeError(
                    "{node} node's object has empty value"
                    " of {field} related rfield.".format(
                        node = root.name,
                        field = rfield,
                        node_value = field_value))

        return obj

    def model_tree_verify_index(self):
        for model in self.instance_index:
            if not model.objects.count() == self.instance_index[model]:
                raise AssertionError(
                    "Number of instances of model {model}"
                    " is {instance_num}. Expected {index_num}.".format(
                        model = model, instance_num = model.objects.count(),
                        index_num = self.instance_index[model]))

    def verify_tree(self):
        self.model_tree_verify_tree(self.root)
        self.model_tree_verify_index()

    @classmethod
    def __cls_getitem(cls, root, key):
        if root.name == key:
            return root

        for dfield in root.direct_fields:
            field_type, field_value = root.direct_fields[dfield]
            if field_value:
                if not field_type == ManyToManyField:
                    item = cls.__cls_getitem(field_value, key)
                    if item is not None:
                        return item
                else:
                    for node in field_value:
                        item = cls.__cls_getitem(node, key)
                        if item is not None:
                            return item

        for rfield in root.related_fields:
            field_type, field_value = root.related_fields[rfield]
            if field_value:
                if field_type == OneToOneField:
                    item = cls.__cls_getitem(field_value, key)
                    if item is not None:
                        return item
                else:
                    for node in field_value:
                        item = cls.__cls_getitem(node, key)
                        if item is not None:
                            return item

    def __getitem__(self, key):
        if key in self.__getitem_cache:
            return self.__getitem_cache[key]
        else:
            item = self.__cls_getitem(self.root, key)

            if item is not None:
                self.__getitem_cache[key] = item
                return item
            else:
                raise KeyError("Tree has no {key} node.".format(key = key))
