# -*- coding: utf-8; -*-
"""This module provides various classes to ease wizard processing using selenium."""

from django.conf import settings
from django.utils.importlib import import_module
from django.contrib.sessions.models import Session
from django.contrib.formtools.tests.wizard.storage import get_request
from django.contrib.formtools.wizard.storage import get_storage
from django.contrib.formtools.wizard.views import StepsHelper
from django.core.files.storage import FileSystemStorage

from workers.models import Worker, WorkerAlias

from seleniumforms import SeleniumFormAutomaton
from seleniumfactories import WorkerFactory, WorkerAliasFactory
from seleniummodelverify import SeleniumModelNode as Node

import os
import tempfile
from functools import wraps


temp_storage_location = tempfile.mkdtemp(
    dir = os.environ.get('DJANGO_TEST_TEMP_DIR'))
temp_storage = FileSystemStorage(
    location = temp_storage_location)


class SeleniumWizardFormAutomaton(SeleniumFormAutomaton):

    """Django wizards usually change field ids in forms, which they consist of,
    depending on the step name. This class provides useful method that takes
    this changes into account for you. You only need to specify field names
    (without prefixes, i.e. as they are defined in form) and values for them.

    SeleniumWizardFormAutomaton class does not rely on seleniumtestsuite
    for form processing as those classes are not suited to be used with
    custom prefixes. Also seleniumtestsuite is more HTML-centric,
    while here we deal with Selenium's webobjects.

    Nevertheless, form processing tries to be as efficient as possible.
    """
    @staticmethod
    def init_wizard(klass, session_num = '1', prefix = None):
        wizard = klass(**klass.get_initkwargs(klass.form_list))

        if prefix is None:
            wizard.prefix = wizard.get_prefix(session_object_num = session_num)
        else:
            wizard.prefix = prefix

        request = get_request()

        wizard.file_storage = temp_storage

        # create storage from request
        # this call creates object of type storage_name (SessionStorage)
        # and copies prefix, request and file_storage attributes in it
        # all wizard data handling is done inside request.session[prefix] dict
        wizard.storage = get_storage(wizard.storage_name, wizard.prefix,
                                     request, wizard.file_storage)
        wizard.steps = StepsHelper(wizard)

        return wizard

    @staticmethod
    def connect_to_session():
        # acquire the current running session
        # Session is enclosed into SessionStore
        session, = Session.objects.all()
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore(session.session_key)

        return store

    @staticmethod
    def load_session_data(wizard, store):
        wizard.storage.data = store.load()[wizard.storage.prefix]

    def process_wizard_form(self, wizard, step, fields = None, initial_data = None):
        return self.process_form(wizard.get_form(step), fields, initial_data)

    @staticmethod
    def prepare_existing_worker(factory = WorkerFactory):
        return factory.create_entry()

    @staticmethod
    def prepare_existing_worker_through_alias(factory = WorkerFactory):
        worker = factory.create_entry()
        alias = WorkerAliasFactory.create_entry(worker = worker)

        return worker, alias

    @staticmethod
    def prepare_existing_worker_new_alias(name = "worker", factory = WorkerFactory):
        worker = factory.create_entry()
        node = Node(name, Worker, worker)
        node["aliases"] = [
            Node("alias", WorkerAlias,
                 {
                     "lastname": worker.lastname,
                     "middlename": worker.middlename[0],
                     "firstname": worker.firstname[0]
                 })
        ]

        return worker, node

    @staticmethod
    def prepare_new_worker(name = "worker", kwargs = None):
        fields = {
            "firstname": "Firstname",
            "middlename": "Middlename",
            "lastname": "Lastname"
        }
        if kwargs is not None:
            fields.update(kwargs)

        node = Node(name, Worker, fields)

        setattr(node, "fullname", " ".join(
            (node[key] for key in ("lastname", "firstname", "middlename"))))

        return node


def connecttosession(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        func(self, *args, **kwargs)

        self.store = self.connect_to_session()

    return wrapper


def loadsessiondata(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        self.load_session_data(self.wizard, self.store)

        func(self, *args, **kwargs)

    return wrapper
