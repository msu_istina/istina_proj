# -*- coding: utf-8; -*-
from seleniumtestsuite import SeleniumTestCase
from seleniumfactories import ProfileWithWorkerFactory
from basic.test_login import LoginToolbox


class SetupProfile(SeleniumTestCase):

    def setUp(self):
        super(SetupProfile, self).setUp()

        username = "username"
        password = "password"

        firstname = u"Тимофей"
        middlename = u"Викторович"
        lastname = u"Марков"

        self.profile = ProfileWithWorkerFactory.create_entry(
            firstname = firstname,
            middlename = middlename,
            lastname = lastname,
            user__username = username)

        self.worker = self.profile.worker
        self.user = self.profile.user
        # we have to store password in plaintext
        # as we want to be able to login
        self.password = password

        self.user.set_password(self.password)
        self.user.save()


class RunAsUser(SetupProfile, LoginToolbox):

    def setUp(self):
        super(RunAsUser, self).setUp()
        self.login_user_login(self.user.username, self.password)

    def run_as_user_goto_profile_page(self):
        self.driver.get(
            self.base_url + "/profile/{0}".format(self.user.username))
