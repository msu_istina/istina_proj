from django.conf import settings

# browser used for tests
SELENIUM_DRIVER = getattr(settings, 'SELENIUM_DRIVER', 'Firefox')

# WebDriver polls the DOM for a certain amount of time
# when trying to find an element or elements
# if they are not immediately available.
# This parameter specifies the said amount.
SELENIUM_TIMEOUT = int(getattr(settings, 'SELENIUM_TIMEOUT', '10'))
