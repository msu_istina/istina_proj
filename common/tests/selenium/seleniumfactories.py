# -*- coding: utf-8; -*-
"""This module provides various factories used by selenium tests.

See also by common.utils.factories module.
"""

import factory
import factory.fuzzy
from factory.django import DjangoModelFactory
from workers.models import Worker, WorkerAlias, Profile
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from userprofile.signals import create_profile
from publications.models import Article, Book
from journals.models import Journal
from publishers.models import Publisher
from icollections.models import Collection, Series
from conferences.models import Conference
from organizations.models import Organization, Department
from dissertation_councils.models import DissertationCouncil
from dissertations.models import Domain, Specialty
from dissertation_councils.models import DissertationCouncilPost
from dissertations.models import DissertationReviewshipCategory
from common.models import Country
from intrelations.models import HonoraryMembershipCategory
from awards.models import AwardCategory, AwardName
from courses.models import DisciplineType, TeachingKind, Course
import datetime


class SeleniumTestFactory(DjangoModelFactory):

    """Simple wrapper around DjangoModelFactory class
    that provides useful create_entry classmethod.
    """
    @classmethod
    def create_entry(cls, num = 0, **kwargs):
        """Creates entry of a given type in database."""
        if (not num) and (not kwargs):
            return cls.create()
        elif (not num) and kwargs:
            return cls.create(**kwargs)
        elif (num > 0) and (not kwargs):
            return cls.create_batch(num)
        elif (num > 0) and kwargs:
            return cls.create_batch(num, **kwargs)
        else:
            raise ValueError(
                "Unsupported combination of arguments:\n"
                "num must be greater or equal to 0 "
                "and kwargs must be valid named parameters "
                "for {0} class.".format(cls.__name__))


class WorkerFactory(SeleniumTestFactory):
    FACTORY_FOR = Worker

    firstname = factory.Sequence(lambda n: u"Имя_{0}".format(n))
    middlename = factory.Sequence(lambda n: u"Отчество_{0}".format(n))
    lastname = factory.Sequence(lambda n: u"Фамилия_{0}".format(n))


class DistinctWorkerFactory(SeleniumTestFactory):
    FACTORY_FOR = Worker

    firstname = factory.fuzzy.FuzzyText(length = 6)
    middlename = factory.fuzzy.FuzzyText(length = 10)
    lastname = factory.fuzzy.FuzzyText(length = 8)


class WorkerAliasFactory(SeleniumTestFactory):
    FACTORY_FOR = WorkerAlias

    worker = factory.SubFactory(WorkerFactory)
    firstname = factory.LazyAttribute(lambda o: u"Aliased_" + o.worker.firstname)
    middlename = factory.LazyAttribute(lambda o: u"Aliased_" + o.worker.middlename)
    lastname = factory.LazyAttribute(lambda o: u"Aliased_" + o.worker.lastname)


class PureUserFactory(SeleniumTestFactory):

    """This factory generates pure User objects, i.e. without any Profile.

    In our model User always must correspond to a single existing Profile
    and vice versa. This is why you should not directly use this factory
    unless you want pure User in database for some reason.
    """
    FACTORY_FOR = User

    username = factory.Sequence(lambda n: u"username_{0}".format(n))
    email = factory.LazyAttribute(
        lambda obj: u"{username}@example.com".format(username = obj.username))

    @classmethod
    def _generate(cls, create, attrs):
        """Override the default _generate() to disable the post-save signal.
        Taken from factory_boy documentation (see common recipes from project docs).
        """

        post_save.disconnect(
            sender=User,
            dispatch_uid="django-profile.userprofile.signals.create_profile")

        user = super(PureUserFactory, cls)._generate(create, attrs)

        post_save.connect(
            create_profile,
            sender=User,
            dispatch_uid="django-profile.userprofile.signals.create_profile")

        return user


class ProfileFactory(SeleniumTestFactory):
    FACTORY_FOR = Profile

    firstname = factory.Sequence(lambda n: u"Имя_{0}".format(n))
    middlename = factory.Sequence(lambda n: u"Отчество_{0}".format(n))
    lastname = factory.Sequence(lambda n: u"Фамилия_{0}".format(n))
    user = factory.SubFactory(PureUserFactory)


class ProfileWithWorkerFactory(SeleniumTestFactory):
    FACTORY_FOR = Profile

    firstname = factory.Sequence(lambda n: u"Имя_{0}".format(n))
    middlename = factory.Sequence(lambda n: u"Отчество_{0}".format(n))
    lastname = factory.Sequence(lambda n: u"Фамилия_{0}".format(n))
    user = factory.SubFactory(PureUserFactory)

    @classmethod
    def _generate(cls, create, attrs):
        profile = super(ProfileWithWorkerFactory, cls)._generate(create, attrs)

        profile.worker = WorkerFactory(
            firstname = profile.firstname,
            middlename = profile.middlename,
            lastname = profile.lastname)

        return profile


class ArticleFactory(SeleniumTestFactory):
    FACTORY_FOR = Article

    title = factory.Sequence(lambda n: u"Article {0}".format(n))
    year = datetime.date.today().year


class BookFactory(SeleniumTestFactory):
    FACTORY_FOR = Book

    title = factory.Sequence(lambda n: u"Book {0}".format(n))
    location = factory.Sequence(lambda n: u"City {0}".format(n))
    year = datetime.date.today().year


class JournalFactory(SeleniumTestFactory):
    FACTORY_FOR = Journal

    name = factory.Sequence(lambda n: u"Journal {0}".format(n))


class PublisherFactory(SeleniumTestFactory):
    FACTORY_FOR = Publisher

    name = factory.Sequence(lambda n: u"Publisher {0}".format(n))
    city = factory.Sequence(lambda n: u"City {0}".format(n))


class CollectionFactory(SeleniumTestFactory):
    FACTORY_FOR = Collection

    title = factory.Sequence(lambda n: u"Collection {0}".format(n))
    year = datetime.date.today().year


class SeriesFactory(SeleniumTestFactory):
    FACTORY_FOR = Series

    title = factory.Sequence(lambda n: u"Series {0}".format(n))


class ThesesCollectionFactory(SeleniumTestFactory):
    FACTORY_FOR = Collection

    title = factory.Sequence(lambda n: u"Theses collection {0}".format(n))
    year = datetime.date.today().year
    theses = True


class ThesisFactory(SeleniumTestFactory):
    FACTORY_FOR = Article

    title = factory.Sequence(lambda n: u"Thesis {0}".format(n))
    year = datetime.date.today().year
    collection = factory.SubFactory(ThesesCollectionFactory)


class ConferenceFactory(SeleniumTestFactory):
    FACTORY_FOR = Conference

    name = factory.Sequence(lambda n: u"Conference {0}".format(n))
    year = datetime.date.today().year


class OrganizationFactory(SeleniumTestFactory):
    FACTORY_FOR = Organization

    name = factory.Sequence(lambda n: u"Organization {0}".format(n))


class DepartmentFactory(SeleniumTestFactory):
    FACTORY_FOR = Department

    name = factory.Sequence(lambda n: u"Department {0}".format(n))
    organization = factory.SubFactory(OrganizationFactory)


class DissertationCouncilFactory(SeleniumTestFactory):
    FACTORY_FOR = DissertationCouncil

    number = factory.Sequence(lambda n: n)
    organization = factory.SubFactory(OrganizationFactory)
    organization_str = u""


class DomainFactory(SeleniumTestFactory):
    FACTORY_FOR = Domain

    name = factory.Sequence(lambda n: u"Science domain {0}".format(n))


class SpecialtyFactory(SeleniumTestFactory):
    FACTORY_FOR = Specialty

    number = factory.Sequence(lambda n: n)
    name = factory.Sequence(lambda n: u"Dissertation specialty {0}".format(n))


class DissertationCouncilPostFactory(SeleniumTestFactory):
    FACTORY_FOR = DissertationCouncilPost

    name = factory.Sequence(lambda n: u"Position {0}".format(n))


class DissertationReviewshipCategoryFactory(SeleniumTestFactory):
    FACTORY_FOR = DissertationReviewshipCategory

    name = factory.Sequence(lambda n: u"Reviewship category {0}".format(n))


class CountryFactory(SeleniumTestFactory):
    FACTORY_FOR = Country

    name = factory.Sequence(lambda n: u"COUNTRY{0}".format(n))


class HonoraryMembershipCategoryFactory(SeleniumTestFactory):
    FACTORY_FOR = HonoraryMembershipCategory

    name = factory.Sequence(lambda n: u"Honor type {0}".format(n))


class AwardCategoryFactory(SeleniumTestFactory):
    FACTORY_FOR = AwardCategory

    name = factory.Sequence(lambda n: u"Award type {0}".format(n))


class AwardNameFactory(SeleniumTestFactory):
    FACTORY_FOR = AwardName

    name = factory.Sequence(lambda n: u"Award name {0}".format(n))
    category = factory.SubFactory(AwardCategoryFactory)


class DisciplineTypeFactory(SeleniumTestFactory):
    FACTORY_FOR = DisciplineType

    name = factory.Sequence(lambda n: u"Discipline type {0}".format(n))


class TeachingKindFactory(SeleniumTestFactory):
    FACTORY_FOR = TeachingKind

    name = factory.Sequence(lambda n: u"Teaching type {0}".format(n))


class CourseFactory(SeleniumTestFactory):
    FACTORY_FOR = Course

    title = factory.Sequence(lambda n: u"Course {0}".format(n))
    year = datetime.date.today().year
