# -*- coding: utf-8; -*-

from conferences.views import ConferencePresentationWizardView as Wizard

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep

import datetime


@SeleniumTestStepByStep
class Issue1169(RunAsUser, SeleniumToolbox, SeleniumWizardFormAutomaton):

    def setUp(self):
        super(Issue1169, self).setUp()

        self.add_get_window_method(self.driver)

        self.presentation = {
            "title": "A title",
            "abstract": "An abstract",
        }

        self.conference = {
            "name": "A conference",
            "year": datetime.date.today().year,
        }

        self.another_presentation = {
            "title": "Another title",
            "abstract": "Another abstract",
        }

        self.another_conference = {
            "name": "Another conference",
            "year": datetime.date.today().year + 100,
        }

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/conferences/presentations/add/")

    def stepbystep_next(self):
        pass

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.presentation["title"],
                "conference_str": self.conference["name"],
                "year": self.conference["year"],
                "abstract": self.presentation["abstract"],
                "authors_str": self.worker.fullname
            })

        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

    def stepbystep_pre_step2(self):
        self.another_worker = self.prepare_existing_worker()

        self.parent, self.child = self.driver.get_window(
            self.base_url + "/conferences/presentations/add/")

        self.driver.switch_to_window(self.child)

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.another_presentation["title"],
                "conference_str": self.another_conference["name"],
                "year": self.another_conference["year"],
                "abstract": self.another_presentation["abstract"],
                "authors_str": self.another_worker.fullname
            })

        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

    def stepbystep_pre_step3(self):
        self.driver.switch_to_window(self.parent)

    def stepbystep_step3(self):
        self.driver.find_element_by_css_selector(
            u'input[type="button"][title="Вернуться на предыдущий шаг и исправить данные"]').click()

        self.process_wizard_form(
            self.wizard, "main", initial_data = {
                "title": self.presentation["title"],
                "conference_str": self.conference["name"],
                "year": str(self.conference["year"]),
                "abstract": self.presentation["abstract"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_finalize(self):
        pass
