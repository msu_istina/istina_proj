# -*- coding: utf-8; -*-
"""This module does testing of register page.

    - very long usernames are truncated
    - invalid usernames are rejected with proper warning
    - invalid emails are rejected with proper warning
    - non-matching passwords are rejected with proper warning
    - valid username/password/email combinations are registered properly
    - existing usernames are rejected with proper warning
"""

from django.contrib.auth.models import User
from workers.models import Profile

from userprofile.forms import RegistrationForm

from common.tests.selenium.seleniumtestsuite import SeleniumTestCase
from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumtestsuite import TestEmailFormByCSS
from common.tests.selenium.seleniumforms import SeleniumFormAutomaton
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import ProfileFactory


class RegisterToolbox(SeleniumFormAutomaton, SeleniumTestCase):

    def register_user_register(self, username, email, password):
        """Submit register form with given username, email and password."""

        self.driver.get(self.base_url + "/accounts/register/")

        form = self.process_form(
            RegistrationForm(),
            {
                "username": username,
                "email": email,
                "password1": password,
                "password2": password
            })
        form.submit()


class Register(TestEmailFormByCSS, SeleniumToolbox, RegisterToolbox):

    def setUp(self):
        super(Register, self).setUp()

        self.driver.get(self.base_url + "/accounts/register/")

        self.stb_test_istina_page_title(u"Регистрация пользователя")

        self.form = RegistrationForm()

    def test_register_submit_user_long(self):
        """Test username field with a long username.

        The length limit is 30 chars. If input has greater length,
        then it should be truncated to 30 chars.
        """

        for username in (u"username" * 10, u"юзернейм" * 10):
            self.multiple_input_form_fieldset_test_maxlength_field(
                "form", max_length = 30, id_username = username)
            self.multiple_input_form_fieldset_clear("form", id_username = "")

    def test_register_submit_user_invalid(self):
        """Test username field with invalid input.

        Only english letters, digits and chars @.+_, are allowed.
        Everything else should give a warning.
        No need to test URI encoded strings, since we don't accept '%' char.
        """

        def _test_submit_user_invalid(username):
            form = self.process_form(
                self.form,
                {
                    "username": username,
                    "email": "username@example.com",
                    "password1": "password",
                    "password2": "password"
                })
            form.submit()

            self.css_test_multiple_elements_children_text(
                "form > fieldset", "p", "span",
                [
                    [u"Разрешены латинские буквы, цифры и знаки @.+-_, "
                     u"всего не более 30 символов.",
                     u"* Значение должно состоять только из букв, цифр и "
                     u"знаков @/./+/-/_."],
                    [],
                    [],
                    [u"Введите еще раз тот же пароль, чтобы избежать ошибки."]
                ]
            )

        for username in (u"юзернейм", u"юзерname", u"userнейм",
                         u"юзер123", u"123юзер"):
            _test_submit_user_invalid(username)

        for char in ur"~!#$%^&*()=[]{}\|/,?№;:":
            _test_submit_user_invalid(char.join((u"user", u"name")))
            _test_submit_user_invalid(char.join((u"юзер", u"нейм")))

    def test_register_submit_email_invalid(self):
        """Test email field with invalid input."""

        self.email_form_test_invalid(
            "form",
            self._construct_callback(
                self.css_test_multiple_elements_children_text,
                "form > fieldset", "p", "span",
                [
                    [u"Разрешены латинские буквы, цифры и знаки @.+-_, "
                     u"всего не более 30 символов."],
                    [u"* Введите правильный адрес электронной почты."],
                    [],
                    [u"Введите еще раз тот же пароль, чтобы избежать ошибки."]
                ]
            ),
            "id_email",
            id_username = "username",
            id_password1 = "password",
            id_password2 = "password"
        )

    def test_register_submit_password1_not_equal_password2(self):
        """Test password{1,2} fields with non-matching passwords.

        Situations tested here are accindentally CapsLock'ed passwords
        and a mix of english and russian letters that look the same,
        for example "c" and "o".
        """

        def _test_submit_password1_not_equal_password2(password1, password2):
            form = self.process_form(
                self.form,
                {
                    "username": "username",
                    "email": "username@example.com",
                    "password1": password1,
                    "password2": password2
                })
            form.submit()

            self.css_test_multiple_elements_children_text(
                "form > fieldset", "p", "span",
                [
                    [u"Разрешены латинские буквы, цифры и знаки @.+-_, "
                     u"всего не более 30 символов."],
                    [],
                    [],
                    [u"Введите еще раз тот же пароль, чтобы избежать ошибки.",
                     u"* Два поля с паролями не совпадают."]
                ]
            )

        for passwords in [
                (u"password", u"PASSWORD"),
                (u"пароль", u"ПАРОЛЬ"),
                # russian letters "а" and "о" in the second password below
                (u"password", u"pаsswоrd"),
                # english letters "a", "o" and "p" in the second password below
                (u"пароль", u"пapoль"),
        ]:
            _test_submit_password1_not_equal_password2(*passwords)

    def test_register_user_register(self):
        """Test that a valid username/email/password combination is registered
        properly and a newly created user is inactive.

        After each combination is processed the corresponding User object is deleted.
        This has to be done as we want to _reliably_ check that only one User
        is created per registration procedure and this User has proper attributes.
        In the case of multiple users in database we have to use db query,
        not the direct reference, which is not reliable enough.
        """

        for i in xrange(len(self.valid_emails)):
            # check that there are no users in database
            self.assertFalse(User.objects.exists())

            user = Node(
                "user", User,
                {
                    "username": u"UserName@.+-_9876543210{0}".format(i),
                    "email": self.valid_emails[i],
                    "is_active": False,
                    "is_staff": False,
                    "is_superuser": False
                })

            profile = Node(
                "profile", Profile,
                {
                    "user": user,
                    "worker": None
                })

            tree = Tree(profile)

            self.register_user_register(
                user["username"], user["email"],
                ur"PassWordПароль!#$%^&*()=[]{{}}\|/,?№;:@.+-_9876543210"
            )

            self.stb_test_istina_page_title(u"Регистрация завершена")

            self.css_test_single_element_text(
                "div.success",
                u"Вы успешно зарегистрировали новый аккаунт. "
                u"Ссылка для активации аккаунта направлена на указанный адрес e-mail. "
                u"Нажмите на ссылку в письме, чтобы закончить регистрацию. "
                u"Внимание! Если письмо долго не приходит, проверьте, "
                u"не попало ли оно в спам."
            )

            tree.verify_tree()

            # get rid of user
            User.objects.get().delete()

    def test_register_submit_user_existing(self):
        """Test that registration attempt with existing username is rejected."""

        username = u"UserName@.+-_9876543210"
        email = "mailbox@example.com"

        ProfileFactory.create_entry(
            user__username = username,
            user__email = email
        )

        self.register_user_register(
            username, "another.mailbox@example.com", "password")

        self.css_test_multiple_elements_children_text(
            "form > fieldset", "p", "span",
            [
                [u"Разрешены латинские буквы, цифры и знаки @.+-_, "
                 u"всего не более 30 символов.",
                 u"* Пользователь с таким именем уже существует."],
                [],
                [],
                [u"Введите еще раз тот же пароль, чтобы избежать ошибки."]
            ]
        )
