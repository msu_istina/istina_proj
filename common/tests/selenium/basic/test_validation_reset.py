# -*- coding: utf-8; -*-
"""This module does testing of validation reset page.

    - invalid emails are rejected with proper warning
    - non-existing emails are rejected with proper warning
    - validation reset email for inactive user is sent with proper contents
    - validation reset email for active user is not sent with proper warning
    - validation reset link is not valid once activation procedure is complete
"""

from django.contrib.auth.models import User

from userprofile.forms import ResendEmailValidationForm

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumtestsuite import TestEmailFormByCSS

from test_register import RegisterToolbox
from test_activation_email import ActivationEmailToolbox


class ValidationReset(TestEmailFormByCSS, SeleniumToolbox,
                      RegisterToolbox, ActivationEmailToolbox):

    def setUp(self):
        super(ValidationReset, self).setUp()

        self.driver.get(self.base_url + "/accounts/email/validation/reset/")

        self.stb_test_istina_page_title(u"Послать ещё раз e-mail подтверждения")

        self.form = ResendEmailValidationForm()

    def test_validation_reset_submit_email_invalid(self):
        """Test email field with invalid input."""

        self.email_form_test_invalid(
            "form",
            self._construct_callback(
                self.css_test_single_element_text,
                "input#{id} + span".format(id = "id_email"),
                u"* Введите правильный адрес электронной почты."
            ),
            "id_email"
        )

    def test_validation_reset_submit_email_non_existing(self):
        """Test that non-existing emails are rejected with proper warning."""

        self.email_form_test_valid(
            "form",
            self._construct_callback(
                self.css_test_single_element_text,
                "input#{id} + span".format(id = "id_email"),
                u"* Такой e-mail не зарегистрирован."
            ),
            "id_email"
        )

    def test_validation_reset_for_inactive_user(self):
        """Test that validation reset email for inactive user is sent properly
        and with proper contents. Also, test that user is activated when
        activation procedure is completed through validation reset link.
        """

        # manually reset message outbox
        if self.mail.outbox: self.mail.outbox = []

        username = "username"
        email = "{0}@example.com".format(username)
        password = "password"

        self.register_user_register(username, email, password)

        self.driver.get(self.base_url + "/accounts/email/validation/reset/")

        form = self.process_form(
            self.form,
            {
                "email": email
            })
        form.submit()

        self.stb_test_istina_page_title(
            u"Повторная посылка письма для подтверждения электронного адреса")

        self.css_test_single_element_text(
            "div#content-main",
            u"Мы отправили вам письмо со ссылкой для подтверждения."
        )

        self.assertEqual(len(self.mail.outbox), 2)
        register_email = self.mail.outbox[0]
        reset_email = self.mail.outbox[1]

        self.activation_email_test_message(reset_email, username, email)

        self.assertEqual(
            self.activation_email_get_activation_link(register_email),
            self.activation_email_get_activation_link(reset_email)
        )

        self.activation_email_activate(reset_email)

        self.assertTrue(User.objects.get(username = username).is_active)

    def test_validation_reset_for_active_user(self):
        """Test that validation reset email for active user
        is not sent with proper warning.

        Note that superuser will get
        "Пользователь с этим адресом электронной почты уже подтвержден. "
        "Попробуйте войти в систему под своим именем пользователя и паролем."
        on this page: /accounts/email/validation/reset/user_verified/
        Regular user will get
        "Ваш e-mail уже подтвержден."
        on this page: /accounts/email/validation/reset/email_verified/
        We test the regular user case.
        """

        # manually reset message outbox
        if self.mail.outbox: self.mail.outbox = []

        username = "username"
        email = "{0}@example.com".format(username)
        password = "password"

        self.register_user_register(username, email, password)

        self.activation_email_activate(self.mail.outbox[0])

        self.driver.get(self.base_url + "/accounts/email/validation/reset/")

        form = self.process_form(
            self.form,
            {
                "email": email
            })
        form.submit()

        self.assertEqual(len(self.mail.outbox), 1)

        self.stb_test_istina_page_title(
            u"Повторная посылка письма для подтверждения электронного адреса")

        self.css_test_single_element_text(
            "div#content-main",
            u"Ваш e-mail уже подтвержден."
        )

    def test_validation_reset_link_expires_after_activation(self):
        """Test that validation reset link expires
        once activation procedure is completed through validation reset link.
        """

        # manually reset message outbox
        if self.mail.outbox: self.mail.outbox = []

        username = "username"
        email = "{0}@example.com".format(username)
        password = "password"

        self.register_user_register(username, email, password)

        self.driver.get(self.base_url + "/accounts/email/validation/reset/")

        form = self.process_form(
            self.form,
            {
                "email": email
            })
        form.submit()

        # activate created user via validation reset link
        self.activation_email_activate(self.mail.outbox[1])

        # second activation should fail
        self.activation_email_activate(self.mail.outbox[1])

        self.stb_test_istina_page_title(u"Смена e-mail не удалась")

        self.css_test_single_element_text(
            "div.error",
            u"Ключ проверки, который вы получили, уже не действителен. "
            u"Пожалуйста, запросите подтверждение почты ещё раз."
        )
