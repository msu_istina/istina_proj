# -*- coding: utf-8; -*-
"""This module does testing of search page.

    - empty worker query returns nothing, but help message
    - empty publication query returns nothing, but help message
    - invalid worker query returns no hits
    - invalid publication query returns no hits
    - valid query with single worker returns proper results
    - valid query with single alias returns proper results
    - valid query with single article returns proper results
    - valid query with single thesis returns proper results
    - valid query with single book returns proper results
    - valid query with multiple workers returns proper results
    - valid query with multiple publications returns proper results
"""

from common.utils.cache import default_cache, bigdata_cache

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumtestsuite import TestSingleInputFormByCSS
from common.tests.selenium.seleniumfactories import (
    WorkerFactory, WorkerAliasFactory)
from common.tests.selenium.seleniumfactories import (
    ArticleFactory, ThesisFactory, BookFactory)

import datetime
import re


class Search(TestSingleInputFormByCSS, SeleniumToolbox):

    def setUp(self):
        super(Search, self).setUp()

        self.driver.get(self.base_url + "/search/")

        self.stb_test_istina_page_title(u"Поиск")

    def test_worker_search_submit_empty(self):
        """Test that empty worker search returns no hits, but help message."""

        self.single_input_form_submit_empty("form#id_worker_search")

        self.stb_test_istina_page_title(u"Результаты поиска")
        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(ur"^[\s\S]*Результаты поиска:[\s\S]*$",
                       flags = re.UNICODE)
        )
        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(
                ur"^[\s\S]*Для поиска Вы должны ввести непустую строку.[\s\S]*$",
                flags = re.UNICODE))

    def test_publication_search_submit_empty(self):
        """Test that empty publication search returns no hits, but help message."""

        self.single_input_form_submit_empty("form#id_publication_search")

        self.stb_test_istina_page_title(u"Результаты поиска")
        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(ur"^[\s\S]*Результаты поиска:[\s\S]*$",
                       flags = re.UNICODE)
        )
        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(
                ur"^[\s\S]*Ваш запрос слишком короткий, "
                ur"он должен содержать хотя бы 3 символа.[\s\S]*$",
                flags = re.UNICODE))

    def test_worker_search_submit_invalid(self):
        """Test that invalid worker search query returns no hits.

        Query used: "Lorem ipsum dolor sit amet, consectetur adipisicing elit"
        """

        self.single_input_form_submit(
            "form#id_worker_search",
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit")
        self.single_input_form_test_value(
            "form#id_worker_search",
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit")

        self.stb_test_istina_page_title(u"Результаты поиска")
        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(ur"^[\s\S]*Результаты поиска:[\s\S]*$",
                       flags = re.UNICODE)
        )
        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(
                ur"^[\s\S]*По вашему запросу ничего не найдено.[\s\S]*$",
                flags = re.UNICODE))

    def test_publication_search_submit_invalid(self):
        """Test that invalid publication search query returns no hits.

        Query used: "Lorem ipsum dolor sit amet, consectetur adipisicing elit"
        """

        self.single_input_form_submit(
            "form#id_publication_search",
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit")
        self.single_input_form_test_value(
            "form#id_publication_search",
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit")

        self.stb_test_istina_page_title(u"Результаты поиска")
        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(ur"^[\s\S]*Результаты поиска:[\s\S]*$",
                       flags = re.UNICODE)
        )
        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(
                ur"^[\s\S]*По вашему запросу ничего не найдено.[\s\S]*$",
                flags = re.UNICODE))

    def _test_worker_search_valid(self, worker):
        """Test that search for a given worker successfully returns some hits."""

        self.single_input_form_submit("form#id_worker_search", worker)
        self.single_input_form_test_value("form#id_worker_search", worker)

        self.stb_test_istina_page_title(u"Результаты поиска")
        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(ur"^[\s\S]*Результаты поиска:[\s\S]*$",
                       flags = re.UNICODE)
        )
        self.assertNotRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(
                ur"^[\s\S]*По вашему запросу ничего не найдено.[\s\S]*$",
                flags = re.UNICODE))

    def _test_publication_search_valid(self, title):
        """Test that search for a given publication successfully returns some hits."""

        self.single_input_form_submit("form#id_publication_search", title)
        self.single_input_form_test_value("form#id_publication_search", title)

        self.stb_test_istina_page_title(u"Результаты поиска")
        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(ur"^[\s\S]*Результаты поиска:[\s\S]*$",
                       flags = re.UNICODE)
        )
        self.assertNotRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(
                ur"^[\s\S]*По вашему запросу ничего не найдено.[\s\S]*$",
                flags = re.UNICODE))

    def test_worker_search_single_valid(self):
        """Test that a valid query with single worker returns proper results."""

        # drop caches
        default_cache.clear()
        bigdata_cache.clear()

        worker_node = {
            "firstname": u"Имя",
            "middlename": u"Отчество",
            "lastname": u"Фамилия"
        }

        self.worker = WorkerFactory.create_entry(**worker_node)

        self._test_worker_search_valid(self.worker.lastname)

        # we expect a single result, thus no pagination
        self.assertFalse(self.css_is_element_present("div.pagination"))

        found_workers = self.css_list_children(
            "h2 + div", 'div[class^="worker_short"]')

        self.assertTrue(len(found_workers), 1)

        self.assertEqual(found_workers[0].text, self.worker.fullname)

    def test_worker_search_single_alias(self):
        """Test that a valid query with single worker alias returns proper results."""

        # drop caches
        default_cache.clear()
        bigdata_cache.clear()

        worker_node = {
            "firstname": u"Имя",
            "middlename": u"Отчество",
            "lastname": u"Фамилия"
        }
        alias_node = {
            "firstname": "Firstname",
            "middlename": "Middlename",
            "lastname": "Lastname"
        }

        self.worker = WorkerFactory.create_entry(**worker_node)
        self.alias = WorkerAliasFactory.create_entry(
            worker = self.worker, **alias_node)

        self._test_worker_search_valid(self.alias.lastname)

        # we expect a single result, thus no pagination
        self.assertFalse(self.css_is_element_present("div.pagination"))

        found_workers = self.css_list_children(
            "h2 + div", 'div[class^="worker_short"]')

        self.assertTrue(len(found_workers), 1)

        self.assertEqual(found_workers[0].text, "\n".join(
            (self.worker.fullname, u"Сотрудник найден по альтернативному имени")))

    def _test_publication_search_single_valid(self, factory, title, **kwargs):

        # drop caches
        default_cache.clear()
        bigdata_cache.clear()

        factory.reset_sequence(force = True)

        kwargs["title"] = title
        kwargs["year"] = datetime.date.today().year
        factory.create_entry(**kwargs)

        self._test_publication_search_valid(title)

        # we expect a single result, thus no pagination
        self.assertFalse(self.css_is_element_present("div.pagination"))

        found_pubs = self.css_list_children(
            "div.search_labels + ul", "li > ul.activity")

        self.assertEqual(len(found_pubs), 1)

        self.assertTrue(
            found_pubs[0].text.startswith(
                " ".join((unicode(kwargs["year"]), kwargs["title"]))
            )
        )

    def test_publication_search_single_article(self):
        """Test that a valid query with single article returns proper results."""

        self._test_publication_search_single_valid(
            ArticleFactory, "On example titles for articles.")

        # only the search label for articles should be present with proper text
        self.css_test_single_element_text("div.search_labels", u"статьи (1)")

    def test_publication_search_single_thesis(self):
        """Test that a valid query with single thesis returns proper results."""

        self._test_publication_search_single_valid(
            ThesisFactory, "On example titles for theses.")

        # only the search label for theses should be present with proper text
        self.css_test_single_element_text("div.search_labels", u"тезисы (1)")

    def test_publication_search_single_book(self):
        """Test that a valid query with single book returns proper results."""

        self._test_publication_search_single_valid(
            BookFactory, "A good book title: myth or reality?")

        # only the search label for books should be present with proper text
        self.css_test_single_element_text("div.search_labels", u"книги (1)")

    def _test_search_results_pagination_present(
            self, pagination_id, current_page, total_pages):
        """Test that pagination block is present with correct number of page links."""

        self.assertGreaterEqual(total_pages, 2)
        self.assertGreaterEqual(current_page, 1)
        self.assertGreaterEqual(total_pages, current_page)

        # normally 10 page links are shown in pagination block:
        # 5 pages back and 5 pages forward
        forward_pages = back_pages = 5

        if (total_pages - current_page) < 5:
            # we are close to the tail of list of pages, it will be truncated
            forward_pages = total_pages - current_page

        if (current_page - 1) < 5:
            # we are close to the head of list of pages, it will be truncated
            back_pages = current_page - 1

        link_titles = []

        if current_page > 1:
            link_titles += [u"Перейти на предыдущую страницу"]

        if back_pages:
            link_titles += [u"Перейти на страницу {0}".format(i)
                            for i in xrange(current_page - back_pages,
                                            current_page)]

        if forward_pages:
            link_titles += [u"Перейти на страницу {0}".format(i)
                            for i in xrange(1 + current_page,
                                            1 + current_page + forward_pages)]

        if current_page < total_pages:
            link_titles += [u"Перейти на следующую страницу"]

        # we double lists below as we have two pagination blocks per page
        self.css_test_single_element_children_text(
            pagination_id, "span.current", [unicode(current_page)] * 2)

        self.css_test_single_element_children_attribute(
            pagination_id, "a", "title", link_titles * 2)

    def _search_pagination_click_next(self, pagination_id):
        self.driver.find_element_by_css_selector(
            '{pagination_id} > a[title="Перейти на следующую страницу"]'.format(
                pagination_id = pagination_id)).click()

    def test_worker_search_multiple_valid(self):
        """Test that a valid query with multiple workers returns proper results."""

        # drop caches
        default_cache.clear()
        bigdata_cache.clear()

        full_pages = 13
        results_per_page = 25
        workers_num = results_per_page * full_pages + 1
        lastname = u"Фамилия"

        WorkerFactory.reset_sequence(force = True)
        WorkerFactory.create_entry(workers_num, lastname = lastname)

        self._test_worker_search_valid(lastname)

        # check that first full_pages pages are full of workers
        for i in xrange(1, 1 + full_pages):
            # check that pagination is present
            self._test_search_results_pagination_present(
                "div.pagination", i, full_pages + 1)

            found_workers = self.css_list_children(
                "h2 + div.pagination + div", 'div[class^="worker_short"]')

            # check that only results_per_page workers are displayed
            self.assertTrue(len(found_workers), results_per_page)

            # check that results are relevant to query
            for worker in found_workers:
                self.assertTrue(worker.text.startswith(lastname))

            self._search_pagination_click_next("div.pagination")

        # we are on the last page now, which is not full

        # check that pagination is present
        self._test_search_results_pagination_present(
            "div.pagination", full_pages + 1, full_pages + 1)

        found_workers = self.css_list_children(
            "h2 + div.pagination + div", 'div[class^="worker_short"]')

        # check that only one worker is present on the last page
        self.assertTrue(len(found_workers), 1)

        self.assertTrue(found_workers[0].text.startswith(lastname))

    def _test_publication_search_search_labels_present(
            self, label_id, current_label_num, labels):

        self.css_test_single_element_text(
            label_id + " > span", labels[current_label_num])

        self.css_test_single_element_children_text(
            label_id, "a",
            labels[:current_label_num] + labels[current_label_num + 1:])

    def _search_labels_click_next(self, label_id, current_label_num, labels):
        label = self.driver.find_element_by_css_selector(label_id)
        label.find_element_by_link_text(labels[current_label_num + 1]).click()

    def test_publication_search_multiple_valid(self):
        """Test that a valid query with multiple publications returns proper results."""

        # drop caches
        default_cache.clear()
        bigdata_cache.clear()

        full_pages = 13
        results_per_page = 25
        pubs_num = results_per_page * full_pages + 1
        title = u"Example"
        year = datetime.date.today().year

        ArticleFactory.create_entry(
            pubs_num, title = title + " article", year = year)
        ThesisFactory.create_entry(
            pubs_num, title = title + " thesis", year = year)
        BookFactory.create_entry(
            pubs_num, title = title + " book", year = year)

        self._test_publication_search_valid(title)

        labels = [u"{label} ({num})".format(label = label, num = pubs_num)
                  for label in (u"статьи", u"тезисы", u"книги")]
        labels_num = len(labels)

        for label_num in xrange(labels_num):
            # check that first full_pages pages are full
            for i in xrange(1, 1 + full_pages):
                # check that pagination is present
                self._test_search_results_pagination_present(
                    "div.pagination", i, full_pages + 1)

                # check that search labels are present
                self._test_publication_search_search_labels_present(
                    "div.search_labels", label_num, labels)

                found_pubs = self.css_list_children(
                    "div.search_labels + div.pagination + ul",
                    "li > ul.activity")

                # check that only results_per_page pubs are displayed
                self.assertTrue(len(found_pubs), results_per_page)

                # check that results are relevant to query
                for pub in found_pubs:
                    self.assertTrue(
                        pub.text.startswith(
                            " ".join((unicode(year), title))
                        )
                    )

                self._search_pagination_click_next("div.pagination")

            # we are on the last page now, which is not full

            # check that pagination is present
            self._test_search_results_pagination_present(
                "div.pagination", full_pages + 1, full_pages + 1)

            # check that search labels are present
            self._test_publication_search_search_labels_present(
                "div.search_labels", label_num, labels)

            found_pubs = self.css_list_children(
                "div.search_labels + div.pagination + ul",
                "li > ul.activity")

            # check that only one pub is present on the last page
            self.assertTrue(len(found_pubs), 1)

            self.assertTrue(
                found_pubs[0].text.startswith(
                    " ".join((unicode(year), title))
                )
            )

            # proceed to the next label
            if label_num < labels_num - 1:
                self._search_labels_click_next(
                    "div.search_labels", label_num, labels)
