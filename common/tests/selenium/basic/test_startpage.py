# -*- coding: utf-8; -*-
"""This module does testing of startpage.

    - startpage is loaded and important links are present
"""

from selenium.webdriver.common.by import By
from common.tests.selenium.seleniumtestsuite import SeleniumToolbox

import re


class Startpage(SeleniumToolbox):

    def test_startpage(self):
        """Test that startpage is loaded and important links are present."""

        self.driver.get(self.base_url + "/")

        self.stb_test_istina_page_title(u"Главная")

        self.assertRegexpMatches(
            self.driver.find_element_by_css_selector("body").text,
            re.compile(ur"^[\s\S]*Добро пожаловать в систему ИСТИНА![\s\S]*$",
                       flags = re.UNICODE)
        )

        for link_text in (
                u"Главная", u"Поиск", u"Статистика", u"О проекте", u"Помощь",
                u"Войти в систему", u"Регистрация"):
            self.assertTrue(self.is_element_present(By.LINK_TEXT, link_text))
