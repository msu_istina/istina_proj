# -*- coding: utf-8; -*-
"""This module does testing of password reset page.

    - invalid emails are rejected with proper warning
    - non-existing emails are rejected with proper warning
    - password reset email for inactive user is not sent with proper warning
    - password reset email for active user is sent with proper contents
    - non-matching new passwords are rejected with proper warning
    - user is allowed to login with new password after password reset
    - user is not allowed to login with old password after password reset
    - password reset link is not valid once password reset procedure is complete
"""

from django.contrib.sites.models import Site
from django.contrib.auth.models import User

from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumtestsuite import TestMultipleInputFormByCSS
from common.tests.selenium.seleniumtestsuite import TestEmailFormByCSS

from test_register import RegisterToolbox
from test_activation_email import ActivationEmailToolbox
from test_login import LoginToolbox

import re


class PasswordResetToolbox(RegisterToolbox, ActivationEmailToolbox, LoginToolbox):

    def password_reset_test_message(self, message, username, email):
        """Test that password reset email message has proper contents."""

        # no garbage in our messages
        self.assertEqual(message.cc, [])
        self.assertEqual(message.bcc, [])
        self.assertEqual(message.attachments, [])

        # check that To address is correct
        self.assertEqual(len(message.to), 1)
        self.assertEqual(message.to[0], email)

        # check that Subject is correct
        self.assertEqual(
            message.subject,
            u"Сброс пароля на {site}".format(
                site = Site.objects.get_current().domain)
        )

        # check that Body has a link and a correct username
        self.assertRegexpMatches(
            message.body,
            re.compile(
                ur"^Пожалуйста перейдите на следующую страницу и введите новый пароль:\n\n"
                ur"^http://{site}/accounts/reset/[\w-]*/\n\n"
                ur"^Ваше имя пользователя \(вдруг забыли\): {username}$".format(
                    site = Site.objects.get_current().domain,
                    username = username),
                flags = re.UNICODE | re.MULTILINE))

    def password_reset_get_reset_link(self, message):
        """Retrieve relative password reset URL from message."""

        link_re = re.compile(
            ur"^http://{site}(?P<link>/accounts/reset/[\w-]*/)$".format(
                site = Site.objects.get_current().domain),
            flags = re.UNICODE | re.MULTILINE)

        link = link_re.search(message.body).group('link')
        self.assertNotEqual(link, None)

        return link

    def password_reset_request_password_reset_and_click_reset_link(
            self, username, email, password):
        """Register new user, activate it, request password reset
        and click on password reset link in password reset message.

        Returns password reset email message.
        """

        # manually reset message outbox
        if self.mail.outbox: self.mail.outbox = []

        self.register_user_register(username, email, password)

        self.activation_email_activate(self.mail.outbox[0])

        self.driver.get(self.base_url + "/accounts/password/reset/")

        form = self.process_form(
            PasswordResetForm(),
            {
                "email": email
            })
        form.submit()

        link = self.password_reset_get_reset_link(self.mail.outbox[1])
        self.driver.get(self.base_url + link)

        return self.mail.outbox[1]


class PasswordReset(TestEmailFormByCSS, TestMultipleInputFormByCSS,
                    SeleniumToolbox, PasswordResetToolbox):

    def setUp(self):
        super(PasswordReset, self).setUp()

        self.driver.get(self.base_url + "/accounts/password/reset/")

        self.stb_test_istina_page_title(u"Сброс пароля")

        self.request_form = PasswordResetForm()

    def test_password_reset_request_submit_email_invalid(self):
        """Test email field with invalid input."""

        self.email_form_test_invalid(
            "form",
            self._construct_callback(
                self.css_test_single_element_text,
                "input#{id} + span".format(id = "id_email"),
                u"* Введите правильный адрес электронной почты."
            ),
            "id_email"
        )

    def test_password_reset_request_submit_email_non_existing(self):
        """Test that non-existing emails are rejected with proper warning."""

        self.email_form_test_valid(
            "form",
            self._construct_callback(
                self.css_test_single_element_text,
                "input#{id} + span".format(id = "id_email"),
                u"* Этот адрес электронной почты не связан ни с одной учетной записью. "
                u"Вы уверены, что зарегистрированы?"
            ),
            "id_email"
        )

    def test_password_reset_request_for_inactive_user(self):
        """Test that password reset email for inactive user
        is not sent with proper warning.
        """

        username = "username"
        email = "{0}@example.com".format(username)
        password = "password"

        self.register_user_register(username, email, password)

        self.driver.get(self.base_url + "/accounts/password/reset/")

        form = self.process_form(
            self.request_form,
            {
                "email": email
            })
        form.submit()

        self.assertEqual(len(self.mail.outbox), 1)

        self.css_test_single_element_text(
            "input#{id} + span".format(id = "id_email"),
            u"* Этот адрес электронной почты не связан ни с одной учетной записью. "
            u"Вы уверены, что зарегистрированы?"
        )

    def test_password_reset_request_for_active_user(self):
        """Test that password reset email for active user is sent properly
        and proper hint is shown after password reset request completion.
        """
        username = "username"
        email = "{0}@example.com".format(username)
        password = "password"

        self.register_user_register(username, email, password)

        self.activation_email_activate(self.mail.outbox[0])

        self.driver.get(self.base_url + "/accounts/password/reset/")

        form = self.process_form(
            self.request_form,
            {
                "email": email
            })
        form.submit()

        # check that password reset email was sent
        self.assertEqual(len(self.mail.outbox), 2)
        reset_email = self.mail.outbox[1]

        self.password_reset_test_message(reset_email, username, email)

        self.stb_test_istina_page_title(u"Сброс пароля успешно завершен")

        self.css_test_single_element_text(
            "div.success",
            u"Сброс пароля успешно завершен\n"
            u"Мы отправили вам на email адрес инструкции по получению нового пароля. "
            u"Вы получите письмо в ближайшее время.")

    def test_password_reset_submit_new_password1_not_equal_new_password2(self):
        username = "username"
        email = "{0}@example.com".format(username)
        password = "password"

        self.password_reset_request_password_reset_and_click_reset_link(
            username, email, password)

        user = User.objects.get()

        def _test_submit_new_password1_not_equal_new_password2(password1, password2):
            form = self.process_form(
                SetPasswordForm(user),
                {
                    "new_password1": password1,
                    "new_password2": password2
                })
            form.submit()

            self.css_test_multiple_elements_children_text(
                "form > fieldset", "p", "span",
                [
                    [],
                    [u"* Два поля с паролями не совпадают."]
                ]
            )

        for passwords in [
                (u"password", u"PASSWORD"),
                (u"пароль", u"ПАРОЛЬ"),
                # russian letters "а" and "о" in the second password below
                (u"password", u"pаsswоrd"),
                # english letters "a", "o" and "p" in the second password below
                (u"пароль", u"пapoль"),
        ]:
            _test_submit_new_password1_not_equal_new_password2(*passwords)

    def test_password_reset_login_allowed_with_new_password(self):
        """Test that user is allowed to login with new password after password reset."""

        username = "username"
        email = "{0}@example.com".format(username)
        old_password = "password"
        new_password = ur"PassWordПароль!#$%^&*()=[]{{}}\|/,?№;:@.+-_9876543210"

        self.password_reset_request_password_reset_and_click_reset_link(
            username, email, old_password)

        user = User.objects.get()

        form = self.process_form(
            SetPasswordForm(user),
            {
                "new_password1": new_password,
                "new_password2": new_password
            })
        form.submit()

        self.login_user_login(username, new_password)

        self.login_test_login_correct(username)

    def test_password_reset_login_not_allowed_with_old_password(self):
        """Test that user is not allowed to login with old password after password reset."""

        username = "username"
        email = "{0}@example.com".format(username)
        old_password = "password"
        new_password = ur"PassWordПароль!#$%^&*()=[]{{}}\|/,?№;:@.+-_9876543210"

        self.password_reset_request_password_reset_and_click_reset_link(
            username, email, old_password)

        user = User.objects.get()

        form = self.process_form(
            SetPasswordForm(user),
            {
                "new_password1": new_password,
                "new_password2": new_password
            })
        form.submit()

        self.login_user_login(username, old_password)

        self.login_test_login_incorrect()

    def test_password_reset_link_expires_after_reset(self):
        """Test that password reset link expires
        once password reset procedure is complete.
        """

        username = "username"
        email = "{0}@example.com".format(username)
        old_password = "password"
        new_password = ur"PassWordПароль!#$%^&*()=[]{{}}\|/,?№;:@.+-_9876543210"

        reset_email = self.password_reset_request_password_reset_and_click_reset_link(
            username, email, old_password)

        user = User.objects.get()

        form = self.process_form(
            SetPasswordForm(user),
            {
                "new_password1": new_password,
                "new_password2": new_password
            })
        form.submit()

        link = self.password_reset_get_reset_link(reset_email)
        self.driver.get(self.base_url + link)

        self.stb_test_istina_page_title(u"Смена пароля для AnonymousUser")

        self.css_test_single_element_text(
            "div.error",
            u"Сброс пароля не удался\n"
            u"Ссылка для сброса пароля неверна, возможно она уже была использована. "
            u"Пожалуйста, запросите смену пароля ещё раз."
        )
