# -*- coding: utf-8; -*-
"""This module does testing of user activation emails.

    - activation email is sent with proper contents and activation link activates user
    - activation link is not valid once activation procedure is complete
"""

from django.contrib.sites.models import Site
from django.contrib.auth.models import User

from common.tests.selenium.seleniumtestsuite import SeleniumTestCase
from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumtestsuite import TestEmailFormByCSS

from test_register import RegisterToolbox

import re


class ActivationEmailToolbox(SeleniumTestCase):

    def activation_email_test_message(self, message, username, email):
        """Test that activation email message has proper contents.

        This method also used in validation reset tests.
        """

        # no garbage in our messages
        self.assertEqual(message.cc, [])
        self.assertEqual(message.bcc, [])
        self.assertEqual(message.attachments, [])

        # check that To address is correct
        self.assertEqual(len(message.to), 1)
        self.assertEqual(message.to[0], email)

        # check that Subject is correct
        self.assertEqual(
            message.subject,
            u"Подтверждение почты для сайта {site}".format(
                site = Site.objects.get_current().domain)
        )

        # check that Body has a link and a correct username
        self.assertRegexpMatches(
            message.body,
            re.compile(
                ur"^Следуйте по ссылке для подтверждения e-mail адреса:\n\n"
                ur"^http://{site}/accounts/email/validation/\w*/\n\n"
                ur"^Ваше имя пользователя \(вдруг забыли\): {username}$".format(
                    site = Site.objects.get_current().domain,
                    username = username),
                flags = re.UNICODE | re.MULTILINE))

    def activation_email_get_activation_link(self, message):
        """Retrieve relative activation URL from activation message."""

        link_re = re.compile(
            ur"^http://{site}(?P<link>/accounts/email/validation/\w*/)$".format(
                site = Site.objects.get_current().domain),
            flags = re.UNICODE | re.MULTILINE)

        link = link_re.search(message.body).group('link')
        self.assertNotEqual(link, None)

        return link

    def activation_email_activate(self, message):
        """Click on activation link in activation message."""

        link = self.activation_email_get_activation_link(message)
        self.driver.get(self.base_url + link)


class ActivationEmail(TestEmailFormByCSS, SeleniumToolbox,
                      RegisterToolbox, ActivationEmailToolbox):

    def test_activation_email(self):
        """Test that activation email has proper contents and
        a valid link that activates user.
        """

        # manually reset message outbox
        if self.mail.outbox: self.mail.outbox = []

        for i in xrange(len(self.valid_emails)):
            self.register_user_register(
                "username_{0}".format(i),
                self.valid_emails[i],
                "password_{0}".format(i)
            )

            # check that activation email was sent
            self.assertEqual(len(self.mail.outbox), i + 1)

            self.activation_email_test_message(
                self.mail.outbox[i],
                "username_{0}".format(i),
                self.valid_emails[i]
            )

            self.activation_email_activate(self.mail.outbox[i])

            self.stb_test_istina_page_title(u"Вход на сайт")

            self.stb_test_messages_text(
                [u"Вы успешно зарегистрировались. "
                 u"Для входа в систему, пожалуйста, введите логин и пароль."])

            self.assertTrue(User.objects.get(
                username = "username_{0}".format(i)).is_active)

    def test_activation_email_link_expires_after_activation(self):
        """Test that activation link expires once activation procedure is complete."""

        # manually reset message outbox
        if self.mail.outbox: self.mail.outbox = []

        username = "username"
        email = "{0}@example.com".format(username)
        password = "password"

        self.register_user_register(username, email, password)

        # activate created user
        self.activation_email_activate(self.mail.outbox[0])

        # second activation should fail
        self.activation_email_activate(self.mail.outbox[0])

        self.stb_test_istina_page_title(u"Смена e-mail не удалась")

        self.css_test_single_element_text(
            "div.error",
            u"Ключ проверки, который вы получили, уже не действителен. "
            u"Пожалуйста, запросите подтверждение почты ещё раз."
        )
