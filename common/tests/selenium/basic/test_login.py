# -*- coding: utf-8; -*-
"""This module does testing of login page.

    - very long usernames are truncated
    - non-existing usernames are rejected with proper warning
    - login without activation is impossible
    - login after activation is possible
    - invalid username/password combination for registered active user
      is rejected with proper warning
"""

from django.contrib.auth.forms import AuthenticationForm

from common.tests.selenium.seleniumtestsuite import SeleniumTestCase
from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumforms import SeleniumFormAutomaton
from common.tests.selenium.seleniumtestsuite import TestMultipleInputFormByCSS
from common.tests.selenium.seleniumtestsuite import TestEmailFormByCSS

from test_register import RegisterToolbox
from test_activation_email import ActivationEmailToolbox


class LoginToolbox(SeleniumFormAutomaton, SeleniumTestCase):

    def login_user_login(self, username, password):
        """Submit login form with given username and password."""

        self.driver.get(self.base_url + "/accounts/login/")

        form = self.process_form(
            AuthenticationForm(),
            {
                "username": username,
                "password": password
            })
        form.submit()

    def login_test_login_correct(self, username):
        """Test that login was successfull."""

        self.stb_test_istina_page_title(u"{0} - пользователь".format(username))

    def login_test_login_incorrect(self):
        """Test that login failed."""

        self.css_test_single_element_text(
            "ul.errorlist",
            u"Пожалуйста, введите верные имя пользователя и пароль. "
            u"Помните, оба поля чувствительны к регистру."
        )


class Login(TestEmailFormByCSS, TestMultipleInputFormByCSS, SeleniumToolbox,
            RegisterToolbox, ActivationEmailToolbox, LoginToolbox):

    def setUp(self):
        super(Login, self).setUp()

        self.driver.get(self.base_url + "/accounts/login/")

        self.stb_test_istina_page_title(u"Вход на сайт")

        self.form = AuthenticationForm()

    def test_login_submit_user_long(self):
        """Test username field with a long username.

        The length limit is 30 chars. If input string has greater length,
        then it should be truncated to 30 chars.
        """

        for username in (u"username" * 10, u"юзернейм" * 10):
            self.multiple_input_form_fieldset_test_maxlength_field(
                "form", max_length = 30, id_username = username)
            self.multiple_input_form_fieldset_clear("form", id_username = "")

    def _test_login_submit_invalid(self, username, password):
        """Submit invalid username/password combination
        and check that login has failed.
        """

        form = self.process_form(
            self.form,
            {
                "username": username,
                "password": password
            })
        form.submit()

        self.login_test_login_incorrect()

    def test_login_submit_user_non_existing(self):
        """Test that non-existing username/password combinations are rejected."""

        for username, password in [
                (u"admin", u"admin"),
                (u"username", u"password"),
                (u" ", u" "),
        ]:
            self._test_login_submit_invalid(username, password)

    def test_login_user_login_inactive(self):
        """Test that inactive user is not allowed to login."""

        username = "username"
        email = "{0}@example.com".format(username)
        password = "password"

        self.register_user_register(username, email, password)

        self.login_user_login(username, password)

        self.css_test_single_element_text(
            "ul.errorlist", u"Эта учетная запись отключена."
        )

    def test_login_user_login_active(self):
        """Test that user is allowed to login after activation."""

        # manually reset message outbox
        if self.mail.outbox: self.mail.outbox = []

        username = "username"
        email = "{0}@example.com".format(username)
        password = "password"

        self.register_user_register(username, email, password)

        self.activation_email_activate(self.mail.outbox[0])

        self.login_user_login(username, password)

        self.login_test_login_correct(username)

    def test_login_user_login_invalid(self):
        """Test that invalid username/password combination is not allowed
        for registered active user.
        """

        # manually reset message outbox
        if self.mail.outbox: self.mail.outbox = []

        username = "username"
        email = "{0}@example.com".format(username)
        password = "password"

        self.register_user_register(username, email, password)

        self.activation_email_activate(self.mail.outbox[0])

        self.driver.get(self.base_url + "/accounts/login/")

        for username, password in [
                (u"username", u" "),
                (u" ", u"password"),
                (u"username", u"PASSWORD"),
                (u"USERNAME", u"password"),
                # russian letters 'а' and 'е' in username below
                (u"usеrnаmе", u"password"),
                # russian letters 'а', 'р' and 'о' in password below
                (u"username", u"раsswоrd"),
        ]:
            self._test_login_submit_invalid(username, password)
