# -*- coding: utf-8; -*-
"""This module provides class decorator for testing step by step wizards.

Define stepbystep_step*, stepbystep_next and stepbystep_finalize methods
in your class and new test_stepbystep_wizard method will be created for you.
This method calls steps in the alphabetical order, each followed by
stepbystep_next call; in the end stepbystep_finalize is called.
These methods are required to use this class decorator.
You can also use shorter "sbs_" prefix for method names, but you should choose
either one for all your methods.

Optionally you can provide stepbystep_prepare method, which will be called
prior to any steps.

Optionally you can provide stepbystep_pre_foo and stepbystep_post_foo
methods (hooks), which will be called, respectively, before and after
stepbystep_foo call.
"""

import inspect


def SeleniumTestStepByStep(cls):
    PREFIXES = ("stepbystep_", "sbs_")
    REQUIRED_METHODS = ("next", "finalize")
    STEPS_PREFIXES = ("step", )
    HOOKS_PREFIXES = ("pre_", "post_")

    def is_stepbystep_method(name):
        for prefix in PREFIXES:
            if name.startswith(prefix):
                return True

        return False

    def get_unprefixed_name(name):
        for prefix in PREFIXES:
            if name.startswith(prefix):
                return name.replace(prefix, "", 1)

        return name

    def get_stepbystep_methods(inspected_methods):
        stepbystep_methods = []
        stepbystep_methods_names = set()

        for name, method in inspected_methods:
            if is_stepbystep_method(name):
                name = get_unprefixed_name(name)
                if name not in stepbystep_methods_names:
                    stepbystep_methods.append((name, method))
                    stepbystep_methods_names.add(name)
                else:
                    raise RuntimeError(
                        "Class {cls} defines both stepbystep_{method} "
                        "and sbs_{method} methods".format(
                            cls = cls.__name__, method = name))

        return stepbystep_methods

    def is_required_methods_present(stepbystep_methods):
        for req in REQUIRED_METHODS:
            if req not in map(lambda (name, method): name, stepbystep_methods):
                return False

        return True

    def is_stepbystep_step(name):
        for prefix in STEPS_PREFIXES:
            if name.startswith(prefix):
                return True

        return False

    def filter_steps(stepbystep_methods):
        stepbystep_steps = filter(
            lambda (name, method): is_stepbystep_step(name),
            stepbystep_methods)

        return sorted(stepbystep_steps, key = lambda (name, method): name)

    def is_stepbystep_hook(name):
        for prefix in HOOKS_PREFIXES:
            if name.startswith(prefix):
                return True

        return False

    def filter_hooks(stepbystep_methods):
        stepbystep_hooks = filter(
            lambda (name, method): is_stepbystep_hook(name),
            stepbystep_methods)

        return stepbystep_hooks

    def get_method_by_name(name, stepbystep_methods):
        if not stepbystep_methods:
            return None

        ret = filter(lambda(n, method): n == name, stepbystep_methods)
        if ret:
            name, method = ret[0]
            return method

        return None

    def wrap_method_with_hooks(name, stepbystep_methods, stepbystep_hooks):
        pre_hook = get_method_by_name("pre_" + name, stepbystep_hooks)
        post_hook = get_method_by_name("post_" + name, stepbystep_hooks)
        method = get_method_by_name(name, stepbystep_methods)

        def wrapped(self):
            if pre_hook:
                pre_hook(self)
            method(self)
            if post_hook:
                post_hook(self)

        return wrapped

    methods = inspect.getmembers(cls, predicate = inspect.ismethod)

    stepbystep_methods = get_stepbystep_methods(methods)

    stepbystep_steps = filter_steps(stepbystep_methods)

    stepbystep_hooks = filter_hooks(stepbystep_methods)

    if not stepbystep_steps:
        raise NameError(
            "Class {0} does not provide any stepbystep_step "
            "methods".format(cls.__name__))

    if not is_required_methods_present(stepbystep_methods):
        raise NameError(
            "Class {0} does not provide required stepbystep_next and "
            "stepbystep_finalize methods".format(cls.__name__))

    def test_stepbystep_wizard(self):
        if "prepare" in map(lambda (name, method): name, stepbystep_methods):
            wrap_method_with_hooks(
                "prepare", stepbystep_methods, stepbystep_hooks)(self)

        if len(stepbystep_steps) > 1:
            for name, _ in stepbystep_steps[:-1]:
                wrap_method_with_hooks(name,
                                       stepbystep_steps, stepbystep_hooks)(self)

                wrap_method_with_hooks("next",
                                       stepbystep_methods, stepbystep_hooks)(self)

        wrap_method_with_hooks(stepbystep_steps[-1][0],
                               stepbystep_steps, stepbystep_hooks)(self)

        wrap_method_with_hooks("finalize",
                               stepbystep_methods, stepbystep_hooks)(self)

    setattr(cls, "test_stepbystep_wizard", test_stepbystep_wizard)
    return cls
