# -*- coding: utf-8; -*-
"""This module does verification of collection editorial board membership addition procedure.

    - membership for new collection without publisher for self is added properly
    - membership for new collection with publisher for self is added properly
    - membership for new collection for new series for self is added properly
    - membership for new collection for existing series for self is added properly
    - membership for new theses collection for self is added properly
    - membership for existing collection for self is added properly
    - membership for existing worker is added properly
    - membership for existing worker is added properly through alias
    - membership for existing worker is added properly with new alias
    - membership for non-existing worker is added properly
"""

from icollections.models import Collection, CollectionEditorship, Series
from publishers.models import Publisher
from workers.models import Worker

from icollections.views import CollectionEditorshipWizardView as Wizard
from workers.utils import get_fullname_raw

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import (
    CollectionFactory, SeriesFactory, PublisherFactory)

from common.tests.selenium.seleniumwizards import connecttosession, loadsessiondata

import datetime
import random


class AddCollectionEditorialship(RunAsUser, WorkerList, SeleniumToolbox,
                                 SeleniumWizardFormAutomaton):

    @staticmethod
    def theses_to_str(theses):
        if theses:
            return u"Да"
        else:
            return u"Нет"

    def setUp(self):
        super(AddCollectionEditorialship, self).setUp()

        self.name = None

        self.collection = Node(
            "collection", Collection,
            {
                "title": "A collection",
                "year": datetime.date.today().year,
                "series": None,
                "volume": random.randint(0, 10E6),
                "publisher": None,
                "location": "A city",
                "theses": False,
                "abstract": "An abstract"
            })

        self.root = Node(
            "membership", CollectionEditorship,
            {
                "worker": None,
                "collection": self.collection
            })

        self.wizard = self.init_wizard(Wizard)

    @connecttosession
    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/collections/editorships/add/")

        self.stb_test_istina_page_title(
            u"Добавление членства в редколлегии сборника")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(
            u"Добавление членства в редколлегии сборника")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить членство в редколлегии в систему"]').submit()

        self.tree.verify_tree()


class AddColEdtrlshpExistingSeries(AddCollectionEditorialship):

    def setUp(self):
        super(AddColEdtrlshpExistingSeries, self).setUp()

        self.series = SeriesFactory.create_entry()
        self.collection["series"] = Node("series", Series, self.series)


class AddColEdtrlshpExistingPublisher(AddCollectionEditorialship):

    def setUp(self):
        super(AddColEdtrlshpExistingPublisher, self).setUp()

        self.publisher = PublisherFactory.create_entry()
        self.collection["publisher"] = Node(
            "publisher", Publisher, self.publisher)


class AddColEdtrlshpExistingCollection(
        AddColEdtrlshpExistingSeries,
        AddColEdtrlshpExistingPublisher):

    def setUp(self):
        super(AddColEdtrlshpExistingCollection, self).setUp()

        CollectionFactory.create_entry(
            series = self.series,
            publisher = self.publisher,
            **self.collection.lookup)

    def stepbystep_post_finalize(self):
        self.stb_test_messages_text(
            [u"Членство в редколлегии сборника успешно добавлено."])

        self.stb_test_istina_page_title(
            u"{name} в редколлегии сборника {collection} "
            u"({year} год, серия {series}, том {volume}, {location}) "
            u"- членство в редколлегии сборника".format(
                name = self.name, collection = self.collection["title"],
                year = self.collection["year"],
                series = self.collection["series"]["title"],
                volume = self.collection["volume"],
                location = self.collection["location"]))


class AddColEdtrlshpNewCollection(AddCollectionEditorialship):

    def stepbystep_post_finalize(self):
        self.stb_test_messages_text(
            [
                u"Сборник успешно добавлен.",
                u"Членство в редколлегии сборника успешно добавлено."
            ]
        )


@SeleniumTestStepByStep
class AddColEdtrlshpSelfNewCollectionNoPublisher(AddColEdtrlshpNewCollection):

    def setUp(self):
        super(AddColEdtrlshpSelfNewCollectionNoPublisher, self).setUp()

        self.name = self.worker.fullname_short

        self.publisher_str = "A publisher"

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_collection",
            {
                "publisher_str": self.publisher_str,
                "location": self.tree["collection"]["location"],
                "theses": self.theses_to_str(self.tree["collection"]["theses"]),
                "abstract": self.tree["collection"]["abstract"]
            }, initial_data = {
                "title": self.tree["collection"]["title"],
                "volume": unicode(self.tree["collection"]["volume"]),
                "year": unicode(self.tree["collection"]["year"])
            })

        self.tree["collection"]["location"] = ", ".join(
            (self.publisher_str, self.tree["collection"]["location"]))

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_post_finalize(self):
        super(AddColEdtrlshpSelfNewCollectionNoPublisher,
              self).stepbystep_post_finalize()
        self.stb_test_istina_page_title(
            u"{name} в редколлегии сборника {collection} "
            u"({year} год, том {volume}, {location}) "
            u"- членство в редколлегии сборника".format(
                name = self.name, collection = self.collection["title"],
                year = self.collection["year"],
                volume = self.collection["volume"],
                location = self.collection["location"]))


@SeleniumTestStepByStep
class AddColEdtrlshpSelfNewCollectionExistingPublisher(
        AddColEdtrlshpNewCollection, AddColEdtrlshpExistingPublisher):
    # AddColEdtrlshpSelfNewCollectionExistingPublisherThroughSimilar should be
    # added later, when this wizard is fixed
    # Also note that similar publishers are searched separately with and w/o
    # location

    def setUp(self):
        super(AddColEdtrlshpSelfNewCollectionExistingPublisher, self).setUp()

        self.name = self.worker.fullname_short

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_collection",
            {
                "publisher_str": self.tree["collection"]["publisher"]["name"],
                "location": self.tree["collection"]["location"],
                "theses": self.theses_to_str(self.tree["collection"]["theses"]),
                "abstract": self.tree["collection"]["abstract"]
            }, initial_data = {
                "title": self.tree["collection"]["title"],
                "volume": unicode(self.tree["collection"]["volume"]),
                "year": unicode(self.tree["collection"]["year"])
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_post_finalize(self):
        super(AddColEdtrlshpSelfNewCollectionExistingPublisher,
              self).stepbystep_post_finalize()
        self.stb_test_istina_page_title(
            u"{name} в редколлегии сборника {collection} "
            u"({year} год, том {volume}, {location}) "
            u"- членство в редколлегии сборника".format(
                name = self.name, collection = self.collection["title"],
                year = self.collection["year"],
                volume = self.collection["volume"],
                location = self.collection["location"]))


@SeleniumTestStepByStep
class AddColEdtrlshpSelfNewCollectionNewSeries(
        AddColEdtrlshpNewCollection, AddColEdtrlshpExistingPublisher):

    def setUp(self):
        super(AddColEdtrlshpSelfNewCollectionNewSeries, self).setUp()

        self.name = self.worker.fullname_short

        self.collection["series"] = Node("series", Series, {"title": "Series"})

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_collection",
            {
                "series_str": self.tree["collection"]["series"]["title"],
                "publisher_str": self.tree["collection"]["publisher"]["name"],
                "location": self.tree["collection"]["location"],
                "theses": self.theses_to_str(self.tree["collection"]["theses"]),
                "abstract": self.tree["collection"]["abstract"]
            }, initial_data = {
                "title": self.tree["collection"]["title"],
                "volume": unicode(self.tree["collection"]["volume"]),
                "year": unicode(self.tree["collection"]["year"])
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_post_finalize(self):
        super(AddColEdtrlshpSelfNewCollectionNewSeries,
              self).stepbystep_post_finalize()
        self.stb_test_istina_page_title(
            u"{name} в редколлегии сборника {collection} "
            u"({year} год, серия {series}, том {volume}, {location}) "
            u"- членство в редколлегии сборника".format(
                name = self.name, collection = self.collection["title"],
                year = self.collection["year"],
                series = self.collection["series"]["title"],
                volume = self.collection["volume"],
                location = self.collection["location"]))


@SeleniumTestStepByStep
class AddColEdtrlshpSelfNewCollectionNewSeriesThroughSimilar(
        AddColEdtrlshpNewCollection, AddColEdtrlshpExistingPublisher):

    def setUp(self):
        super(AddColEdtrlshpSelfNewCollectionNewSeriesThroughSimilar, self).setUp()

        self.name = self.worker.fullname_short

        self.collection["series"] = Node("series", Series, {"title": "Series"})

        SeriesFactory.create_entry(
            title = self.collection["series"]["title"].upper())

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Series] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_collection",
            {
                "series_str": self.tree["collection"]["series"]["title"],
                "publisher_str": self.tree["collection"]["publisher"]["name"],
                "location": self.tree["collection"]["location"],
                "theses": self.theses_to_str(self.tree["collection"]["theses"]),
                "abstract": self.tree["collection"]["abstract"]
            }, initial_data = {
                "title": self.tree["collection"]["title"],
                "volume": unicode(self.tree["collection"]["volume"]),
                "year": unicode(self.tree["collection"]["year"])
            })

    @loadsessiondata
    def stepbystep_step3(self):
        self.process_wizard_form(
            self.wizard, "similar_series_publishers",
            {
                "series": u" ".join((self.tree["collection"]["series"]["title"],
                                     u"(добавить новую серию)"))
            })

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_post_finalize(self):
        super(AddColEdtrlshpSelfNewCollectionNewSeriesThroughSimilar,
              self).stepbystep_post_finalize()
        self.stb_test_istina_page_title(
            u"{name} в редколлегии сборника {collection} "
            u"({year} год, серия {series}, том {volume}, {location}) "
            u"- членство в редколлегии сборника".format(
                name = self.name, collection = self.collection["title"],
                year = self.collection["year"],
                series = self.collection["series"]["title"],
                volume = self.collection["volume"],
                location = self.collection["location"]))


@SeleniumTestStepByStep
class AddColEdtrlshpSelfNewCollectionExistingSeriesThroughSimilar(
        AddColEdtrlshpNewCollection,
        AddColEdtrlshpExistingPublisher,
        AddColEdtrlshpExistingSeries):

    def setUp(self):
        super(AddColEdtrlshpSelfNewCollectionExistingSeriesThroughSimilar, self).setUp()

        self.name = self.worker.fullname_short

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_collection",
            {
                "series_str": self.tree["collection"]["series"]["title"].upper(),
                "publisher_str": self.tree["collection"]["publisher"]["name"],
                "location": self.tree["collection"]["location"],
                "theses": self.theses_to_str(self.tree["collection"]["theses"]),
                "abstract": self.tree["collection"]["abstract"]
            }, initial_data = {
                "title": self.tree["collection"]["title"],
                "volume": unicode(self.tree["collection"]["volume"]),
                "year": unicode(self.tree["collection"]["year"])
            })

    @loadsessiondata
    def stepbystep_step3(self):
        self.process_wizard_form(
            self.wizard, "similar_series_publishers",
            {
                "series": self.tree["collection"]["series"]["title"]
            })

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_post_finalize(self):
        super(AddColEdtrlshpSelfNewCollectionExistingSeriesThroughSimilar,
              self).stepbystep_post_finalize()
        self.stb_test_istina_page_title(
            u"{name} в редколлегии сборника {collection} "
            u"({year} год, серия {series}, том {volume}, {location}) "
            u"- членство в редколлегии сборника".format(
                name = self.name, collection = self.collection["title"],
                year = self.collection["year"],
                series = self.collection["series"]["title"],
                volume = self.collection["volume"],
                location = self.collection["location"]))


@SeleniumTestStepByStep
class AddColEdtrlshpSelfNewCollectionExistingSeries(
        AddColEdtrlshpNewCollection,
        AddColEdtrlshpExistingPublisher,
        AddColEdtrlshpExistingSeries):

    def setUp(self):
        super(AddColEdtrlshpSelfNewCollectionExistingSeries, self).setUp()

        self.name = self.worker.fullname_short

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_collection",
            {
                "series_str": self.tree["collection"]["series"]["title"],
                "publisher_str": self.tree["collection"]["publisher"]["name"],
                "location": self.tree["collection"]["location"],
                "theses": self.theses_to_str(self.tree["collection"]["theses"]),
                "abstract": self.tree["collection"]["abstract"]
            }, initial_data = {
                "title": self.tree["collection"]["title"],
                "volume": unicode(self.tree["collection"]["volume"]),
                "year": unicode(self.tree["collection"]["year"])
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_post_finalize(self):
        super(AddColEdtrlshpSelfNewCollectionExistingSeries,
              self).stepbystep_post_finalize()
        self.stb_test_istina_page_title(
            u"{name} в редколлегии сборника {collection} "
            u"({year} год, серия {series}, том {volume}, {location}) "
            u"- членство в редколлегии сборника".format(
                name = self.name, collection = self.collection["title"],
                year = self.collection["year"],
                series = self.collection["series"]["title"],
                volume = self.collection["volume"],
                location = self.collection["location"]))


@SeleniumTestStepByStep
class AddColEdtrlshpSelfNewCollectionTheses(
        AddColEdtrlshpNewCollection,
        AddColEdtrlshpExistingPublisher,
        AddColEdtrlshpExistingSeries):

    def setUp(self):
        super(AddColEdtrlshpSelfNewCollectionTheses, self).setUp()

        self.name = self.worker.fullname_short

        self.collection["theses"] = True

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_collection",
            {
                "series_str": self.tree["collection"]["series"]["title"],
                "publisher_str": self.tree["collection"]["publisher"]["name"],
                "location": self.tree["collection"]["location"],
                "theses": self.theses_to_str(self.tree["collection"]["theses"]),
                "abstract": self.tree["collection"]["abstract"]
            }, initial_data = {
                "title": self.tree["collection"]["title"],
                "volume": unicode(self.tree["collection"]["volume"]),
                "year": unicode(self.tree["collection"]["year"])
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_post_finalize(self):
        super(AddColEdtrlshpSelfNewCollectionTheses,
              self).stepbystep_post_finalize()
        self.stb_test_istina_page_title(
            u"{name} в редколлегии сборника {collection} "
            u"({year} год, серия {series}, том {volume}, {location}, тезисы) "
            u"- членство в редколлегии сборника".format(
                name = self.name, collection = self.collection["title"],
                year = self.collection["year"],
                series = self.collection["series"]["title"],
                volume = self.collection["volume"],
                location = self.collection["location"]))


@SeleniumTestStepByStep
class AddColEdtrlshpSelfNewCollectionThroughSimilar(
        AddColEdtrlshpNewCollection,
        AddColEdtrlshpExistingPublisher,
        AddColEdtrlshpExistingSeries):

    def setUp(self):
        super(AddColEdtrlshpSelfNewCollectionThroughSimilar, self).setUp()

        CollectionFactory.create_entry(
            title = self.collection["title"].upper(),
            year = self.collection["year"],
            volume = self.collection["volume"])

        self.name = self.worker.fullname_short

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Collection] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    @loadsessiondata
    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "similar_collections",
            {
                "collection": u" ".join((self.tree["collection"]["title"],
                                         u"(добавить новый сборник)"))
            })

    def stepbystep_step3(self):
        self.process_wizard_form(
            self.wizard, "new_collection",
            {
                "series_str": self.tree["collection"]["series"]["title"],
                "publisher_str": self.tree["collection"]["publisher"]["name"],
                "location": self.tree["collection"]["location"],
                "theses": self.theses_to_str(self.tree["collection"]["theses"]),
                "abstract": self.tree["collection"]["abstract"]
            }, initial_data = {
                "title": self.tree["collection"]["title"],
                "volume": unicode(self.tree["collection"]["volume"]),
                "year": unicode(self.tree["collection"]["year"])
            })

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_post_finalize(self):
        super(AddColEdtrlshpSelfNewCollectionThroughSimilar,
              self).stepbystep_post_finalize()
        self.stb_test_istina_page_title(
            u"{name} в редколлегии сборника {collection} "
            u"({year} год, серия {series}, том {volume}, {location}) "
            u"- членство в редколлегии сборника".format(
                name = self.name, collection = self.collection["title"],
                year = self.collection["year"],
                series = self.collection["series"]["title"],
                volume = self.collection["volume"],
                location = self.collection["location"]))


@SeleniumTestStepByStep
class AddColEdtrlshpSelfExistingCollectionThroughSimilar(
        AddColEdtrlshpExistingCollection):

    def setUp(self):
        super(AddColEdtrlshpSelfExistingCollectionThroughSimilar, self).setUp()

        self.name = self.worker.fullname_short

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"].upper(),
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    @loadsessiondata
    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "similar_collections",
            {
                "collection": u"{collection} ({year} год, серия {series}, "
                u"том {volume}, {location})".format(
                    collection = self.collection["title"],
                    year = self.collection["year"],
                    series = self.collection["series"]["title"],
                    volume = self.collection["volume"],
                    location = self.collection["location"])
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddColEdtrlshpSelfExistingCollection(AddColEdtrlshpExistingCollection):

    def setUp(self):
        super(AddColEdtrlshpSelfExistingCollection, self).setUp()

        self.name = self.worker.fullname_short

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddColEdtrlshpExistingWorker(AddColEdtrlshpExistingCollection):

    def setUp(self):
        super(AddColEdtrlshpExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.name = self.worker.fullname_short

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddColEdtrlshpExistingWorkerThroughAlias(AddColEdtrlshpExistingCollection):

    def setUp(self):
        super(AddColEdtrlshpExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.name = self.worker.fullname_short

        self.root["worker"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddColEdtrlshpExistingWorkerNewAlias(AddColEdtrlshpExistingCollection):

    def setUp(self):
        super(AddColEdtrlshpExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.name = self.worker.fullname_short

        self.root["worker"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddColEdtrlshpNewWorker(AddColEdtrlshpExistingCollection):

    def setUp(self):
        super(AddColEdtrlshpNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.name = get_fullname_raw(
            self.worker_node["lastname"],
            self.worker_node["firstname"],
            self.worker_node["middlename"],
            initials = True)

        self.root["worker"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "collection_str": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "year": self.tree["collection"]["year"],
                "worker_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
