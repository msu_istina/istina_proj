# -*- coding: utf-8; -*-
"""This module does verification of dissertation council membership addition procedure.

    - membership for new council for non-existing organization for self is added properly
    - membership for new council for existing organization for self is added properly
    - membership for existing council for self is added properly
    - membership without post for self is added properly
    - membership with post for self is added properly
    - membership for existing worker is added properly
    - membership for existing worker is added properly through alias
    - membership for existing worker is added properly with new alias
    - membership for non-existing worker is added properly
"""

from dissertation_councils.models import DissertationCouncil
from dissertation_councils.models import DissertationCouncilPost
from dissertation_councils.models import DissertationCouncilMembership
from organizations.models import Organization
from workers.models import Worker

from dissertation_councils.views import DissertationCouncilMembershipWizardView as Wizard
from workers.utils import get_fullname_raw

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import (
    DissertationCouncilFactory, DissertationCouncilPostFactory, OrganizationFactory)

import datetime
import random


class AddCouncilMembership(RunAsUser, WorkerList, SeleniumToolbox,
                           SeleniumWizardFormAutomaton):

    @staticmethod
    def post_to_str(post):
        if post is not None:
            return post["name"]
        else:
            return "---------"

    def setUp(self):
        super(AddCouncilMembership, self).setUp()

        self.name = None

        self.membership = Node(
            "membership", DissertationCouncilMembership,
            {
                "member": None,
                "post": None,
                "startdate": datetime.date.today(),
                "enddate": datetime.date.today()
            })

        self.root = Node(
            "council", DissertationCouncil,
            {
                "number": unicode(random.randint(0, 10E6)),
                "organization_str": "",
                "organization": None,
                "address": "",
                "place": "",
                "phone": "",
                "email": "",
                "specialties": [],
                "memberships": [self.membership]
            })

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.driver.get(
            self.base_url + "/dissertation_councils/memberships/add/")

        self.stb_test_istina_page_title(
            u"Добавление членства в диссертационном совете")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(
            u"Добавление членства в диссертационном совете")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить членство в совете в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{name} в совете #{number} - членство в диссертационном совете".format(
                name = self.name, number = self.root["number"]))

        self.stb_test_messages_text(
            [u"Членство в диссертационном совете успешно добавлено."])

        self.tree.verify_tree()


class AddCouncilMembershipExistingOrganization(AddCouncilMembership):

    def setUp(self):
        super(AddCouncilMembershipExistingOrganization, self).setUp()

        self.organization = OrganizationFactory.create_entry(
            name = "An organization")

        self.root["organization"] = Node(
            "organization", Organization, self.organization)


class AddCouncilMembershipExistingPost(AddCouncilMembershipExistingOrganization):

    def setUp(self):
        super(AddCouncilMembershipExistingPost, self).setUp()

        self.post = DissertationCouncilPostFactory.create_entry(
            name = "Position")

        self.membership["post"] = Node(
            "post", DissertationCouncilPost, self.post)


class AddCouncilMembershipExistingCouncil(AddCouncilMembershipExistingPost):

    def setUp(self):
        super(AddCouncilMembershipExistingCouncil, self).setUp()

        self.council = DissertationCouncilFactory.create_entry(
            number = self.root["number"], organization = self.organization)


@SeleniumTestStepByStep
class AddCouncilMembershipSelfNewCouncilOrganizationStr(AddCouncilMembership):

    def setUp(self):
        super(AddCouncilMembershipSelfNewCouncilOrganizationStr, self).setUp()

        self.name = self.worker.fullname_short

        self.membership["member"] = Node("worker", Worker, self.worker)

        self.root["organization_str"] = "Another organization"

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "council_number": self.tree["council"]["number"],
                "post": self.post_to_str(self.tree["membership"]["post"]),
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "organization",
            {
                "organization_str": self.root["organization_str"]
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCouncilMembershipSelfNewCouncilExistingOrg(
        AddCouncilMembershipExistingOrganization):

    def setUp(self):
        super(AddCouncilMembershipSelfNewCouncilExistingOrg, self).setUp()

        self.name = self.worker.fullname_short

        self.membership["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "council_number": self.tree["council"]["number"],
                "post": self.post_to_str(self.tree["membership"]["post"]),
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "organization",
            {
                "organization_str": self.root["organization"]["name"]
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCouncilMembershipSelfNewCouncilExistingPost(AddCouncilMembershipExistingPost):

    def setUp(self):
        super(AddCouncilMembershipSelfNewCouncilExistingPost, self).setUp()

        self.name = self.worker.fullname_short

        self.membership["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "council_number": self.tree["council"]["number"],
                "post": self.post_to_str(self.tree["membership"]["post"]),
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "organization",
            {
                "organization_str": self.root["organization"]["name"]
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCouncilMembershipSelfExistingCouncil(AddCouncilMembershipExistingCouncil):

    def setUp(self):
        super(AddCouncilMembershipSelfExistingCouncil, self).setUp()

        self.name = self.worker.fullname_short

        self.membership["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "council_number": self.tree["council"]["number"],
                "post": self.post_to_str(self.tree["membership"]["post"]),
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCouncilMembershipExistingWorker(AddCouncilMembershipExistingCouncil):

    def setUp(self):
        super(AddCouncilMembershipExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.name = self.worker.fullname_short

        self.membership["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "council_number": self.tree["council"]["number"],
                "post": self.post_to_str(self.tree["membership"]["post"]),
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCouncilMembershipExistingWorkerThroughAlias(AddCouncilMembershipExistingCouncil):

    def setUp(self):
        super(AddCouncilMembershipExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.name = self.worker.fullname_short

        self.membership["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "council_number": self.tree["council"]["number"],
                "post": self.post_to_str(self.tree["membership"]["post"]),
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddCouncilMembershipExistingWorkerNewAlias(AddCouncilMembershipExistingCouncil):

    def setUp(self):
        super(AddCouncilMembershipExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.name = self.worker.fullname_short

        self.membership["member"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "council_number": self.tree["council"]["number"],
                "post": self.post_to_str(self.tree["membership"]["post"]),
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddCouncilMembershipNewWorker(AddCouncilMembershipExistingCouncil):

    def setUp(self):
        super(AddCouncilMembershipNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.name = get_fullname_raw(
            self.worker_node["lastname"],
            self.worker_node["firstname"],
            self.worker_node["middlename"],
            initials = True)

        self.membership["member"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "council_number": self.tree["council"]["number"],
                "post": self.post_to_str(self.tree["membership"]["post"]),
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
