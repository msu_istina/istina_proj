# -*- coding: utf-8; -*-
"""This module does verification of conference presentation addition procedures.

    - presentation for new conference for self is added properly
    - presentation for existing conference for self is added properly
    - presentation for existing worker is added properly
    - presentation for existing worker is added properly through alias
    - presentation for existing worker is added properly with new alias
    - presentation for non-existing worker is added properly
"""

from conferences.models import Conference, ConferencePresentation
from conferences.models import ConferencePresentationAuthorship as PAuthorship
from workers.models import Worker

from conferences.views import ConferencePresentationWizardView as Wizard

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import ConferenceFactory

import datetime


class AddPresentation(RunAsUser, WorkerList, SeleniumToolbox,
                      SeleniumWizardFormAutomaton):

    def setUp(self):
        super(AddPresentation, self).setUp()

        self.authorship = Node(
            "authorship", PAuthorship,
            {
                "author": None,
                "original_name": ""
            })

        self.conference = Node(
            "conference", Conference,
            {
                "name": "A conference",
                "location": "A city",
                "year": datetime.date.today().year,
                "description": "A description"
            })

        self.root = Node(
            "presentation", ConferencePresentation,
            {
                "title": "A title",
                "conference": self.conference,
                "abstract": "An abstract",
                "authorships": [self.authorship]
            })

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/conferences/presentations/add/")

        self.stb_test_istina_page_title(u"Добавление доклада на конференции")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(u"Добавление доклада на конференции")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить доклад на конференции в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{title} - доклад на конференции".format(
                title = self.root["title"]))

        self.tree.verify_tree()


class AddPresentationExistingConference(AddPresentation):

    def setUp(self):
        super(AddPresentationExistingConference, self).setUp()

        ConferenceFactory.create_entry(**self.conference.lookup)

    def stepbystep_post_finalize(self):
        self.stb_test_messages_text(
            [u"Доклад на конференции успешно добавлен."])


class AddPresentationNewConference(AddPresentation):

    def stepbystep_post_finalize(self):
        self.stb_test_messages_text(
            [
                u"Конференция успешно добавлена.",
                u"Доклад на конференции успешно добавлен."
            ]
        )


@SeleniumTestStepByStep
class AddPresentationSelfNewConference(AddPresentationNewConference):

    def setUp(self):
        super(AddPresentationSelfNewConference, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["presentation"]["title"],
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "abstract": self.tree["presentation"]["abstract"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_conference",
            {
                "location": self.tree["conference"]["location"],
                "description": self.tree["conference"]["description"]
            }, initial_data = {
                "name": self.tree["conference"]["name"],
                "year": unicode(self.tree["conference"]["year"])
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddPresentationSelfNewConferenceThroughSimilar(AddPresentationNewConference):

    def setUp(self):
        super(AddPresentationSelfNewConferenceThroughSimilar, self).setUp()

        ConferenceFactory.create_entry(
            name = self.conference["name"].upper(),
            year = self.conference["year"])

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Conference] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["presentation"]["title"],
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "abstract": self.tree["presentation"]["abstract"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "similar_conferences",
            {
                "conference": u" ".join((self.tree["conference"]["name"],
                                         u"(добавить новую конференцию)"))
            })

    def stepbystep_step3(self):
        self.process_wizard_form(
            self.wizard, "new_conference",
            {
                "location": self.tree["conference"]["location"],
                "description": self.tree["conference"]["description"]
            }, initial_data = {
                "name": self.tree["conference"]["name"],
                "year": unicode(self.tree["conference"]["year"])
            })

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddPresentationSelfExistingConferenceThroughSimilar(
        AddPresentationExistingConference):

    def setUp(self):
        super(AddPresentationSelfExistingConferenceThroughSimilar, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["presentation"]["title"],
                "conference_str": self.tree["conference"]["name"].upper(),
                "year": self.tree["conference"]["year"],
                "abstract": self.tree["presentation"]["abstract"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "similar_conferences",
            {
                "conference": "{name} ({year})".format(
                    name = self.tree["conference"]["name"],
                    year = self.tree["conference"]["year"])
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddPresentationSelfExistingConference(AddPresentationExistingConference):

    def setUp(self):
        super(AddPresentationSelfExistingConference, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["presentation"]["title"],
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "abstract": self.tree["presentation"]["abstract"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddPresentationExistingWorker(AddPresentationExistingConference):

    def setUp(self):
        super(AddPresentationExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["presentation"]["title"],
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "abstract": self.tree["presentation"]["abstract"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddPresentationExistingWorkerThroughAlias(AddPresentationExistingConference):

    def setUp(self):
        super(AddPresentationExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.alias.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["presentation"]["title"],
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "abstract": self.tree["presentation"]["abstract"],
                "authors_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddPresentationExistingWorkerNewAlias(AddPresentationExistingConference):

    def setUp(self):
        super(AddPresentationExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker.fullname_short

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["presentation"]["title"],
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "abstract": self.tree["presentation"]["abstract"],
                "authors_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddPresentationNewWorker(
        AddPresentationExistingConference):

    def setUp(self):
        super(AddPresentationNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker_node.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["presentation"]["title"],
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "abstract": self.tree["presentation"]["abstract"],
                "authors_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
