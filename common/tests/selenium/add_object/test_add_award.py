# -*- coding: utf-8; -*-
"""This module does verification of award addition procedure.

    - award for self with custom name is added properly
    - award for self with listed name is added properly
    - award for existing worker is added properly
    - award for existing worker is added properly through alias
    - award for existing worker is added properly with new alias
    - award for non-existing worker is added properly
"""

from awards.models import Award, AwardAuthorship
from awards.models import AwardCategory, AwardName
from common.models import Country
from workers.models import Worker

from awards.forms import AwardForm
from common.views import LinkedToWorkersModelWizardView as Wizard
Wizard = Wizard.create_wizard_from_form(AwardForm)

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import (
    AwardCategoryFactory, AwardNameFactory, CountryFactory)

import datetime


class AddAward(RunAsUser, WorkerList, SeleniumToolbox,
               SeleniumWizardFormAutomaton):

    def setUp(self):
        super(AddAward, self).setUp()

        self.country = CountryFactory.create_entry(name = "COUNTRY")

        self.award_category = AwardCategoryFactory.create_entry(
            name = "Award category")

        self.award_name = AwardNameFactory.create_entry(
            name = "Award name", category = self.award_category)

        self.name = self.award_name.name

        self.authorship = Node(
            "authorship", AwardAuthorship,
            {
                "author": None,
                "original_name": "",
                "position": 1
            })

        self.root = Node(
            "award", Award,
            {
                "name_from_list": None,
                "name_custom_text": "",
                "category": Node("category", AwardCategory, self.award_category),
                "organization": "An organization",
                "country": Node("country", Country, self.country),
                "startdate": datetime.date.today(),
                "comment": "A comment",
                "authorships": [self.authorship]
            })

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/awards/add/")

        self.stb_test_istina_page_title(u"Добавление награды")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(u"Добавление награды")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить награду в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{name} - награда".format(name = self.name))

        self.stb_test_messages_text([u"Награда успешно добавлена."])

        self.tree.verify_tree()


class AddAwardFromList(AddAward):

    def setUp(self):
        super(AddAwardFromList, self).setUp()

        self.root["name_from_list"] = Node("name", AwardName, self.award_name)


@SeleniumTestStepByStep
class AddAwardSelfNameCustomText(AddAward):

    def setUp(self):
        super(AddAwardSelfNameCustomText, self).setUp()

        self.name = "An award"

        self.root["name_custom_text"] = self.name

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[AwardName] = 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "name_custom_text": self.tree["award"]["name_custom_text"],
                "category": self.tree["award"]["category"]["name"],
                "organization": self.tree["award"]["organization"],
                "country": self.tree["award"]["country"]["name"],
                "startdate": self.tree["award"]["startdate"].strftime("%d.%m.%Y"),
                "comment": self.tree["award"]["comment"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddAwardSelfNameFromList(AddAwardFromList):

    def setUp(self):
        super(AddAwardSelfNameFromList, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "name_from_list": self.tree["award"]["name_from_list"]["name"],
                "category": self.tree["award"]["category"]["name"],
                "organization": self.tree["award"]["organization"],
                "country": self.tree["award"]["country"]["name"],
                "startdate": self.tree["award"]["startdate"].strftime("%d.%m.%Y"),
                "comment": self.tree["award"]["comment"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddAwardExistingWorker(AddAwardFromList):

    def setUp(self):
        super(AddAwardExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "name_from_list": self.tree["award"]["name_from_list"]["name"],
                "category": self.tree["award"]["category"]["name"],
                "organization": self.tree["award"]["organization"],
                "country": self.tree["award"]["country"]["name"],
                "startdate": self.tree["award"]["startdate"].strftime("%d.%m.%Y"),
                "comment": self.tree["award"]["comment"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddAwardExistingWorkerThroughAlias(AddAwardFromList):

    def setUp(self):
        super(AddAwardExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.alias.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "name_from_list": self.tree["award"]["name_from_list"]["name"],
                "category": self.tree["award"]["category"]["name"],
                "organization": self.tree["award"]["organization"],
                "country": self.tree["award"]["country"]["name"],
                "startdate": self.tree["award"]["startdate"].strftime("%d.%m.%Y"),
                "comment": self.tree["award"]["comment"],
                "authors_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddAwardExistingWorkerNewAlias(AddAwardFromList):

    def setUp(self):
        super(AddAwardExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker.fullname_short

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "name_from_list": self.tree["award"]["name_from_list"]["name"],
                "category": self.tree["award"]["category"]["name"],
                "organization": self.tree["award"]["organization"],
                "country": self.tree["award"]["country"]["name"],
                "startdate": self.tree["award"]["startdate"].strftime("%d.%m.%Y"),
                "comment": self.tree["award"]["comment"],
                "authors_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddAwardNewWorker(AddAwardFromList):

    def setUp(self):
        super(AddAwardNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker_node.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "name_from_list": self.tree["award"]["name_from_list"]["name"],
                "category": self.tree["award"]["category"]["name"],
                "organization": self.tree["award"]["organization"],
                "country": self.tree["award"]["country"]["name"],
                "startdate": self.tree["award"]["startdate"].strftime("%d.%m.%Y"),
                "comment": self.tree["award"]["comment"],
                "authors_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
