# -*- coding: utf-8; -*-
"""This module does verification of book addition procedure.

    - book without publisher is added properly
    - book with publisher is added properly
    - book for existing worker is added properly
    - book for existing worker is added properly through alias
    - book for existing worker is added properly with new alias
    - book for non-existing worker is added properly
"""

from publications.models import Book, BookAuthorship
from publishers.models import Publisher
from workers.models import Worker

from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import PublisherFactory
from add_publication import AddPublication

import datetime
import random


class AddBook(AddPublication):

    def setUp(self):
        super(AddBook, self).setUp()

        self.authorship = Node(
            "authorship", BookAuthorship,
            {
                "author": None,
                "original_name": "",
                "position": 1
            })

        self.root = Node(
            "book", Book,
            {
                "title": "A book",
                "year": datetime.date.today().year,
                "publisher": None,
                "location": "",
                "pages": random.randint(0, 10E6),
                "isbn": "123-4-56789-012-3",
                # meaningful abstract and doi are added to node later
                # as they can be added only during edit step
                "abstract": None,
                "doi": None,
                "categories": [],
                "language": None,
                "authorships": [self.authorship]
            })

    def stepbystep_step1(self):
        pass

    def stepbystep_pre_step2(self):
        super(AddBook, self).stepbystep_pre_step2()
        self.radiobutton_check_by_id("work_type", "in_book")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить публикацию и '
            u'перейти к следующему шагу добавления публикации"]').submit()

        self.stb_test_istina_page_title(
            u"Добавление дополнительной информации")

        self.stb_test_messages_text(
            [u"Книга {title} успешно добавлена.".format(
                title = self.root["title"])])

        self.tree.verify_tree()

        self.tree["book"]["abstract"] = "An abstract"
        self.tree["book"]["doi"] = "12:3456/i.am.an.example.d0i.000.111"

        self.css_set_single_element(
            "form#additional_form textarea#id_abstract",
            self.tree["book"]["abstract"])

        self.css_set_single_element(
            "form#additional_form input#id_doi",
            self.tree["book"]["doi"])

        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить информацию в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{title} - книга".format(title = self.root["title"]))

        self.stb_test_messages_text([u"Книга успешно отредактирована."])

        self.tree.verify_tree()


class AddBookExistingPublisher(AddBook):

    def setUp(self):
        super(AddBookExistingPublisher, self).setUp()

        self.publisher = PublisherFactory.create_entry()
        self.root["publisher"] = Node("publisher", Publisher, self.publisher)


@SeleniumTestStepByStep
class AddBookSelfNoPublisher(AddBook):

    def setUp(self):
        super(AddBookSelfNoPublisher, self).setUp()

        self.publisher_str = "A publisher"
        self.root["location"] = "A city"

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["book"]["title"],
                "publisher_name": self.publisher_str,
                "publisher_city": self.tree["book"]["location"],
                "year": self.tree["book"]["year"],
                "pages": self.tree["book"]["pages"],
                "isbn": self.tree["book"]["isbn"],
                "authors": self.worker.fullname
            })

        self.tree["book"]["location"] = " ".join(
            (self.publisher_str, self.tree["book"]["location"]))

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddBookSelfNoPublisherThroughSimilar(AddBook):

    def setUp(self):
        super(AddBookSelfNoPublisherThroughSimilar, self).setUp()

        self.publisher = PublisherFactory.create_entry()
        self.root["location"] = "A city"

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Publisher] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["book"]["title"],
                "publisher_name": self.publisher.name,
                "publisher_city": self.tree["book"]["location"],
                "year": self.tree["book"]["year"],
                "pages": self.tree["book"]["pages"],
                "isbn": self.tree["book"]["isbn"],
                "authors": self.worker.fullname
            })

        self.tree["book"]["location"] = " ".join(
            (self.publisher.name, self.tree["book"]["location"]))

    def stepbystep_step3(self):
        self.radiobutton_check_by_id("publisher-id", "id_publisher-id-create")

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddBookSelfExistingPublisher(AddBookExistingPublisher):

    def setUp(self):
        super(AddBookSelfExistingPublisher, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["book"]["title"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["book"]["year"],
                "pages": self.tree["book"]["pages"],
                "isbn": self.tree["book"]["isbn"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddBookExistingWorker(AddBookExistingPublisher):

    def setUp(self):
        super(AddBookExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["book"]["title"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["book"]["year"],
                "pages": self.tree["book"]["pages"],
                "isbn": self.tree["book"]["isbn"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddBookExistingWorkerThroughAlias(AddBookExistingPublisher):

    def setUp(self):
        super(AddBookExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.alias.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["book"]["title"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["book"]["year"],
                "pages": self.tree["book"]["pages"],
                "isbn": self.tree["book"]["isbn"],
                "authors": self.alias.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.alias.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddBookExistingWorkerNewAlias(AddBookExistingPublisher):

    def setUp(self):
        super(AddBookExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker.fullname_short

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["book"]["title"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["book"]["year"],
                "pages": self.tree["book"]["pages"],
                "isbn": self.tree["book"]["isbn"],
                "authors": self.worker.fullname_short
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short,
            has_unknown = True)


@SeleniumTestStepByStep
class AddBookNewWorker(AddBookExistingPublisher):

    def setUp(self):
        super(AddBookNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker_node.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["book"]["title"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["book"]["year"],
                "pages": self.tree["book"]["pages"],
                "isbn": self.tree["book"]["isbn"],
                "authors": self.worker_node.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
