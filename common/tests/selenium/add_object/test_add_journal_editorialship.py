# -*- coding: utf-8; -*-
"""This module does verification of journal editorial board membership addition procedure.

    - membership for new journal for self is added properly
    - membership for existing journal for self is added properly
    - membership for existing worker is added properly
    - membership for existing worker is added properly through alias
    - membership for existing worker is added properly with new alias
    - membership for non-existing worker is added properly
"""

from journals.models import JournalEditorialBoardMembership as JMembership
from journals.models import Journal
from workers.models import Worker

from journals.views import JournalEditorialBoardMembershipWizardView as Wizard
from workers.utils import get_fullname_raw

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import JournalFactory

from common.tests.selenium.seleniumwizards import connecttosession, loadsessiondata

import datetime


class AddJournalEditorialship(RunAsUser, WorkerList, SeleniumToolbox,
                              SeleniumWizardFormAutomaton):

    def setUp(self):
        super(AddJournalEditorialship, self).setUp()

        self.name = None

        self.journal = Node(
            "journal", Journal,
            {
                "name": "A journal",
                "publisher": None,
                "url": None,
                "language": None,
                "abstract": ""
            })

        self.root = Node(
            "membership", JMembership,
            {
                "member": None,
                "journal": self.journal,
                "startdate": datetime.date.today(),
                "enddate": datetime.date.today()
            })

        self.wizard = self.init_wizard(Wizard)

    @connecttosession
    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/journals/memberships/add/")

        self.stb_test_istina_page_title(
            u"Добавление членства в редколлегии журнала")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(
            u"Добавление членства в редколлегии журнала")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"]'
            u'[title="Сохранить членство в редколлегии в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{name} в редколлегии журнала {journal} "
            u"- членство в редколлегии журнала".format(
                name = self.name, journal = self.journal["name"]))

        self.stb_test_messages_text(
            [u"Членство в редколлегии журнала успешно добавлено."])

        self.tree.verify_tree()


class AddJournalEditorialshipExistingJournal(AddJournalEditorialship):

    def setUp(self):
        super(AddJournalEditorialshipExistingJournal, self).setUp()

        JournalFactory.create_entry(**self.journal.lookup)


@SeleniumTestStepByStep
class AddJournalEditorialshipSelfNewJournal(AddJournalEditorialship):

    def setUp(self):
        super(AddJournalEditorialshipSelfNewJournal, self).setUp()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "journal_str": self.tree["journal"]["name"],
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddJournalEditorialshipSelfNewJournalThroughSimilar(AddJournalEditorialship):

    def setUp(self):
        super(AddJournalEditorialshipSelfNewJournalThroughSimilar, self).setUp()

        JournalFactory.create_entry(name = self.journal["name"].upper())

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Journal] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "journal_str": self.tree["journal"]["name"],
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname
            })

    @loadsessiondata
    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "similar_journals",
            {
                "journal": u" ".join((self.tree["journal"]["name"],
                                      u"(добавить новый журнал)"))
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddJournalEditorialshipSelfExistingJournalThroughSimilar(
        AddJournalEditorialshipExistingJournal):

    def setUp(self):
        super(AddJournalEditorialshipSelfExistingJournalThroughSimilar, self).setUp()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "journal_str": self.tree["journal"]["name"].upper(),
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname
            })

    @loadsessiondata
    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "similar_journals",
            {
                "journal": self.tree["journal"]["name"]
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddJournalEditorialshipSelfExistingJournal(
        AddJournalEditorialshipExistingJournal):

    def setUp(self):
        super(AddJournalEditorialshipSelfExistingJournal, self).setUp()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "journal_str": self.tree["journal"]["name"],
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddJournalEditorialshipExistingWorker(
        AddJournalEditorialshipExistingJournal):

    def setUp(self):
        super(AddJournalEditorialshipExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "journal_str": self.tree["journal"]["name"],
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddJournalEditorialshipExistingWorkerThroughAlias(
        AddJournalEditorialshipExistingJournal):

    def setUp(self):
        super(AddJournalEditorialshipExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "journal_str": self.tree["journal"]["name"],
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddJournalEditorialshipExistingWorkerNewAlias(
        AddJournalEditorialshipExistingJournal):

    def setUp(self):
        super(AddJournalEditorialshipExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.name = self.worker.fullname_short

        self.root["member"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "journal_str": self.tree["journal"]["name"],
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddJournalEditorialshipNewWorker(
        AddJournalEditorialshipExistingJournal):

    def setUp(self):
        super(AddJournalEditorialshipNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.name = get_fullname_raw(
            self.worker_node["lastname"],
            self.worker_node["firstname"],
            self.worker_node["middlename"],
            initials = True)

        self.root["member"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "journal_str": self.tree["journal"]["name"],
                "startdate": self.tree["membership"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["membership"]["enddate"].strftime("%d.%m.%Y"),
                "member_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
