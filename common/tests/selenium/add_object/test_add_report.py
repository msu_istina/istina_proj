# -*- coding: utf-8; -*-
"""This module does verification of report addition procedure.

    - report for self without department is added properly
    - report for self with department is added properly
    - report for existing worker is added properly
    - report for existing worker is added properly through alias
    - report for existing worker is added properly with new alias
    - report for non-existing worker is added properly
"""

from reports.models import Report, ReportAuthorship
from organizations.models import Organization, Department
from workers.models import Worker

from reports.forms import ReportForm
from common.views import LinkedToWorkersModelWizardView as Wizard
Wizard = Wizard.create_wizard_from_form(ReportForm)

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import (
    OrganizationFactory, DepartmentFactory)

import datetime
import random


class AddReport(RunAsUser, WorkerList, SeleniumToolbox,
                SeleniumWizardFormAutomaton):

    def setUp(self):
        super(AddReport, self).setUp()

        self.organization = OrganizationFactory.create_entry(
            name = "An organization")

        self.authorship = Node(
            "authorship", ReportAuthorship,
            {
                "author": None,
                "original_name": "",
                "position": 1
            })

        self.root = Node(
            "report", Report,
            {
                "title": "A title",
                "number": unicode(random.randint(0, 10E6)),
                # this is actually DateField, but we care about year only
                # see issue #583
                "date": datetime.date(datetime.date.today().year, 1, 1),
                "organization": Node(
                    "organization", Organization, self.organization),
                "department": None,
                "pages": random.randint(0, 10E6),
                "abstract": "An abstract",
                "authorships": [self.authorship]
            })

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/reports/add/")

        self.stb_test_istina_page_title(u"Добавление отчета")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(u"Добавление отчета")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить отчет в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{title} - отчет".format(
                title = self.root["title"]))

        self.stb_test_messages_text([u"Отчет успешно добавлен."])

        self.tree.verify_tree()


class AddReportExistingDepartment(AddReport):

    def setUp(self):
        super(AddReportExistingDepartment, self).setUp()

        self.department = DepartmentFactory.create_entry(
            name = "A department", organization = self.organization)

        self.root["department"] = Node(
            "department", Department, self.department)


@SeleniumTestStepByStep
class AddReportSelfNoDepartment(AddReport):

    def setUp(self):
        super(AddReportSelfNoDepartment, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["report"]["title"],
                "number": self.tree["report"]["number"],
                "year": self.tree["report"]["date"].strftime("%Y"),
                "pages": self.tree["report"]["pages"],
                "abstract": self.tree["report"]["abstract"],
                "organization": self.tree["report"]["organization"]["name"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddReportSelfExistingDepartment(AddReportExistingDepartment):

    def setUp(self):
        super(AddReportSelfExistingDepartment, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["report"]["title"],
                "number": self.tree["report"]["number"],
                "year": self.tree["report"]["date"].strftime("%Y"),
                "pages": self.tree["report"]["pages"],
                "abstract": self.tree["report"]["abstract"],
                "organization": self.tree["report"]["organization"]["name"],
                "department": self.tree["report"]["department"]["name"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddReportExistingWorker(AddReportExistingDepartment):

    def setUp(self):
        super(AddReportExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["report"]["title"],
                "number": self.tree["report"]["number"],
                "year": self.tree["report"]["date"].strftime("%Y"),
                "pages": self.tree["report"]["pages"],
                "abstract": self.tree["report"]["abstract"],
                "organization": self.tree["report"]["organization"]["name"],
                "department": self.tree["report"]["department"]["name"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddReportExistingWorkerThroughAlias(AddReportExistingDepartment):

    def setUp(self):
        super(AddReportExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.alias.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["report"]["title"],
                "number": self.tree["report"]["number"],
                "year": self.tree["report"]["date"].strftime("%Y"),
                "pages": self.tree["report"]["pages"],
                "abstract": self.tree["report"]["abstract"],
                "organization": self.tree["report"]["organization"]["name"],
                "department": self.tree["report"]["department"]["name"],
                "authors_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddReportExistingWorkerNewAlias(AddReportExistingDepartment):

    def setUp(self):
        super(AddReportExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker.fullname_short

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["report"]["title"],
                "number": self.tree["report"]["number"],
                "year": self.tree["report"]["date"].strftime("%Y"),
                "pages": self.tree["report"]["pages"],
                "abstract": self.tree["report"]["abstract"],
                "organization": self.tree["report"]["organization"]["name"],
                "department": self.tree["report"]["department"]["name"],
                "authors_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddReportNewWorker(AddReportExistingDepartment):

    def setUp(self):
        super(AddReportNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker_node.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["report"]["title"],
                "number": self.tree["report"]["number"],
                "year": self.tree["report"]["date"].strftime("%Y"),
                "pages": self.tree["report"]["pages"],
                "abstract": self.tree["report"]["abstract"],
                "organization": self.tree["report"]["organization"]["name"],
                "department": self.tree["report"]["department"]["name"],
                "authors_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
