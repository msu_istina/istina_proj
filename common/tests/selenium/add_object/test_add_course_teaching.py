# -*- coding: utf-8; -*-
"""This module does verification of course teaching addition procedure.

    - course teaching for new course without organization for self is added properly
    - course teaching for new course with organization for self is added properly
    - course teaching for existing course for self is added properly
    - course teaching for existing worker is added properly
    - course teaching for existing worker is added properly through alias
    - course teaching for existing worker is added properly with new alias
    - course teaching for non-existing worker is added properly
"""

from courses.models import Course, CourseAuthorship, CourseTeaching
from courses.models import DisciplineType, TeachingKind
from organizations.models import Organization, Department
from workers.models import Worker

from courses.views import CourseTeachingWizardView as Wizard
from workers.utils import get_fullname_raw

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import (
    CourseFactory, DisciplineTypeFactory, TeachingKindFactory)
from common.tests.selenium.seleniumfactories import (
    OrganizationFactory, DepartmentFactory)
from common.tests.selenium.seleniumfactories import DistinctWorkerFactory

from common.tests.selenium.seleniumwizards import connecttosession, loadsessiondata

import datetime
import random


class AddCourseTeaching(RunAsUser, WorkerList, SeleniumToolbox,
                        SeleniumWizardFormAutomaton):

    def setUp(self):
        super(AddCourseTeaching, self).setUp()

        self.name = None

        self.msu = Organization.objects.get_msu()

        self.department = DepartmentFactory.create_entry(
            name = "A department", organization = self.msu)

        self.discipline_type = DisciplineTypeFactory.create_entry(
            name = "Discipline type")

        self.teaching_kind = TeachingKindFactory.create_entry(
            name = "Teaching kind")

        self.course = Node(
            "course", Course,
            {
                "title": "A title",
                "year": datetime.date.today().year,
                "organization": None,
                "description": "A description"
            })

        self.root = Node(
            "teaching", CourseTeaching,
            {
                "teacher": None,
                "course": self.course,
                "department": Node(
                    "department", Department, self.department),
                "discipline_type": Node(
                    "discipline_type", DisciplineType, self.discipline_type),
                "teaching_kind": Node(
                    "teaching_kind", TeachingKind, self.teaching_kind),
                "startdate": datetime.date.today(),
                "enddate": datetime.date.today(),
                "hours": random.randint(0, 10E6)
            })

        self.wizard = self.init_wizard(Wizard)

    @connecttosession
    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/courses/teachings/add/")

        self.stb_test_istina_page_title(u"Добавление преподавания курса")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(u"Добавление преподавания курса")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить преподавание учебного курса в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{name}, преподаватель курса {course}".format(
                name = self.name, course = self.course["title"]))

        self.tree.verify_tree()


class AddCourseTeachingNewCourse(AddCourseTeaching):

    def setUp(self):
        super(AddCourseTeachingNewCourse, self).setUp()

        self.authorship = Node(
            "authorship", CourseAuthorship,
            {
                "author": None
            })

        self.course["authorships"] = [self.authorship]

    def stepbystep_post_finalize(self):
        self.stb_test_messages_text(
            [
                u"Учебный курс успешно добавлен.",
                u"Преподавание учебного курса успешно добавлено."
            ]
        )


class AddCourseTeachingNewCourseExistingOrganization(AddCourseTeachingNewCourse):

    def setUp(self):
        super(AddCourseTeachingNewCourseExistingOrganization, self).setUp()

        self.organization = OrganizationFactory.create_entry(
            name = "An organization")

        self.course["organization"] = Node(
            "organization", Organization, self.organization)


class AddCourseTeachingExistingCourse(AddCourseTeaching):

    def setUp(self):
        super(AddCourseTeachingExistingCourse, self).setUp()

        CourseFactory.create_entry(**self.course.lookup)

    def stepbystep_post_finalize(self):
        self.stb_test_messages_text(
            [u"Преподавание учебного курса успешно добавлено."])


@SeleniumTestStepByStep
class AddCourseTeachingSelfNewCourseNoOrganization(AddCourseTeachingNewCourse):

    def setUp(self):
        super(AddCourseTeachingSelfNewCourseNoOrganization, self).setUp()

        self.name = self.worker.fullname_short

        self.author = self.prepare_existing_worker(DistinctWorkerFactory)

        self.authorship["author"] = Node("author", Worker, self.author)

        self.root["teacher"] = Node("teacher", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "course_str": self.tree["course"]["title"],
                "department": self.tree["department"]["name"],
                "discipline_type": self.tree["discipline_type"]["name"],
                "teaching_kind": self.tree["teaching_kind"]["name"],
                "startdate": self.tree["teaching"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["teaching"]["enddate"].strftime("%d.%m.%Y"),
                "hours": self.tree["teaching"]["hours"],
                "teacher_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_course",
            {
                "year": self.tree["course"]["year"],
                "organization": "---------",
                "description": self.tree["course"]["description"],
                "authors_str": self.author.fullname
            }, initial_data = {
                "title": self.tree["course"]["title"]
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.author.fullname)


@SeleniumTestStepByStep
class AddCourseTeachingSelfNewCourseExistingOrganization(
        AddCourseTeachingNewCourseExistingOrganization):

    def setUp(self):
        super(AddCourseTeachingSelfNewCourseExistingOrganization, self).setUp()

        self.name = self.worker.fullname_short

        self.author = self.prepare_existing_worker(DistinctWorkerFactory)

        self.authorship["author"] = Node("author", Worker, self.author)

        self.root["teacher"] = Node("teacher", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "course_str": self.tree["course"]["title"],
                "department": self.tree["department"]["name"],
                "discipline_type": self.tree["discipline_type"]["name"],
                "teaching_kind": self.tree["teaching_kind"]["name"],
                "startdate": self.tree["teaching"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["teaching"]["enddate"].strftime("%d.%m.%Y"),
                "hours": self.tree["teaching"]["hours"],
                "teacher_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_course",
            {
                "year": self.tree["course"]["year"],
                "organization": self.tree["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.author.fullname
            }, initial_data = {
                "title": self.tree["course"]["title"]
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.author.fullname)


@SeleniumTestStepByStep
class AddCourseTeachingSelfNewCourseThroughSimilar(
        AddCourseTeachingNewCourseExistingOrganization):

    def setUp(self):
        super(AddCourseTeachingSelfNewCourseThroughSimilar, self).setUp()

        CourseFactory.create_entry(
            title = self.course["title"].upper(),
            year = self.course["year"])

        self.name = self.worker.fullname_short

        self.author = self.prepare_existing_worker(DistinctWorkerFactory)

        self.authorship["author"] = Node("author", Worker, self.author)

        self.root["teacher"] = Node("teacher", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Course] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "course_str": self.tree["course"]["title"],
                "department": self.tree["department"]["name"],
                "discipline_type": self.tree["discipline_type"]["name"],
                "teaching_kind": self.tree["teaching_kind"]["name"],
                "startdate": self.tree["teaching"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["teaching"]["enddate"].strftime("%d.%m.%Y"),
                "hours": self.tree["teaching"]["hours"],
                "teacher_str": self.worker.fullname
            })

    @loadsessiondata
    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "similar_courses",
            {
                "course": u" ".join((self.tree["course"]["title"],
                                     u"(добавить новый курс)"))
            })

    def stepbystep_step3(self):
        self.process_wizard_form(
            self.wizard, "new_course",
            {
                "year": self.tree["course"]["year"],
                "organization": self.tree["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.author.fullname
            }, initial_data = {
                "title": self.tree["course"]["title"]
            })

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_step5(self):
        self.workerlist_test_single_worker_is_unique(self.author.fullname)


@SeleniumTestStepByStep
class AddCourseTeachingSelfExistingCourseThroughSimilar(
        AddCourseTeachingExistingCourse):

    def setUp(self):
        super(AddCourseTeachingSelfExistingCourseThroughSimilar, self).setUp()

        self.name = self.worker.fullname_short

        self.root["teacher"] = Node("teacher", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "course_str": self.tree["course"]["title"].upper(),
                "department": self.tree["department"]["name"],
                "discipline_type": self.tree["discipline_type"]["name"],
                "teaching_kind": self.tree["teaching_kind"]["name"],
                "startdate": self.tree["teaching"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["teaching"]["enddate"].strftime("%d.%m.%Y"),
                "hours": self.tree["teaching"]["hours"],
                "teacher_str": self.worker.fullname
            })

    @loadsessiondata
    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "similar_courses",
            {
                "course": u"{title} ({year})".format(
                    title = self.tree["course"]["title"],
                    year = str(self.tree["course"]["year"]))
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCourseTeachingSelfExistingCourse(
        AddCourseTeachingExistingCourse):

    def setUp(self):
        super(AddCourseTeachingSelfExistingCourse, self).setUp()

        self.name = self.worker.fullname_short

        self.root["teacher"] = Node("teacher", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "course_str": self.tree["course"]["title"],
                "department": self.tree["department"]["name"],
                "discipline_type": self.tree["discipline_type"]["name"],
                "teaching_kind": self.tree["teaching_kind"]["name"],
                "startdate": self.tree["teaching"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["teaching"]["enddate"].strftime("%d.%m.%Y"),
                "hours": self.tree["teaching"]["hours"],
                "teacher_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCourseTeachingExistingWorker(
        AddCourseTeachingNewCourseExistingOrganization):

    def setUp(self):
        super(AddCourseTeachingExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker(DistinctWorkerFactory)
        self.author = self.prepare_existing_worker(DistinctWorkerFactory)

        self.name = self.worker.fullname_short

        self.authorship["author"] = Node("author", Worker, self.author)

        self.root["teacher"] = Node("teacher", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "course_str": self.tree["course"]["title"],
                "department": self.tree["department"]["name"],
                "discipline_type": self.tree["discipline_type"]["name"],
                "teaching_kind": self.tree["teaching_kind"]["name"],
                "startdate": self.tree["teaching"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["teaching"]["enddate"].strftime("%d.%m.%Y"),
                "hours": self.tree["teaching"]["hours"],
                "teacher_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_course",
            {
                "year": self.tree["course"]["year"],
                "organization": self.tree["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.author.fullname
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.author.fullname)


@SeleniumTestStepByStep
class AddCourseTeachingExistingWorkerThroughAlias(
        AddCourseTeachingNewCourseExistingOrganization):

    def setUp(self):
        super(AddCourseTeachingExistingWorkerThroughAlias, self).setUp()

        self.worker, self.worker_alias = self.prepare_existing_worker_through_alias(
            factory = DistinctWorkerFactory)
        self.author, self.author_alias = self.prepare_existing_worker_through_alias(
            factory = DistinctWorkerFactory)

        self.name = self.worker.fullname_short

        self.authorship["author"] = Node("author", Worker, self.author)

        self.root["teacher"] = Node("teacher", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "course_str": self.tree["course"]["title"],
                "department": self.tree["department"]["name"],
                "discipline_type": self.tree["discipline_type"]["name"],
                "teaching_kind": self.tree["teaching_kind"]["name"],
                "startdate": self.tree["teaching"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["teaching"]["enddate"].strftime("%d.%m.%Y"),
                "hours": self.tree["teaching"]["hours"],
                "teacher_str": self.worker_alias.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_course",
            {
                "year": self.tree["course"]["year"],
                "organization": self.tree["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.author_alias.fullname
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker_alias.fullname)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.author_alias.fullname)


@SeleniumTestStepByStep
class AddCourseTeachingExistingWorkerNewAlias(
        AddCourseTeachingNewCourseExistingOrganization):

    def setUp(self):
        super(AddCourseTeachingExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias(
            name = "teacher", factory = DistinctWorkerFactory)
        self.author, self.author_node = self.prepare_existing_worker_new_alias(
            name = "author", factory = DistinctWorkerFactory)

        self.name = self.worker.fullname_short

        self.authorship["author"] = self.author_node

        self.root["teacher"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "course_str": self.tree["course"]["title"],
                "department": self.tree["department"]["name"],
                "discipline_type": self.tree["discipline_type"]["name"],
                "teaching_kind": self.tree["teaching_kind"]["name"],
                "startdate": self.tree["teaching"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["teaching"]["enddate"].strftime("%d.%m.%Y"),
                "hours": self.tree["teaching"]["hours"],
                "teacher_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_course",
            {
                "year": self.tree["course"]["year"],
                "organization": self.tree["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.author.fullname_short
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(
            self.author.fullname, aliased_name = self.author.fullname_short)


@SeleniumTestStepByStep
class AddCourseTeachingNewWorker(AddCourseTeachingNewCourseExistingOrganization):

    def setUp(self):
        super(AddCourseTeachingNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker("teacher")
        self.author_node = self.prepare_new_worker(
            "author",
            {
                "firstname": u"Имя",
                "middlename": u"Отчество",
                "lastname": u"Фамилия"
            })

        self.name = get_fullname_raw(
            self.worker_node["lastname"],
            self.worker_node["firstname"],
            self.worker_node["middlename"],
            initials = True)

        self.authorship["author"] = self.author_node

        self.root["teacher"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "course_str": self.tree["course"]["title"],
                "department": self.tree["department"]["name"],
                "discipline_type": self.tree["discipline_type"]["name"],
                "teaching_kind": self.tree["teaching_kind"]["name"],
                "startdate": self.tree["teaching"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["teaching"]["enddate"].strftime("%d.%m.%Y"),
                "hours": self.tree["teaching"]["hours"],
                "teacher_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_course",
            {
                "year": self.tree["course"]["year"],
                "organization": self.tree["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.author_node.fullname
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_new(self.author_node.fullname)
