# -*- coding: utf-8; -*-
"""A helper class for publication addition testing"""

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumtestsuite import SeleniumRadioButton
from common.tests.selenium.seleniumtestsuite import TestMultipleInputFormByCSS
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser


class AddPublication(RunAsUser, WorkerList, SeleniumToolbox,
                     TestMultipleInputFormByCSS, SeleniumRadioButton,
                     SeleniumWizardFormAutomaton):

    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/publications/add/")

        self.stb_test_istina_page_title(u"Добавление публикации - шаг 1")

    def stepbystep_next(self):
        # publication addition procedure is 'unique' in the following way:
        # it stores all forms from previous steps under display:none property
        # therefore simply submitting form with the proper title is not enough,
        # as it will submit nondisplayed form instead
        # here is a crutch to workaround this
        submit, = (elem for elem in self.driver.find_elements_by_css_selector(
            u'input[type="submit"][title="Перейти к следующему шагу добавления публикации"]')
            if elem.is_displayed())

        submit.submit()

    def stepbystep_pre_step2(self):
        self.stb_test_istina_page_title(u"Добавление публикации - шаг 2")

    def stepbystep_pre_step3(self):
        self.stb_test_istina_page_title(u"Добавление публикации - шаг 3")
