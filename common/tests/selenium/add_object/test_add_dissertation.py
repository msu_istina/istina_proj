# -*- coding: utf-8; -*-
"""This module does verification of dissertation addition procedure.

    - dissertation for new council for non-existing organization for self is added properly
    - dissertation for new council for existing organization for self is added properly
    - dissertation for existing council for self is added properly
    - non-advised non-phd dissertation for self is added properly
    - advised non-phd dissertation for self is added properly
    - advised phd dissertation for self is added properly
    - dissertation without specialty for self is added properly
    - dissertation with specialty for self is added properly
    - dissertation with different statuses for self is added properly
    - dissertation for non-existing leading organization for self is added properly
    - dissertation for existing leading organization for self is added properly
    - dissertation for non-existing organization where done for self is added properly
    - dissertation for existing organization where done for self is added properly
    - reviewed dissertation for self is added properly
    - dissertation for existing worker is added properly
    - dissertation for existing worker is added properly through alias
    - dissertation for existing worker is added properly with new alias
    - dissertation for non-existing worker is added properly
"""

from dissertations.models import Dissertation
from dissertations.models import DissertationAdvisement, DissertationReviewship
from dissertations.models import DissertationReviewshipCategory
from dissertations.models import Specialty, Domain
from dissertation_councils.models import DissertationCouncil
from organizations.models import Organization
from workers.models import Worker

from dissertations.views import DissertationWizardView as Wizard

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import (
    DissertationReviewshipCategoryFactory, SpecialtyFactory, DomainFactory)
from common.tests.selenium.seleniumfactories import DissertationCouncilFactory
from common.tests.selenium.seleniumfactories import OrganizationFactory
from common.tests.selenium.seleniumfactories import DistinctWorkerFactory

import datetime
import random


class AddDissertation(RunAsUser, WorkerList, SeleniumToolbox,
                      SeleniumWizardFormAutomaton):

    @staticmethod
    def phd_to_str(phd):
        if phd == "Y":
            return u"Кандидатская"
        elif phd == "N":
            return u"Докторская"
        else:
            raise ValueError

    @staticmethod
    def status_to_str(status):
        if status == "defended":
            return u"Защищена"
        elif status == "to_defend":
            return u"Принята к защите"
        elif status == "to_consider":
            return u"Принята к рассмотрению"
        else:
            raise ValueError

    def setUp(self):
        super(AddDissertation, self).setUp()

        self.domain = DomainFactory.create_entry(
            name = "A science domain")

        self.council = Node(
            "council", DissertationCouncil,
            {
                "number": unicode(random.randint(0, 10E6)),
                "organization": None,
                "organization_str": "",
                "address": "",
                "place": "",
                "phone": "",
                "email": "",
                "specialties": []
            })

        self.root = Node(
            "dissertation", Dissertation,
            {
                "title": "A title",
                "author": None,
                "author_original_name": "",
                "council": self.council,
                "domain": Node("domain", Domain, self.domain),
                "specialty": None,
                "specialty_text": "",
                "phd": "",
                "date": datetime.date.today(),
                "time": datetime.time(23, 14),
                "leading_organization_str": "",
                "leading_organization": None,
                "organization_where_done_str": "",
                "organization_where_done": None,
                "status": "defended",
                "abstract": "An abstract"
            })

        self.root["year"] = self.root["date"].year

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.tree.instance_index[DissertationAdvisement]
        self.tree.instance_index[DissertationReviewship]
        self.tree.instance_index[DissertationReviewshipCategory]

        self.driver.get(self.base_url + "/dissertations/add/")

        self.stb_test_istina_page_title(u"Добавление диссертации")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(u"Добавление диссертации")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить диссертацию в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{title} - диссертация".format(
                title = self.root["title"]))

        self.stb_test_messages_text([u"Диссертация успешно добавлена."])

        self.tree.verify_tree()


class AddDissertationExistingCouncil(AddDissertation):

    def setUp(self):
        super(AddDissertationExistingCouncil, self).setUp()

        self.organization = OrganizationFactory.create_entry()

        DissertationCouncilFactory.create_entry(
            number = self.council["number"],
            organization = self.organization)

        self.council["organization"] = Node(
            "organization", Organization, self.organization)


class AddDissertationExistingSpecialty(AddDissertation):

    def setUp(self):
        super(AddDissertationExistingSpecialty, self).setUp()

        self.specialty = SpecialtyFactory.create_entry(
            name = "A specialty")

        self.root["specialty"] = Node("specialty", Specialty, self.specialty)


class AddDissertationExistingOpponentCategory(AddDissertation):

    def setUp(self):
        super(AddDissertationExistingOpponentCategory, self).setUp()

        DissertationReviewshipCategoryFactory.create_entry(
            name = u"Оппонент")


class AddDissertationAdvised(AddDissertation):

    def setUp(self):
        super(AddDissertationAdvised, self).setUp()

        self.advisement = Node(
            "advisement", DissertationAdvisement,
            {
                "adviser": None,
                "original_name": ""
            })

        self.root["advisements"] = [self.advisement]


class AddDissertationReviewed(AddDissertation):

    def setUp(self):
        super(AddDissertationReviewed, self).setUp()

        self.category = Node(
            "opponent category", DissertationReviewshipCategory,
            {
                "name": u"Оппонент"
            })

        self.opponentship = Node(
            "opponentship", DissertationReviewship,
            {
                "reviewer": None,
                "original_name": "",
                "position": 1,
                "category": self.category
            })

        # reviewERships? really?
        self.root["reviewerships"] = [self.opponentship]


class AddDissertationExistingLeadingWhereDoneOrganizations(AddDissertation):

    def setUp(self):
        super(AddDissertationExistingLeadingWhereDoneOrganizations, self).setUp()

        self.root["leading_organization"] = Node(
            "leading_organization", Organization,
            OrganizationFactory.create_entry()
        )

        self.root["organization_where_done"] = Node(
            "organization_where_done", Organization,
            OrganizationFactory.create_entry()
        )


@SeleniumTestStepByStep
class AddDissertationSelfNewCouncilOrganizationStr(AddDissertation):

    def setUp(self):
        super(AddDissertationSelfNewCouncilOrganizationStr, self).setUp()

        self.council["organization_str"] = "An organization"

        self.root["phd"] = "N"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "council_number": self.tree["council"]["number"],
                "abstract": self.tree["dissertation"]["abstract"],
                "author_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "organization",
            {
                "organization_str": self.council["organization_str"]
            })

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfNewCouncilExistingOrganization(AddDissertation):

    def setUp(self):
        super(AddDissertationSelfNewCouncilExistingOrganization, self).setUp()

        self.organization = OrganizationFactory.create_entry()

        self.council["organization"] = Node(
            "organization", Organization, self.organization)

        self.root["phd"] = "N"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "council_number": self.tree["council"]["number"],
                "abstract": self.tree["dissertation"]["abstract"],
                "author_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "organization",
            {
                "organization_str": self.council["organization"]["name"]
            })

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfExistingCouncil(AddDissertationExistingCouncil):

    def setUp(self):
        super(AddDissertationSelfExistingCouncil, self).setUp()

        self.root["phd"] = "N"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "council_number": self.tree["council"]["number"],
                "abstract": self.tree["dissertation"]["abstract"],
                "author_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfNoPhdAdvised(
        AddDissertationExistingCouncil, AddDissertationAdvised):

    def setUp(self):
        super(AddDissertationSelfNoPhdAdvised, self).setUp()

        self.adviser = self.prepare_existing_worker(DistinctWorkerFactory)

        self.root["phd"] = "N"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "council_number": self.tree["council"]["number"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname,
                "author_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfPhdAdvised(
        AddDissertationExistingCouncil, AddDissertationAdvised):

    def setUp(self):
        super(AddDissertationSelfPhdAdvised, self).setUp()

        self.adviser = self.prepare_existing_worker(DistinctWorkerFactory)

        self.root["phd"] = "Y"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "council_number": self.tree["council"]["number"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname,
                "author_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfExistingSpecialty(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationAdvised):

    def setUp(self):
        super(AddDissertationSelfExistingSpecialty, self).setUp()

        self.adviser = self.prepare_existing_worker(DistinctWorkerFactory)

        self.root["phd"] = "Y"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname,
                "author_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfToDefend(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationAdvised):

    def setUp(self):
        super(AddDissertationSelfToDefend, self).setUp()

        self.adviser = self.prepare_existing_worker(DistinctWorkerFactory)

        self.root["phd"] = "Y"
        self.root["status"] = "to_defend"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "status": self.status_to_str(self.tree["dissertation"]["status"]),
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname,
                "author_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfToConsider(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationAdvised):

    def setUp(self):
        super(AddDissertationSelfToConsider, self).setUp()

        self.adviser = self.prepare_existing_worker(DistinctWorkerFactory)

        self.root["phd"] = "Y"
        self.root["status"] = "to_consider"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "status": self.status_to_str(self.tree["dissertation"]["status"]),
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname,
                "author_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfLeadingWhereDoneOrganizationsStr(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationAdvised):

    def setUp(self):
        super(AddDissertationSelfLeadingWhereDoneOrganizationsStr, self).setUp()

        self.adviser = self.prepare_existing_worker(DistinctWorkerFactory)

        self.root["phd"] = "Y"

        self.root["leading_organization_str"] = "Leading organization"
        self.root["organization_where_done_str"] = "Organization Where Done"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "leading_organization_str": self.tree["dissertation"]["leading_organization_str"],
                "organization_where_done_str": self.tree["dissertation"]["organization_where_done_str"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname,
                "author_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfLeadingWhereDoneExistingOrganizations(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationExistingLeadingWhereDoneOrganizations,
        AddDissertationAdvised):

    def setUp(self):
        super(AddDissertationSelfLeadingWhereDoneExistingOrganizations, self).setUp()

        self.adviser = self.prepare_existing_worker(DistinctWorkerFactory)

        self.root["phd"] = "Y"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "leading_organization_str": self.tree["dissertation"]["leading_organization"]["name"],
                "organization_where_done_str": self.tree["dissertation"]["organization_where_done"]["name"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname,
                "author_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfReviewedNoOpponentCategory(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationExistingLeadingWhereDoneOrganizations,
        AddDissertationAdvised, AddDissertationReviewed):

    def setUp(self):
        super(AddDissertationSelfReviewedNoOpponentCategory, self).setUp()

        self.adviser = self.prepare_existing_worker(DistinctWorkerFactory)
        self.reviewer = self.prepare_existing_worker(DistinctWorkerFactory)

        self.root["phd"] = "Y"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser.fullname

        self.opponentship["reviewer"] = Node("reviewer", Worker, self.reviewer)
        self.opponentship["original_name"] = self.reviewer.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "leading_organization_str": self.tree["dissertation"]["leading_organization"]["name"],
                "organization_where_done_str": self.tree["dissertation"]["organization_where_done"]["name"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname,
                "author_str": self.worker.fullname,
                "opponents_str": self.reviewer.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.reviewer.fullname)


@SeleniumTestStepByStep
class AddDissertationSelfReviewedExistingOpponentCategory(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationExistingLeadingWhereDoneOrganizations,
        AddDissertationExistingOpponentCategory,
        AddDissertationAdvised, AddDissertationReviewed):

    def setUp(self):
        super(AddDissertationSelfReviewedExistingOpponentCategory, self).setUp()

        self.adviser = self.prepare_existing_worker(DistinctWorkerFactory)
        self.reviewer = self.prepare_existing_worker(DistinctWorkerFactory)

        self.root["phd"] = "Y"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser.fullname

        self.opponentship["reviewer"] = Node("reviewer", Worker, self.reviewer)
        self.opponentship["original_name"] = self.reviewer.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "leading_organization_str": self.tree["dissertation"]["leading_organization"]["name"],
                "organization_where_done_str": self.tree["dissertation"]["organization_where_done"]["name"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname,
                "author_str": self.worker.fullname,
                "opponents_str": self.reviewer.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.reviewer.fullname)


@SeleniumTestStepByStep
class AddDissertationExistingWorker(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationExistingLeadingWhereDoneOrganizations,
        AddDissertationExistingOpponentCategory,
        AddDissertationAdvised, AddDissertationReviewed):

    def setUp(self):
        super(AddDissertationExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker(DistinctWorkerFactory)
        self.adviser = self.prepare_existing_worker(DistinctWorkerFactory)
        self.reviewer = self.prepare_existing_worker(DistinctWorkerFactory)

        self.root["phd"] = "Y"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser.fullname

        self.opponentship["reviewer"] = Node("reviewer", Worker, self.reviewer)
        self.opponentship["original_name"] = self.reviewer.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "leading_organization_str": self.tree["dissertation"]["leading_organization"]["name"],
                "organization_where_done_str": self.tree["dissertation"]["organization_where_done"]["name"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname,
                "author_str": self.worker.fullname,
                "opponents_str": self.reviewer.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.reviewer.fullname)


@SeleniumTestStepByStep
class AddDissertationExistingWorkerThroughAlias(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationExistingLeadingWhereDoneOrganizations,
        AddDissertationExistingOpponentCategory,
        AddDissertationAdvised, AddDissertationReviewed):

    def setUp(self):
        super(AddDissertationExistingWorkerThroughAlias, self).setUp()

        self.worker, self.worker_alias = self.prepare_existing_worker_through_alias(
            DistinctWorkerFactory)
        self.adviser, self.adviser_alias = self.prepare_existing_worker_through_alias(
            DistinctWorkerFactory)
        self.reviewer, self.reviewer_alias = self.prepare_existing_worker_through_alias(
            DistinctWorkerFactory)

        self.root["phd"] = "Y"

        self.root["author"] = Node("author", Worker, self.worker)
        self.root["author_original_name"] = self.worker_alias.fullname

        self.advisement["adviser"] = Node("adviser", Worker, self.adviser)
        self.advisement["original_name"] = self.adviser_alias.fullname

        self.opponentship["reviewer"] = Node("reviewer", Worker, self.reviewer)
        self.opponentship["original_name"] = self.reviewer_alias.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "leading_organization_str": self.tree["dissertation"]["leading_organization"]["name"],
                "organization_where_done_str": self.tree["dissertation"]["organization_where_done"]["name"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser_alias.fullname,
                "author_str": self.worker_alias.fullname,
                "opponents_str": self.reviewer_alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.adviser_alias.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker_alias.fullname)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.reviewer_alias.fullname)


@SeleniumTestStepByStep
class AddDissertationExistingWorkerNewAlias(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationExistingLeadingWhereDoneOrganizations,
        AddDissertationExistingOpponentCategory,
        AddDissertationAdvised, AddDissertationReviewed):

    def setUp(self):
        super(AddDissertationExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias(
            name = "worker", factory = DistinctWorkerFactory)
        self.adviser, self.adviser_node = self.prepare_existing_worker_new_alias(
            name = "adviser", factory = DistinctWorkerFactory)
        self.reviewer, self.reviewer_node = self.prepare_existing_worker_new_alias(
            name = "reviewer", factory = DistinctWorkerFactory)

        self.root["phd"] = "Y"

        self.root["author"] = self.worker_node
        self.root["author_original_name"] = self.worker.fullname_short

        self.advisement["adviser"] = self.adviser_node
        self.advisement["original_name"] = self.adviser.fullname_short

        self.opponentship["reviewer"] = self.reviewer_node
        self.opponentship["original_name"] = self.reviewer.fullname_short

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "leading_organization_str": self.tree["dissertation"]["leading_organization"]["name"],
                "organization_where_done_str": self.tree["dissertation"]["organization_where_done"]["name"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser.fullname_short,
                "author_str": self.worker.fullname_short,
                "opponents_str": self.reviewer.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.adviser.fullname, aliased_name = self.adviser.fullname_short)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(
            self.reviewer.fullname, aliased_name = self.reviewer.fullname_short)


@SeleniumTestStepByStep
class AddDissertationNewWorker(
        AddDissertationExistingCouncil, AddDissertationExistingSpecialty,
        AddDissertationExistingLeadingWhereDoneOrganizations,
        AddDissertationExistingOpponentCategory,
        AddDissertationAdvised, AddDissertationReviewed):

    def setUp(self):
        super(AddDissertationNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker("author")
        self.adviser_node = self.prepare_new_worker(
            "adviser",
            {
                "firstname": u"Имя",
                "middlename": u"Отчество",
                "lastname": u"Фамилия"
            })
        self.reviewer_node = self.prepare_new_worker(
            "reviewer",
            {
                "firstname": u"Николай",
                "middlename": u"Ильич",
                "lastname": u"Тумайкин"
            })

        self.root["phd"] = "Y"

        self.root["author"] = self.worker_node
        self.root["author_original_name"] = self.worker_node.fullname

        self.advisement["adviser"] = self.adviser_node
        self.advisement["original_name"] = self.adviser_node.fullname

        self.opponentship["reviewer"] = self.reviewer_node
        self.opponentship["original_name"] = self.reviewer_node.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["dissertation"]["title"],
                "date": self.tree["dissertation"]["date"].strftime("%d.%m.%Y"),
                "time": self.tree["dissertation"]["time"].strftime("%H:%M"),
                "phd": self.phd_to_str(self.tree["dissertation"]["phd"]),
                "domain": self.tree["dissertation"]["domain"]["name"],
                "specialty": " - ".join(
                    (unicode(self.tree["dissertation"]["specialty"]["number"]),
                     self.tree["dissertation"]["specialty"]["name"])),
                "council_number": self.tree["council"]["number"],
                "leading_organization_str": self.tree["dissertation"]["leading_organization"]["name"],
                "organization_where_done_str": self.tree["dissertation"]["organization_where_done"]["name"],
                "abstract": self.tree["dissertation"]["abstract"],
                "advisers_str": self.adviser_node.fullname,
                "author_str": self.worker_node.fullname,
                "opponents_str": self.reviewer_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.adviser_node.fullname)

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_new(self.reviewer_node.fullname)
