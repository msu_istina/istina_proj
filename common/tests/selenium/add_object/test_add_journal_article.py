# -*- coding: utf-8; -*-
"""This module does verification of journal article addition procedure.

    - article for new journal is added properly
    - article for existing journal is added properly
    - article for existing worker is added properly
    - article for existing worker is added properly through alias
    - article for existing worker is added properly with new alias
    - article for non-existing worker is added properly
"""

from publications.models import Article, ArticleAuthorship
from journals.models import Journal
from workers.models import Worker

from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import JournalFactory
from add_publication import AddPublication

import datetime
import random


class AddJournalArticle(AddPublication):

    def setUp(self):
        super(AddJournalArticle, self).setUp()

        self.authorship = Node(
            "authorship", ArticleAuthorship,
            {
                "author": None,
                "original_name": "",
                "position": 1
            })

        self.journal = Node(
            "journal", Journal,
            {
                "name": "A journal",
                "publisher": None,
                "url": None,
                "language": None,
                "abstract": ""
            })

        self.root = Node(
            "article", Article,
            {
                "title": "An article",
                "journal": self.journal,
                "volume": random.randint(0, 10E6),
                "number": unicode(random.randint(0, 10E6)),
                "year": datetime.date.today().year,
                "collection": None,
                "firstpage": unicode(random.randint(0, 10E6)),
                "lastpage": unicode(random.randint(0, 10E6)),
                # meaningful abstract and doi are added to node later
                # as they can be added only during edit step
                "abstract": None,
                "doi": None,
                "language": None,
                "authorships": [self.authorship]
            })

    def stepbystep_step1(self):
        pass

    def stepbystep_pre_step2(self):
        super(AddJournalArticle, self).stepbystep_pre_step2()
        self.radiobutton_check_by_id("work_type", "in_journal")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить публикацию и '
            u'перейти к следующему шагу добавления публикации"]').submit()

        self.stb_test_istina_page_title(
            u"Добавление дополнительной информации")

        self.stb_test_messages_text(
            [u"Статья {title} успешно добавлена.".format(
                title = self.root["title"])])

        self.tree.verify_tree()

        self.tree["article"]["abstract"] = "An abstract"
        self.tree["article"]["doi"] = "12:3456/i.am.an.example.d0i.000.111"

        self.css_set_single_element(
            "form#additional_form textarea#id_abstract",
            self.tree["article"]["abstract"])

        self.css_set_single_element(
            "form#additional_form input#id_doi",
            self.tree["article"]["doi"])

        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить информацию в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{title} - статья".format(title = self.root["title"]))

        self.stb_test_messages_text([u"Статья успешно отредактирована."])

        self.tree.verify_tree()


class AddJournalArticleExistingJournal(AddJournalArticle):

    def setUp(self):
        super(AddJournalArticleExistingJournal, self).setUp()

        JournalFactory.create_entry(**self.journal.lookup)


@SeleniumTestStepByStep
class AddJournalArticleSelfNewJournal(AddJournalArticle):

    def setUp(self):
        super(AddJournalArticleSelfNewJournal, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "journal": self.tree["journal"]["name"],
                "volume": self.tree["article"]["volume"],
                "number": self.tree["article"]["number"],
                "year": self.tree["article"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddJournalArticleSelfExistingJournal(AddJournalArticleExistingJournal):

    def setUp(self):
        super(AddJournalArticleSelfExistingJournal, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "journal": self.tree["journal"]["name"],
                "volume": self.tree["article"]["volume"],
                "number": self.tree["article"]["number"],
                "year": self.tree["article"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddJournalArticleExistingWorker(AddJournalArticleExistingJournal):

    def setUp(self):
        super(AddJournalArticleExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "journal": self.tree["journal"]["name"],
                "volume": self.tree["article"]["volume"],
                "number": self.tree["article"]["number"],
                "year": self.tree["article"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddJournalArticleExistingWorkerThroughAlias(AddJournalArticleExistingJournal):

    def setUp(self):
        super(AddJournalArticleExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.alias.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "journal": self.tree["journal"]["name"],
                "volume": self.tree["article"]["volume"],
                "number": self.tree["article"]["number"],
                "year": self.tree["article"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.alias.fullname
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(
            self.alias.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddJournalArticleExistingWorkerNewAlias(AddJournalArticleExistingJournal):

    def setUp(self):
        super(AddJournalArticleExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker.fullname_short

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "journal": self.tree["journal"]["name"],
                "volume": self.tree["article"]["volume"],
                "number": self.tree["article"]["number"],
                "year": self.tree["article"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname_short
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short,
            has_unknown = True)


@SeleniumTestStepByStep
class AddJournalArticleNewWorker(AddJournalArticleExistingJournal):

    def setUp(self):
        super(AddJournalArticleNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker_node.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "journal": self.tree["journal"]["name"],
                "volume": self.tree["article"]["volume"],
                "number": self.tree["article"]["number"],
                "year": self.tree["article"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker_node.fullname
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
