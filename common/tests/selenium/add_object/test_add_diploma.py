# -*- coding: utf-8; -*-
"""This module does verification of diploma advisement addition procedure.

    - diploma advisement without organization for self is added properly
    - diploma advisement with organization for self is added properly
    - diploma advisement for existing worker is added properly
    - diploma advisement for existing worker is added properly through alias
    - diploma advisement for existing worker is added properly with new alias
    - diploma advisement for non-existing worker is added properly
"""

from diplomas.models import Diploma, DiplomaAdvisement
from organizations.models import Organization
from workers.models import Worker

from diplomas.forms import DiplomaForm
from common.views import LinkedToWorkersModelWizardView as Wizard
Wizard = Wizard.create_wizard_from_form(DiplomaForm)

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import OrganizationFactory

import datetime


class AddDiplomaAdvisement(RunAsUser, WorkerList, SeleniumToolbox,
                           SeleniumWizardFormAutomaton):

    def setUp(self):
        super(AddDiplomaAdvisement, self).setUp()

        self.advisement = Node(
            "advisement", DiplomaAdvisement,
            {
                "adviser": None
            })

        self.root = Node(
            "diploma", Diploma,
            {
                "title": "A title",
                "author": "Firstname Middlename Lastname",
                "year": datetime.date.today().year,
                "organization": None,
                "abstract": "An abstract",
                "advisements": [self.advisement]
            })

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/diplomas/add/")

        self.stb_test_istina_page_title(u"Добавление дипломной работы")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(u"Добавление дипломной работы")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить диплом в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{title} - дипломная работа".format(
                title = self.root["title"]))

        self.stb_test_messages_text([u"Дипломная работа успешно добавлена."])

        self.tree.verify_tree()


class AddDiplomaAdvisementExistingOrganization(AddDiplomaAdvisement):

    def setUp(self):
        super(AddDiplomaAdvisementExistingOrganization, self).setUp()

        self.organization = OrganizationFactory.create_entry(
            name = "An organization")

        self.root["organization"] = Node(
            "organization", Organization, self.organization)


@SeleniumTestStepByStep
class AddDiplomaAdvisementSelfNoOrganization(AddDiplomaAdvisement):

    def setUp(self):
        super(AddDiplomaAdvisementSelfNoOrganization, self).setUp()

        self.advisement["adviser"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["diploma"]["title"],
                "author": self.tree["diploma"]["author"],
                "year": self.tree["diploma"]["year"],
                # for no good reason this form has the existing
                # organization's name as the default value
                "organization": "---------",
                "abstract": self.tree["diploma"]["abstract"],
                "advisers_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDiplomaAdvisementSelfExistingOrganization(
        AddDiplomaAdvisementExistingOrganization):

    def setUp(self):
        super(AddDiplomaAdvisementSelfExistingOrganization, self).setUp()

        self.advisement["adviser"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["diploma"]["title"],
                "author": self.tree["diploma"]["author"],
                "year": self.tree["diploma"]["year"],
                "organization": self.tree["diploma"]["organization"]["name"],
                "abstract": self.tree["diploma"]["abstract"],
                "advisers_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDiplomaAdvisementExistingWorker(
        AddDiplomaAdvisementExistingOrganization):

    def setUp(self):
        super(AddDiplomaAdvisementExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.advisement["adviser"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["diploma"]["title"],
                "author": self.tree["diploma"]["author"],
                "year": self.tree["diploma"]["year"],
                "organization": self.tree["diploma"]["organization"]["name"],
                "abstract": self.tree["diploma"]["abstract"],
                "advisers_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddDiplomaAdvisementExistingWorkerThroughAlias(
        AddDiplomaAdvisementExistingOrganization):

    def setUp(self):
        super(AddDiplomaAdvisementExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.advisement["adviser"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["diploma"]["title"],
                "author": self.tree["diploma"]["author"],
                "year": self.tree["diploma"]["year"],
                "organization": self.tree["diploma"]["organization"]["name"],
                "abstract": self.tree["diploma"]["abstract"],
                "advisers_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddDiplomaAdvisementExistingWorkerNewAlias(
        AddDiplomaAdvisementExistingOrganization):

    def setUp(self):
        super(AddDiplomaAdvisementExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.advisement["adviser"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["diploma"]["title"],
                "author": self.tree["diploma"]["author"],
                "year": self.tree["diploma"]["year"],
                "organization": self.tree["diploma"]["organization"]["name"],
                "abstract": self.tree["diploma"]["abstract"],
                "advisers_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddDiplomaAdvisementNewWorker(AddDiplomaAdvisementExistingOrganization):

    def setUp(self):
        super(AddDiplomaAdvisementNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.advisement["adviser"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["diploma"]["title"],
                "author": self.tree["diploma"]["author"],
                "year": self.tree["diploma"]["year"],
                "organization": self.tree["diploma"]["organization"]["name"],
                "abstract": self.tree["diploma"]["abstract"],
                "advisers_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
