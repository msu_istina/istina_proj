# -*- coding: utf-8; -*-
"""This module does verification of course authorship addition procedure.

    - course authorship without organization for self is added properly
    - course authorship with organization for self is added properly
    - course authorship for existing worker is added properly
    - course authorship for existing worker is added properly through alias
    - course authorship for existing worker is added properly with new alias
    - course authorship for non-existing worker is added properly
"""

from courses.models import Course, CourseAuthorship
from organizations.models import Organization
from workers.models import Worker

from courses.forms import CourseForm
from common.views import LinkedToWorkersModelWizardView as Wizard
Wizard = Wizard.create_wizard_from_form(CourseForm)

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import OrganizationFactory

import datetime


class AddCourseAuthorship(RunAsUser, WorkerList, SeleniumToolbox,
                          SeleniumWizardFormAutomaton):

    def setUp(self):
        super(AddCourseAuthorship, self).setUp()

        self.authorship = Node(
            "authorship", CourseAuthorship,
            {
                "author": None
            })

        self.root = Node(
            "course", Course,
            {
                "title": "A title",
                "year": datetime.date.today().year,
                "organization": None,
                "description": "A description",
                "authorships": [self.authorship]
            })

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/courses/add/")

        self.stb_test_istina_page_title(u"Добавление учебного курса")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(u"Добавление учебного курса")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить курс в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{title} - учебный курс".format(
                title = self.root["title"]))

        self.stb_test_messages_text([u"Учебный курс успешно добавлен."])

        self.tree.verify_tree()


class AddCourseAuthorshipExistingOrganization(AddCourseAuthorship):

    def setUp(self):
        super(AddCourseAuthorshipExistingOrganization, self).setUp()

        self.organization = OrganizationFactory.create_entry(
            name = "An organization")

        self.root["organization"] = Node(
            "organization", Organization, self.organization)


@SeleniumTestStepByStep
class AddCourseAuthorshipSelfNoOrganization(AddCourseAuthorship):

    def setUp(self):
        super(AddCourseAuthorshipSelfNoOrganization, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["course"]["title"],
                "year": self.tree["course"]["year"],
                # for no good reason this form has the existing
                # organization's name as the default value
                "organization": "---------",
                "description": self.tree["course"]["description"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCourseAuthorshipSelfExistingOrganization(AddCourseAuthorshipExistingOrganization):

    def setUp(self):
        super(AddCourseAuthorshipSelfExistingOrganization, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["course"]["title"],
                "year": self.tree["course"]["year"],
                "organization": self.tree["course"]["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCourseAuthorshipExistingWorker(AddCourseAuthorshipExistingOrganization):

    def setUp(self):
        super(AddCourseAuthorshipExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.authorship["author"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["course"]["title"],
                "year": self.tree["course"]["year"],
                "organization": self.tree["course"]["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCourseAuthorshipExistingWorkerThroughAlias(AddCourseAuthorshipExistingOrganization):

    def setUp(self):
        super(AddCourseAuthorshipExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.authorship["author"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["course"]["title"],
                "year": self.tree["course"]["year"],
                "organization": self.tree["course"]["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddCourseAuthorshipExistingWorkerNewAlias(AddCourseAuthorshipExistingOrganization):

    def setUp(self):
        super(AddCourseAuthorshipExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.authorship["author"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["course"]["title"],
                "year": self.tree["course"]["year"],
                "organization": self.tree["course"]["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddCourseAuthorshipNewWorker(AddCourseAuthorshipExistingOrganization):

    def setUp(self):
        super(AddCourseAuthorshipNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.authorship["author"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["course"]["title"],
                "year": self.tree["course"]["year"],
                "organization": self.tree["course"]["organization"]["name"],
                "description": self.tree["course"]["description"],
                "authors_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
