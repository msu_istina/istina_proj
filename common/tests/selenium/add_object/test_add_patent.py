# -*- coding: utf-8; -*-
"""This module does verification of patent addition procedure.

    - patent for self is added properly
    - patent for existing worker is added properly
    - patent for existing worker is added properly through alias
    - patent for existing worker is added properly with new alias
    - patent for non-existing worker is added properly
"""

from patents.models import Patent, PatentAuthorship
from workers.models import Worker

from patents.forms import PatentForm
from common.views import LinkedToWorkersModelWizardView as Wizard
Wizard = Wizard.create_wizard_from_form(PatentForm)

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree

import datetime
import random


class AddPatent(RunAsUser, WorkerList, SeleniumToolbox,
                SeleniumWizardFormAutomaton):

    def setUp(self):
        super(AddPatent, self).setUp()

        self.authorship = Node(
            "authorship", PatentAuthorship,
            {
                "author": None,
                "original_name": "",
                "position": 1
            })

        self.root = Node(
            "patent", Patent,
            {
                "title": "A title",
                "number": unicode(random.randint(0, 10E6)),
                "date": datetime.date.today(),
                "description": "A description",
                "authorships": [self.authorship]
            })

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.driver.get(self.base_url + "/patents/add/")

        self.stb_test_istina_page_title(u"Добавление патента")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(u"Добавление патента")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить патент в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{title} - патент".format(
                title = self.root["title"]))

        self.stb_test_messages_text([u"Патент успешно добавлен."])

        self.tree.verify_tree()


@SeleniumTestStepByStep
class AddPatentSelf(AddPatent):

    def setUp(self):
        super(AddPatentSelf, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["patent"]["title"],
                "number": self.tree["patent"]["number"],
                "date": self.tree["patent"]["date"].strftime("%d.%m.%Y"),
                "description": self.tree["patent"]["description"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddPatentExistingWorker(AddPatent):

    def setUp(self):
        super(AddPatentExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["patent"]["title"],
                "number": self.tree["patent"]["number"],
                "date": self.tree["patent"]["date"].strftime("%d.%m.%Y"),
                "description": self.tree["patent"]["description"],
                "authors_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddPatentExistingWorkerThroughAlias(AddPatent):

    def setUp(self):
        super(AddPatentExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.alias.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["patent"]["title"],
                "number": self.tree["patent"]["number"],
                "date": self.tree["patent"]["date"].strftime("%d.%m.%Y"),
                "description": self.tree["patent"]["description"],
                "authors_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddPatentExistingWorkerNewAlias(AddPatent):

    def setUp(self):
        super(AddPatentExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker.fullname_short

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["patent"]["title"],
                "number": self.tree["patent"]["number"],
                "date": self.tree["patent"]["date"].strftime("%d.%m.%Y"),
                "description": self.tree["patent"]["description"],
                "authors_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddPatentNewWorker(AddPatent):

    def setUp(self):
        super(AddPatentNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker_node.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "title": self.tree["patent"]["title"],
                "number": self.tree["patent"]["number"],
                "date": self.tree["patent"]["date"].strftime("%d.%m.%Y"),
                "description": self.tree["patent"]["description"],
                "authors_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
