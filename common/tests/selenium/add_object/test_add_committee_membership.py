# -*- coding: utf-8; -*-
"""This module does verification of committee membership addition procedures.

    - membership for new conference for self is added properly
    - membership for existing conference for self is added properly
    - membership for member type for self is added properly
    - membership for chairman type for self is added properly
    - membership for existing worker is added properly
    - membership for existing worker is added properly through alias
    - membership for existing worker is added properly with new alias
    - membership for non-existing worker is added properly
"""

from conferences.models import Conference, ConferenceCommitteeMembership
from workers.models import Worker

from conferences.views import ConferenceCommitteeMembershipWizardView as Wizard
from workers.utils import get_fullname_raw

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import ConferenceFactory

from common.tests.selenium.seleniumwizards import connecttosession, loadsessiondata

import datetime


class AddCommitteeMembership(RunAsUser, WorkerList, SeleniumToolbox,
                             SeleniumWizardFormAutomaton):

    @staticmethod
    def type_to_str(membership_type):
        if membership_type == 1:
            return u"Член программного комитета"
        elif membership_type == 2:
            return u"Председатель программного комитета"
        else:
            raise ValueError

    def setUp(self):
        super(AddCommitteeMembership, self).setUp()

        self.name = None

        self.conference = Node(
            "conference", Conference,
            {
                "name": "A conference",
                "location": "A city",
                "year": datetime.date.today().year,
                "description": "A description"
            })

        self.root = Node(
            "membership", ConferenceCommitteeMembership,
            {
                "member": None,
                "conference": self.conference,
                "type": 1
            })

        self.wizard = self.init_wizard(Wizard)

    @connecttosession
    def stepbystep_prepare(self):
        self.driver.get(
            self.base_url + "/conferences/committee_memberships/add/")

        self.stb_test_istina_page_title(
            u"Добавление членства в программном комитете")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(
            u"Добавление членства в программном комитете")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить членство в программном комитете в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{name} в программном комитете конференции {conference} ({year}) "
            u"- членство в программном комитете конференции".format(
                name = self.name, conference = self.conference["name"],
                year = self.conference["year"]))

        self.tree.verify_tree()


class AddCommitteeMembershipExistingConference(AddCommitteeMembership):

    def setUp(self):
        super(AddCommitteeMembershipExistingConference, self).setUp()

        ConferenceFactory.create_entry(**self.conference.lookup)

    def stepbystep_post_finalize(self):
        self.stb_test_messages_text(
            [u"Членство в программном комитете успешно добавлено."])


class AddCommitteeMembershipNewConference(AddCommitteeMembership):

    def stepbystep_post_finalize(self):
        self.stb_test_messages_text(
            [
                u"Конференция успешно добавлена.",
                u"Членство в программном комитете успешно добавлено."
            ]
        )


@SeleniumTestStepByStep
class AddCommitteeMemberNewConference(AddCommitteeMembershipNewConference):

    def setUp(self):
        super(AddCommitteeMemberNewConference, self).setUp()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "type": self.type_to_str(self.tree["membership"]["type"]),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "new_conference",
            {
                "location": self.tree["conference"]["location"],
                "description": self.tree["conference"]["description"]
            }, initial_data = {
                "name": self.tree["conference"]["name"],
                "year": unicode(self.tree["conference"]["year"])
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCommitteeMemberNewConferenceThroughSimilar(
        AddCommitteeMembershipNewConference):

    def setUp(self):
        super(AddCommitteeMemberNewConferenceThroughSimilar, self).setUp()

        ConferenceFactory.create_entry(
            name = self.conference["name"].upper(),
            year = self.conference["year"])

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Conference] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "type": self.type_to_str(self.tree["membership"]["type"]),
                "member_str": self.worker.fullname
            })

    @loadsessiondata
    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "similar_conferences",
            {
                "conference": u" ".join((self.tree["conference"]["name"],
                                         u"(добавить новую конференцию)"))
            })

    def stepbystep_step3(self):
        self.process_wizard_form(
            self.wizard, "new_conference",
            {
                "location": self.tree["conference"]["location"],
                "description": self.tree["conference"]["description"]
            }, initial_data = {
                "name": self.tree["conference"]["name"],
                "year": unicode(self.tree["conference"]["year"])
            })

    def stepbystep_step4(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCommitteeMemberExistingConferenceThroughSimilar(
        AddCommitteeMembershipExistingConference):

    def setUp(self):
        super(AddCommitteeMemberExistingConferenceThroughSimilar, self).setUp()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "conference_str": self.tree["conference"]["name"].upper(),
                "year": self.tree["conference"]["year"],
                "type": self.type_to_str(self.tree["membership"]["type"]),
                "member_str": self.worker.fullname
            })

    @loadsessiondata
    def stepbystep_step2(self):
        self.process_wizard_form(
            self.wizard, "similar_conferences",
            {
                "conference": "{name} ({year})".format(
                    name = self.tree["conference"]["name"],
                    year = self.tree["conference"]["year"])
            })

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCommitteeMemberExistingConference(
        AddCommitteeMembershipExistingConference):

    def setUp(self):
        super(AddCommitteeMemberExistingConference, self).setUp()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "type": self.type_to_str(self.tree["membership"]["type"]),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCommitteeChairmanExistingConference(
        AddCommitteeMembershipExistingConference):

    def setUp(self):
        super(AddCommitteeChairmanExistingConference, self).setUp()

        self.name = self.worker.fullname_short

        self.root["type"] = 2
        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "type": self.type_to_str(self.tree["membership"]["type"]),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCommitteeMemberExistingWorker(
        AddCommitteeMembershipExistingConference):

    def setUp(self):
        super(AddCommitteeMemberExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "type": self.type_to_str(self.tree["membership"]["type"]),
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddCommitteeMemberExistingWorkerThroughAlias(
        AddCommitteeMembershipExistingConference):

    def setUp(self):
        super(AddCommitteeMemberExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("worker", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "type": self.type_to_str(self.tree["membership"]["type"]),
                "member_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddCommitteeMemberExistingWorkerNewAlias(
        AddCommitteeMembershipExistingConference):

    def setUp(self):
        super(AddCommitteeMemberExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.name = self.worker.fullname_short

        self.root["member"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "type": self.type_to_str(self.tree["membership"]["type"]),
                "member_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddCommitteeMemberNewWorker(
        AddCommitteeMembershipExistingConference):

    def setUp(self):
        super(AddCommitteeMemberNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.name = get_fullname_raw(
            self.worker_node["lastname"],
            self.worker_node["firstname"],
            self.worker_node["middlename"],
            initials = True)

        self.root["member"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "conference_str": self.tree["conference"]["name"],
                "year": self.tree["conference"]["year"],
                "type": self.type_to_str(self.tree["membership"]["type"]),
                "member_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
