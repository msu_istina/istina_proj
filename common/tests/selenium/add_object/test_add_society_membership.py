# -*- coding: utf-8; -*-
"""This module does verification of society membership addition procedure.

    - membership for self is added properly
    - membership for existing worker is added properly
    - membership for existing worker is added properly through alias
    - membership for existing worker is added properly with new alias
    - membership for non-existing worker is added properly
"""

from intrelations.models import SocietyMembership
from common.models import Country
from workers.models import Worker

from intrelations.forms import SocietyMembershipForm
from common.views import LinkedToWorkersModelWizardView as Wizard
Wizard = Wizard.create_wizard_from_form(SocietyMembershipForm)
from workers.utils import get_fullname_raw

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import CountryFactory

import datetime


class AddSocietyMembership(RunAsUser, WorkerList, SeleniumToolbox,
                           SeleniumWizardFormAutomaton):

    def setUp(self):
        super(AddSocietyMembership, self).setUp()

        self.country = CountryFactory.create_entry(name = "COUNTRY")

        self.name = None

        self.root = Node(
            "membership", SocietyMembership,
            {
                "member": None,
                "society": "A society",
                "country": Node("country", Country, self.country),
                "year": datetime.date.today().year,
                "comment": "A comment"
            })

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.driver.get(
            self.base_url + "/intrelations/memberships/societies/add/")

        self.stb_test_istina_page_title(
            u"Добавление членства в научном обществе")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(
            u"Добавление членства в научном обществе")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"]'
            u'[title="Сохранить членство в обществе в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{name} в научном обществе {society} "
            u"- членство в научном обществе".format(
                name = self.name, society = self.root["society"]))

        self.stb_test_messages_text(
            [u"Членство в научном обществе успешно добавлено."])

        self.tree.verify_tree()


@SeleniumTestStepByStep
class AddSocietyMembershipSelf(AddSocietyMembership):

    def setUp(self):
        super(AddSocietyMembershipSelf, self).setUp()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("member", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "society": self.tree["membership"]["society"],
                "country": self.tree["membership"]["country"]["name"],
                "year": self.tree["membership"]["year"],
                "comment": self.tree["membership"]["comment"],
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddSocietyMembershipExistingWorker(AddSocietyMembership):

    def setUp(self):
        super(AddSocietyMembershipExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("member", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "society": self.tree["membership"]["society"],
                "country": self.tree["membership"]["country"]["name"],
                "year": self.tree["membership"]["year"],
                "comment": self.tree["membership"]["comment"],
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddSocietyMembershipExistingWorkerThroughAlias(AddSocietyMembership):

    def setUp(self):
        super(AddSocietyMembershipExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("member", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "society": self.tree["membership"]["society"],
                "country": self.tree["membership"]["country"]["name"],
                "year": self.tree["membership"]["year"],
                "comment": self.tree["membership"]["comment"],
                "member_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddSocietyMembershipExistingWorkerNewAlias(AddSocietyMembership):

    def setUp(self):
        super(AddSocietyMembershipExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.name = self.worker.fullname_short

        self.root["member"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "society": self.tree["membership"]["society"],
                "country": self.tree["membership"]["country"]["name"],
                "year": self.tree["membership"]["year"],
                "comment": self.tree["membership"]["comment"],
                "member_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddSocietyMembershipNewWorker(AddSocietyMembership):

    def setUp(self):
        super(AddSocietyMembershipNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.name = get_fullname_raw(
            self.worker_node["lastname"],
            self.worker_node["firstname"],
            self.worker_node["middlename"],
            initials = True)

        self.root["member"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "society": self.tree["membership"]["society"],
                "country": self.tree["membership"]["country"]["name"],
                "year": self.tree["membership"]["year"],
                "comment": self.tree["membership"]["comment"],
                "member_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
