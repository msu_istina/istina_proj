# -*- coding: utf-8; -*-
"""This module does verification of collection article addition procedure.

    - article for new collection for self is added properly
    - article for existing collection for self is added properly
    - article for existing worker is added properly
    - article for existing worker is added properly through alias
    - article for existing worker is added properly with new alias
    - article for non-existing worker is added properly
"""

from publications.models import Article, ArticleAuthorship
from icollections.models import Collection, Series
from publishers.models import Publisher
from workers.models import Worker

from common.tests.selenium.seleniumtestsuite import SeleniumCheckbox
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import CollectionFactory
from common.tests.selenium.seleniumfactories import SeriesFactory
from common.tests.selenium.seleniumfactories import PublisherFactory
from add_publication import AddPublication

import datetime
import random


class AddCollectionArticle(AddPublication, SeleniumCheckbox):

    def setUp(self):
        super(AddCollectionArticle, self).setUp()

        self.authorship = Node(
            "authorship", ArticleAuthorship,
            {
                "author": None,
                "original_name": "",
                "position": 1
            })

        self.collection = Node(
            "collection", Collection,
            {
                "title": "A collection",
                "year": datetime.date.today().year,
                "series": None,
                "volume": random.randint(0, 10E6),
                "publisher": None,
                "location": "",
                "theses": False,
                "abstract": ""
            })

        self.root = Node(
            "article", Article,
            {
                "title": "An article",
                "journal": None,
                "volume": self.collection["volume"],
                "number": None,
                "year": self.collection["year"],
                "collection": self.collection,
                "firstpage": unicode(random.randint(0, 10E6)),
                "lastpage": unicode(random.randint(0, 10E6)),
                # meaningful abstract and doi are added to node later
                # as they can be added only during edit step
                "abstract": None,
                "doi": None,
                "language": None,
                "authorships": [self.authorship]
            })

    def stepbystep_step1(self):
        pass

    def stepbystep_pre_step2(self):
        super(AddCollectionArticle, self).stepbystep_pre_step2()
        self.radiobutton_check_by_id("work_type", "in_collection")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить публикацию и '
            u'перейти к следующему шагу добавления публикации"]').submit()

        self.stb_test_istina_page_title(
            u"Добавление дополнительной информации")

        self.stb_test_messages_text(
            [u"Статья {title} успешно добавлена.".format(
                title = self.root["title"])])

        self.tree.verify_tree()

        self.tree["article"]["abstract"] = "An abstract"
        self.tree["article"]["doi"] = "12:3456/i.am.an.example.d0i.000.111"

        self.css_set_single_element(
            "form#additional_form textarea#id_abstract",
            self.tree["article"]["abstract"])

        self.css_set_single_element(
            "form#additional_form input#id_doi",
            self.tree["article"]["doi"])

        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить информацию в систему"]').submit()

    def stepbystep_post_finalize(self):
        self.stb_test_istina_page_title(
            u"{title} - статья".format(title = self.root["title"]))

        self.stb_test_messages_text([u"Статья успешно отредактирована."])

        self.tree.verify_tree()


class AddColArticleExistingSeries(AddCollectionArticle):

    def setUp(self):
        super(AddColArticleExistingSeries, self).setUp()

        self.series = SeriesFactory.create_entry()
        self.collection["series"] = Node("series", Series, self.series)


class AddColArticleExistingPublisher(AddCollectionArticle):

    def setUp(self):
        super(AddColArticleExistingPublisher, self).setUp()

        self.publisher = PublisherFactory.create_entry()
        self.collection["publisher"] = Node(
            "publisher", Publisher, self.publisher)


class AddColArticleExistingCollection(
        AddColArticleExistingSeries,
        AddColArticleExistingPublisher):

    def setUp(self):
        super(AddColArticleExistingCollection, self).setUp()

        collection = CollectionFactory.create_entry(
            series = self.series,
            publisher = self.publisher,
            **self.collection.lookup)

        self.collection.pk = collection.pk


@SeleniumTestStepByStep
class AddColArticleSelfNewCollectionNoPublisher(AddCollectionArticle):

    def setUp(self):
        super(AddColArticleSelfNewCollectionNoPublisher, self).setUp()

        self.publisher_str = "A publisher"
        self.collection["location"] = "A city"

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.publisher_str,
                "publisher_city": self.tree["collection"]["location"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

        self.tree["collection"]["location"] = " ".join(
            (self.publisher_str, self.tree["collection"]["location"]))

    def stepbystep_step3(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddColArticleSelfNewCollectionNoPublisherThroughSimilar(AddCollectionArticle):

    def setUp(self):
        super(AddColArticleSelfNewCollectionNoPublisherThroughSimilar, self).setUp()

        self.publisher = PublisherFactory.create_entry()
        self.collection["location"] = "A city"

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Publisher] = 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.publisher.name,
                "publisher_city": self.tree["collection"]["location"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

        self.tree["collection"]["location"] = " ".join(
            (self.publisher.name, self.tree["collection"]["location"]))

    def stepbystep_step3(self):
        self.radiobutton_check_by_id("publisher-id", "id_publisher-id-create")

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddColArticleSelfNewCollectionExistingPublisher(
        AddColArticleExistingPublisher):

    def setUp(self):
        super(AddColArticleSelfNewCollectionExistingPublisher, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddColArticleSelfNewCollectionNewSeries(
        AddColArticleExistingPublisher):

    def setUp(self):
        super(AddColArticleSelfNewCollectionNewSeries, self).setUp()

        self.collection["series"] = Node("series", Series, {"title": "Series"})

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "series": self.tree["series"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddColArticleSelfNewCollectionExistingSeries(
        AddColArticleExistingPublisher, AddColArticleExistingSeries):

    def setUp(self):
        super(AddColArticleSelfNewCollectionExistingSeries, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "series": self.tree["series"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddColArticleSelfNewCollectionTheses(
        AddColArticleExistingPublisher, AddColArticleExistingSeries):

    def setUp(self):
        super(AddColArticleSelfNewCollectionTheses, self).setUp()

        self.collection["theses"] = True

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "series": self.tree["series"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

        self.checkbox_check('form#input_fields input#input_theses')

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)

    def stepbystep_post_finalize(self):
        self.stb_test_istina_page_title(
            u"{title} - тезисы доклада".format(
                title = self.root["title"]))

        self.stb_test_messages_text(
            [u"Тезисы доклада успешно отредактированы."])

        self.tree.verify_tree()


@SeleniumTestStepByStep
class AddColArticleSelfNewCollectionThroughSimilar(
        AddColArticleExistingPublisher, AddColArticleExistingSeries):

    def setUp(self):
        super(AddColArticleSelfNewCollectionThroughSimilar, self).setUp()

        kwargs = self.collection.lookup
        kwargs.update({"title": kwargs["title"].upper()})

        CollectionFactory.create_entry(
            series = self.series, publisher = self.publisher, **kwargs)

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Collection] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "series": self.tree["series"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id("id", "id_id-create")

        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddColArticleSelfExistingCollectionThroughSimilar(
        AddColArticleExistingCollection):

    def setUp(self):
        super(AddColArticleSelfExistingCollectionThroughSimilar, self).setUp()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "series": self.tree["series"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "id", "id_id-{collection_id}".format(
                collection_id = self.collection.pk))

        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddColArticleExistingWorker(AddColArticleExistingCollection):

    def setUp(self):
        super(AddColArticleExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.worker.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "series": self.tree["series"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "id", "id_id-{collection_id}".format(
                collection_id = self.collection.pk))

        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddColArticleExistingWorkerThroughAlias(AddColArticleExistingCollection):

    def setUp(self):
        super(AddColArticleExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.authorship["author"] = Node("worker", Worker, self.worker)
        self.authorship["original_name"] = self.alias.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "series": self.tree["series"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.alias.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "id", "id_id-{collection_id}".format(
                collection_id = self.collection.pk))

        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.alias.fullname, has_unknown = True)


@SeleniumTestStepByStep
class AddColArticleExistingWorkerNewAlias(AddColArticleExistingCollection):

    def setUp(self):
        super(AddColArticleExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker.fullname_short

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "series": self.tree["series"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker.fullname_short
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "id", "id_id-{collection_id}".format(
                collection_id = self.collection.pk))

        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short,
            has_unknown = True)


@SeleniumTestStepByStep
class AddColArticleNewWorker(AddColArticleExistingCollection):

    def setUp(self):
        super(AddColArticleNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.authorship["author"] = self.worker_node
        self.authorship["original_name"] = self.worker_node.fullname

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step2(self):
        self.multiple_input_form_fieldset_set(
            "form#input_fields", prefix = "input_",
            **{
                "title": self.tree["article"]["title"],
                "collection": self.tree["collection"]["title"],
                "series": self.tree["series"]["title"],
                "volume": self.tree["collection"]["volume"],
                "publisher_name": self.tree["publisher"]["name"],
                "publisher_city": self.tree["publisher"]["city"],
                "year": self.tree["collection"]["year"],
                "firstpage": self.tree["article"]["firstpage"],
                "lastpage": self.tree["article"]["lastpage"],
                "authors": self.worker_node.fullname
            })

    def stepbystep_step3(self):
        self.radiobutton_check_by_id(
            "id", "id_id-{collection_id}".format(
                collection_id = self.collection.pk))

        self.radiobutton_check_by_id(
            "publisher-id", "id_publisher-id-{publisher_id}".format(
                publisher_id = self.publisher.pk))

        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
