# -*- coding: utf-8; -*-
"""This module does verification of honorary membership addition procedure.

    - membership for self is added properly
    - membership for existing worker is added properly
    - membership for existing worker is added properly through alias
    - membership for existing worker is added properly with new alias
    - membership for non-existing worker is added properly
"""

from intrelations.models import HonoraryMembership, HonoraryMembershipCategory
from common.models import Country
from workers.models import Worker

from intrelations.forms import HonoraryMembershipForm
from common.views import LinkedToWorkersModelWizardView as Wizard
Wizard = Wizard.create_wizard_from_form(HonoraryMembershipForm)
from workers.utils import get_fullname_raw

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import (
    HonoraryMembershipCategoryFactory, CountryFactory)

import datetime


class AddHonoraryMembership(RunAsUser, WorkerList, SeleniumToolbox,
                            SeleniumWizardFormAutomaton):

    def setUp(self):
        super(AddHonoraryMembership, self).setUp()

        self.country = CountryFactory.create_entry(name = "COUNTRY")

        self.category = HonoraryMembershipCategoryFactory.create_entry(
            name = "Honor type")

        self.name = None

        self.root = Node(
            "membership", HonoraryMembership,
            {
                "member": None,
                "category": Node(
                    "category", HonoraryMembershipCategory, self.category),
                "organization": "An organization",
                "country": Node("country", Country, self.country),
                "year": datetime.date.today().year,
                "comment": "A comment"
            })

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        self.driver.get(
            self.base_url + "/intrelations/memberships/honorary/add/")

        self.stb_test_istina_page_title(
            u"Добавление почетного членства в организации")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(
            u"Добавление почетного членства в организации")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"]'
            u'[title="Сохранить почетное членство в организации в систему"]').submit()

        self.stb_test_istina_page_title(
            u"{name}, {honortype} в {organization} "
            u"- почетное членство в организации".format(
                name = self.name,
                honortype = self.root["category"]["name"],
                organization = self.root["organization"]))

        self.stb_test_messages_text(
            [u"Почетное членство в организации успешно добавлено."])

        self.tree.verify_tree()


@SeleniumTestStepByStep
class AddHonoraryMembershipSelf(AddHonoraryMembership):

    def setUp(self):
        super(AddHonoraryMembershipSelf, self).setUp()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("member", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "category": self.tree["membership"]["category"]["name"],
                "organization": self.tree["membership"]["organization"],
                "country": self.tree["membership"]["country"]["name"],
                "year": self.tree["membership"]["year"],
                "comment": self.tree["membership"]["comment"],
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddHonoraryMembershipExistingWorker(AddHonoraryMembership):

    def setUp(self):
        super(AddHonoraryMembershipExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("member", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "category": self.tree["membership"]["category"]["name"],
                "organization": self.tree["membership"]["organization"],
                "country": self.tree["membership"]["country"]["name"],
                "year": self.tree["membership"]["year"],
                "comment": self.tree["membership"]["comment"],
                "member_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddHonoraryMembershipExistingWorkerThroughAlias(AddHonoraryMembership):

    def setUp(self):
        super(AddHonoraryMembershipExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.name = self.worker.fullname_short

        self.root["member"] = Node("member", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "category": self.tree["membership"]["category"]["name"],
                "organization": self.tree["membership"]["organization"],
                "country": self.tree["membership"]["country"]["name"],
                "year": self.tree["membership"]["year"],
                "comment": self.tree["membership"]["comment"],
                "member_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddHonoraryMembershipExistingWorkerNewAlias(AddHonoraryMembership):

    def setUp(self):
        super(AddHonoraryMembershipExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.name = self.worker.fullname_short

        self.root["member"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "category": self.tree["membership"]["category"]["name"],
                "organization": self.tree["membership"]["organization"],
                "country": self.tree["membership"]["country"]["name"],
                "year": self.tree["membership"]["year"],
                "comment": self.tree["membership"]["comment"],
                "member_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddHonoraryMembershipNewWorker(AddHonoraryMembership):

    def setUp(self):
        super(AddHonoraryMembershipNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.name = get_fullname_raw(
            self.worker_node["lastname"],
            self.worker_node["firstname"],
            self.worker_node["middlename"],
            initials = True)

        self.root["member"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "category": self.tree["membership"]["category"]["name"],
                "organization": self.tree["membership"]["organization"],
                "country": self.tree["membership"]["country"]["name"],
                "year": self.tree["membership"]["year"],
                "comment": self.tree["membership"]["comment"],
                "member_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
