# -*- coding: utf-8; -*-
"""This module does verification of traineeship addition procedure.

    - traineeship import for self is added properly
    - traineeship export for self is added properly
    - traineeship for existing worker is added properly
    - traineeship for existing worker is added properly through alias
    - traineeship for existing worker is added properly with new alias
    - traineeship for non-existing worker is added properly
"""

from intrelations.models import Traineeship
from organizations.models import Organization, Department
from common.models import Country
from workers.models import Worker

from intrelations.forms import TraineeshipForm
from common.views import LinkedToWorkersModelWizardView as Wizard
Wizard = Wizard.create_wizard_from_form(TraineeshipForm)
from workers.utils import get_fullname_raw

from common.tests.selenium.seleniumtestsuite import SeleniumToolbox
from common.tests.selenium.seleniumwizards import SeleniumWizardFormAutomaton
from common.tests.selenium.seleniumworkerlist import WorkerList
from common.tests.selenium.seleniumuserprofile import RunAsUser
from common.tests.selenium.seleniumstepbystep import SeleniumTestStepByStep
from common.tests.selenium.seleniummodelverify import SeleniumModelNode as Node
from common.tests.selenium.seleniummodelverify import SeleniumModelTree as Tree
from common.tests.selenium.seleniumfactories import (
    DepartmentFactory, CountryFactory)

import datetime


class AddTraineeship(RunAsUser, WorkerList, SeleniumToolbox,
                     SeleniumWizardFormAutomaton):

    @staticmethod
    def importing_to_str(importing):
        if importing:
            return u"Стажировка внешнего сотрудника в подразделении"
        else:
            return u"Стажировка сотрудника подразделения во внешней организации"

    def setUp(self):
        super(AddTraineeship, self).setUp()

        self.country = CountryFactory.create_entry(name = "COUNTRY")

        self.department = DepartmentFactory.create_entry(name = "A department")

        self.name = None

        self.root = Node(
            "traineeship", Traineeship,
            {
                "trainee": None,
                "department": Node(
                    "department", Department, self.department),
                "organization": "An organization",
                "country": Node("country", Country, self.country),
                "importing": None,
                "task": "A task",
                "startdate": datetime.date.today(),
                "enddate": datetime.date.today()
            })

        self.wizard = self.init_wizard(Wizard)

    def stepbystep_prepare(self):
        # DepartmentFactory creates additional Organization
        self.tree.instance_index[Organization] += 1

        self.driver.get(self.base_url + "/intrelations/traineeships/add/")

        self.stb_test_istina_page_title(u"Добавление стажировки")

    def stepbystep_next(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Перейти на следующий шаг добавления"]').submit()

        self.stb_test_istina_page_title(u"Добавление стажировки")

    def stepbystep_finalize(self):
        self.driver.find_element_by_css_selector(
            u'input[type="submit"][title="Сохранить стажировку в систему"]').submit()

        self.stb_test_messages_text([u"Стажировка успешно добавлена."])

        self.tree.verify_tree()


class AddTraineeshipExport(AddTraineeship):

    def setUp(self):
        super(AddTraineeshipExport, self).setUp()

        self.root["importing"] = False

    def stepbystep_post_finalize(self):
        self.stb_test_istina_page_title(
            u"{name}, стажировка в {organization} - стажировка в организации".format(
                name = self.name, organization = self.root["organization"]))


class AddTraineeshipImport(AddTraineeship):

    def setUp(self):
        super(AddTraineeshipImport, self).setUp()

        self.root["importing"] = True

    def stepbystep_post_finalize(self):
        self.stb_test_istina_page_title(
            u"{name}, стажировка из {organization} - стажировка в организации".format(
                name = self.name, organization = self.root["organization"]))


@SeleniumTestStepByStep
class AddTraineeshipImportSelf(AddTraineeshipImport):

    def setUp(self):
        super(AddTraineeshipImportSelf, self).setUp()

        self.name = self.worker.fullname_short

        self.root["trainee"] = Node("trainee", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "department": self.tree["traineeship"]["department"]["name"],
                "organization": self.tree["traineeship"]["organization"],
                "country": self.tree["traineeship"]["country"]["name"],
                "direction": self.importing_to_str(self.tree["traineeship"]["importing"]),
                "task": self.tree["traineeship"]["task"],
                "startdate": self.tree["traineeship"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["traineeship"]["enddate"].strftime("%d.%m.%Y"),
                "trainee_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddTraineeshipExportSelf(AddTraineeshipExport):

    def setUp(self):
        super(AddTraineeshipExportSelf, self).setUp()

        self.name = self.worker.fullname_short

        self.root["trainee"] = Node("trainee", Worker, self.worker)

        self.tree = Tree(self.root)

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "department": self.tree["traineeship"]["department"]["name"],
                "organization": self.tree["traineeship"]["organization"],
                "country": self.tree["traineeship"]["country"]["name"],
                "direction": self.importing_to_str(self.tree["traineeship"]["importing"]),
                "task": self.tree["traineeship"]["task"],
                "startdate": self.tree["traineeship"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["traineeship"]["enddate"].strftime("%d.%m.%Y"),
                "trainee_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddTraineeshipExistingWorker(AddTraineeshipExport):

    def setUp(self):
        super(AddTraineeshipExistingWorker, self).setUp()

        self.worker = self.prepare_existing_worker()

        self.name = self.worker.fullname_short

        self.root["trainee"] = Node("trainee", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "department": self.tree["traineeship"]["department"]["name"],
                "organization": self.tree["traineeship"]["organization"],
                "country": self.tree["traineeship"]["country"]["name"],
                "direction": self.importing_to_str(self.tree["traineeship"]["importing"]),
                "task": self.tree["traineeship"]["task"],
                "startdate": self.tree["traineeship"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["traineeship"]["enddate"].strftime("%d.%m.%Y"),
                "trainee_str": self.worker.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.worker.fullname)


@SeleniumTestStepByStep
class AddTraineeshipExistingWorkerThroughAlias(AddTraineeshipExport):

    def setUp(self):
        super(AddTraineeshipExistingWorkerThroughAlias, self).setUp()

        self.worker, self.alias = self.prepare_existing_worker_through_alias()

        self.name = self.worker.fullname_short

        self.root["trainee"] = Node("trainee", Worker, self.worker)

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "department": self.tree["traineeship"]["department"]["name"],
                "organization": self.tree["traineeship"]["organization"],
                "country": self.tree["traineeship"]["country"]["name"],
                "direction": self.importing_to_str(self.tree["traineeship"]["importing"]),
                "task": self.tree["traineeship"]["task"],
                "startdate": self.tree["traineeship"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["traineeship"]["enddate"].strftime("%d.%m.%Y"),
                "trainee_str": self.alias.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(self.alias.fullname)


@SeleniumTestStepByStep
class AddTraineeshipExistingWorkerNewAlias(AddTraineeshipExport):

    def setUp(self):
        super(AddTraineeshipExistingWorkerNewAlias, self).setUp()

        self.worker, self.worker_node = self.prepare_existing_worker_new_alias()

        self.name = self.worker.fullname_short

        self.root["trainee"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "department": self.tree["traineeship"]["department"]["name"],
                "organization": self.tree["traineeship"]["organization"],
                "country": self.tree["traineeship"]["country"]["name"],
                "direction": self.importing_to_str(self.tree["traineeship"]["importing"]),
                "task": self.tree["traineeship"]["task"],
                "startdate": self.tree["traineeship"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["traineeship"]["enddate"].strftime("%d.%m.%Y"),
                "trainee_str": self.worker.fullname_short
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_unique(
            self.worker.fullname, aliased_name = self.worker.fullname_short)


@SeleniumTestStepByStep
class AddTraineeshipNewWorker(AddTraineeshipExport):

    def setUp(self):
        super(AddTraineeshipNewWorker, self).setUp()

        self.worker_node = self.prepare_new_worker()

        self.name = get_fullname_raw(
            self.worker_node["lastname"],
            self.worker_node["firstname"],
            self.worker_node["middlename"],
            initials = True)

        self.root["trainee"] = self.worker_node

        self.tree = Tree(self.root)
        self.tree.instance_index[Worker] += 1

    def stepbystep_step1(self):
        self.process_wizard_form(
            self.wizard, "main",
            {
                "department": self.tree["traineeship"]["department"]["name"],
                "organization": self.tree["traineeship"]["organization"],
                "country": self.tree["traineeship"]["country"]["name"],
                "direction": self.importing_to_str(self.tree["traineeship"]["importing"]),
                "task": self.tree["traineeship"]["task"],
                "startdate": self.tree["traineeship"]["startdate"].strftime("%d.%m.%Y"),
                "enddate": self.tree["traineeship"]["enddate"].strftime("%d.%m.%Y"),
                "trainee_str": self.worker_node.fullname
            })

    def stepbystep_step2(self):
        self.workerlist_test_single_worker_is_new(self.worker_node.fullname)
