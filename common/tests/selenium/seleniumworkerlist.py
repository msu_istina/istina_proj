# -*- coding: utf-8; -*-
from seleniumtestsuite import TestTableByCSS


class WorkerList(TestTableByCSS):
    workerlist_selector = "table.worker-list"

    def workerlist_list_workers(self):
        return self.table_list_rows(self.workerlist_selector)[1:]  # skip the first hidden row

    def workerlist_get_worker_row_by_name(self, name):
        workers = self.workerlist_list_workers()
        for worker in workers:
            if worker.find_element_by_css_selector("td.initial-name").text == name:
                return worker

    def workerlist_test_worker_is_new(self, worker_row, name):
        self.assertEqual(worker_row.find_element_by_css_selector(
            "td.initial-name").text, name)
        self.assertEqual(worker_row.find_element_by_css_selector(
            'td:last-child span[class$="selected"]').text, u"добавить нового сотрудника")

    def workerlist_parse_row(self, worker_row, has_unknown = False):
        name = worker_row.find_element_by_css_selector("td.initial-name").text

        spans = worker_row.find_elements_by_css_selector(
            'td:last-child > span[class^="worker-instance"]')

        if not has_unknown:
            self.assertEqual(spans[-1].text, u"добавить нового сотрудника")
        else:
            self.assertEqual(spans[-2].text, u"добавить нового сотрудника")
            self.assertEqual(spans[-1].text, u"затрудняюсь выбрать")

        return name, spans

    def workerlist_test_worker_is_unique(
            self, worker_row, name, has_unknown = False, aliased_name = ""):
        row_name, spans = self.workerlist_parse_row(worker_row, has_unknown)

        if not aliased_name:
            self.assertEqual(row_name, name)
        else:
            self.assertEqual(row_name, aliased_name)

        if not has_unknown:
            spans.pop()
        else:
            spans.pop()
            spans.pop()

        span, = spans
        self.assertEqual(span.find_element_by_css_selector(
            'a[class$="selected"]').text, name)

    def workerlist_test_single_worker_is_new(self, name):
        worker_row, = self.workerlist_list_workers()
        self.workerlist_test_worker_is_new(worker_row, name)

    def workerlist_test_single_worker_is_unique(self, name, **kwargs):
        worker_row, = self.workerlist_list_workers()
        self.workerlist_test_worker_is_unique(worker_row, name, **kwargs)
