from django.contrib import admin

from common.utils.admin import register_with_versions, MyModelAdmin
from common.models import Attachment, AttachmentCategory, AttachmentCategoryMembership, MergeRequest, MergeRequestItem, \
ObjectList, ObjectListItem, ObjectListChangeStatusEvent, ChoiceListOption, Country, ScienceWork, AuthorRole, AuthorRoleObject, AuthorScience

register_with_versions(Attachment)
register_with_versions(AttachmentCategory)
register_with_versions(AttachmentCategoryMembership)
register_with_versions(MergeRequest)
register_with_versions(MergeRequestItem)
register_with_versions(ObjectList)
register_with_versions(ObjectListItem)
register_with_versions(ObjectListChangeStatusEvent)
register_with_versions(ChoiceListOption)
register_with_versions(ScienceWork)
register_with_versions(AuthorScience)
register_with_versions(AuthorRoleObject)
register_with_versions(AuthorRole)


class CountryAdmin(MyModelAdmin):
    list_display = ('name', 'name_ru',)
    list_display_links = ('name',)

register_with_versions(Country, CountryAdmin)
