# in this dictionary all hardcoded primary keys are given meaningful labels
DB_CONSTANTS = {
    "bookgrouptype": {
        "textbook": 6054932,
        "textbook_grif": 217739,
        "monograph": 190,
        },
    "tagtype": {
        "epub": 4620649,
        "in_encyclopedia": 4620647,
        "review": 4620650, # a review of someone's else work
        "chapter": 6438853,
        },
    "sponsors": {
        "budget": 8045661,
        },
    }

DB_CONSTANTS["bookgrouptype"]["all_textbooks"] = [
    217739, 6054932, 100, 120, 130
    ]

DB_CONSTANTS["bookgrouptype"]["all_monographs"] = [
    DB_CONSTANTS["bookgrouptype"][t] for t in ['monograph',]
    ]


DB_CONSTANTS["bookgrouptype"]["all_reprints"] = [
    217741 # identical reprints
    , 9609137 # corrected reprints
    ]
DB_CONSTANTS["bookgrouptype"]["all_reprints_str"] = ",".join(map(str, DB_CONSTANTS["bookgrouptype"]["all_reprints"]))


DB_CONSTANTS["bookgrouptype"]["all_monographs_str"] = ",".join(map(str, DB_CONSTANTS["bookgrouptype"]["all_monographs"]))

DB_CONSTANTS["bookgrouptype"]["all_textbooks_str"] = ",".join(map(str, DB_CONSTANTS["bookgrouptype"]["all_textbooks"]))

