#  #!/usr/bin/env python


import sys
import site
import os
os.environ['VIRTUAL_ENV']='/home/max/.virtualenvs/web/'
envpath = os.path.join(os.environ['VIRTUAL_ENV'], 'lib/python2.7/site-packages')
prev_sys_path = list(sys.path)
site.addsitedir(envpath)
sys.path.append(os.path.dirname(os.path.abspath(__file__)))

new_sys_path = [p for p in sys.path if p not in prev_sys_path]
for item in new_sys_path:
    sys.path.remove(item)
    sys.path[:0] = new_sys_path
from django.core.handlers.wsgi import WSGIHandler
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
application = WSGIHandler()
