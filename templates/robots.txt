User-agent: *
Disallow: /workers/*/style
Disallow: /publications/*.
Disallow: /publications/article/download/
Disallow: /*style/
Disallow: /*.ris$
Disallow: /*.isi$
Disallow: /*.enw$
Disallow: /*.ads$
