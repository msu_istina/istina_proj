# used for sending notifications of 500 requests
ADMINS = ()

# used for sending notifications of 404 requests
MANAGERS = ADMINS

# send notifications of 404 requests to managers
SEND_BROKEN_LINK_EMAILS = False
