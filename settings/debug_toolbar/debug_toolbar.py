from settings.default.apps import INSTALLED_APPS
from settings.default.middleware import MIDDLEWARE_CLASSES

INSTALLED_APPS += ("debug_toolbar",)

MIDDLEWARE_CLASSES += ("debug_toolbar.middleware.DebugToolbarMiddleware",)

def show_toolbar(request):
    if request.user and request.user.username in ["goldan", "negrega", "Vladimir_Bukhonov", "vladimir_bukhonov", "izzet_vladimir"]:
        return True
    return False

DEBUG_TOOLBAR_CONFIG = {'INTERCEPT_REDIRECTS': False, 'SHOW_TOOLBAR_CALLBACK': show_toolbar}

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.cache.CacheDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)
