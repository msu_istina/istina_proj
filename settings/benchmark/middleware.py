MIDDLEWARE_CLASSES = (
"django.middleware.common.CommonMiddleware",
"django.contrib.sessions.middleware.SessionMiddleware",
#"django.middleware.csrf.CsrfViewMiddleware",
"common.disable_csrf.DisableCSRF",
"django.contrib.auth.middleware.AuthenticationMiddleware",
"django.middleware.doc.XViewMiddleware",
"django.middleware.transaction.TransactionMiddleware",
"django.contrib.messages.middleware.MessageMiddleware",
"django.contrib.flatpages.middleware.FlatpageFallbackMiddleware",
"common.middleware.MessageMiddleware",
"debug_toolbar.middleware.DebugToolbarMiddleware",
)

