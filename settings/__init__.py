# This module loads the configuration variant that is set in local_config.py
# It means that it loads all from modules in default folder and then loads all from modules in folders specified by variable LOCAL_CONFIG, possibly overriding default values.
import os
from os import listdir
from os.path import join, splitext, abspath, dirname, realpath
import sys

# variables that are used by modules in default folder
# e.g. envs/project_name/django-project_name/project_name/ (settings dir)
SETTINGS_DIR = abspath(join(dirname(realpath(__file__)), '..'))
# e.g. envs/project_name/django-project_name/ (repository working copy dir)
WC_DIR = abspath(join(SETTINGS_DIR, '..'))
# e.g. envs/project_name/ (virtual env dir)
ENV_DIR = abspath(join(SETTINGS_DIR, '..', '..'))
# url from project static files are served (js, css etc.)
STATIC_URL = '/static/'

def get_modules(folder):
    '''Returns names of all .py files in folder except __init__.py'''
    modules = [splitext(file)[0] for file in listdir(folder) if splitext(file)[1] == '.py']
    modules.remove('__init__')
    return modules

# load all from modules from the 'default' directory
folder = abspath(dirname(__file__))
default_modules = get_modules(join(folder, 'default'))
for module in default_modules:
    exec "from settings.default.%s import *" % module

# load all from modules from directories specified in LOCAL_CONFIG options tuple
import settings.local_config
from settings.local_config import LOCAL_CONFIG
for option in LOCAL_CONFIG:
    modules = get_modules(join(folder, option))
    for module in modules:
        exec "from settings.%s.%s import *" % (option, module)

# import finally overriding settings from local_config file again
from settings.local_config import * # works for simple settings: DEBUG etc.
reload(settings.local_config) # works for imported settings, e.g. DATABASES[]

apps_directory = join(dirname(folder[:-1]), 'apps')
if apps_directory not in sys.path:
    sys.path.append(apps_directory)
    pythonpath = os.environ.get('PYTHONPATH', None)
    os.environ['PYTHONPATH'] = os.pathsep.join(
        (pythonpath, apps_directory)
    ) if pythonpath else apps_directory

# needed to be able to run scripts, e.g. istina_bibtex2html from python
os.environ['PATH'] += '%s%s' % (os.pathsep, abspath(join(ENV_DIR, 'bin'))) # e.g. envs/istina/bin/
os.environ['PATH'] += '%s%s' % (os.pathsep, abspath(join(WC_DIR, 'env', 'bin'))) # e.g. envs/istina/django-istina/env/bin/
