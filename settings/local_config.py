# local configuration options (don't forget comma for a single option). Ordering matters, since settings can be overriden by the next option!
# this file shouldn't be commited via svn, on each machine there should be its own file, possibly different

# Configuration options for the current machine
LOCAL_CONFIG = ('development', 'local', 'debug_toolbar')

def show_toolbar(request):
    if request.user and request.user.username in ["goldan", "negrega", "Vladimir_Bukhonov", "vladimir_bukhonov", "izzet_vladimir", "maxxk"]:
        return True
    return False

DEBUG_TOOLBAR_CONFIG = {'INTERCEPT_REDIRECTS': False, 'SHOW_TOOLBAR_CALLBACK': show_toolbar}

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)



DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "testscience",
        "PASSWORD": "tttpwd",
        "HOST": "188.44.49.70",
        "PORT": "1521",
    },
    'read_only_report': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "SCIENCE_READ",
        "PASSWORD": "sicence+ISTINA_RO",
        "HOST": "188.44.50.108",
        "PORT": "1521",
    }
}

CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        },
        'bigdata': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        }
}

DATABASES['reports_server'] = DATABASES['default'] #= DATABASES['read_only_report']
DATABASES['read_write_default'] = DATABASES['default']
#DATABASES['reports_server'] = DATABASES['read_only_report']
DEBUG=True
