# local configuration options (don't forget comma for a single option). Ordering matters, since settings can be overriden by the next option!
# this file shouldn't be commited via svn, on each machine there should be its own file, possibly different

# Configuration options for the current machine
LOCAL_CONFIG = ('development', 'local')
