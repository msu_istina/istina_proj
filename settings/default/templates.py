# List of context processors being used.
TEMPLATE_CONTEXT_PROCESSORS = (
    #for django-authority
    #start
    #'django.core.context_processors.auth', # it's written in the docs, but causes errors, so it should be commented
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    #for django-authority
    #end
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'userprofile.context_processors.css_classes',
    'common.context_processors.current_database',
    'common.context_processors.cache_time',
    'common.context_processors.get_orgfields'
)

from os.path import join
from settings import SETTINGS_DIR
# paths to search for templates
TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    join(SETTINGS_DIR, 'templates'), # first search for a project version of a template
    # then perform other searches, e.g. in app's folder etc. (automatically)
)
