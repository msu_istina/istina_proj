# -*- coding: utf-8 -*-
from os.path import join
from settings import ENV_DIR, SETTINGS_DIR


INSTALLED_APPS = (
    # django contrib apps
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.comments',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.formtools.wizard',
    'django.contrib.humanize',
    'django.contrib.messages',
    'django.contrib.sessions',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django.contrib.staticfiles',

    # third-party apps
    'actstream',
    'authority',
    'django_extensions',
    'djcelery',
    'feedback',
    'indexer',
    'myforms',
    'paging',
    'pytils',
    'sentry',
    'sentry.client',
    'sqlreports',
    'userprofile',
    'object_permissions',
    'compressor',
    'reversion',
    'discover_runner',
    'factory',
    'django_select2',

    # project core apps
    'common',
    'icollections',
    'journals',
    'organizations',
    'patents',
    'publications',
    'publishers',
    'workers',
    'certificates',
    'reports',
    'investments',
    'courses',
    'diplomas',
    'dissertation_councils',
    'dissertations',
    'projects',
    'conferences',
    'statistics',
    'benchmarks',
    'schools',
    'smi',
    'semantic',
    'achievements',
    'scireport',
    'equipment',
    'tags',
    'intrelations',
    'awards',
    'orgproperty',
    'depadmin',
    'export_activities',
    'celery_utils',
    'contests',
    'events',
    'unified_permissions',
    'pmodel',
    'eduwork',
)

# django-profile app
AUTH_PROFILE_MODULE = 'workers.profile'
GOOGLE_MAPS_API_KEY = "ABQIAAAAkhBh3zp6jWFSMw1NYoPcsBSs0rQttn0QMzb0riN1iEygcKbfZxQCiZVPZK3aKAL7UutIrocbUEaWKQ"
AVATAR_WEBSEARCH = True
REQUIRE_EMAIL_CONFIRMATION = True
LOGIN_REDIRECT_URL = "/home/"
ALLOW_DUPLICATE_EMAILS = True
USERPROFILE_CSS_CLASSES = {
    'content_main_2col':    'span-15 colborder',
    'content_related':      'span-8 last',
    'content_main':         'span-22 prepend-1 append-1',
    'nav':                  'span-24 last',
    'public_vcard_content': 'span-10',
    'public_vcard_map':     'span-10',
    'overview_avatar':      'span-3',
    'overview_details':     'span-9 last',
    'overview_location':    'span-12 last',
}

# django.contrib.staticfiles app
STATICFILES_DIRS = (
    join(ENV_DIR, 'src', 'django-profile', 'media'),
    ('object_permissions', join(ENV_DIR, 'src', 'django-object-permissions', 'object_permissions', 'media')),
    join(SETTINGS_DIR, 'static'),
)
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
)

# publications app
FREECITE_URL = "http://erizo.s2s.msu.ru:3000/citations/create"

# django.contrib.auth app
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'object_permissions.backend.ObjectPermBackend',
)

SENTRY_DATABASE_USING = "read_write_default"

DISAMBIGUATION = {
    'url': 'http://93.180.23.137:9091/',
    'timeout': 90
}

REDMINE_FEEDBACK = {
    'root_url': 'http://93.180.23.137:8080/',
    'issues_url':   'issues.json',
    'users_url':    'users.json',
    'user_to_group_url':    'groups/14/users.json',
    'redmine_api_key':  'ef4808cab95ede429583b68b844480bd5c64bbcf',
    'project_id': '2',
    'tracker_id': '5',
    'status_id': '1',
    'custom_field_id' : '1',
    'representatives_tracker_id': '10',
    'anonymous_tracker_id': '11',
}
