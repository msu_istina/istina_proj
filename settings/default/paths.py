from os.path import join
from settings import ENV_DIR

# Path from which user-uploaded media is served
MEDIA_ROOT = join(ENV_DIR, 'var', 'media')
# apps' static files are served from this path by staticfiles app
STATIC_ROOT = join(ENV_DIR, 'var', 'static')
# URL for user-uploaded media, used in templates
MEDIA_URL = '/media/'
# path to root urls.py
ROOT_URLCONF = 'urls'
