from os.path import join
from settings import ENV_DIR

DATABASES = {
    'science': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "science",
        "PASSWORD": "istinaproduction42",
        "HOST": "localhost",
        "PORT": "1521",
    },
    'science_erizo': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "science",
        "PASSWORD": "scipwd",
        "HOST": "erizo.s2s.msu.ru",
        "PORT": "1521",
    },
    'since': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "since",
        "PASSWORD": "since",
        "HOST": "erizo.s2s.msu.ru",
        "PORT": "1521",
    },
    'scirep': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "scirep",
        "PASSWORD": "$scirep$",
        "HOST": "erizo.s2s.msu.ru",
        "PORT": "1521",
    },
    'local': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'mydatabase'
    },
    'local_goldan': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(ENV_DIR, 'var', 'db', 'istina.db')
    },
    'local_oracle': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "since",
        "PASSWORD": "since",
        "HOST": "goldan.ru",
        "PORT": "1521",
    },
    'production_tunneled': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "science",
        "PASSWORD": "scipwd",
        "HOST": "goldan.ru",
        "PORT": "1521",
    },
    'calculator': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "science",
        "PASSWORD": "tttpwd",
        "HOST": "voracle-calc",
        "PORT": "1521",
    },
    'calculator2': {
       'ENGINE': 'django.db.backends.oracle',
       'NAME': 'orcl',
       "USER": "science",
       "PASSWORD": "tttpwd",
       "HOST": "voracle",
       "PORT": "1521",
    },    
    'dev-oracle': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "science",
        "PASSWORD": "tttpwd",
        "HOST": "dev-oracle",
        "PORT": "1521",
    },
    'read_only_default': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "science_ro",
        "PASSWORD": "istinaproduction42",
        "HOST": "localhost",
        "PORT": "1521",
    },
    'read_write_default': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "science_rw",
        "PASSWORD": "istinaproduction42",
        "HOST": "localhost",
        "PORT": "1521",
    },
    'development-oracle': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'orcl',
        "USER": "science",
        "PASSWORD": "tttpwd",
        "HOST": "grid-4.maxxk.me",
        "PORT": "1521",
    },    
}

DATABASES['default'] = DATABASES['local']
DATABASES['reports_server'] = DATABASES['default'] 
