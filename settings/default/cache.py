﻿# -*- coding: utf-8 -*-
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211',
    },
    'bigdata': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'disabled': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

# Lifetime of cache in seconds, 1 month= 30*24*60*60 = 259200 seconds. Minus one second, for sure.
CACHE_TIME = 259199
# Lifetime of outer cache for articles in seconds, 26 hours = 26*60*60 =  93600 seconds.
CACHE_TIME_ARTICLES = 93600
#Lifetime of workers_list
CACHE_TIME_WORKERS_LIST = 600