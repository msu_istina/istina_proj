import os
import django.core.handlers.wsgi

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
os.environ['NLS_SORT'] = 'RUSSIAN'
application = django.core.handlers.wsgi.WSGIHandler()
