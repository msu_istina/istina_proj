from os.path import join
import logging

from settings import ENV_DIR

# Default configuration is the following:
# 1. There are three debug types of messages:
# django.db.backends (database-related, including sql logs) > database.log
# django (generic django, e.g. request) > django.log
# apps (all other debug messages) > apps.log
# These handlers emit only debug messages (through debug filter)
# 2. All messages of level INFO and WARNING go to warning.log
# 3. All messages of level ERROR and CRITICAL go into three destinations:
# error.log file, console and admin emails.

LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(levelname)-8s %(asctime)s %(name)-20s %(funcName)-20s %(lineno)-4d %(message)s'
        },
        'simple': {
            'format': '%(levelname)-8s %(message)s'
        },
    },
    'filters': {
        'debug': {
            '()': 'settings.default.log.LevelFilter',
            'levels': (logging.DEBUG,)
        },
        'warning': {
            '()': 'settings.default.log.LevelFilter',
            'levels': (logging.INFO, logging.WARNING)
        },
    },
    'handlers': {
        'console': { # all warnings and errors
            'class': 'logging.StreamHandler',
            'level': logging.ERROR, # critical, error, warning, info, debug
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': logging.ERROR,
            'class': 'common.utils.log.ThrottledAdminEmailHandler',
        },
        'error': { # all errors
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'level': logging.ERROR,
            'formatter': 'verbose',
            'filename': join(ENV_DIR, 'var/log/error.log'),
            'backupCount': 100,
            'delay': True
        },
        'warning': { # all infos and warnings
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'level': logging.INFO,
            'formatter': 'verbose',
            'filters': ['warning'],
            'filename': join(ENV_DIR, 'var/log/warning.log'),
            'backupCount': 100,
            'delay': True
        },
        'database': { # all sql
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'level': logging.DEBUG,
            'formatter': 'verbose',
            'filters': ['debug'],
            'filename': join(ENV_DIR, 'var/log/database.log'),
            'backupCount': 100,
            'delay': True
        },
        'django': { # all django except sql
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'level': logging.DEBUG,
            'formatter': 'verbose',
            'filters': ['debug'],
            'filename': join(ENV_DIR, 'var/log/django.log'),
            'backupCount': 100,
            'delay': True
        },
        'apps': { # all except django and sql
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'level': logging.DEBUG,
            'formatter': 'verbose',
            'filters': ['debug'],
            'filename': join(ENV_DIR, 'var/log/apps.log'),
            'backupCount': 100,
            'delay': True
        },
        'instant': { # all
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'level': logging.DEBUG,
            'formatter': 'verbose',
            'filename': join(ENV_DIR, 'var/log/instant.log'),
            'backupCount': 100,
            'delay': True,
            'when': 'S',
            'interval': 10
        },
        'benchmark': {
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'level': logging.DEBUG,
            'formatter': 'verbose',
            'filename': join(ENV_DIR, 'var/log/benchmark.log'),
            'backupCount': 100,
            'delay': True
        },
        'sentry': {
            'level': 'DEBUG',
            'class': 'sentry.client.handlers.SentryHandler',
        },
    },
    'loggers': { # additional loggers here
        'django.db.backends': { # common handlers (console, error, mail_admins) + database
            'level': logging.DEBUG,
            'propagate': False,
            'handlers': ['database', 'console', 'error', 'mail_admins', 'warning']
        },
        'django': { # common handlers + django
            'level': logging.DEBUG,
            'propagate': False,
            'handlers': ['django', 'console', 'error', 'mail_admins', 'warning']
        },
        'benchmark': {
            'level': logging.DEBUG,
            'propagate': False,
            'handlers': ['benchmark']
        },
        'sentry_debug': {
            'level': logging.DEBUG,
            'propagate': False,
            'handlers': ['sentry']
        }
    },
    'root': { # common handlers + apps
        'level': logging.DEBUG, # used as a first filter. Handlers' levels are used as secondary filters.
        'handlers': ['apps', 'console', 'error', 'mail_admins', 'warning']
    }
}


class LevelFilter(logging.Filter):
    def __init__(self, *args, **kwargs):
        self.levels = kwargs.pop('levels', [10, 20, 30, 40, 50])
        logging.Filter.__init__(self, *args, **kwargs)

    def filter(self, record):
        '''Emit only messages with level in self.levels.'''
        return record.levelno in self.levels
