# used for sending notifications of 500 requests
ADMINS = (
#    ('goldan', 'denis.golomazov+istina@gmail.com'),
)

# used for sending notifications of 404 requests
MANAGERS = ADMINS

# send notifications of 404 requests to managers
SEND_BROKEN_LINK_EMAILS = True

# used for debug template variable and debug_toolbar app
INTERNAL_IPS = ('127.0.0.1', '193.232.119.114', '212.192.237.5', '212.192.251.16')
