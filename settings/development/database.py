from settings.default.database import DATABASES
DATABASES['default'] = DATABASES['development-oracle']
DATABASES['read_write_default'] = DATABASES['default']
