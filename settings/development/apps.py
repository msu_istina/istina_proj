# -*- coding: utf-8 -*-

# django.contrib.messages
from django.contrib.messages import constants as message_constants
MESSAGE_LEVEL = message_constants.DEBUG

# publications app
FREECITE_URL = "http://erizo.s2s.msu.ru:3000/citations/create"
