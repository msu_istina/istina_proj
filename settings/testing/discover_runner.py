from settings import WC_DIR

TEST_DISCOVER_TOP_LEVEL = WC_DIR
# this is the default discover_runner and unittest2 pattern
TEST_DISCOVER_PATTERN = "test*.py"
TEST_RUNNER = "discover_runner.DiscoverRunner"
