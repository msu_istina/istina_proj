# messages framework
from django.contrib import messages
from settings.default.apps import DISAMBIGUATION
MESSAGE_LEVEL = messages.INFO

# publications app
#FREECITE_URL = "http://istina.imec.msu.ru:3000/citations/create"
FREECITE_URL = "http://hedgehog.404.pc.imec.msu.ru:3000/citations/create"


REDMINE_FEEDBACK = {
    'root_url': 'http://93.180.23.137:8080/',
    'issues_url':   'issues.json',
    'users_url':    'users.json',
    'user_to_group_url':    'groups/14/users.json',
    'redmine_api_key':  'ef4808cab95ede429583b68b844480bd5c64bbcf',
    'project_id': '3',
    'tracker_id': '5',
    'status_id': '1',
    'custom_field_id' : '1',
    'representatives_tracker_id': '10',
    'anonymous_tracker_id': '11',
}

DISAMBIGUATION['timeout'] = 12
