# -*- coding: utf-8 -*-
DEFAULT_FROM_EMAIL = u"ИСТИНА <info@istina.msu.ru>"
EMAIL_HOST = 'mx.virtual.msu.ru'
EMAIL_HOST_PASSWORD = ""
EMAIL_HOST_USER = ""
EMAIL_PORT = 25
EMAIL_SUBJECT_PREFIX = u"ИСТИНА | "
EMAIL_USE_TLS = 0
# The e-mail address that error messages come from,
# such as those sent to ADMINS and MANAGERS.
SERVER_EMAIL = u"ИСТИНА - ошибки <error@istina.msu.ru>"
