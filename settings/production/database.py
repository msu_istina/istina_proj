from settings.default.database import DATABASES
# both must be overriden in local_config.py for safety reasons
DATABASES['default'] = {} 
DATABASES['read_write_default'] = DATABASES['default']
