import logging
from settings.default.log import LOGGING

LOGGING['root']['level'] = logging.WARNING
for logger in LOGGING['loggers'].keys():
    LOGGING['loggers'][logger]['level'] = logging.WARNING

# uncomment this to activate database logs
# LOGGING['loggers']['django.db.backends']['level'] = logging.DEBUG

#from production.log import LOGGING, logging
#LOGGING['loggers']['benchmark']['level'] = logging.INFO
