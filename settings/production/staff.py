ADMINS = ()
#ADMINS = (
#    ('goldan', 'denis.golomazov+istina@gmail.com'),
#    ('anton', 'anton@bakhtin.ru'),
#    ('safonin', 'serg@msu.ru'),
#    ('negrega', 'gregorian21@yandex.ru'),
#)

#MANAGERS = ADMINS
MANAGERS = (
    ('goldan', 'denis.golomazov+istina@gmail.com'),
    ('safonin', 'serg@msu.ru'),
    ('maxxk', 'maxim.krivchikov@gmail.com'),
)

FREECITE_MANAGER = ('safonin', 'serg@msu.ru')

# Whether to send an e-mail to the MANAGERS
# each time somebody visits a Django-powered page
# that is 404ed with a non-empty referer (i.e., a broken link).
SEND_BROKEN_LINK_EMAILS = False
