from settings.default.database import DATABASES
DATABASES = {'default': DATABASES['dev-oracle'], 'read_write_default': DATABASES['dev-oracle'],
             'read_only_default': DATABASES['dev-oracle']}
