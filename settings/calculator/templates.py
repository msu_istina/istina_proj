from settings import TEMPLATE_CONTEXT_PROCESSORS

TEMPLATE_CONTEXT_PROCESSORS += (
    'common.context_processors.hg_info',
    'common.context_processors.debug_toolbar_toogle_info',
    'common.context_processors.dummy_debug',
)
