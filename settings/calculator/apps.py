FREECITE_URL = "http://istina.imec.msu.ru:3000/citations/create"

#disable multi-threading for django-select2 on calculator, because calculator restarts every minute and this drives
# django-select2 crazy. Without multithreading django-select2 fields can get response not every time.
GENERATE_RANDOM_SELECT2_ID = False
ENABLE_SELECT2_MULTI_PROCESS_SUPPORT = False
