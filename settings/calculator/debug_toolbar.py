from settings.debug_toolbar.debug_toolbar import MIDDLEWARE_CLASSES


def custom_show_toolbar(request):
    return not request.session.get(DEBUG_TOOLBAR_SESSION_KEY) and not "Siege" in request.META["HTTP_USER_AGENT"]

MIDDLEWARE_CLASSES += ("common.middleware.DebugToolbarToogleMiddleware",)

DEBUG_TOOLBAR_SESSION_KEY = "DISABLE_DEBUG_TOOLBAR"
DEBUG_TOOLBAR_ENABLE_REQUEST = "ENABLE_DEBUG_TOOLBAR"
DEBUG_TOOLBAR_DISABLE_REQUEST = "DISABLE_DEBUG_TOOLBAR"

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': custom_show_toolbar,
}

DEBUG_TOOLBAR_PANELS = (
    #'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.cache.CacheDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    #'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)
